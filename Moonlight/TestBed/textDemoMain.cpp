#include <MoonLight.h>

using namespace std;
using namespace MLKernel;

const wstring text = 

	//ML_STR("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris enim risus, fermentum eget facilisis quis, condimentum quis magna. Nullam aliquet felis arcu, quis hendrerit nibh ullamcorper non. Nulla facilisi. Phasellus scelerisque, sapien quis laoreet semper, augue metus ornare odio, adipiscing cursus elit tortor sit amet nisl. Integer fringilla dapibus nulla vitae viverra. Quisque commodo nibh augue, eget auctor sapien eleifend et. Vivamus tempor rhoncus odio sit amet fringilla. Sed volutpat tempus augue, id pharetra tellus facilisis sit amet. Pellentesque vel consequat sem.");
	ML_STR("Hello\nWorldBLABLABLA\nThis is a lot more text\nthat will require fitting\nto the area of the text box\nyo\0");
	//ML_STR("He\nll\no");

ML_INT main(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, ML_INT nShowCmd)
{
	MLConsole::InitaliseConsole();

	ML_TRACE(ML_STR("Time"), MLConsoleStatus::Information, ML_STR("%s\n"), MLDateTime::Now().ToString().c_str());
	ML_TRACE(ML_STR("Version"), MLConsoleStatus::Information, ML_STR("Moonlight DLL Early Dev Build (Text Demo)\n"));

	MLWindows::MLWindowsContext* WindowContext;

	WindowContext = new MLWindows::MLWindowsContext(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
	WindowContext->Create(L"MoonLight Test Bed", 1280, 720, 32, false);

	MLRender::MLScreenScaler scaler;
	scaler.BaseHeight = 720;
	scaler.BaseWidth = 1280;
	scaler.Method = MLRender::MLScreenScaling::Center;
	scaler.OffsetHeight = 0;
	scaler.OffsetWidth = 0;

	WindowContext->BindToOpenGL();
	MLRender::MLGLContext::Initalise();
	MLRender::MLGLContext::SetClearColour(MLVector4f(0.0f, 0.5f, 0.0f, 1.0f));

	MLRender::MLGLContext::SetOrtho(0, 0, 1280, 720);
	MLRender::MLGLContext::SetScissor(0, 0, 1280, 720);
	MLRender::MLGLContext::SetViewPort(0, 0, 1280, 720);

	MLAudio::MLSoundFX* sfxGroup = new MLAudio::MLSoundFX(ML_STR("TEST_GROUP"));
	MLAudio::MLMusic* music = new MLAudio::MLMusic();
	

	MLInput::MLInputManager::CreateRawKeyboard();
	MLRender::MLFontCore::Initalise();
	MLRender::MLCameraManager::Initalise();
	MLAudio::MLAudioManager::InitaliseAudio();
	MLAudio::MLAudioManager::SetListener();
	WindowContext->SetResizeCallBack(&MLRender::ResizeGL);
	WindowContext->SetInputCallBack(&MLInput::MLInputManager::WindowsMessageHook);
	//MLAudio::MLAudioFile* file = MLAudio::MLAudioManager::LoadAudioFile("Test2.mp3");
	//MLAudio::MLAudioManager::PlayAudio(file);

	ML_STRING ID = sfxGroup->LoadFile(ML_STR("effect.mp3"));
	ML_STRING ID2 = sfxGroup->LoadFile(ML_STR("effect2.mp3"));
	//sfxGroup->Play(ID);

	music->LoadFile(ML_STR("loop2.wav"));
	//music->Play();
	//music->SetLooped(ML_TRUE);

	MLRender::MLGraphics* m_spriteBatch = new MLRender::MLGraphics();
	m_spriteBatch->Initalise(5000, scaler);

	MLKernel::MLVirtualPath* p = new MLKernel::MLVirtualPath();
	MLKernel::MLVirtualFile* File = p->GetFile(ML_STR("GARABD.TTF"));

	MLRender::MLTrueTypeFont font;
	font.LoadFont(File, ML_FALSE, 256);
	font.SetSize(50);
	font.SetPaddingX(8);
	font.SetPaddingY(8);
	font.SetTextureWidth(1024);
	font.SetTextureHeight(1024);
	font.LoadDefaultGlyphs();

	MLRender::MLWrite* writer = new MLRender::MLWrite();
	writer->Font = &font;
	writer->Colour = MLVector4f(0.34f, 0.67f, 0.89f, 1.0f);

	writer->Position = MLVector2f(640.0f, 360.0f);
	writer->Origin = 0.0f;
	writer->Alignment = MLRender::MLJustifyText::LeftJustify;

	writer->TBoxSize = MLVector2f(250, 250);

	MLRender::MLPoint origin;
	origin.Position = writer->Position + writer->Origin;
	origin.Size = 5.0f;
	origin.Colour = MLVector4f(1.0f, 0.78f, 0.4f, 1.0f);

	MLRender::MLQuad quad;
	
	quad.Size = MLVector2f(650, 250);
	quad.LineWidth = 2;
	quad.Filled = ML_FALSE;
	quad.Colour = MLVector4f(0.76f,0.18f,0.34f,1.0f);
	quad.Origin = MLVector2f(-quad.Size.x/2.0f, -quad.Size.y/2.0f);
	
	ML_FLOAT rot = 0.0f;
	ML_FLOAT rotmod = 0;

	ML_BOOL wireFrame = ML_FALSE;

	
	writer->LineThickness = 2.0f;
	
	ML_BOOL prtScrDown = ML_FALSE;

	wstring text = ML_STR("Hello World Im some font\0");

	ML_BOOL DrawOrigin = ML_FALSE;
	ML_BOOL DrawQuad = ML_FALSE;
	ML_BOOL keydown = ML_FALSE;
	ML_BOOL keydown2 = ML_FALSE;

	MLInput::MLController* controller = new MLInput::MLController();
	controller->SetControllerID(1);

	MLRender::MLLine cursor;

	

	cursor.Colour = writer->Colour;
	cursor.Width = 2;
	cursor.Point1 = MLVector2f(writer->Position + writer->Origin);
	cursor.Point2.x = cursor.Point1.x;
	cursor.Point2.y = cursor.Point1.y + font.GetSize();
	
	ML_FLOAT cursorStartx = cursor.Point1.x;
	
	ML_BOOL recording = ML_FALSE;

	MLRender::MLSprite sprite;
	sprite.Texture = MLRender::MLTextureManager::LoadTexture(ML_STRING(ML_STR("av.png")), ML_STRING(ML_STR("av.png")));
	sprite.Size = MLVector2f(1280, 720);
	sprite.Position = MLVector2f(0, 0);
	sprite.SetSourceFromTexture();
	sprite.Colour = MLVector4f(1.0f);
	sprite.Layer = -1;

	ML_SCHAR pos[30];

	while (1)
	{
		

		MLRender::MLGLContext::StartFrame();

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::One))
		{
			scaler.Method = MLRender::MLScreenScaling::None;
			m_spriteBatch->SetScreenScaler(scaler);

		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Two))
		{
			scaler.Method = MLRender::MLScreenScaling::Strech;
			m_spriteBatch->SetScreenScaler(scaler);
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Three))
		{
			scaler.Method = MLRender::MLScreenScaling::BestFit;
			m_spriteBatch->SetScreenScaler(scaler);
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Four))
		{
			scaler.Method = MLRender::MLScreenScaling::Zoom;
			m_spriteBatch->SetScreenScaler(scaler);
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Five))
		{
			scaler.Method = MLRender::MLScreenScaling::Center;
			m_spriteBatch->SetScreenScaler(scaler);
		}

		m_spriteBatch->Begin(ML_TRUE, wireFrame);

		if (controller->IsButtonPressed(MLInput::MLControllerButtons::A))
		{
			ML_TRACE(ML_STR("Controller"), MLConsoleStatus::Information, ML_STR("Controller button A pressed trigger = %1.4f %1.4f\n"), controller->GetAnalog(MLInput::MLControllerAnalogs::TriggerAnalog).x, controller->GetAnalog(MLInput::MLControllerAnalogs::TriggerAnalog).y);
			
		}
		if (controller->IsButtonHeld(MLInput::MLControllerButtons::A))
		{
			controller->SetForceFeedback(1.0f);
		}
		else
		{
			controller->SetForceFeedback(0.0f);
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F6))
		{
			WindowContext->SetDisplayMode(ML_TRUE);
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F7))
		{
			WindowContext->SetDisplayMode(ML_FALSE);
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F5))
		{
			
			if (!recording)
			{
				recording = ML_TRUE;
				MLInput::MLInputManager::StartRecordKeyString();
			}
			else
			{
				recording = ML_FALSE;
				MLInput::MLInputManager::StopRecordingKeyString();
			}

			ML_TRACE(ML_STR("F5"), MLConsoleStatus::Information, ML_STR("Setting Record State %s\n"), (recording ? ML_STR("true") : ML_STR("false")));
		}

		if (recording)
		{
			text = MLInput::MLInputManager::GetRecordKeyString();
			//this is temp to function like this as it wont work with multiline
			ML_FLOAT pos = font.GetTextWidth(text.substr(0, MLInput::MLInputManager::GetIndexPosition()));
			cursor.Point1.x = cursorStartx + pos;
			cursor.Point2.x = cursorStartx + pos;
		}

		/*if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F1) && !keydown)
		{
		sfxGroup->Play(ID);
		keydown = ML_TRUE;
		}
		if (!MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F1))
		{
		keydown = ML_FALSE;
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F2) && !keydown2)
		{
		sfxGroup->Play(ID2);
		keydown2 = ML_TRUE;
		}
		if (!MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::F2))
		{
		keydown2 = ML_FALSE;
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::PrintScreen) && prtScrDown == ML_FALSE)
		{
		MLScreenshot::Screenshot();
		prtScrDown = ML_TRUE;
		}
		if (!MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::PrintScreen) && prtScrDown == ML_TRUE)
		prtScrDown = ML_FALSE;

		writer->Rotation = (ML_FLOAT)ML_Deg2Rad(rot);
		quad.Rotation = (ML_FLOAT)ML_Deg2Rad(rot);
		rot += rotmod;
		if(rot >= 360 && rotmod != 0)
		rotmod = 0.05;
		else if (rot <= 0 && rotmod != 0)
		rotmod = -0.05;

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::One))
		{
		text = ML_STR("Hello World Im some font\0");
		writer->Scale = 1.0f;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Two))
		{
		text = ML_STR("Hello World Im some font x1.5\0");
		writer->Scale = 1.5f;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Three))
		{
		writer->Scale = 1.0f;
		text = ML_STR("Hello World\nCentre Aligned Text\0");
		writer->Alignment = MLJustifyText::CentreJustify;
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Four))
		{
		text = ML_STR("Hello World\nRight Aligned Text\0");
		writer->Alignment = MLJustifyText::RightJustify;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Five))
		{
		writer->Origin.x = -(font.GetTextWidth(text)/2.0f);
		writer->Origin.y = -(font.GetTextHeight(text)/2.0f);
		writer->Position = MLVector2f(200.0f, 200.0f) - (writer->Origin);
		origin.Position = writer->Position;
		text = ML_STR("Hello World\nMy Origin Is centred\0");
		writer->Alignment = MLJustifyText::LeftJustify;
		DrawOrigin = ML_TRUE;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Six))
		{
		writer->Origin.x = -(font.GetTextWidth(text)/2.0f);
		writer->Origin.y = -(font.GetTextHeight(text)/2.0f);
		writer->Position = MLVector2f(200.0f, 200.0f) - (writer->Origin);
		origin.Position = writer->Position;
		text = ML_STR("Hello World\nI Can Rotate Around It\0");
		rot = 0.0f;
		rotmod = 0.05f;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Seven))
		{
		text = ML_STR("Hello World\nI Can Change It\0");
		writer->Position = MLVector2f(200.0f, 200.0f);
		origin.Position = writer->Position;
		writer->Origin = 0.0f;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Eight))
		{
		rot = 0.0f;
		rotmod = 0.0f;
		text = ML_STR("Hello World\nI Can Highlight Text and stop it\0");

		writer->HighlightStart	= 12;
		writer->HighlightEnd	= 32;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Nine))
		{
		writer->HighlightStart	= 0;
		writer->HighlightEnd	= 11;
		text = ML_STR("Hello World\nI Can strike Text\0");
		writer->LineType = MLFontLine::Strike;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Zero))
		{
		text = ML_STR("Hello World\nOr Underline it\0");
		writer->LineType = MLFontLine::Underline;
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Q))
		{
		writer->TBoxPositionOffset = 0.0f;
		DrawQuad = ML_TRUE;
		quad.Position = writer->Position;
		quad.Origin = 0.0f;
		writer->TBoxSize = quad.Size;
		writer->TBoxEnabled = ML_TRUE;
		writer->TBoxFlow = ML_TRUE;
		text = ML_STR("Hello World\nI Can Put My Text In A\nBounding Box\0");
		}

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::W))
		{
		writer->HighlightStart	= 0;
		writer->HighlightEnd	= 0;
		writer->TBoxPositionOffset = 0.0f;
		DrawQuad = ML_TRUE;
		quad.Position = writer->Position;
		quad.Origin = 0.0f;
		quad.Size = MLVector2f(200.0f, quad.Size.Height);
		writer->TBoxSize = quad.Size;
		writer->TBoxEnabled = ML_TRUE;
		writer->TBoxFlow = ML_TRUE;
		text = ML_STR("Hello World With No \\n The Text Can also Be wrapped to the size of the text box\0");
		text = font.WrapText(quad.Size.Width, text);
		}*/

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Up))
		{
			MLRender::MLCameraManager::MoveActiveCamera(0, -1);
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Down))
		{
			MLRender::MLCameraManager::MoveActiveCamera(0, 1);
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Left))
		{
			MLRender::MLCameraManager::MoveActiveCamera(-1, 0);
			MLInput::MLInputManager::SetRecordingIndex(MLInput::MLInputManager::GetIndexPosition() - 1);
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::Right))
		{
			MLRender::MLCameraManager::MoveActiveCamera(1, 0);
			MLInput::MLInputManager::SetRecordingIndex(MLInput::MLInputManager::GetIndexPosition() + 1);
		}

		swprintf_s(pos, 30, ML_STR("x: %4.1f, y: %4.1f\n"), MLInput::MLInputManager::GetMousePositon(scaler).x, MLInput::MLInputManager::GetMousePositon(scaler).y);
		text = pos;
		swprintf_s(pos, 30, ML_STR("x: %4.1f, y: %4.1f"), MLInput::MLInputManager::GetMousePositon().x, MLInput::MLInputManager::GetMousePositon().y);
		text += pos;
		WindowContext->Update();

		m_spriteBatch->DrawPoint(&origin);

		m_spriteBatch->DrawLine(&cursor);

		m_spriteBatch->Write(writer, text);

		if (DrawQuad)
			m_spriteBatch->DrawQuad(&quad);

		m_spriteBatch->Draw(&sprite);

		m_spriteBatch->End();

		//m_spriteBatch->Begin();

		//origin.Position = MLVector2f(0, 0);

		//m_spriteBatch->DrawPoint(&origin);

		////GLContext->SetScreen(10, -10, MLScreenWidth, MLScreenHight);

		//

		//m_spriteBatch->End();

		
		
		origin.Position = writer->Position + writer->Origin;
		
		
		
		

		MLRender::MLGLContext::EndFrame();

		if (MLInput::MLInputManager::IsKeyPressed(MLInput::MLKeys::PrintScreen))
		{
			MLRender::MLScreenshot::Screenshot();
		}
	}
}

