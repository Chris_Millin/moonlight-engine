#include <MoonLight.h>
//using namespace MLMath::MLVector3;

using namespace std;

MLRenderer::MLSpriteBatch* sb;
MLWindows::windowsHandler* winHand;

using namespace  MLMath::MLVecFunc;


struct Shape
{
	std::vector<Vector2f> Points;
	std::vector<int> PointIDs;
};


#pragma region Polygon Triangulation

typedef unsigned int uint;

vector<int> removedPoints;

//bool PointAvaliable(Shape currentShape, uint point)
//{
//	return true;
//}
//
//vector<Shape> BreakDownShape(Shape currentShape)
//{
//
//}
//
//void FindConvex(Shape currentShape,uint startPointID, uint checkPointID, Shape* resultShape, uint* resultOffset)
//{
//
//}
//
//vector<Shape> DecompositionPoly(Shape baseShape, Shape currentShape)
//{
//	//This will store the final convex shapes
//	vector<Shape> finalShapes;
//	
//	//will store the shape for the first convex
//	Shape resultShape;
//	//add point 0 and 1 as they should not have been used yet and since
//	//there are only 2 points it cant be anti clockwise until the end of
//	//shape (n to 0 to 1 is anti clockwise sometimes so 0 to 1 will be ignored
//	//this check is done at the end any way so it wont matter until then)
//	resultShape.PointIDs.push_back(currentShape.PointIDs[0]);
//	resultShape.PointIDs.push_back(currentShape.PointIDs[1]);
//	//the last added point is taken
//	uint lastPoint = currentShape.PointIDs[1];
//	//the point to check is set to 0
//	uint pointToCheck = 0;
//	//the point offset which is the offset in the array where the first point
//	//of the convex shape starts.
//	uint resultOffset = 0;
//	
//	//find the first convex shape
//	FindConvex(currentShape, lastPoint, pointToCheck, &resultShape, &resultOffset);
//
//	//this will store the full points of the convex as currently result shape may have an offset
//	Shape finalShape;
//	for (int p = resultOffset; p < resultShape.PointIDs.size(); p++)
//	{
//		finalShape.PointIDs.push_back(resultShape.PointIDs[p]);
//		finalShape.Points.push_back(baseShape.Points[resultShape.PointIDs[p]]);
//	}
//
//	//set the final shape to the same as the result shape to allow for the offset to now be ignored
//	resultShape = finalShape;
//
//	//any unneeded points are removed from the current shape 
//	for (int i = 0; i < resultShape.PointIDs.size(); i++)
//	{
//		int pointA = 0, pointB = 0;
//		pointA = resultShape.PointIDs[i] + 1 >= i ? 0 : resultShape.PointIDs[i] + 1;
//		pointB = resultShape.PointIDs[i] - 1 < 0 ? i - 1 : resultShape.PointIDs[i] - 1;
//		/*if (!isPointAvaliable(pointA) && !isPointAvaliable(pointB) && !isPointAvaliable(resultShape.PointIDs[i]))
//		{
//		removedPoints.push_back(resultShape.PointIDs[i]);
//		}*/
//	}
//
//	//after the points are removed the current shape is broken down into one or more
//	//new shapes which will then perform the shape task to remove the convex shapes.
//	vector<Shape> nextShapes = BreakDownShape(currentShape);
//
//
//
//}
//




#pragma endregion

#pragma region Convex_Shape_Creation

struct ConvexPoly
{
	std::vector<Vector2f> Points;
	std::vector<int> PointIDs;
};

//This will store all the convex polygon shapes
vector<ConvexPoly> polys;

//this will store the current pointID's from array
vector<int> cPointID;
//this will store the finished pointID's. this will be
//all the points that can not be used any more
vector<int> fPointID;
//this will store the id of the starting point in cPointID
//it will only change if cPointID[n] to cPointID[0] ends up
//with an invalid line and cPointID[n] to cPointID[1<] has to
//be used and the id is stored
int cPointStart;

bool isPointAvaliable(int point)
{
	if (fPointID.size() == 0 && cPointID.size() == 0)
	{
		return true;
	}

	if (fPointID.size() > 0)
	{
		if ((std::find(fPointID.begin(), fPointID.end(), point)!=fPointID.end()))
		{
			return false;
		}
	}
	if (cPointID.size() > 0)
	{
		if (std::find(cPointID.begin(), cPointID.end(), point)!=cPointID.end())
		{
			return false;
		}
	}

	return true;
}

bool endConvex = false;
int indexCounter = 0;
void createConvex(Vector2f* array, int count, int lastPoint, int checkPoint)
{
	bool pValid = false;
	int crossP = 0;

	if (lastPoint >= count - 1)
	{
		lastPoint = -1;
		while(pValid == false)
		{
			lastPoint += 1;
			pValid = isPointAvaliable(lastPoint);
			if (lastPoint == cPointID[0])
			{
				break;
			}
		}
	}

	if (cPointID[0] == (lastPoint))
	{
		endConvex = true;
		lastPoint = cPointID[0];
		//at this point there should be two checks
		//one for lastpoint - 1, lastpoint and firstpoint
		//and nother for lastpoint, firstpoint, second point
		//if any of then result in counter clockwise then
		//if the first is ccw then last to first is invalid
		//if the second is ccw then first to second is invalid
		//due to the angle of the lastpoint
		crossP = RotationDirection(array[cPointID[checkPoint]], array[cPointID[checkPoint + 1]], array[lastPoint]);

		if (crossP >= 0)
		{
			cPointID.push_back(lastPoint);
			if (cPointID.size() >= 3)
			{
				checkPoint++;
			}
			lastPoint = cPointID[1];
		}
	}

	if (endConvex == false)
	{
		while(pValid == false)
		{
			lastPoint += 1;
			pValid = isPointAvaliable(lastPoint);
		}

		crossP = RotationDirection(array[cPointID[checkPoint]], array[cPointID[checkPoint + 1]], array[lastPoint]);

		if (crossP >= 0)
		{
			cPointID.push_back(lastPoint);
			if (cPointID.size() >= 3)
			{
				checkPoint++;
			}
		}
		createConvex(array, count, lastPoint, checkPoint);
	}
	else
	{
		crossP = RotationDirection(array[cPointID[checkPoint]], array[cPointID[checkPoint + 1]], array[lastPoint]);

		if (crossP >= 0)
		{
			cPointID.push_back(lastPoint);
			return;
		}
		else
		{
			lastPoint = cPointID[++indexCounter];
			createConvex(array, count, lastPoint, checkPoint);
		}
	}
}

void drawConvex(Vector2f* array, int count)
{
	
	int lastPoint = 0;
	int checkPoint = 0;
	indexCounter = 0;
	endConvex = false;
	polys.clear();
	cPointID.clear();
	fPointID.clear();
	cPointStart = 0;
	//pick the first two points from 0 - 1 (this should never intersect another line)
	cPointID.push_back(0);
	cPointID.push_back(1);
	lastPoint = 1;
	//now the two points have been set the next step is to look at look to the next point
	//before that the next point in this case will be (2) needs to be checked to see if it
	//is valid
	//by valid it means has it already been used and is it not in fPointID
	while(fPointID.size() < count)
	{
		createConvex(array, count, lastPoint, checkPoint);
		
		ConvexPoly newPoly;
		for (int i = indexCounter + 1; i < cPointID.size(); i++)
		{
			newPoly.Points.push_back(array[cPointID[i]]);
			newPoly.PointIDs.push_back(cPointID[i]);
		}

		cPointID = newPoly.PointIDs;

		for (int y = 0; y < newPoly.Points.size(); y++)
		{
			int pointA = 0, pointB = 0;
			pointA = newPoly.PointIDs[y] + 1 >= count ? 0 : newPoly.PointIDs[y] + 1;
			pointB = newPoly.PointIDs[y] - 1 < 0 ? count - 1 : newPoly.PointIDs[y] - 1;

			if (!isPointAvaliable(pointA) && !isPointAvaliable(pointB) && !isPointAvaliable(newPoly.PointIDs[y]))
			{
				fPointID.push_back(newPoly.PointIDs[y]);
			}
		}

		polys.push_back(newPoly);

		if (fPointID.size() < count)
		{
			lastPoint = -1;
			cPointID.clear();
			bool pValid = false;
			while(pValid == false)
			{
				lastPoint += 1;
				pValid = isPointAvaliable(lastPoint);
			}
			cPointID.push_back(lastPoint);
			pValid = false;
			indexCounter = 0;
			endConvex = false;
			checkPoint = 0;
			while(pValid == false)
			{
				lastPoint += 1;
				pValid = isPointAvaliable(lastPoint);
			}
			cPointID.push_back(lastPoint);
		}
		
		sb->Begin();

		float colourChange = 1.0f / (float)polys.size();
		float colour = colourChange;
		for (int i = 0; i < polys.size(); i++)
		{
			sb->DrawLine(polys[i].Points[0], polys[i].Points[1], 1, Vector4f(0.5f,0.0f,colour,1.0f));
			for (int y = 1; y < polys[i].Points.size() - 1; y++)
			{
				sb->DrawLine(polys[i].Points[y], polys[i].Points[y + 1], 1, Vector4f(0.5f,0.0f,colour,1.0f));
			}
			sb->DrawLine(polys[i].Points[polys[i].Points.size() - 1], polys[i].Points[0], 1, Vector4f(0.5f,0.0f,colour,1.0f));
			colour += colourChange;
		}

		sb->End();

		winHand->update();

		if (MLInput::MLInputManager::IsKeyPressed(MLKeys::NPFive))
		{
			break;
		}

	}

	
	//now a valid point is found it needs to be check with the last 2 to see if it is clockwise
	


	

}

#pragma endregion Convex Shape Creation

int main(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	winHand = new MLWindows::windowsHandler(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
	winHand->createWindow(L"MoonLight Test Bed", 1280, 720, 32, true);
	//set up the openGL stuff
	MLRenderer::MLCoreGL* _GLCore = new MLRenderer::MLCoreGL();
	_GLCore->Bind(hInstance, MLWindows::windowsHandler::_hWnd, MLWindows::windowsHandler::_wWidth, MLWindows::windowsHandler::_wHeight, MLWindows::windowsHandler::_BitMode);
	_GLCore->Initalise();

	MLInput::MLInputManager::Initalise();
	MLInput::MLInputManager::CreateRawKeyboard();
	MLRenderer::MLText::MLFontCore::Initalise();
	MLRenderer::MLCameraManager::Initalise();
	MLRenderer::MLParticleEmitter::Initalise();
	MLAudio::MLAudioManager::InitaliseAudio(false, 0);
	MLWindows::setResizeFunction(&MLRenderer::ResizeGL);
	MLWindows::setInputFunction(&MLInput::MLInputManager::InputMSGHandler);
	


	sb = new MLRenderer::MLSpriteBatch();
	
	int textureID = MLRenderer::MLTextureManager::LoadTexture("Char.jpg");

	MLFormats::MLEmitterSettings eSetting;
	eSetting.maxSpriteCount = 1000;
	eSetting.amount = 150;
	eSetting.emitHeight = 10000.0f;
	eSetting.emitWidth = 10000.0f;
	eSetting.emitLife = 2;
	eSetting.emitTime = 0.02f;
	eSetting.emitSpeed = 5.0f;
	eSetting.minAngle = 0;
	eSetting.maxAngle = 360;
	eSetting.tintColour = Vector3f(1.0f);
	eSetting.setTimeAsZaxis = true;
	eSetting.spriteWidth = MLRenderer::MLTextureManager::GetWidth(textureID);
	eSetting.spriteHeight = MLRenderer::MLTextureManager::GetHeight(textureID);

	MLFormats::MLParticleEffect* pEffect = new MLFormats::MLParticleEffect();
	pEffect->SetShader(MLRenderer::MLParticleEmitter::GetShader());
	pEffect->SetDeadCount(0);
	pEffect->SetElapsedTime(0);
	pEffect->SetPointCount(0);
	pEffect->SetSettings(eSetting);


	//set up the keyboard initalise the font core and also set the resize and input functions
	
	MLAudio::MLAudioManager::SetListenerPosition(Vector3f(0.0f, 0.0f, 0.0f));
	MLAudio::MLAudioFile* audio = MLAudio::MLAudioManager::LoadAudioFile("Test.mp3");
	MLAudio::MLAudioManager::PrintAudioInfo(audio);
	//MLAudio::MLAudioManager::PlayAudio(audio);
	//MLAudio::MLAudioManager::PrintAudioInfo(audio);

	
	
	MLPolyList points;
	MLTriangulation trang;
	std::vector<Vector2f> trisPoints;

	Vector2f msPos; 
	float crossP;

	bool mousedown = false;
	bool mousedownR = false;
	bool completeShape = false;
	bool spacedown = false;

	bool holeMode = false;

	int child = 0;

	while(1)
	{
		msPos = MLInput::MLInputManager::GetMousePositon();
		if ((MLInput::MLInputManager::IsMouseButtonPressed(MLMouseButtons::MLeft) == true) && mousedown == false)
		{
			if (holeMode == false)
			{
				points.AddPoint(Vector2f(msPos.x, msPos.y));
			}
			if(holeMode == true)
			{
				MLPolyList* childPoly = points.GetChild(child);
				if (childPoly == NULL)
				{
					MLPolyList* newChild = new MLPolyList(&points, msPos);
				}
				else
					childPoly->AddPoint(msPos);
			}
			mousedown = true;
		}
		if ((MLInput::MLInputManager::IsMouseButtonPressed(MLMouseButtons::MLeft) == false) && mousedown == true)
		{
			mousedown = false;
		}
		if ((MLInput::MLInputManager::IsMouseButtonPressed(MLMouseButtons::MRight) == true) && mousedown == false)
		{
			completeShape = true;
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLKeys::Enter))
		{
			completeShape = false;
			points.Clear();
		}
		if (MLInput::MLInputManager::IsKeyPressed(MLKeys::Space) && !spacedown)
		{
			if (holeMode)
			{
				holeMode = false;
			}
			else
			{
				holeMode = true;
			}
			spacedown = true;
		}
		if (!MLInput::MLInputManager::IsKeyPressed(MLKeys::Space) && spacedown)
		{
			spacedown = false;
		}

		_GLCore->StartFrame();
		
		MLAudio::MLAudioManager::UpdateAudio();
		winHand->update();
		
		sb->Begin();
		
		if (points.Size() > 1)
		{
			MLPolyNode* current = points.Start();
			unsigned int count = points.Size();
			while(count != 0 && count >= 0)
			{
				sb->DrawLine(current->point, current->next->point, 0.5, Vector4f(0.0f,1.0f,0.6f,1.0f));
				current = current->next;
				count--;
			}
			sb->DrawPoint(current->previous->point, 5, Vector4f(0.0f,1.0f,0.6f,1.0f));

			for (int i = 0; i < points.ChildSize(); i++)
			{
				MLPolyList* child = points.GetChild(i);
				MLPolyNode* currentN = child->Start();
				unsigned int countS = child->Size();
				while(countS != 0 && countS >= 0)
				{
					sb->DrawLine(currentN->point, currentN->next->point, 0.5, Vector4f(0.5f,1.0f,0.6f,1.0f));
					currentN = currentN->next;
					countS--;
				}
				sb->DrawPoint(currentN->previous->point, 5, Vector4f(0.5f,1.0f,0.6f,1.0f));
			}

		}
		

		if (completeShape == true)
		{
			for (int i = 0; i < trisPoints.size(); i+=3)
			{
				sb->DrawLine(trisPoints[i], trisPoints[i+1], 1.0, Vector4f(0.7f,1.0f,0.0f,1.0f));
				sb->DrawLine(trisPoints[i+1], trisPoints[i+2], 1.0, Vector4f(0.7f,1.0f,0.0f,1.0f));
				sb->DrawLine(trisPoints[i+2], trisPoints[i], 1.0, Vector4f(0.7f,1.0f,0.0f,1.0f));
			}
		}

		if (holeMode)
			sb->DrawPoint(Vector2f(msPos.x, msPos.y), 10,  Vector4f(1.0f,0.0f,0.0f,1.0f));
		else
			sb->DrawPoint(Vector2f(msPos.x, msPos.y), 10);

		sb->End();

		if(completeShape == true && points.Size() > 2)
		{
			trisPoints = trang.Triangulate(&points);
		}

		_GLCore->EndFrame();
	}

};