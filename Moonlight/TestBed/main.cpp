#include <MoonLight.h>

using namespace std;
using namespace MLKernel;

ML_DWORD WorkThread(void* data)
{
	int* newdata = (int*)data;
	for (int i = 0; i < *newdata; i++)
	{
		ML_TRACE(ML_STR("WorkThread"), MLConsoleStatus::Thread, ML_STR("Im working %d %d\n"), i, *newdata);
	}

	return 0;
}

int dataInt = 7000;

ML_INT main(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, ML_INT nShowCmd)
{
	

	MLConsole::InitaliseConsole();

	ML_TRACE(ML_STR("Time"), MLConsoleStatus::Information, ML_STR("%s\n"), MLDateTime::Now().ToString().c_str());
	ML_TRACE(ML_STR("Version"), MLConsoleStatus::Information, ML_STR("Moonlight DLL Early Dev Build\n"));

	MLWindows::MLWindowsContext* WindowContext;

	WindowContext = new MLWindows::MLWindowsContext(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
	WindowContext->Create(L"MoonLight Test Bed", 1280, 720, 32, ML_TRUE);
	
	MLKernel::MLUUID* id = new MLKernel::MLUUID();
	id->Generate();

	

	//MLKernel::MLThread* thread = new MLKernel::MLThread(&WorkThread, &dataInt);
	//thread->ThreadCreate(&WorkThread, &dataInt);
	

	//set up the openGL stuff
	MLRender::MLGLContext* GLContext = new MLRender::MLGLContext();
	WindowContext->BindGLContext(GLContext);
	GLContext->Initalise();
	GLContext->SetClearColour(MLVector4f(0.0f,0.5f,0.0f,1.0f));

		
	MLInput::MLInputManager::Initalise();
	MLInput::MLInputManager::CreateRawKeyboard();
	MLRender::MLText::MLFontCore::Initalise();
	MLRender::MLCameraManager::Initalise();
	MLAudio::MLAudioManager::InitaliseAudio(ML_FALSE, 0);
	WindowContext->SetResizeCallBack(&MLRender::ResizeGL);
	WindowContext->SetInputCallBack(&MLInput::MLInputManager::InputMSGHandler);

	MLGraphics::Initalise(5000);

	MLKernel::MLVirtualPath* p = new MLKernel::MLVirtualPath();
	MLKernel::MLVirtualFile* File = p->GetFile(ML_STR("GARABD.TTF"));
	MLKernel::MLVirtualFile* File2 = p->GetFile(ML_STR("bmpFNT.fnt"));

	MLText::MLBitmapFont font2;
	font2.LoadFontXML(File2);

	MLRender::MLText::MLFontCore::Initalise();
	MLText::MLTrueTypeFont font;
	font.LoadFont(File, ML_FALSE, 256);
	font.SetSize(60);
	font.SetPaddingX(8);
	font.SetPaddingY(8);
	font.SetTextureWidth(1024);
	font.SetTextureHeight(1024);
	font.LoadDefaultGlyphs();

	wstring text = 

		//ML_STR("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris enim risus, fermentum eget facilisis quis, condimentum quis magna. Nullam aliquet felis arcu, quis hendrerit nibh ullamcorper non. Nulla facilisi. Phasellus scelerisque, sapien quis laoreet semper, augue metus ornare odio, adipiscing cursus elit tortor sit amet nisl. Integer fringilla dapibus nulla vitae viverra. Quisque commodo nibh augue, eget auctor sapien eleifend et. Vivamus tempor rhoncus odio sit amet fringilla. Sed volutpat tempus augue, id pharetra tellus facilisis sit amet. Pellentesque vel consequat sem.");
		ML_STR("Hello\nWorldBLABLABLA\0");

	

	MLWrite* writer = new MLWrite();
	writer->Position = MLVector2f(50.0f, 190.0f);
	writer->Font = &font2;

	MLWrite* writer2 = new MLWrite();
	writer2->Position = MLVector2f(400.0f, 190.0f);
	writer2->Font = &font;

	MLTexture* sdfTest = new MLTexture();
	sdfTest->LoadTexture("test3.png");
	//printf("[TEXTURE] Started Editing");
	//sdfTest->StartEditing();
	//sdfTest->ConverttoGrayScale();
	//sdfTest->ExportImage("Export_TestFont1.png");
	//printf("[TEXTURE] Started SD");
	//sdfTest->ConverttoSignDistanceField();
	//printf("[TEXTURE] Exporting SD");
	//sdfTest->ExportImage("Export_TestFont2.png");
	//printf("[TEXTURE] End Editing");
	//sdfTest->EndEditing();


	MLSprite newSprite;
//	MLTexture* text = font.GetFontTexture(0);
	//text->StartEditing();
	//text->ExportImage("TestBefore.png");
	//text->ConverttoGrayScale();
	//text->ExportImage("Test2.png");
	//text->ConverttoSignDistanceField(1, 6);
	//text->ExportImage("sdf.png");
	//text->EndEditing();

	newSprite.Texture = sdfTest;
	newSprite.SetSizeFromTexture();
	newSprite.SetSourceFromTexture();
	newSprite.Colour = MLVector4f(1.0f);
	newSprite.Source.x -=1;
	newSprite.Source.y -=1;
	newSprite.Source.Width -=1;
	newSprite.Source.Height -=1;
	newSprite.Origin = 100.0f;
	//newSprite.SetEffect(MLSpriteEffect::FlippedHoriz);


	MLShader* sdfShader = new MLShader();
	sdfShader->LoadShaderFile("shaders\\SignedDistanceField", GL_VERTEX_SHADER);
	sdfShader->LoadShaderFile("shaders\\SignedDistanceField", GL_FRAGMENT_SHADER);
	sdfShader->Lock();
	sdfShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXPOS,	BASE_SHADER_VERT_POS);
	sdfShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXCOL,	BASE_SHADER_VERT_COL);
	sdfShader->BindAtt(MLRender::MLAttID::ML_ATT_TEXTURE0,	BASE_SHADER_VERT_UV);
	sdfShader->LinkShaders();
	sdfShader->Unlock();

	ML_FLOAT adder = -0.0004f;

	MLPoint origin;
	origin.Position = writer->Position - writer->Origin;
	origin.Colour = 1.0f;
	origin.Size = 5.0f;

	MLQuad quad;
	MLQuad quad2;

	ML_INT scaleChance = -1;
	ML_UINT scale = 500;
	newSprite.Shader = sdfShader;
	ML_FLOAT rot = 0.0f;
	ML_FLOAT rotmod = 0.05;

	ML_BOOL wireFrame = ML_FALSE;

	writer->Colour = MLVector4f(0.34f, 0.67f, 0.89f, 1.0f);
	writer2->Colour = MLVector4f(0.34f, 0.67f, 0.89f, 1.0f);

	ML_BOOL prtScrDown = ML_FALSE;

	while(1)
	{
		writer2->Rotation = (ML_FLOAT)ML_Deg2Rad(rot);
		//rot += rotmod;
		if(rot >= 360)
			rotmod = 0.05;
		else if (rot <= 0)
			rotmod = -0.05;



		//wstring text2 = font2.WrapText(scale, text);

		writer2->Origin.x = -(font.GetTextWidth(text)/2.0f);
		writer2->Origin.y = -(font.GetTextHeight(text)/2.0f);

		writer->Origin.x = -(font2.GetTextWidth(text)/2.0f);
		writer->Origin.y = -(font2.GetTextHeight(text)/2.0f);

		quad.Position = MLVector2f(50.0f, 30.0f) + writer->Origin;
		quad.Size = MLVector2f(font2.GetTextWidth(text), font2.GetTextHeight(text));
		quad.LineWidth = 2.0f;
		quad.Filled = ML_FALSE;
		quad.Colour = MLVector4f(0.76f,0.18f,0.34f,1.0f);

		quad2.Position = (writer2->Position + (writer2->Origin  * writer2->Scale));
		quad2.Size = MLVector2f(font.GetTextWidth(text), font.GetTextHeight(text))  * writer2->Scale;
		quad2.LineWidth = 2.0f;
		quad2.Filled = ML_FALSE;
		quad2.Colour = MLVector4f(0.76f,0.18f,0.34f,1.0f);

		/*scale+= scaleChance;
		if(scale == 200)
			scaleChance = 1;
		if(scale == 500)
			scaleChance = -1;*/
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::One))
		{
			writer->Scale = 1.0f;
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::Two))
		{
			writer->Scale = 2.2f;
		}


		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::F1))
		{
			wireFrame = ML_FALSE;
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::F2))
		{
			wireFrame = ML_TRUE;
		}
		
		MLGraphics::Begin(ML_TRUE, wireFrame);

		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::Up))
		{
			newSprite.Scale.x += 0.002f;
			newSprite.Scale.y += 0.002f;
			newSprite.Rotation += ML_Deg2Rad(5);
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::Down))
		{
			newSprite.Scale.x -= 0.002f;
			newSprite.Scale.y -= 0.002f;
			newSprite.Rotation -= ML_Deg2Rad(5);
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::W))
		{
			newSprite.Position.y -= 20;
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::A))
		{
			newSprite.Position.x -= 20;
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::S))
		{
			newSprite.Position.y += 20;
		}
		if(MLInput::MLInputManager::IsKeyPressed(MLKeys::D))
		{
			newSprite.Position.x += 20;
		}
		GLContext->StartFrame();
		MLAudio::MLAudioManager::UpdateAudio();
		WindowContext->Update();

		writer2->Alignment = MLJustifyText::RightJustify;
		//newSprite.Rotation = ML_Deg2Rad(20);
		//newSprite.Origin = newSprite.Size/2;
		//MLGraphics::Draw(&newSprite);
		//MLGraphics::Write(writer, text);
		MLGraphics::Write(writer2, text);
		MLGraphics::DrawQuad(&quad);
		quad.Position = writer2->Position + writer2->Origin;
		quad.Position.y += writer2->Font->GetTextHeight(text);
		MLGraphics::DrawQuad(&quad);
		MLGraphics::DrawQuad(&quad2);
		quad2.Position = writer->Position + writer->Origin;
		quad2.Position.y += writer->Font->GetTextHeight(text);
		MLGraphics::DrawQuad(&quad2);
		MLGraphics::DrawPoint(&origin);
		//MLVector4f col = font.GetFontColour();
		//col.a += adder;
		//font.SetFontColour(col);
		//if(col.a <= 0)
		//	adder = 0.04f;
		//if(col.a >= 1)
		//	adder = -0.04f;

		MLGraphics::End();

		GLContext->EndFrame();
	}
}