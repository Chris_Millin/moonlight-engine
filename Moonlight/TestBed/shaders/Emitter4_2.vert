#version 400

in vec3 VertexPosition;

uniform mat4 View;
uniform bool TimeEnabled;

void main()
{
	if(TimeEnabled == false)
	{
		gl_Position = View * vec4(VertexPosition.x, VertexPosition.y, 0.0,1.0); 
	}
	else
	{
		gl_Position = View * vec4(VertexPosition, 1.0); 
	}

	
}