in vec3 VertexPosition;
in vec4 VertexColour;
in vec2 TextureCoord;


out vec3 VPosition;
out vec4 VColour;
out vec2 VTexCoord;

uniform mat4 inMV;
uniform mat4 Projection;

void main()
{

	//gl_PointSize = 4.0;
	VTexCoord = TextureCoord;
	VColour = VertexColour;
	VPosition =  VertexPosition;
	//gl_Position = (Projection * pos);
	gl_Position = (Projection) * vec4( VertexPosition, 1.0f); 
	//gl_Position = vec4( VertexPosition, 1.0f);
}
//inMV * 