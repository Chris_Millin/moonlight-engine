#version 400
in vec3 VertexPosition;
in vec4 VertexColour;
in vec2 TextureCoord;

out vec2 VTexCoord;
out vec4 VColour;


uniform mat4 Projection;
uniform mat4 View;

void main()
{

	gl_PointSize = VertexPosition.z;

	VTexCoord = TextureCoord;
	VColour = VertexColour;

	gl_Position = (Projection * View * vec4(VertexPosition, 1.0f)); 
}
