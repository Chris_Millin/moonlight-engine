
in vec3 VPosition;
in vec4 VColour;
in vec2 VTexCoord;

uniform sampler2D Tex1;
uniform bool EnableWire;

out vec4 FragColour;

void main() {

	FragColour = texture( Tex1, VTexCoord );
	FragColour = FragColour * VColour;	
}