#version 400
in vec2 VTexCoord;
in vec4 VColour;


uniform sampler2D Tex1;

out vec4 FragColour;

void main() {

	FragColour = texture( Tex1, VTexCoord );
	FragColour = FragColour * VColour;
    
	
}