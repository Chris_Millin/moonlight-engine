#version 400
in vec3 VertexPosition;
in vec4 VertexColour;
in vec2 TextureCoord;


out vec4 VColour;
out vec2 VTexCoord;

uniform mat4 inMV;
uniform mat4 Projection;

uniform float pointSize;


void main()
{

	gl_PointSize = VertexPosition.z;
	
	VTexCoord = TextureCoord;
	VColour = VertexColour;

	gl_Position = (Projection) * vec4( VertexPosition, 1.0f); 
}
//* Projection