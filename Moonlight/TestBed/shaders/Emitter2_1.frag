
in float time;

uniform sampler2D Tex1;

uniform mat4 ProjectionMat;
uniform bool TimeEnabled;
uniform float MaxTime;
uniform vec3 tintColour;

out vec4 FragColour;

void main() {

	float Time = 1.0f;
    float normalisedvalue = 0.0f;
    if(TimeEnabled == true)
    {
        Time = time;
        normalisedvalue = Time / MaxTime;
    }
	 FragColour  = texture( Tex1, gl_PointCoord);
	 FragColour = FragColour *  vec4(tintColour.x,tintColour.y,tintColour.z,1.0 - normalisedvalue);
}