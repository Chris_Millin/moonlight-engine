


in vec3 VertexPosition;

out float time;

uniform mat4 ProjectionMat;
uniform bool TimeEnabled;

uniform float SizeX; // Half the width of the quad
uniform float SizeY; // Half the width of the quad

void main()
{
	if(TimeEnabled == false)
	{
		gl_Position = ProjectionMat * vec4(VertexPosition.x, VertexPosition.y, 0.0,1.0);
		gl_PointSize = SizeX * 2;
		time = 0.0;
	}
	else
	{
		gl_PointSize = (SizeX * 2) * VertexPosition.z;
		//gl_PointSize = (SizeY * 2) * VertexPosition.z;
		time = VertexPosition.z;
		gl_Position = ProjectionMat * vec4(VertexPosition, 1.0); 
	}

	
}