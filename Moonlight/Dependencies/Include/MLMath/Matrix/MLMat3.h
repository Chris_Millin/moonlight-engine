#ifndef MLMAT3_H
#define MLMAT3_H
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector4.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector2.h"

using MLMath::MLVec4::MLVector4;
using MLMath::MLVec3::MLVector3;
using MLMath::MLVec2::MLVector2;

#define ML_MAT3_ROWS 3
#define ML_MAT3_COLUMNS 3

namespace MLMath
{
	namespace MLMat3
	{
		//=========================================================
		// Name: Matrix 3
		// Description: A 3x3 dimensional matrix containing most
		// of the basic operator calculations
		// Note: structure
		//
		// =================
		// | m00, m01, m02 |
		// | m10, m11, m12 |
		// | m20, m21, m22 |
		// =================
		//
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLMatrix3
		{
			union
			{
				//=========================================================
				// Name: Matrix 
				// Description: template type 3x3 array to store all the 
				// components of the array
				// Creator: Chris Millin
				//=========================================================
				T m[3][3];

				struct  
				{
					//=========================================================
					// Name: X
					// Description: X Row Component stored as a vector 3
					// use .x.x/y/z to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector3<T> x;

					//=========================================================
					// Name: Y
					// Description: y Row Component stored as a vector 3
					// use .y.x/y/z to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector3<T> y;

					//=========================================================
					// Name: Z
					// Description: Z Row Component stored as a vector 3
					// use .z.x/y/z to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector3<T> z;
				};
			};
			

			//=========================================================
			// Name: Constructor
			// Description: Constructs a default identified matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3();
			
			//=========================================================
			// Name: Constructor
			// Description: Constructs a identity matrix with a defined
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3(T val);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a vector4 for
			// each row
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3(T** val);
		
			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a double pointer.
			// will need to ensure that the pointer is cleaned up
			// correctly
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3(MLVector3<T> m0, MLVector3<T> m1, MLVector3<T> m2);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with each individual
			// component
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3(	T m00, T m01, T m02,
						T m10, T m11, T m12,
						T m20, T m21, T m22);

			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals Operator
			// Description: checks if this matrix is equal to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Equals Operator
			// Description: checks if this matrix is equal to a single
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Addition Assignment Operator
			// Description: Add and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator+=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Addition Assignment Operator Single Value
			// Description: Add and assign a single value to all the
			// values in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtraction Assignment Operator
			// Description: Subtract and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator-=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Subtraction Assignment Operator Single Value
			// Description: Subtracts and assign a single value to all the
			// values in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiplication Assignment Operator
			// Description: Multiply and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator*=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Multiplication Assignment Operator Single Value
			// Description: Multiply and assign one matrix to a single
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Division Assignment Operator Single Value
			// Description: Divide and assign one matrix by a single 
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition Operator 
			// Description: Adds one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator+(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Addition Operator Single Value
			// Description: Adds a single value to each element in the
			// matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtract Operator
			// Description: Subtract a Matrix from another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator-(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Subtraction Operator Single Value
			// Description: Subtracts a single value to each element in the
			// matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply one matrix with another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator*(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Multiplication Operator 
			// Description: Multiply a matrix with a Vector4
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator*(const MLVector4<T> &inVec4);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a matrix with a Vector3
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator*(const MLVector3<T> &inVec3);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a matrix with a Vector2
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator*(const MLVector2<T> &inVec2);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a single 
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division Operator
			// Description: Division with a single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals Operator
			// Description: Check if this matrix is equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLMatrix3<T> &inMat);
			
			//=========================================================
			// Name: Not Equals Operator
			// Description: Check if this matrix is not equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Greater than Operator
			// Description: Check if this matrix is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator> (const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Less than Operator
			// Description: Check if this matrix is less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator< (const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Greater than or Equal Operator
			// Description: Check if this matrix is greater than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLMatrix3<T> &inMat);

			//=========================================================
			// Name: Less than or Equal Operator
			// Description: Check if this matrix is less than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLMatrix3<T> &inMat);

			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: Returns the value in the desired index
			// anything less than 0 will be treated as 0 and greater
			// than 2 as 2
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Determinant
			// Description: Determinant of the matrix
			// Creator: Chris Millin
			//=========================================================
			T Determinant();

			//=========================================================
			// Name: Transpose
			// Description: Transpose so rows are columns and columns
			// become rows
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> Transpose();

			//=========================================================
			// Name: Inverse
			// Description: Convert the matrix to inverse its current
			// structure
			// Creator: Chris Millin
			//=========================================================
			MLMatrix3<T> Inverse();

			//=========================================================
			// Name: Pointer converter
			// Description: Returns the matrix as a pointer to the
			// 3x3 array
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLMatrix3<T> &Mat3A);
		};

		//=========================================================
		// Name: Matrix 3x3 Float
		// Description: Matrix 3x3 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix3<ML_FLOAT>		MLMatrix3f;

		//=========================================================
		// Name: Matrix 3x3 Int
		// Description: Matrix 3x3 using integer data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix3<ML_INT>		MLMatrix3i;

		//=========================================================
		// Name: Matrix 3x3 Double
		// Description: Matrix 3x3 using double decimal data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix3<ML_DOUBLE>	MLMatrix3d;


		//=========================================================		
		//constructors
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>::MLMatrix3()
		{
			m[0][0] = 1.0f;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = 1.0f;
			m[1][2] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = 1.0f;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>::MLMatrix3(T val)
		{
			m[0][0] = val;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = val;
			m[1][2] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>::MLMatrix3(T** val)
		{
			m[0][0] = **val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>::MLMatrix3(MLVector3<T> m0, MLVector3<T> m1, MLVector3<T> m2)
		{
			this->x = m0;
			this->y = m1;
			this->z = m2;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>::MLMatrix3(	T m00, T m01, T m02,
												T m10, T m11, T m12,
												T m20, T m21, T m22)
		{
			m[0][0] = m00;
			m[0][1] = m01;
			m[0][2] = m02;

			m[1][0] = m10;
			m[1][1] = m11;
			m[1][2] = m12;

			m[2][0] = m20;
			m[2][1] = m21;
			m[2][2] = m22;

		}

		//=========================================================
		//Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator =(const MLMatrix3<T> &inMat)
		{
			this->m[0][0] = inMat.m[0][0];
			this->m[1][0] = inMat.m[1][0];
			this->m[2][0] = inMat.m[2][0];

			this->m[0][1] = inMat.m[0][1];
			this->m[1][1] = inMat.m[1][1];
			this->m[2][1] = inMat.m[2][1];

			this->m[0][2] = inMat.m[0][2];
			this->m[1][2] = inMat.m[1][2];
			this->m[2][2] = inMat.m[2][2];

			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator =(const T &inVal)
		{
			this->m[0][0] = inVal;
			this->m[1][0] = 0.0f;
			this->m[2][0] = 0.0f;

			this->m[0][1] = 0.0f;
			this->m[1][1] = inVal;
			this->m[2][1] = 0.0f;

			this->m[0][2] = 0.0f;
			this->m[1][2] = 0.0f;
			this->m[2][2] = inVal;

			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator+=(const MLMatrix3<T> &inMat)
		{
			this->m[0][0] += inMat.m[0][0];
			this->m[1][0] += inMat.m[1][0];
			this->m[2][0] += inMat.m[2][0];

			this->m[0][1] += inMat.m[0][1];
			this->m[1][1] += inMat.m[1][1];
			this->m[2][1] += inMat.m[2][1];

			this->m[0][2] += inMat.m[0][2];
			this->m[1][2] += inMat.m[1][2];
			this->m[2][2] += inMat.m[2][2];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator+=(const T &inVal)
		{
			this->m[0][0] += inVal;
			this->m[1][0] += inVal;
			this->m[2][0] += inVal;

			this->m[0][1] += inVal;
			this->m[1][1] += inVal;
			this->m[2][1] += inVal;

			this->m[0][2] += inVal;
			this->m[1][2] += inVal;
			this->m[2][2] += inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator-=(const MLMatrix3<T> &inMat)
		{
			this->m[0][0] -= inMat.m[0][0];
			this->m[1][0] -= inMat.m[1][0];
			this->m[2][0] -= inMat.m[2][0];

			this->m[0][1] -= inMat.m[0][1];
			this->m[1][1] -= inMat.m[1][1];
			this->m[2][1] -= inMat.m[2][1];

			this->m[0][2] -= inMat.m[0][2];
			this->m[1][2] -= inMat.m[1][2];
			this->m[2][2] -= inMat.m[2][2];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator-=(const T &inVal)
		{
			this->m[0][0] -= inVal;
			this->m[1][0] -= inVal;
			this->m[2][0] -= inVal;

			this->m[0][1] -= inVal;
			this->m[1][1] -= inVal;
			this->m[2][1] -= inVal;

			this->m[0][2] -= inVal;
			this->m[1][2] -= inVal;
			this->m[2][2] -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator*=(const MLMatrix3<T> &inMat)
		{
			this->m[0][0] = (this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]) + (this->m[0][2] * inMat.m[2][0]);
			this->m[1][0] = (this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]) + (this->m[0][2] * inMat.m[2][1]);
			this->m[2][0] = (this->m[0][0] * inMat.m[0][2]) + (this->m[0][1] * inMat.m[1][2]) + (this->m[0][2] * inMat.m[2][2]);

			this->m[0][1] = (this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]) + (this->m[1][2] * inMat.m[2][0]);
			this->m[1][1] = (this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]) + (this->m[1][2] * inMat.m[2][1]);
			this->m[2][1] = (this->m[1][0] * inMat.m[0][2]) + (this->m[1][1] * inMat.m[1][2]) + (this->m[1][2] * inMat.m[2][2]);

			this->m[0][2] = (this->m[2][0] * inMat.m[0][0]) + (this->m[2][1] * inMat.m[1][0]) + (this->m[2][2] * inMat.m[2][0]);
			this->m[1][2] = (this->m[2][0] * inMat.m[0][1]) + (this->m[2][1] * inMat.m[1][1]) + (this->m[2][2] * inMat.m[2][1]);
			this->m[2][2] = (this->m[2][0] * inMat.m[0][2]) + (this->m[2][1] * inMat.m[1][2]) + (this->m[2][2] * inMat.m[2][2]);
			
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator*=(const T &inVal)
		{
			this->m[0][0] *= inVal;
			this->m[1][0] *= inVal;
			this->m[2][0] *= inVal;

			this->m[0][1] *= inVal;
			this->m[1][1] *= inVal;
			this->m[2][1] *= inVal;

			this->m[0][2] *= inVal;
			this->m[1][2] *= inVal;
			this->m[2][2] *= inVal;

			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T>& MLMatrix3<T>::operator/=(const T &inVal)
		{
			this->m[0][0] /= inVal;
			this->m[1][0] /= inVal;
			this->m[2][0] /= inVal;

			this->m[0][1] /= inVal;
			this->m[1][1] /= inVal;
			this->m[2][1] /= inVal;

			this->m[0][2] /= inVal;
			this->m[1][2] /= inVal;
			this->m[2][2] /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator+(const MLMatrix3<T> &inMat)
		{
			return MLMatrix3<T>(this->m[0][0] + inMat.m[0][0],
								this->m[1][0] + inMat.m[1][0],
								this->m[2][0] + inMat.m[2][0],

								this->m[0][1] + inMat.m[0][1],
								this->m[1][1] + inMat.m[1][1],
								this->m[2][1] + inMat.m[2][1],

								this->m[0][2] + inMat.m[0][2],
								this->m[1][2] + inMat.m[1][2],
								this->m[2][2] + inMat.m[2][2]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator+(const T &inVal)
		{
			return MLMatrix3<T>(this->m[0][0] + inVal,
								this->m[1][0] + inVal,
								this->m[2][0] + inVal,

								this->m[0][1] + inVal,
								this->m[1][1] + inVal,
								this->m[2][1] + inVal,

								this->m[0][2] + inVal,
								this->m[1][2] + inVal,
								this->m[2][2] + inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator-(const MLMatrix3<T> &inMat)
		{
			return MLMatrix3<T>(this->m[0][0] - inMat.m[0][0],
								this->m[1][0] - inMat.m[1][0],
								this->m[2][0] - inMat.m[2][0],

								this->m[0][1] - inMat.m[0][1],
								this->m[1][1] - inMat.m[1][1],
								this->m[2][1] - inMat.m[2][1],

								this->m[0][2] - inMat.m[0][2],
								this->m[1][2] - inMat.m[1][2],
								this->m[2][2] - inMat.m[2][2]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator-(const T &inVal)
		{
			return MLMatrix3<T>(this->m[0][0] - inVal,
								this->m[1][0] - inVal,
								this->m[2][0] - inVal,

								this->m[0][1] - inVal,
								this->m[1][1] - inVal,
								this->m[2][1] - inVal,

								this->m[0][2] - inVal,
								this->m[1][2] - inVal,
								this->m[2][2] - inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator*(const MLMatrix3<T> &inMat)
		{
			return MLMatrix3<T>((this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]) + (this->m[0][2] * inMat.m[2][0]),
								(this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]) + (this->m[0][2] * inMat.m[2][1]),
								(this->m[0][0] * inMat.m[0][2]) + (this->m[0][1] * inMat.m[1][2]) + (this->m[0][2] * inMat.m[2][2]),

								(this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]) + (this->m[1][2] * inMat.m[2][0]),
								(this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]) + (this->m[1][2] * inMat.m[2][1]),
								(this->m[1][0] * inMat.m[0][2]) + (this->m[1][1] * inMat.m[1][2]) + (this->m[1][2] * inMat.m[2][2]),

								(this->m[2][0] * inMat.m[0][0]) + (this->m[2][1] * inMat.m[1][0]) + (this->m[2][2] * inMat.m[2][0]),
								(this->m[2][0] * inMat.m[0][1]) + (this->m[2][1] * inMat.m[1][1]) + (this->m[2][2] * inMat.m[2][1]),
								(this->m[2][0] * inMat.m[0][2]) + (this->m[2][1] * inMat.m[1][2]) + (this->m[2][2] * inMat.m[2][2]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLMatrix3<T>::operator*(const MLVector4<T> &inVec4)
		{
			return MLVector4<T>((this->m[0][0] * inVec4.x) + (this->m[0][1] * inVec4.y) + (this->m[0][2] * inVec4.z),
								(this->m[1][0] * inVec4.x) + (this->m[1][1] * inVec4.y) + (this->m[1][2] * inVec4.z),
								(this->m[2][0] * inVec4.x) + (this->m[2][1] * inVec4.y) + (this->m[2][2] * inVec4.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLMatrix3<T>::operator*(const MLVector3<T> &inVec3)
		{
			return MLVector3<T>((this->m[0][0] * inVec3.x) + (this->m[0][1] * inVec3.y) + (this->m[0][2] * inVec3.z),
								(this->m[1][0] * inVec3.x) + (this->m[1][1] * inVec3.y) + (this->m[1][2] * inVec3.z),
								(this->m[2][0] * inVec3.x) + (this->m[2][1] * inVec3.y) + (this->m[2][2] * inVec3.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLMatrix3<T>::operator*(const MLVector2<T> &inVec2)
		{
			MLVector3<T> result = this * MLVector3<T>(invec2.x, inVec2.y, 1.0f);
			return MLVector2<T>(result.x, result.y);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator*(const T &inVal)
		{
			return MLMatrix3<T>(this->m[0][0] * inVal,
								this->m[1][0] * inVal,
								this->m[2][0] * inVal,

				this->m[0][1] * inVal,
				this->m[1][1] * inVal,
				this->m[2][1] * inVal,

				this->m[0][2] * inVal,
				this->m[1][2] * inVal,
				this->m[2][2] * inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::operator/(const T &inVal)
		{
			return MLMatrix3<T>(this->m[0][0] / inVal,
								this->m[1][0] / inVal,
								this->m[2][0] / inVal,

								this->m[0][1] / inVal,
								this->m[1][1] / inVal,
								this->m[2][1] / inVal,

								this->m[0][2] / inVal,
								this->m[1][2] / inVal,
								this->m[2][2] / inVal);
		}

		//=========================================================
		// Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator==(const MLMatrix3<T> &inMat)
		{
			return	this->m[0][0] == inMat.m[0][0] &&
					this->m[1][0] == inMat.m[1][0] &&
					this->m[2][0] == inMat.m[2][0] &&

					this->m[0][1] == inMat.m[0][1] && 
					this->m[1][1] == inMat.m[1][1] &&
					this->m[2][1] == inMat.m[2][1] &&

					this->m[0][2] == inMat.m[0][2] &&
					this->m[1][2] == inMat.m[1][2] &&
					this->m[2][2] == inMat.m[2][2] &&
					this->m[3][2] == inMat.m[3][2];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator!=(const MLMatrix3<T> &inMat)
		{
			return !(this == inMat);
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator<(const MLMatrix3<T> &inMat)
		{
			return	this->m[0][0] < inMat.m[0][0] &&
					this->m[1][0] < inMat.m[1][0] &&
					this->m[2][0] < inMat.m[2][0] &&

					this->m[0][1] < inMat.m[0][1] && 
					this->m[1][1] < inMat.m[1][1] &&
					this->m[2][1] < inMat.m[2][1] &&

					this->m[0][2] < inMat.m[0][2] &&
					this->m[1][2] < inMat.m[1][2] &&
					this->m[2][2] < inMat.m[2][2] &&
					this->m[3][2] < inMat.m[3][2];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator>(const MLMatrix3<T> &inMat)
		{
			return	this->m[0][0] > inMat.m[0][0] &&
					this->m[1][0] > inMat.m[1][0] &&
					this->m[2][0] > inMat.m[2][0] &&

					this->m[0][1] > inMat.m[0][1] && 
					this->m[1][1] > inMat.m[1][1] &&
					this->m[2][1] > inMat.m[2][1] &&

					this->m[0][2] > inMat.m[0][2] &&
					this->m[1][2] > inMat.m[1][2] &&
					this->m[2][2] > inMat.m[2][2] &&
					this->m[3][2] > inMat.m[3][2];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator<=(const MLMatrix3<T> &inMat)
		{
			return	this->m[0][0] <= inMat.m[0][0] &&
					this->m[1][0] <= inMat.m[1][0] &&
					this->m[2][0] <= inMat.m[2][0] &&

					this->m[0][1] <= inMat.m[0][1] && 
					this->m[1][1] <= inMat.m[1][1] &&
					this->m[2][1] <= inMat.m[2][1] &&

					this->m[0][2] <= inMat.m[0][2] &&
					this->m[1][2] <= inMat.m[1][2] &&
					this->m[2][2] <= inMat.m[2][2] &&
					this->m[3][2] <= inMat.m[3][2];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix3<T>::operator>=(const MLMatrix3<T> &inMat)
		{
			return	this->m[0][0] >= inMat.m[0][0] &&
					this->m[1][0] >= inMat.m[1][0] &&
					this->m[2][0] >= inMat.m[2][0] &&

					this->m[0][1] >= inMat.m[0][1] && 
					this->m[1][1] >= inMat.m[1][1] &&
					this->m[2][1] >= inMat.m[2][1] &&

					this->m[0][2] >= inMat.m[0][2] &&
					this->m[1][2] >= inMat.m[1][2] &&
					this->m[2][2] >= inMat.m[2][2] &&
					this->m[3][2] >= inMat.m[3][2];
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLMatrix3<T>::operator[](const ML_INT index)
		{
			ML_UINT i = index;
			if(i < 0) i = 0;
			if(i > 2) i = 2;
			if(i == 0) return this->x;
			if(i == 1) return this->y;
			if(i == 2) return this->z;
		}

		//=========================================================
		// Matrix 3 Math Functions
		//=========================================================
		
		template<typename T>
		T MLMatrix3<T>::Determinant()
		{
			return	(this->m[0][0] * (this->m[1][1] * this->m[2][2]) - (this->m[1][2] * this->m[2][1])) - 
					(this->m[0][1] * (this->m[1][0] * this->m[2][2]) - (this->m[1][2] * this->m[2][0])) +
					(this->m[0][2] * (this->m[1][0] * this->m[2][1]) - (this->m[1][1] * this->m[2][0]));
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::Transpose()
		{
			MLMatrix3<T> Result = this;

			Result.m[0][1] = this->m[1][0];	
			Result.m[0][2] = this->m[2][0];

			Result.m[1][0] = this->m[0][1];	
			Result.m[1][2] = this->m[2][1];

			Result.m[2][0] = this->m[0][2];	
			Result.m[2][1] = this->m[1][2]; 
			
			return Result;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix3<T> MLMatrix3<T>::Inverse()
		{
			MLMatrix3<T> OriginalMat = this;
			MLMatrix3<T> Result;

			for(ML_UINT i = 0; i < ML_MAT3_ROWS; i++)
			{
				ML_FLOAT divVal = OriginalMat[i][i];
				//Divide at row by the diagonal values
				Result[i]		/= divVal;
				OriginalMat[i]	/= divVal;

				for(ML_UINT i = 0; i < ML_MAT3_ROWS; i++)
				{
					if(j != i)
					{
						ML_FLOAT multVal1 = OriginalMat[j][i];
						ML_FLOAT multVal2 = OriginalMat[j][i];
						OriginalMat[j] = OriginalMat[j] - (OriginalMat[i] * multVal1);
						Result[j] = Result[j] - (Result[i] * multVal2);
					}
				}
			}

			return Result;
		}

		template<typename T>
		ML_MATH_INLINE T* toptr(const MLMatrix3<T> &Mat3A)
		{
			return (T*)Mat3A.m;
		}


	}
}

#endif