#ifndef MLMAT2_H
#define MLMAT2_H
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector4.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector2.h"

using MLMath::MLVec4::MLVector4;
using MLMath::MLVec3::MLVector3;
using MLMath::MLVec2::MLVector2;

namespace MLMath
{
	namespace MLMat2
	{
		//=========================================================
		// Name: Matrix 2
		// Description: A 2x2 dimensional matrix containing most
		// of the basic operator calculations
		// Note: structure
		//
		// ============
		// | m00, m01 |
		// | m10, m11 |
		// ============
		//
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLMatrix2
		{
			union
			{
				//=========================================================
				// Name: Matrix 
				// Description: template type 2x2 array to store all the 
				// components of the array
				// Creator: Chris Millin
				//=========================================================
				T m[2][2];
				struct  
				{
					//=========================================================
					// Name: X
					// Description: X Row Component stored as a vector 2
					// use .x.x/y to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector2<T> x;

					//=========================================================
					// Name: Y
					// Description: Y Row Component stored as a vector 2
					// use .y.x/y to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector2<T> y;
				};
			};
			

			//=========================================================
			// Name: Constructor
			// Description: Constructs a default identified matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2();

			//=========================================================
			// Name: Constructor
			// Description: Constructs a identity matrix with a defined
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2(T val);


			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a double pointer.
			// will need to ensure that the pointer is cleaned up
			// correctly since this structure will only use it as a 
			// reference
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2(T** val);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a vector2 for
			// each row
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2(MLVector2<T> m0, MLVector2<T> m1);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with each individual
			// component
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2(T m00, T m01, T m10, T m11);

			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals Operator
			// Description: will make this matrix equal the same
			// as the right hand side
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Equals Operator  Single Value
			// Description: Set all the values in the matrix to the same
			// value as the right hand side if a single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Addition Assignment Operator
			// Description: Add and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator+=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Addition Assignment Operator  Single Value
			// Description: Add and assign a single value to all
			// the values in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtraction Assignment Operator
			// Description: Subtract and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator-=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Subtraction Assignment Operator  Single Value
			// Description: Subtracts and assign a single value to all
			// the values in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiplication Assignment Operator
			// Description: Multiply and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator*=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Multiplication Assignment Operator  Single Value 
			// Description: Multiply and assign one matrix to a single
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Division Assignment Operator Single Value
			// Description: Divide and assign one matrix by a single 
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition Operator
			// Description: Multiply and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator+(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Addition Operator Single Value
			// Description: Multiply and assign one matrix to a
			// single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtract Operator
			// Description: Subtract a Matrix from another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator-(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Subtraction Operator Single Value
			// Description: Subtracts and assign one matrix to a
			// single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply one matrix with another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator*(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Multiplication Operator 
			// Description: Multiply a matrix with a Vector4
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator*(const MLVector4<T> &inVec4);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a matrix with a Vector3
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator*(const MLVector3<T> &inVec3);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a matrix with a Vector3
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator*(const MLVector2<T> &inVec2);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a single 
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division Operator
			// Description: Division with a single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals Operator
			// Description: Check if this matrix is equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Not Equals Operator
			// Description: Check if this matrix is not equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Greater than Operator
			// Description: Check if this matrix is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator> (const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Less than Operator
			// Description: Check if this matrix is less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator< (const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Greater than or Equal Operator
			// Description: Check if this matrix is greater than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLMatrix2<T> &inMat);

			//=========================================================
			// Name: Less than or Equal Operator
			// Description: Check if this matrix is less than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLMatrix2<T> &inMat);

			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: Returns the value in the desired index
			// anything less than 0 will be treated as 0 and greater
			// than 1 as 1
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Determinant
			// Description: Determinant of the matrix
			// Creator: Chris Millin
			//=========================================================
			T Determinant();

			//=========================================================
			// Name: Transpose
			// Description: Transpose so rows are columns and columns
			// become rows
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> Transpose();

			//=========================================================
			// Name: Inverse
			// Description: Returns the inverse of the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix2<T> Inverse();

			//=========================================================
			// Name: Pointer converter
			// Description: Returns the matrix as a pointer to the
			// 2x2 array
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLMatrix2<T> &Mat2A);
		};

		//=========================================================
		// Name: Matrix 2x2 Float
		// Description: Matrix 2x2 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix2<ML_FLOAT>		MLMatrix2f;

		//=========================================================
		// Name: Matrix 2x2 Int
		// Description: Matrix 2x2 using integer data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix2<ML_INT>		MLMatrix2i;

		//=========================================================
		// Name: Matrix 2x2 Double
		// Description: Matrix 2x2 using double decimal data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix2<ML_DOUBLE>	MLMatrix2d;


		//=========================================================		
		//constructors
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>::MLMatrix2()
		{
			m[0][0] = 1.0f;
			m[0][1] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = 1.0f;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>::MLMatrix2(T val)
		{
			m[0][0] = val;
			m[0][1] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>::MLMatrix2(	MLVector2<T> m0,
												MLVector2<T> m1 )
		{
			this->x = m0;
			this->y = m1;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>::MLMatrix2(T** val)
		{
			m[0][0] = **val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>::MLMatrix2(	T m00, T m01,
												T m10, T m11 )
		{
			m[0][0] = m00;
			m[0][1] = m01;

			m[1][0] = m10;
			m[1][1] = m11;
		}

		//=========================================================
		// Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator =(const MLMatrix2<T> &inMat)
		{
			this->m[0][0] = inMat.m[0][0];
			this->m[1][0] = inMat.m[1][0];

			this->m[0][1] = inMat.m[0][1];
			this->m[1][1] = inMat.m[1][1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator =(const T &inVal)
		{
			this->m[0][0] = inVal;
			this->m[1][0] = inVal;

			this->m[0][1] = inVal;
			this->m[1][1] = inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator+=(const MLMatrix2<T> &inMat)
		{
			this->m[0][0] += inMat.m[0][0];
			this->m[1][0] += inMat.m[1][0];

			this->m[0][1] += inMat.m[0][1];
			this->m[1][1] += inMat.m[1][1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator+=(const T &inVal)
		{
			this->m[0][0] += inVal;
			this->m[1][0] += inVal;

			this->m[0][1] += inVal;
			this->m[1][1] += inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator-=(const MLMatrix2<T> &inMat)
		{
			this->m[0][0] -= inMat.m[0][0];
			this->m[1][0] -= inMat.m[1][0];

			this->m[0][1] -= inMat.m[0][1];
			this->m[1][1] -= inMat.m[1][1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator-=(const T &inVal)
		{
			this->m[0][0] -= inVal;
			this->m[1][0] -= inVal;

			this->m[0][1] -= inVal;
			this->m[1][1] -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator*=(const MLMatrix2<T> &inMat)
		{
			this->m[0][0] = (this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]);
			this->m[1][0] = (this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]);

			this->m[0][1] = (this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]);
			this->m[1][1] = (this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]);
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator*=(const T &inVal)
		{
			this->m[0][0] *= inVal;
			this->m[1][0] *= inVal;

			this->m[0][1] *= inVal;
			this->m[1][1] *= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T>& MLMatrix2<T>::operator/=(const T &inVal)
		{
			this->m[0][0] /= inVal;
			this->m[1][0] /= inVal;

			this->m[0][1] /= inVal;
			this->m[1][1] /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator+(const MLMatrix2<T> &inMat)
		{
			return MLMatrix2<T>(this->m[0][0] + inMat.m[0][0],
								this->m[1][0] + inMat.m[1][0],

								this->m[0][1] + inMat.m[0][1],
								this->m[1][1] + inMat.m[1][1]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator+(const T &inVal)
		{
			return MLMatrix2<T>(this->m[0][0] + inVal,
								this->m[1][0] + inVal,

								this->m[0][1] + inVal,
								this->m[1][1] + inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator-(const MLMatrix2<T> &inMat)
		{
			return MLMatrix2<T>(this->m[0][0] - inMat.m[0][0],
								this->m[1][0] - inMat.m[1][0],

								this->m[0][1] - inMat.m[0][1],
								this->m[1][1] - inMat.m[1][1]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator-(const T &inVal)
		{
			return MLMatrix2<T>(this->m[0][0] - inVal,
								this->m[1][0] - inVal,

								this->m[0][1] - inVal,
								this->m[1][1] - inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator*(const MLMatrix2<T> &inMat)
		{
			return MLMatrix2<T>((this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]),
								(this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]),

								(this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]),
								(this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLMatrix2<T>::operator*(const MLVector4<T> &inVec4)
		{
			return MLVector4<T>((this->m[0][0] * inVec4.x) + (this->m[0][1] * inVec4.y),
								(this->m[1][0] * inVec4.x) + (this->m[1][1] * inVec4.y),
								inVec4.Width, inVec4.Height);
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLMatrix2<T>::operator*(const MLVector3<T> &inVec3)
		{
			return MLVector3<T>((this->m[0][0] * inVec3.x) + (this->m[0][1] * inVec3.y),
								(this->m[1][0] * inVec3.x) + (this->m[1][1] * inVec3.y),
								inVec3.z);
		}


		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLMatrix2<T>::operator*(const MLVector2<T> &inVec2)
		{
			return MLVector2<T>((this->m[0][0] * inVec2.x) + (this->m[0][1] * inVec2.y),
								(this->m[1][0] * inVec2.x) + (this->m[1][1] * inVec2.y));
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator*(const T &inVal)
		{
			return MLMatrix2<T>(this->m[0][0] * inVal,
								this->m[1][0] * inVal,

								this->m[0][1] * inVal,
								this->m[1][1] * inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::operator/(const T &inVal)
		{
			return MLMatrix2<T>(this->m[0][0] / inVal,
								this->m[1][0] / inVal,

								this->m[0][1] / inVal,
								this->m[1][1] / inVal)
		}

		//=========================================================
		// Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator==(const MLMatrix2<T> &inMat)
		{
			return	this->m[0][0] == inMat.m[0][0] && this->m[1][0] == inMat.m[1][0] &&
					this->m[0][1] == inMat.m[0][1] && this->m[1][1] == inMat.m[1][1] &&
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator!=(const MLMatrix2<T> &inMat)
		{
			return !(this == inMat)
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator> (const MLMatrix2<T> &inMat)
		{
			return	this->m[0][0] > inMat.m[0][0] && this->m[1][0] > inMat.m[1][0] &&
					this->m[0][1] > inMat.m[0][1] && this->m[1][1] > inMat.m[1][1] && 
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator< (const MLMatrix2<T> &inMat)
		{
			return	this->m[0][0] < inMat.m[0][0] && this->m[1][0] < inMat.m[1][0] &&
					this->m[0][1] < inMat.m[0][1] && this->m[1][1] < inMat.m[1][1] && 
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator>=(const MLMatrix2<T> &inMat)
		{
			return	this->m[0][0] >= inMat.m[0][0] && this->m[1][0] >= inMat.m[1][0] &&
					this->m[0][1] >= inMat.m[0][1] && this->m[1][1] >= inMat.m[1][1] && 
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix2<T>::operator<=(const MLMatrix2<T> &inMat)
		{
			return	this->m[0][0] <= inMat.m[0][0] && this->m[1][0] <= inMat.m[1][0] &&
					this->m[0][1] <= inMat.m[0][1] && this->m[1][1] <= inMat.m[1][1] && 
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLMatrix2<T>::operator[](const ML_INT index)
		{
			if(index < 0) index = 0;
			if(index > 1) index = 1;
			if(index == 0) return this->x;
			if(index == 1) return this->y;
		}

		//=========================================================
		// Matrix 2 Math Functions
		//=========================================================

		template<typename T>
		T MLMatrix2<T>::Determinant()
		{
			return (this->m[0][0] * this->m[1][1]) - (this->m[0][1] * this->m[1][0]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::Transpose()
		{
			MLMatrix2<T> Result = this

			T m10 = Result.m[1][0];

			Result.m[1][0] = Result.m[0][1];
			Result.m[0][1] = m10;

			return Result
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix2<T> MLMatrix2<T>::Inverse()
		{
			MLMatrix2<T> Result;

			//store the inverse of the determinant
			ML_FLOAT iDeterminant = 1.0f/this->Determinant();

			Result[0][0] = iDeterminant *	this->m[1][1];
			Result[0][1] = iDeterminant * -(this->m[0][1]);

			Result[1][0] = iDeterminant * -(this->m[1][0]);
			Result[1][1] = iDeterminant *	this->m[0][0];

			return Result;
		}

		template<typename T>
		ML_MATH_INLINE T* toptr(const MLMatrix2<T> &Mat2A)
		{
			return (T*)Mat2A.m;
		}


	}
}

#endif