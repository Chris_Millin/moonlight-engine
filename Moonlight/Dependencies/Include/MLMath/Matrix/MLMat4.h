#ifndef MLMAT4_H
#define MLMAT4_H
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector4.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector2.h"

using MLMath::MLVec4::MLVector4;
using MLMath::MLVec3::MLVector3;
using MLMath::MLVec2::MLVector2;

#define ML_MAT4_ROWS 4
#define ML_MAT4_COLUMNS 4

namespace MLMath
{
	namespace MLMat4
	{
		//=========================================================
		// Name: Mat 4
		// Description: A 4x4 dimensional matrix containing most
		// of the basic operator calculations
		// Note: structure
		//
		// ======================
		// | m00, m01, m02, m03 |
		// | m10, m11, m12, m13 |
		// | m20, m21, m22, m23 |
		// | m30, m31, m32, m33 |
		// ======================
		//
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLMatrix4
		{
			union
			{
				//=========================================================
				// Name: Matrix 
				// Description: template type 4x4 array to store all the 
				// components of the array
				// Creator: Chris Millin
				//=========================================================
				T m[4][4];

				struct  
				{
					//=========================================================
					// Name: X
					// Description: X Row Component stored as a vector 4
					// use .x.x/y/z/w to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector4<T> x;

					//=========================================================
					// Name: Y
					// Description: Y Row Component stored as a vector 4
					// use .y.x/y/z/w to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector4<T> y;

					//=========================================================
					// Name: Z
					// Description: Z Row Component stored as a vector 4
					// use .z.x/y/z/w to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector4<T> z;

					//=========================================================
					// Name: W
					// Description: W Row Component stored as a vector 4
					// use .w.x/y/z/w to get the different columns of the row
					// Creator: Chris Millin
					//=========================================================
					MLVector4<T> w;
				};
			};
			
			//=========================================================
			// Name: Constructor
			// Description: Constructs a default identified matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4();

			//=========================================================
			// Name: Constructor
			// Description: Constructs a identity matrix with a defined
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4(T val);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a double pointer.
			// will need to ensure that the pointer is cleaned up
			// correctly
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4(T** val);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with a vector4 for
			// each row
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4(MLVector4<T> m0, MLVector4<T> m1, MLVector4<T> m2, MLVector4<T> m3);

			//=========================================================
			// Name: Constructor
			// Description: Constructs the matrix with each individual
			// component
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4(	T m00, T m01, T m02, T m03,
						T m10, T m11, T m12, T m13,
						T m20, T m21, T m22, T m23,
						T m30, T m31, T m32, T m33);

			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals Operator
			// Description: checks if this matrix is equal to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Equals Operator
			// Description: checks if this matrix is equal to a single
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Addition Assignment Operator
			// Description: Add and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator+=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Addition Assignment Operator Single Value
			// Description: Add and assign a value to all the elements
			// in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtraction Assignment Operator
			// Description: Subtract and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator-=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Subtraction Assignment Operator Single Value
			// Description: Subtract and assign a value to all the
			// elements in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiplication Assignment Operator
			// Description: Multiply and assign one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator*=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Multiplication Assignment Operator Single Value
			// Description: Multiply and assign one matrix to a single
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Division Assignment Operator Single Value
			// Description: Divide and assign one matrix by a single 
			// value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition Operator
			// Description: Adds one matrix to another
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator+(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Addition Operator Single Value
			// Description: Adds a single value to all the elements in
			// the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtract Operator
			// Description: Subtract a Matrix from another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator-(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Subtract Operator
			// Description: Subtract a single value to all the elements
			// in the matrix
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply one matrix with another				
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator*(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a single 
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator*(const MLVector4<T> &inVec4);

			//=========================================================
			// Name: Multiplication Operator 
			// Description: Multiply a matrix with a Vector4
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator*(const MLVector3<T> &inVec3);
			
			//=========================================================
			// Name: Multiplication Operator 
			// Description: Multiply a matrix with a Vector2
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator*(const MLVector2<T> &inVec2);

			//=========================================================
			// Name: Multiplication Operator
			// Description: Multiply a matrix with a Vector3
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division Operator
			// Description: Division with a single value
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals operator
			// Description: checks to see if the two matrices are the
			// same
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Not Equals operator
			// Description: checks to see if the two matrices are not
			// the same
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Greater than Operator
			// Description: Check if this matrix is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator> (const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Less than Operator
			// Description: Check if this matrix is less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator< (const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Greater than or Equal Operator
			// Description: Check if this matrix is greater than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLMatrix4<T> &inMat);

			//=========================================================
			// Name: Less than or Equal Operator
			// Description: Check if this matrix is less than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLMatrix4<T> &inMat);

			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: Returns the value in the desired index
			// anything less than 0 will be treated as 0 and greater
			// than 3 as 3
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Determinant
			// Description: Determinant of the matrix
			// Creator: Chris Millin
			//=========================================================
			T Determinant();

			//=========================================================
			// Name: Transpose
			// Description: Transpose so rows are columns and columns
			// become rows
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> Transpose();

			//=========================================================
			// Name: Inverse
			// Description: Convert the matrix to inverse its current
			// structure
			// Creator: Chris Millin
			//=========================================================
			MLMatrix4<T> Inverse();

			//=========================================================
			// Name: Pointer converter
			// Description: Returns the matrix as a pointer to the
			// 4x4 array
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLMatrix4<T> &Mat4A);
		};

		//=========================================================
		// Name: Matrix 4x4 Float
		// Description: Matrix 4x4 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix4<ML_FLOAT>		MLMatrix4f;

		//=========================================================
		// Name: Matrix 4x4 Int
		// Description: Matrix 4x4 using integer data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix4<ML_INT>		MLMatrix4i;

		//=========================================================
		// Name: Matrix 4x4 Double
		// Description: Matrix 4x4 using double decimal data
		// Creator: Chris Millin
		//=========================================================
		typedef MLMatrix4<ML_DOUBLE>	MLMatrix4d;


		//=========================================================		
		//constructors
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>::MLMatrix4()
		{
			m[0][0] = 1.0f;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = 1.0f;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = 1.0f;
			m[2][3] = 0.0f;

			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = 0.0f;
			m[3][3] = 1.0f;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>::MLMatrix4(T val)
		{
			m[0][0] = val;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = val;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = val;
			m[2][3] = 0.0f;

			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = 0.0f;
			m[3][3] = val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>::MLMatrix4(	MLVector4<T> m0, MLVector4<T> m1,
												MLVector4<T> m2, MLVector4<T> m3 )
		{
			this->x = m0;
			this->y = m1;
			this->z = m2;
			this->w = m3;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>::MLMatrix4(T** val)
		{
			m[0][0] = **val;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>::MLMatrix4(	T m00, T m01, T m02, T m03,
												T m10, T m11, T m12, T m13,
												T m20, T m21, T m22, T m23,
												T m30, T m31, T m32, T m33 )
		{
			m[0][0] = m00;
			m[0][1] = m01;
			m[0][2] = m02;
			m[0][3] = m03;

			m[1][0] = m10;
			m[1][1] = m11;
			m[1][2] = m12;
			m[1][3] = m13;

			m[2][0] = m20;
			m[2][1] = m21;
			m[2][2] = m22;
			m[2][3] = m23;

			m[3][0] = m30;
			m[3][1] = m31;
			m[3][2] = m32;
			m[3][3] = m33;
		}


		//=========================================================
		//Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator =(const MLMatrix4<T> &inMat)
		{
			this->m[0][0] = inMat.m[0][0];
			this->m[1][0] = inMat.m[1][0];
			this->m[2][0] = inMat.m[2][0];
			this->m[3][0] = inMat.m[3][0];

			this->m[0][1] = inMat.m[0][1];
			this->m[1][1] = inMat.m[1][1];
			this->m[2][1] = inMat.m[2][1];
			this->m[3][1] = inMat.m[3][1];

			this->m[0][2] = inMat.m[0][2];
			this->m[1][2] = inMat.m[1][2];
			this->m[2][2] = inMat.m[2][2];
			this->m[3][2] = inMat.m[3][2];

			this->m[0][3] = inMat.m[0][3];
			this->m[1][3] = inMat.m[1][3];
			this->m[2][3] = inMat.m[2][3];
			this->m[3][3] = inMat.m[3][3];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator =(const T &inVal)
		{
			this->m[0][0] = inVal;
			this->m[1][0] = 0.0f;
			this->m[2][0] = 0.0f;
			this->m[3][0] = 0.0f;

			this->m[0][1] = 0.0f;
			this->m[1][1] = inVal;
			this->m[2][1] = 0.0f;
			this->m[3][1] = 0.0f;

			this->m[0][2] = 0.0f;
			this->m[1][2] = 0.0f;
			this->m[2][2] = inVal;
			this->m[3][2] = 0.0f;

			this->m[0][3] = 0.0f;
			this->m[1][3] = 0.0f;
			this->m[2][3] = 0.0f;
			this->m[3][3] = inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator+=(const MLMatrix4<T> &inMat)
		{
			this->m[0][0] += inMat.m[0][0];
			this->m[1][0] += inMat.m[1][0];
			this->m[2][0] += inMat.m[2][0];
			this->m[3][0] += inMat.m[3][0];

			this->m[0][1] += inMat.m[0][1];
			this->m[1][1] += inMat.m[1][1];
			this->m[2][1] += inMat.m[2][1];
			this->m[3][1] += inMat.m[3][1];

			this->m[0][2] += inMat.m[0][2];
			this->m[1][2] += inMat.m[1][2];
			this->m[2][2] += inMat.m[2][2];
			this->m[3][2] += inMat.m[3][2];

			this->m[0][3] += inMat.m[0][3];
			this->m[1][3] += inMat.m[1][3];
			this->m[2][3] += inMat.m[2][3];
			this->m[3][3] += inMat.m[3][3];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator+=(const T &inVal)
		{
			this->m[0][0] += inVal;
			this->m[1][0] += inVal;
			this->m[2][0] += inVal;
			this->m[3][0] += inVal;

			this->m[0][1] += inVal;
			this->m[1][1] += inVal;
			this->m[2][1] += inVal;
			this->m[3][1] += inVal;

			this->m[0][2] += inVal;
			this->m[1][2] += inVal;
			this->m[2][2] += inVal;
			this->m[3][2] += inVal;

			this->m[0][3] += inVal;
			this->m[1][3] += inVal;
			this->m[2][3] += inVal;
			this->m[3][3] += inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator-=(const MLMatrix4<T> &inMat)
		{
			this->m[0][0] -= inMat.m[0][0];
			this->m[1][0] -= inMat.m[1][0];
			this->m[2][0] -= inMat.m[2][0];
			this->m[3][0] -= inMat.m[3][0];

			this->m[0][1] -= inMat.m[0][1];
			this->m[1][1] -= inMat.m[1][1];
			this->m[2][1] -= inMat.m[2][1];
			this->m[3][1] -= inMat.m[3][1];

			this->m[0][2] -= inMat.m[0][2];
			this->m[1][2] -= inMat.m[1][2];
			this->m[2][2] -= inMat.m[2][2];
			this->m[3][2] -= inMat.m[3][2];

			this->m[0][3] -= inMat.m[0][3];
			this->m[1][3] -= inMat.m[1][3];
			this->m[2][3] -= inMat.m[2][3];
			this->m[3][3] -= inMat.m[3][3];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator-=(const T &inVal)
		{
			this->m[0][0] -= inVal;
			this->m[1][0] -= inVal;
			this->m[2][0] -= inVal;
			this->m[3][0] -= inVal;

			this->m[0][1] -= inVal;
			this->m[1][1] -= inVal;
			this->m[2][1] -= inVal;
			this->m[3][1] -= inVal;

			this->m[0][2] -= inVal;
			this->m[1][2] -= inVal;
			this->m[2][2] -= inVal;
			this->m[3][2] -= inVal;

			this->m[0][3] -= inVal;
			this->m[1][3] -= inVal;
			this->m[2][3] -= inVal;
			this->m[3][3] -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator*=(const MLMatrix4<T> &inMat)
		{
			this->m[0][0] = (this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]) + (this->m[0][2] * inMat.m[2][0]) + (this->m[0][3] * inMat.m[3][0]);
			this->m[1][0] = (this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]) + (this->m[0][2] * inMat.m[2][1]) + (this->m[0][3] * inMat.m[3][1]);
			this->m[2][0] = (this->m[0][0] * inMat.m[0][2]) + (this->m[0][1] * inMat.m[1][2]) + (this->m[0][2] * inMat.m[2][2]) + (this->m[0][3] * inMat.m[3][2]);
			this->m[3][0] = (this->m[0][0] * inMat.m[0][3]) + (this->m[0][1] * inMat.m[1][3]) + (this->m[0][2] * inMat.m[2][3]) + (this->m[0][3] * inMat.m[3][3]);

			this->m[0][1] = (this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]) + (this->m[1][2] * inMat.m[2][0]) + (this->m[1][3] * inMat.m[3][0]);
			this->m[1][1] = (this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]) + (this->m[1][2] * inMat.m[2][1]) + (this->m[1][3] * inMat.m[3][1]);
			this->m[2][1] = (this->m[1][0] * inMat.m[0][2]) + (this->m[1][1] * inMat.m[1][2]) + (this->m[1][2] * inMat.m[2][2]) + (this->m[1][3] * inMat.m[3][2]);
			this->m[3][1] = (this->m[1][0] * inMat.m[0][3]) + (this->m[1][1] * inMat.m[1][3]) + (this->m[1][2] * inMat.m[2][3]) + (this->m[1][3] * inMat.m[3][3]);

			this->m[0][2] = (this->m[2][0] * inMat.m[0][0]) + (this->m[2][1] * inMat.m[1][0]) + (this->m[2][2] * inMat.m[2][0]) + (this->m[2][3] * inMat.m[3][0]);
			this->m[1][2] = (this->m[2][0] * inMat.m[0][1]) + (this->m[2][1] * inMat.m[1][1]) + (this->m[2][2] * inMat.m[2][1]) + (this->m[2][3] * inMat.m[3][1]);
			this->m[2][2] = (this->m[2][0] * inMat.m[0][2]) + (this->m[2][1] * inMat.m[1][2]) + (this->m[2][2] * inMat.m[2][2]) + (this->m[2][3] * inMat.m[3][2]);
			this->m[3][2] = (this->m[2][0] * inMat.m[0][3]) + (this->m[2][1] * inMat.m[1][3]) + (this->m[2][2] * inMat.m[2][3]) + (this->m[2][3] * inMat.m[3][3]);

			this->m[0][3] = (this->m[3][0] * inMat.m[0][0]) + (this->m[3][1] * inMat.m[1][0]) + (this->m[3][2] * inMat.m[2][0]) + (this->m[3][3] * inMat.m[3][0]);
			this->m[1][3] = (this->m[3][0] * inMat.m[0][1]) + (this->m[3][1] * inMat.m[1][1]) + (this->m[3][2] * inMat.m[2][1]) + (this->m[3][3] * inMat.m[3][1]);
			this->m[2][3] = (this->m[3][0] * inMat.m[0][2]) + (this->m[3][1] * inMat.m[1][2]) + (this->m[3][2] * inMat.m[2][2]) + (this->m[3][3] * inMat.m[3][2]);
			this->m[3][3] = (this->m[3][0] * inMat.m[0][3]) + (this->m[3][1] * inMat.m[1][3]) + (this->m[3][2] * inMat.m[2][3]) + (this->m[3][3] * inMat.m[3][3]);
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator*=(const T &inVal)
		{
			this->m[0][0] *= inVal;
			this->m[1][0] *= inVal;
			this->m[2][0] *= inVal;
			this->m[3][0] *= inVal;

			this->m[0][1] *= inVal;
			this->m[1][1] *= inVal;
			this->m[2][1] *= inVal;
			this->m[3][1] *= inVal;

			this->m[0][2] *= inVal;
			this->m[1][2] *= inVal;
			this->m[2][2] *= inVal;
			this->m[3][2] *= inVal;

			this->m[0][3] *= inVal;
			this->m[1][3] *= inVal;
			this->m[2][3] *= inVal;
			this->m[3][3] *= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T>& MLMatrix4<T>::operator/=(const T &inVal)
		{
			this->m[0][0] /= inVal;
			this->m[1][0] /= inVal;
			this->m[2][0] /= inVal;
			this->m[3][0] /= inVal;

			this->m[0][1] /= inVal;
			this->m[1][1] /= inVal;
			this->m[2][1] /= inVal;
			this->m[3][1] /= inVal;

			this->m[0][2] /= inVal;
			this->m[1][2] /= inVal;
			this->m[2][2] /= inVal;
			this->m[3][2] /= inVal;

			this->m[0][3] /= inVal;
			this->m[1][3] /= inVal;
			this->m[2][3] /= inVal;
			this->m[3][3] /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator+(const MLMatrix4<T> &inMat)
		{
			return MLMatrix4<T>(this->m[0][0] + inMat.m[0][0],
								this->m[1][0] + inMat.m[1][0],
								this->m[2][0] + inMat.m[2][0],
								this->m[3][0] + inMat.m[3][0],

								this->m[0][1] + inMat.m[0][1],
								this->m[1][1] + inMat.m[1][1],
								this->m[2][1] + inMat.m[2][1],
								this->m[3][1] + inMat.m[3][1],

								this->m[0][2] + inMat.m[0][2],
								this->m[1][2] + inMat.m[1][2],
								this->m[2][2] + inMat.m[2][2],
								this->m[3][2] + inMat.m[3][2],

								this->m[0][3] + inMat.m[0][3],
								this->m[1][3] + inMat.m[1][3],
								this->m[2][3] + inMat.m[2][3],
								this->m[3][3] + inMat.m[3][3]);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator+(const T &inVal)
		{
			return MLMatrix4<T>(this->m[0][0] + inVal,
								this->m[1][0] + inVal,
								this->m[2][0] + inVal,
								this->m[3][0] + inVal,

								this->m[0][1] + inVal,
								this->m[1][1] + inVal,
								this->m[2][1] + inVal,
								this->m[3][1] + inVal,

								this->m[0][2] + inVal,
								this->m[1][2] + inVal,
								this->m[2][2] + inVal,
								this->m[3][2] + inVal,

								this->m[0][3] + inVal,
								this->m[1][3] + inVal,
								this->m[2][3] + inVal,
								this->m[3][3] + inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator-(const MLMatrix4<T> &inMat)
		{
			return MLMatrix4<T>(this->m[0][0] - inMat.m[0][0],
						 this->m[1][0] - inMat.m[1][0],
						 this->m[2][0] - inMat.m[2][0],
						 this->m[3][0] - inMat.m[3][0],

						 this->m[0][1] - inMat.m[0][1],
						 this->m[1][1] - inMat.m[1][1],
						 this->m[2][1] - inMat.m[2][1],
						 this->m[3][1] - inMat.m[3][1],

						 this->m[0][2] - inMat.m[0][2],
						 this->m[1][2] - inMat.m[1][2],
						 this->m[2][2] - inMat.m[2][2],
						 this->m[3][2] - inMat.m[3][2],

						 this->m[0][3] - inMat.m[0][3],
						 this->m[1][3] - inMat.m[1][3],
						 this->m[2][3] - inMat.m[2][3],
						 this->m[3][3] - inMat.m[3][3]);
		}
		
		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator-(const T &inVal)
		{
			return MLMatrix4<T>(this->m[0][0] - inVal,
								this->m[1][0] - inVal,
								this->m[2][0] - inVal,
								this->m[3][0] - inVal,

								this->m[0][1] - inVal,
								this->m[1][1] - inVal,
								this->m[2][1] - inVal,
								this->m[3][1] - inVal,

								this->m[0][2] - inVal,
								this->m[1][2] - inVal,
								this->m[2][2] - inVal,
								this->m[3][2] - inVal,

								this->m[0][3] - inVal,
								this->m[1][3] - inVal,
								this->m[2][3] - inVal,
								this->m[3][3] - inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator*(const MLMatrix4<T> &inMat)
		{
			return MLMatrix4<T>((this->m[0][0] * inMat.m[0][0]) + (this->m[0][1] * inMat.m[1][0]) + (this->m[0][2] * inMat.m[2][0]) + (this->m[0][3] * inMat.m[3][0]),
								(this->m[0][0] * inMat.m[0][1]) + (this->m[0][1] * inMat.m[1][1]) + (this->m[0][2] * inMat.m[2][1]) + (this->m[0][3] * inMat.m[3][1]),
								(this->m[0][0] * inMat.m[0][2]) + (this->m[0][1] * inMat.m[1][2]) + (this->m[0][2] * inMat.m[2][2]) + (this->m[0][3] * inMat.m[3][2]),
								(this->m[0][0] * inMat.m[0][3]) + (this->m[0][1] * inMat.m[1][3]) + (this->m[0][2] * inMat.m[2][3]) + (this->m[0][3] * inMat.m[3][3]),

								(this->m[1][0] * inMat.m[0][0]) + (this->m[1][1] * inMat.m[1][0]) + (this->m[1][2] * inMat.m[2][0]) + (this->m[1][3] * inMat.m[3][0]),
								(this->m[1][0] * inMat.m[0][1]) + (this->m[1][1] * inMat.m[1][1]) + (this->m[1][2] * inMat.m[2][1]) + (this->m[1][3] * inMat.m[3][1]),
								(this->m[1][0] * inMat.m[0][2]) + (this->m[1][1] * inMat.m[1][2]) + (this->m[1][2] * inMat.m[2][2]) + (this->m[1][3] * inMat.m[3][2]),
								(this->m[1][0] * inMat.m[0][3]) + (this->m[1][1] * inMat.m[1][3]) + (this->m[1][2] * inMat.m[2][3]) + (this->m[1][3] * inMat.m[3][3]),

								(this->m[2][0] * inMat.m[0][0]) + (this->m[2][1] * inMat.m[1][0]) + (this->m[2][2] * inMat.m[2][0]) + (this->m[2][3] * inMat.m[3][0]),
								(this->m[2][0] * inMat.m[0][1]) + (this->m[2][1] * inMat.m[1][1]) + (this->m[2][2] * inMat.m[2][1]) + (this->m[2][3] * inMat.m[3][1]),
								(this->m[2][0] * inMat.m[0][2]) + (this->m[2][1] * inMat.m[1][2]) + (this->m[2][2] * inMat.m[2][2]) + (this->m[2][3] * inMat.m[3][2]),
								(this->m[2][0] * inMat.m[0][3]) + (this->m[2][1] * inMat.m[1][3]) + (this->m[2][2] * inMat.m[2][3]) + (this->m[2][3] * inMat.m[3][3]),

								(this->m[3][0] * inMat.m[0][0]) + (this->m[3][1] * inMat.m[1][0]) + (this->m[3][2] * inMat.m[2][0]) + (this->m[3][3] * inMat.m[3][0]),
								(this->m[3][0] * inMat.m[0][1]) + (this->m[3][1] * inMat.m[1][1]) + (this->m[3][2] * inMat.m[2][1]) + (this->m[3][3] * inMat.m[3][1]),
								(this->m[3][0] * inMat.m[0][2]) + (this->m[3][1] * inMat.m[1][2]) + (this->m[3][2] * inMat.m[2][2]) + (this->m[3][3] * inMat.m[3][2]),
								(this->m[3][0] * inMat.m[0][3]) + (this->m[3][1] * inMat.m[1][3]) + (this->m[3][2] * inMat.m[2][3]) + (this->m[3][3] * inMat.m[3][3]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLMatrix4<T>::operator*(const MLVector4<T> &inVec4)
		{
			return MLVector4<T>((this->m[0][0] * inVec4.x) + (this->m[0][1] * inVec4.y) + (this->m[0][2] * inVec4.z) + (this->m[0][3] * inVec4.w),
								(this->m[1][0] * inVec4.x) + (this->m[1][1] * inVec4.y) + (this->m[1][2] * inVec4.z) + (this->m[1][3] * inVec4.w),
								(this->m[2][0] * inVec4.x) + (this->m[2][1] * inVec4.y) + (this->m[2][2] * inVec4.z) + (this->m[2][3] * inVec4.w),
								(this->m[3][0] * inVec4.x) + (this->m[3][1] * inVec4.y) + (this->m[3][2] * inVec4.z) + (this->m[3][3] * inVec4.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLMatrix4<T>::operator*(const MLVector3<T> &inVec3)
		{
			MLVector4<T> mulVec(inVec3.x, inVec3.y, inVec3.z, 1.0f);

			return MLVector3<T>((this->m[0][0] * mulVec.x) + (this->m[0][1] * mulVec.y) + (this->m[0][2] * mulVec.z) + (this->m[0][3] * mulVec.w),
								(this->m[1][0] * mulVec.x) + (this->m[1][1] * mulVec.y) + (this->m[1][2] * mulVec.z) + (this->m[1][3] * mulVec.w),
								(this->m[2][0] * mulVec.x) + (this->m[2][1] * mulVec.y) + (this->m[2][2] * mulVec.z) + (this->m[2][3] * mulVec.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLMatrix4<T>::operator*(const MLVector2<T> &inVec2)
		{
			MLVector4<T> mulVec(inVec2.x, inVec2.y, 0.0f, 1.0f);
			MLVector4<T> result = *this * mulVec;

			return MLVector2<T>(result.x, result.y);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator*(const T &inVal)
		{
			return MLMatrix4<T>(this->m[0][0] * inVal,
								this->m[1][0] * inVal,
								this->m[2][0] * inVal,
								this->m[3][0] * inVal,

								this->m[0][1] * inVal,
								this->m[1][1] * inVal,
								this->m[2][1] * inVal,
								this->m[3][1] * inVal,

								this->m[0][2] * inVal,
								this->m[1][2] * inVal,
								this->m[2][2] * inVal,
								this->m[3][2] * inVal,

								this->m[0][3] * inVal,
								this->m[1][3] * inVal,
								this->m[2][3] * inVal,
								this->m[3][3] * inVal);
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::operator/(const T &inVal)
		{
			return MLMatrix4<T>(this->m[0][0] / inVal,
								this->m[1][0] / inVal,
								this->m[2][0] / inVal,
								this->m[3][0] / inVal,

								this->m[0][1] / inVal,
								this->m[1][1] / inVal,
								this->m[2][1] / inVal,
								this->m[3][1] / inVal,

								this->m[0][2] / inVal,
								this->m[1][2] / inVal,
								this->m[2][2] / inVal,
								this->m[3][2] / inVal,

								this->m[0][3] / inVal,
								this->m[1][3] / inVal,
								this->m[2][3] / inVal,
								this->m[3][3] / inVal);
		}


		//=========================================================
		// Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator==(const MLMatrix4<T> &inMat)
		{
			return	this->m[0][0] == inMat.m[0][0] &&
					this->m[1][0] == inMat.m[1][0] &&
					this->m[2][0] == inMat.m[2][0] &&
					this->m[3][0] == inMat.m[3][0] &&

					this->m[0][1] == inMat.m[0][1] && 
					this->m[1][1] == inMat.m[1][1] &&
					this->m[2][1] == inMat.m[2][1] &&
					this->m[3][1] == inMat.m[3][1] &&

					this->m[0][2] == inMat.m[0][2] &&
					this->m[1][2] == inMat.m[1][2] &&
					this->m[2][2] == inMat.m[2][2] &&
					this->m[3][2] == inMat.m[3][2] &&

					this->m[0][3] == inMat.m[0][3] &&
					this->m[1][3] == inMat.m[1][3] &&
					this->m[2][3] == inMat.m[2][3] &&
					this->m[3][3] == inMat.m[3][3];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator!=(const MLMatrix4<T> &inMat)
		{
			return !(this == inMat);
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator<(const MLMatrix4<T> &inMat)
		{
			return	this->m[0][0] < inMat.m[0][0] &&
					this->m[1][0] < inMat.m[1][0] &&
					this->m[2][0] < inMat.m[2][0] &&
					this->m[3][0] < inMat.m[3][0] &&

					this->m[0][1] < inMat.m[0][1] && 
					this->m[1][1] < inMat.m[1][1] &&
					this->m[2][1] < inMat.m[2][1] &&
					this->m[3][1] < inMat.m[3][1] &&

					this->m[0][2] < inMat.m[0][2] &&
					this->m[1][2] < inMat.m[1][2] &&
					this->m[2][2] < inMat.m[2][2] &&
					this->m[3][2] < inMat.m[3][2] &&

					this->m[0][3] < inMat.m[0][3] &&
					this->m[1][3] < inMat.m[1][3] &&
					this->m[2][3] < inMat.m[2][3] &&
					this->m[3][3] < inMat.m[3][3];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator>(const MLMatrix4<T> &inMat)
		{
			return	this->m[0][0] > inMat.m[0][0] &&
					this->m[1][0] > inMat.m[1][0] &&
					this->m[2][0] > inMat.m[2][0] &&
					this->m[3][0] > inMat.m[3][0] &&

					this->m[0][1] > inMat.m[0][1] && 
					this->m[1][1] > inMat.m[1][1] &&
					this->m[2][1] > inMat.m[2][1] &&
					this->m[3][1] > inMat.m[3][1] &&

					this->m[0][2] > inMat.m[0][2] &&
					this->m[1][2] > inMat.m[1][2] &&
					this->m[2][2] > inMat.m[2][2] &&
					this->m[3][2] > inMat.m[3][2] &&

					this->m[0][3] > inMat.m[0][3] &&
					this->m[1][3] > inMat.m[1][3] &&
					this->m[2][3] > inMat.m[2][3] &&
					this->m[3][3] > inMat.m[3][3];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator<=(const MLMatrix4<T> &inMat)
		{
			return	this->m[0][0] <= inMat.m[0][0] &&
					this->m[1][0] <= inMat.m[1][0] &&
					this->m[2][0] <= inMat.m[2][0] &&
					this->m[3][0] <= inMat.m[3][0] &&

					this->m[0][1] <= inMat.m[0][1] && 
					this->m[1][1] <= inMat.m[1][1] &&
					this->m[2][1] <= inMat.m[2][1] &&
					this->m[3][1] <= inMat.m[3][1] &&

					this->m[0][2] <= inMat.m[0][2] &&
					this->m[1][2] <= inMat.m[1][2] &&
					this->m[2][2] <= inMat.m[2][2] &&
					this->m[3][2] <= inMat.m[3][2] &&

					this->m[0][3] <= inMat.m[0][3] &&
					this->m[1][3] <= inMat.m[1][3] &&
					this->m[2][3] <= inMat.m[2][3] &&
					this->m[3][3] <= inMat.m[3][3];
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLMatrix4<T>::operator>=(const MLMatrix4<T> &inMat)
		{
			return	this->m[0][0] >= inMat.m[0][0] &&
					this->m[1][0] >= inMat.m[1][0] &&
					this->m[2][0] >= inMat.m[2][0] &&
					this->m[3][0] >= inMat.m[3][0] &&

					this->m[0][1] >= inMat.m[0][1] && 
					this->m[1][1] >= inMat.m[1][1] &&
					this->m[2][1] >= inMat.m[2][1] &&
					this->m[3][1] >= inMat.m[3][1] &&

					this->m[0][2] >= inMat.m[0][2] &&
					this->m[1][2] >= inMat.m[1][2] &&
					this->m[2][2] >= inMat.m[2][2] &&
					this->m[3][2] >= inMat.m[3][2] &&

					this->m[0][3] >= inMat.m[0][3] &&
					this->m[1][3] >= inMat.m[1][3] &&
					this->m[2][3] >= inMat.m[2][3] &&
					this->m[3][3] >= inMat.m[3][3];
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLMatrix4<T>::operator[](ML_INT index)
		{
			if(index < 0) index = 0;
			if(index > 3) index = 3;
			if(index == 0) return this->x;
			if(index == 1) return this->y;
			if(index == 2) return this->z;
			if(index == 3) return this->w;
		}

		//=========================================================
		// Matrix 4 Math Functions
		//=========================================================

		template<typename T>
		T MLMatrix4<T>::Determinant()
		{
			T A =	this->m[0][0] * (this->m[1][1] * (this->m[2][2] * this->m[3][3]) - (this->m[2][3] * this->m[3][2])) - 
									(this->m[1][2] * (this->m[2][1] * this->m[3][3]) - (this->m[2][3] * this->m[3][1])) +
									(this->m[1][3] * (this->m[2][1] * this->m[3][2]) - (this->m[2][2] * this->m[3][1]));
			
			T B =	this->m[0][1] * (this->m[1][0] * (this->m[2][2] * this->m[3][3]) - (this->m[2][3] * this->m[3][2])) - 
									(this->m[1][2] * (this->m[2][0] * this->m[3][3]) - (this->m[2][3] * this->m[3][0])) +
									(this->m[1][3] * (this->m[2][0] * this->m[3][2]) - (this->m[2][2] * this->m[3][0]));

			T C =	this->m[0][2] * (this->m[1][0] * (this->m[2][1] * this->m[3][3]) - (this->m[2][3] * this->m[3][1])) - 
									(this->m[1][1] * (this->m[2][0] * this->m[3][3]) - (this->m[2][3] * this->m[3][0])) +
									(this->m[1][3] * (this->m[2][0] * this->m[3][1]) - (this->m[2][1] * this->m[3][0]));
			
			T D =	this->m[0][3] * (this->m[1][0] * (this->m[2][1] * this->m[3][2]) - (this->m[2][2] * this->m[3][1])) - 
									(this->m[1][1] * (this->m[2][0] * this->m[3][2]) - (this->m[2][2] * this->m[3][0])) +
									(this->m[1][2] * (this->m[2][0] * this->m[3][1]) - (this->m[2][1] * this->m[3][0]));
			return A - B + C - D;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::Transpose()
		{
			MLMatrix4<T> Result;

			Result.m[0][1] = this->m[1][0];	
			Result.m[0][2] = this->m[2][0];
			Result.m[0][3] = this->m[3][0];

			Result.m[1][0] = this->m[0][1];	
			Result.m[1][2] = this->m[2][1];
			Result.m[1][3] = this->m[3][1];

			Result.m[2][0] = this->m[0][2];	
			Result.m[2][1] = this->m[1][2];
			Result.m[2][3] = this->m[3][2];

			Result.m[3][0] = this->m[0][3];	
			Result.m[3][1] = this->m[1][3];
			Result.m[3][2] = this->m[2][3];

			return Result;
		}

		template<typename T>
		ML_MATH_INLINE MLMatrix4<T> MLMatrix4<T>::Inverse()
		{
			MLMatrix4<T> OriginalMat = this;
			MLMatrix4<T> Result;

			for(ML_UINT i = 0; i < ML_MAT4_ROWS; i++)
			{
				ML_FLOAT divVal = OriginalMat[i][i];
				//Divide at row by the diagonal values
				Result[i]		/= divVal;
				OriginalMat[i]	/= divVal;

				for (ML_UINT j = 0; j < ML_MAT4_ROWS; j++)
				{
					if(j != i)
					{
						ML_FLOAT multVal1 = OriginalMat[j][i];
						ML_FLOAT multVal2 = OriginalMat[j][i];
						OriginalMat[j] = OriginalMat[j] - (OriginalMat[i] * multVal1);
						Result[j] = Result[j] - (Result[i] * multVal2);
					}
				}
			}

			return Result;
		}

		template<typename T>
		ML_MATH_INLINE T* toptr(const MLMatrix4<T> &Mat4A)
		{
			return (T*)Mat4A.m;
		}


	}
}

#endif