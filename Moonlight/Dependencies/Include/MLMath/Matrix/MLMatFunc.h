#ifndef MLMATFUNC_H
#define MLMATFUNC_H
#include <math.h>
#include "MLMath/MLMathDefine.h"

#include "Matrix/MLMat4.h"

#include "Vector/MLVector3.h"

namespace MLMath
{
	namespace MLMat4
	{
		//=========================================================
		// Name: Identify Matrix
		// Description: Sets the input matrix to a 4x4 identity
		// matrix.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE void MLidentify(MLMatrix4<T>& inMat)
		{
			inMat.m[0][0] = 1.0f;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 1.0f;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = 1.0f;
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		//=========================================================
		// Name: Translation Matrix
		// Description: Creates a homogeneous translation matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE void MLtranslate(MLMatrix4<T>& inMat, MLVec3::MLVector3<T> pos)
		{
			inMat.m[0][0] = 1.0f;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = pos.x;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 1.0f;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = pos.y;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = 1.0f;
			inMat.m[2][3] = pos.z;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		//=========================================================
		// Name: Scale Matrix
		// Description: Creates a homogeneous scale matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE void MLscale(MLMatrix4<T>& inMat, MLVec3::MLVector3<T> XYZScale)
		{
			inMat.m[0][0] = XYZScale.x;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = XYZScale.y;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = XYZScale.z;
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		//=========================================================
		// Name: Rotation Matrix
		// Description: Creates a homogeneous rotation matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE void MLrotation(MLMatrix4<T>& inMat, T rotX, T rotY, T rotZ)
		{
			T cosY = cos(rotY);
			T cosZ = cos(rotZ);
			T cosX = cos(rotX);
			T sinY = sin(rotY);
			T sinZ = sin(rotZ);
			T sinX = sin(rotX);

			inMat.m[0][0] = (cosY * cosZ);
			inMat.m[0][1] = ((cosZ * sinX * sinY) - (cosX * sinZ));
			inMat.m[0][2] = ((cosX * cosZ * sinY) + (sinX * sinZ));
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = (cosY * sinZ);
			inMat.m[1][1] = ((cosX * cosZ) + (sinX * sinY * sinZ));
			inMat.m[1][2] = -((cosZ * sinX) + (cosX * sinY * sinZ));
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = -(sinY);
			inMat.m[2][1] = (cosY * sinX);
			inMat.m[2][2] = (cosX * cosY);
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		//=========================================================
		// Name: Inverse
		// Description: Returns a inverse matrix of the input matrix
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		MLMatrix4<T> MLinverse(MLMatrix4<T> inMat)
		{
			MLMatrix4<T> OriginalMat = inMat;
			MLMatrix4<T> Result;

			for(ML_UINT i = 0; i < ML_MAT4_ROWS; i++)
			{
				ML_FLOAT divVal = OriginalMat[i][i];
				//Divide at row by the diagonal values
				Result[i]		/= divVal;
				OriginalMat[i]	/= divVal;

				for (ML_UINT j = 0; j < ML_MAT4_ROWS; j++)
				{
					if(j != i)
					{
						ML_FLOAT multVal1 = OriginalMat[j][i];
						ML_FLOAT multVal2 = OriginalMat[j][i];
						OriginalMat[j] = OriginalMat[j] - (OriginalMat[i] * multVal1);
						Result[j] = Result[j] - (Result[i] * multVal2);
					}
				}
			}

			return Result;
		}

		//=========================================================
		// Name: Orthographic Matrix
		// Description: Creates a orthographic matrix with prefixed
		// distance of 0 to 1.
		// Creator: Chris Millin
		//=========================================================
		template<typename T> 
		ML_MATH_INLINE void MLortho(MLMatrix4<T>& inMat, T top, T bottom, T left, T right)
		{
			MLortho<T>(inMat, top, bottom, left, right, 0.0f, 1.0f);
		}

		//=========================================================
		// Name: Orthographic Matrix
		// Description: Creates a orthographic matrix 
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE void MLortho(MLMatrix4<T>& inMat, T top, T bottom, T left, T right, T nearz, T farz)
		{
			inMat.m[0][0] = 2 / (right - left);
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 2 / (top - bottom);
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = -(2 / (farz - nearz));
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = -((right + left) / (right - left));
			inMat.m[3][1] = -((top + bottom) / (top - bottom));
			inMat.m[3][2] = -((farz + nearz) / (farz - nearz));
			inMat.m[3][3] = 1.0f;
		}

	}

	
	namespace MLMat3
	{
		//=========================================================
		// Name: Identify Matrix
		// Description: Sets the input matrix to a 3x3 identity
		// matrix.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void identify(MLMatrix3<T>& inMat)
		{
			inMat[0][0] = 1.0f;
			inMat[0][1] = 0.0f;
			inMat[0][2] = 0.0f;

			inMat[1][0] = 0.0f;
			inMat[1][1] = 1.0f;
			inMat[1][2] = 0.0f;

			inMat[2][0] = 0.0f;
			inMat[2][1] = 0.0f;
			inMat[2][2] = 1.0f;
		}

		//=========================================================
		// Name: Translation Matrix
		// Description: Creates a homogeneous translation matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void translate(MLMatrix3<T>& inMat, MLVec3::MLVector3<T> pos)
		{
			inMat.m[0][0] = 1.0f;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = pos.x;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 1.0f;
			inMat.m[1][2] = pos.y;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = pos.z;
		}

		//=========================================================
		// Name: Scale Matrix
		// Description: Creates a homogeneous scale matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void scale(MLMatrix3<T>& inMat, MLVec3::MLVector3<T> XYZScale)
		{
			inMat.m[0][0] = XYZScale.x;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = XYZScale.y;
			inMat.m[1][2] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = XYZScale.z;
		}
		
		//=========================================================
		// Name: Rotation Matrix
		// Description: Creates a homogeneous rotation matrix  
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void rotation(MLMatrix3<T>& inMat, T rotX, T rotY, T rotZ)
		{
			T cosY = cos(rotY);
			T cosZ = cos(rotZ);
			T cosX = cos(rotX);
			T sinY = sin(rotY);
			T sinZ = sin(rotZ);
			T sinX = sin(rotX);

			inMat.m[0][0] = (cosY * cosZ);
			inMat.m[0][1] = ((cosZ * sinX * sinY) - (cosX * sinZ));
			inMat.m[0][2] = ((cosX * cosZ * sinY) + (sinX * sinZ));

			inMat.m[1][0] = (cosY * sinZ);
			inMat.m[1][1] = ((cosX * cosZ) + (sinX * sinY * sinZ));
			inMat.m[1][2] = -((cosZ * sinX) + (cosX * sinY * sinZ));

			inMat.m[2][0] = -(sinY);
			inMat.m[2][1] = (cosY * sinX);
			inMat.m[2][2] = (cosX * cosY);
		}
		
		//=========================================================
		// Name: Shear/Skew
		// Description: Converts the input matrix into a Shear/Skew
		// matrix based on the XY Skew
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLshear(MLMatrix3<T>& inMat, MLVec2::MLVector2<T> XYSkew)
		{
			inMat[0][0] = 1.0f;
			inMat[0][1] = tanf(XYSkew.x);
			inMat[0][2] = 0.0f;

			inMat[1][0] = tanf(XYSkew.x);
			inMat[1][1] = 1.0f;
			inMat[1][2] = 0.0f;

			inMat[2][0] = 0.0f;
			inMat[2][1] = 0.0f;
			inMat[2][2] = 1.0f;
		}

		//=========================================================
		// Name: Inverse
		// Description: Returns a inverse matrix of the input matrix
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		MLMatrix3<T> MLinverse(MLMatrix3<T> inMat)
		{
			MLMatrix3<T> OriginalMat = inMat;
			MLMatrix3<T> Result;

			for(ML_UINT i = 0; i < ML_MAT3_ROWS; i++)
			{
				ML_FLOAT divVal = OriginalMat[i][i];
				//Divide at row by the diagonal values
				Result[i]		/= divVal;
				OriginalMat[i]	/= divVal;

				for (ML_UINT j = 0; j < ML_MAT3_ROWS; j++)
				{
					if(j != i)
					{
						ML_FLOAT multVal1 = OriginalMat[j][i];
						ML_FLOAT multVal2 = OriginalMat[j][i];
						OriginalMat[j] = OriginalMat[j] - (OriginalMat[i] * multVal1);
						Result[j] = Result[j] - (Result[i] * multVal2);
					}
				}
			}

			return Result;
		}
	}

	namespace MLMat2
	{
		//=========================================================
		// Name: Identify
		// Description: Converts the input matrix into a identity
		// matrix
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLidentify(MLMatrix2<T>& inMat)
		{
			inMat[0][0] = 1.0f;
			inMat[0][1] = 0.0f;

			inMat[1][0] = 0.0f;
			inMat[1][1] = 1.0f;
		}

		//=========================================================
		// Name: Translate
		// Description: Converts the input matrix into a translation
		// matrix based on pos
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLtranslate(MLMatrix2<T>& inMat, MLVec2::MLVector2<T> pos)
		{
			inMat[0][0] = 1.0f;
			inMat[0][1] = pos.x;

			inMat[1][0] = 0.0f;
			inMat[1][1] = pos.y;
		}

		//=========================================================
		// Name: Scale
		// Description: Converts the input matrix into a scaling
		// matrix based on XYScale
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLscale(MLMatrix2<T>& inMat, MLVec2::MLVector2<T> XYScale)
		{
			inMat[0][0] = XYScale.x;
			inMat[0][1] = 0.0f;

			inMat[1][0] = 0.0f;
			inMat[1][1] = XYScale.y;
		}

		//=========================================================
		// Name: Rotate
		// Description: Converts the input matrix into a Rotation
		// matrix based on Rot(in radians)
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLrotate(MLMatrix2<T>& inMat, T Rot)
		{
			inMat[0][0] = (ML_FLOAT)cos(Rot);
			inMat[0][1] = -(ML_FLOAT)sin(Rot);

			inMat[1][0] = (ML_FLOAT)sin(Rot);
			inMat[1][1] = (ML_FLOAT)sin(Rot);
		}

		//=========================================================
		// Name: Shear/Skew
		// Description: Converts the input matrix into a Shear/Skew
		// matrix based on the XY Skew
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		void MLshear(MLMatrix2<T>& inMat, MLVec2::MLVector2<T> XYSkew)
		{
			inMat[0][0] = 1.0f;
			inMat[0][1] = XYSkew.x;

			inMat[1][0] = 0.0f;
			inMat[1][1] = XYSkew.y;
		}

		//=========================================================
		// Name: Inverse
		// Description: Returns a inverse matrix of the input matrix
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		MLMatrix2<T> MLinverse(MLMatrix2<T> inMat)
		{
			MLMatrix2<T> result;
			//store the inverse of the determinant
			ML_FLOAT iDeterminant = 1.0f/inMat.Determinant();
			result[0][0] = iDeterminant * inMat[1][1];
			result[0][1] = iDeterminant * -(inMat[0][1]);

			result[1][0] = iDeterminant * -(inMat[1][0]);
			result[1][1] = iDeterminant * inMat[0][0];

			return result;
		}

	}

}

#endif