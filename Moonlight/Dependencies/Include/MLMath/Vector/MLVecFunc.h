#ifndef MLVECFUNC_H
#define MLVECFUNC_H
#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector2.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector4.h"

using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;
using namespace MLMath::MLVec4;




#define X_AXIS 1
#define Y_AXIS 2

namespace MLMath
{
	namespace MLVec2
	{
		//=========================================================
		// Name: Dot Product
		// Description: Calculates the Dot Product of the vector
		// which is the projection of B onto A with the scale of
		// A's magnitude. It also calculates the cosine of the
		// two vectors.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE T MLdot(const MLVector2<T> &VecA, const MLVector2<T> &VecB)
		{
			return (VecA.x * VecB.x) + (VecA.y * VecB.y);
		}

		//=========================================================
		// Name: Cross Product
		// Description: Calculates the Cross Product of the 2D 
		// vector (where z is 0) this results in a perpendicular
		// vector pointing out of the screen (in the Z axis). In
		// this instance it can be used to calculate the direction
		// of the two vectors (positive clockwise, negative anti 
		// clockwise)
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE T MLcross(const MLVector2<T> &vecA, const MLVector2<T> &vecB)
		{
			return (VecA.x * VecB.y - VecA.y * VecB.x);
		}

		//=========================================================
		// Name: Mid Point
		// Description: Calculates the mid point between vectorA to
		// VectorB. This is done by assuming vector C is the middle
		// and using the understanding of the vector A to B is 
		// B - A then the mid point C will be C - A and B - C.
		// reordering this C^2 = B + A and then C = (B + A)/2
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLmidPoint(const MLVector2<T> &PointA, const MLVector2<T> &PointB)
		{
			return (PointA+PointB)/2;
		}

		//=========================================================
		// Name: Angle
		// Description: Calculates the angle between A and B
		// between -180 and 180
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE T MLangle(const MLVector2<T> &VecA, const MLVector2<T> &VecB)
		{
			return ML_Rad2Deg(atan2(VecB.x - VecA.x, VecB.y - VecA.y));
		}
	}

	namespace MLVec3
	{

		//=========================================================
		// Name: Dot Product
		// Description: Calculates the shadow which is casted on
		// A by vector B based on the magnitude of A.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE T MLdot(const MLVector3<T> &VecA, const MLVector3<T> &VecB)
		{
			return (VecA.x * VecB.x) + (VecA.y * VecB.y) + (VecA.z * VecB.z);
		}

		//=========================================================
		// Name: Cross Product
		// Description: Calculates the perpendicular vector between 
		// A and B (A X B)
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLcross(const MLVector3<T> &VecA, const MLVector3<T> &VecB)
		{
			return MLVector3<T>((VecA.y * VecB.z - VecB.y * VecB.z), (VecB.x * VecA.z - VecA.x * VecB.z), (VecA.x * VecB.y - VecA.y * VecB.x));
		}

		//=========================================================
		// Name: Angle
		// Description: Calculates the angle between vector and and
		// Vector Between 0 and 180 
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		ML_MATH_INLINE T MLangle(const MLVector3<T> &VecA, const MLVector3<T> &VecB)
		{
			return ML_Rad2Deg(atan2(cross(VecA, VecB).normalise(), dot(VecA, VecB)));
		}
	}

	namespace MLVec4
	{
		template<typename T>
		ML_MATH_INLINE ML_BOOL MLboundIntersect(const MLVector4<T> &BoundA, const MLVector4<T> &BoundB)
		{
			return BoundA.x <= BoundB.x1 && BoundA.x1 >= BoundB.x && BoundA.y <= BoundB.y1	&& BoundA.y1 >= BoundB.y;
		}
	}

	namespace MLVecFunc
	{
		enum LineType
		{
			None = -1,
			ParallelY = 0,
			ParallelX = 1,
			Slope = 2
		};

		template<typename T>
		ML_MATH_INLINE ML_BOOL FindIntersect(MLVector4<T> lineA, MLVector4<T> lineB, MLVector2<T>* intersect, ML_BOOL ingnoreLength)
		{
			LineType lineAType = LineType::None;
			LineType lineBType = LineType::None;

			ML_INT lineAX = 0, lineAY = 0;
			ML_INT lineBX = 0, lineBY = 0;

			//lineA x does not change. line is parallel to y axis
			if((lineA.x - lineA.z == 0) && (lineA.y - lineA.w != 0))
			{
				lineAType = LineType::ParallelY;
				lineAX = lineA.x;
			}
			//lineB x does not change. line is parallel to y axis
			if ((lineB.x - lineB.z == 0) && (lineB.y - lineB.w != 0))
			{
				lineBType = LineType::ParallelY;
				lineBX = lineB.x;
				if(lineAType == LineType::ParallelY)
				{
					if (lineA.x == lineB.x)
					{
						if ((lineA.y < lineB.y) && (lineA.w < lineB.y) && (lineA.y < lineB.w) && (lineA.w < lineB.w))
						{
							intersect->x = 0;
							intersect->y = 0;
							return ML_FALSE;
						}
						else if ((lineA.y < lineB.y) && (lineA.w < lineB.y) && (lineA.y > lineB.w) && (lineA.w > lineB.w))
						{
							intersect->x = 0;
							intersect->y = 0;
							return ML_FALSE;
						}

						if ((lineA.y < lineA.w) && (lineB.y < lineB.w))
						{
							if(lineA.y < lineB.y)
							{
								intersect->y = lineB.y;
							}
							else if(lineA.y > lineB.y)
							{
								intersect->y = lineA.y;
							}
						}
						else if((lineA.y > lineA.w) && (lineB.y < lineB.w))
						{
							if(lineA.w < lineB.y)
							{
								intersect->y = lineB.y;
							}
							else if(lineA.w > lineB.y)
							{
								intersect->y = lineA.w;
							}
						}
						else if((lineA.y < lineA.w) && (lineB.y > lineB.w))
						{
							if(lineA.y < lineB.w)
							{
								intersect->y = lineB.w;
							}
							else if(lineA.y > lineB.w)
							{
								intersect->y = lineA.y;
							}
						}
						else if((lineA.y > lineA.w) && (lineB.y > lineB.w))
						{
							if(lineA.w < lineB.w)
							{
								intersect->y = lineB.w;
							}
							else if(lineA.w > lineB.w)
							{
								intersect->y = lineA.w;
							}
						}

						intersect->x = lineA.x;

						return ML_TRUE;
					}
					else
					{
						intersect->x = 0;
						intersect->y = 0;
						return ML_FALSE;
					}
				}
			}

			if ((lineA.y - lineA.w == 0) && (lineA.x - lineA.z != 0))
			{
				lineAType = LineType::ParallelX;
				lineAY = lineA.y;
			}

			if ((lineB.y - lineB.w == 0) && (lineB.x - lineB.z != 0))
			{
				lineBType = LineType::ParallelX;
				lineBY = lineB.y;
				if(lineAType == LineType::ParallelX)
				{
					if (lineA.x == lineB.x)
					{
						if ((lineA.x < lineB.x) && (lineA.z < lineB.x) && (lineA.x < lineB.z) && (lineA.z < lineB.z))
						{
							intersect->x = 0;
							intersect->y = 0;
							return ML_FALSE;
						}
						else if ((lineA.x < lineB.x) && (lineA.z < lineB.x) && (lineA.x > lineB.z) && (lineA.z > lineB.z))
						{
							intersect->x = 0;
							intersect->y = 0;
							return ML_FALSE;
						}

						if ((lineA.x < lineA.z) && (lineB.x < lineB.z))
						{
							if(lineA.x < lineB.x)
							{
								intersect->x = lineB.x;
							}
							else if(lineA.x > lineB.x)
							{
								intersect->x = lineA.x;
							}
						}
						else if((lineA.x > lineA.z) && (lineB.x < lineB.z))
						{
							if(lineA.z < lineB.x)
							{
								intersect->x = lineB.x;
							}
							else if(lineA.z > lineB.x)
							{
								intersect->x = lineA.z;
							}
						}
						else if((lineA.x < lineA.z) && (lineB.x > lineB.z))
						{
							if(lineA.x < lineB.z)
							{
								intersect->x = lineB.z;
							}
							else if(lineA.x > lineB.z)
							{
								intersect->x = lineA.x;
							}
						}
						else if((lineA.x > lineA.z) && (lineB.x > lineB.z))
						{
							if(lineA.z < lineB.z)
							{
								intersect->x = lineB.z;
							}
							else if(lineA.z > lineB.z)
							{
								intersect->x = lineA.z;
							}
						}

						intersect->y = lineA.y;

						return ML_TRUE;
					}
					else
					{
						intersect->x = 0;
						intersect->y = 0;
						return ML_FALSE;
					}
				}
			}

			ML_FLOAT lineAM = 0, lineAC = 0;
			ML_FLOAT lineBM = 0, lineBC = 0;

			if (lineAType == LineType::None)
			{
				lineAType = LineType::Slope;

				lineAM = (lineA.w - lineA.y)/(lineA.z - lineA.x);
				lineAC = lineA.y - (lineA.x * lineAM);
				//printf("\ny = %.3fx + %.3f \nLineA x1 = %f, LineA y2 = %f,\nLineA x2 = %f, LineA y2 = %f\n", lineAM, lineAC, lineA.x, lineA.y, lineA.z, lineA.w);
				if(lineBType == LineType::ParallelY)
				{
					intersect->x = lineBX;
					intersect->y = ((lineAM * lineBX) + lineAC);
					if(ingnoreLength == ML_FALSE)
					{
						if(((intersect->y < lineB.y) && (intersect->y < lineB.w)) || ((intersect->y > lineB.y) && (intersect->y > lineB.w)))
						{
							return ML_FALSE;
						}
					}
					return ML_TRUE;
				}
				if(lineBType == LineType::ParallelX)
				{
					intersect->x = ((lineBY - lineAC)/ lineAM);
					intersect->y = lineBY;
					if(ingnoreLength == ML_FALSE)
					{
						if(((intersect->x < lineB.x) && (intersect->x < lineB.z)) || ((intersect->x > lineB.x) && (intersect->x > lineB.z)))
						{
							return ML_FALSE;
						}
					}
					return ML_TRUE;
				}
			}

			if (lineBType == LineType::None)
			{
				lineBType = LineType::Slope;

				lineBM = (lineB.w - lineB.y)/(lineB.z - lineB.x);
				lineBC = lineB.y - (lineB.x * lineBM);
				//printf("\ny = %.3fx + %.3f \nLineB x1 = %f, LineB y1 = %f,\nLineB x2 = %f, LineB y2 = %f\n", lineBM, lineBC, lineB.x, lineB.y, lineB.z, lineB.w);
				if(lineAType == LineType::ParallelY)
				{
					intersect->x = lineAX;
					intersect->y = ((lineBM * lineAX) + lineBC);
					if(ingnoreLength == ML_FALSE)
					{
						if(((intersect->y < lineA.y) && (intersect->y < lineA.w)) || ((intersect->y > lineA.y) && (intersect->y > lineA.w)))
						{
							return ML_FALSE;
						}
					}
					return ML_TRUE;
				}
				if(lineAType == LineType::ParallelX)
				{
					intersect->x = ((lineAY / lineBM) - lineBC);
					intersect->y = lineAY;
					if(ingnoreLength == ML_FALSE)
					{
						if(((intersect->x < lineA.x) && (intersect->x < lineA.z)) || ((intersect->x > lineA.x) && (intersect->x > lineA.z)))
						{
							return ML_FALSE;
						}
					}
					return ML_TRUE;
				}
				if(lineAType == LineType::Slope)
				{


					intersect->x = (0 - (lineAC - lineBC))/(lineAM - lineBM);
					intersect->y = ((lineAM * intersect->x) + lineAC);
					//printf("Intersect X = %0.f Y = %.0f\n-(c1 - c2) = %.2f, (m1 - m2) = %.2f\n", intersect->x, intersect->y, (0 - (lineBC - lineAC)), (lineAM - lineBM));
					if (ingnoreLength == ML_FALSE)
					{
						ML_BOOL validLineA = ML_FALSE;
						if ((lineA.x < lineA.z) && (intersect->x > lineA.x) && (intersect->x < lineA.z))
						{
							validLineA =  ML_TRUE;
						}
						if((lineA.x > lineA.z) && (intersect->x < lineA.x) && (intersect->x > lineA.z))
						{
							validLineA = ML_TRUE;
						}

						if(validLineA == ML_FALSE)
						{
							return ML_FALSE;
						}

						if((lineB.x < lineB.y) && (intersect->x > lineB.x) && (intersect->x < lineB.z))
						{
							return ML_TRUE;
						}
						if((lineB.x > lineB.y) && (intersect->x < lineB.x) && (intersect->x > lineB.z))
						{
							return ML_TRUE;
						}

						return ML_FALSE;
					}
					return ML_TRUE;

				}
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL FindIntersect(MLVector2<T> lineAStart, MLVector2<T> lineAEnd, MLVector2<T> lineBStart, MLVector2<T> lineBEnd, MLVector2<T>* intersect, ML_BOOL ingnoreLength)
		{
			FindIntersect(MLVector4<T>(lineAStart, lineAEnd), MLVector4<T>(lineBStart, lineBEnd), intersect, ingnoreLength);
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLpointOnLine(MLVector4<T> lineA, MLVector2<T> point)
		{
			MLVector2<T> tempLine = MLVector2(lineA.p2.x - lineA.p1.x, lineA.p2.y - lineA.p1.y);
			MLVector2<T> tempPoint = MLVector2(point.x - lineA.p1.x, point.y - lineA.p1.y);
			T crossVal =  MLVec2::MLcross(tempLine.p2, tempPoint);
			return abs(crossVal) < FLT_EPSILON;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLpointRightOfLine(MLVector4<T> lineA, MLVector2<T> point)
		{
			MLVector2<T> tempLine = MLVector2(lineA.p2.x - lineA.p1.x, lineA.p2.y - lineA.p1.y);
			MLVector2<T> tempPoint = MLVector2(point.x - lineA.p1.x, point.y - lineA.p1.y);
			return MLVec2::MLcross(tempLine.p2, tempPoint) < 0;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLlineSegementsTouch(MLVector4<T> lineA, MLVector4<T> lineB)
		{
			return MLpointOnLine(lineA, lineB.p1) || MLpointOnLine(lineA, lineB.p2) || 
				MLpointRightOfLine(lineA, lineB.p1) ^ MLpointRightOfLine(lineA, lineB.p2);
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL CheckIntersect(MLVector4<T> lineA, MLVector4<T> lineB)
		{
			if(MLVec4::MLboundIntersect(lineA, lineB))
				return MLlineSegementsTouch(lineA, lineB) && MLlineSegementsTouch(lineB, lineA);
			else
				return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL CheckIntersect(MLVector2<T> lineAStart, MLVector2<T> lineAEnd, MLVector2<T> lineBStart, MLVector2<T> lineBEnd)
		{
			MLVector4<T> lineA = MLVector4<T>(lineAStart, lineAEnd);
			MLVector4<T> lineB = MLVector4<T>(lineBStart, lineBEnd);

			if(MLVec4::MLboundIntersect(lineA, lineB))
				return MLlineSegementsTouch(lineA, lineB) && MLlineSegementsTouch(lineB, lineA);
			else
				return ML_FALSE;
		}
	}

}
#endif // MLVECFUNC_H
