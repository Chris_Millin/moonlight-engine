#ifndef MLVECTOR2_H
#define MLVECTOR2_H

#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"

//swizzle defines
//===================================================================
#define xx_swizzle swizzle<0>()
#define yy_swizzle swizzle<1>()
#define yx_swizzle swizzle<1,0>()
#define xy_swizzle swizzle<0,1>()
//===================================================================

namespace MLMath
{
	namespace MLVec2
	{
		//=========================================================
		// Name: Vector 2
		// Description: A vector with two components with uses 
		// templates but is advised to use the typedef versions
		// rather than this directly 
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLVector2
		{
			union
			{
				//=========================================================
				// Name: Vector Values
				// Description: Stores the vector data in an array format
				// Creator: Chris Millin
				//=========================================================
				T v[2];
				struct  
				{
					//=========================================================
					// Name: X
					// Description: x component of the vector (v[0])
					// Creator: Chris Millin
					//=========================================================
					T x;

					//=========================================================
					// Name: Y
					// Description: y component of the vector (v[1])
					// Creator: Chris Millin
					//=========================================================
					T y;
				};
				struct  
				{
					//=========================================================
					// Name: Width
					// Description: Width component of the vector (v[0])
					// Creator: Chris Millin
					//========================================================
					T Width;

					//=========================================================
					// Name: Height
					// Description: Height component of the vector (v[1])
					// Creator: Chris Millin
					//=========================================================
					T Height;
				};
			};

			//////////////////////////////////////////////////////////////////////////
			//Constructor
			//////////////////////////////////////////////////////////////////////////
			MLVector2();
			MLVector2(T inXY);
			MLVector2(T inX, T inY);
			MLVector2(const MLVector2<T> &inVec);

			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector2 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of a scaler to a vector2
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Add Assignment operator 
			// Description: Adds the vector2 to this and then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator+=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Add Assignment operator 
			// Description: Adds the scaler to this vector and then 
			// assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtract Assignment operator 
			// Description: Subtracts the vector2 from this and then 
			// assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator-=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Subtract Assignment operator 
			// Description: Subtracts the scaler from this vector and 
			// then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiply Assignment operator 
			// Description: Multiply the vector with this vector and 
			// then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator*=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Multiply Assignment operator 
			// Description: Multiply the scaler with this vector and 
			// then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Division Assignment operator 
			// Description: Divide the vector with this vector and 
			// then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator/=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Division Assignment operator 
			// Description: Divide the scaler with this vector and 
			// then assigns it
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds a Vector to this and returns it as a
			// new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator+(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds a scaler to this and returns it as a
			// new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Subtracts a Vector to this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator-(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Subtracts a scaler to this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies a Vector to this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator*(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies a scaler to this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a vector with this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator/(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a scaler with this and returns it 
			// as a new vector
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals operator 
			// Description: Checks if Vector is not equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Not Equals operator 
			// Description: Checks if Vector is not equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Less than operator 
			// Description: Checks if this vector is Less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Greater than operator 
			// Description: Checks if this vector is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Less than or Equal operator 
			// Description: Checks if this vector is less than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Greater than or Equal operator 
			// Description: Checks if this vector is greater than
			// or equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLVector2<T> &inVec);
			
			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: Returns the value in the desired index
			// anything less than 0 will be treated as 0 and greater
			// than 1 as 1
			// Creator: Chris Millin
			//=========================================================
			T operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Single Swizzle
			// Description: template swizzle function which is returns
			// a new vector with the vector swizzled
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx>
			MLVector2<T> swizzle();

			//=========================================================
			// Name: Single Swizzle
			// Description: template swizzle function which is returns
			// a new vector with the vector swizzled
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy>
			MLVector2<T> swizzle();

			//=========================================================
			// Name: Length (Magnitude)
			// Description: Calculates the length of a vector
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT length();

			//=========================================================
			// Name: Length (Magnitude)
			// Description: Calculates the length of a vector
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT magnitude() { return length(); }

			//=========================================================
			// Name: Normalise (Unit)
			// Description: Calculates the normalised value of the
			// vector (between -1 and 1)
			// Creator: Chris Millin
			//=========================================================
			void normalise();

			//=========================================================
			// Name: Normalise (Unit)
			// Description: Calculates the normalised value of the
			// vector (between -1 and 1)
			// Creator: Chris Millin
			//=========================================================
			void unit() { normalise(); }

			//=========================================================
			// Name: SetZero
			// Description: Sets the current Vector to 0
			// Creator: Chris Millin
			//=========================================================
			void SetZero() { this->v[0] = 0; this->v[1] = 0; }

			//=========================================================
			// Name: Zero
			// Description: Returns a Zero Vector
			// Creator: Chris Millin
			//=========================================================
			static MLVector2<T> Zero() { return MLVector2<T>(0); }
			
			//=========================================================
			// Name: To Pointer
			// Description: Returns the vector as a pointer. This is
			// Useful for OpenGL
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLVector2<T> &VecA);
		};

		//=========================================================
		// Name: Vector 2 Float
		// Description: Vector 2 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector2<ML_FLOAT>		MLVector2f;

		//=========================================================
		// Name: Vector 2 Int
		// Description: Vector 2 using integer values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector2<ML_INT>		MLVector2i;

		//=========================================================
		// Name: Vector 2 Double
		// Description: Vector 2 using double precision values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector2<ML_DOUBLE>	MLVector2d;
		
		//=========================================================
		// Name: Vector 2 16Bit
		// Description: Vector 2 using a 16bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector2<ML_SHORT>		MLVector2_16;

		//=========================================================
		// Name: Vector 2 8Bit
		// Description: Vector 2 using a 8bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector2<ML_CHAR>		MLVector2_8;


		//================================================================
		//constructors
		//================================================================

		template<typename T>
		ML_MATH_INLINE MLVector2<T>::MLVector2() : x(v[0]), y(v[1]) { v[0] = 0; v[1] = 0; }
		template<typename T>
		ML_MATH_INLINE MLVector2<T>::MLVector2(T inXY) : x(v[0]), y(v[1]) { v[0] = inXY; v[1] = inXY; }
		template<typename T>
		ML_MATH_INLINE MLVector2<T>::MLVector2(T inX, T inY) : x(v[0]), y(v[1]) { v[0] = inX; v[1] = inY; } 
		template<typename T>
		ML_MATH_INLINE MLVector2<T>::MLVector2(const MLVector2<T> &inVec) : x(inVec.v[0]), y(inVec.v[1]) { v[0] = inVec.v[0]; v[1] = inVec.v[1]; }
		
		//=========================================================
		//Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator=(const MLVector2<T> &inVec)
		{
			this->v[0] = inVec.v[0];
			this->v[1] = inVec.v[1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator=(const T &inVal)
		{
			this->v[0] = inVal;
			this->v[1] = inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator+=(const MLVector2<T> &inVec)
		{
			this->v[0] += inVec.v[0];
			this->v[1] += inVec.v[1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator+=(const T &inVal)
		{
			this->v[0] += inVal;
			this->v[1] += inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator-=(const MLVector2<T> &inVec)
		{
			this->v[0] -= inVec.v[0];
			this->v[1] -= inVec.v[1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator-=(const T &inVal)
		{
			this->v[0] -= inVal;
			this->v[1] -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator*=(const MLVector2<T> &inVec)
		{
			this->v[0] *= inVec.v[0];
			this->v[1] *= inVec.v[1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator*=(const T &inVal)
		{
			this->v[0] *= inVal;
			this->v[1] *= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator/=(const MLVector2<T> &inVec)
		{
			this->v[0] /= inVec.v[0];
			this->v[1] /= inVec.v[1];
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T>& MLVector2<T>::operator/=(const T &inVal)
		{
			this->v[0] /= inVal;
			this->v[1] /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator+(const MLVector2<T> &inVec)
		{
			return MLVector2<T>((this->v[0] + inVec.v[0]), (this->v[1] + inVec.v[1]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator+(const T &inVal)
		{
			return MLVector2<T>((this->v[0] + inVal), (this->v[1] + inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator-(const MLVector2<T> &inVec)
		{
			return MLVector2<T>((this->v[0] - inVec.v[0]), (this->v[1] - inVec.v[1]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator-(const T &inVal)
		{
			return MLVector2<T>((this->v[0] - inVal), (this->v[1] - inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator*(const MLVector2<T> &inVec)
		{
			return MLVector2<T>((this->v[0] * inVec.v[0]), (this->v[1] * inVec.v[1]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator*(const T &inVal)
		{
			return MLVector2<T>((this->v[0] * inVal), (this->v[1] * inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator/(const MLVector2<T> &inVec)
		{
			return MLVector2<T>((this->v[0] / inVec.v[0]), (this->v[1] / inVec.v[1]));
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::operator/(const T &inVal)
		{
			return MLVector2<T>((this->v[0] / inVal), (this->v[1] / inVal));
		}

		//=========================================================
		// Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator==(const MLVector2<T> &inVec)
		{
			if (this->v[0] == inVec.x && this->v[1] == inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator!=(const MLVector2<T> &inVec)
		{
			if (this->v[0] != inVec.x && this->v[1] != inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator<(const MLVector2<T> &inVec)
		{
			if (this->v[0] < inVec.x && this->v[1] < inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator>(const MLVector2<T> &inVec)
		{
			if (this->v[0] > inVec.x && this->v[1] > inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator<=(const MLVector2<T> &inVec)
		{
			if (this->v[0] <= inVec.x && this->v[1] <= inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector2<T>::operator>=(const MLVector2<T> &inVec)
		{
			if (this->v[0] >= inVec.x && this->v[1] >= inVec.y)
			{
				return ML_TRUE;
			}

			return ML_FALSE;
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE T MLVector2<T>::operator[](const ML_INT index)
		{
			if(index < 0) index = 0;
			if(index > 1) index = 1;
			return this->v[index];
		}

		//================================================================
		// Vector2 math functions
		//================================================================

		template<typename T>
		template<unsigned ML_INT sx>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::swizzle()
		{
			return MLVector2<T>(this->v[sx > 1 ? 0 :  sx], this->v[sx > 1 ? 1 :  sx]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy>
		ML_MATH_INLINE MLVector2<T> MLVector2<T>::swizzle()
		{
			return MLVector2<T>(this->v[sx > 1 ? 0 :  sx], this->v[sy > 1 ? 1 :  sy]);
		}

		template<typename T>
		ML_MATH_INLINE ML_FLOAT MLVector2<T>::length()
		{
			return sqrtf((this->x * this->x) + (this->y * this->y));
		}

		template<typename T>
		ML_MATH_INLINE void MLVector2<T>::normalise()
		{
			this->x /= length();
			this->y /= length();
		}

		template<typename T>
		ML_MATH_INLINE T* MLVector2<T>::toptr(const MLVector2<T> &VecA)
		{
			return (T*)VecA.v;
		}
	}
}

#endif

