#ifndef MLVector3_H
#define MLVector3_H
#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector2.h"

//swizzle defines
//===================================================================
#define xxx_swizzle swizzle<0>()
#define yyy_swizzle swizzle<1>()
#define zzz_swizzle swizzle<2>()
#define xyz_swizzle swizzle<0,1,2>()
#define zyx_swizzle swizzle<2,1,0>()

#define xxy_swizzle swizzle<0,0,1>()
#define xxz_swizzle swizzle<0,0,2>()
#define xyy_swizzle swizzle<0,1,1>()
#define xyx_swizzle swizzle<0,1,0>()
#define xzy_swizzle swizzle<0,2,1>()
#define xzz_swizzle swizzle<0,2,2>()

#define yxy_swizzle swizzle<1,0,1>()
#define yxz_swizzle swizzle<1,0,2>()
#define yyx_swizzle swizzle<1,1,0>()
#define yyz_swizzle swizzle<1,1,2>()
#define yzy_swizzle swizzle<1,2,1>()
#define yzz_swizzle swizzle<1,2,2>()

#define zxy_swizzle swizzle<2,0,1>()
#define zxz_swizzle swizzle<2,0,2>()
#define zyz_swizzle swizzle<2,1,2>()
#define zzy_swizzle swizzle<2,2,1>()
#define zzx_swizzle swizzle<2,2,0>()
//===================================================================


using namespace MLMath::MLVec2;

namespace MLMath
{
	namespace MLVec3
	{
		//=========================================================
		// Name: Vector 3
		// Description: Definition of a 3 part vector component
		// with a template strucutre. it is advised to use the
		// pre defined typedef structures.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLVector3
		{
			union
			{
				//=========================================================
				// Name: Vector Values
				// Description: Stores the vector data in an array format
				// Creator: Chris Millin
				//=========================================================
				T v[3];
				struct  
				{
					//=========================================================
					// Name: X
					// Description: x component of the vector (v[0])
					// Creator: Chris Millin
					//=========================================================
					T x;

					//=========================================================
					// Name: Y
					// Description: y component of the vector (v[1])
					// Creator: Chris Millin
					//=========================================================
					T y;

					//=========================================================
					// Name: Z
					// Description: z component of the vector (v[2])
					// Creator: Chris Millin
					//=========================================================
					T z;
				};
				struct
				{
					//=========================================================
					// Name: Red
					// Description: Red Colour component of the vector (v[0])
					// Note: Format is RGB using V component to set colour
					// which is in BGR format will not work and will
					// require the zyx_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T r;

					//=========================================================
					// Name: Green
					// Description: Green Colour component of the vector (v[1])
					// Note: Format is RGB using V component to set colour
					// which is in BGR format will not work and will
					// require the zyx_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T g;

					//=========================================================
					// Name: Blue
					// Description: Blue Colour component of the vector (v[2])
					// Note: Format is RGB using V component to set colour
					// which is in BGR format will not work and will
					// require the zyx_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T b;
				};
			};

			//////////////////////////////////////////////////////////////////////////
			//Constructor
			//////////////////////////////////////////////////////////////////////////
			MLVector3();
			MLVector3(ML_FLOAT inXYZ);
			MLVector3(ML_FLOAT inX, ML_FLOAT inY, ML_FLOAT inZ);
			MLVector3(const MLVector2<T> &inVec);
			MLVector3(const MLVector3<T> &inVec);


			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////
			
			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector3 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector2 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of a scaler to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Addition Assignment operator 
			// Description: Adds and assigns a vector3 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator+=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Addition Assignment operator 
			// Description: Adds and assigns a scaler to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtraction Assignment operator 
			// Description: Subtracts and assigns a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator-=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Subtraction Assignment operator 
			// Description: Subtracts and assigns a scaler to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiply Assignment operator 
			// Description: Multiplies and assigns a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator*=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Multiply Assignment operator 
			// Description: Multiplies and assigns a scaler to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Divide Assignment operator 
			// Description: Divide and assigns a vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator/=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Divide Assignment operator 
			// Description: Divide and assigns a scaler to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds two vectors together 
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator+(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds scaler to a vector (vector + scaler) 
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Subtracts two vectors 
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator-(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Subtracts a scaler with a vector 
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies two vectors 
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator*(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies a scaler with a vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a two vectors
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator/(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a scaler with a vector
			// Creator: Chris Millin
			//=========================================================
			MLVector3<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equals operator 
			// Description: check if another vector is equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Not Equals operator 
			// Description: check if another vector is not equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Less Than operator 
			// Description: check if this vector is less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Greater Than operator 
			// Description: check if this vector is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Less Than Equal operator 
			// Description: check if this vector is less than or equal
			// to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Greater Than Equal operator 
			// Description: check if this vector is greater than or equal
			// to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLVector3<T> &inVec);

			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: returns the value in the array index
			// if less than 0 will be treated as 0 if greater than
			// 2 will be treated as 2
			// Creator: Chris Millin
			//=========================================================
			T& operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Swizzle Single
			// Description: swizzles the components with a single
			// component (0 = x, 1 = y, 2 = z)
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx>
			MLVector3<T> swizzle();

			//=========================================================
			// Name: Swizzle double
			// Description: swizzles the components of the x and y
			// z is left as standard
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy>
			MLVector3<T> swizzle();

			//=========================================================
			// Name: Swizzle triple
			// Description: swizzles the components of the x, y and z
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz>
			MLVector3<T> swizzle();


			//=========================================================
			// Name: Length
			// Description: Calculates the length/magnitude of the
			// vector
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT length();

			//=========================================================
			// Name: Magnitude
			// Description: Calculates the length/magnitude of the
			// vector
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT magnitude() { return length(); }

			//=========================================================
			// Name: Normalise
			// Description: Normalises the vector to between (-1 and 1)
			// Creator: Chris Millin
			//=========================================================
			void normalise();

			//=========================================================
			// Name: Unit
			// Description: Normalises the vector to between (-1 and 1)
			// Creator: Chris Millin
			//=========================================================
			void unit() { return normalise(); }

			//=========================================================
			// Name: SetZero
			// Description: Sets the current Vector to 0
			// Creator: Chris Millin
			//=========================================================
			void SetZero() { this->v[0] = 0; this->v[1] = 0; this->v[2] = 0;}

			//=========================================================
			// Name: Zero
			// Description: Returns a Zero Vector
			// Creator: Chris Millin
			//=========================================================
			static MLVector3<T> Zero() { return MLVector3<T>(0); }

			//=========================================================
			// Name: To Pointer
			// Description: Returns the vector as a pointer which is 
			// useful for OpenGL shader uniforms.
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLVector3<T>& VecA);
		};
	
		//=========================================================
		// Name: Vector 3 Float
		// Description: Vector 3 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector3<ML_FLOAT>		MLVector3f;

		//=========================================================
		// Name: Vector 3 Int
		// Description: Vector 3 using integer values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector3<ML_INT>		MLVector3i;

		//=========================================================
		// Name: Vector 3 Double
		// Description: Vector 3 using double precision values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector3<ML_DOUBLE>	MLVector3d;

		//=========================================================
		// Name: Vector 3 16Bit
		// Description: Vector 3 using a 16bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector3<ML_SHORT>		MLVector3_16;

		//=========================================================
		// Name: Vector 3 8Bit
		// Description: Vector 3 using a 8bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector3<ML_CHAR>		MLVector3_8;


		//================================================================
		//constructors
		//================================================================

		template<typename T>
		ML_MATH_INLINE MLVector3<T>::MLVector3() : x(v[0]), y(v[1]), z(v[2]) { v[0] = 0.0f; v[1] = 0.0f; v[2] = 0.0f; }
		template<typename T>
		ML_MATH_INLINE MLVector3<T>::MLVector3(ML_FLOAT inXYZ) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inXYZ; v[1] = inXYZ; v[2] = inXYZ;}
		template<typename T>
		ML_MATH_INLINE MLVector3<T>::MLVector3(ML_FLOAT inX, ML_FLOAT inY, ML_FLOAT inZ) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inX; v[1] = inY; v[2] = inZ;}
		template<typename T>
		ML_MATH_INLINE MLVector3<T>::MLVector3(const MLVector2<T> &inVec) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inVec.x; v[1] = inVec.y; v[2] = 0;}
		template<typename T>
		ML_MATH_INLINE MLVector3<T>::MLVector3(const MLVector3<T> &inVec) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inVec.x; v[1] = inVec.y; v[2] = inVec.z;}

		//=========================================================
		//Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator =(const MLVector3<T> &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = inVec.z;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator =(const MLVector2<T> &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator =(const T &inVal)
		{
			this->x = inVal;
			this->y = inVal;
			this->z = inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator+=(const MLVector3<T> &inVec)
		{
			this->x += inVec.x;
			this->y += inVec.y;
			this->z += inVec.z;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator+=(const T &inVal)
		{
			this->x += inVal;
			this->y += inVal;
			this->z += inVal;
			return *this;
		}  

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator-=(const MLVector3<T> &inVec)
		{
			this->x -= inVec.x;
			this->y -= inVec.y;
			this->z -= inVec.z;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator-=(const T &inVal)
		{
			this->x -= inVal;
			this->y -= inVal;
			this->z -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator*=(const MLVector3<T> &inVec)
		{
			this->x *= inVec.x;
			this->y *= inVec.y;
			this->z *= inVec.z;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator*=(const T &inVal)
		{
			this->x *= inVal;
			this->y *= inVal;
			this->z *= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator/=(const MLVector3<T> &inVec)
		{
			this->x /= inVec.x;
			this->y /= inVec.y;
			this->z /= inVec.z;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T>& MLVector3<T>::operator/=(const T &inVal)
		{
			this->x /= inVal;
			this->y /= inVal;
			this->z /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator+(const MLVector3<T> &inVec)
		{
			return MLVector3<T>((this->x + inVec.x), (this->y + inVec.y), (this->z + inVec.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator+(const T &inVal)
		{
			return MLVector3<T>((this->x + inVal), (this->y + inVal), (this->z + inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator-(const MLVector3<T> &inVec)
		{
			return MLVector3<T>((this->x - inVec.x), (this->y - inVec.y), (this->z - inVec.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator-(const T &inVal)
		{
			return MLVector3<T>((this->x - inVal), (this->y - inVal), (this->z - inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator*(const MLVector3<T> &inVec)
		{
			return MLVector3<T>((this->x * inVec.x), (this->y * inVec.y), (this->z * inVec.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator*(const T &inVal)
		{
			return MLVector3<T>((this->x * inVal), (this->y * inVal),  (this->z * inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator/(const MLVector3<T> &inVec)
		{
			return MLVector3<T>((this->x / inVec.x), (this->y / inVec.y), (this->z / inVec.z));
		}

		template<typename T>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::operator/(const T &inVal)
		{
			return MLVector3<T>((this->x / inVal), (this->y / inVal), (this->z / inVal));
		}

		//=========================================================
		//Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator==(const MLVector3<T> &inVec)
		{
			return this->x == inVec.x && this->y == inVec.y && this->z == inVec.z ? ML_TRUE : ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator!=(const MLVector3<T> &inVec)
		{
			return this->x != inVec.x && this->y != inVec.y && this->z != inVec.z ? ML_TRUE : ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator<(const MLVector3<T> &inVec)
		{
			return this->x < inVec.x && this->y < inVec.y && this->z < inVec.z ? ML_TRUE : ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator>(const MLVector3<T> &inVec)
		{
			return this->x > inVec.x && this->y > inVec.y && this->z > inVec.z ? ML_TRUE : ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator<=(const MLVector3<T> &inVec)
		{
			return this->x <= inVec.x && this->y <= inVec.y && this->z <= inVec.z ? ML_TRUE : ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector3<T>::operator>=(const MLVector3<T> &inVec)
		{
			return this->x >= inVec.x && this->y >= inVec.y && this->z >= inVec.z ? ML_TRUE : ML_FALSE;
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE T& MLVector3<T>::operator[](const ML_INT index)
		{
			ML_UINT i = index;
			if(i < 0) i = 0;
			if(i > 2) i = 2;
			return this->v[i];
		}

		//================================================================
		//vector3 math functions
		//================================================================

		template<typename T>
		template<unsigned ML_INT sx>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::swizzle()
		{
			return sx > 2 ?  MLVector3<T>(this->v[0], this->v[0], this->v[0]) : MLVector3<T>(this->v[sx], this->v[sx], this->v[sx]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::swizzle()
		{
			return MLVector3<T>(this->v[sx > 2 ? 0 :  sx], this->v[sy > 2 ? 0 :  sy], this->v[2]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz>
		ML_MATH_INLINE MLVector3<T> MLVector3<T>::swizzle()
		{
			return MLVector3<T>(this->v[sx > 2 ? 0 :  sx], this->v[sy > 2 ? 1 :  sy], this->v[sz > 2 ? 2 :  sz]);
		}

		template<typename T>
		ML_MATH_INLINE ML_FLOAT MLVector3<T>::length()
		{
			return sqrtf((this->x * this->y) + (this->y * this->y) + (this->z * this->z));
		}

		template<typename T>
		ML_MATH_INLINE void MLVector3<T>::normalise()
		{
			this->x /= (T)length();
			this->y /= (T)length();
			this->z /= (T)length();
		}

		template<typename T>
		ML_MATH_INLINE T* MLVector3<T>::toptr(const MLVector3<T> &VecA)
		{
			return (T*)VecA.v;
		}
	}
}

#endif