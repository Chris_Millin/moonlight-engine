#ifndef  MLVector4_H
#define  MLVector4_H
#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
#include "MLMath/Vector/MLVector2.h"
#include "MLMath/Vector/MLVector3.h"


//swizzle defines
//===================================================================
#define xxxx_swizzle swizzle<0>()
#define yyyy_swizzle swizzle<1>()
#define zzzz_swizzle swizzle<2>()
#define wwww_swizzle swizzle<3>()
#define zyxw_swizzle swizzle<2,1,0,3>()
#define xyzw_swizzle swizzle<0,1,2,3>()
#define wzyx_swizzle swizzle<3,2,1,0>()
#define xwzy_swizzle swizzle<0,3,2,1>()
#define xzyw_swizzle swizzle<0,2,1,3>()
#define wyzx_swizzle swizzle<3,1,2,0>()
//===================================================================

using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;

namespace MLMath
{
	namespace MLVec4
	{
		//=========================================================
		// Name: Vector 4
		// Description: Definition of a 4 part vector component
		// with a template structure. it is advised to use the
		// pre defined typedef structures.
		// Creator: Chris Millin
		//=========================================================
		template<typename T>
		struct MLVector4
		{
			union
			{
				//=========================================================
				// Name: Vector Values
				// Description: Stores the vector data in an array format
				// Creator: Chris Millin
				//=========================================================
				T v[4];
				struct  
				{
					//=========================================================
					// Name: X
					// Description: x component of the vector (v[0])
					// Creator: Chris Millin
					//=========================================================
					T x;

					//=========================================================
					// Name: Y
					// Description: y component of the vector (v[1])
					// Creator: Chris Millin
					//=========================================================
					T y;

					//=========================================================
					// Name: Z
					// Description: z component of the vector (v[2])
					// Creator: Chris Millin
					//=========================================================
					T z;

					//=========================================================
					// Name: W
					// Description: w component of the vector (v[3])
					// Creator: Chris Millin
					//=========================================================
					T w;
				};
				struct
				{
					//=========================================================
					// Name: X
					// Description: x component of the vector (v[0])
					// for bounding box
					// Creator: Chris Millin
					//=========================================================
					T x;

					//=========================================================
					// Name: Y
					// Description: y component of the vector (v[1])
					// for bounding box
					// Creator: Chris Millin
					//=========================================================
 					T y;

					//=========================================================
					// Name: Width
					// Description: Width component of the vector (v[2])
					// Creator: Chris Millin
					//=========================================================
					T Width;

					//=========================================================
					// Name: Height
					// Description: Height component of the vector (v[3])
					// Creator: Chris Millin
					//=========================================================
					T Height;
				};
				struct
				{
					//=========================================================
					// Name: Red
					// Description: Red Colour component of the vector (v[0])
					// Note: Format is RGBA using V component to set colour
					// which is in BGRA format will not work and will
					// require the zyxw_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T r;

					//=========================================================
					// Name: Green
					// Description: Green Colour component of the vector (v[1])
					// Note: Format is RGBA using V component to set colour
					// which is in BGRA format will not work and will
					// require the zyxw_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T g;

					//=========================================================
					// Name: Blue
					// Description: Blue Colour component of the vector (v[2])
					// Note: Format is RGBA using V component to set colour
					// which is in BGRA format will not work and will
					// require the zyxw_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T b;

					//=========================================================
					// Name: Alpha
					// Description: Alpha Colour component of the vector (v[3])
					// Note: Format is RGBA using V component to set colour
					// which is in BGRA format will not work and will
					// require the zyxw_swizzle to format to BGR
					// Creator: Chris Millin
					//=========================================================
					T a;
				};
				struct
				{
					//=========================================================
					// Name: X 1
					// Description: X 1 component of the vector (v[0]) for
					// use of defining two points in the vector4
					// Creator: Chris Millin
					//=========================================================
					T x1;

					//=========================================================
					// Name: Y 1
					// Description: Y 1 component of the vector (v[1]) for
					// use of defining two points in the vector4
					// Creator: Chris Millin
					//=========================================================
					T y1;

					//=========================================================
					// Name: X 2
					// Description: X 2 component of the vector (v[2]) for
					// use of defining two points in the vector4
					// Creator: Chris Millin
					//=========================================================
					T x2;

					//=========================================================
					// Name: Y 2
					// Description: Y 2 component of the vector (v[3]) for
					// use of defining two points in the vector4
					// Creator: Chris Millin
					//=========================================================
					T y2;
				};
				struct
				{
					//=========================================================
					// Name: Point 1
					// Description: Vector 2 Point component of the vector
					// will contain 2 parts of v (v[0], v[1])
					// Creator: Chris Millin
					//=========================================================
					MLVector2<T> p1;

					//=========================================================
					// Name: Point 2
					// Description: Vector 2 Point component of the vector
					// will contain 2 parts of v (v[2], v[3])
					// Creator: Chris Millin
					//=========================================================
					MLVector2<T> p2;
				};
				struct
				{
					//=========================================================
					// Name: x min
					// Description: Minimum value of x in a bounding box
					// Creator: Chris Millin
					//=========================================================
					T xmin;

					//=========================================================
					// Name: y min
					// Description: Minimum value of y in a bounding box
					// Creator: Chris Millin
					//=========================================================
					T ymin;

					//=========================================================
					// Name: x max
					// Description: Maximum value of x in a bounding box
					// Creator: Chris Millin
					//=========================================================
					T xmax;

					//=========================================================
					// Name: y max
					// Description: Maximum value of y in a bounding box
					// Creator: Chris Millin
					//=========================================================
					T ymax;
				};
			};
			

			//constructors
			MLVector4();
			MLVector4(T inXYZW);
			MLVector4(T inX, T inY, T inZ, T inW);
			MLVector4(const MLVector2<T> &inVecA, const MLVector2<T> &inVecB);
			MLVector4(const MLVector4<T> &inVec);

			//////////////////////////////////////////////////////////////////////////
			//Mathematical Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector4 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector3 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator=(const MLVector3<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of one vector2 to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator=(const MLVector2<T> &inVec);

			//=========================================================
			// Name: Assignment operator 
			// Description: Assignment of scaler to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator=(const T &inVal);

			//=========================================================
			// Name: Addition Assignment operator 
			// Description: Addition and assignment of vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator+=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Addition Assignment operator 
			// Description: Addition and assignment of scaler to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator+=(const T &inVal);

			//=========================================================
			// Name: Subtraction Assignment operator 
			// Description: Subtraction and assignment of Vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator-=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Subtraction Assignment operator 
			// Description: Subtraction and assignment of scaler to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator-=(const T &inVal);

			//=========================================================
			// Name: Multiplication Assignment operator 
			// Description: Multiply and assignment of Vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator*=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Multiplication Assignment operator 
			// Description: Multiply and assignment of scaler to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator*=(const T &inVal);

			//=========================================================
			// Name: Division Assignment operator 
			// Description: Divides and assignment of Vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator/=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Division Assignment operator 
			// Description: Divides and assignment of Vector to this 
			// vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T>& operator/=(const T &inVal);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator+(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Addition operator 
			// Description: Adds a scaler to this vectors together
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator+(const T &inVal);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Adds a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator-(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Subtraction operator 
			// Description: Subtracts a scaler to this vectors together
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator-(const T &inVal);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator*(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Multiplication operator 
			// Description: Multiplies a scaler to this vectors together
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator*(const T &inVal);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a vector to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator/(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Division operator 
			// Description: Divides a Scaler to this vector
			// Creator: Chris Millin
			//=========================================================
			MLVector4<T> operator/(const T &inVal);

			//////////////////////////////////////////////////////////////////////////
			//Boolean Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Equal operator 
			// Description: Checks if Vector is equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator==(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Not Equal operator 
			// Description: Checks if Vector is not equal to this
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator!=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Less Than operator 
			// Description: Checks if this vector is less than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Greater Than operator 
			// Description: Checks if this vector is greater than another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Less Than Equal operator 
			// Description: Checks if this vector is less than or
			// equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator<=(const MLVector4<T> &inVec);

			//=========================================================
			// Name: Greater Than Equal operator 
			// Description: Checks if this vector is greater or
			// equal to another
			// Creator: Chris Millin
			//=========================================================
			ML_BOOL operator>=(const MLVector4<T> &inVec);

			//////////////////////////////////////////////////////////////////////////
			//Generic Operators
			//////////////////////////////////////////////////////////////////////////

			//=========================================================
			// Name: Index operator 
			// Description: returns the value in the array index
			// if less than 0 will be treated as 0 if greater than
			// 23 will be treated as 23
			// Creator: Chris Millin
			//=========================================================
			T& operator[](const ML_INT index);

			//////////////////////////////////////////////////////////////////////////
			//Methods
			//////////////////////////////////////////////////////////////////////////


			//=========================================================
			// Name: Swizzle Single 
			// Description: Single component swizzle
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx>
			MLVector4<T> swizzle();

			//=========================================================
			// Name: Swizzle Double
			// Description: Double component swizzle
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy>
			MLVector4<T> swizzle();

			//=========================================================
			// Name: Swizzle Triple 
			// Description: Trple component swizzle
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz>
			MLVector4<T> swizzle();

			//=========================================================
			// Name: Swizzle Quadruple  
			// Description: Quadruple component swizzle
			// Creator: Chris Millin
			//=========================================================
			template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz, unsigned ML_INT sw>
			MLVector4<T> swizzle();

			//=========================================================
			// Name: Length 
			// Description: Length of the vector4
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT length();

			//=========================================================
			// Name: Magnitude 
			// Description: Length of the vector4
			// Creator: Chris Millin
			//=========================================================
			ML_FLOAT magnitude() { return length(); }

			//=========================================================
			// Name: Normalise 
			// Description: Normalise the vector to be between -1 and 1
			// Creator: Chris Millin
			//=========================================================
			void normalise();

			//=========================================================
			// Name: Unit
			// Description: Normalise the vector to be between -1 and 1
			// Creator: Chris Millin
			//=========================================================
			void unit() { return normalise(); }

			//=========================================================
			// Name: Area 
			// Description: Calculate the area of the vector if its
			// used to describe a square
			// Creator: Chris Millin
			//=========================================================
			T area();

			//=========================================================
			// Name: Middle 
			// Description: Find the mid point of the vector if its
			// used to describe a square
			// Creator: Chris Millin
			//=========================================================
			MLVector2<T> middle();

			//=========================================================
			// Name: Resize 
			// Description: Changes the width and height to the scaler
			// value
			// Creator: Chris Millin
			//=========================================================
			void resize(const T &inVal);

			//=========================================================
			// Name: Resize 
			// Description: Changes the width and height to the vector
			// value
			// Creator: Chris Millin
			//=========================================================
			void resize(const MLVector2<T> &inVec);

			//=========================================================
			// Name: SetZero
			// Description: Sets the current Vector to 0
			// Creator: Chris Millin
			//=========================================================
			void SetZero() { this->v[0] = 0; this->v[1] = 0; this->v[2] = 0; this->v[3] = 0; }

			//=========================================================
			// Name: Zero
			// Description: Returns a Zero Vector
			// Creator: Chris Millin
			//=========================================================
			static MLVector4<T> Zero() { return MLVector4<T>(0); }

			//=========================================================
			// Name: To Pointer
			// Description: Returns the vector as a pointer which is 
			// useful for OpenGL shader uniforms.
			// Creator: Chris Millin
			//=========================================================
			static T* toptr(const MLVector4<T>& VecA);
		};
		
		//=========================================================
		// Name: Vector 4 Float
		// Description: Vector 4 using floating point
		// data
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector4<ML_FLOAT>		MLVector4f;

		//=========================================================
		// Name: Vector 4 Int
		// Description: Vector 4 using integer values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector4<ML_INT>		MLVector4i;

		//=========================================================
		// Name: Vector 4 Double
		// Description: Vector 4 using double precision values
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector4<ML_DOUBLE>	MLVector4d;

		//=========================================================
		// Name: Vector 4 16Bit
		// Description: Vector 4 using a 16bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector4<ML_SHORT>		MLVector4_16;

		//=========================================================
		// Name: Vector 4 8Bit
		// Description: Vector 4 using a 8bit short
		// Creator: Chris Millin
		//=========================================================
		typedef MLVector4<ML_CHAR>		MLVector4_8;


		//================================================================
		//constructors
		//================================================================
		template<typename T>
		ML_MATH_INLINE MLVector4<T>::MLVector4() : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) { v[0] = 0; v[1] = 0; v[2] = 0; v[3] = 0; }
		template<typename T>
		ML_MATH_INLINE MLVector4<T>::MLVector4(T inXYZW) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) {v[0] = inXYZW; v[1] = inXYZW; v[2] = inXYZW; v[3] = inXYZW;}
		template<typename T>
		ML_MATH_INLINE MLVector4<T>::MLVector4(T inX, T inY, T inZ,  T inW) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) {v[0] = inX; v[1] = inY; v[2] = inZ; v[3] = inW;} 
		template<typename T>
		ML_MATH_INLINE MLVector4<T>::MLVector4(const MLVector2<T> &inVecA, const MLVector2<T> &inVecB) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) {v[0] = inVecA.x; v[1] = inVecA.y; v[2] = inVecB.x; v[3] = inVecB.y;}
		template<typename T>
		ML_MATH_INLINE MLVector4<T>::MLVector4(const MLVector4<T>& inVec) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) { v[0] = inVec.v[0]; v[1] = inVec.v[1]; v[2] = inVec.v[2]; v[3] = inVec.v[3]; }

		//=========================================================
		//Mathematic Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator =(const MLVector4<T> &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = inVec.z;
			this->w = inVec.w;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator =(const MLVector3<T> &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = inVec.z;
			this->w = 0.0f;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator =(const MLVector2<T> &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = 0.0f;
			this->w = 0.0f;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator =(const T &inVal)
		{
			this->x = inVal;
			this->y = inVal;
			this->z = inVal;
			this->w = inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator+=(const MLVector4<T> &inVec)
		{
			this->x += inVec.x;
			this->y += inVec.y;
			this->z += inVec.z;
			this->w += inVec.w;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator+=(const T &inVal)
		{
			this->x += inVal;
			this->y += inVal;
			this->z += inVal;
			this->w += inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator-=(const MLVector4<T> &inVec)
		{
			this->x -= inVec.x;
			this->y -= inVec.y;
			this->z -= inVec.z;
			this->w -= inVec.w;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator-=(const T &inVal)
		{
			this->x -= inVal;
			this->y -= inVal;
			this->z -= inVal;
			this->w -= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator*=(const MLVector4<T> &inVec)
		{
			this->x *= inVec.x;
			this->y *= inVec.y;
			this->z *= inVec.z;
			this->w *= inVec.w;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator*=(const T &inVal)
		{
			this->x *= inVal;
			this->y *= inVal;
			this->z *= inVal;
			this->w *= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator/=(const MLVector4<T> &inVec)
		{
			this->x /= inVec.x;
			this->y /= inVec.y;
			this->z /= inVec.z;
			this->w /= inVec.w;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T>& MLVector4<T>::operator/=(const T &inVal)
		{
			this->x /= inVal;
			this->y /= inVal;
			this->z /= inVal;
			this->w /= inVal;
			return *this;
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator+(const MLVector4<T> &inVec)
		{
			return MLVector4<T>((this->x + inVec.x), (this->y + inVec.y), (this->z + inVec.z), (this->w + inVec.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator+(const T &inVec)
		{
			return MLVector4<T>((this->x + inVal), (this->y + inVal), (this->z + inVal), (this->w + inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator-(const MLVector4<T> &inVec)
		{
			return MLVector4<T>((this->x - inVec.x), (this->y - inVec.y), (this->z - inVec.z), (this->w - inVec.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator-(const T &inVal)
		{
			return MLVector4<T>((this->x - inVal), (this->y - inVal), (this->z - inVal), (this->w - inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator*(const MLVector4<T> &inVec)
		{
			return MLVector4<T>((this->x * inVec.x), (this->y * inVec.y), (this->z * inVec.z), (this->w * inVec.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator*(const T &inVal)
		{
			return MLVector4<T>((this->x * inVal), (this->y * inVal), (this->z * inVal), (this->w * inVal));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator/(const MLVector4<T> &inVec)
		{
			return MLVector4<T>((this->x / inVec.x), (this->y / inVec.y), (this->z / inVec.z), (this->w / inVec.w));
		}

		template<typename T>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::operator/(const T &inVal)
		{
			return MLVector4<T>(( this->x / inVal), (this->y / inVal), (this->z / inVal), (this->w / inVal));
		}

		//=========================================================
		//Boolean Operators
		//=========================================================

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator==(const MLVector4<T> &inVec)
		{
			if (this->x == inVec.x && this->y == inVec.y && this->z == inVec.z && this->w == inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator!=(const MLVector4<T> &inVec)
		{
			if (this->x != inVec.x && this->y != inVec.y && this->z != inVec.z && this->w != inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator<(const MLVector4<T> &inVec)
		{
			if (this->x < inVec.x && this->y < inVec.y && this->z < inVec.z && this->w < inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator>(const MLVector4<T> &inVec)
		{
			if (this->x > inVec.x && this->y > inVec.y && this->z > inVec.z && this->w > inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator<=(const MLVector4<T> &inVec)
		{
			if (this->x <= inVec.x && this->y <= inVec.y && this->z <= inVec.z && this->w <= inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		template<typename T>
		ML_MATH_INLINE ML_BOOL MLVector4<T>::operator>=(const MLVector4<T> &inVec)
		{
			if (this->x >= inVec.x && this->y >= inVec.y && this->z >= inVec.z && this->w >= inVec.w)
				return ML_TRUE;

			return ML_FALSE;
		}

		//================================================================
		// Generic Operators
		//================================================================

		template<typename T>
		ML_MATH_INLINE T& MLVector4<T>::operator[](const ML_INT index)
		{
			if(index < 0) index = 0;
			if(index > 3) index = 3;
			return &this->v[index];
		}

		//=========================================================
		//vector4 math functions
		//=========================================================

		template<typename T>
		template<unsigned ML_INT sx>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::swizzle()
		{
			return sx > 2 ?  MLVector4<T>(this->v[0], this->v[0], this->v[0], this->v[0]) : MLVector4<T>(this->v[sx], this->v[sx], this->v[sx], this->v[sx]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::swizzle()
		{
			return MLVector4<T>(this->v[sx > 3 ? 0 :  sx], this->v[sy > 3 ? 1 :  sy], this->v[2],  this->v[3]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::swizzle()
		{
			return MLVector4<T>(this->v[sx > 3 ? 0 :  sx], this->v[sy > 3 ? 1 :  sy], this->v[sz > 3 ? 2 :  sz],  this->v[3]);
		}

		template<typename T>
		template<unsigned ML_INT sx, unsigned ML_INT sy, unsigned ML_INT sz, unsigned ML_INT sw>
		ML_MATH_INLINE MLVector4<T> MLVector4<T>::swizzle()
		{
			return MLVector4<T>(this->v[sx > 3 ? 0 :  sx], this->v[sy > 3 ? 1 :  sy], this->v[sz > 3 ? 2 :  sz],  this->v[sw > 3 ? 3 :  sw]);
		}

		template<typename T>
		ML_MATH_INLINE ML_FLOAT MLVector4<T>::length()
		{
			return sqrtf((this->x * this->x) + (this->y * this->y) + (this->z * this->z) + (this->w * this->w));
		}

		template<typename T>
		ML_MATH_INLINE void MLVector4<T>::normalise()
		{
			this->x /= (T)length();
			this->y /= (T)length();
			this->z /= (T)length();
			this->w /= (T)length();
		}

		template<typename T>
		ML_MATH_INLINE T MLVector4<T>::area()
		{
			return this->Width * this->Height;
		}

		template<typename T>
		ML_MATH_INLINE MLVector2<T> MLVector4<T>::middle()
		{
			return MLVector2<T>(((this->Width/2) + this->x), ((this->Width/2) + this->y));
		}

		template<typename T>
		ML_MATH_INLINE void MLVector4<T>::resize(const T &inVal)
		{
			this->Width		= inVal;
			this->Height	= inVal;
		}

		template<typename T>
		ML_MATH_INLINE void MLVector4<T>::resize(const MLVector2<T> &inVec)
		{
			this->Width		= inVec.x;
			this->Height	= inVec.y;
		}

		template<typename T>
		ML_MATH_INLINE T* MLVector4<T>::toptr(const MLVector4<T> &VecA)
		{
			return (T*)VecA.v;
		}
	}
}


#endif // MLVector4_H
