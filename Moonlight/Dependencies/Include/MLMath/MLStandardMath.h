#ifndef MLSTANDARDMATH_H
#define MLSTANDARDMATH_H

#include "MLMath/MLMathDefine.h"
#include <math.h>
#include <float.h>
#define ML_PI 3.1415926536897932384626
#define ML_Rad2Deg(r) (r * (180/ML_PI))
#define ML_Deg2Rad(d) (d * (ML_PI/180))
#define ML_MIN(a, b) (((a) < (b)) ? (a) : (b))
#define ML_MAX(a, b) (((a) > (b)) ? (a) : (b))

namespace MLMath
{
	namespace MLStandard
	{
		ML_MATH_INLINE ML_INT MLfactorial(ML_INT x);

		ML_MATH_INLINE ML_INT MLfactorial(ML_INT x)
		{
			return (x == 1 ? x : x * MLfactorial(x - 1));
		}

		ML_MATH_INLINE ML_FLOAT MLinvert(ML_FLOAT x)
		{
			return 1.0f / x;
		}

		ML_MATH_INLINE ML_FLOAT MLclamp(ML_FLOAT val, ML_FLOAT minVal, ML_FLOAT maxVal)
		{
			if(val > maxVal) val = maxVal; return val;
			if(val < minVal) val = minVal; return val;
			return val;
		}

		ML_MATH_INLINE ML_UINT MLlog2(ML_UINT x)
		{
			ML_UINT ans = 0;
			while( x>>=1 ) ans++;
			return ans ;
		}
	}
}
#endif

