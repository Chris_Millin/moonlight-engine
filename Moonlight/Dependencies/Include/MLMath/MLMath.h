#ifndef MLMATH_H
#define MLMATH_H

#include "MLMath/Vector/MLVecFunc.h"
#include "MLMath/Vector/MLVector2.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector4.h"


#include "MLMath/Matrix/MLMat4.h"
#include "MLMath/Matrix/MLMat3.h"
#include "MLMath/Matrix/MLMat2.h"
#include "MLMath/Matrix/MLMatFunc.h"

#endif