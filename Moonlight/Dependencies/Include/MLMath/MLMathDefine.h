#ifndef MLMATHDEFINE_H
#define MLMATHDEFINE_H


#ifndef ML_FORCE_INLINE
	#define ML_MATH_INLINE inline
#else
	#define ML_MATH_INLINE __forceinline
#endif

#define ML_FLOAT	float
#define ML_DOUBLE	double
#define ML_INT		int
#define ML_UINT		unsigned int
#define ML_SHORT	short
#define ML_USHORT	unsigned short
#define ML_CHAR		char
#define ML_UCHAR	unsigned char
#define ML_BOOL		bool
#define ML_TRUE		true
#define ML_FALSE	false
#define ML_QWORD	unsigned long long
#define ML_64		long long
#define ML_DWORD	unsigned long
#define ML_32		long long
#define ML_WORD		unsigned short
#define ML_16		short
#define ML_HWORD	unsigned char
#define ML_8		char
#define ML_BYTE		unsigned char


#pragma warning(disable : 4482)
//This should be re-enabled at some point as
//it checks for implicit basic type conversion which
//could lead to some errors
#pragma warning(disable : 4800)

#endif