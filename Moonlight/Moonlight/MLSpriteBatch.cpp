#include "MLSpriteBatch.h"

using MLRenderer::MLSpriteBatch;

//Sprite Batch Constructors
//=================================================================== 
#pragma region Constructors
//=========================================================
// Name: MLSpriteBatch Constructor
// Description: This is one of the default constructors 
// for the sprite batch. All that really is done here is the
// shaders are loaded and compiled.
// Creator: Chris Millin
//=========================================================
MLCoreDLL MLSpriteBatch::MLSpriteBatch() : m_defaultShader(true), m_BufferSize(ML_BATCH_BUFFER_SIZE), m_spriteSize((sizeof(MLFormats::MLVertexPosTexCol) * 6))
{
	//set the default search mode to texured sort
	m_sortMode = MLFormats::MLSortMode::Texture;
	//create a new shader object
	m_shader = new MLShader();
	//load the default sprite batch shaders
	m_shader->LoadShaderFile("shaders\\SBatch", GL_VERTEX_SHADER);
	m_shader->LoadShaderFile("shaders\\SBatch", GL_GEOMETRY_SHADER);
	m_shader->LoadShaderFile("shaders\\SBatch", GL_FRAGMENT_SHADER);
	//lock the shader in place to add the attributes and link
	m_shader->Lock();
	{
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXPOS,	"VertexPosition");
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXCOL,	"VertexColour");
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_TEXTURE0,		"TextureCoord");

		m_shader->LinkShaders();
	}
	m_shader->Unlock(); //unlock the shader 

	//create the shader for rendering point primitives
	m_primitiveShader = new MLShader();
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_VERTEX_SHADER);
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_FRAGMENT_SHADER);
	//lock the shader in place to add the attributes and link
	m_primitiveShader->Lock();
	{
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXPOS,	"VertexPosition");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXCOL,	"VertexColour");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_TEXTURE0,	"TextureCoord");

		m_primitiveShader->LinkShaders();
	}
	m_primitiveShader->Unlock(); //unlock the shader 


	m_beginCall = false;
	MLError::ErrorHandler::ML_Printf(ML_SpriteBatch, "Creating batch\n");

	//initalise the batcher
	Initalise();

	MLError::ErrorHandler::ML_Printf(ML_SpriteBatch, "Created Sprite Batch\n");
}

//=========================================================
// Name: MLSpriteBatch Constructor
// Description: Same as the default constructor but the
// vertex buffer size can be specified which could be useful
// on systems which have low graphics memory.
// Creator: Chris Millin
//=========================================================
MLCoreDLL MLSpriteBatch::MLSpriteBatch(int buffersize) : m_defaultShader(true), m_BufferSize(buffersize), m_spriteSize((sizeof(MLFormats::MLVertexPosTexCol) * 6))
{
	//set the default search mode to texured sort
	m_sortMode = MLFormats::MLSortMode::Texture;
	//create a new shader object
	m_shader = new MLShader();
	//load the default sprite batch shaders
	m_shader->LoadShaderFile("shaders\\SBatch", GL_VERTEX_SHADER);
	m_shader->LoadShaderFile("shaders\\SBatch", GL_GEOMETRY_SHADER);
	m_shader->LoadShaderFile("shaders\\SBatch", GL_FRAGMENT_SHADER);
	//lock the shader in place to add the attributes and link
	m_shader->Lock();
	{
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXPOS,	"VertexPosition");
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXCOL,	"VertexColour");
		m_shader->BindAtt(MLRenderer::MLAttID::ML_ATT_TEXTURE0,		"TextureCoord");
		
		m_shader->LinkShaders();
	}

	m_shader->Unlock(); //unlock the shader 

	//create the shader for rendering point primitives
	m_primitiveShader = new MLShader();
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_VERTEX_SHADER);
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_FRAGMENT_SHADER);
	//lock the shader in place to add the attributes and link
	m_primitiveShader->Lock();
	{
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXPOS,	"VertexPosition");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXCOL,	"VertexColour");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_TEXTURE0,	"TextureCoord");

		m_primitiveShader->LinkShaders();
	}
	m_primitiveShader->Unlock(); //unlock the shader 

	m_beginCall = false;
	MLError::ErrorHandler::ML_Printf(ML_SpriteBatch, "Creating batch\n");

	//initalise the batcher
	Initalise();

	MLError::ErrorHandler::ML_Printf(ML_SpriteBatch, "Created Sprite Batch\n");
}

//=========================================================
// Name: MLSpriteBatch
// Description: Same as other constructor but allows for
// a shader and buffer size to be specified. (Only
// sets the sprite batch shader)
// Creator: Chris Millin
//=========================================================
MLCoreDLL MLSpriteBatch::MLSpriteBatch(MLShader* inShader, int buffersize) : m_defaultShader(false), m_shader(inShader), m_BufferSize(buffersize), m_spriteSize((sizeof(MLFormats::MLVertexPosTexCol) * 6))
{
	//set the default search mode to texured sort
	m_sortMode = MLFormats::MLSortMode::Texture;

	//create the shader for rendering point primitives
	m_primitiveShader = new MLShader();
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_VERTEX_SHADER);
	m_primitiveShader->LoadShaderFile("shaders\\GBatch", GL_FRAGMENT_SHADER);
	//lock the shader in place to add the attributes and link
	m_primitiveShader->Lock();
	{
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXPOS,	"VertexPosition");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_VERTEXCOL,	"VertexColour");
		m_primitiveShader->BindAtt(MLRenderer::MLAttID::ML_ATT_TEXTURE0,	"TextureCoord");

		m_primitiveShader->LinkShaders();
	}
	m_primitiveShader->Unlock(); //unlock the shader 

	m_beginCall = false;

	Initalise();
}
#pragma endregion Constructors
//===================================================================

//Release and destructor
//===================================================================
#pragma region Destruction
//=========================================================
// Name:
// Description:
// Creator:
//=========================================================
MLCoreDLL MLSpriteBatch::~MLSpriteBatch(void)
{
	Release();
}

//=========================================================
// Name: Release
// Description: Releases objects created on the heap and the
// texture for primitive shapes
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Release(void)
{
	//realse the vertex buffer object
	if(m_vertexBuffer)
	{
		MLRELEASE(m_vertexBuffer);
		m_vertexBuffer = NULL;
	}
	//release the default shader if it is used
	if(m_defaultShader && m_shader)
	{
		MLRELEASE(m_shader);
		m_shader = NULL;
	}
	//release the primitive shader
	if (m_primitiveShader)
	{
		MLRELEASE(m_primitiveShader);
		m_primitiveShader = NULL;
	}
	//release the sprite
	if (m_sprites)
	{
		delete [] m_sprites;
		m_spriteQueue.clear();
		m_sprites = NULL;
	}
	//release the blank texture used for rendering
	//primitives
	if (m_blankTexture != 0)
	{
		MLTextureManager::DeleteTexture(m_blankTexture);
		m_blankTexture = 0;
	}

	return true;
}
#pragma endregion Destruction
//===================================================================

//Initalise/Begin and End
//===================================================================
#pragma region Start_and_End_Functions
//=========================================================
// Name: Initalise
// Description: Initalise the sprite batch vertex buffer
// as well as the batch array and the blank 2x2 texture
// used for primitives.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Initalise(void)
{
	//set the buffer offset to 0
	m_BufferOffset = 0;

	//create a new vertex buffer object
	m_vertexBuffer = new MLVertexBuffer();
	m_vertexBuffer->CreateBuffer(GL_ARRAY_BUFFER, m_BufferSize, GL_STREAM_DRAW);
	m_vertexBuffer->Bind();
	m_vertexBuffer->SetBufferData(NULL, GL_STREAM_DRAW);


	//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);
	
	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
	glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));

	m_vertexBuffer->Unbind();
	//void unbind()	{glBindBuffer(m_Type, 0);}

	//create the sprite que array
	m_sprites = new MLBatchItem[ML_DEFAULT_BATCH_SIZE];
	//ensure the vector que is clear
	m_spriteQueue.clear();

	//set the total sprite count to 0
	m_currentSpriteCount = 0;
	//set batch size
	m_currentBatchSize = ML_DEFAULT_BATCH_SIZE;

	//create a blank 2x2 white texture for primitive rendering;
	GLubyte* texdata = new GLubyte[4 * 2 * 2];
	for(int y=0; y < 2; y++) 
	{
		for(int x=0; x < 2; x++)
		{
			texdata[((x + y * 2) * 4)]			= 255;
			texdata[((x + y * 2) * 4) + 1]		= 255;
			texdata[((x + y * 2) * 4) + 2]		= 255;
			texdata[((x + y * 2) * 4) + 3]		= 255;	
		}
	}
	//load the texture into the texture manager
	m_blankTexture = MLRenderer::MLTextureManager::LoadRawBytes(texdata, 2, 2, GL_RGBA);

	//delete the pointer data
	delete [] texdata;

	return true;
}

//=========================================================
// Name: Begin
// Description: will start the sprite batch
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Begin(bool Alpha /* = true*/, MLFormats::MLSortMode sortType /* = MLFormats::sortMode::Texture*/, bool wireFrame /* = true*/)
{
	//Set the rendering settings
	m_wireFrame = wireFrame;
	m_sortMode = sortType;
		
	//simple check for alpha. This is pre-set to true
	if (Alpha)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
		glDisable(GL_BLEND);

	//set the begincall to true so now end can be called without error
	m_beginCall = true;

	return true;
}

//=========================================================
// Name: End
// Description: Ends the sprite batch calling flush to render
// the sprites
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::End(void)
{
	//If begin has not been called then there is an error
	if (!m_beginCall)
	{
		MLError::ErrorHandler::ML_Printf(ML_SpriteBatchError, "Begin must be called before end");
		return false;
	}
	//if the sprite count is greater than 0 the something will need to be rendered still
	if (m_currentSpriteCount > 0)
		Flush();

	//reset the sprite count
	m_currentSpriteCount = 0;
	//reset the buffer offset !!!!!!!!!! THIS MAY NOT BE NEEDED SO COMMENTED OUT FOR NOW
	m_BufferOffset = 0; 
	//clear the sprite que
	m_spriteQueue.clear();

	return true;	
}
#pragma endregion Start_and_End_Functions
//===================================================================

//Draw Functions including write function
//===================================================================
#pragma region Draw_Functions
//=========================================================
// Name: Draw
// Description: Draws a textures with set paramets
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Colour /* = NULL */)
{
	Vector2f pos	= Vector2f(Destination.x, Destination.y);
	Vector2f size	= Vector2f(Destination.Width, Destination.Height);
	Vector2f origin = Vector2f(Destination.Width / 2, Destination.Height / 2);
	Vector4f src	= Vector4f(0, 0, MLRenderer::MLTextureManager::GetWidth(Texture), MLRenderer::MLTextureManager::GetHeight(Texture)); 

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
	
	Batch(Texture, pos, camera, src, 0.0f, origin, 0, size, Vector2f(1.0f, 1.0f), MLFormats::MLSpriteEffects::None, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, Vector4f& Colour /* = NULL */)
{
	Vector2f pos	= Vector2f(Destination.x, Destination.y);
	Vector2f size	= Vector2f(Destination.Width, Destination.Height);
	Vector2f origin = Vector2f(Destination.Width / 2, Destination.Height / 2);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, pos, camera, Source, 0.0f, origin, 0, size, Vector2f(1.0f, 1.0f), MLFormats::MLSpriteEffects::None, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, float rotation, Vector2f& origin, MLFormats::MLSpriteEffects effect, Vector4f& Colour /* = NULL */)
{
	Vector2f pos	= Vector2f(Destination.x, Destination.y);
	Vector2f size	= Vector2f(Destination.Width, Destination.Height);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, pos, camera, Source, rotation, origin, 0, size, Vector2f(1.0f, 1.0f), effect, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, float rotation, Vector2f& origin, int layer, MLFormats::MLSpriteEffects effect, Vector4f& Colour /* = NULL */)
{
	Vector2f pos	= Vector2f(Destination.x, Destination.y);
	Vector2f size	= Vector2f(Destination.Width, Destination.Height);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, pos, camera, Source, rotation, origin, layer, size, Vector2f(1.0f, 1.0f), effect, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector2f& Position, Vector4f& Colour /* = NULL */)
{
	Vector2f size	= Vector2f(MLRenderer::MLTextureManager::GetWidth(Texture), MLRenderer::MLTextureManager::GetHeight(Texture));
	Vector2f origin = Vector2f(0, 0);
	Vector4f src	= Vector4f(0, 0, size.Width, size.Height); 

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, Position, true, src, 0.0f, origin, 0, size, Vector2f(1.0f, 1.0f), MLFormats::MLSpriteEffects::None, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, Vector4f& Colour /* = NULL */)
{
	Vector2f size	= Vector2f(Source.Width - Source.x, Source.Height - Source.y);
	Vector2f origin = Vector2f(0, 0);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, Position, camera, Source, 0.0f, origin, 0, size, Vector2f(1.0f, 1.0f), MLFormats::MLSpriteEffects::None, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, float rotation, Vector2f& origin, Vector2f& scale, MLFormats::MLSpriteEffects effect, Vector4f& Colour /* = NULL */)
{
	Vector2f size	= Vector2f(Source.Width - Source.x, Source.Height - Source.y);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, Position, camera, Source, rotation, origin, 0, size, scale,effect, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, float rotation, Vector2f& origin, int layer, Vector2f& scale, MLFormats::MLSpriteEffects effect, Vector4f& Colour /* = NULL */)
{
	Vector2f size	= Vector2f(Source.Width - Source.x, Source.Height - Source.y);

	if (Colour == NULL)
		Colour = Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

	Batch(Texture, Position, camera, Source, rotation, origin, layer, size, scale, effect, Colour, MLFormats::MLPrimitiveTypes::Quad);
}

//=========================================================
// Name: Write
// Description: Will write text to a specified location
// with a set font
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::Write(MLFont* font, Vector2f& position, bool camera, std::string& text)
{
	wchar_t* writeText = new wchar_t[text.length() + 1];

	for (int i = 0; i < text.length(); i++)
	{
		writeText[i] = text[i];
	}

	writeText[text.length()] = '\0';

	Print(font, Vector4f(position.x, position.y, 0, 0), camera, writeText, 0);
	
	delete[] writeText;
}

MLCoreDLL void MLSpriteBatch::Write(MLFont* font, Vector2f& position, bool camera, int layer, std::string& text)
{
	wchar_t* writeText = new wchar_t[text.length() + 1];

	for (int i = 0; i < text.length(); i++)
	{
		writeText[i] = text[i];
	}

	writeText[text.length()] = '\0';

	Print(font, Vector4f(position.x, position.y, 0, 0), camera, writeText, layer);

	delete[] writeText;
}

MLCoreDLL void MLSpriteBatch::Write(MLFont* font, Vector2f& position, bool camera, const wchar_t* format, ...)
{
	//the max character count per write is 1024 though it will need to be improved to 5x bigger
	wchar_t text[1024];

	va_list args;
	va_start(args, format);
	vswprintf_s(text, format, args);
	va_end(args);

	Print(font, Vector4f(position.x, position.y, 0, 0), camera, (wchar_t*)text, 0);
}

MLCoreDLL void MLSpriteBatch::Write(MLFont* font, Vector2f& position, bool camera, int layer, const wchar_t* format, ...)
{
	//the max character count per write is 1024 though it will need to be improved to 5x bigger
	wchar_t text[1024];

	va_list args;
	va_start(args, format);
	vswprintf_s(text, format, args);
	va_end(args);

	Print(font, Vector4f(position.x, position.y, 0, 0), camera, (wchar_t*)text, layer);
}

MLCoreDLL void MLSpriteBatch::Write(MLFont* font, Vector4f& position, bool camera, const wchar_t* format, ...)
{
	//the max character count per write is 1024 though it will need to be improved to 5x bigger
	wchar_t text[1024];

	va_list args;
	va_start(args, format);
	vswprintf_s(text, format, args);
	va_end(args);

	Print(font, position, camera, (wchar_t*)text, 0);
}
#pragma endregion Draw_Functions
//===================================================================

//Primitive and geometry functions
//===================================================================
#pragma region Primitive Function
//=========================================================
// Name: Draw Point
// Description: Will draw a single point in a desired location
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawPoint(Vector2f& Position)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(1.0f,1.0f), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Point);
}

MLCoreDLL void MLSpriteBatch::DrawPoint(Vector2f& Position, float size)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Point);
}

MLCoreDLL void MLSpriteBatch::DrawPoint(Vector2f& Position, float size, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Point);
}

MLCoreDLL void MLSpriteBatch::DrawPoint(Vector2f& Position, float size, MLFormats::MLPointType style, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Point);
}

MLCoreDLL void MLSpriteBatch::DrawPoint(Vector2f& Position, float size, MLFormats::MLPointType style, float layer, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), layer, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Point);
}

//=========================================================
// Name: Draw Line
// Description: Will draw a line in a specified location
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawLine(Vector2f& PosA, Vector2f& PosB)
{
	Batch(m_blankTexture, PosA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, PosB, Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
}

MLCoreDLL void MLSpriteBatch::DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness)
{
	Batch(m_blankTexture, PosA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, PosB, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
}

MLCoreDLL void MLSpriteBatch::DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness, Vector4f& colour)
{
	Batch(m_blankTexture, PosA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, PosB, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
}

MLCoreDLL void MLSpriteBatch::DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness, float layer, Vector4f& colour)
{
	Batch(m_blankTexture, PosA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), layer, PosB, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour,  MLFormats::MLPrimitiveTypes::Line);
}

//=========================================================
// Name: Draw Triangle
// Description: Will draw a triangle in a specified location
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, float size)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(1.0f,1.0f), Vector2f(size,size), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f),  MLFormats::MLPrimitiveTypes::Triangle);
}

MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, float size, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(1.0f,1.0f), Vector2f(size,size), MLFormats::MLSpriteEffects::None, colour,  MLFormats::MLPrimitiveTypes::Triangle);
}

MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, float size, Vector2f& origin, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, origin, 0, Vector2f(1.0f,1.0f), Vector2f(size,size), MLFormats::MLSpriteEffects::None, colour,  MLFormats::MLPrimitiveTypes::Triangle);
}

MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, Vector2f& size, float rot, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), rot, Vector2f(0,0), 0, Vector2f(1.0f,1.0f), size,  MLFormats::MLSpriteEffects::None, colour,  MLFormats::MLPrimitiveTypes::Triangle);
}

MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), rot, origin, 0, Vector2f(1.0f,1.0f), size, MLFormats::MLSpriteEffects::None, colour,  MLFormats::MLPrimitiveTypes::Triangle);
}

MLCoreDLL void MLSpriteBatch::DrawTriangle(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, float layer, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), rot, origin, layer, Vector2f(1.0f,1.0f), size, MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Triangle);
}

//=========================================================
// Name: Draw Quad
// Description: Draws a coloured quad in a specified location
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawQuad(Vector2f& Position, float size)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector2f& Position, float size, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(size,size), Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), 0, origin, 0, size,  Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), rot, origin, 0, size, Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, float layer, Vector4f& colour)
{
	Batch(m_blankTexture, Position, true, Vector4f(0,0,1,1), rot, origin, layer, size, Vector2f(1.0f,1.0f), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Quad);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector4f& Destination)
{
		Vector2f posA1 = Vector2f(Destination.x, Destination.y);
		Vector2f posB1 = Vector2f(Destination.Width, Destination.y);
		Batch(m_blankTexture, posA1, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB1, Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
		Vector2f posA2 = Vector2f(Destination.Width, Destination.y);
		Vector2f posB2 = Vector2f(Destination.Width, Destination.Height);
		Batch(m_blankTexture, posA2, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB2, Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
		Vector2f posA3 = Vector2f(Destination.Width, Destination.Height);
		Vector2f posB3 = Vector2f(Destination.x, Destination.Height);
		Batch(m_blankTexture, posA3, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB3, Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
		Vector2f posA4 = Vector2f(Destination.x, Destination.Height);
		Vector2f posB4 = Vector2f(Destination.x, Destination.y);
		Batch(m_blankTexture, posA4, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB4, Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector4f& Destination, Vector4f& colour)
{
	Vector2f posA1 = Vector2f(Destination.x, Destination.y);
	Vector2f posB1 = Vector2f(Destination.Width, Destination.y);
	Batch(m_blankTexture, posA1, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB1, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA2 = Vector2f(Destination.Width, Destination.y);
	Vector2f posB2 = Vector2f(Destination.Width, Destination.Height);
	Batch(m_blankTexture, posA2, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB2, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA3 = Vector2f(Destination.Width, Destination.Height);
	Vector2f posB3 = Vector2f(Destination.x, Destination.Height);
	Batch(m_blankTexture, posA3, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB3, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA4 = Vector2f(Destination.x, Destination.Height);
	Vector2f posB4 = Vector2f(Destination.x, Destination.y);
	Batch(m_blankTexture, posA4, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB4, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
}

MLCoreDLL void MLSpriteBatch::DrawQuad(Vector4f& Destination, float lineThinkness, Vector4f& colour)
{
	Vector2f posA1 = Vector2f(Destination.x, Destination.y);
	Vector2f posB1 = Vector2f(Destination.Width, Destination.y);
	Batch(m_blankTexture, posA1, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB1, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA2 = Vector2f(Destination.Width, Destination.y);
	Vector2f posB2 = Vector2f(Destination.Width, Destination.Height);
	Batch(m_blankTexture, posA2, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB2, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA3 = Vector2f(Destination.Width, Destination.Height);
	Vector2f posB3 = Vector2f(Destination.x, Destination.Height);
	Batch(m_blankTexture, posA3, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB3, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	Vector2f posA4 = Vector2f(Destination.x, Destination.Height);
	Vector2f posB4 = Vector2f(Destination.x, Destination.y);
	Batch(m_blankTexture, posA4, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB4, Vector2f(lineThinkness,lineThinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
}

//=========================================================
// Name: Draw Circle & Draw Circle Fill
// Description: Draws a circle in a specific location.
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawCircle(Vector2f& Position, float radius)
{
	float angle = 0;
	for(int i = 0; i < 50; i++) 
	{

		angle = i*2*ML_PI/50;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		angle = (i+1)*2*ML_PI/50;
		Vector2f posB = Vector2f(Position.x  + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB, Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircle(Vector2f& Position, float radius, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < 50; i++) 
	{

		angle = i*2*ML_PI/50;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		angle = (i+1)*2*ML_PI/50;
		Vector2f posB = Vector2f(Position.x  + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircle(Vector2f& Position, float radius, float thinkness, int segments)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		angle = (i+1)*2*ML_PI/segments;
		Vector2f posB = Vector2f(Position.x  + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB, Vector2f(thinkness,thinkness), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::Line);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircle(Vector2f& Position, float radius, float thinkness, int segments, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		angle = (i+1)*2*ML_PI/segments;
		Vector2f posB = Vector2f(Position.x  + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB, Vector2f(thinkness,thinkness), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircle(Vector2f& Position, Vector2f& size, float thinkness, int segments, float rot, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * size.x), Position.y + (sin(angle) * size.y));
		angle = (i+1)*2*ML_PI/segments;
		Vector2f posB = Vector2f(Position.x  + (cos(angle) * size.x), Position.y + (sin(angle) * size.y));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, posB, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::Line);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircleFill(Vector2f& Position, float radius)
{
	float angle = 0;
	for(int i = 0; i < 50; i++) 
	{

		angle = i*2*ML_PI/50;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		
		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(radius, radius), Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::TriangleFan);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircleFill(Vector2f& Position, float radius, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < 50; i++) 
	{

		angle = i*2*ML_PI/50;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(radius, radius), Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::TriangleFan);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircleFill(Vector2f& Position, float radius, int segments)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));

		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(radius, radius), Vector2f(1,1), MLFormats::MLSpriteEffects::None, Vector4f(1.0f,1.0f, 1.0f, 1.0f), MLFormats::MLPrimitiveTypes::TriangleFan);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircleFill(Vector2f& Position, float radius, int segments, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * radius), Position.y + (sin(angle) * radius));
		
		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, Vector2f(radius, radius), Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::TriangleFan);
	}
}

MLCoreDLL void MLSpriteBatch::DrawCircleFill(Vector2f& Position, Vector2f& size, int segments, float rot, Vector4f& colour)
{
	float angle = 0;
	for(int i = 0; i < segments; i++) 
	{

		angle = i*2*ML_PI/segments;
		Vector2f posA = Vector2f(Position.x + (cos(angle) * size.x), Position.y + (sin(angle) * size.y));
		
		Batch(m_blankTexture, posA, true, Vector4f(0,0,1,1), 0, Vector2f(0,0), 0, size, Vector2f(1,1), MLFormats::MLSpriteEffects::None, colour, MLFormats::MLPrimitiveTypes::TriangleFan);
	}
}
#pragma endregion Primitive Function
//===================================================================

//Other Draw Types
//===================================================================
#pragma region Vertex_Array_and_Particle_Emitter

//=========================================================
// Name: Draw Vertex Array
// Description: Will draw a vertex array and texture it
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, Vector4f& colour)
{

}

MLCoreDLL void MLSpriteBatch::DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector4f& source, Vector4f& colour)
{

}

MLCoreDLL void MLSpriteBatch::DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector4f& source, float rotation, Vector2f& origin, Vector4f& colour)
{

}

MLCoreDLL void MLSpriteBatch::DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector2f& source, float rotation, Vector2f& origin, MLFormats::MLSpriteEffects effect, Vector4f& colour)
{

}

MLCoreDLL void MLSpriteBatch::DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector2f& source, float rotation, Vector2f& origin, int layer, MLFormats::MLSpriteEffects effect, Vector4f& colour)
{

}

//=========================================================
// Name: Draw Particle Effect
// Description: Will draw a Particle Effect
// Creator: Chris Millin
//=========================================================
MLCoreDLL void MLSpriteBatch::DrawParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect)
{
	BatchParticleEffect(textureID, position, pEffect, true, 0);
}

MLCoreDLL void MLSpriteBatch::DrawParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect, int layer)
{
	BatchParticleEffect(textureID, position, pEffect, true, layer);
}

#pragma endregion Vertex Array and Particle Emitter
//===================================================================

//Core Functions
//===================================================================
#pragma region Core_Functions
//=========================================================
// Name: Batch
// Description: Will batch a sprite or render it immediately
// if the sort mode has been set to immediate. The batch is
// also dynamically expanded if it becomes out of range to 
// store all the sprites
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Batch(GLuint Texture, Vector2f Position, bool camera, Vector4f Source, float rotation, Vector2f origin, int layer, Vector2f size, Vector2f scale, MLFormats::MLSpriteEffects effect, Vector4f Colour, MLFormats::MLPrimitiveTypes Type)
{
	//check to see if the texute is null or begin has not been called
	if (Texture == NULL && !m_beginCall)
		return false;

	//if the sort mode is immediate then the previous sprite need to be drawn (LAST TEXTURE IS NEEDED TO IMPROVE IMMEDIATE RENDERING)
	if (m_sortMode == MLFormats::MLSortMode::Immediate/* && m_LastTexture != Texture*/)
	{
		//double check to see if the sprite count is over 0
		if (m_currentSpriteCount > 0)
			Flush();
	}
	
	//if the sprite count is bigger or the same as the current max batch size
	//then it needs to be expanded
	if (m_currentSpriteCount >= m_currentBatchSize)
	{
		//expand the batch
		ExpandBatch();
	}

	//store all the data about the sprite
	m_sprites[m_currentSpriteCount].SetType(MLFormats::MLBatchType::ML_PRIMITIVE);
	m_sprites[m_currentSpriteCount].SetTexture(Texture);
	m_sprites[m_currentSpriteCount].SetPos(Position);
	m_sprites[m_currentSpriteCount].SetSource(Source);
	m_sprites[m_currentSpriteCount].SetRotation(rotation);
	m_sprites[m_currentSpriteCount].SetOrigin(origin);
	m_sprites[m_currentSpriteCount].SetLayer(layer);
	m_sprites[m_currentSpriteCount].SetSize(size);
	m_sprites[m_currentSpriteCount].SetScale(scale);
	m_sprites[m_currentSpriteCount].SetEffect(effect);
	m_sprites[m_currentSpriteCount].SetColour(Colour);
	m_sprites[m_currentSpriteCount].SetRenderType(Type);
	m_sprites[m_currentSpriteCount].SetCameraEnabled(camera);

	//push a pointer to the data onto the vector so that it can be used for sorting
	m_spriteQueue.push_back(&m_sprites[m_currentSpriteCount]);

	//increase the current sprite count
	m_currentSpriteCount++;	

	return true;
}

//=========================================================
// Name: Batch Char
// Description: Batches a single character
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::BatchChar(MLFont* font, MLRenderer::MLText::MLFontCache* character, bool camera, int xLoc, int yLoc, int layer)
{
	//check to see if the texture exists and begin has been called
	if (font->GetFontTexture() == NULL && !m_beginCall)
		return false;

	//if the sort mode is immediate then draw
	if (m_sortMode == MLFormats::MLSortMode::Immediate /*&& m_LastTexture != font->GetFontTexture()*/)
	{
		//check to see if there is sprites batched
		if (m_currentSpriteCount > 0)
			Flush();
	}

	//if the sprite count is bigger or the same as the current max batch size
	//then it needs to be expanded 
	if (m_currentSpriteCount >= m_currentBatchSize)
	{
		//expand the batch
		ExpandBatch();
	}

	//for the position the x and y is used but the y is offset by the bearing so haging letters like g and j can
	//sit on the same pen line as other characters
	//information about the character is also used to create the character size and source
	Vector2f pos = Vector2f(xLoc, yLoc - /*(font->GetFontHeight() - character->height) -*/character->bearingY);
	Vector4f source = Vector4f(0, character->textureLoc , character->width, character->textureLoc - character->height);
	Vector2f size = Vector2f(character->width, character->height);

	//set all the sprite data using the some fixed settings and also information from the font
	m_sprites[m_currentSpriteCount].SetType(MLFormats::MLBatchType::ML_FONT_CHARACTER);
	m_sprites[m_currentSpriteCount].SetTexture(font->GetFontTexture());
	m_sprites[m_currentSpriteCount].SetPos(pos);
	m_sprites[m_currentSpriteCount].SetSource(source);
	m_sprites[m_currentSpriteCount].SetRotation(0.0f);
	m_sprites[m_currentSpriteCount].SetOrigin(Vector2f(0.0f,0.0f));
	m_sprites[m_currentSpriteCount].SetLayer(layer);
	m_sprites[m_currentSpriteCount].SetSize(size);
	m_sprites[m_currentSpriteCount].SetScale(Vector2f(1.0f, 1.0f));
	m_sprites[m_currentSpriteCount].SetEffect(MLFormats::MLSpriteEffects::None);
	m_sprites[m_currentSpriteCount].SetColour(font->GetPenColour());
	m_sprites[m_currentSpriteCount].SetRenderType(MLFormats::MLPrimitiveTypes::Quad);
	m_sprites[m_currentSpriteCount].SetCameraEnabled(camera);
	
	//push a pointer onto the sprite queue
	m_spriteQueue.push_back(&m_sprites[m_currentSpriteCount]);

	//increase the sprite count
	m_currentSpriteCount++;

	return true;
}

//=========================================================
// Name: Batch Vertex Array
// Description: Batches a vertex array
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::BatchVertexArray(GLuint Texture, Vector2f Position, Vector2f* vertices, int vertexCount, bool camera, Vector4f Source, float rotation, Vector2f origin, int layer, Vector2f size, Vector2f scale, MLFormats::MLSpriteEffects effect, Vector4f Colour, MLFormats::MLPrimitiveTypes Type)
{
	//check to see if the texture exists and begin has been called
	if (Texture == NULL && !m_beginCall)
		return false;

	//if the sort mode is immediate then draw
	if (m_sortMode == MLFormats::MLSortMode::Immediate /*&& m_LastTexture != font->GetFontTexture()*/)
	{
		//check to see if there is sprites batched
		if (m_currentSpriteCount > 0)
			Flush();
	}

	//if the sprite count is bigger or the same as the current max batch size
	//then it needs to be expanded 
	if (m_currentSpriteCount >= m_currentBatchSize)
	{
		//expand the batch
		ExpandBatch();
	}

	
	//set all the sprite data useing the some fixed settings and also information from the font
	m_sprites[m_currentSpriteCount].SetType(MLFormats::MLBatchType::ML_VERTEX_ARRAY);
	m_sprites[m_currentSpriteCount].SetTexture(Texture);
	m_sprites[m_currentSpriteCount].SetPos(Position);
	m_sprites[m_currentSpriteCount].SetSource(Source);
	m_sprites[m_currentSpriteCount].SetRotation(rotation);
	m_sprites[m_currentSpriteCount].SetOrigin(origin);
	m_sprites[m_currentSpriteCount].SetLayer(layer);
	m_sprites[m_currentSpriteCount].SetSize(size);
	m_sprites[m_currentSpriteCount].SetScale(scale);
	m_sprites[m_currentSpriteCount].SetEffect(effect);
	m_sprites[m_currentSpriteCount].SetColour(Colour);
	m_sprites[m_currentSpriteCount].SetRenderType(MLFormats::MLPrimitiveTypes::TriangleFan);
	m_sprites[m_currentSpriteCount].SetCameraEnabled(camera);
	m_sprites[m_currentSpriteCount].GetVertices().clear();
	m_sprites[m_currentSpriteCount].GetVertices().assign(vertices, vertices + vertexCount);

	//push a pointer onto the sprite que
	m_spriteQueue.push_back(&m_sprites[m_currentSpriteCount]);

	//increase the sprite count
	m_currentSpriteCount++;

	return true;
}

//=========================================================
// Name: Batch Particle Effect
// Description: Batches a Particle Effect
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::BatchParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect, bool camera, int layer)
{
	//check to see if the texture exists and begin has been called
	if (textureID == NULL && !m_beginCall)
		return false;

	//if the sort mode is immediate then draw
	if (m_sortMode == MLFormats::MLSortMode::Immediate /*&& m_LastTexture != font->GetFontTexture()*/)
	{
		//check to see if there is sprites batched
		if (m_currentSpriteCount > 0)
			Flush();
	}

	//if the sprite count is bigger or the same as the current max batch size
	//then it needs to be expanded 
	if (m_currentSpriteCount >= m_currentBatchSize)
	{
		//expand the batch
		ExpandBatch();
	}


	//set all the sprite data useing the some fixed settings and also information from the font
	m_sprites[m_currentSpriteCount].SetType(MLFormats::MLBatchType::ML_PARTICALE_EMITTER);
	m_sprites[m_currentSpriteCount].SetTexture(textureID);
	m_sprites[m_currentSpriteCount].SetPos(position);
	m_sprites[m_currentSpriteCount].SetSource(Vector4f(0,0,0,0));
	m_sprites[m_currentSpriteCount].SetRotation(0);
	m_sprites[m_currentSpriteCount].SetOrigin(Vector2f(0,0));
	m_sprites[m_currentSpriteCount].SetLayer(layer);
	m_sprites[m_currentSpriteCount].SetSize(Vector2f(0,0));
	m_sprites[m_currentSpriteCount].SetScale(Vector2f(0,0));
	m_sprites[m_currentSpriteCount].SetEffect(MLFormats::MLSpriteEffects::None);
	m_sprites[m_currentSpriteCount].SetColour(Vector4f(0,0,0,0));
	m_sprites[m_currentSpriteCount].SetRenderType(MLFormats::MLPrimitiveTypes::Quad);
	m_sprites[m_currentSpriteCount].SetCameraEnabled(camera);
	m_sprites[m_currentSpriteCount].SetParitcleEffect(pEffect);

	//push a pointer onto the sprite que
	m_spriteQueue.push_back(&m_sprites[m_currentSpriteCount]);

	//increase the sprite count
	m_currentSpriteCount++;

	return true;
}

//=========================================================
// Name: Print
// Description: Will work out how to batch each character
// with the chosen text and font used.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Print(MLFont* font, Vector4f TextBlock, bool camera, const wchar_t* Text, int layer)
{
	//This is a basic print function
	//Later wrapping will need to be used but wrapping could be created in edit mode but I have
	//no idea. alignment, format and immediate mode need to be added as well as allowing
	//for a font layer
	int xLoc = TextBlock.x;
	int yLoc = TextBlock.y;

	//the width of the last character rendered
	int lastCharWidth  = 0;

	//loop through all the characters in the string
	for(; *Text != 0; ++Text)
	{
		//if the null terminator is hit then
		//break the loop
		if (*Text == '\0') break;
		//if the next line terminator is hit move down the font size
		else if (*Text == '\n')
		{
			//move down by the font size
			yLoc += font->GetFontWidth() + 2;
			lastCharWidth = -2;
			//reset the x position
			xLoc = TextBlock.x;
		}
		//if the character is a space then move on by a fixed amount
		else if (*Text == ' ')
		{
			//I found that with font width divided by 3 is a good amount
			lastCharWidth = font->GetFontWidth()/3;//TEMP
		}
		else
		{
			//if it is any other character then it the character information is obtained
			MLRenderer::MLText::MLFontCache* characterdata = font->GetCacheData(*Text);
			//batch the character
			BatchChar(font, characterdata, camera, xLoc, yLoc, layer);
			//move on by the characters advance amount
			lastCharWidth = characterdata->advance;
		}

		//move the pen on
		xLoc += lastCharWidth;
	}

	return true;
}

//=========================================================
// Name: Flush
// Description: Will send all the sprites to Get rendered
// off.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Flush()
{
	//bind the vertex buffer to see the shader attributes
	m_vertexBuffer->Bind();

	//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
	glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//if the sprite count is 0 the just return as nothing can be done
	if(m_currentSpriteCount == 0)
		return false;

	//if the mode is immediate or no sort then sorting is not performed
	if(m_sortMode != MLFormats::MLSortMode::Immediate || m_sortMode != MLFormats::MLSortMode::NoSort)
	{
		//sort the sprites if sorting is enabled
		Sort();
	}

	//current position in the sprite array
	int count = 0;
	//Gets the first texture from the que
	GLuint currentTexture = m_spriteQueue[0]->GetTexture();
	//Get the first primitve type from the que
	MLFormats::MLPrimitiveTypes currentPrimitive = m_spriteQueue[0]->GetRenderType();
	//Get the first shader
	//MLShader* currentShader = m_spriteQueue[0]->GetShader();
	//Get the first Item Type
	MLFormats::MLBatchType currentItem = m_spriteQueue[0]->GetType();

	//loop through all the sprites
	for(int i = 0; i < m_currentSpriteCount; i++)
	{
		

		//if the render type or texture changes then a render needs to be made
		if (currentTexture != m_spriteQueue[i]->GetTexture() || currentPrimitive != m_spriteQueue[i]->GetRenderType()
			/*|| currentShader != m_spriteQueue[i]->GetShader()*/|| currentItem != m_spriteQueue[i]->GetType())
		{
			if (currentItem == MLFormats::MLBatchType::ML_PRIMITIVE)
			{
				//bind the vertex buffer to see the shader attributes
				m_vertexBuffer->Bind();

				//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
				glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
				//unbind the vertex buffer
				m_vertexBuffer->Unbind();

				//check to see what primitve type to render
				switch(currentPrimitive)
				{
				case MLFormats::MLPrimitiveTypes::Point:
					RenderPoints(currentTexture, count, i - count);
					break;
				case MLFormats::MLPrimitiveTypes::Line:
					RenderLines(currentTexture, count, i - count);
					break;
				case MLFormats::MLPrimitiveTypes::Triangle:
					RenderTriangles(currentTexture, count, i - count, false);
					break;
				case MLFormats::MLPrimitiveTypes::TriangleFan:
					RenderTriangles(currentTexture, count, i - count, true);
					break;
				case MLFormats::MLPrimitiveTypes::Quad:
					RenderQuads(currentTexture, count, i - count);
					break;
				}
			}
			else if(currentItem == MLFormats::MLBatchType::ML_FONT_CHARACTER)
			{
				//bind the vertex buffer to see the shader attributes
				m_vertexBuffer->Bind();

				//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
				glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
				//unbind the vertex buffer
				m_vertexBuffer->Unbind();

				RenderQuads(currentTexture, count, i - count);
			}
			else if (currentItem == MLFormats::MLBatchType::ML_VERTEX_ARRAY)
			{
				//bind the vertex buffer to see the shader attributes
				m_vertexBuffer->Bind();

				//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
				glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
				glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
				glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
				//unbind the vertex buffer
				m_vertexBuffer->Unbind();

				RenderVertexArray(currentTexture, count, i - count);
			}
			else if(currentItem == MLFormats::MLBatchType::ML_PARTICALE_EMITTER)
			{
				RenderParticleEmitter(currentTexture, count, i - count);
			}

			//move the counter on
			count = i;
			//Get the next texture
			currentTexture = m_spriteQueue[i]->GetTexture();
			//Get the next primitve type
			currentPrimitive = m_spriteQueue[i]->GetRenderType();
			//Get the next shader
			/*currentShader = m_spriteQueue[i]->GetShader();*/
			//Get the next type
			currentItem = m_spriteQueue[i]->GetType();
		}
	}

	//for the last set of sprites they will need to be rendered as well as they will exit the
	//previous loop without rendering
	if (currentItem == MLFormats::MLBatchType::ML_PRIMITIVE)
	{
		//bind the vertex buffer to see the shader attributes
		m_vertexBuffer->Bind();

		//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
		glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
		//unbind the vertex buffer
		m_vertexBuffer->Unbind();

		//check to see what primitve type to render
		switch(currentPrimitive)
		{
		case MLFormats::MLPrimitiveTypes::Point:
			RenderPoints(currentTexture, count, m_currentSpriteCount - count);
			break;
		case MLFormats::MLPrimitiveTypes::Line:
			RenderLines(currentTexture, count, m_currentSpriteCount - count);
			break;
		case MLFormats::MLPrimitiveTypes::Triangle:
			RenderTriangles(currentTexture, count, m_currentSpriteCount - count, false);
			break;
		case MLFormats::MLPrimitiveTypes::TriangleFan:
			RenderTriangles(currentTexture, count, m_currentSpriteCount - count, true);
			break;
		case MLFormats::MLPrimitiveTypes::Quad:
			RenderQuads(currentTexture, count, m_currentSpriteCount - count);
			break;
		}
	}
	else if(currentItem == MLFormats::MLBatchType::ML_FONT_CHARACTER)
	{
		//bind the vertex buffer to see the shader attributes
		m_vertexBuffer->Bind();

		//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
		glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
		//unbind the vertex buffer
		m_vertexBuffer->Unbind();

		RenderQuads(currentTexture, count, m_currentSpriteCount - count);
	}
	else if (currentItem == MLFormats::MLBatchType::ML_VERTEX_ARRAY)
	{
		//bind the vertex buffer to see the shader attributes
		m_vertexBuffer->Bind();

		//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);

		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte *)NULL );
		glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)sizeof(Vector3f));
		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLFormats::MLVertexPosTexCol), (GLubyte*)(sizeof(Vector2f) + sizeof(Vector3f)));
		//unbind the vertex buffer
		m_vertexBuffer->Unbind();

		RenderVertexArray(currentTexture, count, m_currentSpriteCount - count);
	}
	else if(currentItem == MLFormats::MLBatchType::ML_PARTICALE_EMITTER)
	{
		RenderParticleEmitter(currentTexture, count, m_currentSpriteCount - count);
	}

	return true;
}

//=========================================================
// Name: Sort
// Description: If a specific sort has been set then it will
// be processed here.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::Sort()
{
	//check to see which sort mode is selected and perform the correct sort
	switch (m_sortMode)
	{
	case MLFormats::MLSortMode::Texture:
		// Sort by texture
		std::sort(m_spriteQueue.begin(), m_spriteQueue.end(), &this->SortTexture);
		break;

	case MLFormats::MLSortMode::BacktoFront:
		// Sort back to front.
		std::sort(m_spriteQueue.begin(), m_spriteQueue.end(), &this->SortBacktoFront);
		break;

	case MLFormats::MLSortMode::FronttoBack:
		// Sort front to back.
		std::sort(m_spriteQueue.begin(), m_spriteQueue.end(), &this->SortFronttoBack);
		break;
	}

	return true;
}

//=========================================================
// Name: Expand Batch
// Description: will expand the amount of sprites that can
// be batched.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::ExpandBatch()
{
	//if the batch needs to be expanded hen create a new batch
	MLBatchItem* newSize = new MLBatchItem[(m_currentBatchSize * 2) + 1];
	//copy over all the data and the new pointers to the vector
	for(int i = 0; i < m_currentSpriteCount; i++)
	{
		newSize[i] = m_sprites[i];
		m_spriteQueue[i] = &newSize[i];
	}
	//delete the old array
	delete [] m_sprites;
	//make the old array pointer point to the new memory 
	m_sprites = newSize;
	//increase the batch size to the new size
	m_currentBatchSize = (m_currentBatchSize * 2) + 1;

	return true;
}
#pragma endregion Core_Functions
//===================================================================

//core render function
//===================================================================
#pragma region Render_Function
//=========================================================
// Name: Render Point
// Description: Will process a point set of points to be
// rendered.
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::RenderPoints(GLuint texture, int startIndex, int count)
{
	//bind the vertex buffer
	m_vertexBuffer->Bind();

	//check to see if the vertex buffer has enough memory to store the amount of sprites that need to render
	if (m_BufferOffset + (sizeof(MLFormats::MLVertexPosTexCol) * count) >= m_BufferSize)
	{
		//if not the orphan a new buffer
		m_vertexBuffer->SetBufferData(m_BufferSize, NULL, GL_STREAM_DRAW);
		//move the buffer to 0 offset
		m_BufferOffset = 0;
	}

	//map the size needed to store the vertex data
	MLFormats::MLVertexPosTexCol* mappedData = (MLFormats::MLVertexPosTexCol*)m_vertexBuffer->MapRange(m_BufferOffset, m_BufferOffset + (sizeof(MLFormats::MLVertexPosTexCol) * count));

	//copy over all the vertex data
	for(int i = 0; i < count; i++)
	{
		if(m_spriteQueue[startIndex + i]->GetCamEnabled())
		{
			mappedData[i].VPos	= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(m_spriteQueue[startIndex + i]->GetPos().x, m_spriteQueue[startIndex + i]->GetPos().y, m_spriteQueue[startIndex + i]->GetSize().x);
		}
		else
		{
			mappedData[i].VPos	= Vector3f(m_spriteQueue[startIndex + i]->GetPos().x, m_spriteQueue[startIndex + i]->GetPos().y, m_spriteQueue[startIndex + i]->GetSize().x);
		}
		mappedData[i].UV		= Vector2f(1, 1); 
		mappedData[i].Colour	= m_spriteQueue[startIndex + i]->GetColour();
	}

	glEnable(GL_PROGRAM_POINT_SIZE);

	//lock the shader
	m_primitiveShader->Lock();
	
	//apply any uniforms
	m_primitiveShader->AddUniform("View", (MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix()));
	m_primitiveShader->AddUniform("Projection", (MLRenderer::MLCoreGL::GetOrtho()));
	m_primitiveShader->AddUniform("Tex1", 0);
	
	//bind the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//unmap the buffer
	m_vertexBuffer->Unmap();

	//draw the points
	glDrawArrays(GL_POINTS, (m_BufferOffset/sizeof(MLFormats::MLVertexPosTexCol)), count);
	
	//move the buffer on by the drawn amount
	m_BufferOffset += (sizeof(MLFormats::MLVertexPosTexCol) * count);

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//unlock the shader 
	m_primitiveShader->Unlock();

	glDisable(GL_PROGRAM_POINT_SIZE);

	return true;
}

//=========================================================
// Name: Render Lines
// Description: Will render a group of lines
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::RenderLines(GLuint texture, int startIndex, int count)
{
	//bind the vertex buffer
	m_vertexBuffer->Bind();

	//lock the shader
	m_shader->Lock();
	
	//apply the uniforms
	m_shader->AddUniform("View", MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix());
	m_shader->AddUniform("Projection", (MLRenderer::MLCoreGL::GetOrtho()));
	m_shader->AddUniform("Tex1", 0);

	m_shader->AddUniform("EnableWire", m_wireFrame);
	m_shader->AddUniform("Line.Width", 1.0f);
	m_shader->AddUniform("Line.Colour", Vector4f(1.0f,1.0f,1.00f,1.0f));
	m_shader->AddUniform("ViewportMatrix", MLRenderer::MLCoreGL::GetView());


	//*INFO*
	//In order for the mapping to work correctly a max limit needs to be made for the vbo
	//this will then be filled with data up to a point. once the data is filled then a new
	//mapped range is needed and the draw call can be made. this means only a max amount of sprites
	//can be batch drawn but it meas a fixed size index buffer is possible
	if (m_BufferOffset +  (count * m_spriteSize) >= m_BufferSize)
	{
		m_vertexBuffer->SetBufferData(m_BufferSize, NULL, GL_STREAM_DRAW);
		m_BufferOffset = 0;
	}

	//map the size needed to store the vertex data
	MLFormats::MLVertexPosTexCol* mappedData = (MLFormats::MLVertexPosTexCol*)m_vertexBuffer->MapRange(m_BufferOffset, m_BufferOffset +  (count  * m_spriteSize));

	//loop through all the lines
	int j = 0;
	for(int i = 0; i < count; i++)
	{
		//Get a pointer to the currect sprite so less code is needed and looks a little cleaner
		MLBatchItem* currentsprite = m_spriteQueue[startIndex + i];

		//Get the perpendicular of the line
		Vector2f perp = normalize((currentsprite->GetPos()-currentsprite->GetSize()));
		//work out the way it is pointing
		perp = Vector2f(-perp.y, perp.x);
		//set the 4 points with the offset to make angle when a line is not stright
		Vector2f topLeft = currentsprite->GetPos() - perp * currentsprite->GetScale();
		Vector2f topRight = currentsprite->GetPos() + perp * currentsprite->GetScale();
		Vector2f bottomLeft = currentsprite->GetSize() - perp * currentsprite->GetScale();
		Vector2f bottomRight = currentsprite->GetSize() + perp * currentsprite->GetScale();

		//copy over all the vertex data

		if(m_spriteQueue[startIndex + i]->GetCamEnabled())
		{
			//top left
			mappedData[j + 0].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 0].UV		= Vector2f(0,0);
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//top right
			mappedData[j + 1].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topRight.x,topRight.y, 1.0f);
			mappedData[j + 1].UV		= Vector2f(1,0);
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 2].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 2].UV		= Vector2f(1,1);
			mappedData[j + 2].Colour	= currentsprite->GetColour();

			//bottom left
			mappedData[j + 3].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomLeft.x, bottomLeft.y, 1.0f);
			mappedData[j + 3].UV		= Vector2f(0,1);
			mappedData[j + 3].Colour	= currentsprite->GetColour();

			//top left
			mappedData[j + 4].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 4].UV		= Vector2f(0,0);
			mappedData[j + 4].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 5].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 5].UV		= Vector2f(1,1);
			mappedData[j + 5].Colour	= currentsprite->GetColour();
		}
		else
		{
			//top left
			mappedData[j + 0].VPos		= Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 0].UV		= Vector2f(0,0);
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//top right
			mappedData[j + 1].VPos		= Vector3f(topRight.x,topRight.y, 1.0f);
			mappedData[j + 1].UV		= Vector2f(1,0);
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 2].VPos		= Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 2].UV		= Vector2f(1,1);
			mappedData[j + 2].Colour	= currentsprite->GetColour();

			//bottom left
			mappedData[j + 3].VPos		= Vector3f(bottomLeft.x, bottomLeft.y, 1.0f);
			mappedData[j + 3].UV		= Vector2f(0,1);
			mappedData[j + 3].Colour	= currentsprite->GetColour();

			//top left
			mappedData[j + 4].VPos		= Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 4].UV		= Vector2f(0,0);
			mappedData[j + 4].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 5].VPos		= Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 5].UV		= Vector2f(1,1);
			mappedData[j + 5].Colour	= currentsprite->GetColour();
			//mappedData[i].VPos			= Vector3f(m_spriteQueue[startIndex + i]->GetPos().x, m_spriteQueue[startIndex + i]->GetPos().y, m_spriteQueue[startIndex + i]->GetSize().x);
		}

		

		
		j+=6;
	}

	//bind the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//unmap the vertex buffer
	m_vertexBuffer->Unmap();

	//draw all the objects	
	glDrawArrays(GL_TRIANGLES, (m_BufferOffset/sizeof(MLFormats::MLVertexPosTexCol)), count * 6);
	
	//offset the buffer by the drawn amount
	m_BufferOffset += (count * m_spriteSize);

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//unlock the shader 
	m_shader->Unlock();

	return true;
}

//=========================================================
// Name: Render Triangles
// Description: Will render a group of triangles
// Creator:
//=========================================================
MLCoreDLL bool MLSpriteBatch::RenderTriangles(GLuint texture, int startIndex, int count, bool fan)
{
	Mat4f homoMats[4]; //0 = scale 1 = origin trans 2 = rotation 3 = pos trans
	Mat4f resultMat;   //the result from all the homomats multiplied

	//bind the vertex buffer
	m_vertexBuffer->Bind();

	//lock the shader
	m_shader->Lock();

	//apply the uniforms
	m_shader->AddUniform("View", MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix());
	m_shader->AddUniform("Projection", (MLRenderer::MLCoreGL::GetOrtho()));
	m_shader->AddUniform("Tex1", 0);

	m_shader->AddUniform("EnableWire", m_wireFrame);
	m_shader->AddUniform("Line.Width", 1.0f);
	m_shader->AddUniform("Line.Colour", Vector4f(1.0f,1.0f,1.00f,1.0f));
	m_shader->AddUniform("ViewportMatrix", MLRenderer::MLCoreGL::GetView());

	//*INFO*
	//In order for the mapping to work correctly a max limit needs to be made for the vbo
	//this will then be filled with data up to a point. once the data is filled then a new
	//mapped range is needed and the draw call can be made. this means only a max amount of sprites
	//can be batch drawn but it meas a fixed size index buffer is possible
	if (m_BufferOffset +  (count * (sizeof(MLFormats::MLVertexPosTexCol) * 3)) >= m_BufferSize)
	{
		m_vertexBuffer->SetBufferData(m_BufferSize, NULL, GL_STREAM_DRAW);
		m_BufferOffset = 0;
	}

	//map the size needed to store the vertex data
	MLFormats::MLVertexPosTexCol* mappedData = (MLFormats::MLVertexPosTexCol*)m_vertexBuffer->MapRange(m_BufferOffset, m_BufferOffset +  (count  * m_spriteSize));

	//loop through all the triangles
	int j = 0;
	for(int i = 0; i < count; i++)
	{

		MLBatchItem* currentsprite = m_spriteQueue[startIndex + i];

		//identify the matricies to make sure they are clean before calculating the information
		MLMath::MLMat4::identify(homoMats[0]);
		MLMath::MLMat4::identify(homoMats[1]);
		MLMath::MLMat4::identify(homoMats[2]);
		MLMath::MLMat4::identify(homoMats[3]);

		//work out the scale it its not 1,1
		if (currentsprite->GetScale().x != 1.0f && currentsprite->GetScale().y != 1.0f)
		{
			MLMath::MLMat4::scale(homoMats[0], Vector3f(currentsprite->GetScale().x, currentsprite->GetScale().y, 1.0f));
		}

		//work out the origin offset if its not 0,0
		if (currentsprite->GetOrigin().x != 0 && currentsprite->GetOrigin().y != 0)
		{
			MLMath::MLMat4::translate(homoMats[1], Vector3f(0 - currentsprite->GetOrigin().x, 0 - currentsprite->GetOrigin().y, 0.0f));
		}

		//work out the rotation is its not 0
		if (currentsprite->GetRotation() != 0.0f)
		{
			MLMath::MLMat4::rotation(homoMats[2], 0.0f, 0.0f,  currentsprite->GetRotation());
		}

		//set the position
		MLMath::MLMat4::translate(homoMats[3], Vector3f(currentsprite->GetPos().x, currentsprite->GetPos().y, 0.0f));

		//combine the results
		resultMat = homoMats[3] * homoMats[2] * homoMats[1] * homoMats[0];

		//set the texture points
		Vector4f top = resultMat * Vector4f(0.5,0,0,1);
		Vector4f Left = resultMat * Vector4f(0,1,0,1);
		Vector4f Right = resultMat * Vector4f(1,1,0,1);

		if(m_spriteQueue[startIndex + i]->GetCamEnabled())
		{
			//top
			mappedData[j + 0].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(top.x,top.y, 1.0f);
			mappedData[j + 0].UV		= Vector2f(0,0); 
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//left
			mappedData[j + 1].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(Left.x,Left.y, 1.0f);
			mappedData[j + 1].UV		= Vector2f(0,1); 
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//right
			mappedData[j + 2].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(Right.x, Right.y, 1.0f);
			mappedData[j + 2].UV		= Vector2f(1,0); 
			mappedData[j + 2].Colour	= currentsprite->GetColour();
		}
		else
		{
			//top
			mappedData[j + 0].VPos		= Vector3f(top.x,top.y, 1.0f);
			mappedData[j + 0].UV		= Vector2f(0,0); 
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//left
			mappedData[j + 1].VPos		= Vector3f(Left.x,Left.y, 1.0f);
			mappedData[j + 1].UV		= Vector2f(0,1); 
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//right
			mappedData[j + 2].VPos		= Vector3f(Right.x, Right.y, 1.0f);
			mappedData[j + 2].UV		= Vector2f(1,0); 
			mappedData[j + 2].Colour	= currentsprite->GetColour();
		}

		j+=3;
	}

	//bind the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//unmap the buffer
	m_vertexBuffer->Unmap();

	//draw all the objects
	if(!fan)
		glDrawArrays(GL_TRIANGLES, (m_BufferOffset/sizeof(MLFormats::MLVertexPosTexCol)), count * 3);
	else
		glDrawArrays(GL_TRIANGLE_FAN, (m_BufferOffset/sizeof(MLFormats::MLVertexPosTexCol)), count * 3);
	
	
	//off set the buffer by the drawn amount
	m_BufferOffset += (count * (sizeof(MLFormats::MLVertexPosTexCol) * 3));

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//unlock the shader 
	m_shader->Unlock();

	return true;
}

//=========================================================
// Name: Render Quad
// Description: Will render a quad as well as sprites
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::RenderQuads(GLuint texture, int startIndex, int count)
{
	Mat4f homoMats[4]; //0 = scale 1 = origin trans 2 = rotation 3 = pos trans
	Mat4f resultMat;   //the result from all the homomats multiplied

	//bind the vertex buffer
	m_vertexBuffer->Bind();

	//lock the shader
	m_shader->Lock();

	//apply the uniforms
	m_shader->AddUniform("View", MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix());
	m_shader->AddUniform("Projection", (MLRenderer::MLCoreGL::GetOrtho()));
	m_shader->AddUniform("Tex1", 0);

	m_shader->AddUniform("EnableWire", m_wireFrame);
	m_shader->AddUniform("Line.Width", 1.0f);
	m_shader->AddUniform("Line.Colour", Vector4f(1.0f,1.0f,1.00f,1.0f));
	m_shader->AddUniform("ViewportMatrix", MLRenderer::MLCoreGL::GetView());

	//*INFO*
	//In order for the mapping to work correctly a max limit needs to be made for the vbo
	//this will then be filled with data up to a point. once the data is filled then a new
	//mapped range is needed and the draw call can be made. this means only a max amount of sprites
	//can be batch drawn but it meas a fixed size index buffer is possible
	if (m_BufferOffset +  (count * m_spriteSize) >= m_BufferSize)
	{
		m_vertexBuffer->SetBufferData(m_BufferSize, NULL, GL_STREAM_DRAW);
		m_BufferOffset = 0;
	}

	//map the size needed to store the vertex data
	MLFormats::MLVertexPosTexCol* mappedData = (MLFormats::MLVertexPosTexCol*)m_vertexBuffer->MapRange(m_BufferOffset, m_BufferOffset +  (count  * m_spriteSize));

	//Get the normalized value per pixel for working out the texture coords
	float widthval = 1.0/(float)MLTextureManager::GetWidth(texture);
	float heighval = 1.0/(float)MLTextureManager::GetHeight(texture);

	int j = 0;
	for(int i = 0; i < count; i++)
	{

		MLBatchItem* currentsprite = m_spriteQueue[startIndex + i];

		//depending on the sprite effect the source is swizzeled to Get the correct
		//setting
		if (currentsprite->GetEffect() == MLFormats::MLSpriteEffects::FlipVertical)
			currentsprite->SetSource(currentsprite->GetSource().zyxw);
		else if (currentsprite->GetEffect() == MLFormats::MLSpriteEffects::FlipHorizontal)
			currentsprite->SetSource(currentsprite->GetSource().xwzy);
		else if (currentsprite->GetEffect() == MLFormats::MLSpriteEffects::FlipBoth)
			currentsprite->SetSource(currentsprite->GetSource().wzyx);

		//identify the matrices
		MLMath::MLMat4::identify(homoMats[0]);
		MLMath::MLMat4::identify(homoMats[1]);
		MLMath::MLMat4::identify(homoMats[2]);
		MLMath::MLMat4::identify(homoMats[3]);

		//work out the scale it its not 1,1
		if (currentsprite->GetScale().x != 1.0f && currentsprite->GetScale().y != 1.0f)
		{
			MLMath::MLMat4::scale(homoMats[0], Vector3f(currentsprite->GetScale().x, currentsprite->GetScale().y, 1.0f));
		}

		//work out the origin offset if its not 0,0
		if (currentsprite->GetOrigin().x != 0 && currentsprite->GetOrigin().y != 0)
		{
			MLMath::MLMat4::translate(homoMats[1], Vector3f(0 - currentsprite->GetOrigin().x, 0 - currentsprite->GetOrigin().y, 0.0f));
		}

		//work out the rotation is its not 0
		if (currentsprite->GetRotation() != 0.0f)
		{
			MLMath::MLMat4::rotation(homoMats[2], 0.0f, 0.0f,  currentsprite->GetRotation());
		}

		//work out the position
		MLMath::MLMat4::translate(homoMats[3], Vector3f(currentsprite->GetPos().x, currentsprite->GetPos().y, 0.0f));

		//create the result matrix
		resultMat = homoMats[3] * homoMats[2] * homoMats[1] * homoMats[0];
		
		//resultMat = resultMat * MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix();

		//work out the points of the quad
		Vector4f topLeft = resultMat * Vector4f(0,0,0,1);
		Vector4f topRight = resultMat * Vector4f(currentsprite->GetSize().Width,0,0,1);
		Vector4f bottomLeft = resultMat * Vector4f(0,currentsprite->GetSize().Height,0,1);
		Vector4f bottomRight = resultMat * Vector4f(currentsprite->GetSize().Width,currentsprite->GetSize().Height,0,1);

		//work out the texture coordinates of the quad
		Vector2f UVtopLeft = Vector2f(currentsprite->GetSource().x * widthval, currentsprite->GetSource().y * heighval);
		Vector2f UVtopRight = Vector2f(currentsprite->GetSource().Width * widthval, currentsprite->GetSource().y * heighval);
		Vector2f UVbottomLeft = Vector2f(currentsprite->GetSource().x * widthval, currentsprite->GetSource().Height * heighval);
		Vector2f UVbottomRight = Vector2f(currentsprite->GetSource().Width * widthval, currentsprite->GetSource().Height * heighval);

		if(m_spriteQueue[startIndex + i]->GetCamEnabled())
		{
			//set the vertex data
			//top left
			mappedData[j + 0].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 0].UV		= UVtopLeft; 
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//top right
			mappedData[j + 1].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topRight.x,topRight.y, 1.0f);
			mappedData[j + 1].UV		= UVtopRight;
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 2].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 2].UV		= UVbottomRight;
			mappedData[j + 2].Colour	= currentsprite->GetColour();

			//bottom left
			mappedData[j + 3].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomLeft.x, bottomLeft.y, 1.0f);
			mappedData[j + 3].UV		= UVbottomLeft;
			mappedData[j + 3].Colour	= currentsprite->GetColour();

			//top left
			mappedData[j + 4].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 4].UV		= UVtopLeft;  
			mappedData[j + 4].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 5].VPos		= MLRenderer::MLCameraManager::GetActiveCamera()->GetMatrix() * Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 5].UV		= UVbottomRight;
			mappedData[j + 5].Colour	= currentsprite->GetColour();
		}
		else
		{
			//set the vertex data
			//top left
			mappedData[j + 0].VPos		= Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 0].UV		= UVtopLeft; 
			mappedData[j + 0].Colour	= currentsprite->GetColour();

			//top right
			mappedData[j + 1].VPos		= Vector3f(topRight.x,topRight.y, 1.0f);
			mappedData[j + 1].UV		= UVtopRight;
			mappedData[j + 1].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 2].VPos		= Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 2].UV		= UVbottomRight;
			mappedData[j + 2].Colour	= currentsprite->GetColour();

			//bottom left
			mappedData[j + 3].VPos		= Vector3f(bottomLeft.x, bottomLeft.y, 1.0f);
			mappedData[j + 3].UV		= UVbottomLeft;
			mappedData[j + 3].Colour	= currentsprite->GetColour();

			//top left
			mappedData[j + 4].VPos		= Vector3f(topLeft.x,topLeft.y, 1.0f);
			mappedData[j + 4].UV		= UVtopLeft;  
			mappedData[j + 4].Colour	= currentsprite->GetColour();

			//bottom right
			mappedData[j + 5].VPos		= Vector3f(bottomRight.x, bottomRight.y, 1.0f);
			mappedData[j + 5].UV		= UVbottomRight;
			mappedData[j + 5].Colour	= currentsprite->GetColour();
		}

		

		j+=6;
	}

	//bind the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//unmap the buffer
	m_vertexBuffer->Unmap();

	//draw all the objects
	glDrawArrays(GL_TRIANGLES, (m_BufferOffset/sizeof(MLFormats::MLVertexPosTexCol)), count * 6);
	
	//offset the buffer by the drawn amount
	m_BufferOffset += (count * m_spriteSize);

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//unlock the shader 
	m_shader->Unlock();

	return true;
}

//=========================================================
// Name: Render Vertex Array
// Description: Will render a vertex array
// Creator: Chris Millin
//=========================================================
MLCoreDLL bool MLSpriteBatch::RenderVertexArray(GLuint texture, int startIndex, int count)
{
	return true;
}

MLCoreDLL bool MLSpriteBatch::RenderParticleEmitter(GLuint texture, int startIndex, int count)
{
	for(int i = 0; i < count; i++)
	{

		MLBatchItem* currentEmitter = m_spriteQueue[startIndex + i];

		MLParticleEmitter::Update(MLWindows::windowsHandler::getDeltaTime(), currentEmitter);
		m_BufferOffset = MLParticleEmitter::Draw(m_vertexBuffer, m_BufferOffset, m_BufferSize, currentEmitter);
	}

	return true;
}

#pragma endregion Render_Function
//===================================================================
