#ifndef MLTRUETYPEFONT_H
#define MLTRUETYPEFONT_H

#include "MLFontCore.h"
#include "MLFont.h"
#include "MLIncludes.h"
#include "MLTexture.h"
#include "MLTextureAtlas.h"

#include <MLMath/MLMath.h>

namespace MLRender
{
	#define DEFAULT_FONT_SIZE 12
	#define DEFAULT_FONT_TEXTURE_WIDTH	1024
	#define DEFAULT_FONT_TEXTURE_HEIGHT 1024

	#define DEFAULT_FONT_GLYPH_START 32
	#define DEFAULT_FONT_GLYPH_END 256

	//=========================================================
	// Name: True Type Font
	// Description: Uses the Free Type font library to load
	// and render true type style font. It also provides
	// a method to set the glyphs to load in the library
	// as well as many other parameters to help with modifying
	// the font. Font is also generated as a texture atlas so
	// font is optimised when it gets process for rendering
	// Creator: Chris Millin
	//=========================================================
	class MLTrueTypeFont : public MLFont
	{
	public:

		MLCoreDLL MLTrueTypeFont(void);
		MLCoreDLL virtual ~MLTrueTypeFont(void);

		//=========================================================
		// Name: Release
		// Description: Releases any object that need deleting
		// or clearing
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release();

		//=========================================================
		// Name: Load Font
		// Description: Loads a font file (if valid). Pre loading
		// Glyphs can also be done during the load. If the character
		// set is set but loadGlyphs is set to false then glyphs
		// wont load and the character set will be ignored. 
		// The default texture size is 1024 so if this needs to be
		// changed do it before font loading.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadFont(const MLKernel::MLVirtualFile* File, const ML_BOOL loadGlyphs = ML_FALSE, 
			const ML_UINT Size = DEFAULT_FONT_SIZE, const ML_STRING& CharacterSet = ML_EMPTY_STRING);

		//=========================================================
		// Name: Set Texture Height
		// Description: Sets the value to be used for the texture
		// atlas height.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetTextureHeight(const ML_UINT height)		{ m_textureHeight	= height; }

		//=========================================================
		// Name: Set Texture Width
		// Description: Sets the value to be used for the texture
		// atlas width.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetTextureWidth(const ML_UINT width)			{ m_textureWidth	= width;  }
	
		//=========================================================
		// Name: Set Size
		// Description: Sets the size value for the font. Cant be 
		// changed once the glyphs have been generate without 
		// regenerating the font again.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetSize(const ML_UINT size)			{ m_fontSize = size;   }

		//=========================================================
		// Name: Get Padding X 
		// Description: Returns the value used for padding in the
		// X axis when generating the glyphs.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT GetPaddingX()	const				{ return m_paddingX; }

		//=========================================================
		// Name: Get Padding Y 
		// Description: Returns the value used for padding in the
		// Y axis when generating the glyphs. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT GetPaddingY()	const				{ return m_paddingY; }

		//=========================================================
		// Name: Set Padding X
		// Description: Set the value to use for padding around
		// glyphs X axis when generating the glyph atlas. Cant be
		// changed without re generating the glyph atlas again
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetPaddingX(const ML_UINT amount)	{ m_paddingX = amount; }

		//=========================================================
		// Name: Set Padding Y
		// Description: Set the value to use for padding around
		// glyphs Y axis when generating the glyph atlas. Cant be
		// changed without re generating the glyph atlas again
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetPaddingY(const ML_UINT amount)	{ m_paddingY = amount; }

		//=========================================================
		// Name: Load Glyphs
		// Description: Will load the glyphs that are placed in
		// character string. Duplicate glyphs will be added twice
		// to the texture but not to the texture list so be careful
		// to not use the same glyphs
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadGlyphs(const ML_STRING& characters);

		//=========================================================
		// Name: Load Default Glyphs
		// Description: Will load a set of default glyphs from
		// the range of 32 to 256
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadDefaultGlyphs();

		//=========================================================
		// Name: Get Kerning
		// Description: If the font supports kerning check it
		// to find the value.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVector2f GetKerning(const ML_SCHAR& lastChar, const ML_SCHAR& thisChar);

		//=========================================================
		// Name: Get Glyph Sprite
		// Description: Will return a sprite with the glyph information
		// ready to render but will not have the position set.
		// Any positioning and advancing as well as kerning will have
		// to be done after the sprite is generated. Origin is also
		// set to the centure of the sprite.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLSprite GetGlyphSprite(const ML_SCHAR& character);
		MLCoreDLL virtual MLSprite GetGlyphSprite(const MLGlyph& character);

		//MLCoreDLL ML_BOOL ConvertToSignedDistance();
	private:

		//=========================================================
		// Name: Load Empty Character
		// Description: The empty character will be defined as the
		// glyph located in position 26 which is defined as a 
		// substitute character for ascii which will allow it to
		// work for unicode and non unicode cases.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadEmptyCharacter(MLTextureAtlas* atlas);

		//=========================================================
		// Name: Add Bitmap
		// Description: When the glyph bitmap is rendered by Freetype
		// it is passed to this function to be padded and added to the
		// texture atlas. If the texture atlas is full it will finalise it
		// then delete it and then create a new atlas to use. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL AtlasTexture AddBitmap(const FT_Bitmap& slot, MLTextureAtlas* atlas);

		//=========================================================
		// Name: Add Metrics
		// Description: Will add the character metric information 
		// and the atlas positioning information into the glyph
		// cache in MLFont.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AddMetrics(const ML_INT characterIndex, const FT_GlyphSlot& glythMetric, AtlasTexture& atlasInfo);

	private:

		//Font face object
		FT_Face m_fontFace;

		//X and Y padding around glyphs
		ML_UINT m_paddingX;
		ML_UINT m_paddingY;
	};

}
#endif