#ifndef MLVIRTUALFILE_H
#define MLVIRTUALFILE_H

#include "MLCore.h"
#include "MLDateTime.h"

#include <vector>
#include <shobjidl.h>
#include <shlwapi.h>

using std::string;

namespace MLKernel
{
	class MLVirtualPath;

	//=========================================================
	// Name: File
	// Description: Structure stores basic information a file
	// on the system
	// Creator: Chris Millin
	//=========================================================
	struct MLFile
	{

		//=========================================================
		// Name: File Name
		// Description: Stores the file name with extention
		// e.g tmp.txt
		// Creator: Chris Millin
		//=========================================================
		ML_STRING FileName;

		//=========================================================
		// Name: Full Path
		// Description: Stores the full path address of the file
		// (if using root path this will be not start with a drive
		// letter)
		// Creator: Chris Millin
		//=========================================================
		ML_STRING FullPath;

		//=========================================================
		// Name: Extention 
		// Description: Stores the extension format of the file
		// Creator: Chris Millin
		//=========================================================
		ML_STRING Extention;

		//=========================================================
		// Name: Attribute
		// Description: Stores any attributes about the file such
		// as if it is compressed or not
		// Creator: Chris Millin
		//=========================================================
		ML_UINT		Attribute;

		//=========================================================
		// Name: Create
		// Description: Date the file was created 
		// Creator: Chris Millin
		//=========================================================
		MLDateTime	Created;

		//=========================================================
		// Name: Accessed
		// Description: Date the file was last accessed
		// Creator: Chris Millin
		//=========================================================
		MLDateTime	Accessed;

		//=========================================================
		// Name: Last write
		// Description: Date the file was last modified
		// Creator: Chris Millin
		//=========================================================
		MLDateTime	LastWrite;

		//=========================================================
		// Name: Size 
		// Description: The byte size of the file 
		// Creator: Chris Millin
		//=========================================================
		ML_UINT		Size;

		//=========================================================
		// Name: Exists
		// Description: True if the file exists on the file system
		// false if not
		// Creator: Chris Millin
		//=========================================================
		ML_BOOL		Exists;

		//=========================================================
		// Name: Open
		// Description: True if the file has been opened for reading
		// Creator: Chris Millin
		//=========================================================
		ML_BOOL		IsOpen;

		//=========================================================
		// Name: File
		// Description: The file pointer
		// Creator: Chris Millin
		//=========================================================
		FILE*		File;

		MLFile()
		{
			FileName.clear();
			FullPath.clear();
			Extention.clear();
			File = NULL;
			IsOpen = ML_FALSE;
			Exists = ML_FALSE;
			Attribute = 0;
		}
	};

	//=========================================================
	// Name: File Access
	// Description: Defines the type of access to the file
	// from fopen
	// Creator: Chris Millin
	//=========================================================
	enum MLFileAccess
	{
		ReadOnly		= 0x72,
		ReadWrite		= 0x2b72,
		WriteOnly		= 0x77,
		AppendOnly		= 0x61,
		ReadWriteNew	= 0x2b77,
		AppendRead		= 0x2b61,
	};

	//=========================================================
	// Name: Seek Mode
	// Description: Defines what direction and position the 
	// seek command should be called from
	// Creator: Chris Millin
	//=========================================================
	enum MLSeekMode
	{
		Forward		= SEEK_CUR,
		Backwards	= SEEK_CUR,
		ToEnd		= SEEK_END,
		ToStart		= SEEK_SET,
	};

	//=========================================================
	// Name: Virtual File
	// Description: The virtual file contains information about
	// a valid file location. Reading and writing to the file
	// can also be made if required.
	// Creator: Chris Millin
	//=========================================================
	class MLVirtualFile
	{
	public:

		MLCoreDLL MLVirtualFile(void);
	
		//=========================================================
		// Name: Constructor String
		// Description: Sets the file from a single file name 
		// location.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualFile(const ML_STRING& file);

		//=========================================================
		// Name: Constructor String
		// Description: Sets the file name and path from two strings.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualFile(const ML_STRING& Name, const ML_STRING& Path);

		//=========================================================
		// Name: Constructor Virtual Path
		// Description: Sets the file name from a string but the
		// path is defined by a virtual path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualFile(const ML_STRING& Name, const MLVirtualPath* Path);

		MLCoreDLL virtual ~MLVirtualFile(void);

		//=========================================================
		// Name: Release
		// Description: Releases the object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Open File Default
		// Description: Opens the file in standard mode if the file
		// location has already been specified and the file has not
		// already been opened.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile();
	
		//=========================================================
		// Name: Open File
		// Description: Opens the file using the defined mode and 
		// if data is stored binary or text. Works in the same way
		// as OpenFile with no parameters in that that file has 
		// to be specified first.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile(const MLFileAccess& mode, const ML_BOOL binary);
	
		//=========================================================
		// Name: Open File Default String
		// Description: Opens the file defined in standard read
		// only and in text format
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile(const ML_STRING& Name, const ML_STRING& Path);
	
		//=========================================================
		// Name: Open File Default Virtual Path
		// Description: Opens the file defined in standard read only
		// and text format from a virtual path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile(const ML_STRING& Name, const MLVirtualPath* Path);
	
		//=========================================================
		// Name: Open File String
		// Description: Opens the file using the set mode and choice
		// between binary or text using a string path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile(const ML_STRING& Name, const ML_STRING& Path, const MLFileAccess& mode, const ML_BOOL binary);
	
		//=========================================================
		// Name: Open File Virtual Path
		// Description: Opens the file using the set mode and choice
		// between binary or text using a virtual path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL OpenFile(const ML_STRING& Name, const MLVirtualPath* Path, const MLFileAccess& mode, const ML_BOOL binary);

		//=========================================================
		// Name: Set File String
		// Description: Will Set the file to be accessed if the
		// file is valid.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetFile(const ML_STRING& Name, const ML_STRING& Path);
	
		//=========================================================
		// Name: Set File Virtual Path
		// Description: Will Set the file to be accessed if the
		// file is valid from a virtual path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetFile(const ML_STRING& Name, const MLVirtualPath* Path);
	
		//=========================================================
		// Name: Close File
		// Description: Will close the file if it is open.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL CloseFile() const;

		//=========================================================
		// Name: Write Data
		// Description: will write the specified amount of data to
		// the file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL WriteData(void* Data, const ML_UINT ItemSize, const ML_UINT Size);
	
		//=========================================================
		// Name: Write String
		// Description: will write the specified string to
		// the file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL WriteString(const ML_STRING& string);

		//=========================================================
		// Name: Read Data
		// Description: Will read the specified amount of data
		// and return a pointer storing it.
		// Note: The return pointer must be deleted if not used any more
		// after the function call. does not point directly to the file
		// so data in the pointer can be modified.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void*	ReadData(const ML_UINT ItemSize, const ML_UINT Size);

		//=========================================================
		// Name: Read String
		// Description: will Read a string the size of charCount
		// from the file and return it
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING ReadString(const ML_UINT charCount);

		//=========================================================
		// Name: Seek
		// Description: Will seek to a position in the file based
		// on the defined mode and the amount.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL Seek(const MLSeekMode& mode, const ML_USHORT amount = 0);
	
		//=========================================================
		// Name: End Of File
		// Description: Checks if the end of the file has been 
		// reached.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL EndOfFile() const;

		//=========================================================
		// Name: Move File String
		// Description: Will move the file to the specified path
		// location if it is valid.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL MoveFile(const ML_STRING& ToPath);
	
		//=========================================================
		// Name: Move File Virtual Path
		// Description: Will move the file to the specified path
		// location in the virtual path if it is valid.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL MoveFile(const MLVirtualPath* ToPath);

		//=========================================================
		// Name: Copy File String
		// Description: Will copy the file to the specified path
		// location if it is valid.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL CopyFile(const ML_STRING& ToPath);
	
		//=========================================================
		// Name: Copy File Virtual Path
		// Description: Will copy the file to the specified path
		// location in the virtual path if it is valid
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL CopyFile(const MLVirtualPath* ToPath);

		//=========================================================
		// Name: Rename File
		// Description: Will rename the file.
		// Note: At the moment this will require the extension to be
		// included.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL RenameFile(const ML_STRING& NewName);

		//=========================================================
		// Name: Delete File
		// Description: Will delete the current file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL DeleteFile();

		//This will be used when MLMD5 is implemented. It will use the file size to create an
		//MD5 which can be checked against an existing MD5. A database of MD5's can be generated
		//for all game files and this will be used to check if the file is valid
		//ML_BOOL VarifyMD5(MLMD5Hash* Hash); 

		//=========================================================
		// Name: Last Written
		// Description: Returns how much data was last written to 
		// the file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT LastWritten() const { return m_lastWrite; }

		//=========================================================
		// Name: Last Read
		// Description: Returns how much data was last read from the
		// file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT LastRead() const	{ return m_lastRead; }

		//=========================================================
		// Name: Exists
		// Description: Checks to see if the file location exists 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL Exists() const;
	
		//=========================================================
		// Name: Is Open
		// Description: Returns true is the current File is open
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsOpen() const;

		//=========================================================
		// Name: Full File Path
		// Description: Returns full file path of the file
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING FullFilePath() const;

		//=========================================================
		// Name: Get File Name
		// Description: Returns Name of the file from the MLFile
		// structure.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetFileName() const { if (m_file) return m_file->FileName; else return ML_STR(""); }

		//=========================================================
		// Name: Set File Name
		// Description: Changes the name of the file to obtain
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetFileName(const ML_STRING& name);

		//=========================================================
		// Name: Get File Name
		// Description: Returns extension of the file from the MLFile
		// structure.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetFileExtention() const { if (m_file) return m_file->Extention; else return ML_STR(""); }

		//=========================================================
		// Name: Set File Extention
		// Description: Changes the extension of the file to obtain
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetFileExtention(const ML_STRING& ext);
		
		//=========================================================
		// Name: Get File Size
		// Description: Returns the size of the file in bytes
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT GetFileSize() const { if(m_file) return m_file->Size; else return 0; }

		//=========================================================
		// Name: To String
		// Description: Returns the object as a string
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING ToString() const;

	protected:

		MLCoreDLL virtual ML_BOOL CreateNewFile(const ML_STRING& fullFilePath);

		//=========================================================
		// Name: Build File Info
		// Description: Will build the current file information
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL BuildFileInfo();

		//for windows
		//=========================================================
		// Name: Create ShellItem
		// Description: Creates a windows file shellitem used
		// for defining a folder
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual IShellItem* CreateShellItem(const ML_STRING& file);

		//for windows
		//=========================================================
		// Name: Create File Operation
		// Description: Will create a windows file operation for use
		// with the shellitems to delete and create folders
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual IFileOperation* CreateOperation();

		//=========================================================
		// Name: Is File Closed and Valid
		// Description: Error checking encapsulation
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsFileClosedandValid()	{ return (!m_file->File && !m_file->IsOpen && !m_file->FullPath.empty() && m_file->Exists); }
	
		//=========================================================
		// Name: Is File Open
		// Description: Error checking encapsulation
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsFileOpen()	const		{ return (m_file->File && m_file->IsOpen); }

	protected:

		//=========================================================
		// Name: File
		// Description: Current active file 
		//=========================================================
		MLFile* m_file;

		//=========================================================
		// Name: Last Write
		// Description: Defines how much data was last written to the
		// file
		//=========================================================
		ML_UINT m_lastWrite;

		//=========================================================
		// Name: Last Read 
		// Description: Defines how much data was last read from the
		// file.
		//=========================================================
		ML_UINT m_lastRead;
	};
}

#endif


