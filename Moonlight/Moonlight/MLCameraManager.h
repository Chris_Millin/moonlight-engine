#ifndef MLCAMERAMANAGER_H
#define MLCAMERAMANAGER_H

#include "MLIncludes.h"
#include "MLGLContext.h"
#include "MLCamera.h"

#include <map>

//to-do 
//Delete and remove cameras

namespace MLRender
{
	/// <summary>	The Default camera name. </summary>
	#define ML_DEFAULT_CAMERA ML_STR("DefaultCamera")

	///-------------------------------------------------------------------------------------------------
	/// <summary>	The Camera Manager will store a list of
	///				cameras which can be switched at any time. This is
	///				integrated into the sprite batch so the no really need to
	///				do anything to make the active camera effect the objects
	///				apart from set its properties. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLCameraManager
	{
	public:
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will create a DefaultCamera which will
		///				be positioned at 0,0 with no rotation or scale. This is
		///				also set to the active camera so the camera manager can
		///				be used right away without any problem. If the camera
		///				changes it can always be set back to the default camera
		///				by using DefualtCamera as the name. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if its successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL Initalise(void);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will remove (and should delete it if its
		///				created within the manager). </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Name">	The name. </param>
		///
		/// <returns>	Re. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL RemoveCamera(const ML_STRING& Name);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	This will delete all the cameras created
		///				within the manager but will not delete cameras added
		///				directly to the manager. The camera list is then cleared. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if its successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL RemoveCameras();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will create a new camera and return the UUID name. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	returns the UUID Name key. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_STRING CreateCamera();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will create a camera within the camera manager. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Name">	 The name of the camera. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL CreateCamera(const ML_STRING& Name);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will add a camera created outside of the manager
		///				and give it a UUID name. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="camera">	The camera object to add. </param>
		///
		/// <returns>	Returns the name of the camera. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_STRING AddCamera(MLCamera* camera);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will add a camera created outside of the manager. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Name">  	The name of the camera to add. </param>
		/// <param name="camera">	The camera object to add. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL AddCamera(const ML_STRING& Name, MLCamera* camera);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Check if a camera exists. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Name">	The name of the camera. </param>
		///
		/// <returns>	Returns true if the camera is loaded. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL HasCamera(const ML_STRING& Name);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will set one of the cameras to an active camera
		///				(needs to be a bit better than this. should find it via
		///				a iterator and then if its found change it and if not
		///				false is returned). </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Name">	The name of the camera to set as active. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetActiveCamera(const ML_STRING& Name);
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the current active camera object  </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	null if it fails, else the active camera. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static MLCamera* GetActiveCamera() { return m_activeCamera; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the name of the current active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	The active camera name. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static const ML_STRING& GetActiveCameraName() { return m_activeCameraName; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	will print the active camera matrix to the console. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	A ML_BOOL. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL PrintCameraMatrix();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X position of the camera. </param>
		/// <param name="y">	Y position of the camera. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraPos(const ML_FLOAT x, const ML_FLOAT y)		{ m_activeCamera->SetPosition(x, y); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="pos">	The vector2 position. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraPos(const MLVector2f& pos)						{ m_activeCamera->SetPosition(pos); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X position of the camera. </param>
		/// <param name="y">	Y position of the camera. </param>
		/// <param name="z">	Z position of the camera. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraPos(const ML_FLOAT x, const ML_FLOAT y, const ML_FLOAT z)	{ m_activeCamera->SetPosition(x, y, z); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="pos">	The vector3 position. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraPos(const MLVector3f& pos)						{ m_activeCamera->SetPosition(pos); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the rotation of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="rot">	The rotation amount in radians. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraRotation(const ML_FLOAT rot)					{ m_activeCamera->SetRotation(rot); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="XY">	The scale value of both x and y. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraScale(const ML_FLOAT XY)						{ m_activeCamera->SetScale(XY); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X scale value. </param>
		/// <param name="y">	Y scale value. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraScale(const ML_FLOAT x, const ML_FLOAT y)		{ m_activeCamera->SetScale(x, y); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="scale">	The scale as a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraScale(const MLVector2f& scale)					{ m_activeCamera->SetScale(scale); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the active camera matrix. Will not recompute matrix
		/// 			using the current positional information until it is modified. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Matrix">	The matrix to set the camera to. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void SetActiveCameraMatrix(const MLMatrix4f Matrix)				{ m_activeCamera->SetMatrix(Matrix); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the active camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to move the camera in the x coordinate. </param>
		/// <param name="y">	Amount to move the camera in the y coordinate. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void MoveActiveCamera(const ML_FLOAT x, const ML_FLOAT y)			{ m_activeCamera->MoveCamera(x, y); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the active camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="amount">	Amount to move the camera in via a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void MoveActiveCamera(const MLVector2f& amount)					{ m_activeCamera->MoveCamera(amount); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the active camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to move the camera in the x coordinate. </param>
		/// <param name="y">	Amount to move the camera in the y coordinate. </param>
		/// <param name="z">	Amount to move the camera in the z coordinate. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void MoveActiveCamera(const ML_FLOAT x, const ML_FLOAT y, const ML_FLOAT z)		{ m_activeCamera->MoveCamera(x, y, z); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the active camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="amount">	Amount to move the camera in via a vector3. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void MoveActiveCamera(const MLVector3f& amount)					{ m_activeCamera->MoveCamera(amount); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the active camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="XY">	Zoom in by the x and y. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void ZoomActiveCamera(const ML_FLOAT XY)							{ m_activeCamera->ZoomCamera(XY); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the active camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to zoom in by in the x coordinates. </param>
		/// <param name="y">	Amount to zoom in by in the y coordinates. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void ZoomActiveCamera(const ML_FLOAT x, const ML_FLOAT y)			{ m_activeCamera->ZoomCamera(x, y); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the active camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="scale">	Amount to zoom in by in the x and y via a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void ZoomActiveCamera(const MLVector2f& scale)						{ m_activeCamera->ZoomCamera(scale); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Modify the rotation of the active camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="rot">	Amount to change the rotation by. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static void RotateActiveCamera(const ML_FLOAT rot)						{ m_activeCamera->RotateCamera(rot); }

	private:
		MLCoreDLL MLCameraManager(void) {}
		MLCoreDLL ~MLCameraManager(void) {}
	
private:

		/// <summary>	The active camera name. </summary>
		MLCoreDLL static ML_STRING m_activeCameraName;
		/// <summary>	The active camera object. </summary>
		MLCoreDLL static MLCamera* m_activeCamera;
		/// <summary>	The camera map object containing all the cameras. </summary>
		MLCoreDLL static std::map<ML_STRING, MLCamera*> m_cameraDictionary;
	};
}

#endif