#ifndef MLPOSTPROCESS_H
#define MLPOSTPROCESS_H

#include "MLIncludes.h"
#include "MLFrameBuffer.h"
#include "MLShader.h"
#include "MLRenderTarget.h"
#include "MLGraphics.h"

#include <map>

#define ML_LOG_POST_PROCESS ML_STR("Post Process")

namespace MLRender
{

	//=========================================================
	// Name: ML Post Process Input
	// Description: A structure to describe a texture input for
	// the post process. The name must be the same as one of the
	// existing post processes otherwise the texture will not
	// be attached.
	// Creator: Chris Millin
	//=========================================================
	struct MLPostProcessInput
	{
		ML_INT TextureID;
		ML_INT ShaderTextureIndex;
		ML_UINT GLTextureIndex;
		ML_UINT GLTextureType;
		ML_U8STRING InputName;
	};

	//=========================================================
	// Name: Post Process
	// Description: The post process class is nothing more than
	// an abstract class to build a post process from. It can be
	// added to the post process manager which can handle post or
	// pre render post processing (pre render being rendering the
	// scene to a texture
	// Creator: Chris Millin
	//=========================================================
	class MLPostProcess : MLRenderTarget
	{
	public:
		MLCoreDLL MLPostProcess(void);
		MLCoreDLL virtual ~MLPostProcess(void);

		//=========================================================
		// Name: Release
		// Description: Releases all objects in the post process.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Initalise
		// Description: Will initalise all the frame buffer objects
		// ready for rendering.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Initalise(const MLFBOTypes enableFBO = MLFBOTypes::ColourBuffer);

		//=========================================================
		// Name: Being Process
		// Description: Will do any pre-drawing initialisation.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Bind();

		//=========================================================
		// Name: Draw
		// Description: Will draw a quad the size of the screen
		// and render it with the post process shader. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Draw(MLGraphics* batch);

		//=========================================================
		// Name: End Process
		// Description: Will do any end processing.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Unbind();

		//=========================================================
		// Name: Add Input
		// Description: Add an input texture to the post process.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL AddInput(const ML_U8STRING& name, const ML_INT shaderTextureIndex, 
			const ML_INT textureID, const ML_UINT glTextureIndex, const ML_UINT glTextureType);

		//=========================================================
		// Name: Set Uniform Callback
		// Description: Adds a uniform function call back for
		// post processing
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void SetUniformCallback(UniformCallBack callback) { m_UniformCallback = callback; }

		//=========================================================
		// Name: Get Uniform Callback
		// Description: Returns the uniform callback used for the
		// post process
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual UniformCallBack GetUniformCallback() { return m_UniformCallback; }

		//=========================================================
		// Name: Remove Uniform Callback
		// Description: Removes the uniform callback
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void RemoveUniformCallback() { m_UniformCallback = NULL; }

		//=========================================================
		// Name: Get Input
		// Description: Will return an input if the name exists.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLPostProcessInput* GetInput(const ML_U8STRING& name);

		//=========================================================
		// Name: Get Inputs
		// Description: returns all the added inputs.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<MLPostProcessInput*> GetInputs() { return m_PostProcessTextureInput; }

		//=========================================================
		// Name: Set Shader
		// Description: will set the post process shader.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void SetShader(MLShader* shader) { m_shader = shader; }

		//=========================================================
		// Name: Get Shader
		// Description: Returns the post process shader.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLShader* GetShader() { return m_shader; }

		//=========================================================
		// Name: Has Finished
		// Description: Returns true if the post process has finished.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL HasFinished() { return m_finished; }

		//=========================================================
		// Name: Has Started
		// Description: Returns true if the post process has been
		// started.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL HasStarted()  { return m_started; }
	
	protected:

		std::vector<MLPostProcessInput*> m_PostProcessTextureInput; 

		MLShader* m_shader;

		UniformCallBack m_UniformCallback;

		ML_BOOL m_postRender;
		ML_BOOL m_started;
		ML_BOOL m_finished;
	};

}

#endif

