#include "MLController.h"

namespace MLInput
{

	MLCoreDLL MLController::MLController()
	{
		//The controller ID is initalised to 5 to make sure that
		//it does not check a valid controller ID
		m_gamePadID = 5;
		for (ML_INT i = 0; i < 16; i++)
		{
			m_buttonCurrentStates[i] = ML_FALSE;
			m_buttonPreviousStates[i] = ML_FALSE;
		}
	}

	MLCoreDLL MLController::MLController(const ML_BYTE ID)
	{
		//The controller ID is initalised to 5 to make sure that
		//it does not check a valid controller ID
		if (!SetControllerID(ID))
			m_gamePadID = 5;

		for (ML_INT i = 0; i < 16; i++)
		{
			m_buttonCurrentStates[i] = ML_FALSE;
			m_buttonPreviousStates[i] = ML_FALSE;
		}
	}


	MLCoreDLL MLController::~MLController(void)
	{
	}

	MLCoreDLL ML_BOOL MLController::SetControllerID(const ML_BYTE ID)
	{
		//if the controller id is greater than 4 or less than 1
		//then there is an error
		if (ID > 4) return ML_FALSE;
		if (ID < 1) return ML_FALSE;

		//since the ID actually ranges from 0-3 -1 is needed just to move
		//it to the correct ID
		m_gamePadID = ID - 1;

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLController::IsControllerActive() const 
	{
		//stores the result of the function call
		DWORD dwResult;

		//the controller state structure
		XINPUT_STATE state;
		//zero the memory
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		// Simply get the state of the controller from XInput.
		dwResult = XInputGetState(m_gamePadID, &state);

		//if it was a success then the controller is connected
		//if not then false is returned
		if (dwResult == ERROR_SUCCESS)
			return ML_TRUE;
		else
			return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLController::IsButtonHeld(const ML_BYTE button)
	{
		//If the ID is not 5 then read the buttons and then return the
		//state of that button
		if (m_gamePadID != 5)
		{
			ReadButtons();
			return m_buttonCurrentStates[button];
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLController::IsButtonPressed(const ML_BYTE button)
	{
		//If the ID is not 5 then read the buttons and then return the
		//state of that button
		if (m_gamePadID != 5)
		{
			ReadButtons();
			if (m_buttonCurrentStates[button] && !m_buttonPreviousStates[button])
				return ML_TRUE;
			else
				return ML_FALSE;
		}

		return ML_FALSE;
	}

	MLCoreDLL MLVector2f MLController::GetAnalog(const MLControllerAnalogs& analog)
	{
		//checks to see if the controller ID is valid
		if (m_gamePadID != 5)
		{
			//checks to see which analog value is needed
			if (analog == MLControllerAnalogs::LeftAnalog)
			{
				//return a normailsed version 
				return NormaliseAnalog(m_axis[0]);
			}
			else if (analog == MLControllerAnalogs::RightAnalog)
			{
				return NormaliseAnalog(m_axis[1]);
			}
			else if (analog == MLControllerAnalogs::TriggerAnalog)
			{
				return m_axis[2] / 255;
			}
		}


		return MLVector2f(0, 0);
	}

	MLCoreDLL MLVector2f MLController::NormaliseAnalog(MLVector2f inVec)
	{
		//check if the controller is outside a circular dead zone
		if (inVec.x > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		{
			//clip the magnitude at its expected maximum value
			if (inVec.x > 32767) m_axis[0].x = 32767;

			//adjust magnitude relative to the end of the dead zone
			inVec.x -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

			//optionally normalize the magnitude with respect to its expected range
			//giving a magnitude value of 0.0 to 1.0
			inVec.x = inVec.x / (32767 - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		}
		else if (inVec.x < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		{
			//clip the magnitude at its expected maximum value
			if (inVec.x < -32767) inVec.x = -32767;

			//adjust magnitude relative to the end of the dead zone
			inVec.x += XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

			//optionally normalize the magnitude with respect to its expected range
			//giving a magnitude value of 0.0 to 1.0
			inVec.x = -(inVec.x / (-32767 + XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE));
		}
		else//if the controller is in the deadzone zero out the magnitude
		{
			inVec.x = 0.0;
		}
		//check if the controller is outside a circular dead zone
		if (inVec.y > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		{
			//clip the magnitude at its expected maximum value
			if (inVec.y  > 32767) inVec.y = 32767;

			//adjust magnitude relative to the end of the dead zone
			inVec.y -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

			//optionally normalize the magnitude with respect to its expected range
			//giving a magnitude value of 0.0 to 1.0
			inVec.y = -(inVec.y / (32767 - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE));
		}
		else if (inVec.y < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		{
			//clip the magnitude at its expected maximum value
			if (inVec.y < -32767) inVec.y = -32767;

			//adjust magnitude relative to the end of the dead zone
			inVec.y += XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

			//optionally normalize the magnitude with respect to its expected range
			//giving a magnitude value of 0.0 to 1.0
			inVec.y = inVec.y / (-32767 + XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		}
		else//if the controller is in the deadzone zero out the magnitude
		{
			inVec.y = 0.0;
		}
		return inVec;
	}

	MLCoreDLL ML_BOOL MLController::SetForceFeedback(const ML_FLOAT amount)
	{
		//create a vibration structure
		XINPUT_VIBRATION vibration;
		//zero the memory
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		vibration.wLeftMotorSpeed = (ML_INT)(amount * 65535.0f); // use any value between 0-65535 here
		vibration.wRightMotorSpeed = (ML_INT)(amount * 65535.0f); // use any value between 0-65535 here
		//set the vibration
		XInputSetState(0, &vibration);
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLController::SetForceFeedback(const ML_FLOAT amountL, const ML_FLOAT amountR)
	{
		//create a vibration structure
		XINPUT_VIBRATION vibration;
		//zero the memory
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
		vibration.wLeftMotorSpeed = (ML_INT)(amountL * 65535.0f); // use any value between 0-65535 here
		vibration.wRightMotorSpeed = (ML_INT)(amountR * 65535.0f); // use any value between 0-65535 here
		//set the vibration
		XInputSetState(0, &vibration);
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLController::ReadButtons()
	{
		//stored the function call result
		DWORD dwResult;
		//The controller state structure
		XINPUT_STATE state;
		//zero the memory
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		// Simply get the state of the controller from XInput.
		dwResult = XInputGetState(0, &state);

		//if the result was a success the read the keys
		if (dwResult == ERROR_SUCCESS)
		{
			//gets the key bit states
			for (ML_INT i = 0; i < 16; i++)
			{
				//if the button is not pressed it is false and released so it cant have been pressed
				//the button is then pressed it is not true and not released and has also been pressed
				//the button is held it is true and not released but since it was not released and pressed is true 
				m_buttonPreviousStates[i] = m_buttonCurrentStates[i];
				m_buttonCurrentStates[i] = (ML_BOOL)(state.Gamepad.wButtons & (1 << i));

			}

			//get the analog axis values
			m_axis[0].x = state.Gamepad.sThumbLX;
			m_axis[0].y = state.Gamepad.sThumbLY;

			m_axis[1].x = state.Gamepad.sThumbRX;
			m_axis[1].y = state.Gamepad.sThumbRY;

			m_axis[2].x = state.Gamepad.bLeftTrigger;
			m_axis[2].y = state.Gamepad.bRightTrigger;
		}
		else
			return ML_FALSE;

		return ML_TRUE;
	}
}

