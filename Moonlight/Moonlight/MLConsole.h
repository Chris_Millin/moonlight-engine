#ifndef MLCONSOLE_H
#define MLCONSOLE_H

#include "MLCore.h"
#include "MLDateTime.h"

#include <vector>
#include <iostream>
#include <stdarg.h>
#include <dbgHelp.h>
#include <comdef.h>

#define ML_TRACE(area, status, fmt, ...) MLKernel::MLConsole::Log(__FILE__, __FUNCSIG__, __LINE__, area, status, fmt, ##__VA_ARGS__)
#define ML_TRACE_STACK(area) MLKernel::MLConsole::LogStack(__FILE__, __FUNCSIG__, __LINE__, area)
#define ML_TRACE_STACK_COUNT(area, count) MLKernel::MLConsole::LogStack(__FILE__, __FUNCSIG__, __LINE__, area, count)

#define ML_MAX_CONSOLE_CHAR 1024

#define ML_STATUS_TYPES 12

namespace MLKernel
{
	class MLVirtualFile;
	class MLVirtualPath;
	class MLThread;

	//=========================================================
	// Name: Log Function
	// Description: Log Callback Function. Should be used
	// to obtain log information just before the log is made
	// to the console. 
	// Creator: Chris Millin
	//=========================================================
	typedef void(*Logfn)(const ML_STRING& Area, const ML_STRING& Status, const ML_STRING& text);

	//=========================================================
	// Name: Console Status
	// Description: Used to set the console status e.g. (Error)
	// they are also bitwise flags which can be used to disable
	// or enable the log from being made.
	// Creator: Chris Millin
	//=========================================================
	enum MLConsoleStatus
	{
		Task		= 0x01,
		Event		= 0x02,
		Thread		= 0x04,
		Warning		= 0x08,
		Exception	= 0x10,
		Error		= 0x20,
		FatalError	= 0x40,
		Progress	= 0x80,
		Information = 0x100,
		FileAccess	= 0x200,
		Debug		= 0x400,
		Trace		= 0x800

	};

	//=========================================================
	// Name: Console Colours
	// Description: Colour ID's for the windows based console
	// (maybe other consoles as well)
	// Creator: Chris Millin
	//=========================================================
	enum MLConsoleColour
	{
		ccBlack		=0,
		ccDBlue		=1,
		ccDGreen	=2,
		ccDCyan		=3,
		ccDRed		=4,
		ccDPurple	=5,
		ccDYellow	=6,
		ccDWhite	=7,
		ccGray		=8,
		ccBlue		=9,
		ccGreen		=10,
		ccCyan		=11,
		ccRed		=12,
		ccPurple	=13,
		ccYellow	=14,
		ccWhite		=15
	};

	//=========================================================
	// Name: Console Status String
	// Description: Stores the strings for each console
	// status. log2 the a console status and it will be the index
	// of this list.
	// Creator: Chris Millin
	//=========================================================
	const static ML_STRING MLConsoleStatusString[ML_STATUS_TYPES] = 
	{
		ML_STR("Task"), ML_STR("Event"), ML_STR("Thread"), ML_STR("Warning"),
		ML_STR("Exception"), ML_STR("Error"), ML_STR("Fatal Error"),
		ML_STR("Progress"), ML_STR("Information"), ML_STR("File Access"),
		ML_STR("Debug"), ML_STR("Trace") 
	};

	//=========================================================
	// Name: Console Status Colour
	// Description: Console for each console status
	// Creator: Chris Millin
	//=========================================================
	const static ML_BYTE MLConsoleStatusColour[ML_STATUS_TYPES] =
	{
		/*Task*/3, /*Event*/2, /*Thread*/8, /*Warning*/14, /*Exception*/6,
		/*Error*/12, /*Fatal Error*/4, /*Progress*/10, /*Information*/11,
		/*FileAccess*/13, /*Debug*/7, /*Trace*/9
	};

	//=========================================================
	// Name: Console
	// Description: Static class which enables logging to a
	// windows based console, log file, and support for a callback
	// to allow for external logging. Windows stack tracing can
	// also be made from the console.
	// Creator: Chris Millin
	//=========================================================
	class MLConsole
	{
	public:

		//=========================================================
		// Name: Initalise Console
		// Description: Initalises the console and also sets the log
		// output which will be from the executable path in a folder
		// called Logs.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void InitaliseConsole();

		//=========================================================
		// Name: Disable STD Console
		// Description: Will disable the log from being printed to
		// the console window.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void DisableSTDConsole()	{ m_stdConsole = ML_FALSE; }

		//=========================================================
		// Name: Enable
		// Description: Will enable the log to be printed to the 
		// console window (true by default from initalise)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void EnableSTDConsole()	{ m_stdConsole = ML_TRUE; }

		//=========================================================
		// Name: Shutdown
		// Description: Will shutdown the console closing the
		// log file.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Shutdown();

		//=========================================================
		// Name: Set Ignore Flag
		// Description: Set a console status flag to ignore or enable
		// by default all are enabled. multiple flags can be disabled
		// but not through this call.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void SetIgnoreFlag(const MLConsoleStatus& Status, const ML_BOOL Ignore);

		//=========================================================
		// Name: Log
		// Description: Will log the information passed to it.
		// Area should be set to the class or place the log is called
		// from and will be the first thing to be displayed in the 
		// log line [Area] (Status) Message. The message will be
		// displayed in white by the std console all the time while
		// the status will change the start colour of the log.
		// Warnings, Errors and Fatal Errors will also use
		// file, function and line to add extra information to
		// help with finding the bug.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Log(const ML_U8STRING& file, const ML_U8STRING& function, const ML_UINT& line,
			const ML_STRING& Area, const MLConsoleStatus& Status, const ML_STRING format, ...);

		//=========================================================
		// Name: Log
		// Description: Will log the information passed to it.
		// Area should be set to the class or place the log is called
		// from and will be the first thing to be displayed in the 
		// log line [Area] (Status) Message. The message will be
		// displayed in white by the std console all the time while
		// the status will change the start colour of the log.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Log(const ML_STRING& Area, const MLConsoleStatus& Status, const ML_STRING format, ...);

		//=========================================================
		// Name: Log Stack
		// Description: If a stack trace if required to help with the
		// error then this should be called. Similar to the Log it
		// also requires the Area to be set to make it easier to
		// know where the trace is being made from. All Stack log
		// lines will be processed as a error. Frames can be set to
		// loop through a select amount of frames back. By default
		// the frame count is set to 12 and will also stop on
		// main.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void LogStack(const ML_U8STRING& file, const ML_U8STRING& function, const ML_UINT& line, 
			const ML_STRING& Area, ML_USHORT frames = 12);

		//=========================================================
		// Name: Log Callback 
		// Description: Sets the custom log callback which will be
		// run if set when a log is made
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void LogCallback(Logfn callback) { m_logCallback = callback; }

		//=========================================================
		// Name: Get Log
		// Description: Returns a constant reference to the log
		// being stored by the console.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static const std::vector<ML_STRING>& GetLog() { return m_fullLog; }

		//=========================================================
		// Name: Update
		// Description: Updates the log with all the messages passed to it
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Update();

	private:
		//private constructor and de-constructor to prevent no static
		//creation.
		MLConsole(void);
		virtual ~MLConsole(void);

		//=========================================================
		// Name: Get Date Time String
		// Description: returns the Date Time as a valid string
		// for use with the file name.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_STRING GetDateTimeString();

		//=========================================================
		// Name: Change Console Font Colour
		// Description: Changes the STD console font colour
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void ChangeConsoleFontColour(const MLConsoleColour& Colour);

		//=========================================================
		// Name: Change Console Back Colour
		// Description: Changes the STD console back colour
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void ChangeConsoleBackColour(const MLConsoleColour& Colour);

		//=========================================================
		// Name: Current Console Font Colour
		// Description: returns the current console font colour
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_UINT CurrentConsoleFontColour();

		//=========================================================
		// Name: Current Console Back Colour
		// Description: returns the current console back colour
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_UINT CurrentConsoleBackColour();

	private:

		//bitfield for the status ignore flag
		static ML_UINT				m_ignoreFlags;
		//custom log call back function
		static Logfn				m_logCallback;
		//Log file output
		static MLVirtualFile*		m_outLog;
		//stores a list of the full log
		static std::vector<ML_STRING> m_fullLog;
		//STD Console handle
		static HANDLE				m_consoleHandle;
		//set the console to be initalises
		static ML_BOOL				m_initalised;
		//set if the STD console is enabled
		static ML_BOOL				m_stdConsole;
		//console thread object
		static ML_STRING			m_threadMutex;
	};

	ML_DWORD ConsoleThreadUpade(void* arg);
}
#endif