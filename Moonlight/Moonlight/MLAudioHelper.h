#ifndef MLAUDIOHELPER_H
#define MLAUDIOHELPER_H

#include "MLIncludes.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
};

#include <AL/al.h>
#include <AL/alc.h>

namespace MLAudio
{

/// <summary>	Audio Log Name. </summary>
#define ML_LOG_AUDIO ML_STR("Audio")
/// <summary>	OpenAL Log Name. </summary>
#define ML_LOG_OPENAL ML_STR("OpenAL")

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Used as a definition for the Audio Listener </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	struct MLAudioListener
	{
		/// <summary>	The Volume gain of the listener. </summary>
		ML_FLOAT gain;
		/// <summary>	The position of the listener. </summary>
		MLVector3f position;
		/// <summary>	The velocity of the listener. </summary>
		MLVector3f velocity;
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	This is a definition an audio source. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	struct MLAudioSource
	{
		/// <summary>	The maximum distance the sound can be heard. </summary>
		ML_FLOAT maxDistance;
		/// <summary>	How much the audio will roll off. </summary>
		ML_FLOAT rollOff;
		/// <summary>	The reference distance. </summary>
		ML_FLOAT referenceDistance;
		/// <summary>	The minimum Volume. </summary>
		ML_FLOAT minGain;
		/// <summary>	The maximum Volume. </summary>
		ML_FLOAT maxGain;
		/// <summary>	The cone outer Volume. </summary>
		ML_FLOAT coneOuterGain;
		/// <summary>	The cone inner angle audio can be heard from. </summary>
		ML_FLOAT coneInnerAngle;
		/// <summary>	The cone outer angle audio can be heard from. </summary>
		ML_FLOAT coneOuterAngle;

		/// <summary>	The position of the audio source. </summary>
		MLVector3f position;
		/// <summary>	The velocity of the audio. </summary>
		MLVector3f velocity;
		/// <summary>	The direction the audio is playing from. </summary>
		MLVector3f direction;

		/// <summary>	Loop audio. </summary>
		ML_BOOL looping;

		MLAudioSource()
		{
			maxDistance = 0.0f;
			rollOff = 0.0f;
			referenceDistance = 0.0f;
			minGain = 0.0f;
			maxGain = 0.1f;
			coneOuterGain = 0.0f;
			coneInnerAngle = 360.0f;
			coneOuterAngle = 360.0f;

			position  = MLVector3f::Zero();
			velocity  = MLVector3f::Zero();
			direction = MLVector3f::Zero();

			looping = ML_FALSE;
		}
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Stores the FFMPEG information about the
	///	FFMPEG codec. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	struct MLCodec
	{
		/// <summary>	This contains the file context for FFMpeg. It hold a few bits of information about the file
		///	but is mainly used with avformat to get more information about the file.. </summary>
		AVFormatContext*	avFmtCtx;			
		/// <summary>	This is the codec context which holds some information about the codec in the file. </summary>
		AVCodecContext*		avCodecCtx;
		/// <summary>	This I believe is the actual codec information which has function or something to decode the file. </summary>
		AVCodec*			avCodec;
		/// <summary>	This is the stream that contains that audio (or could be video but in this case should always be audio). </summary>
		AVStream*			avStream;
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Stores formatted FFMPEG frame data for it
	///	to be ready for openAL. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	struct MLAudioFrame
	{
		/// <summary>	The Audio frame data in PCM 16 format. </summary>
		ML_SHORT* Data;
		/// <summary>	The size of the audio data (not byte size). </summary>
		ML_UINT Size;
		/// <summary>	Byte size of the audio data. </summary>
		ML_UINT Bytes;
	};
}

#endif // !MLAUDIOHELPER_H