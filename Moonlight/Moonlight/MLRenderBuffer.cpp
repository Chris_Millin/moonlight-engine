#include "MLRenderBuffer.h"

namespace MLRender
{

	MLRenderBuffer::MLRenderBuffer(void)
	{
		m_bufferID = 0;
		m_bound = ML_FALSE;
	}


	MLRenderBuffer::~MLRenderBuffer(void)
	{
		Release();
	}

	MLCoreDLL ML_BOOL MLRenderBuffer::Release()
	{
		if (m_bufferID != 0)
		{
			glDeleteRenderbuffers(1, &m_bufferID);
			m_bufferID = 0;
		}

		return ML_TRUE;
	}

	MLCoreDLL void MLRenderBuffer::CreateBuffer(const GLenum format, const GLsizei width, const GLsizei height)
	{
		if (m_bufferID == 0)
			glGenRenderbuffers(1, &m_bufferID);

		m_format = format;
		Bind();
		glRenderbufferStorage(GL_RENDERBUFFER, m_format, width, height);
		Unbind();
	}

	MLCoreDLL void MLRenderBuffer::CreateMultiSampleBuffer(const GLenum format, const GLsizei width,
		const GLsizei height, const ML_INT samples)
	{
		if (m_bufferID == 0)
			glGenRenderbuffers(1, &m_bufferID);

		m_format = format;
		Bind();            
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, m_format,  width, height);
		Unbind();
	}

	MLCoreDLL ML_BOOL MLRenderBuffer::Bind()
	{
		if (!m_bound && m_bufferID)
		{
			glBindRenderbuffer(GL_RENDERBUFFER, m_bufferID);
			m_bound = ML_TRUE;
			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLRenderBuffer::Unbind()
	{
		if (m_bound && m_bufferID)
		{
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
			m_bound = ML_FALSE;
			return ML_TRUE;
		}

		return ML_FALSE;
	}

}
