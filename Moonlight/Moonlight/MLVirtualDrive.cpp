#include "MLVirtualDrive.h"

namespace MLKernel
{
	MLVirtualDrive::MLVirtualDrive()
	{
		m_drive.clear();
		DefaultRoot();
	}

	MLVirtualDrive::~MLVirtualDrive()
	{
		Release();
	}

	void MLVirtualDrive::Release()
	{
		m_drive.clear();
	}

	void MLVirtualDrive::DefaultRoot()
	{
		m_drive = ML_STR("\\");
	}

	ML_BOOL MLVirtualDrive::SetDrive(const ML_STRING& drive)
	{
		if(GetDriveType(drive.c_str()) != DRIVE_UNKNOWN)
		{
			m_drive = drive;
			readDriveSpace();
			return ML_TRUE;
		}
		else
		{
			ML_TRACE( 	ML_STR("Virtual Drive"), MLConsoleStatus::Warning, 
							ML_STR("Drive %s is an invalid drive. Set back to default drive\n"), drive.c_str());
			m_drive.clear();
			DefaultRoot();
			return ML_FALSE;
		}
	}

	//http://msdn.microsoft.com/en-us/library/cc542456%28v=vs.85%29.aspx
	std::vector<ML_STRING> MLVirtualDrive::EnumerateDrives() const
	{
		std::vector<ML_STRING> Drives;
		ML_STRING drivePath;

		WCHAR  VolumeName[MAX_PATH] = ML_STR("");
		WCHAR  DeviceName[MAX_PATH] = ML_STR("");
		DWORD  CharCount            = 0;
		HANDLE FindHandle           = INVALID_HANDLE_VALUE;
		DWORD  Error                = ERROR_SUCCESS;
		ML_BOOL Finished			= ML_FALSE;

		FindHandle = FindFirstVolume(VolumeName, ARRAYSIZE(VolumeName));

		if(FindHandle != INVALID_HANDLE_VALUE)
		{
			while(!Finished)
			{
				//  Skip the \\?\ prefix and remove the trailing backslash.
				size_t Index = wcslen(VolumeName) - 1;

				if (VolumeName[0]     != ML_STR('\\') ||
					VolumeName[1]     != ML_STR('\\') ||
					VolumeName[2]     != ML_STR('?')  ||
					VolumeName[3]     != ML_STR('\\') ||
					VolumeName[Index] != ML_STR('\\')) 
				{
					return Drives;
				}

				//  QueryDosDeviceW does not allow a trailing backslash,
				//  so temporarily remove it.
				VolumeName[Index] = L'\0';
				CharCount = QueryDosDeviceW(&VolumeName[4], DeviceName, ARRAYSIZE(DeviceName)); 
				VolumeName[Index] = L'\\';

				//  Allocate a buffer to hold the paths.
				PWCHAR Names = (PWCHAR) new BYTE [CharCount * sizeof(WCHAR)];
				if ( !Names ) 
				{
					//  If memory can't be allocated, return.
					return Drives;
				}

				//  Obtain all of the paths
				//  for this volume.
				ML_INT Success = GetVolumePathNamesForVolumeNameW(VolumeName, Names, CharCount, &CharCount);

				if(Success && CharCount != 1/*GetLastError() != ERROR_MORE_DATA*/)
				{
					drivePath = Names; 
					Drives.push_back(drivePath);	
				}	

				delete[] Names;
				Names = NULL;

				Success = FindNextVolumeW(FindHandle, VolumeName, ARRAYSIZE(VolumeName));

				if (!Success) 
				{
					Error = GetLastError();

					if (Error != ERROR_NO_MORE_FILES) 
					{
						ML_TRACE( 	ML_STR("Virtual Drive"), MLConsoleStatus::Error, 
										ML_STR("Error occurred obtaining drives Err Code: %i\n"), Error);
						break;
					}
					//  Finished iterating
					//  through all the volumes.
					Error = ERROR_SUCCESS;
					break;
				}
			}
		}
		else
		{
			return Drives;
		}

		return Drives;
	}

	ML_BOOL MLVirtualDrive::readDriveSpace()
	{
		ML_QWORD FreeBytes		= 0;
		ML_QWORD TotalBytes		= 0;
		ML_QWORD TotalFreeBytes = 0;

		ML_BOOL result = ML_FALSE;
		if(GetDiskFreeSpaceEx(m_drive.c_str(), (PULARGE_INTEGER)&m_freeSpace, 
			(PULARGE_INTEGER)&m_space, (PULARGE_INTEGER)&TotalFreeBytes) > FALSE)
			result = ML_TRUE;
	
		if (!result)
		{
			ML_TRACE( 	ML_STR("Virtual Drive"), MLConsoleStatus::Error, 
				ML_STR("Could not read drive space\n"), Error);
			m_freeSpace			= 0;
			m_space				= 0;
		}

		return result;
	}

	ML_BOOL MLVirtualDrive::IsValid() const
	{
		if(GetDriveType(m_drive.c_str()) != DRIVE_UNKNOWN || m_drive == ML_STR("\\"))
		{
			return ML_TRUE;
		}
		else
		{
			return ML_FALSE;
		}
	}
}