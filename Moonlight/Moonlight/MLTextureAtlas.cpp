#include "MLTextureAtlas.h"

namespace MLRender
{

	MLTextureAtlas::MLTextureAtlas(ML_STRING atlasName)
	{
		m_atlasTextureName = atlasName;
	}


	MLTextureAtlas::~MLTextureAtlas(void)
	{
	}

	MLCoreDLL ML_BOOL MLTextureAtlas::Release()
	{
		m_usedRectangles.clear();
		m_freeRectangles.clear();

		return ML_TRUE;
	}

	MLCoreDLL void MLTextureAtlas::CreateAtlas(const ML_UINT Width, const ML_UINT Height, 
		const MLPixelFormat& format, const ML_BOOL enableRotate /*= ML_TRUE*/)
	{
		m_textureFormat = format;
		m_enableRotate = enableRotate;
		m_width = Width;
		m_height = Height;

		AtlasTexture free;
		free.rect = MLVector4f(0, 0, (ML_FLOAT)Width, (ML_FLOAT)Height);

		m_usedRectangles.clear();
		m_freeRectangles.clear();

		m_freeRectangles.push_back(free);
	}

	MLCoreDLL ML_BOOL MLTextureAtlas::AddTexture(const MLKernel::MLVirtualFile* Path, AtlasTexture* atlasInfo)
	{
		if(Path->Exists())
			return AddTexture(MLtoAscii(Path->FullFilePath()).c_str(), atlasInfo);
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_ATLAS, MLKernel::MLConsoleStatus::Error, ML_STR("File path %s does not exist"));
			return ML_FALSE;
		}
	}

	MLCoreDLL ML_BOOL MLTextureAtlas::AddTexture(const ML_U8STRING& Path, AtlasTexture* atlasInfo)
	{
		//The image format variable for free image
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
		//The bitmap of the image	
		FIBITMAP* fib(0);
		//the raw bytes of the image
		BYTE* bits(0);

		//the width and height of the image
		ML_UINT width(0), height(0), bpp(0);

		//get the filetype from the path
		fif = FreeImage_GetFileType(Path.c_str());

		//if the file type is unknown for some reason see if it can
		//be opatined from the file path itself
		if (fif == FIF_UNKNOWN)
			fif = FreeImage_GetFIFFromFilename(Path.c_str());
		//if not then the file must be unsupported
		if (fif == FIF_UNKNOWN)
			return 0;

		//if the file is readable by free image the load it
		if (FreeImage_FIFSupportsReading(fif))
			fib = FreeImage_Load(fif, Path.c_str());
		if (!fib)
			return 0;
		//flip the image as bitmaps are upsidedown
		FreeImage_FlipVertical(fib);

		switch(m_textureFormat)
		{
		case MLPixelFormat::RGBA32: fib = FreeImage_ConvertTo32Bits(fib); break;
		case MLPixelFormat::BGRA32: fib = FreeImage_ConvertTo32Bits(fib); break;
		case MLPixelFormat::RGB24:  fib = FreeImage_ConvertTo24Bits(fib); break;
		case MLPixelFormat::BGR24:  fib = FreeImage_ConvertTo24Bits(fib); break;
		case MLPixelFormat::LuminanceAlpha16:	FreeImage_Unload(fib); return ML_FALSE; break;
		case MLPixelFormat::SingleChannel8:		FreeImage_Unload(fib); return ML_FALSE; break;
		case MLPixelFormat::RedChannel8:		FreeImage_Unload(fib); return ML_FALSE; break;
		case MLPixelFormat::GreenChannel8:		FreeImage_Unload(fib); return ML_FALSE; break;
		case MLPixelFormat::BlueChannel8:		FreeImage_Unload(fib); return ML_FALSE; break;
		case MLPixelFormat::AlphaChannel8:		FreeImage_Unload(fib); return ML_FALSE; break;
		}
	
		//get the bits
		bits = FreeImage_GetBits(fib);
		//get the width
		width = FreeImage_GetWidth(fib);
		//get the height
		height = FreeImage_GetHeight(fib);
		//get the bit per pixel
		bpp = FreeImage_GetBPP(fib);

		switch(m_textureFormat)
		{
		case MLPixelFormat::RGBA32: swapRB32(bits, width, height); break;
		case MLPixelFormat::RGB24:  swapRB24(bits, width, height); break;
		}

		if(bpp < 24 && bpp > 32)
		{
			FreeImage_Unload(fib);
			return ML_FALSE;
		}

		//if there are any errors with loading the file then the data is not loaded
		if((bits == 0) || (width == 0) || (height == 0))
			return 0;

		BYTE* data = (BYTE*)malloc( width * height * bpp/8);
		memset(data, 0, width * height * bpp/8);
		memcpy(data, bits, width * height * bpp/8);

		ML_BOOL result = AddTexture(data, width, height, atlasInfo);

		if (result == ML_FALSE)
			delete[] data;

		FreeImage_Unload(fib);

		return result;
	}

	MLCoreDLL ML_BOOL MLTextureAtlas::AddTexture(void* Data, const ML_INT Width, const ML_INT Height, AtlasTexture* atlasInfo)
	{
		AtlasTexture newNode;
	
		newNode = FindNewPositionNode(MLVector2f((ML_FLOAT)Width, (ML_FLOAT)Height));
	
		if(newNode.rect.Height == 0)
			return ML_FALSE;

		if(newNode.rect.Height == Width && m_enableRotate)
		{
			newNode.pixelData = rotateTexture(Data, Width, Height, m_textureFormat);
			newNode.Rotated = ML_TRUE;
		}
		else
		{
			newNode.pixelData = Data;
			newNode.Rotated = ML_FALSE;
		}

		ML_INT freeRectCount = m_freeRectangles.size();
		for(ML_INT i = 0; i < freeRectCount; i++)
		{
			if(SplitFreeNode(m_freeRectangles[i], newNode))
			{
				m_freeRectangles.erase(m_freeRectangles.begin() + i);
				i--;
				freeRectCount--;
			}
		}

		CleanFreeRects();

		m_usedRectangles.push_back(newNode);

		*atlasInfo = newNode;

		return ML_TRUE;
	}

	MLCoreDLL void MLTextureAtlas::swapRB24(ML_BYTE* data, const ML_INT width, const ML_INT height)
	{
		for(ML_INT y=0; y < height; y++) 
		{
			for(ML_INT x=0; x < width; x++)
			{
				ML_INT colour1 = data[((3 * y * width) + x * 3)]; 
				data[((3 * y * width) + x * 3)] = data[((3 * y * width) + x * 3) + 2]; 
				data[((3 * y * width) + x * 3) + 2] = colour1;
			}
		}	
	}

	MLCoreDLL void MLTextureAtlas::swapRB32(ML_BYTE* data, const ML_INT width, const ML_INT height)
	{
		for(ML_INT y=0; y < height; y++) 
		{
			for(ML_INT x=0; x < width; x++)
			{
				ML_INT colour1 = data[((4 * y * width) + x * 4)]; 
				data[((4 * y * width) + x * 4)] = data[((4 * y * width) + x * 4) + 2]; 
				data[((4 * y * width) + x * 4) + 2] = colour1;
			}
		}	
	}

	MLCoreDLL void MLTextureAtlas::BuildTexture()
	{
		m_textureAtlas = MLTextureManager::CreateTexture();

		switch(m_textureFormat)
		{
		case MLPixelFormat::RGBA32: BuildRGBA32Texture(); break;
		case MLPixelFormat::BGRA32: BuildBGRA32Texture(); break;
		case MLPixelFormat::RGB24:  BuildRGB24Texture();  break;
		case MLPixelFormat::BGR24:  BuildBGR24Texture();  break;
		case MLPixelFormat::LuminanceAlpha16:	BuildLumiAlpha16Texture();	break;
		case MLPixelFormat::SingleChannel8:		BuildChannel8Texture();		break;
		case MLPixelFormat::RedChannel8:		BuildChannel8Texture();		break;
		case MLPixelFormat::GreenChannel8:		BuildChannel8Texture();		break;
		case MLPixelFormat::BlueChannel8:		BuildChannel8Texture();		break;
		case MLPixelFormat::AlphaChannel8:		BuildChannel8Texture();		break;
		}

	}

	MLCoreDLL void MLTextureAtlas::BuildRGBA32Texture()
	{
		MLPixelRGBA32* pixelData = new MLPixelRGBA32[m_width * m_height];

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelRGBA32* texturePixelData = (MLPixelRGBA32*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * (ML_INT)currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}

		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_RGBA);
		delete[] pixelData;
	}

	MLCoreDLL void MLTextureAtlas::BuildBGRA32Texture()
	{
		MLPixelBGRA32* pixelData = new MLPixelBGRA32[m_width * m_height];

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelBGRA32* texturePixelData = (MLPixelBGRA32*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}

		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_BGRA);
		delete[] pixelData;
	}

	MLCoreDLL void MLTextureAtlas::BuildRGB24Texture()
	{
		MLPixelRGB24* pixelData = new MLPixelRGB24[m_width * m_height];

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelRGB24* texturePixelData = (MLPixelRGB24*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}

		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_RGB);
		delete[] pixelData;
	}

	MLCoreDLL void MLTextureAtlas::BuildBGR24Texture()
	{
		MLPixelBGR24* pixelData = new MLPixelBGR24[m_width * m_height];

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelBGR24* texturePixelData = (MLPixelBGR24*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}

		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_BGR);
		delete[] pixelData;
	}

	MLCoreDLL void MLTextureAtlas::BuildLumiAlpha16Texture()
	{
		MLPixelLumiAlpha16* pixelData = new MLPixelLumiAlpha16[m_width * m_height];

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelLumiAlpha16* texturePixelData = (MLPixelLumiAlpha16*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}

		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_LUMINANCE_ALPHA);
		delete[] pixelData;
	}

	MLCoreDLL void MLTextureAtlas::BuildChannel8Texture()
	{
		MLPixelChannel8* pixelData = new MLPixelChannel8[m_width * m_height];
		memset(pixelData, 255, m_width * m_height * sizeof(MLPixelChannel8));

		for (ML_UINT i = 0; i < m_usedRectangles.size(); i++)
		{
			AtlasTexture* currentTexture = &m_usedRectangles[i];
			MLPixelChannel8* texturePixelData = (MLPixelChannel8*)currentTexture->pixelData;
			for (ML_UINT y = (ML_UINT)currentTexture->rect.y; y < (ML_UINT)currentTexture->rect.Height + (ML_UINT)currentTexture->rect.y; y++)
			{
				for (ML_UINT x = (ML_UINT)currentTexture->rect.x; x < (ML_UINT)currentTexture->rect.Width + (ML_UINT)currentTexture->rect.x; x++)
				{
					ML_INT index = (ML_INT)(((y - currentTexture->rect.y) * currentTexture->rect.Width) + (x - currentTexture->rect.x));
					pixelData[((y * m_width) + x)] = texturePixelData[index];
				}
			}

			delete[] currentTexture->pixelData;
		}



		switch(m_textureFormat)
		{
		case MLPixelFormat::SingleChannel8:		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_RED);	break;
		case MLPixelFormat::RedChannel8:		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_RED);	break;
		case MLPixelFormat::GreenChannel8:		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_GREEN); break;
		case MLPixelFormat::BlueChannel8:		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_BLUE);	break;
		case MLPixelFormat::AlphaChannel8:		m_textureAtlas->StoreTexture((ML_BYTE*)pixelData, m_width, m_height, GL_ALPHA); break;
		}

		delete[] pixelData;
	}

	MLCoreDLL AtlasTexture MLTextureAtlas::FindNewPositionNode(const MLVector2f& size)
	{
		AtlasTexture bestNode;

		for (ML_UINT i = 0; i < m_freeRectangles.size(); i++)
		{
			if (m_freeRectangles[i].rect.Width >= size.Width && m_freeRectangles[i].rect.Height >= size.Height)
			{
				ML_INT leftoverHoriz	= (ML_INT)abs(m_freeRectangles[i].rect.Width - size.Width);
				ML_INT leftoverVert		= (ML_INT)abs(m_freeRectangles[i].rect.Height - size.Height);

				bestNode.rect.x			= m_freeRectangles[i].rect.x;
				bestNode.rect.y			= m_freeRectangles[i].rect.y;
				bestNode.rect.Width		= size.Width;
				bestNode.rect.Height	= size.Height;
			}
			else if(m_freeRectangles[i].rect.Width >= size.Height && m_freeRectangles[i].rect.Height >= size.Width && m_enableRotate)
			{
				ML_INT flippedHoriz = (ML_INT)abs(m_freeRectangles[i].rect.Width - size.Height);
				ML_INT leftoverVert = (ML_INT)abs(m_freeRectangles[i].rect.Height - size.Width);

				bestNode.rect.x			= m_freeRectangles[i].rect.x;
				bestNode.rect.y			= m_freeRectangles[i].rect.y;
				bestNode.rect.Width		= size.Height;
				bestNode.rect.Height	= size.Width;
			}
		}

		return bestNode;
	}

	MLCoreDLL ML_BOOL MLTextureAtlas::SplitFreeNode(AtlasTexture freeNode, const AtlasTexture& usedNode)
	{
		// Test with SAT if the rectangles even intersect.
		if (usedNode.rect.x >= freeNode.rect.x + freeNode.rect.Width || usedNode.rect.x + usedNode.rect.Width <= freeNode.rect.x ||
			usedNode.rect.y >= freeNode.rect.y + freeNode.rect.Height || usedNode.rect.y + usedNode.rect.Height <= freeNode.rect.y)
			return ML_FALSE;

		if (usedNode.rect.x < freeNode.rect.x + freeNode.rect.Width && usedNode.rect.x + usedNode.rect.Width > freeNode.rect.x)
		{
			// New node at the top side of the used node.
			if (usedNode.rect.y > freeNode.rect.y && usedNode.rect.y < freeNode.rect.y + freeNode.rect.Height)
			{
				AtlasTexture newNode = freeNode;
				newNode.rect.Height = usedNode.rect.y - newNode.rect.y;
				m_freeRectangles.push_back(newNode);
			}

			// New node at the bottom side of the used node.
			if (usedNode.rect.y + usedNode.rect.Height < freeNode.rect.y + freeNode.rect.Height)
			{
				AtlasTexture newNode = freeNode;
				newNode.rect.y = usedNode.rect.y + usedNode.rect.Height;
				newNode.rect.Height = freeNode.rect.y + freeNode.rect.Height - (usedNode.rect.y + usedNode.rect.Height);
				m_freeRectangles.push_back(newNode);
			}
		}

		if (usedNode.rect.y < freeNode.rect.y + freeNode.rect.Height && usedNode.rect.y + usedNode.rect.Height > freeNode.rect.y)
		{
			// New node at the left side of the used node.
			if (usedNode.rect.x > freeNode.rect.x && usedNode.rect.x < freeNode.rect.x + freeNode.rect.Width)
			{
				AtlasTexture newNode = freeNode;
				newNode.rect.Width = usedNode.rect.x - newNode.rect.x;
				m_freeRectangles.push_back(newNode);
			}

			// New node at the right side of the used node.
			if (usedNode.rect.x + usedNode.rect.Width < freeNode.rect.x + freeNode.rect.Width)
			{
				AtlasTexture newNode = freeNode;
				newNode.rect.x = usedNode.rect.x + usedNode.rect.Width;
				newNode.rect.Width = freeNode.rect.x + freeNode.rect.Width - (usedNode.rect.x + usedNode.rect.Width);
				m_freeRectangles.push_back(newNode);
			}
		}

		return ML_TRUE;
	}

	MLCoreDLL void MLTextureAtlas::CleanFreeRects()
	{
		for(ML_UINT i = 0; i < m_freeRectangles.size(); i++)
		{
			for(ML_UINT j = i+1; j < m_freeRectangles.size(); j++)
			{
				if (AtlasTexture::IsContainedIn(m_freeRectangles[i], m_freeRectangles[j]))
				{
					m_freeRectangles.erase(m_freeRectangles.begin()+i);
					i--;
					break;
				}
				if (AtlasTexture::IsContainedIn(m_freeRectangles[j], m_freeRectangles[i]))
				{
					m_freeRectangles.erase(m_freeRectangles.begin()+j);
					j--;
				}
			}
		}
	}

}