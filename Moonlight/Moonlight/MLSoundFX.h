#ifndef MLSOUNDFX_H
#define MLSOUNDFX_H

#include "MLIncludes.h"
#include "MLAudioHelper.h"
#include "MLAudioManager.h"



namespace MLAudio
{
/// <summary>	Sound Effect Log Name. </summary>
#define ML_LOG_SFX ML_STR("SFX")

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Stores information about a sound effect file
	///	this stores information both for ffmpeg and openAL. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	struct MLSFX
	{
		/// <summary>	File name. </summary>
		ML_STRING   FileName;

		/// <summary>	The Decoded PCM 16 data from the sound effect. </summary>
		ML_SHORT*	PCMData;
		/// <summary>	This size is the array count not the byte size. </summary>
		ML_UINT		PCMDataSize;
		/// <summary>	Byte size for the PCMData. </summary>
		ML_UINT		PCMDataBytes;

		/// <summary>	The frequency of the file. </summary>
		ML_UINT		Frequency;
		/// <summary>	The amount of channels in the file. </summary>
		ML_UINT		Channels;
		/// <summary>	The bit rate of the file (though I think it may actually be byte rate). </summary>
		ML_UINT		BitRate;
		/// <summary>	This is the Channel format. </summary>
		ML_UINT		ChannelFormat;
		/// <summary>	This is the format of the output from the decoding process. </summary>
		ML_UINT		DecodeFormat;

		/// <summary>	Duration of the audio track in seconds. </summary>
		ML_DOUBLE	Duration;
		/// <summary>	If the sound effect in use currently. </summary>
		ML_BOOL InUse;

		/// <summary>	The openAL sources playing the audio. </summary>
		std::vector<ALuint> ALSources;
		/// <summary>	The OpenAL Buffer the sound effect data is stored in. </summary>
		ALuint				ALBuffer;
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	The sound effect package is designed to
	///	store pre loaded audio sound effects into a single package.
	///	Packages are added by a unquie ID which is also used to
	///	access them.
	///	E.g soundPackage->LoadFile("explosion", "file.ogg")
	///	soundPackage->Play("explosion")
	///	Because the audio is pre loaded it can be played multiple
	///	times at once as long as their are enough audio sources available.. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLSoundFX
	{
	public:
		MLCoreDLL MLSoundFX(const ML_STRING& groupName);
		MLCoreDLL virtual ~MLSoundFX(void);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Release all the sound effect objects. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Release();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Load a sound effect file and uses a UUID for
		///	the key which is passed back from the function (string path). 
		///	returns a empty string if file failed to load </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="file"> The sound effect file to load. </param>
		///
		/// <returns>	The UUID name for the sound effect file to be used to reference it.
		/// 			Returns an empty string if loading failed. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_STRING LoadFile(const ML_STRING& file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Load a sound effect file and uses a UUID for
		///	the key which will be passed back from the function (virtual file path).
		///	returns a empty string if file failed to load </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="file">	The sound effect file to load. </param>
		///
		/// <returns>	The UUID name for the sound effect file to be used to reference it. 
		/// 			Returns an empty string if loading failed. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_STRING LoadFile(const MLKernel::MLVirtualFile* file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Loads a sound effect file but uses a custom
		///	ID (string path). returns a empty string if file failed to load. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">  	The ID to use for the sound effect file. </param>
		/// <param name="file">	The sound effect file to load. </param>
		///
		/// <returns>	Returns the ID if no other sfx is using it otherwise it creates a UUID for
		/// 			the file to use so its still loaded. An empty string is returned if the 
		/// 			loading of the file failed. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_STRING LoadFile(const ML_STRING& ID, const ML_STRING& file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Loads a sound effect file but uses a custom
		///	ID (Virtual file path). returns a empty string if file failed to load. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">  	The ID to use for the sound effect file. </param>
		/// <param name="file">	The sound effect file to load. </param>
		///
		/// <returns>	Returns the ID if no other sfx is using it otherwise it creates a UUID for
		/// 			the file to use so its still loaded. An empty string is returned if the
		/// 			loading of the file failed. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_STRING LoadFile(const ML_STRING& ID, const MLKernel::MLVirtualFile* file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Check to see if the ID already exists or not. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID to check exists or not. </param>
		///
		/// <returns>	Returns true if the SFX ID already exists. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SFXExists(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will Unload the sound effect from openAL
		///	But will not remove the data from memory. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect to unload. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL UnloadSFX(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will reload a SFX file that has been unloaded
		///	from openAL as long as the PCM data is valid. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect to reload. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ReloadSFX(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will remove a specific sound effect file
		/// This will forces unload the sound effect file from OpenAL. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect to remove from the sound effect group. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL RemoveSFX(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will remove all the sound effect files
		///	and unload from openAL as well. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL RemoveAll();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the volume for a single effect
		///	this will set the max gain to the gain value </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">  	The ID of the sound effect file. </param>
		/// <param name="gain">	The volume to set the effect to. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetEffectVolume(const ML_STRING& ID, const  ML_FLOAT gain);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the volume for a all stored effect
		///	this will set the max gain to the gain value. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="gain">	The Volume to set all effects to. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetAllEffetVolume(const ML_FLOAT gain);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Check to see if the sound effect is available
		///	for playback. This is different to Exists as this also
		///	checks for a OpenAL buffer in the sound effects object. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect file. </param>
		///
		/// <returns>	Returns true if the sound effect has been loaded into this group
		/// 			if is ready for playback. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SFXAvaliable(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will check to see if the file exists
		///	based on the file path and if the PCM and ALBuffer is
		///	loaded. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="path">	Full path address of the sound effect file. </param>
		///
		/// <returns>	Returns true if the PCM Data is loaded and a AL buffer is loaded with the data. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SFXLoaded(const ML_STRING& path);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will check to see if the file exists
		///	based on the file path and if the PCM and ALBuffer is
		///	loaded. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="file">	The virtual file of the sound effect file. </param>
		///
		/// <returns>	Returns true if the PCM Data is loaded and a AL buffer is loaded with the data. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SFXLoaded(const MLKernel::MLVirtualFile* file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns a sound effect object if it exists
		///	otherwise null is returned. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect file. </param>
		///
		/// <returns>	null if it fails, else the sound effect structure object. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL MLSFX* GetSFX(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	 Will set the source settings for a specific
		///	audio source. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	   	The ID of the sound effect file. </param>
		/// <param name="sourceID">	The openAL source ID of the playing effect. </param>
		/// <param name="source">  	The source information structure. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetSource(const ML_STRING& ID, const ML_INT& sourceID, const MLAudioSource& source);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will play the sound effects ID if the
		///	file is valid. The return value if the source index in
		///	the sound effect source array since a single source
		///	can be played multiple times it also needs multiple
		///	audio sources. Return -1 if unable to play. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect file. </param>
		///
		/// <returns>	The audio source ID of the playing sound effect. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_INT Play(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Plays a sound effect from a sfx file. This
		/// is capable of playing any sfx file even if it isn't in
		///	the sound effect pack. it will only play if valid
		///	and will return the index of the new audio source.
		///	Return -1 if unable to play. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">	Sound effect object. </param>
		///
		/// <returns>	The audio source ID index of the playing sound effect. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_INT Play(MLSFX* sfx);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Plays the audio file the same as without
		///	the source but provides setting for how the audio is
		///	played back. Return -1 if unable to play. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	 	The ID of the sound effect file. </param>
		/// <param name="source">	The source information structure to play with. </param>
		///
		/// <returns>	The audio source ID index of the playing sound effect. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_INT Play(const ML_STRING& ID, const MLAudioSource& source);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Play the audio file the same as without the
		///	source but provides settings for how the audio is
		///	played back. Return -1 if unable to play. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">   	Sound effect object. </param>
		/// <param name="source">	The source information structure to play with. </param>
		///
		/// <returns>	The audio source ID index of the playing sound effect. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_INT Play(MLSFX* sfx, const MLAudioSource& source);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Pauses the audio source. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">   	The ID of the sound effect file. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Pause(const ML_STRING& ID, const ML_INT index);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Pauses the audio sound effect source. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">  	The sfx object. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Pause(const MLSFX* sfx, const ML_INT index);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Pauses all the audio for a single sound effect type. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect file. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL PauseAll(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Pauses all the audio for a single sound effect file </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">	The sfx object. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL PauseAll(const MLSFX* sfx);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will Pause all the audio  for all the sound
		///	effects in the package. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL PauseAll();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Resumes a single source from a sound effect
		///	false is return if the id is invalid. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">   	The ID of the sound effect file. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Resume(const ML_STRING& ID, const ML_INT index);
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Resumes a single source from a sound effect
		///	false is return if the id is invalid.  </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">  	The sfx object. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Resume(const MLSFX* sfx, const ML_INT index);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will resume all paused items for a
		///	sound effect. Returns false if fails </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	he ID of the sound effect file. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ResumeAll(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will resume all paused items for a
		///	sound effect. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">	The sfx object. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ResumeAll(const MLSFX* sfx);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will resume all paused sources on all
		///	sound effects. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ResumeAll();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop a single audio source for a
		///	sound effect. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">   	The ID of the sound effect file. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Stop(const ML_STRING& ID, const ML_INT index);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop a single audio source for a
		///	sound effect. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">  	The sfx object. </param>
		/// <param name="index">	The audio source array index. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Stop(MLSFX* sfx, const ML_INT index);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop all audio sources for a single
		/// audio sound effect. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="ID">	The ID of the sound effect file. </param>
		///
		/// <returns>	Returns true if succesful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL StopAll(const ML_STRING& ID);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop all audio sources for a single
		///	audio sound effect. Returns false if fails. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfx">	The sfx object. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL StopAll(MLSFX* sfx);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop all the audio sources for all
		///	the sound effect files. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL StopAll();

	private:

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will remove all the audio sources
		///	that have finished playing. This will happen every time
		///	a new file is to be played. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfxFile">	The sfx object to remove unused source from. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void RemoveUnusedSources(MLSFX* sfxFile);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will perform the pre load action for an audio
		///	file using FFMPEG. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="key">	   	The ID key of the audio file to add. </param>
		/// <param name="sfxCodec">	The loaded sound effect codec for the file. </param>
		/// <param name="sfxFile"> 	The sound effect object to load data into. </param>
		///
		/// <returns>	Return true if succesful </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL PreLoad(const ML_STRING& key, const MLCodec* sfxCodec, MLSFX* sfxFile);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will decode an entire stream into a list
		///	of Audio Frames storing the data for the audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="sfxFile"> 	The sfx file to decode data onto. </param>
		/// <param name="sfxCodec">	The sfx codec to use for decoding . </param>
		/// <param name="dataSize">	[out] Size of the data that was doceoded. </param>
		///
		/// <returns>	Returns a vector of Decoded audio frames which will require piecing together
		/// 			have the fully decoded audio data. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL std::vector<MLAudioFrame*> DecodeStream(const MLSFX* sfxFile, const MLCodec* sfxCodec, ML_UINT& dataSize);

	private:

		/// <summary>	Name of the sound effect group. </summary>
		ML_STRING m_groupName;

		/// <summary>	The sound effect pool. </summary>
		std::map<ML_STRING, MLSFX*> m_SFXPool;
	};

}

#endif //MLSOUNDFX_H