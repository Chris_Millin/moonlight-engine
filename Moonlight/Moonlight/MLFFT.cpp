//#include "MLFFT.h"
//
//using MLAudio::MLFFT;
//
//MLCoreDLL MLFFT::MLFFT(void)
//{
//}
//
//
//MLCoreDLL MLFFT::~MLFFT(void)
//{
//	release();
//}
//
////=========================================================
//// Name: Release
//// Description: Releases all the pointer objects
//// Creator: Chris Millin
////=========================================================
//MLCoreDLL bool MLFFT::release(void)
//{
//	if (m_fftResult)
//	{
//		delete m_fftResult;
//		m_fftResult = NULL;
//	}
//	if(m_wavData)
//	{
//		delete m_wavData;
//		m_wavData = NULL;
//	}
//	if (m_inData)
//	{
//		delete m_inData;
//		m_inData = NULL;
//	}
//	if (m_resultData)
//	{
//		delete m_resultData;
//		m_resultData = NULL;
//	}
//
//	return true;
//}
//
////=========================================================
//// Name: Load Wave FFT Data
//// Description: This will load wave data into a format that
//// can be used by the FFT library.
//// Creator: Chris Millin
////=========================================================
//MLCoreDLL bool MLFFT::LoadWavFFTData(MLAudioFile* af)
//{
//	CWaves* wavLoad = MLAudioManager::GetWavLoader();
//
//	short* pSData;
//	ALint size;
//
//	wavLoad->GetWaveData(id, (void**)&pSData);
//	wavLoad->GetWaveSize(id, (unsigned long*)&size);
//
//	m_wavData = new float[size/2];
//
//	for (int i = 0; i < size/2; i++)
//		m_wavData[i] = pSData[i];
//
//	m_currentWavID = id;
//
//	return true;
//}
//
////=========================================================
//// Name: Create FFT
//// Description: This will set up the FFT with a defined plan
//// Creator: Chris Millin
////=========================================================
//MLCoreDLL bool MLFFT::CreateFFT(int nSamples, double sampleDelay)
//{
//	m_sampleCount = nSamples;
//	m_sampleDelay = sampleDelay;
//
//	m_inData	= fftwf_alloc_real(nSamples);
//	m_fftResult = fftwf_alloc_complex(nSamples/2);
//
//	m_fftPlan = fftwf_plan_dft_r2c_1d(nSamples, m_inData, m_fftResult, FFTW_ESTIMATE);
//
//	m_resultCount = m_sampleCount/2;
//
//	m_resultData = new float[m_resultCount];
//
//	return true;
//}
//
////=========================================================
//// Name: Perform FFT
//// Description: Will perform the FFT on the audio file
//// the result will be stored in result data.
//// Creator: Chris Millin
////=========================================================
//MLCoreDLL bool MLFFT::PerformFFT(double deltaTime)
//{
//	//The current Audio Position
//	int position = MLAudioManager::GetAudioPosition(m_currentWavID);
//
//	for(int i = 0; i < m_sampleCount; i++)
//	{
//		m_inData[i] = (*((m_wavData + (position/2) + i))) /** (0.54 - 0.46 * cos((2*ML_PI*i)/(m_sampleCount - 1)))*/;
//	}
//
//	fftwf_execute(m_fftPlan);
//
//	for (int i = 0; i < m_resultCount; i++)
//	{
//		m_resultData[i] = 20 * log10f(sqrt(m_fftResult[i][0]*m_fftResult[i][0] + m_fftResult[i][1]*m_fftResult[i][1]));
//	}
//
//	return true;
//}
//
////=========================================================
//// Name: Set Result Pointer
//// Description: This can be used to set the array for where
//// the result is stored.
//// Creator: Chris Millin
////=========================================================
//MLCoreDLL bool MLFFT::SetResultPointer(float* result,int count)
//{
//	if (count < m_sampleCount/2)
//	{
//		if (m_custonResult == false)
//		{
//			m_resultData = result;
//			m_resultCount = count;
//			m_custonResult = true;
//		}
//		else if(m_custonResult == true)
//		{
//			m_resultData = result;
//			m_resultCount = count;
//		}
//	}
//
//	return true;
//}
//
