#include "MLFrameBuffer.h"

namespace MLRender
{
	MLFrameBuffer::MLFrameBuffer(void)
	{
		m_bufferID = 0;
		m_bound = ML_FALSE;
	}


	MLFrameBuffer::~MLFrameBuffer(void)
	{
		Release();
	}


	MLCoreDLL ML_BOOL MLFrameBuffer::Release(void)
	{
		if (m_bufferID != 0)
		{
			glDeleteFramebuffers(1, &m_bufferID);
			m_bufferID = 0;
		}
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLFrameBuffer::Bind(const GLenum type /* = -1 */)
	{
		if(!m_bound && m_bufferID) 
		{ 
			if(type == -1)
				glBindFramebuffer(m_type, m_bufferID);
			else
				glBindFramebuffer(type, m_bufferID);
			m_bound = ML_TRUE;
			return ML_TRUE;
		}
		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLFrameBuffer::UnBind(const GLenum type /* = -1 */)
	{
		if(m_bound && m_bufferID) 
		{ 
			if(type == -1)
				glBindFramebuffer(m_type, 0);
			else
				glBindFramebuffer(type, 0);
			m_bound = ML_FALSE;
			return ML_TRUE;
		}
		return ML_FALSE;
	}

	MLCoreDLL void MLFrameBuffer::CreateBuffer(const GLenum& type)
	{
		if (m_bufferID == 0)
			glGenFramebuffers(1, &m_bufferID);
	
		m_type = type;
	}

	MLCoreDLL void MLFrameBuffer::AttachTexture(const MLTexture* texture, const GLenum& attachment)
	{
		if (!m_bound)
		{
			Bind();
			glFramebufferTexture2D(m_type, attachment, GL_TEXTURE_2D, texture->GetTextureID(), 0);
			UnBind();
		}
		else
		{
			glFramebufferTexture2D(m_type, attachment, GL_TEXTURE_2D, texture->GetTextureID(), 0);
		}

		m_textureAttachment = attachment;
	}

	MLCoreDLL MLTexture* MLFrameBuffer::AttachTexture(const ML_UINT width, const ML_UINT height, const GLenum& attachment)
	{
		return NULL;
	}

	MLCoreDLL void MLFrameBuffer::AttachMSTexture(const MLTexture* texture, const GLenum& attachment)
	{
		if (!m_bound)
		{
			Bind();
			glFramebufferTexture(m_type, attachment, texture->GetTextureID(), 0);
			UnBind();
		}
		else
		{
			glFramebufferTexture(m_type, attachment, texture->GetTextureID(), 0);
		}

		m_textureAttachment = attachment;
	}

	MLCoreDLL void MLFrameBuffer::AttachRenderBuffer(const MLRenderBuffer* buffer, const GLenum& attachment)
	{
		if (!m_bound)
		{
			Bind();
			glFramebufferRenderbuffer(m_type, attachment, GL_RENDERBUFFER, buffer->GetBufferID());
			UnBind();
		}
		else
		{
			glFramebufferRenderbuffer(m_type, attachment, GL_RENDERBUFFER, buffer->GetBufferID());
		}

		m_renderAttachment = attachment;
	}

	MLCoreDLL void MLFrameBuffer::Blit(const ML_INT width, const ML_INT height)
	{
		//improve on this 
		glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}
}