#ifndef MLINPUTMANAGER_H
#define MLINPUTMANAGER_H

#include "MLIncludes.h"
#include "MLInput.h"
#include "MLController.h"
#include "MLFormats.h"

using namespace MLMath::MLVec2;

namespace MLInput
{

#define ML_LOG_INPUT ML_STR("Input")

	//=========================================================
	// Name: Mouse State
	// Description: Stores information about the mouse.
	// If the mouse is set as a raw mouse the axis will store
	// movement change rather than the position of the mouse in
	// the screen like the standard windows mouse does.
	// Creator: Chris Millin
	//=========================================================
	struct MLMouseState
	{
		MLMouseState() : PreviousAxisX(0), PreviousAxisY(0), CurrentAxisX(0), CurrentAxisY(0), WheelAxis(0)
		{
			CurrentButttons[0] = ML_FALSE;
			CurrentButttons[1] = ML_FALSE;
			CurrentButttons[2] = ML_FALSE;
			CurrentButttons[3] = ML_FALSE;
			CurrentButttons[4] = ML_FALSE;

			PreviousButttons[0] = ML_FALSE;
			PreviousButttons[1] = ML_FALSE;
			PreviousButttons[2] = ML_FALSE;
			PreviousButttons[3] = ML_FALSE;
			PreviousButttons[4] = ML_FALSE;
		}
		ML_INT PreviousAxisX; //last x movement (raw input only)
		ML_INT PreviousAxisY; //last y movement (raw input only)
		ML_INT CurrentAxisX;  //The position in the window in the x axis
		ML_INT CurrentAxisY;  //The position in the window in the y axis
		ML_SHORT WheelAxis; //The amount the mouse wheel has moved
		ML_BOOL CurrentButttons[ML_MOUSE_BUTTON_COUNT]; //The button states
		ML_BOOL PreviousButttons[ML_MOUSE_BUTTON_COUNT];
	};

	//=========================================================
	// Name: Key Event
	// Description: Stores information about the last key
	// pressed. This is used for creating recording strings.
	// When a recording string is active at the moment it will
	// consume the delete and backspace keys so if these keys
	// are required process them before the recording string
	// is updated.
	// Creator: Chris Millin
	//=========================================================
	struct MLKeyEvent 
	{
		ML_BOOL  keyDown;
		ML_BYTE  keyID;
		ML_BYTE  keyModifiyer;
		ML_SCHAR keyCharacter;
	};

	//=========================================================
	//Name: MLInputManager
	//Description: The core for Input Management and uses 2
	// different methods for input. The keyboard uses Raw input
	// while the mouse only uses the normal windows handler. This
	// is due to the mouse being too sensitive and off with raw input
	// Controllers are also supported with XInput.
	// The keyboard can also be recorded through last key presses
	// at the moment this is the same message that can be received
	// from the manager so dont attempt to modify it while recording.
	//Creator: Chris Millin
	//=========================================================
	class MLInputManager
	{
	public:

		//=========================================================
		// Name:  Windows Message Hook
		// Description: This function can be registered with the
		// windows call back with the windows manager. Each time
		// the call back for the window is made this function will
		// run to manager all the input messages set from windows
		// to the application.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void WindowsMessageHook(LPARAM lParamHandle, UINT message, WPARAM wParam);

		//=========================================================
		// Name: Get Controller
		// Description: returns a chosen controller from 1 - 4
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static MLInput::MLController* GetController(const ML_INT ID);

		//=========================================================
		// Name: Create Raw Keyboard
		// Description: This will create a raw keyboard input device
		// with windows. It also creates the default LUT for the
		// keyboard.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL CreateRawKeyboard(void);

		//=========================================================
		// Name:Release Raw Keyboard
		// Description: This will release the raw keyboard object
		// if it has been created through create raw keyboard
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL ReleaseRawKeyboard(void);

		//=========================================================
		// Name: Is Key Held
		// Description: Returns the state of the key this will be
		// true for the duration the key is held down.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsKeyHeld(const MLKeys Key)
		{
			return IsKeyHeld((ML_BYTE)Key);
		}

		//=========================================================
		// Name: Is Key Pressed
		// Description: Returns if the key has been pressed or not.
		// This will ignore the key being held and will allow for the
		// function to return true again if the key has been released
		// first.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsKeyPressed(const MLKeys Key)
		{
			return IsKeyPressed((ML_BYTE)Key);
		}

		//=========================================================
		// Name: Is Key Pressed 
		// Description: Returns the state of the key this will be
		// true for the duration the key is held down.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsKeyHeld(const ML_BYTE Key);

		//=========================================================
		// Name: Is Key Pressed 
		// Description: Returns if the key has been pressed or not.
		// This will ignore the key being held and will allow for the
		// function to return true again if the key has been released
		// first. 
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsKeyPressed(const ML_BYTE Key);

		//=========================================================
		// Name: Start Record Key String
		// Description: Will start recording the keyboard input
		// to generate a input string. A limit can be specified for
		// the string but the default is 1024 to prevent the 
		// string for getting overboard if end is not called.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL StartRecordKeyString(ML_UINT limit = 1024);

		//=========================================================
		// Name: Set Recording Index
		// Description: Set the index to where recording string will
		// start adding the next character input. This should be
		// lower than the current size of the recording string and
		// higher or equal to 0.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL SetRecordingIndex(const ML_UINT index);

		//=========================================================
		// Name: Get Index Position
		// Description: Get where the string is currently recording
		// from.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_UINT GetIndexPosition() { return m_recordingLowerString.size(); }

		//=========================================================
		// Name: Get Recording Key String
		// Description: Returns the current recording string.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_STRING GetRecordKeyString(void);

		//=========================================================
		// Name: Stop Recording Key String
		// Description: Will stop the recording update.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL StopRecordingKeyString();

		//=========================================================
		// Name: Get Key Event
		// Description: Returns the state of the last key has been
		// pressed.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static const MLKeyEvent& GetKeyEvent(void) { return m_lastKeyPressed; }

		//=========================================================
		// Name: Create Raw Mouse
		// Description: If a raw mouse input device is needed
		// instead of the call back mouse input then this can be
		// used.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL CreateRawMouse(void);

		//=========================================================
		// Name: Release Raw Mouse
		// Description: Release the raw mouse if it has been created
		// by create raw mouse.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL ReleaseRawMouse(void);

		//=========================================================
		// Name: Get Mouse Position
		// Description: This will return the X and Y of the mouse
		// with windows acceleration applied to it and will also
		// be the true position of the mouse on the screen.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static const MLVector2f GetMousePositon(void);
		
		//=========================================================
		// Name: Get Mouse Position
		// Description: This will return the X and Y of the mouse
		// with windows acceleration applied to it and will also
		// be the true position of the mouse on the screen.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static const MLVector2f GetMousePositon(const MLRender::MLScreenScaler& screen);

		//=========================================================
		// Name: Get Mouse Last Poisition
		// Description: This will return the X and Y of how much
		// the mouse has moved from its last position. This is only
		// active when the raw mouse is used as its the only way to
		// get movement from the mouse. There is no acceletation or
		// deadzone applied to this so it will be very very accurate.
		// but should really be avoided for 2D games really
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static const MLVector2f GetMouseLastPositon(void);

		//=========================================================
		// Name: Get Mouse Position
		// Description: This will return the X and Y of the mouse
		// with windows acceleration applied to it and will also
		// be the true position of the mouse on the screen.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static const MLVector2f GetMouseLastPositon(const MLRender::MLScreenScaler& screen);

		//=========================================================
		// Name: Is Mouse Button Held
		// Description: Returns the state of the mouse button this
		// will be true for the duration the key is held down.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsMouseButtonHeld(const MLMouseButtons& button)
		{
			return IsMouseButtonHeld((ML_BYTE)button);
		}

		//=========================================================
		// Name: Is Mouse Button Pressed
		// Description: Returns if the mouse button has been pressed
		// or not. This will ignore the key being held and will allow
		// for the function to return true again if the key has been 
		// released first. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsMouseButtonPressed(const MLMouseButtons& button)
		{
			return IsMouseButtonPressed((ML_BYTE)button);
		}

		//=========================================================
		// Name: Is Mouse Button Pressed
		// Description: Returns the state of the mouse button this
		// will be true for the duration the key is held down.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsMouseButtonHeld(const ML_BYTE button);

		//=========================================================
		// Name: Is Mouse Button Pressed
		// Description: Returns if the mouse button has been pressed
		// or not. This will ignore the key being held and will allow
		// for the function to return true again if the key has been 
		// released first. 
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL IsMouseButtonPressed(const ML_BYTE button);

		//=========================================================
		// Name: Get Scroll Wheel
		// Description: This will return a short with the max and min
		// of how much the scroll wheel has moved. so -32767 is down
		// and 32767 is up.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_INT GetScrollWheel(void);
	private:

		//=========================================================
		// Name: Update Recording Key String
		// Description: Updates the state of the recording string.
		// Creator: Chrism Millin
		//=========================================================
		MLCoreDLL static ML_BOOL UpdateRecordingKeyString();

	private:

		//Constructor
		MLCoreDLL MLInputManager() {}
		//Deconstuctor
		MLCoreDLL virtual ~MLInputManager(void) {}

		MLCoreDLL static RAWINPUTDEVICE m_rawDevices[2];

		//This is for the new string recording (lower will be the lower cursor position in
		//what ever is recording in and upper is anything past the cursor position)
		//This will allow for editing a input string at a particular point e.g
		// | = cursor, hello| lower = "hello" upper = "" hel|lo lower = "hel" upper = "lo"
		// |hello lower = "" upper = "hello"
		MLCoreDLL static ML_STRING m_recordingLowerString;
		MLCoreDLL static ML_STRING m_recordingUpperString;
		MLCoreDLL static ML_UINT m_recordingLimit;
		MLCoreDLL static ML_UINT m_recordingIndex;
		MLCoreDLL static ML_BOOL m_recording;

		MLCoreDLL static MLKeyEvent m_lastKeyPressed;

		//static keyboard state to store the keyboard input
		MLCoreDLL static ML_BOOL m_KeyCurrentState[ML_KEY_COUNT];
		MLCoreDLL static ML_BOOL m_KeyPreviousState[ML_KEY_COUNT];
		MLCoreDLL static MLMouseState m_MouseState;

		//The xbox controllers that can be plugged in (max of 4)
		MLCoreDLL static MLInput::MLController m_controllers[4];
		
		//a bool just to check if the controllers have been initalised or not before they are obtained
		MLCoreDLL static ML_BOOL m_controllersInitalised;
	};
}


#endif
