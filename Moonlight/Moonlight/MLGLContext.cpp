#include "MLGLContext.h"
#include "MLFormats.h"

ML_UINT MLScreenWidth	= ML_DEFAULT_SCREEN_WIDTH;
ML_UINT MLScreenHeight	= ML_DEFAULT_SCREEN_HEIGHT;
ML_INT MLScreenWidthOffset = ML_DEFAULT_SCREEN_OFFSET_WIDTH;
ML_INT MLScreenHeightOffset = ML_DEFAULT_SCREEN_OFFSET_HEIGHT;
ML_FLOAT MLScreenRatio	= ML_DEFAULT_RATIO;

namespace MLRender
{
	MLCoreDLL HDC				MLGLContext::m_hDC;			// Private GDI Device Context
	MLCoreDLL HGLRC				MLGLContext::m_hRCmain;		// Permanent Rendering Context
	MLCoreDLL HGLRC				MLGLContext::m_hRCthread;
	MLCoreDLL MLMatrix4f		MLGLContext::m_ortho;
	MLCoreDLL MLMatrix4f		MLGLContext::m_viewPort;
	MLCoreDLL ML_CHAR*			MLGLContext::m_GLSVersion;
	MLCoreDLL ML_FLOAT			MLGLContext::m_GLFVersion;
	MLCoreDLL MLScreenScaler	MLGLContext::m_globalScaler;

	MLCoreDLL MLGLContext::MLGLContext(void)
	{
		//set the windows pointer items to null
		m_hDC = NULL;
		m_hRCmain = NULL;
	}

	MLCoreDLL MLGLContext::~MLGLContext(void)
	{
		//release the devices if they have not already
		Release();
	}


	MLCoreDLL ML_BOOL MLGLContext::Release(void)
	{
		//release the objects
		if (m_hDC)
		{
			m_hDC = NULL;
		}
		if(m_hRCmain)
		{
			wglDeleteContext(m_hRCmain);
			m_hRCmain = NULL;
		}
		if(m_hRCthread)
		{
			wglDeleteContext(m_hRCthread);
			m_hRCmain = NULL;
		}
	
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLGLContext::Initalise(void)
	{
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);				
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_2D_MULTISAMPLE);
		glEnable(GL_DEPTH);
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLGLContext::Bind(HINSTANCE hInastance,HWND hWnd, GLsizei Width, GLsizei Height, ML_INT BitCount)
	{
		ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Information, 
			ML_STR("Binding OpenGL context to Window\n"));

		//create a id for the pixel format
		GLuint		PixelFormat;						//Store details for pixel format

		//create a pixel format definition
		static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
		{
			sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
			4,											// Version Number
			PFD_DRAW_TO_WINDOW |						// Format Must Support Window
			PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
			PFD_DOUBLEBUFFER | PFD_SWAP_COPY,							// Must Support Double Buffering
			PFD_TYPE_RGBA,								// Request An RGBA Format
			BitCount,									// Select Our Color Depth
			0, 0, 0, 0, 0, 0,							// Color Bits Ignored
			0,											// No Alpha Buffer
			0,											// Shift Bit Ignored
			0,											// No Accumulation Buffer
			0, 0, 0, 0,									// Accumulation Bits Ignored
			16,											// 16Bit Z-Buffer (Depth Buffer)  
			0,											// No Stencil Buffer
			0,											// No Auxiliary Buffer
			PFD_MAIN_PLANE,								// Main Drawing Layer
			0,											// Reserved
			0, 0, 0										// Layer Masks Ignored
		};

		//get the device context from the windows handler
		m_hDC = GetDC(hWnd);							//Get device context
		//check to see if it was obtained ok
		if (!m_hDC)
		{
			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Error, 
				ML_STR("Could not set device context\n"));
			MessageBox(hWnd, ML_STR("Could not set device context"), ML_STR("GL ERROR"), MB_ICONERROR);
			//MLErrorSet("Could not set the device context", "MLGLContext::Bind", "", MLE_CREATE);
			return ML_FALSE;
		}
	
		//Let windows choose pixel format
		PixelFormat=ChoosePixelFormat(m_hDC,&pfd);		

		//Set format chosen
		SetPixelFormat(m_hDC,PixelFormat,&pfd);			

		//Get rendering context
		m_hRCmain=wglCreateContext(m_hDC);						
		if (!m_hRCmain)
		{
			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Error, 
				ML_STR("Could not set main render context\n"));
			return ML_FALSE;
		}
		m_hRCthread=wglCreateContext(m_hDC);
		if (!m_hRCthread)
		{
			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Warning, 
				ML_STR("Could not set the second render context\n"));
		}

		BOOL error=wglShareLists(m_hRCmain, m_hRCthread);
		if(error == FALSE)
		{
			DWORD errorCode=GetLastError();
			LPVOID lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR) &lpMsgBuf, 0, NULL);

			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Warning, 
				ML_STR("Could not share render contexts Reason: %s\n"), lpMsgBuf);
			LocalFree(lpMsgBuf);
			//Destroy the GL context and just use 1 GL context
			wglDeleteContext(m_hRCthread);
		}
		//Activate rendering context
		wglMakeCurrent(m_hDC,m_hRCmain);							


		//initalises glew to enable the linkage to 4.0
		GLenum err = glewInit();
		const GLubyte* GLEWError;
		if( GLEW_OK != err )
		{
			MessageBox(hWnd, ML_STR("Could not Initalise GLEW"), ML_STR("GL ERROR"), MB_ICONERROR);
		
			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Error, 
				ML_STR("Could not Initalise GLEW\n"));

			GLEWError = glewGetErrorString(err);

			ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Error, 
				ML_STR("%s\n"), MLtoUnicode((ML_CHAR*)GLEWError).c_str());
		}

		const GLubyte *renderer		= glGetString( GL_RENDERER );
		const GLubyte *vendor		= glGetString( GL_VENDOR );
		const GLubyte *version		= glGetString( GL_VERSION );
		const GLubyte *glslVersion	= glGetString( GL_SHADING_LANGUAGE_VERSION );

		ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Information, 
			ML_STR("Graphics Card: %s\n"), MLtoUnicode((ML_CHAR*)renderer).c_str());
		ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Information, 
			ML_STR("Vendor: %s\n"), MLtoUnicode((ML_CHAR*)vendor).c_str());
		ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Information, 
			ML_STR("OpenGL Version: %s\n"), MLtoUnicode((ML_CHAR*)version).c_str());
		ML_TRACE( ML_LOG_GL, MLKernel::MLConsoleStatus::Information, 
			ML_STR("GLSL Version: %s\n"), MLtoUnicode((ML_CHAR*)glslVersion).c_str());

		GLint major, minor;
		glGetIntegerv(GL_MAJOR_VERSION, &major);
		glGetIntegerv(GL_MINOR_VERSION, &minor);

		m_GLFVersion = major + (ML_FLOAT)(minor/10);

		m_GLSVersion = new ML_CHAR[10];
		sprintf_s(m_GLSVersion, 10, "%i_%i", major, minor);


		//presents the window
		ShowWindow(hWnd, SW_SHOWNORMAL);

		// Slightly Higher Priority
		SetForegroundWindow(hWnd);	
		// Sets Keyboard Focus To The Window
		SetFocus(hWnd);						
	
		//set viewport to size of window
		glViewport(0,0,Width,Height);		
		glScissor(0, 0, Width, Height);
		SetEnable(GL_SCISSOR_TEST);
		ML_FLOAT halfWidth  = (ML_FLOAT)(Width/2);
		ML_FLOAT halfHeight = (ML_FLOAT)(Height/2);
		m_viewPort = MLMath::MLMat4::MLMatrix4f(halfWidth, 0.0f, 0.0f, 0.0f, 
											0.0f, halfHeight, 0.0f, 0.0f,
											0.0f, 0.0f, 1.0f, 0.0f,
											halfWidth + 0.0f, halfHeight + 0.0f, 0.0f, 1.0f);
	
		MLMath::MLMat4::MLortho(m_ortho, 0.0f, (ML_FLOAT)Height,  0.0f, (ML_FLOAT)Width, 10.0f, -10.0f);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLGLContext::StartFrame()
	{
		SetDisable(GL_SCISSOR_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		SetEnable(GL_SCISSOR_TEST);

		return ML_TRUE;
	}


	MLCoreDLL ML_BOOL MLGLContext::EndFrame()
	{
		SwapBuffers(m_hDC);
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLGLContext::SetThreadContext(void)
	{
		wglMakeCurrent(m_hDC, m_hRCthread);

		return ML_TRUE;
	}

	void MLGLContext::SetGlobalScale(MLScreenScaler& scaler)
	{
		m_globalScaler = scaler;
	}

	MLCoreDLL void MLGLContext::SetScreen(const MLScreenScaler& scaler)
	{
		SetDisable(GL_SCISSOR_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		SetEnable(GL_SCISSOR_TEST);

		ML_FLOAT Ratio = (ML_FLOAT)scaler.BaseWidth / (ML_FLOAT)scaler.BaseHeight;
		ML_FLOAT invRatio = (ML_FLOAT)scaler.BaseHeight / (ML_FLOAT)scaler.BaseWidth;
		ML_FLOAT CurrentRatio = (ML_FLOAT)MLScreenWidth / (ML_FLOAT)MLScreenHeight;

		switch (scaler.Method)
		{
		case MLScreenScaling::None:
			MLMath::MLMat4::MLidentify(MLGLContext::GetOrtho());
			MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)MLScreenHeight, 0.0f, (ML_FLOAT)MLScreenWidth, 10.0f, -10.0f);
			MLGLContext::SetViewPort(0, 0, MLScreenWidth, MLScreenHeight);
			MLGLContext::SetScissor(0, 0, MLScreenWidth, MLScreenHeight);
			break;
		case MLScreenScaling::Strech:
			MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)scaler.BaseHeight, 0.0f, (ML_FLOAT)scaler.BaseWidth, 10.0f, -10.0f);
			MLGLContext::SetViewPort(0, 0, MLScreenWidth, MLScreenHeight);
			MLGLContext::SetScissor(0, 0, MLScreenWidth, MLScreenHeight);
			break;
		case MLScreenScaling::BestFit:

			MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)scaler.BaseHeight, 0.0f, (ML_FLOAT)scaler.BaseWidth, 10.0f, -10.0f);
			if (Ratio >= CurrentRatio)
			{
				MLGLContext::SetViewPort(0, (ML_INT)(MLScreenHeight - (MLScreenWidth / Ratio)) / 2, MLScreenWidth, (ML_UINT)(MLScreenWidth / Ratio));
				MLGLContext::SetScissor(0, (ML_INT)(MLScreenHeight - (MLScreenWidth / Ratio)) / 2, MLScreenWidth, (ML_UINT)(MLScreenWidth / Ratio));
			}
			else
			{
				MLGLContext::SetViewPort((ML_INT)(MLScreenWidth - (Ratio * MLScreenHeight)) / 2, 0, (ML_UINT)(Ratio * MLScreenHeight), MLScreenHeight);
				MLGLContext::SetScissor((ML_INT)(MLScreenWidth - (Ratio * MLScreenHeight)) / 2, 0, (ML_UINT)(Ratio * MLScreenHeight), MLScreenHeight);
			}
			break;
		case MLScreenScaling::Zoom:

			MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)scaler.BaseHeight, 0.0f, (ML_FLOAT)scaler.BaseWidth, 10.0f, -10.0f);
			if (Ratio < CurrentRatio)
			{
				MLGLContext::SetViewPort(0, (ML_INT)(MLScreenHeight - (invRatio * MLScreenWidth)) / 2, MLScreenWidth, (ML_UINT)(invRatio * MLScreenWidth));
				MLGLContext::SetScissor(0, (ML_INT)(MLScreenHeight - (invRatio * MLScreenWidth)) / 2, MLScreenWidth, (ML_UINT)(invRatio * MLScreenWidth));
			}
			else if (Ratio > CurrentRatio)
			{
				MLGLContext::SetViewPort((ML_INT)(MLScreenWidth - (Ratio * MLScreenHeight)) / 2, 0, (ML_UINT)(Ratio * MLScreenHeight), MLScreenHeight);
				MLGLContext::SetScissor((ML_INT)(MLScreenWidth - (Ratio * MLScreenHeight)) / 2, 0, (ML_UINT)(Ratio * MLScreenHeight), MLScreenHeight);
			}
			else
			{
				MLGLContext::SetViewPort(0, 0, MLScreenWidth, MLScreenHeight);
				MLGLContext::SetScissor(0, 0, MLScreenWidth, MLScreenHeight);
			}
			break;

		case MLScreenScaling::Center:
			if (MLScreenWidthOffset >= 0 && MLScreenHeightOffset >= 0)
			{
				MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)MLScreenHeight, 0.0f, (ML_FLOAT)MLScreenWidth, 10.0f, -10.0f);
				MLGLContext::SetViewPort(MLScreenWidthOffset / 2, -(MLScreenHeightOffset / 2), MLScreenWidth, MLScreenHeight);
				MLGLContext::SetScissor(MLScreenWidthOffset / 2, -(MLScreenHeightOffset / 2), MLScreenWidth, MLScreenHeight);
			}
			else if (MLScreenWidthOffset < 0 && MLScreenHeightOffset < 0)
			{
				MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), -(ML_FLOAT)((MLScreenHeightOffset) / 2), (ML_FLOAT)MLScreenHeight - (ML_FLOAT)(MLScreenHeightOffset / 2),
					-(ML_FLOAT)MLScreenWidthOffset / 2, (ML_FLOAT)MLScreenWidth - (ML_FLOAT)(MLScreenWidthOffset / 2), 10.0f, -10.0f);
				MLGLContext::SetViewPort(0, 0, MLScreenWidth, MLScreenHeight);
				MLGLContext::SetScissor(0, 0, MLScreenWidth, MLScreenHeight);
			}
			else if (MLScreenWidthOffset < 0)
			{
				MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), 0.0f, (ML_FLOAT)MLScreenHeight,
					-(ML_FLOAT)MLScreenWidthOffset / 2, (ML_FLOAT)MLScreenWidth - (ML_FLOAT)(MLScreenWidthOffset / 2), 10.0f, -10.0f);
				MLGLContext::SetViewPort(0, -(MLScreenHeightOffset / 2), MLScreenWidth, MLScreenHeight);
				MLGLContext::SetScissor(0, -(MLScreenHeightOffset / 2), MLScreenWidth, MLScreenHeight);
			}
			else if (MLScreenHeightOffset < 0)
			{
				MLMath::MLMat4::MLortho(MLGLContext::GetOrtho(), -(ML_FLOAT)((MLScreenHeightOffset) / 2), (ML_FLOAT)MLScreenHeight - (ML_FLOAT)(MLScreenHeightOffset / 2),
					0.0f, (ML_FLOAT)MLScreenWidth, 10.0f, -10.0f);
				MLGLContext::SetViewPort(MLScreenWidthOffset / 2, 0, MLScreenWidth, MLScreenHeight);
				MLGLContext::SetScissor(MLScreenWidthOffset / 2, 0, MLScreenWidth, MLScreenHeight);
			}	
			break;
		}
	}


	MLCoreDLL ML_BOOL MLGLContext::SetOrtho(ML_FLOAT Left, ML_FLOAT Right, ML_FLOAT Bottom, ML_FLOAT Top, ML_FLOAT zNear, ML_FLOAT zFar)
	{
		MLMath::MLMat4::MLortho(m_ortho, Top, Bottom, Left, Right, zNear, zFar);

		return ML_TRUE;
	}

	MLCoreDLL void MLRender::ResizeGL(ML_UINT Width, ML_UINT Height)
	{
		MLScreenWidthOffset += (Width - MLScreenWidth);
		MLScreenHeightOffset += (Height - MLScreenHeight);
		MLScreenWidth = Width;
		MLScreenHeight = Height;
	}
}