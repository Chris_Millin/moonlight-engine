#ifndef MLTEXTUREATLAS_H
#define MLTEXTUREATLAS_H

#include "MLIncludes.h"
#include "MLTextureManager.h"

#include <MLMath/MLMath.h>
#include <vector>

using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;

#define ML_LOG_TEXTURE_ATLAS ML_STR("Texture Atlas")

namespace MLRender
{
	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct AtlasTexture
	{
		MLVector4f rect;
		ML_BOOL Rotated;
		void* pixelData;

		AtlasTexture()
		{
			rect = MLVector4f(0.0f);
			Rotated = ML_FALSE;
			pixelData = NULL;
		}

		//=========================================================
		// Name: Is Contained In
		// Description: Check to see if b is inside a
		// Creator: Chris Millin
		//=========================================================
		inline static ML_BOOL IsContainedIn(const AtlasTexture &a, const AtlasTexture &b)
		{
			return a.rect.x >= b.rect.x && a.rect.y >= b.rect.y 
				&& a.rect.x+a.rect.Width <= b.rect.x+b.rect.Width 
				&& a.rect.y+a.rect.Height <= b.rect.y+b.rect.Height;
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLTextureAtlas
	{
	public:
		MLCoreDLL MLTextureAtlas(ML_STRING atlasName = ML_STR(""));
		MLCoreDLL virtual ~MLTextureAtlas(void);

		MLCoreDLL ML_BOOL Release();

		MLCoreDLL void CreateAtlas(const ML_UINT Width, const ML_UINT Height, const MLPixelFormat& format, const ML_BOOL enableRotate = ML_TRUE);

		MLCoreDLL ML_BOOL AddTexture(const ML_U8STRING& Path, AtlasTexture* atlasInfo);
		MLCoreDLL ML_BOOL AddTexture(const MLKernel::MLVirtualFile* Path, AtlasTexture* atlasInfo);
		MLCoreDLL ML_BOOL AddTexture(void* Data, const ML_INT Width, const ML_INT Height, AtlasTexture* atlasInfo);

		MLCoreDLL void BuildTexture();

		MLCoreDLL MLTexture* GetTexture() { return m_textureAtlas; };

	private:

		MLCoreDLL void swapRB32(ML_BYTE* data, const ML_INT width, const ML_INT height);
		MLCoreDLL void swapRB24(ML_BYTE* data, const ML_INT width, const ML_INT height);

		MLCoreDLL void BuildRGBA32Texture();
		MLCoreDLL void BuildBGRA32Texture();
		MLCoreDLL void BuildRGB24Texture();
		MLCoreDLL void BuildBGR24Texture();
		MLCoreDLL void BuildLumiAlpha16Texture();
		MLCoreDLL void BuildChannel8Texture();

		MLCoreDLL AtlasTexture FindNewPositionNode(const MLVector2f& size);
		MLCoreDLL ML_BOOL SplitFreeNode(AtlasTexture freeNode, const AtlasTexture& usedNode);

		MLCoreDLL void CleanFreeRects();

	private:
		MLTexture* m_textureAtlas;

		std::vector<AtlasTexture> m_usedRectangles;
		std::vector<AtlasTexture> m_freeRectangles;

		ML_INT m_width;
		ML_INT m_height;

		ML_BOOL m_enableRotate;

		MLPixelFormat m_textureFormat;

		ML_STRING m_atlasTextureName;
	};

}

#endif