#ifndef MLCAMERA_H
#define MLCAMERA_H

#include "MLIncludes.h"

#include <MLMath/MLMath.h>

using namespace MLMath::MLMat4;
using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;

namespace MLRender
{

/// <summary>	Camera Log Name. </summary>
#define ML_LOG_CAMERA ML_STR("Camera")

	///-------------------------------------------------------------------------------------------------
	/// <summary>	The Camera class stores a matrix with the
	///				current position, scale and rotation of the camera. This
	///				can be obtained and used with other matrices to move
	///				objects to the correct position in relation to the camera. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLCamera
	{
	public:
		MLCoreDLL MLCamera(void) { MLMath::MLMat4::MLidentify(m_cameraMatrix); }
		MLCoreDLL ~MLCamera(void) {}


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X position of the camera. </param>
		/// <param name="y">	Y position of the camera. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetPosition(const ML_FLOAT x, const ML_FLOAT y)	{ m_position.x = x; m_position.y = y; m_position.z = 0; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="pos">	The vector2 position. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetPosition(const MLVector2f& pos)				{ m_position.x = pos.x, m_position.y = pos.y; m_position.z = 0;  ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X position of the camera. </param>
		/// <param name="y">	Y position of the camera. </param>
		/// <param name="z">	Z position of the camera. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetPosition(const ML_FLOAT x, const ML_FLOAT y, const ML_FLOAT z)	{ m_position.x = x; m_position.y = y; m_position.z = z; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the position of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="pos">	The vector3 position. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetPosition(const MLVector3f& pos)				{ m_position = pos; ComputeMatrix(); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the rotation of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="rot">	The rotation amount in radians. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetRotation(const ML_FLOAT rot)					{ m_rotation = rot; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="XY">	The scale value of both x and y. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetScale(const ML_FLOAT XY)						{ m_scale.x = XY; m_scale.y = XY; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	X scale value. </param>
		/// <param name="y">	Y scale value. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetScale(const ML_FLOAT x, const ML_FLOAT y)		{ m_scale.x = x; m_scale.y = y; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the scale of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="scale">	The scale as a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetScale(const MLVector2f& scale)				{ m_scale = scale; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the camera matrix. Will not recompute matrix
		/// 			using the current positional information until it is modified. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="Matrix">	The matrix to set the camera to. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetMatrix(const MLMatrix4f& Matrix)				{ m_cameraMatrix = Matrix; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to move the camera in the x coordinate. </param>
		/// <param name="y">	Amount to move the camera in the y coordinate. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void MoveCamera(const ML_FLOAT x, const ML_FLOAT y)	{ m_position.x += x; m_position.y += y; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="amount">	Amount to move the camera in via a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void MoveCamera(const MLVector2f& amount)				{ m_position.x += amount.x; m_position.y += amount.y; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to move the camera in the x coordinate. </param>
		/// <param name="y">	Amount to move the camera in the y coordinate. </param>
		/// <param name="z">	Amount to move the camera in the z coordinate. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void MoveCamera(const ML_FLOAT x, const ML_FLOAT y, const ML_FLOAT z)	{ m_position.x += x; m_position.y += y; m_position.z += z; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Move the camera by a value which will modify the position in turn. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="amount">	Amount to move the camera in via a vector3. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void MoveCamera(const MLVector3f& amount)				{ m_position += amount; ComputeMatrix(); }
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="XY">	Zoom in by the x and y. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void ZoomCamera(const ML_FLOAT XY)					{ m_scale.x += XY; m_scale.y += XY; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="x">	Amount to zoom in by in the x coordinates. </param>
		/// <param name="y">	Amount to zoom in by in the y coordinates. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void ZoomCamera(const ML_FLOAT x, const ML_FLOAT y)	{ m_scale.x += x; m_scale.y += x; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	The amount to Zoom the camera in by. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="scale">	Amount to zoom in by in the x and y via a vector2. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void ZoomCamera(const MLVector2f& scale)				{ m_scale += scale; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Modify the rotation of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="rot">	Amount to change the rotation by. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void RotateCamera(const ML_FLOAT rot)					{ m_rotation += rot; ComputeMatrix(); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the matrix currently being used by the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	The camera matrix object. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL MLMatrix4f& GetMatrix(void)						{ return m_cameraMatrix; }

	private:

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will create the matrix based on the position,
		///				scale and rotation of the camera. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ComputeMatrix(void);

	private:

		/// <summary>	The camera matrix value. </summary>
		MLMatrix4f m_cameraMatrix;

		/// <summary>	The position that the camera is currently at. </summary>
		MLVector3f m_position;
		/// <summary>	The rotation that the camera is currently at. </summary>
		ML_FLOAT   m_rotation;
		/// <summary>	The scale that the camera is currently at. </summary>
		MLVector2f m_scale;
	};
}

MLCoreDLL inline ML_BOOL MLRender::MLCamera::ComputeMatrix(void)
{
	MLMath::MLMat4::MLidentify(m_cameraMatrix);
	MLMatrix4f modifier;

	MLMath::MLMat4::MLscale(modifier, MLVector3f(m_scale.x, m_scale.y, 1.0f));

	m_cameraMatrix = m_cameraMatrix * modifier;

	MLMath::MLMat4::MLrotation(modifier, 0.0f, 0.0f, m_rotation);

	m_cameraMatrix = m_cameraMatrix * modifier;

	MLMath::MLMat4::MLtranslate(modifier, m_position);

	m_cameraMatrix = m_cameraMatrix * modifier;

	return ML_TRUE;
}

#endif