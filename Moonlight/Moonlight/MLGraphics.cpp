#include "MLGraphics.h"

namespace MLRender
{

//=========================================================
#pragma region Intialisation
ML_BOOL MLGraphics::Initalise(const ML_UINT initalBatchSize, const MLScreenScaler& scaling)
{
	//Set begin to be uncalled so end cant be called first after
	//Intialise
	m_beginCall = ML_FALSE;

	m_screenScaler = scaling;

	if (MLCameraManager::HasCamera(ML_DEFAULT_CAMERA))
	{
		m_cameraName = ML_DEFAULT_CAMERA;
	}
	else
	{
		MLCameraManager::CreateCamera(ML_DEFAULT_CAMERA);
		m_cameraName = ML_DEFAULT_CAMERA;
	}

	//build the shaders which will be used for the default shading
	InitialiseShader();

	//build the vertex buffer with the specified size
	InitaliseVertexBuffer(initalBatchSize);

	//Initalise the final items
	InitaliseFinal(initalBatchSize);

	return ML_TRUE;
}

ML_BOOL MLGraphics::InitialiseShader()
{
	//Create and load the base shader
	m_baseShader = MLShaderManager::CreateShader();
	m_baseShader->LoadShaderFile(BASE_SHADER, GL_VERTEX_SHADER);
	m_baseShader->LoadShaderFile(BASE_SHADER, GL_GEOMETRY_SHADER);
	m_baseShader->LoadShaderFile(BASE_SHADER, GL_FRAGMENT_SHADER);

	//lock the shader ready to bind attributes
	m_baseShader->Lock();
	m_baseShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXPOS,	BASE_SHADER_VERT_POS);
	m_baseShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXCOL,	BASE_SHADER_VERT_COL);
	m_baseShader->BindAtt(MLRender::MLAttID::ML_ATT_TEXTURE0,	BASE_SHADER_VERT_UV);
	m_baseShader->LinkShaders();
	m_baseShader->Unlock();

	return ML_TRUE;
}

ML_BOOL MLGraphics::InitaliseVertexBuffer(const ML_UINT initalBatchSize)
{
	//build the virtual buffer first
	m_virtualBufferOffset = 0;
	//This is multiplied by 6 as there is no index buffer each sprite
	//is made up of 6 vertex's. not all batched object will be sprites
	//but this should provide enough space for if all the objects are
	//sprites
	m_virutalBufferSize = initalBatchSize * QUAD_SIZE;
	m_virtualBuffer = new MLVertexPosTexCol[m_virutalBufferSize];

	//create the main buffer
	m_vertexBuffer = new MLVertexBuffer();
	m_vertexBuffer->CreateBuffer(GL_ARRAY_BUFFER, DEFAULT_BUFFER_SIZE, GL_STREAM_DRAW);
	m_vertexBuffer->Bind();
	m_vertexBuffer->SetBufferData(NULL, GL_STREAM_DRAW);
	m_vertexBuffer->Unbind();
	
	m_vertexBufferSize = DEFAULT_BUFFER_SIZE;
	m_vertexBufferOffset = 0;

	return ML_TRUE;
}

ML_BOOL MLGraphics::InitaliseFinal(const ML_UINT initalBatchSize)
{
	//initalise the drawable object storage and queue
	m_drawableObjectMaxSize = initalBatchSize;
	m_drawableObjects = new MLBatchItem[m_drawableObjectMaxSize];

	m_drawableObjectQueue.clear();
 
	//Load the 2x2 white texture ready to be used with primitives
	LoadBlankTexture();

	return ML_TRUE;
}

ML_BOOL MLGraphics::LoadBlankTexture()
{
	m_blankTexture = MLTextureManager::CreateTexture();

	//create a blank 2x2 white texture for primitive rendering;
	GLubyte* texdata = new GLubyte[4 * 2 * 2];
	for(ML_INT y=0; y < 2; y++) 
	{
		for(ML_INT x=0; x < 2; x++)
		{
			texdata[((x + y * 2) * 4)]			= 255;
			texdata[((x + y * 2) * 4) + 1]		= 255;
			texdata[((x + y * 2) * 4) + 2]		= 255;
			texdata[((x + y * 2) * 4) + 3]		= 255;	
		}
	}

	m_blankTexture->StoreTexture(texdata, 2, 2, GL_RGBA);

	return ML_TRUE;
}
#pragma endregion Intialisation
//=========================================================

//=========================================================
#pragma region Release
void MLGraphics::Release()
{
	if(m_virtualBuffer)
	{
		delete[] m_virtualBuffer;
		m_virtualBuffer = NULL;
	}

	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		delete m_vertexBuffer;
		m_vertexBuffer = NULL;
	}

	if(m_drawableObjectQueue.size() > 0)
	{
		m_drawableObjectQueue.clear();
	}

	if(m_drawableObjects)
	{
		delete[] m_drawableObjects;
		m_drawableObjects = NULL;
	}

	MLTextureManager::RemoveTexture(m_blankTexture);
	MLShaderManager::RemoveShader(m_baseShader);
}
#pragma endregion Release
//=========================================================

//=========================================================
#pragma region Start/End
ML_BOOL MLGraphics::Begin(const ML_BOOL Alpha /* = true*/, const ML_BOOL wireFrame /* = true*/)
{
	//Set the rendering settings
	
	//does nothing for the moment
	m_wireFrame = wireFrame;

	if(wireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//simple check for alpha. This is pre-set to true
	if (Alpha)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
		glDisable(GL_BLEND);

	glEnable(GL_PROGRAM_POINT_SIZE);
	//set the begincall to true so now end can be called without error
	m_beginCall = ML_TRUE;

	return ML_TRUE;
}

ML_BOOL MLGraphics::End()
{
	if (!m_beginCall)
	{
		return ML_FALSE;
	}

	if (m_drawableObjectQueue.size() > 0)
		FlushBatch();

	//reset both buffers back to 0
	m_virtualBufferOffset = 0;
	m_vertexBufferOffset = 0;
	//clear the drawable object queue. items are still stored in the array but will be overwritten
	//as the data is not used for anything else.
	m_drawableObjectQueue.clear();
	
	return ML_TRUE;
}
#pragma endregion Start/End
//=========================================================

//=========================================================
#pragma region Draw calls
void MLGraphics::Draw(const MLSprite* sprite)
{
	BatchSprite(sprite, GL_TRIANGLES);
}

void MLGraphics::DrawPoint(const MLPoint* point)
{
	BatchPoint(point, GL_POINTS);
}

void MLGraphics::DrawLine(const MLLine* line)
{
	BatchLine(line, GL_TRIANGLES);
}

void MLGraphics::DrawQuad(const MLQuad* quad)
{
	BatchQuad(quad, GL_TRIANGLES);
}

void MLGraphics::DrawDuelQuad(MLQuad* quad, const MLVector4f& colour)
{
	//batching order may effect the layering
	MLVector4f colourBefore = quad->Colour;
	if(quad->Filled)
	{
		//This is not the best way in the world to do this but saves creating new object
		//to just render the same object agin but with 2 different settings
		quad->Filled = ML_FALSE;
		quad->Colour = colour;
		BatchQuad(quad, GL_TRIANGLES);
		quad->Filled = ML_TRUE;
		quad->Colour = colourBefore;
		BatchQuad(quad, GL_TRIANGLES);
	}
	else
	{
		BatchQuad(quad, GL_TRIANGLES);
		quad->Filled = ML_TRUE;
		quad->Colour = colour;
		BatchQuad(quad, GL_TRIANGLES);
		quad->Filled = ML_FALSE ;
		quad->Colour = colourBefore;
	}
}

void MLGraphics::DrawCircle(const MLCircle* circle)
{
	if(circle->Filled)
		BatchCircle(circle, GL_TRIANGLE_FAN);
	else
		BatchCircle(circle, GL_TRIANGLES);
}

void MLGraphics::DrawDuelCircle(MLCircle* circle, const MLVector4f& colour)
{
	//batching order may effect the layering
	MLVector4f colourBefore = circle->Colour;
	if(circle->Filled)
	{
		//This is not the best way in the world to do this but saves creating new object
		//to just render the same object again but with 2 different settings
		circle->Filled = ML_FALSE;
		circle->Colour = colour;
		BatchCircle(circle, GL_TRIANGLES);
		circle->Filled = ML_TRUE;
		circle->Colour = colourBefore;
		BatchCircle(circle, GL_TRIANGLE_FAN);
	}
	else
	{
		BatchCircle(circle, GL_TRIANGLES);
		circle->Filled = ML_TRUE;
		circle->Colour = colour;
		BatchCircle(circle, GL_TRIANGLE_FAN);
		circle->Filled = ML_FALSE ;
		circle->Colour = colourBefore;
	}
}

void MLGraphics::DrawVertexArray(const MLVertexArray* vertexArray)
{
	BatchVertexArray(vertexArray, GL_TRIANGLES);
}

void MLGraphics::Write(const MLWrite* write, ML_STRING& text, ...)
{
	va_list args;
	va_start(args, text);
	ML_UINT size = _vscwprintf(text.c_str(), args) + 1;
	ML_SCHAR* buffer = new ML_SCHAR[size];
	vswprintf(buffer, size, text.c_str(), args);

	BatchText(write, text);
}
#pragma endregion Draw Calls
//=========================================================

//=========================================================
#pragma region Batch Calls
void MLGraphics::BatchSprite(const MLSprite* sprite, const ML_INT drawMode)
{
	//check to see if there is enough space in the virtual buffer and if not expand it to give it enough space
	if((m_virtualBufferOffset + QUAD_SIZE) >= m_virutalBufferSize)
		ExpandBatch();

	//since the size of the que will always be one greater then the array index (exception though when 0 but both will
	//be 0 any way) the size can be used as the next index
	AddToBatchQueue(sprite, drawMode, QUAD_SIZE);

	//to convert the UV into the correct coordinates
	ML_FLOAT widthval = 1.0f/(ML_FLOAT)sprite->Texture->GetTextureWidth();
	ML_FLOAT heighval = 1.0f/(ML_FLOAT)sprite->Texture->GetTextureHeight();

	//get the matrix for the item
	MLMatrix4f resultMat = MatrixSetUp(sprite->Scale, sprite->Origin, sprite->Rotation, sprite->Position);

	//work out the points of the quad
	MLVector4f topLeft = resultMat * MLVector4f(0,0,0,1);
	MLVector4f topRight = resultMat * MLVector4f(sprite->Size.Width,0,0,1);
	MLVector4f bottomLeft = resultMat * MLVector4f(0,sprite->Size.Height,0,1);
	MLVector4f bottomRight = resultMat * MLVector4f(sprite->Size.Width, sprite->Size.Height,0,1);

	//work out the texture coordinates of the quad
	MLVector2f UVtopLeft = MLVector2f(sprite->Source.x * widthval, sprite->Source.y * heighval);
	MLVector2f UVtopRight = MLVector2f(sprite->Source.Width * widthval, sprite->Source.y * heighval);
	MLVector2f UVbottomLeft = MLVector2f(sprite->Source.x * widthval, sprite->Source.Height * heighval);
	MLVector2f UVbottomRight = MLVector2f(sprite->Source.Width * widthval, sprite->Source.Height * heighval);

	//set the vertex data
	//top left
	AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), UVtopLeft, sprite->Colour);
	//top right
	AddToVirtualBuffer(MLVector3f(topRight.x,topRight.y, 1.0f), UVtopRight, sprite->Colour);
	//bottom right
	AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), UVbottomRight, sprite->Colour);
	//bottom left
	AddToVirtualBuffer(MLVector3f(bottomLeft.x, bottomLeft.y, 1.0f), UVbottomLeft, sprite->Colour);
	//top left
	AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), UVtopLeft, sprite->Colour);
	//bottom right
	AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), UVbottomRight, sprite->Colour);
}

void MLGraphics::BatchPoint(const MLPoint* point, const ML_INT drawMode)
{
	//check to see if there is enough space in the virtual buffer and if not expand it to give it enough space
	if((m_virtualBufferOffset + POINT_SIZE) >= m_virutalBufferSize)
		ExpandBatch();

	//since the size of the queue will always be one greater then the array index (exception though when 0 but both will
	//be 0 any way) the size can be used as the next index
	AddToBatchQueue(point, drawMode, POINT_SIZE);

	//set the vertex data
	AddToVirtualBuffer(MLVector3f(point->Position.x, point->Position.y, point->Size), MLVector2f(1.0f), point->Colour);
}

void MLGraphics::BatchLine(const MLLine* line, const ML_INT drawMode)
{
	if((m_virtualBufferOffset + QUAD_SIZE) >= m_virutalBufferSize)
		ExpandBatch();

	AddToBatchQueue(line, drawMode, QUAD_SIZE);

	MLVector2f P1 = line->Point1;
	MLVector2f P2 = line->Point2;

	//Get the perpendicular of the line
	MLVector2f perp = P1 - P2;
	perp.normalise();
	//work out the way it is pointing
	perp = MLVector2f(-perp.y, perp.x);

	MLVector2f topLeft = P1 - (perp * line->Width / 2);
	MLVector2f topRight = P1 + (perp * line->Width / 2);
	MLVector2f bottomLeft = P2 - (perp * line->Width / 2);
	MLVector2f bottomRight = P2 + (perp * line->Width / 2);

	//set the vertex data
	//top left
	AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), MLVector2f(0.0f), line->Colour);
	//top right
	AddToVirtualBuffer(MLVector3f(topRight.x,topRight.y, 1.0f), MLVector2f(0.0f), line->Colour);
	//bottom right
	AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), MLVector2f(0.0f), line->Colour);
	//bottom left
	AddToVirtualBuffer(MLVector3f(bottomLeft.x, bottomLeft.y, 1.0f), MLVector2f(0.0f), line->Colour);
	//top left
	AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), MLVector2f(0.0f), line->Colour);
	//bottom right
	AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), MLVector2f(0.0f), line->Colour);
}

void MLGraphics::BatchQuad(const MLQuad* quad, const ML_INT drawMode)
{
	//check to see if there is enough space in the virtual buffer and if not expand it to give it enough space
	if((m_virtualBufferOffset + QUAD_SIZE) >= m_virutalBufferSize)
		ExpandBatch();

	MLMatrix4f resultMat = MatrixSetUp(quad->Scale, quad->Origin, quad->Rotation, quad->Position);

	//work out the points of the quad
	MLVector4f topLeft		= resultMat * MLVector4f(0,0,0,1);
	MLVector4f topRight		= resultMat * MLVector4f(quad->Size.Width,0,0,1);
	MLVector4f bottomLeft	= resultMat * MLVector4f(0,quad->Size.Height,0,1);
	MLVector4f bottomRight	= resultMat * MLVector4f(quad->Size.Width, quad->Size.Height,0,1);

	if (quad->Filled)
	{
		//since the size of the que will always be one greater then the array index (exception though when 0 but both will
		//be 0 any way) the size can be used as the next index
		AddToBatchQueue(quad, drawMode, QUAD_SIZE);

		//set the vertex data
		//top left
		AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), MLVector2f(0.0f), quad->Colour);
		//top right
		AddToVirtualBuffer(MLVector3f(topRight.x,topRight.y, 1.0f), MLVector2f(0.0f), quad->Colour);
		//bottom right
		AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), MLVector2f(0.0f), quad->Colour);
		//bottom left
		AddToVirtualBuffer(MLVector3f(bottomLeft.x, bottomLeft.y, 1.0f), MLVector2f(0.0f), quad->Colour);
		//top left
		AddToVirtualBuffer(MLVector3f(topLeft.x,topLeft.y, 1.0f), MLVector2f(0.0f), quad->Colour);
		//bottom right
		AddToVirtualBuffer(MLVector3f(bottomRight.x, bottomRight.y, 1.0f), MLVector2f(0.0f), quad->Colour);
	}
	else
	{
		//this is not the best way to do this but it will work as the pointer should be valid from the quad
		//and the layer gets copied same as the other bits of information so its fine to go out of scope
		MLLine tempLine;
		tempLine.Point1		= MLVector2f(topLeft.x, topLeft.y - quad->LineWidth/2);
		tempLine.Point2		= MLVector2f(bottomLeft.x, bottomLeft.y + quad->LineWidth/2);
		tempLine.Colour		= quad->Colour;
		tempLine.Width		= (ML_FLOAT)quad->LineWidth;
		tempLine.Layer		= quad->Layer;
		tempLine.Shader		= quad->Shader;
		tempLine.UniformFunction = quad->UniformFunction;

		BatchLine(&tempLine, GL_TRIANGLES);
		tempLine.Point1		= MLVector2f(bottomLeft.x, bottomLeft.y);
		tempLine.Point2		= MLVector2f(bottomRight.x, bottomRight.y);
		BatchLine(&tempLine, GL_TRIANGLES);
		tempLine.Point1		= MLVector2f(bottomRight.x, bottomRight.y + quad->LineWidth/2);
		tempLine.Point2		= MLVector2f(topRight.x, topRight.y - quad->LineWidth/2);
		BatchLine(&tempLine, GL_TRIANGLES);
		tempLine.Point1		= MLVector2f(topRight.x, topRight.y);
		tempLine.Point2		= MLVector2f(topLeft.x, topLeft.y);
		BatchLine(&tempLine, GL_TRIANGLES);
	}
	
}

void MLGraphics::BatchCircle(const MLCircle* circle, const ML_INT drawMode)
{
	ML_FLOAT accuracy = 360.0f/circle->Segments;

	//check to see if there is enough space in the virtual buffer and if not expand it to give it enough space
	if((m_virtualBufferOffset + POINT_SIZE * (circle->Segments + 2)) >= m_virutalBufferSize)
		ExpandBatch();
	

	if (circle->Filled)
	{
		AddToBatchQueue(circle, drawMode, POINT_SIZE * (circle->Segments + 2));

		//get the matrix for the item
		MLMatrix4f resultMat = MatrixSetUp(circle->Scale, circle->Origin, 0.0f, circle->Position);

		MLVector4f circlePoint = resultMat * MLVector4f(0.0f,0.0f,0.0f,1.0f);

		AddToVirtualBuffer(MLVector3f(circlePoint.x, circlePoint.y, 1.0f), MLVector2f(0.0f,0.0f), circle->Colour);

		for (ML_FLOAT angle = accuracy; angle <= 360 + accuracy; angle += accuracy)
		{
			circlePoint = resultMat * MLVector4f((ML_FLOAT)sin(ML_Deg2Rad(angle)) * circle->Size.Width, (ML_FLOAT)cos(ML_Deg2Rad(angle)) * circle->Size.Height,0.0f,1.0f);
			AddToVirtualBuffer(MLVector3f(circlePoint.x, circlePoint.y, 1.0f), MLVector2f(0.0f,0.0f), circle->Colour);
		}
	}
	else
	{
		MLLine tempLine;
		tempLine.Colour		= circle->Colour;
		tempLine.Width		= (ML_FLOAT)circle->LineWidth;
		tempLine.Layer		= circle->Layer;
		tempLine.Shader		= circle->Shader;
		tempLine.UniformFunction = circle->UniformFunction;

		//get the matrix for the item
		MLMatrix4f resultMat = MatrixSetUp(circle->Scale, circle->Origin, 0.0f, circle->Position);


		MLVector4f circlePoint = resultMat * MLVector4f((ML_FLOAT)sin(ML_Deg2Rad(accuracy)) * circle->Size.Width, (ML_FLOAT)cos(ML_Deg2Rad(accuracy)) * circle->Size.Height,0.0f,1.0f);
		tempLine.Point1 = MLVector2f(circlePoint.x, circlePoint.y);
		circlePoint = resultMat * MLVector4f((ML_FLOAT)sin(ML_Deg2Rad(accuracy * 2)) * circle->Size.Width, (ML_FLOAT)cos(ML_Deg2Rad(accuracy * 2)) * circle->Size.Height,0.0f,1.0f);
		tempLine.Point2 = MLVector2f(circlePoint.x, circlePoint.y);
		BatchLine(&tempLine, GL_TRIANGLES);

		for (ML_FLOAT angle = (accuracy * 3); angle <= 360.0f + accuracy; angle += accuracy)
		{
			tempLine.Point1 = tempLine.Point2;
			circlePoint = resultMat * MLVector4f((ML_FLOAT)sin(ML_Deg2Rad(angle)) * circle->Size.Width, (ML_FLOAT)cos(ML_Deg2Rad(angle)) * circle->Size.Height,0.0f,1.0f);
			tempLine.Point2 = MLVector2f(circlePoint.x, circlePoint.y);
			BatchLine(&tempLine, GL_TRIANGLES);
		}
	}

}

void MLGraphics::BatchVertexArray(const MLVertexArray* vertexArray, const ML_INT drawMode)
{
	//check to see if there is enough space in the virtual buffer and if not expand it to give it enough space
	if((m_virtualBufferOffset + (vertexArray->VertexCount * POINT_SIZE)) >= m_virutalBufferSize)
		ExpandBatch();

	//since the size of the que will always be one greater then the array index (exception though when 0 but both will
	//be 0 any way) the size can be used as the next index
	AddToBatchQueue(vertexArray, drawMode, (vertexArray->VertexCount * POINT_SIZE));

	//to convert the UV into the correct coordinates
	ML_FLOAT widthval = 1.0f/(ML_FLOAT)vertexArray->Texture->GetTextureWidth();
	ML_FLOAT heighval = 1.0f/(ML_FLOAT)vertexArray->Texture->GetTextureHeight();

	//get the matrix for the item
	MLMatrix4f resultMat = MatrixSetUp(vertexArray->Scale, vertexArray->Origin, vertexArray->Rotation, vertexArray->Position);

	//loop through all the vertexs to work out the position and the UV that relates to it
	for (ML_INT i = 0; i < vertexArray->VertexCount; i++)
	{
		MLVector4f VertexPositon = resultMat * MLVector4f(vertexArray->VertexData[i].x,vertexArray->VertexData[i].y,0,1);
		MLVector2f UV = MLVector2f(vertexArray->VertexData[i].x * widthval, vertexArray->VertexData[i].y * heighval);
		AddToVirtualBuffer(MLVector3f(VertexPositon.x, VertexPositon.y, 1.0f), UV, vertexArray->Colour);
	}
}

void MLGraphics::BatchText(const MLWrite* write, const ML_STRING& text)
{
	//check to ensure the 
	if(!text.empty())
	{
		MLVector2f WriterPositon = write->Position;
		MLVector2f TextBoxPosition = write->TBoxPositionOffset;
		//Generate the rotation matrix
		MLMath::MLMat4::MLMatrix4f WriteMat = MatrixSetUp(1.0f, write->Origin, write->Rotation, WriterPositon - TextBoxPosition);
	
		//Get the font object
		MLFont* font = write->Font;
	
		//Get the text width and height for bounding box usage
		ML_FLOAT textWidth	= ((ML_FLOAT)font->GetTextWidth(text) * write->Scale.x);
		ML_FLOAT textHeight = ((ML_FLOAT)font->GetTextHeight(text) * write->Scale.y);
	
		//The x and y increment of the font based on the justification
		MLVector2f xyIncrement = 0.0f;
		if(write->Alignment == MLJustifyText::LeftJustify)
			xyIncrement.x = 0.0f;
		if(write->Alignment == MLJustifyText::CentreJustify)
			xyIncrement.x = (textWidth - (font->GetLineWidth(0, text) * write->Scale.y))/2.0f;
		if(write->Alignment == MLJustifyText::RightJustify)
			xyIncrement.x = (textWidth - (font->GetLineWidth(0, text) * write->Scale.y));	
		
		//Highlight Quad if there is any highlighted text
		MLVector2f xyStart = xyIncrement;
		MLQuad highlight;
		highlight.Rotation  = write->Rotation;
		highlight.Scale		= write->Scale;
		highlight.Layer		= write->Layer - 1;
		highlight.Filled	= ML_TRUE;
		highlight.Colour	= MLVector4f(1 - write->Colour.r,1 - write->Colour.g,1 - write->Colour.b, 0.5f);
		highlight.Size.y	= (ML_FLOAT)font->GetSize();
		ML_UINT newlineCounter = 0;

		MLLine textLine;
		textLine.Width	= write->LineThickness * write->Scale.y;
		textLine.Colour = write->Colour;
		textLine.Layer	= write->Layer;

		if(write->LineType == MLFontLine::Underline)
		{
			textLine.Point1 = xyIncrement;
			textLine.Point1.y += font->GetSize() * write->Scale.y;
		}
		if(write->LineType == MLFontLine::Strike)
		{
			textLine.Point1 = xyIncrement;
			//The 1.5 is a little hack to get it more near the middle since the font its
			//positioned half the size.
			textLine.Point1.y += font->GetSize()/1.6f * write->Scale.y;
		}

		//Loop through all the characters
		for (ML_UINT c = 0; c < text.length(); c++)
		{	
			//if the character is not a \n then it will need rendering
			if(text[c] != ML_STR('\n') && text[c] != 0)
			{
				//check to see if the current character is the start of the highlight
				if(c - newlineCounter == write->HighlightStart)
				{
					//set the xyIncrement to its position if it is
					highlight.Position = xyIncrement;
				}

				//Get the glyph information object
				MLGlyph glyphInfo	= font->GetGlyph(text[c]);
				
				//Get the sprite for the glyph
				MLSprite glyphSprite		= font->GetGlyphSprite(glyphInfo);
				//Set the position of the glyph sprite to be at the offset by the x and y bearings
				//of the character with the half scale taken off them because the origin of the
				//glyph is central and the bearing is based off a bottom left origin.
				glyphSprite.Position.x -= ((glyphInfo.bearingX * write->Scale.x) - (glyphInfo.Width * write->Scale.y)/2);
				glyphSprite.Position.y -= ((glyphInfo.bearingY * write->Scale.y) - (glyphInfo.Height * write->Scale.y)/2);
				//glyphSprite.Position.y += ;
				//Check for kerning support
				if(c != 0 && font->KerningSupport())
					glyphSprite.Position.x += font->GetKerning(text[c - 1], text[c]).x * write->Scale.y; //move the glyph by the kerning amount
	
				//Apply the rotation, scale, and colour modification to the sprite
				glyphSprite.Rotation	= write->Rotation;
				glyphSprite.Origin		*= write->Scale;
				glyphSprite.Scale		= write->Scale;
				glyphSprite.Colour		*= write->Colour;
				glyphSprite.Shader		= write->Shader;
				glyphSprite.UniformFunction = write->UniformFunction;
				//Modify the position to be in relation to the text scale, origin, rotation and position.
				//also add the current increment of the advance in the x and y to the current position
				//so the glyph is aligned to its position in the text.
				glyphSprite.Position = WriteMat * (glyphSprite.Position + (xyIncrement));
	
				//Set the layer to the same as the writer
				glyphSprite.Layer = write->Layer;

				if(write->TBoxEnabled)
				{
					if(RenderInTextBox(&glyphSprite, write))
						Draw(&glyphSprite);
				}
				else
				{
					Draw(&glyphSprite);
				}
				
				//Increase the x increment by the glyph advance
				xyIncrement.x += glyphInfo.advance * write->Scale.x;

				if(c - newlineCounter >= write->HighlightStart && c - newlineCounter  < write->HighlightEnd)
				{
					highlight.Size.x		+= glyphInfo.advance;
					highlight.Position		 = xyStart + (MLVector2f((highlight.Size.x/2.0f), (font->GetSize()/2.0f)) * write->Scale);
					highlight.Origin		 = MLVector2f(-(highlight.Size.x/2.0f), -(highlight.Size.y/2.0f)) * write->Scale;
				}

				if(c - newlineCounter == write->HighlightEnd)
				{
					highlight.Position = WriteMat * (highlight.Position);
					if (!write->TBoxEnabled)	
						DrawQuad(&highlight);
					else if(RenderInTextBox(&highlight, write))
						DrawQuad(&highlight);
					xyStart = xyIncrement;
					highlight.Size.x = 0;
					highlight.Size.y = (ML_FLOAT)font->GetSize();
				}
			}
			else if (text[c] == ML_STR('\n'))
			{
				if(write->LineType == MLFontLine::Underline)
				{
					textLine.Point2 = xyIncrement;
					textLine.Point2.y += font->GetSize() * write->Scale.y;
					textLine.Point1 = WriteMat * (textLine.Point1);
					textLine.Point2 = WriteMat * (textLine.Point2);
					if(!write->TBoxEnabled)
						DrawLine(&textLine);
					else if(RenderInTextBox(&textLine, write))
						DrawLine(&textLine);
				}
				if(write->LineType == MLFontLine::Strike)
				{
					textLine.Point2 = xyIncrement;
					textLine.Point2.y += font->GetSize()/1.6f * write->Scale.y;
					textLine.Point1 = WriteMat * (textLine.Point1);
					textLine.Point2 = WriteMat * (textLine.Point2);
					if(!write->TBoxEnabled)
						DrawLine(&textLine);
					else if(RenderInTextBox(&textLine, write))
						DrawLine(&textLine);
				}

				if(c != text.length())
				{
					//If a new line is defined increase the font increment in the
					//y by the font height and reset the x increment back to 0
					if(write->Alignment == MLJustifyText::LeftJustify)
						xyIncrement.x = 0.0f;
					if(write->Alignment == MLJustifyText::CentreJustify)
						xyIncrement.x = (textWidth - (font->GetLineWidth(c+1, text) * write->Scale.y))/2.0f;
					if(write->Alignment == MLJustifyText::RightJustify)
						xyIncrement.x = (textWidth - (font->GetLineWidth(c+1, text) * write->Scale.y));	

					xyIncrement.y += write->Font->GetSize() * write->Scale.x;

					if(write->LineType == MLFontLine::Underline)
					{
						textLine.Point1 = xyIncrement;
						textLine.Point1.y += font->GetSize() * write->Scale.y;
					}
					if(write->LineType == MLFontLine::Strike)
					{
						textLine.Point1 = xyIncrement;
						textLine.Point1.y += font->GetSize()/1.6f * write->Scale.y;
					}

					xyStart = xyIncrement;
				}
				if(c - newlineCounter <= write->HighlightEnd && c - newlineCounter > write->HighlightStart && write->HighlightStart != write->HighlightEnd)
				{
					newlineCounter++;
					highlight.Position = WriteMat * (highlight.Position);
					if (!write->TBoxEnabled)	
						DrawQuad(&highlight);
					else if(RenderInTextBox(&highlight, write))
						DrawQuad(&highlight);
					highlight.Size.x = 0;
					highlight.Size.y = (ML_FLOAT)font->GetSize();
				}
			}
			if(text[c] == ML_STR('\0') || c == text.length() - 1)
			{
				if(write->LineType == MLFontLine::Underline)
				{
					textLine.Point2 = xyIncrement;
					textLine.Point2.y += font->GetSize() * write->Scale.y;
					textLine.Point1 = WriteMat * (textLine.Point1);
					textLine.Point2 = WriteMat * (textLine.Point2);
					if(!write->TBoxEnabled)
						DrawLine(&textLine);
					else if(RenderInTextBox(&textLine, write))
						DrawLine(&textLine);
				}
				if(write->LineType == MLFontLine::Strike)
				{
					textLine.Point2 = xyIncrement;
					textLine.Point2.y += font->GetSize()/1.6f * write->Scale.y;
					textLine.Point1 = WriteMat * (textLine.Point1);
					textLine.Point2 = WriteMat * (textLine.Point2);
					if(!write->TBoxEnabled)
						DrawLine(&textLine);
					else if(RenderInTextBox(&textLine, write))
						DrawLine(&textLine);
				}
				break;
			}
		}
	}
}
#pragma endregion Batch Calls
//=========================================================

//=========================================================
#pragma region Helper Functions
ML_BOOL MLGraphics::RenderInTextBox(MLSprite* sprite, const MLWrite* write)
{
	MLVector4f TBoxBounds;
	TBoxBounds.xmin = write->Position.x + write->Origin.x;
	TBoxBounds.ymin = write->Position.y + write->Origin.y;
	TBoxBounds.xmax = write->Position.x + write->Origin.x + write->TBoxSize.Width;
	TBoxBounds.ymax = write->Position.y + write->Origin.y + write->TBoxSize.Height;

	MLVector4f SpriteBounds;
	SpriteBounds.xmin = sprite->Position.x - (sprite->Size.Width * write->Scale.x)/2.0f;
	SpriteBounds.ymin = sprite->Position.y - (sprite->Size.Height * write->Scale.y)/2.0f;
	SpriteBounds.xmax = sprite->Position.x + (sprite->Size.Width * write->Scale.x)/2.0f;
	SpriteBounds.ymax = sprite->Position.y + (sprite->Size.Height * write->Scale.y)/2.0f;

	if(write->TBoxFlow == ML_FALSE)
	{
		if( SpriteBounds.xmin > TBoxBounds.xmin && SpriteBounds.ymin > TBoxBounds.ymin &&
			SpriteBounds.xmax < TBoxBounds.xmax && SpriteBounds.ymax < TBoxBounds.ymax)
			return ML_TRUE;
		else
			return ML_FALSE;
	}
	else
	{
		//If all the sprite is in the text box
		if( SpriteBounds.xmin > TBoxBounds.xmin && SpriteBounds.ymin > TBoxBounds.ymin &&
			SpriteBounds.xmax < TBoxBounds.xmax && SpriteBounds.ymax < TBoxBounds.ymax)
			return ML_TRUE;

		//remove left out of bounds
		if( SpriteBounds.xmin < TBoxBounds.xmin &&
			SpriteBounds.xmax < TBoxBounds.xmin)
			return ML_FALSE;
		//remove top out of bounds
		if( SpriteBounds.ymin < TBoxBounds.ymin &&
			SpriteBounds.ymax < TBoxBounds.ymin)
			return ML_FALSE;
		///remove right out of bounds
		if( SpriteBounds.xmin > TBoxBounds.xmax)
			return ML_FALSE;
		//remove bottom out of bounds
		if( SpriteBounds.ymin > TBoxBounds.ymax)
			return ML_FALSE;

		//If the left is out of the text box
		if(SpriteBounds.xmin <= TBoxBounds.xmin)
		{
			ML_FLOAT distance = TBoxBounds.xmin - SpriteBounds.xmin;
			if(distance <= (sprite->Size.x * write->Scale.x))
			{
				ML_FLOAT width		 = sprite->Size.x - (distance / write->Scale.x);
				ML_FLOAT widthScale	 = (sprite->Size.x * write->Scale.x) - distance;
				sprite->Position.x	-= widthScale - (sprite->Size.x * write->Scale.x);
				sprite->Source.x	-= width - sprite->Size.x;
				sprite->Size.x		 = width;
			}
		}
		//If the top is out of the text box
		if(SpriteBounds.ymin <= TBoxBounds.ymin)
		{
			ML_FLOAT distance = TBoxBounds.ymin - SpriteBounds.ymin;
			if(distance <= (sprite->Size.y * write->Scale.y))
			{
				ML_FLOAT height			 = sprite->Size.y - (distance/write->Scale.y);
				ML_FLOAT heightScale	 = (sprite->Size.y * write->Scale.y) - distance;
				sprite->Position.y		-= heightScale - (sprite->Size.y * write->Scale.y);
				sprite->Source.y		-= height - sprite->Size.y;
				sprite->Size.y			 = height;
			}
		}
		//If the right is out of the text box
		if(SpriteBounds.xmax >= TBoxBounds.xmax)
		{
			ML_FLOAT distance = SpriteBounds.xmax - TBoxBounds.xmax;
			if(distance <= (sprite->Size.x * write->Scale.x))
			{
				ML_FLOAT width		 = sprite->Size.x - (distance / write->Scale.x);
				sprite->Source.Width -= distance / write->Scale.x;
				sprite->Size.x = width;
			}
		}
		//If the bottom is out of the text box
		if(SpriteBounds.ymax >= TBoxBounds.ymax)
		{
			ML_FLOAT distance = SpriteBounds.ymax - TBoxBounds.ymax;
			if(distance <= (sprite->Size.y * write->Scale.y))
			{
				ML_FLOAT height = sprite->Size.y - (distance/write->Scale.y);
				sprite->Source.Height -= distance/write->Scale.y;
				sprite->Size.y = height;
			}
		}

		return ML_TRUE;
	}
}

ML_BOOL MLGraphics::RenderInTextBox(MLQuad* Quad, const MLWrite* write)
{
	MLVector4f TBoxBounds;
	TBoxBounds.xmin = write->Position.x + write->Origin.x;
	TBoxBounds.ymin = write->Position.y + write->Origin.y;
	TBoxBounds.xmax = write->Position.x + write->Origin.x + write->TBoxSize.Width;
	TBoxBounds.ymax = write->Position.y + write->Origin.y + write->TBoxSize.Height;

	MLVector4f SpriteBounds;
	SpriteBounds.xmin = Quad->Position.x - (Quad->Size.Width * write->Scale.x)/2.0f;
	SpriteBounds.ymin = Quad->Position.y - (Quad->Size.Height * write->Scale.y)/2.0f;
	SpriteBounds.xmax = Quad->Position.x + (Quad->Size.Width * write->Scale.x)/2.0f;
	SpriteBounds.ymax = Quad->Position.y + (Quad->Size.Height * write->Scale.y)/2.0f;

	//If all the sprite is in the text box
	if( SpriteBounds.xmin > TBoxBounds.xmin && SpriteBounds.ymin > TBoxBounds.ymin &&
		SpriteBounds.xmax < TBoxBounds.xmax && SpriteBounds.ymax < TBoxBounds.ymax)
		return ML_TRUE;

	//remove left out of bounds
	if( SpriteBounds.xmin < TBoxBounds.xmin &&
		SpriteBounds.xmax < TBoxBounds.xmin)
		return ML_FALSE;
	//remove top out of bounds
	if( SpriteBounds.ymin < TBoxBounds.ymin &&
		SpriteBounds.ymax < TBoxBounds.ymin)
		return ML_FALSE;
	///remove right out of bounds
	if( SpriteBounds.xmin > TBoxBounds.xmax)
		return ML_FALSE;
	//remove bottom out of bounds
	if( SpriteBounds.ymin > TBoxBounds.ymax)
		return ML_FALSE;

	//If the left is out of the text box
	if(SpriteBounds.xmin <= TBoxBounds.xmin)
	{
		ML_FLOAT distance = TBoxBounds.xmin - SpriteBounds.xmin;
		if(distance <= (Quad->Size.x * write->Scale.x))
		{
			ML_FLOAT width		 = Quad->Size.x - (distance / write->Scale.x);
			ML_FLOAT widthScale	 = (Quad->Size.x * write->Scale.x) - distance;
			Quad->Position.x	-= widthScale - (Quad->Size.x * write->Scale.x);
			Quad->Size.x		 = width;
		}
	}
	//If the top is out of the text box
	if(SpriteBounds.ymin <= TBoxBounds.ymin)
	{
		ML_FLOAT distance = TBoxBounds.ymin - SpriteBounds.ymin;
		if(distance <= (Quad->Size.y * write->Scale.y))
		{
			ML_FLOAT height			 = Quad->Size.y - (distance/write->Scale.y);
			ML_FLOAT heightScale	 = (Quad->Size.y * write->Scale.y) - distance;
			Quad->Position.y		-= heightScale - (Quad->Size.y * write->Scale.y);
			Quad->Size.y			 = height;
		}
	}
	//If the right is out of the text box
	if(SpriteBounds.xmax >= TBoxBounds.xmax)
	{
		ML_FLOAT distance = SpriteBounds.xmax - TBoxBounds.xmax;
		if(distance <= (Quad->Size.x * write->Scale.x))
		{
			ML_FLOAT width		 = Quad->Size.x - (distance / write->Scale.x);
			Quad->Size.x = width;
		}
	}
	//If the bottom is out of the text box
	if(SpriteBounds.ymax >= TBoxBounds.ymax)
	{
		ML_FLOAT distance = SpriteBounds.ymax - TBoxBounds.ymax;
		if(distance <= (Quad->Size.y * write->Scale.y))
		{
			ML_FLOAT height		 = Quad->Size.y - (distance/write->Scale.y);
			Quad->Size.y = height;
		}
	}

	return ML_TRUE;
}

ML_BOOL MLGraphics::RenderInTextBox(MLLine* line, const MLWrite* write)
{
	//THERE IS A BUG HERE IF THE LINE SIZE IS REALLY BIG IT WONT 
	//ADAPT TO CUTTING THE LINE AND WILL INSTEAD NOT RENDERING IT ONCE
	//THE TOP OR BOTTOM MOST POINT IS HIT
	MLVector4f TBoxBounds;
	TBoxBounds.xmin = write->Position.x + write->Origin.x;
	TBoxBounds.ymin = write->Position.y + write->Origin.y;
	TBoxBounds.xmax = write->Position.x + write->Origin.x + write->TBoxSize.Width;
	TBoxBounds.ymax = write->Position.y + write->Origin.y + write->TBoxSize.Height;

	MLVector4f LineBounds;
	LineBounds.xmin = line->Point1.x;
	LineBounds.ymin = line->Point1.y - ((line->Width/2.0f) * write->Scale.y);
	LineBounds.xmax = line->Point2.x;
	LineBounds.ymax = line->Point2.y + ((line->Width/2.0f) * write->Scale.y);;

	//If all the sprite is in the text box
	if( LineBounds.xmin > TBoxBounds.xmin && LineBounds.ymin > TBoxBounds.ymin &&
		LineBounds.xmax < TBoxBounds.xmax && LineBounds.ymax < TBoxBounds.ymax)
		return ML_TRUE;

	//remove left out of bounds
	if( LineBounds.xmin < TBoxBounds.xmin &&
		LineBounds.xmax < TBoxBounds.xmin)
		return ML_FALSE;
	//remove top out of bounds
	if( LineBounds.ymin < TBoxBounds.ymin &&
		LineBounds.ymax < TBoxBounds.ymin)
		return ML_FALSE;
	///remove right out of bounds
	if( LineBounds.xmin > TBoxBounds.xmax)
		return ML_FALSE;
	//remove bottom out of bounds
	if( LineBounds.ymin > TBoxBounds.ymax)
		return ML_FALSE;

	//If the left is out of the text box
	if(LineBounds.xmin <= TBoxBounds.xmin)
	{
		ML_FLOAT distance = TBoxBounds.xmin - LineBounds.xmin;
		line->Point1.x		+= distance;
	}
	////If the top is out of the text box
	//if(LineBounds.ymin <= TBoxBounds.ymin)
	//{
	//	ML_FLOAT distance = TBoxBounds.ymin - LineBounds.ymin;
	//	if(distance <= ((line->Point1.y) * write->Scale.y))
	//	{
	//		ML_FLOAT height			 = line->Point1.y - (distance/write->Scale.y);
	//		ML_FLOAT heightScale	 = ((line->Point1.y) * write->Scale.y) - distance;
	//		line->Point1.y			-= heightScale - (line->Point1.y * write->Scale.y);
	//	}
	//}
	//If the right is out of the text box
	if(LineBounds.xmax >= TBoxBounds.xmax)
	{
		ML_FLOAT distance = LineBounds.xmax - TBoxBounds.xmax;
		line->Point2.x		-= distance;
	}
	////If the bottom is out of the text box
	//if(LineBounds.ymax >= TBoxBounds.ymax)
	//{
	//	ML_FLOAT distance = LineBounds.ymax - TBoxBounds.ymax;
	//	if(distance <= (line->Point2.y * write->Scale.y))
	//	{
	//		ML_FLOAT height			 = line->Point2.y - (distance/write->Scale.y);
	//		ML_FLOAT heightScale	 = ((line->Point2.y) * write->Scale.y) - distance;
	//		line->Point2.y			-= heightScale - (line->Point2.y * write->Scale.y);
	//	}
	//}

	return ML_TRUE;
}

MLMatrix4f MLGraphics::MatrixSetUp(const MLVector2f& scale, const MLVector2f& origin, const ML_FLOAT& rotation, const MLVector2f& position)
{
	//0 = scale 1 = origin trans 2 = rotation 3 = pos trans
	MLMatrix4f homoMats[4];
	
	//identify the matrices
	MLMath::MLMat4::MLidentify(homoMats[0]);
	MLMath::MLMat4::MLidentify(homoMats[1]);
	MLMath::MLMat4::MLidentify(homoMats[2]);
	MLMath::MLMat4::MLidentify(homoMats[3]);

	//work out the scale it its not 1,1
	if (scale.x != 1.0f || scale.y != 1.0f)
		MLMath::MLMat4::MLscale(homoMats[0], MLVector3f(scale.x, scale.y, 1.0f));
	//work out the origin offset if its not 0,0
	if (origin.x != 0 || origin.y != 0)
		MLMath::MLMat4::MLtranslate(homoMats[1], MLVector3f(origin.x, origin.y, 0.0f));
	//work out the rotation is its not 0
	if (rotation != 0.0f)
		MLMath::MLMat4::MLrotation(homoMats[2], 0.0f, 0.0f,  rotation);

	//work out the position
	MLMath::MLMat4::MLtranslate(homoMats[3], MLVector3f(position.x, position.y, 0.0f));

	//create the result matrix
	return homoMats[3] * homoMats[2] * homoMats[1] * homoMats[0];
}

void MLGraphics::AddToVirtualBuffer(const MLVector3f& position, const MLVector2f& UV, const MLVector4f& colour)
{
	m_virtualBuffer[m_virtualBufferOffset].VPos		= position;
	m_virtualBuffer[m_virtualBufferOffset].UV		= UV;
	if(m_wireFrame)
		m_virtualBuffer[m_virtualBufferOffset].Colour	= MLVector4f(1.0f);
	else
		m_virtualBuffer[m_virtualBufferOffset].Colour	= colour;
	m_virtualBufferOffset++;
}

void MLGraphics::AddToBatchQueue(const MLBaseGraphicType* item, const ML_INT& drawMode, const ML_INT& bufferSize)
{
	if(item->Texture)
		m_drawableObjects[m_drawableObjectQueue.size()].TextureID			= item->Texture->GetTextureID();
	else
		m_drawableObjects[m_drawableObjectQueue.size()].TextureID			= m_blankTexture->GetTextureID();

	m_drawableObjects[m_drawableObjectQueue.size()].LayerID				= item->Layer;
	m_drawableObjects[m_drawableObjectQueue.size()].DrawMode			= drawMode;
	m_drawableObjects[m_drawableObjectQueue.size()].VirtualBufferOffset = m_virtualBufferOffset;
	m_drawableObjects[m_drawableObjectQueue.size()].VirtualBufferSize	= bufferSize;

	if (item->Shader)
		m_drawableObjects[m_drawableObjectQueue.size()].DrawShader			= item->Shader;
	else
		m_drawableObjects[m_drawableObjectQueue.size()].DrawShader			= m_baseShader;
	
	m_drawableObjects[m_drawableObjectQueue.size()].UniformatFunction	= item->UniformFunction;

	//in order to speed up the sort process pointers to the array will be stored in the vector
	//when the sort is performed it will only be moving 4bytes of memory rather than the size
	//of a batchitem
	m_drawableObjectQueue.push_back(&m_drawableObjects[m_drawableObjectQueue.size()]);
}

void MLGraphics::ReadyVertexBuffer()
{
	//bind the vertex buffer to see the shader attributes
	m_vertexBuffer->Bind();

	//enable the vertex attributes for the VAO in this case vertex pos and texture are needed
	glEnableVertexAttribArray((GLuint)MLRender::ML_ATT_VERTEXPOS);  // Vertex position
	glEnableVertexAttribArray((GLuint)MLRender::ML_ATT_TEXTURE0);	//vertex texture coord
	glEnableVertexAttribArray((GLuint)MLRender::ML_ATT_VERTEXCOL);

	glVertexAttribPointer(MLRender::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(MLVertexPosTexCol), (GLubyte *)NULL );
	glVertexAttribPointer(MLRender::ML_ATT_TEXTURE0,  2, GL_FLOAT, GL_FALSE, sizeof(MLVertexPosTexCol), (GLubyte*)sizeof(MLVector3f));
	glVertexAttribPointer(MLRender::ML_ATT_VERTEXCOL, 4, GL_FLOAT, GL_FALSE, sizeof(MLVertexPosTexCol), (GLubyte*)(sizeof(MLVector2f) + sizeof(MLVector3f)));

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();
}

ML_BOOL MLGraphics::FlushBatch()
{
	MLGLContext::SetScreen(m_screenScaler);

	if (MLCameraManager::GetActiveCameraName() != m_cameraName && MLCameraManager::HasCamera(m_cameraName))
		MLCameraManager::SetActiveCamera(m_cameraName);
	else if (MLCameraManager::GetActiveCameraName() != m_cameraName && !MLCameraManager::HasCamera(m_cameraName))
		MLCameraManager::SetActiveCamera(ML_DEFAULT_CAMERA);

	//enables the vertex attributes
	ReadyVertexBuffer();

	//shouldn't happen but just in cast double check for 0 objects in the queue
	if (m_drawableObjectQueue.size() == 0)
		return ML_FALSE;

	//need to add a check to see if no sorting is required if requested
	SortBatch();

	//The first item to check against the next. will also be used to check against the previous
	//item to see if there has been any change.
	MLBatchItem* lastItem = m_drawableObjectQueue[0];

	//will store the amount of objects did not change in the queue so they can be batch rendered
	//together to reduce the render time.
	ML_INT itemCount = 1;
	ML_INT startIndex = 0; 

	for (ML_UINT i = 1; i < m_drawableObjectQueue.size(); i++)
	{
		if ((*m_drawableObjectQueue[i]) != (*lastItem))
		{
			Render(startIndex, itemCount);
			startIndex += itemCount;
			itemCount = 0;
		}

		lastItem = m_drawableObjectQueue[i];
		itemCount++;
	}

	Render(startIndex, itemCount);

	return ML_TRUE;
}

ML_BOOL MLGraphics::SortBatch()
{
	std::sort(m_drawableObjectQueue.begin(), m_drawableObjectQueue.end(), &SortFronttoBack);
	return ML_TRUE;
}

ML_BOOL MLGraphics::ExpandBatch()
{
	//if the batch needs to be expanded hen create a new batch
	MLVertexPosTexCol* newSize = new MLVertexPosTexCol[(m_virutalBufferSize * 2) + 1];

	//copy over all the data and the new pointers to the vector
	memcpy(&newSize[0], &m_virtualBuffer[0], (VERTEX_SIZE * m_virutalBufferSize));

	//delete the old array
	delete[] m_virtualBuffer;
	//make the old array pointer point to the new memory 
	m_virtualBuffer = newSize;
	//increase the batch size to the new size
	m_virutalBufferSize = (m_virutalBufferSize * 2) + 1;

	return ML_TRUE;
}

ML_BOOL MLGraphics::SetCamera(const ML_STRING& name)
{
	if (m_beginCall && MLCameraManager::HasCamera(name) && m_drawableObjectQueue.size() > 0)
	{
		End();
	}
	else if (!MLCameraManager::HasCamera(name))
	{
		return ML_FALSE;
	}

	m_cameraName == name;
	
	return ML_TRUE;
}

ML_BOOL MLGraphics::SetScreenScaler(const MLScreenScaler& scaler)
{
	if (m_beginCall && m_drawableObjectQueue.size() > 0)
	{
		End();
	}

	m_screenScaler = scaler;

	return ML_TRUE;
}

#pragma endregion Helper Functions
//=========================================================

//=========================================================
#pragma region Rendering
void MLGraphics::Render(const ML_INT& startIndex, const ML_INT& count)
{
	//bind the vertex buffer
	m_vertexBuffer->Bind();

	ML_INT mapSize = VERTEX_SIZE * m_drawableObjectQueue[startIndex]->VirtualBufferSize;

	m_drawableObjectQueue[startIndex]->DrawShader->Lock();

	m_drawableObjectQueue[startIndex]->DrawShader->AddUniform("Projection", MLGLContext::GetOrtho());
	m_drawableObjectQueue[startIndex]->DrawShader->AddUniform("View", MLCameraManager::GetActiveCamera()->GetMatrix().Transpose());
	m_drawableObjectQueue[startIndex]->DrawShader->AddUniform("Tex1", 0);

	if (m_drawableObjectQueue[startIndex]->UniformatFunction != NULL)
	{
		m_drawableObjectQueue[startIndex]->UniformatFunction(m_drawableObjectQueue[startIndex]->DrawShader);
	}

	//check to see if the vertex buffer has enough memory to store the amount of sprites that need to render
	if (m_vertexBufferOffset + (mapSize * count) >= m_vertexBufferSize)
	{
		//if not the orphan a new buffer
		m_vertexBuffer->SetBufferData(m_vertexBufferSize, NULL, GL_STREAM_DRAW);
		//move the buffer to 0 offset
		m_vertexBufferOffset = 0;
	}

	//map the size needed to store the vertex data
	MLVertexPosTexCol* mappedData = (MLVertexPosTexCol*)m_vertexBuffer->MapRange(m_vertexBufferOffset, (mapSize * count));

	//copy over the memory from the virtual buffer to the memory that will be sent to the GPU
	for (ML_INT i = startIndex; i < count + startIndex; i++)
	{
		memcpy(&mappedData[(i- startIndex) * m_drawableObjectQueue[i]->VirtualBufferSize], &m_virtualBuffer[m_drawableObjectQueue[i]->VirtualBufferOffset], mapSize);
	}

	//unmap the buffer
	m_vertexBuffer->Unmap();
	
	//bind the texture
	if(!m_wireFrame)
		glBindTexture(GL_TEXTURE_2D, m_drawableObjectQueue[startIndex]->TextureID);
	else
		glBindTexture(GL_TEXTURE_2D, m_blankTexture->GetTextureID());

	//If there is a custom shader and custom uniform in place then 
	if (m_drawableObjectQueue[startIndex]->DrawShader != NULL && m_drawableObjectQueue[startIndex]->UniformatFunction != NULL)
	{
		m_drawableObjectQueue[startIndex]->UniformatFunction(m_drawableObjectQueue[startIndex]->DrawShader);
	}

	//draw the objects
	glDrawArrays(m_drawableObjectQueue[startIndex]->DrawMode, (m_vertexBufferOffset/VERTEX_SIZE), count * m_drawableObjectQueue[startIndex]->VirtualBufferSize);
	
	//off set the buffer by the drawn amount
	m_vertexBufferOffset += (count * mapSize);

	//unbind the vertex buffer
	m_vertexBuffer->Unbind();

	//unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//unlock the shader 
	m_drawableObjectQueue[startIndex]->DrawShader->Unlock();
}
#pragma endregion Rendering
//=========================================================

}