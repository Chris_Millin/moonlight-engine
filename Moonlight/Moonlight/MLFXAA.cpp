#include "MLFXAA.h"

namespace MLRender
{
	MLFXAA::MLFXAA(MLFXAAType type) : MLRenderTarget()
	{
		m_type = type;

		m_fxaaShader = MLShaderManager::CreateShader();
		m_fxaaShader->LoadShaderFile(ML_FXAA_SHADER, GL_VERTEX_SHADER);
		m_fxaaShader->LoadShaderFile(ML_FXAA_SHADER, GL_FRAGMENT_SHADER);
	}


	MLFXAA::~MLFXAA(void)
	{
		Release();
	}

	void MLFXAA::Release()
	{
		MLShaderManager::RemoveShader(m_fxaaShader);
		MLTextureManager::RemoveTexture(m_outputTexture);
	}

	void MLFXAA::Initalise(const MLFBOTypes enableFBO /* = MLFBOTypes::ColourBuffer */)
	{
		m_fxaaShader->Lock();
		m_fxaaShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXPOS,	ML_FXAA_SHADER_VERT_POS);
		m_fxaaShader->BindAtt(MLRender::MLAttID::ML_ATT_VERTEXCOL,	ML_FXAA_SHADER_VERT_COL);
		m_fxaaShader->BindAtt(MLRender::MLAttID::ML_ATT_TEXTURE0,	ML_FXAA_SHADER_VERT_UV);
		m_fxaaShader->LinkShaders();
		m_fxaaShader->Unlock();

		m_outputTexture = MLTextureManager::CreateTexture();
		m_outputTexture->CreateEmptyTexture(1280, 720); //THIS SHOULD BE SCREEN X AND Y!!!

		m_FBO = new MLFrameBuffer();
		m_FBO->CreateBuffer(GL_FRAMEBUFFER);
		m_FBO->Bind();

		if(enableFBO == MLFBOTypes::ColourBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_COLOR_ATTACHMENT0);
		else if(enableFBO == MLFBOTypes::StencilBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_STENCIL_ATTACHMENT);
		else if(enableFBO == MLFBOTypes::DepthBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_DEPTH_ATTACHMENT);

		if(m_FBO)
		{
			if(m_FBO->GetStatus() == GL_FRAMEBUFFER_COMPLETE)
				ML_TRACE(ML_LOG_FXAA, MLKernel::MLConsoleStatus::Information, ML_STR("Frame Buffer Status OK"));
			m_FBO->UnBind();
		}
	}

	void MLFXAA::Draw(MLGraphics* batch)
	{
		m_fxaaShader->Lock();
		m_fxaaShader->AddUniform(ML_STRA("sceneTexture"), 0);
	
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_outputTexture->GetTextureID());

		m_fxaaShader->Unlock();

		//START SPRITEBATCH

		MLSprite sprite;
		sprite.Texture = m_outputTexture;
		sprite.SetSizeFromTexture();
		sprite.SetSourceFromTexture();
		sprite.Shader = m_fxaaShader;

		batch->Draw(&sprite);

		//ENDSPRITEBATCH
	}
}
