#include "MLAudioManager.h"

namespace MLAudio
{

	ALCcontext*			MLAudioManager::m_context;
	ALCdevice*			MLAudioManager::m_device;

	MLCoreDLL ML_BOOL MLAudioManager::InitaliseAudio()
	{
		ML_TRACE(ML_LOG_AUDIO_MANAGER, MLKernel::MLConsoleStatus::Progress,
			ML_STR("Creating OpenAL context\n"));

		//creates a device
		m_device = alcOpenDevice(NULL);
		if (!m_device)
			return ML_FALSE;
		//creates the context form the device
		m_context = alcCreateContext(m_device, NULL);
		//make the new context the current context
		alcMakeContextCurrent(m_context);
		if (!m_context)
			return ML_FALSE;

		//get the number of mono and stereo sources
		ALCint nummono, numstereo;
		ALError(alcGetIntegerv(m_device, ALC_MONO_SOURCES, 1, &nummono));
		ALError(alcGetIntegerv(m_device, ALC_STEREO_SOURCES, 1, &numstereo));

		const ML_CHAR* name = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
		ML_U8STRING alDevice(name, strlen(name));

		ML_TRACE(ML_LOG_AUDIO_MANAGER, MLKernel::MLConsoleStatus::Information, 
			ML_STR("Audio Device %S\n"), alDevice.c_str());
		ML_TRACE(ML_LOG_AUDIO_MANAGER, MLKernel::MLConsoleStatus::Information,
			ML_STR("Max mono sources: %d\n"), nummono);
		ML_TRACE(ML_LOG_AUDIO_MANAGER, MLKernel::MLConsoleStatus::Information,
			ML_STR("Max stereo sources: %d\n"), numstereo);

		ML_TRACE(ML_LOG_AUDIO_MANAGER, MLKernel::MLConsoleStatus::Progress,
			ML_STR("Loading FFMPEG Codecs\n"));

		avcodec_register_all();
		av_register_all();

		return ML_TRUE;
	}

	ML_BOOL MLAudioManager::ReleaseAudio()
	{
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information,
			ML_STR("Releasing OpenAL\n"));

		ALError(alcMakeContextCurrent(NULL));
		ALError(alcDestroyContext(m_context));
		ALError(alcCloseDevice(m_device));

		return true;
	}

	ML_BOOL MLAudioManager::ReleaseCodec(MLCodec* codec)
	{
		if (codec && codec->avFmtCtx)
		{
			av_free(codec->avFmtCtx);
			codec->avFmtCtx = NULL;
		}
		if (codec && codec->avCodecCtx)
		{
			avcodec_close(codec->avCodecCtx);
			av_free(codec->avCodecCtx);
			codec->avCodecCtx = NULL;
		}

		if (codec)
		{
			delete codec;
			codec = NULL;
		}

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLAudioManager::SetListener()
	{
		ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };

		ALError(alListener3f(AL_POSITION, 0, 0, 1.0f));
		// check for errors
		ALError(alListener3f(AL_VELOCITY, 0, 0, 0));
		// check for errors
		ALError(alListenerfv(AL_ORIENTATION, listenerOri));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLAudioManager::SetListener(const MLAudio::MLAudioListener& listener)
	{
		//set the litener orientation to up for the moment
		ALfloat listenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
		//gain
		ALError(alListenerf(AL_GAIN, listener.gain));
		// Position ...
		ALError(alListenerfv(AL_POSITION, MLMath::MLVec3::MLVector3f::toptr(listener.position)));
		// Velocity ...
		ALError(alListenerfv(AL_VELOCITY, MLMath::MLVec3::MLVector3f::toptr(listener.velocity)));
		// Orientation ...
		ALError(alListenerfv(AL_ORIENTATION, listenerOri));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLAudioManager::SetListenerPosition(const MLMath::MLVec3::MLVector3f& Position)
	{
		//set just the listener position
		ALError(alListenerfv(AL_POSITION, MLMath::MLVec3::MLVector3f::toptr(Position)));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLAudioManager::SetListenerVolume(const ML_FLOAT val)
	{
		ALError(alListenerf(AL_GAIN, val));
		
		return ML_TRUE;
	}

	ML_BOOL MLAudioManager::ALFailed(const ML_INT line)
	{
		ALenum error = 0;

		if ((error = alGetError()) == AL_NO_ERROR)
			return ML_FALSE;

		switch (error)
		{
		case AL_INVALID_NAME:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Invalid Name in line %d.\n"), line);
			break;
		case AL_INVALID_ENUM:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Invalid Enum in line %d.\n"), line);
			break;
		case AL_INVALID_VALUE:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Invalid Value in line %d.\n"), line);
			break;
		case AL_INVALID_OPERATION:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Invalid Operation in line %d.\n"), line);
			break;
		case AL_OUT_OF_MEMORY:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Out Of Memory at line %d.\n"), line);
			break;
		default:
			ML_TRACE(ML_LOG_OPENAL, MLKernel::MLConsoleStatus::Error,
				ML_STR("Unknown error (%i) at line %d.\n"), error, line);
			break;
		};

		return ML_TRUE;
	}

	ML_BOOL MLAudioManager::SetSourceSettings(const ALuint& source, const MLAudioSource& audioSource)
	{
		ALError(alSourcef(source, AL_MAX_DISTANCE, audioSource.maxDistance));
		ALError(alSourcef(source, AL_ROLLOFF_FACTOR, audioSource.rollOff));
		ALError(alSourcef(source, AL_REFERENCE_DISTANCE, audioSource.referenceDistance));
		ALError(alSourcef(source, AL_MIN_GAIN, audioSource.minGain));
		ALError(alSourcef(source, AL_MAX_GAIN, audioSource.maxGain));
		ALError(alSourcef(source, AL_CONE_OUTER_GAIN, audioSource.coneOuterGain));
		ALError(alSourcef(source, AL_CONE_INNER_ANGLE, audioSource.coneInnerAngle));
		ALError(alSourcef(source, AL_CONE_OUTER_ANGLE, audioSource.coneOuterAngle));
		ALError(alSourcei(source, AL_LOOPING, audioSource.looping));
		ALError(alSourcefv(source, AL_POSITION,  MLMath::MLVec3::MLVector3f::toptr(audioSource.position)));
		ALError(alSourcefv(source, AL_VELOCITY,  MLMath::MLVec3::MLVector3f::toptr(audioSource.velocity)));
		ALError(alSourcefv(source, AL_DIRECTION, MLMath::MLVec3::MLVector3f::toptr(audioSource.direction)));

		return ML_TRUE;
	}

	MLCodec* MLAudioManager::LoadAudioFile(const MLKernel::MLVirtualFile* input)
	{
		ML_INT error;
		MLCodec* audioCodec = new MLCodec();

		audioCodec->avFmtCtx = avformat_alloc_context();
		if (!audioCodec->avFmtCtx)
		{
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not create FFMPEG format context\n"));
			return NULL;
		}

		error = avformat_open_input(&audioCodec->avFmtCtx, MLtoAscii(input->FullFilePath()).c_str(), NULL, NULL);
		if (error)
		{
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to open file \"%s\". Make sure it is a valid path\n"), input->FullFilePath().c_str());
			return NULL;
		}

		error = avformat_find_stream_info(audioCodec->avFmtCtx, NULL);
		if (error)
		{
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error,
				ML_STR("Unable to retrieve stream info.\n"));
			return NULL;
		}

		AVStream* stream = NULL;
		for (ML_UINT i = 0; i < audioCodec->avFmtCtx->nb_streams; i++)
		{
			if (audioCodec->avFmtCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
			{
				stream = audioCodec->avFmtCtx->streams[i];
				break;
			}
		}

		if (stream == NULL)
		{
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to find a audio stream in the file %s\n"), input->FullFilePath().c_str());
			return NULL;
		}

		audioCodec->avStream = stream;
		audioCodec->avCodecCtx = stream->codec;
		audioCodec->avCodec = avcodec_find_decoder(audioCodec->avCodecCtx->codec_id);

		error = avcodec_open2(audioCodec->avCodecCtx, audioCodec->avCodec, NULL);
		if (error)
		{
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to open codec for file %s\n"), input->FullFilePath().c_str());
			return NULL;
		}

		ML_DOUBLE divDuration = (1.0 / (ML_DOUBLE)av_q2d(audioCodec->avStream->time_base));

		ML_INT alloc = ((audioCodec->avCodecCtx->sample_rate * (audioCodec->avCodecCtx->bits_per_raw_sample / 8)) * (ML_INT)audioCodec->avStream->duration);

		ML_UINT minutes = (ML_UINT)(audioCodec->avStream->duration / divDuration) / 60;
		ML_UINT minutes_scale = minutes % 60;
		ML_UINT seconds_scale = (ML_UINT)(audioCodec->avStream->duration / divDuration) % 60;

#if _DEBUG

		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Audio File Info\n"));
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("File Name: %S\n"), audioCodec->avFmtCtx->filename);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Channels: %d\n"), audioCodec->avCodecCtx->channels);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Codec Type: %S\n"), audioCodec->avCodecCtx->codec->long_name);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Sample Rate: %d\n"), audioCodec->avCodecCtx->sample_rate);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Bit Rate: %d\n"), audioCodec->avCodecCtx->bit_rate);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Block Alignment: %d\n"), audioCodec->avCodecCtx->block_align);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Bit Per Sample: %d\n"), audioCodec->avCodecCtx->bits_per_raw_sample);
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Data Format: %S\n"), av_get_sample_fmt_name(audioCodec->avCodecCtx->sample_fmt));
		ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Information, ML_STR("Duration: %dm%ds\n"), minutes_scale, seconds_scale);

#endif

		return audioCodec;
	}

	MLAudioFrame* MLAudioManager::ProcessAudioFrame(const ML_UINT InputFormat, const MLCodec* InputCodec, const AVFrame* InputFrame)
	{
		MLAudioFrame* formattedFrame = NULL;
		ML_SHORT* outputData;

		ML_INT dataSize = 0;
		ML_INT nb_samples = InputFrame->nb_samples;
		ML_INT channels = InputFrame->channels;
		ML_INT frameDataSize = nb_samples * channels;

		switch (InputFormat)
		{
		case AVSampleFormat::AV_SAMPLE_FMT_U8:
		{
			formattedFrame = new MLAudioFrame();
			outputData = new ML_SHORT[frameDataSize];

			for (ML_INT i = 0; i < nb_samples; i++)
			{
				for (ML_INT c = 0; c < channels; c++)
				{
					ML_BYTE* data = (ML_BYTE*)InputFrame->data[0];
					ML_BYTE sample = data[i * channels + c];
					outputData[i * channels + c] = (short)(((sample << 8) | sample) ^ 0x8000);
				}
			}

			formattedFrame->Data = outputData;
			formattedFrame->Size = frameDataSize;
			break;
		}
		case AVSampleFormat::AV_SAMPLE_FMT_S16:
		{
			formattedFrame = new MLAudioFrame();
			outputData = new ML_SHORT[frameDataSize];

			for (ML_INT i = 0; i < nb_samples; i++)
			{
				for (ML_INT c = 0; c < channels; c++)
				{
					ML_SHORT* data = (ML_SHORT*)InputFrame->data[0];
					ML_SHORT sample = data[i * channels + c];
					outputData[i * channels + c] = sample;
				}
			}

			formattedFrame->Data = outputData;
			formattedFrame->Size = frameDataSize;
			break;
		}
		case AVSampleFormat::AV_SAMPLE_FMT_FLT:
		{
			formattedFrame = new MLAudioFrame();
			outputData = new ML_SHORT[frameDataSize];

			for (ML_INT i = 0; i < nb_samples; i++)
			{
				for (ML_INT c = 0; c < channels; c++)
				{
					ML_FLOAT* data = (ML_FLOAT*)InputFrame->data[0];
					ML_FLOAT sample = data[i * channels + c];
					if (sample < -1.0f) sample = -1.0f;
					if (sample > 1.0f) sample = 1.0f;
					outputData[i * channels + c] = (ML_SHORT)round(sample * 32767.0f);
				}
			}

			formattedFrame->Data = outputData;
			formattedFrame->Size = frameDataSize;

			break;
		}
		case AVSampleFormat::AV_SAMPLE_FMT_S16P:
		{
			formattedFrame = new MLAudioFrame();
			outputData = new ML_SHORT[frameDataSize];

			for (ML_INT i = 0; i < nb_samples; i++)
			{
				for (ML_INT c = 0; c < channels; c++)
				{
					ML_SHORT* data = (ML_SHORT*)InputFrame->data[c];
					ML_SHORT sample = data[i];
					outputData[i * channels + c] = sample;
				}
			}

			formattedFrame->Data = outputData;
			formattedFrame->Size = frameDataSize;

			break;
		}
		case AVSampleFormat::AV_SAMPLE_FMT_FLTP:
		{
			formattedFrame = new MLAudioFrame();
			outputData = new ML_SHORT[frameDataSize];

			for (ML_INT i = 0; i < nb_samples; i++)
			{
				for (ML_INT c = 0; c < channels; c++)
				{
					ML_FLOAT* extended_data = (ML_FLOAT*)InputFrame->extended_data[c];
					ML_FLOAT sample = extended_data[i];
					if (sample < -1.0f) sample = -1.0f;
					else if (sample > 1.0f) sample = 1.0f;
					outputData[i * channels + c] = (ML_SHORT)round(sample * 32767.0f);
				}
			}

			formattedFrame->Data = outputData;
			formattedFrame->Size = frameDataSize;

			break;
		}
		default:
			ML_TRACE(ML_LOG_AUDIO, MLKernel::MLConsoleStatus::Error, ML_STR("Sample format is invalid and cant be processed\n"));
			return NULL;
		}

		return formattedFrame;
	}
}