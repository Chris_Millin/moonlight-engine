#include "MLShaderManager.h"

namespace MLRender
{
	MLCoreDLL MLShaderManager::MLShaderGroup MLShaderManager::m_shaders;

	MLShader* MLShaderManager::CreateShader()
	{
		return CreateShader(ML_EMPTY_STRING);
	}

	MLShader* MLShaderManager::CreateShader(ML_STRING& name)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(!ShaderExist(name))
			m_shaders[name] = new MLShader();

		return m_shaders[name];
	}

	MLShader* MLShaderManager::CreateShader(ML_STRING& name, ML_STRING& file, const GLenum type)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(m_shaders[name] == NULL)
			m_shaders[name] = new MLShader();

		m_shaders[name]->LoadShaderFile(file, type);
		return m_shaders[name];

	}

	MLShader* MLShaderManager::CreateShader(ML_STRING& name, MLKernel::MLVirtualFile* file, const GLenum type)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(m_shaders[name] == NULL)
			m_shaders[name] = new MLShader();

		m_shaders[name]->LoadShaderFile(file, type);
		return m_shaders[name];
	}

	ML_BOOL MLShaderManager::RemoveShader(const ML_STRING& name)
	{
		if(ShaderExist(name))
		{
			MLRELEASE(m_shaders[name]);
			m_shaders.erase(name);

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SHADER_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant remove from non existing group."), name.c_str());

			return ML_FALSE;
		}
	}

	ML_BOOL MLShaderManager::RemoveShader(const MLShader* shader)
	{
		MLShaderGroup::iterator it;

		ML_BOOL found = ML_FALSE;

		for(it = m_shaders.begin(); it != m_shaders.end(); it++)
		{
			if(it->second == shader)
			{
				MLRELEASE(it->second);
				break;
			}
		}

		

		if (it != m_shaders.end())
		{
			m_shaders.erase(it);
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	ML_BOOL MLShaderManager::RemoveAll()
	{
		MLShaderGroup::iterator it;

		for(it = m_shaders.begin(); it != m_shaders.end(); it++)
		{
			MLRELEASE(it->second);
		}

		m_shaders.clear();

		return ML_TRUE;
	}

	ML_BOOL MLShaderManager::ShaderExist(const ML_STRING& name)
	{
		return m_shaders.find(name) != m_shaders.end();
	}

	MLShader* MLShaderManager::GetShader(const ML_STRING& name)
	{
		if(ShaderExist(name))
		{
			return m_shaders[name];
		}
		else
		{
			ML_TRACE(ML_LOG_SHADER_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant remove non existing group."), name.c_str());

			return NULL;
		}
	}

	ML_STRING MLShaderManager::GetShaderName(const MLShader* shader)
	{
		MLShaderGroup::iterator it;

		for(it = m_shaders.begin(); it != m_shaders.end(); it++)
		{
			if(it->second == shader)
			{
				return it->first;
			}
		}

		return ML_EMPTY_STRING;
	}
}