#ifndef MLINPUT_H
#define MLINPUT_H

#include "MLIncludes.h"

namespace MLInput
{
#define ML_KEY_COUNT 256
#define ML_MOUSE_BUTTON_COUNT 5
	//=========================================================
	// Name: Input Type
	// Description: This is just a list of types that can be
	// obtained from raw input. At the moment this is only really
	// used for the keyboard but can also be used for the mouse.
	// Creator: Chris Millin
	//=========================================================
	enum class MLInputType
	{
		Pointer		= 0x01,
		Mouse		= 0x02,
		Joystick	= 0x04,
		Gamepad		= 0x05,
		Keyboard	= 0x06,
		Keypad		= 0x07,
		AxisController = 0x08,
		TabletPC	= 0x09
	};

	//=========================================================
	// Name: Mouse Buttons
	// Description: Enumeration of all the mouse buttons that
	// can be obtained
	// Creator: Chris Millin
	//=========================================================
	enum class MLMouseButtons
	{
		Left	= 0x00,
		Right	= 0x01,
		Middle	= 0x02,
		One		= 0x03,
		Two		= 0x04,
	};

	//=========================================================
	// Name: Keys
	// Description: Enumeration of all the keyboard keys that
	// can be obtained. It is all based of the Microsoft Virtual
	// keys at the moment.
	// Creator: Chris Millin
	//=========================================================
	enum MLKeys
	{
		Back	= VK_BACK,
		Tab		= VK_TAB,
		Clear	= VK_CLEAR,
		Enter	= VK_RETURN,
		Shift	= VK_SHIFT,
		Ctrl	= VK_CONTROL,
		Alt		= VK_MENU,
		Pause	= VK_PAUSE,
		Caps	= VK_CAPITAL,
		//Kanji systems
		//=================
		Kana		= VK_KANA,
		Hangeul		= VK_HANGEUL,
		Hangul		= VK_HANGUL,
		Final		= VK_FINAL,
		Hanja		= VK_HANJA,
		Kanji		= VK_KANJI,
		Convert		= VK_CONVERT,
		NonConvert	= VK_NONCONVERT,
		Accept		= VK_ACCEPT,
		ModeChange	= VK_MODECHANGE,
		//=================
		Esc			= VK_ESCAPE,
		Space		= VK_SPACE,
		PageUp		= VK_PRIOR,
		PageDown	= VK_NEXT,
		End			= VK_END,
		Home		= VK_HOME,
		Left		= VK_LEFT,
		Up			= VK_UP,
		Right		= VK_RIGHT,
		Down		= VK_DOWN,
		Select		= VK_SELECT,
		Print		= VK_PRINT,
		Execute		= VK_EXECUTE,
		PrintScreen = VK_SNAPSHOT,
		Insert		= VK_INSERT,
		Delete		= VK_DELETE,
		Help		= VK_HELP,
		//Alpha Keys
		//=================
		A = 0x41,
		B = 0x42,
		C = 0x43,
		D = 0x44,
		E = 0x45,
		F = 0x46,
		G = 0x47,
		H = 0x48,
		I = 0x49,
		J = 0x4A,
		K = 0x4B,
		L = 0x4C,
		M = 0x4D,
		N = 0x4E,
		O = 0x4F,
		P = 0x50,
		Q = 0x51,
		R = 0x52,
		S = 0x53,
		T = 0x54,
		U = 0x55,
		V = 0x56,
		W = 0x57,
		X = 0x58,
		Y = 0x59,
		Z = 0x5A,
		//=================
		//Numerical Keys
		//=================
		Zero	= 0x30,
		One		= 0x31,
		Two		= 0x32,
		Three	= 0x33,
		Four	= 0x34,
		Five	= 0x35,
		Six		= 0x36,
		Seven	= 0x37,
		Eight	= 0x38,
		Nine	= 0x39,
		//================
		LMeta	= VK_LWIN,
		RMeta	= VK_RWIN,
		Apps	= VK_APPS,
		SleepKey = VK_SLEEP,
		//Num Pad Keys
		//===============
		NPZero		= VK_NUMPAD0,
		NPOne		= VK_NUMPAD1,
		NPTwo		= VK_NUMPAD2,
		NPThree		= VK_NUMPAD3,
		NPFour		= VK_NUMPAD4,
		NPFive		= VK_NUMPAD5,
		NPSix		= VK_NUMPAD6,
		NPSeven		= VK_NUMPAD7,
		NPEight		= VK_NUMPAD8,
		NPNine		= VK_NUMPAD9,
		NPMul		= VK_MULTIPLY,
		NPAdd		= VK_ADD,
		NPSeperator = VK_SEPARATOR,
		NPSub		= VK_SUBTRACT,
		NPDecimal	= VK_DECIMAL,
		NPDivide	= VK_DIVIDE,
		NPNumLock	= VK_NUMLOCK,
		NPEqual		= VK_OEM_NEC_EQUAL,
		//===============
		F1 = VK_F1,
		F2 = VK_F2,
		F3 = VK_F3,
		F4 = VK_F4,
		F5 = VK_F5,
		F6 = VK_F6,
		F7 = VK_F7,
		F8 = VK_F8,
		F9 = VK_F9,
		F10 = VK_F10,
		F11 = VK_F11,
		F12 = VK_F12,
		F13 = VK_F13,
		F14 = VK_F14,
		F15 = VK_F15,
		F16 = VK_F16,
		F17 = VK_F17,
		F18 = VK_F18,
		F19 = VK_F19,
		F20 = VK_F20,
		F21 = VK_F21,
		F22 = VK_F22,
		F23 = VK_F23,
		F24 = VK_F24,
		ScrollLock	= VK_SCROLL,
		LShift		= VK_LSHIFT,
		RShift		= VK_RSHIFT,
		LCtrl		= VK_LCONTROL,
		RCtrl		= VK_RCONTROL,
		LAlt		= VK_LMENU,
		RAlt		= VK_RMENU,
		//Media Keys
		//===============
		BrowserBack		= VK_BROWSER_BACK,
		BrowserFoward	= VK_BROWSER_FORWARD,
		BrowserRefresh	= VK_BROWSER_REFRESH,
		BrowserStop		= VK_BROWSER_STOP,
		BrowserSearch	= VK_BROWSER_SEARCH,
		BrowserFav		= VK_BROWSER_FAVORITES,
		BrowserHome		= VK_BROWSER_HOME,
		VolumeUp		= VK_VOLUME_UP,
		VolumeDown		= VK_VOLUME_DOWN,
		VolumeMute		= VK_VOLUME_MUTE,
		NextTrack		= VK_MEDIA_NEXT_TRACK,
		PrevTrack		= VK_MEDIA_PREV_TRACK,
		StopTrack		= VK_MEDIA_STOP,
		PlayTrack		= VK_MEDIA_PLAY_PAUSE,
		Mail	= VK_LAUNCH_MAIL,
		Media	= VK_LAUNCH_MEDIA_SELECT,
		App1	= VK_LAUNCH_APP1,
		App2	= VK_LAUNCH_APP2,
		//===============
		OEM_1	= VK_OEM_1,
		Plus	= VK_OEM_PLUS,
		Comma	= VK_OEM_COMMA,
		Minus	= VK_OEM_MINUS,
		Period	= VK_OEM_PERIOD,
		OEM_2	= VK_OEM_2,
		OEM_3	= VK_OEM_3,
		OEM_4	= VK_OEM_4,
		OEM_5	= VK_OEM_5,
		OEM_6	= VK_OEM_6,
		OEM_7	= VK_OEM_7,
		OEM_8	= VK_OEM_8,
		AX		= VK_OEM_AX,
		OEM_102 = VK_OEM_102,
	};


	//this is the look up table for key polling
	//this is for non caps/shift keys
	extern ML_SCHAR keyboardLut[ML_KEY_COUNT];
	extern ML_SCHAR keyboardCAPLut[ML_KEY_COUNT];
	extern ML_SCHAR keyboardShiftLut[ML_KEY_COUNT];


	//=========================================================
	// Name: Create Keyboard Look Up Table
	// Description: Creates a basic table of all the characters
	// on the keyboard. This function creates the basic UK
	// keyboard layout.
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL inline void CreateKeyboardLUT()
	{

		//int the future add a proper key mapper which will load key maps from a file
		keyboardLut[MLKeys::Enter] = L'\n';
		keyboardLut[MLKeys::Space] = L' ';
		keyboardLut[MLKeys::A] = L'a';
		keyboardLut[MLKeys::B] = L'b';
		keyboardLut[MLKeys::C] = L'c';
		keyboardLut[MLKeys::D] = L'd';
		keyboardLut[MLKeys::E] = L'e';
		keyboardLut[MLKeys::F] = L'f';
		keyboardLut[MLKeys::G] = L'g';
		keyboardLut[MLKeys::H] = L'h';
		keyboardLut[MLKeys::I] = L'i';
		keyboardLut[MLKeys::J] = L'j';
		keyboardLut[MLKeys::K] = L'k';
		keyboardLut[MLKeys::L] = L'l';
		keyboardLut[MLKeys::M] = L'm';
		keyboardLut[MLKeys::N] = L'n';
		keyboardLut[MLKeys::O] = L'o';
		keyboardLut[MLKeys::P] = L'p';
		keyboardLut[MLKeys::Q] = L'q';
		keyboardLut[MLKeys::R] = L'r';
		keyboardLut[MLKeys::S] = L's';
		keyboardLut[MLKeys::T] = L't';
		keyboardLut[MLKeys::U] = L'u';
		keyboardLut[MLKeys::V] = L'v';
		keyboardLut[MLKeys::W] = L'w';
		keyboardLut[MLKeys::X] = L'x';
		keyboardLut[MLKeys::Y] = L'y';
		keyboardLut[MLKeys::Z] = L'z';
		keyboardLut[MLKeys::Zero] = L'0';
		keyboardLut[MLKeys::One] = L'1';
		keyboardLut[MLKeys::Two] = L'2';
		keyboardLut[MLKeys::Three] = L'3';
		keyboardLut[MLKeys::Four] = L'4';
		keyboardLut[MLKeys::Five] = L'5';
		keyboardLut[MLKeys::Six] = L'6';
		keyboardLut[MLKeys::Seven] = L'7';
		keyboardLut[MLKeys::Eight] = L'8';
		keyboardLut[MLKeys::Nine] = L'9';
		keyboardLut[MLKeys::NPZero] = L'0';
		keyboardLut[MLKeys::NPOne] = L'1';
		keyboardLut[MLKeys::NPTwo] = L'2';
		keyboardLut[MLKeys::NPThree] = L'3';
		keyboardLut[MLKeys::NPFour] = L'4';
		keyboardLut[MLKeys::NPFive] = L'5';
		keyboardLut[MLKeys::NPSix] = L'6';
		keyboardLut[MLKeys::NPSeven] = L'7';
		keyboardLut[MLKeys::NPEight] = L'8';
		keyboardLut[MLKeys::NPNine] = L'9';
		keyboardLut[MLKeys::NPAdd] = L'+';
		keyboardLut[MLKeys::NPSub] = L'-';
		keyboardLut[MLKeys::NPMul] = L'*';
		keyboardLut[MLKeys::NPDivide] = L'/';
		keyboardLut[MLKeys::NPDecimal] = L'.';
		keyboardLut[MLKeys::OEM_1] = L';';
		keyboardLut[MLKeys::Plus] = L'=';
		keyboardLut[MLKeys::Comma] = L',';
		keyboardLut[MLKeys::Minus] = L'-';
		keyboardLut[MLKeys::Period] = L'.';
		keyboardLut[MLKeys::OEM_2] = L'/';
		keyboardLut[MLKeys::OEM_3] = L'`';
		keyboardLut[MLKeys::OEM_4] = L'[';
		keyboardLut[MLKeys::OEM_5] = L'#';
		keyboardLut[MLKeys::OEM_6] = L']';
		keyboardLut[MLKeys::OEM_7] = L'\'';
		keyboardLut[MLKeys::OEM_102] = L'\\';

		keyboardShiftLut[MLKeys::Enter] = L'\n';
		keyboardShiftLut[MLKeys::Space] = L' ';
		keyboardShiftLut[MLKeys::A] = L'A';
		keyboardShiftLut[MLKeys::B] = L'B';
		keyboardShiftLut[MLKeys::C] = L'C';
		keyboardShiftLut[MLKeys::D] = L'D';
		keyboardShiftLut[MLKeys::E] = L'E';
		keyboardShiftLut[MLKeys::F] = L'F';
		keyboardShiftLut[MLKeys::G] = L'G';
		keyboardShiftLut[MLKeys::H] = L'H';
		keyboardShiftLut[MLKeys::I] = L'I';
		keyboardShiftLut[MLKeys::J] = L'J';
		keyboardShiftLut[MLKeys::K] = L'K';
		keyboardShiftLut[MLKeys::L] = L'L';
		keyboardShiftLut[MLKeys::M] = L'M';
		keyboardShiftLut[MLKeys::N] = L'N';
		keyboardShiftLut[MLKeys::O] = L'O';
		keyboardShiftLut[MLKeys::P] = L'P';
		keyboardShiftLut[MLKeys::Q] = L'Q';
		keyboardShiftLut[MLKeys::R] = L'R';
		keyboardShiftLut[MLKeys::S] = L'S';
		keyboardShiftLut[MLKeys::T] = L'T';
		keyboardShiftLut[MLKeys::U] = L'U';
		keyboardShiftLut[MLKeys::V] = L'V';
		keyboardShiftLut[MLKeys::W] = L'W';
		keyboardShiftLut[MLKeys::X] = L'X';
		keyboardShiftLut[MLKeys::Y] = L'Y';
		keyboardShiftLut[MLKeys::Z] = L'Z';
		keyboardShiftLut[MLKeys::Zero] = L')';
		keyboardShiftLut[MLKeys::One] = L'!';
		keyboardShiftLut[MLKeys::Two] = L'@';
		keyboardShiftLut[MLKeys::Three] = L'#';
		keyboardShiftLut[MLKeys::Four] = L'�';
		keyboardShiftLut[MLKeys::Five] = L'%';
		keyboardShiftLut[MLKeys::Six] = L'^';
		keyboardShiftLut[MLKeys::Seven] = L'&';
		keyboardShiftLut[MLKeys::Eight] = L'*';
		keyboardShiftLut[MLKeys::Nine] = L'(';
		keyboardShiftLut[MLKeys::NPZero] = L'0';
		keyboardShiftLut[MLKeys::NPOne] = L'1';
		keyboardShiftLut[MLKeys::NPTwo] = L'2';
		keyboardShiftLut[MLKeys::NPThree] = L'3';
		keyboardShiftLut[MLKeys::NPFour] = L'4';
		keyboardShiftLut[MLKeys::NPFive] = L'5';
		keyboardShiftLut[MLKeys::NPSix] = L'6';
		keyboardShiftLut[MLKeys::NPSeven] = L'7';
		keyboardShiftLut[MLKeys::NPEight] = L'8';
		keyboardShiftLut[MLKeys::NPNine] = L'9';
		keyboardShiftLut[MLKeys::NPAdd] = L'+';
		keyboardShiftLut[MLKeys::NPSub] = L'-';
		keyboardShiftLut[MLKeys::NPMul] = L'*';
		keyboardShiftLut[MLKeys::NPDivide] = L'/';
		keyboardShiftLut[MLKeys::NPDecimal] = L'.';
		keyboardShiftLut[MLKeys::OEM_1] = L':';
		keyboardShiftLut[MLKeys::Plus] = L'+';
		keyboardShiftLut[MLKeys::Comma] = L'<';
		keyboardShiftLut[MLKeys::Minus] = L'_';
		keyboardShiftLut[MLKeys::Period] = L'>';
		keyboardShiftLut[MLKeys::OEM_2] = L'?';
		keyboardShiftLut[MLKeys::OEM_3] = L'~';
		keyboardShiftLut[MLKeys::OEM_4] = L'{';
		keyboardShiftLut[MLKeys::OEM_5] = L'#';
		keyboardShiftLut[MLKeys::OEM_6] = L'}';
		keyboardShiftLut[MLKeys::OEM_7] = L'\"';
		keyboardShiftLut[MLKeys::OEM_102] = L'|';

		keyboardCAPLut[MLKeys::Enter] = L'\n';
		keyboardCAPLut[MLKeys::Space] = L' ';
		keyboardCAPLut[MLKeys::A] = L'A';
		keyboardCAPLut[MLKeys::B] = L'B';
		keyboardCAPLut[MLKeys::C] = L'C';
		keyboardCAPLut[MLKeys::D] = L'D';
		keyboardCAPLut[MLKeys::E] = L'E';
		keyboardCAPLut[MLKeys::F] = L'F';
		keyboardCAPLut[MLKeys::G] = L'G';
		keyboardCAPLut[MLKeys::H] = L'H';
		keyboardCAPLut[MLKeys::I] = L'I';
		keyboardCAPLut[MLKeys::J] = L'J';
		keyboardCAPLut[MLKeys::K] = L'K';
		keyboardCAPLut[MLKeys::L] = L'L';
		keyboardCAPLut[MLKeys::M] = L'M';
		keyboardCAPLut[MLKeys::N] = L'N';
		keyboardCAPLut[MLKeys::O] = L'O';
		keyboardCAPLut[MLKeys::P] = L'P';
		keyboardCAPLut[MLKeys::Q] = L'Q';
		keyboardCAPLut[MLKeys::R] = L'R';
		keyboardCAPLut[MLKeys::S] = L'S';
		keyboardCAPLut[MLKeys::T] = L'T';
		keyboardCAPLut[MLKeys::U] = L'U';
		keyboardCAPLut[MLKeys::V] = L'V';
		keyboardCAPLut[MLKeys::W] = L'W';
		keyboardCAPLut[MLKeys::X] = L'X';
		keyboardCAPLut[MLKeys::Y] = L'Y';
		keyboardCAPLut[MLKeys::Z] = L'Z';
		keyboardCAPLut[MLKeys::Zero] = L'0';
		keyboardCAPLut[MLKeys::One] = L'1';
		keyboardCAPLut[MLKeys::Two] = L'2';
		keyboardCAPLut[MLKeys::Three] = L'3';
		keyboardCAPLut[MLKeys::Four] = L'4';
		keyboardCAPLut[MLKeys::Five] = L'5';
		keyboardCAPLut[MLKeys::Six] = L'6';
		keyboardCAPLut[MLKeys::Seven] = L'7';
		keyboardCAPLut[MLKeys::Eight] = L'8';
		keyboardCAPLut[MLKeys::Nine] = L'9';
		keyboardCAPLut[MLKeys::NPZero] = L'0';
		keyboardCAPLut[MLKeys::NPOne] = L'1';
		keyboardCAPLut[MLKeys::NPTwo] = L'2';
		keyboardCAPLut[MLKeys::NPThree] = L'3';
		keyboardCAPLut[MLKeys::NPFour] = L'4';
		keyboardCAPLut[MLKeys::NPFive] = L'5';
		keyboardCAPLut[MLKeys::NPSix] = L'6';
		keyboardCAPLut[MLKeys::NPSeven] = L'7';
		keyboardCAPLut[MLKeys::NPEight] = L'8';
		keyboardCAPLut[MLKeys::NPNine] = L'9';
		keyboardCAPLut[MLKeys::NPAdd] = L'+';
		keyboardCAPLut[MLKeys::NPSub] = L'-';
		keyboardCAPLut[MLKeys::NPMul] = L'*';
		keyboardCAPLut[MLKeys::NPDivide] = L'/';
		keyboardCAPLut[MLKeys::NPDecimal] = L'.';
		keyboardCAPLut[MLKeys::OEM_1] = L';';
		keyboardCAPLut[MLKeys::Plus] = L'=';
		keyboardCAPLut[MLKeys::Comma] = L',';
		keyboardCAPLut[MLKeys::Minus] = L'-';
		keyboardCAPLut[MLKeys::Period] = L'.';
		keyboardCAPLut[MLKeys::OEM_2] = L'/';
		keyboardCAPLut[MLKeys::OEM_3] = L'`';
		keyboardCAPLut[MLKeys::OEM_4] = L'[';
		keyboardCAPLut[MLKeys::OEM_5] = L'#';
		keyboardCAPLut[MLKeys::OEM_6] = L']';
		keyboardCAPLut[MLKeys::OEM_7] = L'\'';
		keyboardCAPLut[MLKeys::OEM_102] = L'\\';
	}

	//=========================================================
	// Name: Load Keyboard Look Up Table
	// Description: This will load a defined key layout from
	// a raw text file.
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL inline ML_BOOL LoadkeyboardLut(const ML_CHAR* location)
	{
		//NEED TO IMPLEMENT
		return ML_TRUE;
	}
}
#endif