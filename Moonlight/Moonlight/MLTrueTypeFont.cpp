#include "MLTrueTypeFont.h"
#include "MLGraphicTypes.h"

namespace MLRender
{

	MLTrueTypeFont::MLTrueTypeFont(void)
	{
		m_fontLoaded = ML_FALSE;
		m_fontSize = DEFAULT_FONT_SIZE;
		m_paddingX = 1;
		m_paddingY = 1;

		m_textureWidth	= DEFAULT_FONT_TEXTURE_WIDTH;
		m_textureHeight = DEFAULT_FONT_TEXTURE_HEIGHT;

		m_type = MLFontType::TrueType;
	}

	MLTrueTypeFont::~MLTrueTypeFont(void)
	{
		Release();
	}

	ML_BOOL MLTrueTypeFont::Release()
	{
		MLFont::Release();

		FT_Done_Face(m_fontFace);

		return ML_TRUE;
	}

	ML_BOOL MLTrueTypeFont::LoadFont(const MLKernel::MLVirtualFile* File, const ML_BOOL loadGlyphs /* = ML_FALSE */,
		const ML_UINT Size /* = DEFAULT_FONT_SIZE */, const ML_STRING& CharacterSet /* = ML_UNICODE("") */)
	{
		if(!File)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font file path is invalid exiting loading font\n"));
			return ML_FALSE;
		}

		if(File->IsOpen())
			File->CloseFile();

		if(File->FullFilePath().empty())
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font file path is invalid exiting loading font\n"));
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Progress, ML_STR("Loading Font Face from: %s \n"), File->FullFilePath().c_str());

		FT_Error error = FT_New_Face(MLFontCore::GetFontLibrary(), MLtoAscii(File->FullFilePath()).c_str(), 0, &m_fontFace);
		if(error == FT_Err_Unknown_File_Format)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Unsupported font file\n"));
			return ML_FALSE;
		}
		else if(error)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Unknown Error Loading Font file (possible cause could be unicode characters in file path)\n"));
			return ML_FALSE;
		}
		else if(MLFontCore::GetFontLibrary() == NULL)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font Core not Initalised\n"));
			return ML_FALSE;
		}
		else
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Progress, ML_STR("Font Face Loaded Successfully\n"));

		m_kerningEnabled = FT_HAS_KERNING(m_fontFace) ? ML_TRUE : ML_FALSE;

		m_fontLoaded = ML_TRUE;

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Font Info:\n"));
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Type TrueType\n"));
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Glyphs %i\n"), m_fontFace->num_glyphs);
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Name  %s\n"), MLtoUnicode(m_fontFace->style_name).c_str());
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Family %s\n"), MLtoUnicode(m_fontFace->family_name).c_str());
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Kerning Support %S\n"), m_kerningEnabled ? "True" : "False");

		m_fontSize = Size;

		if(loadGlyphs)
		{
			if(!CharacterSet.empty())
				LoadGlyphs(CharacterSet);
			else
				LoadDefaultGlyphs();
		}

		return ML_TRUE;
	}

	ML_BOOL MLTrueTypeFont::LoadDefaultGlyphs()
	{
		if(m_fontLoaded)
		{
			FT_Error error = FT_Set_Pixel_Sizes(m_fontFace, 0, m_fontSize);
			if (error)
			{
				ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Could not set Font width and height\n"));
				return ML_FALSE;
			}

			MLTextureAtlas* atlas = new MLTextureAtlas();
			atlas->CreateAtlas(m_textureWidth, m_textureHeight, MLPixelFormat::LuminanceAlpha16, ML_FALSE);

			AtlasTexture atlasInfo;

			for (ML_INT i = DEFAULT_FONT_GLYPH_START; i < DEFAULT_FONT_GLYPH_END; i++)
			{
				FT_Error error = FT_Load_Char(m_fontFace, i, FT_LOAD_RENDER);
				if (error)
					ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Could not load selected Glyph %s\n"), (wchar_t)i);
				FT_Bitmap& slot = m_fontFace->glyph->bitmap;
				atlasInfo = AddBitmap(slot, atlas);

				FT_GlyphSlot&  glythMetric = m_fontFace->glyph;
				AddMetrics(i, glythMetric, atlasInfo);		
			}

			LoadEmptyCharacter(atlas);

			atlas->BuildTexture();
			m_fontTextures.push_back(atlas->GetTexture());
			delete atlas;
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));
		return ML_FALSE;
	}

	ML_BOOL MLTrueTypeFont::LoadGlyphs(const ML_STRING& characters)
	{
		if(m_fontLoaded)
		{
			FT_Error error = FT_Set_Pixel_Sizes(m_fontFace, 0, m_fontSize);
			if (error)
			{
				ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Could not set Font width and height\n"));
				return ML_FALSE;
			}

			MLTextureAtlas* atlas = new MLTextureAtlas();
			atlas->CreateAtlas(m_textureWidth, m_textureHeight, MLPixelFormat::LuminanceAlpha16, ML_FALSE);

			AtlasTexture atlasInfo;

			for (ML_UINT i = 0; i < characters.length(); i++)
			{
				ML_UINT id = characters[i];
				FT_Error error = FT_Load_Char(m_fontFace, id, FT_LOAD_RENDER);
				if (error)
					ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Could not load selected Glyph %s\n"), (wchar_t)characters[i]);
				FT_Bitmap& slot = m_fontFace->glyph->bitmap;
				atlasInfo = AddBitmap(slot, atlas);

				FT_GlyphSlot&  glythMetric = m_fontFace->glyph;
				AddMetrics(characters[i], glythMetric, atlasInfo);
			}

			if(characters.find((wchar_t)0) == -1)
				LoadEmptyCharacter(atlas);

			atlas->BuildTexture();
			m_fontTextures.push_back(atlas->GetTexture());
			delete atlas;
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));
		return ML_FALSE;
	}

	//ML_BOOL MLTrueTypeFont::ConvertToSignedDistance()
	//{
	//	return ML_FALSE;
	//}

	MLVector2f MLTrueTypeFont::GetKerning(const ML_SCHAR& lastChar, const ML_SCHAR& thisChar)
	{
		if(m_kerningEnabled)
		{
			FT_Vector delta; 
			ML_UINT last_index = FT_Get_Char_Index( m_fontFace, lastChar );
			ML_UINT this_index = FT_Get_Char_Index( m_fontFace, thisChar );
			FT_Get_Kerning(m_fontFace, last_index, this_index, FT_KERNING_DEFAULT, &delta );
			return MLVector2f((ML_FLOAT)(delta.x/64), (ML_FLOAT)(delta.y/64));
		}
		else
		{
			return MLVector2f(0.0f);
		}
	}

	MLSprite MLTrueTypeFont::GetGlyphSprite(const ML_SCHAR& character)
	{
		MLSprite GlyphSprite;

		if(m_fontLoaded)
		{
			MLGlyph Glyph = GetGlyph(character);

			ML_FLOAT glythX = Glyph.X + Glyph.paddingX;
			ML_FLOAT glythY = Glyph.Y + Glyph.paddingY;
			GlyphSprite.Position = MLVector2f(0.0f,0.0f);
			GlyphSprite.Texture = GetFontTexture(Glyph.textureID);
			GlyphSprite.Source = MLVector4f(glythX, glythY, (ML_FLOAT)(Glyph.Width + glythX), (ML_FLOAT)(Glyph.Height + glythY));
			GlyphSprite.Size = MLVector2f(Glyph.Width, Glyph.Height);
			GlyphSprite.Colour =  1.0f;
			GlyphSprite.SetOriginMiddleOfSize();
		}
		else
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));

		return GlyphSprite;
	}

	MLSprite MLTrueTypeFont::GetGlyphSprite(const MLGlyph& character)
	{
		MLSprite GlyphSprite;

		if(m_fontLoaded)
		{
			ML_FLOAT glythX = character.X;
			ML_FLOAT glythY = character.Y;

			GlyphSprite.Position = MLVector2f(0.0f,0.0f);
			GlyphSprite.Texture = GetFontTexture(character.textureID);
			GlyphSprite.Source = MLVector4f(glythX, glythY, (ML_FLOAT)(character.Width + character.X), (ML_FLOAT)(character.Height + character.Y));
			GlyphSprite.Size = MLVector2f(character.Width, character.Height);
			GlyphSprite.Colour =  1.0f;
			GlyphSprite.SetOriginMiddleOfSize();
		}
		else
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));
	
		return GlyphSprite;
	}

	ML_BOOL MLTrueTypeFont::LoadEmptyCharacter(MLTextureAtlas* atlas)
	{
		FT_Error error = FT_Load_Char(m_fontFace, 26, FT_LOAD_RENDER);
		if (error)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Could not load unknow Glyph %s\n"), (wchar_t)0);
			return ML_FALSE;
		}

		FT_Bitmap& slot = m_fontFace->glyph->bitmap;
		AtlasTexture atlasInfo = AddBitmap(slot, atlas);

		FT_GlyphSlot&  glythMetric = m_fontFace->glyph;
		AddMetrics(1, glythMetric, atlasInfo);	

		return ML_TRUE;
	}

	AtlasTexture MLTrueTypeFont::AddBitmap(const FT_Bitmap& slot, MLTextureAtlas* atlas)
	{
		AtlasTexture atlasInfo;

		MLPixelLumiAlpha16* data = new MLPixelLumiAlpha16[(slot.width+(m_paddingX*2)) * (slot.rows+(m_paddingY*2))];

		for (ML_UINT y = m_paddingY; y < slot.rows + m_paddingY; y++)
		{
			for (ML_UINT x = m_paddingX; x < slot.width + m_paddingX; x++)
			{
				data[(y * (slot.width + (m_paddingY*2))) + x].L = 255;
				data[(y * (slot.width + (m_paddingY*2))) + x].A = slot.buffer[((y-m_paddingY) * slot.width) + (x-m_paddingX)];
			}
		}
		ML_BOOL result = ML_FALSE;
		if(slot.width && slot.rows)
		{
			result = atlas->AddTexture(data, slot.width+(m_paddingX*2), slot.rows+(m_paddingY*2), &atlasInfo);
			if(!result)
			{
				atlas->BuildTexture();
				m_fontTextures.push_back(atlas->GetTexture());
				delete atlas;
				atlas = new MLTextureAtlas();
				atlas->CreateAtlas(m_textureWidth, m_textureHeight, MLPixelFormat::LuminanceAlpha16, ML_FALSE);
				atlas->AddTexture(data, slot.width+(m_paddingX*2), slot.rows+(m_paddingY*2), &atlasInfo);
			}
		}
		else
		{
			atlasInfo.rect = MLVector4f(0.0f,0.0f,0.0f,0.0f);
		}
		return atlasInfo;
	}

	void MLTrueTypeFont::AddMetrics(const ML_INT characterIndex, const FT_GlyphSlot& glythMetric, AtlasTexture& atlasInfo)
	{
		MLGlyph characterGlyth;
		characterGlyth.character = characterIndex;
		characterGlyth.X = atlasInfo.rect.x + m_paddingX;
		characterGlyth.Y = atlasInfo.rect.y + m_paddingY;
			
		if(atlasInfo.Rotated)
		{
			characterGlyth.Width	= (ML_FLOAT)glythMetric->bitmap.width;
			characterGlyth.Height	= (ML_FLOAT)glythMetric->bitmap.rows;
			characterGlyth.Rotated	= ML_TRUE;
		}
		else
		{
			characterGlyth.Width	= (ML_FLOAT)glythMetric->bitmap.width;
			characterGlyth.Height	= (ML_FLOAT)glythMetric->bitmap.rows;
			characterGlyth.Rotated	= ML_FALSE;
		}

		characterGlyth.paddingX = (ML_FLOAT)m_paddingX;
		characterGlyth.paddingY = (ML_FLOAT)m_paddingY;
		characterGlyth.paddingZ = (ML_FLOAT)m_paddingX;
		characterGlyth.paddingW = (ML_FLOAT)m_paddingY;
		characterGlyth.bearingX = -(ML_FLOAT)(glythMetric->bitmap_left);
		characterGlyth.bearingY = (ML_FLOAT)(glythMetric->bitmap_top) - (m_fontFace->size->metrics.ascender/64);
		characterGlyth.advance	= (ML_FLOAT)(glythMetric->advance.x/64);
		characterGlyth.textureID = m_fontTextures.size();

		if(characterIndex != 1)
			m_glyphSet[characterIndex] = characterGlyth;
		else
			m_unknowGlyph = characterGlyth;

		//wstring val;
		//val.insert(val.begin(), (wchar_t)characterGlyth.character);
		//ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Character: %s bearing x: %f bearing y: %f advance: %f W: %f H: %f\n"), val.c_str(), 
		//	characterGlyth.bearingX, characterGlyth.bearingY, characterGlyth.advance, characterGlyth.Width, characterGlyth.Height);
	}

}