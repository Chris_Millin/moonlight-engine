#ifndef MLUUID_H
#define MLUUID_H

#include "MLCore.h"
#include "MLConsole.h"

#include <Rpcdce.h>

namespace MLKernel
{

	//=========================================================
	// Name: UUID Compare
	// Description: Enumeration for the values that can come
	// back from UUID Compare function
	// Creator: Chris Millin
	//=========================================================
	enum MLUUIDComp
	{
		GreaterThan	= -1,
		Equal		= 0,
		LessThan	= 1,
		Invalid		= 2,
	};

	//=========================================================
	// Name: UUID
	// Description: Universal Unique Identifier object. Also
	// provides functions to compare, check equality and convert
	// to a string.
	// Creator: Chris Millin
	//=========================================================
	class MLUUID
	{
	public:

		//=========================================================
		// Name: Default Constructor
		// Description: Creates a Nil UUID (will return as valid).
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLUUID(void);
		MLCoreDLL MLUUID(const ML_STRING& key) { ML_TRACE(ML_STR("UUID"), MLConsoleStatus::Exception, ML_STR("Function is not implemented")); }
		MLCoreDLL virtual ~MLUUID(void);

		//=========================================================
		// Name: Release
		// Description: Releases the UUID object.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Generate
		// Description: Will generate a UUID and return it as
		// a string. This will also generate a UUID that is not
		// linked to the PC MAC address or IP
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING Generate();

		//=========================================================
		// Name: Generate Sequential
		// Description: Will generate a UUID and return it as a
		// string. This will also generate a UUID that could be
		// linked to the PC MAC address or IP
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GenerateSequential();
		
		//=========================================================
		// Name: Compare
		// Description: Will compare this UUID to another UUID and
		// return if it is less than or greater than the other UUID.
		// Invalid is returned if the compare fails
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLUUIDComp Compare(const MLUUID& id) const;

		//=========================================================
		// Name: Equal
		// Description: Will check to see if one UUID equals another.
		// This in theory should never happen but this will check if
		// it does.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL Equal(const MLUUID& id) const;
		
		//=========================================================
		// Name: Is Valid
		// Description: Returns if the UUID is valid (should only 
		// happen if the creation of the UUID fails or the UUID is
		// released).
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsValid() const;

		//=========================================================
		// Name: To String
		// Description: Returns the UUID as a string.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING ToString() const;

		//=========================================================
		// Name: Get UUID
		// Description: Returns the windows UUID object.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual UUID* GetUUID() const { return m_uuid; }

	private:

		//windows UUID object
		UUID* m_uuid;
	};
}

#endif