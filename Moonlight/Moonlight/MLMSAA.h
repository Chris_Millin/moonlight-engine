#ifndef MLMSAA_H
#define MLMSAA_H

#include "MLIncludes.h"
#include "MLPostProcess.h"
#include "MLGraphics.h"

#define ML_LOG_MSAA ML_STR("MSAA")

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLMSAA : MLRenderTarget
	{
	public:
		MLCoreDLL MLMSAA(ML_UINT Amount);
		MLCoreDLL virtual ~MLMSAA(void);
	
		//=========================================================
		// Name: Release
		// Description: Releases any dynamic objects created
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Initalise
		// Description: Will initalise all the frame buffer objects
		// ready for rendering the scene to a multisampled texture
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Initalise(const MLFBOTypes enableFBO = MLFBOTypes::ColourBuffer);

		//=========================================================
		// Name: Draw
		// Description: This does not have to be called but can be
		// used to draw the MSAA result. If post processing is
		// desired call bind on the post process before calling
		// this and then unbind after this call and then draw the
		// post process. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Draw(MLGraphics* batch);

	private:

		MLFrameBuffer* m_resolveFBO;
		MLTexture* m_msaaTexture;

		ML_UINT m_msaaAmount;
	};

}

#endif