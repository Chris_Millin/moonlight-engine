#ifndef MLDATETIME_H
#define MLDATETIME_H

#include "MLCore.h"

#include <MLMath/MLMath.h>

#define ML_DATE_TIME_STRING_BUFFER 50

const static ML_STRING MLWeekDays[7] =
{
	ML_STR("Monday"), ML_STR("Tuesday"), ML_STR("Wednesday"), ML_STR("Thursday")
	ML_STR("Friday"), ML_STR("Staurday"), ML_STR("Sunday")
};

//=========================================================
// Name: Date Time
// Description: Structure for storing the date and time
// Creator: Chris Millin
//=========================================================
struct MLDateTime
{
	ML_INT		Year;
	ML_INT		Month;
	ML_INT		Day;
	ML_INT		WeekDay;
	ML_INT		Hour;
	ML_INT		Minutes;
	ML_INT		Seconds;
	ML_INT		Milliseconds;

	MLDateTime()
	{
		Year			= 0;
		Month			= 0;
		Day				= 0;
		WeekDay			= 0;
		Hour			= 0;
		Minutes			= 0;
		Milliseconds	= 0;
	}

	MLDateTime static Now();

	MLDateTime& operator=(FILETIME& inTime);

	ML_STRING ToString() const;
	ML_STRING ToFileString() const;
};

inline MLDateTime MLDateTime::Now()
{

	FILETIME time;
	SYSTEMTIME st;

	MLDateTime now;

	GetSystemTimeAsFileTime(&time);
	FileTimeToLocalFileTime( &time, &time );
	FileTimeToSystemTime(&time, &st);
	now.Year			= st.wYear;
	now.Month			= st.wMonth;
	now.Day				= st.wDay;
	now.WeekDay			= st.wDayOfWeek;
	now.Hour			= st.wHour;
	now.Minutes			= st.wMinute;
	now.Seconds			= st.wSecond;
	now.Milliseconds	= st.wMilliseconds;

	return now;
}

inline MLDateTime& MLDateTime::operator =(FILETIME& inTime)
{
	SYSTEMTIME st;
	FileTimeToLocalFileTime( &inTime, &inTime );
	FileTimeToSystemTime(&inTime, &st);
	this->Year			= st.wYear;
	this->Month			= st.wMonth;
	this->Day			= st.wDay;
	this->WeekDay		= st.wDayOfWeek;
	this->Hour			= st.wHour;
	this->Minutes		= st.wMinute;
	this->Seconds		= st.wSecond;
	this->Milliseconds	= st.wMilliseconds;

	return *this;
}

inline ML_STRING MLDateTime::ToString() const
{
	ML_SCHAR* buffer = new wchar_t[ML_DATE_TIME_STRING_BUFFER];
	swprintf_s(buffer, ML_DATE_TIME_STRING_BUFFER, ML_STR("Date: %s %d/%d/%d Time: %d:%d:%d"), MLWeekDays[this->WeekDay].c_str(), this->Day, this->Month, this->Year,
		this->Hour, this->Minutes, this->Seconds);
	ML_STRING out = buffer;
	delete buffer;
	return out;
}


inline ML_STRING MLDateTime::ToFileString() const
{
	ML_SCHAR* buffer = new wchar_t[ML_DATE_TIME_STRING_BUFFER];
	swprintf_s(buffer, ML_DATE_TIME_STRING_BUFFER, ML_STR("%d-%d-%d_%d-%d-%d"), this->Day, this->Month, this->Year,
		this->Hour, this->Minutes, this->Seconds);
	ML_STRING out = buffer;
	delete buffer;
	return out;
}

#endif