#ifndef MLVERTEXBUFFER_H
#define MLVERTEXBUFFER_H

#include "MLGLContext.h"
#include "MLIncludes.h"

namespace MLRender
{
	//=========================================================
	//Name: MLVertexBuffer
	//Namespace: MLRenderer
	//Description: This is a wrapper for the openGL VBO calls to
	// reduce the amount of opengl code needed when using a VBO or
	// Even a IBO 
	//Creator: Chris Millin
	//Librarys: OpenGL
	//=========================================================
	class MLVertexBuffer
	{
	public:
		//Constructor
		MLCoreDLL MLVertexBuffer(void);
		//Deconstructor
		MLCoreDLL virtual ~MLVertexBuffer(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL CreateBuffer(const GLenum Type, const GLsizei size);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL CreateBuffer(const GLenum Type, const GLsizei size, const GLenum usage);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL CreateBuffer(const GLenum Type, const GLsizei size, 
			const GLvoid* data, const GLenum usage);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Bind(void)		{ glBindBuffer(m_type, m_bufferID); return ML_TRUE; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Unbind(void)		{ glBindBuffer(m_type, 0); return ML_TRUE; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetBufferData(const GLvoid* Data, const GLenum usage);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetBufferData(const GLsizei size, const GLvoid* Data, const GLenum usage);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetMapAccess(const GLbitfield access) { m_access = access; return ML_TRUE; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void* MapRange(const GLuint offset, const GLuint size);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Unmap(void) { glUnmapBuffer(m_type); return ML_TRUE; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetID(void) { return m_bufferID; } //returns the buffer id

	private:
		//The buffer ID
		GLuint m_bufferID;
		//The type of buffer
		GLenum m_type;
		//The access type for mapping
		GLbitfield m_access;
		//The size of the buffer
		GLsizei m_size;
	};
}
#endif 
