#ifndef MLCOREGL_H
#define MLCOREGL_H

#include "MLIncludes.h"

#include <MLMath/MLMath.h>

using namespace MLMath::MLMat4;

//This is a cheap way to do this but this will be all the opengl
//versions that can be supported for shader creation
#define ML_GL_VERSION_2_1 ML_STR("2_1\0")
#define ML_GL_VERSION_3_0 ML_STR("3_0\0")
#define ML_GL_VERSION_3_1 ML_STR("3_1\0")
#define ML_GL_VERSION_3_2 ML_STR("3_2\0")
#define ML_GL_VERSION_3_3 ML_STR("3_3\0")
#define ML_GL_VERSION_4_0 ML_STR("4_0\0")
#define ML_GL_VERSION_4_1 ML_STR("4_1\0")
#define ML_GL_VERSION_4_2 ML_STR("4_2\0")
#define ML_GL_VERSION_4_4 ML_STR("4_4\0")
#define ML_GL_VERSIONS_AVAILABLE 9

#define ML_LOG_GL ML_STR("GLCore")
#define ML_DEFAULT_BIT_COLOUR 32

namespace MLRender 
{
	struct MLScreenScaler;

	static ML_SCHAR* versionList[ML_GL_VERSIONS_AVAILABLE] = {	ML_GL_VERSION_2_1, ML_GL_VERSION_3_0, ML_GL_VERSION_3_1,
																ML_GL_VERSION_3_2, ML_GL_VERSION_3_3, ML_GL_VERSION_4_0,
																ML_GL_VERSION_4_1, ML_GL_VERSION_4_2, ML_GL_VERSION_4_4 };

	//=========================================================
	// Name: ResizeGL
	// Description: used to resize the openGL viewport.
	// This function is designed to be linked into windows
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void ResizeGL(ML_UINT width, ML_UINT height);

	//=========================================================
	// Name: MLGLContext
	// Namespace: MLRenderer
	// Description: The core for openGL. This mainly initalises
	// OpenGL and has the start and end frame calls
	// Creator: Chris Millin
	// Librarys: OpenGL
	//=========================================================
	class MLGLContext
	{
	public:

		//=========================================================
		// Name: Release
		// Description: Releases any objects created on the heap
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL Release(void);

		//=========================================================
		// Name: Initalise
		// Description: Will just set a few of the open parameters
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL Initalise(void);

		//=========================================================
		// Name: Bind
		// Description: This will allow for the opengl viewport to
		// be bound to a window's window. It will also intialise
		// openGL 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL Bind(HINSTANCE hInastance, HWND hWnd, ML_INT width = MLScreenWidth, 
					ML_INT height = MLScreenHeight, ML_INT bitCount = ML_DEFAULT_BIT_COLOUR);


		//=========================================================
		// Name: Start Frame
		// Description: Will clear the buffers ready for the next
		// draw.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL StartFrame(void);
		
		//=========================================================
		// Name: Clear Frame
		// Description: Will clear the buffers ready for the next
		// draw. (better name for it when the frame has not ended
		// but the screen needs to start fresh again)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL ClearFrame(void) { return StartFrame(); }

		//=========================================================
		// Name: End Frame 
		// Description: Will sawp the back buffer to the front
		// so that what was rendered can be displayed on the screen
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL EndFrame(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static UseGlobalScale() { SetGlobalScale(m_globalScaler); }

		//Getters
		//////////////////////////////////////////////////////////////////////////
		#pragma region Getters
		//=========================================================
		// Name: Get Version String
		// Description: Returns the current openGL version as a
		// string
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_CHAR* GetVersionS(void)		{ return m_GLSVersion; }
		MLCoreDLL static ML_FLOAT GetVersionF(void)		{ return m_GLFVersion; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLMatrix4f& GetOrtho(void)		{ return m_ortho; }
		//returns the viewport matrix

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLMatrix4f static	GetView(void)		{ return m_viewPort; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL HDC static GetContextID(void)			{ return m_hDC; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL HGLRC static GetRenderContextID(void)	{ return m_hRCmain; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL HGLRC static GetSharedRenderContextID(void)	{ return m_hRCthread; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLScreenScaler& GetGlobalScale() { return m_globalScaler; }
		#pragma endregion
		//////////////////////////////////////////////////////////////////////////

		//Setters
		//////////////////////////////////////////////////////////////////////////
		#pragma region Setters
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void SetGlobalScale(MLScreenScaler& scaler);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetEnable(const GLenum& enable)			{ glEnable(enable); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetDisable(const GLenum& disbale) 		{ glDisable(disbale); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetClearColour(const MLVector4f& col)	{ glClearColor(col.r, col.g, col.b, col.a); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetDrawBuffer(const GLenum& mode)		{ glDrawBuffer(mode); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetViewPort(GLint x, GLint y, GLsizei width, GLsizei height)
		{
			glViewport(x, y, width, height);
		}

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetScissor(GLint x, GLint y, GLsizei width, GLsizei height)
		{
			glScissor(x, y, width, height);
		}

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetOrtho(ML_FLOAT x, ML_FLOAT y, ML_FLOAT width, ML_FLOAT height)
		{
			MLMath::MLMat4::MLortho(m_ortho, x, height, y, width, 10.0f, -10.0f);
		}

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static SetScreen(const MLScreenScaler& scaler);

		//=========================================================
		// Name: Set Thread Context
		// Description: Will change the current context running on
		// a thread. (The main context should always be running on
		// the main thread and then this function will just change
		// for another created thread and not the main thread) this
		// allows for both threads to access the same opengl context
		// however they will take it in turns and will not be able
		// to access it at the same time.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static SetThreadContext(void);
		
		///=========================================================
		// Name: Set Ortho
		// Description: Creates and orthographic projection for
		// openGL to use.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL SetOrtho(ML_FLOAT left, ML_FLOAT right, ML_FLOAT bottom, ML_FLOAT top, ML_FLOAT zNear, ML_FLOAT zFar);
		#pragma endregion
		//////////////////////////////////////////////////////////////////////////

	private:
	
		//Constructor
		MLCoreDLL MLGLContext(void);
		//Deconstuctor
		MLCoreDLL virtual ~MLGLContext(void);

	private:

		MLCoreDLL static HDC			m_hDC;			// Private GDI Device Context
		MLCoreDLL static HGLRC			m_hRCmain;		// Permanent Rendering Context
		MLCoreDLL static HGLRC			m_hRCthread;

		MLCoreDLL static MLMatrix4f		m_ortho;		//stores the orthographic projection
		MLCoreDLL static MLMatrix4f		m_viewPort;    //stores the viewPort projection

		MLCoreDLL static ML_CHAR*		m_GLSVersion;
		MLCoreDLL static ML_FLOAT		m_GLFVersion;
		MLCoreDLL static MLScreenScaler m_globalScaler;
	};
}

#endif

