#ifndef MLMUSIC_H
#define MLMUSIC_H

#include "MLIncludes.h"
#include "MLAudioHelper.h"
#include "MLAudioManager.h"



namespace MLAudio
{
/// <summary>	Music Log Name. </summary>
#define ML_LOG_MUSIC ML_STR("Music")

/// <summary>	Max number of openAL buffers used to store audio data (less than 12 will cause
/// 			errors with some formats). </summary>
#define ML_MAX_ALBUFFER_STREAM 12
	
	class MLAudioThread;

	///-------------------------------------------------------------------------------------------------
	/// <summary>	A music file is designed to only be played
	///	once at a time since the audio is stream decoded rather
	///	than pre loaded like sound effects. The audio updating
	///	is also threaded so there is no need to explicitly
	///	call update unless the thread creation fails. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLMusic
	{
	public:
		MLCoreDLL MLMusic(void);
		MLCoreDLL virtual ~MLMusic(void);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will release the music object. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Release();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Loads an audio file for streaming using
		/// 			a virtual file path. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="file">	The audio file to load. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL LoadFile(const MLKernel::MLVirtualFile* file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Loads an audio file for streaming from a string file path. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="file">	The audio file to load. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL LoadFile(const ML_STRING& file);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the volume of all playing music if
		///	managed by the thread (if not it wont be changed and
		///	will have to be done manually though the set Music volume. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="gain">	The Volume for all music to use. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetGlobalMusicVolume(const ML_FLOAT gain);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the volume for the current audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="gain">	The Volume for the audio file. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetMusicVolume(const ML_FLOAT gain);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Check to see if the file is currently playing
		///	or not. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if the audio file is currently playing. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL IsPlaying() const { return m_playing; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the audio to be looped. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="loop">	Set if the audio file is to loop playing or not. </param>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void SetLooped(const ML_BOOL loop) { m_loop = loop; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Check to see if the file is looped or not. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if the audio file is playing packed looped. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL IsLooped() const { return m_loop; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Get the duration in seconds as a double. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	The duration of the audio file in seconds. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_DOUBLE Duration() const { return m_duration; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the duration as a string in the
		///	format of %%m%%s. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	The audio file duration as a string. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_STRING DurationString() const;

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the frequency value for the music file. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	The frequency of the audio file. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT Frequency() const { return m_frequency; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Return the amount of channels for the audio file. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the amount of channels the audio file uses. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT Channels()  const { return m_channels; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the bit rate of the audio file. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the bitrate of the audio file. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT BitRate() const { return m_bitRate; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the audio source for the music playing. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="source">	Set the source information the audio is going to playback with. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetSource(MLAudioSource& source);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will start streaming the music file. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Play();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will start streaming the music file. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="source">	Audio source information the audio is going to playback with. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Play(MLAudioSource& source);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will pause the audio streaming. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Pause();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will resume the audio streaming. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Resume();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will stop the audio from streaming. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Stop();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will update the audio stream. This is
		///	threaded so shouldn't need to be called unless the thread
		///	creation failed. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Update();

	private:

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will reset the FFMPEG audio stream back to 0. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL ResetStream();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Stream audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="returnFaltyFrame">	(Optional) If the audio has been reset form the to 0
		/// 								this is only important if the frame reading failed and resetting the 
		/// 								audio to 0 still caused the frame reading to fail. </param>
		///
		/// <returns>	Returns true if the audio was streamed successfully. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL StreamAudio(ML_BOOL returnFaltyFrame = ML_FALSE);

		//NOT IMPLEMENTED
		//=========================================================
		// Name: Set Stream Position
		// Description: Set the position of the audio to a specific
		// position.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetStreamPosition(ML_DOUBLE positionSeconds)  { return ML_FALSE; }

	private:

		/// <summary>	Static audio thread to add the audio to and be processed
		/// 			outside of the main loop. </summary>
		static MLAudioThread* m_thread;

		/// <summary>	The codec the audio file uses to decode audio. </summary>
		MLCodec*	m_codec;
	
		/// <summary>	Full path name of the loaded file. </summary>
		ML_STRING   m_loadedFilePath;

		/// <summary>	The frequency of the file. </summary>
		ML_UINT		m_frequency;
		/// <summary>	The amount of channels in the file. </summary>
		ML_UINT		m_channels;
		/// <summary>	The bit rate of the file (though I think it may actually be byte rate). </summary>
		ML_UINT		m_bitRate;
		/// <summary>	This is the Channel format. </summary>
		ML_UINT		m_channelFormat;
		/// <summary>	This is the format of the output from the decoding process. </summary>
		ML_UINT		m_decodeFormat;

		/// <summary>	duration of the audio track in seconds. </summary>
		ML_DOUBLE	m_duration;
		/// <summary>	If the file should loop or not. </summary>
		ML_BOOL		m_loop;
		/// <summary>	If the file is playing or not. </summary>
		ML_BOOL		m_playing;

		ML_BOOL		m_resetPlaying;

		/// <summary>	This is the source file for openAL. </summary>
		ALuint				m_ALSource;
		/// <summary>	The buffers . </summary>
		ALuint				m_ALBuffer[ML_MAX_ALBUFFER_STREAM];
		/// <summary>	Not really needed but is the current bufferID which needs changing (though this is normally always 0). </summary>
		ALuint				m_ALCurrentBuffer;
	};
}

#endif // MLMUSIC_H






