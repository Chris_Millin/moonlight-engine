#include "MLFontCore.h"

namespace MLRender
{
	MLCoreDLL FT_Library MLFontCore::m_library;

	MLCoreDLL ML_BOOL MLFontCore::Initalise(void)
	{
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Progress, ML_STR("Initalising TrueType\n"));
		//intialise FreeType
		FT_Error error = FT_Init_FreeType(&m_library);
		if (error)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Error TrueType could not initalise\n"));
			return ML_FALSE;
		}
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Progress, ML_STR("TrueType initalised successfully\n"));

		return ML_TRUE;
	}
}