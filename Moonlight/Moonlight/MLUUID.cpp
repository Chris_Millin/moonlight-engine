#include "MLUUID.h"

namespace MLKernel
{
	MLCoreDLL MLUUID::MLUUID(void)
	{
		m_uuid = new UUID;
		HRESULT result = UuidCreateNil(m_uuid);
		if(result != RPC_S_OK || result != S_OK)
		{
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not create empty UUID. Error Code %i\n"), result);
			delete m_uuid;
			m_uuid = NULL;
		}
	}

	MLCoreDLL MLUUID::~MLUUID(void)
	{
		Release();
	}

	void MLUUID::Release()
	{
		if(m_uuid)
		{
			delete m_uuid;
			m_uuid = NULL;
		}
	}

	ML_STRING MLUUID::Generate()
	{
		if(m_uuid)
		{
			m_uuid = new UUID;
			HRESULT result = UuidCreate(m_uuid);
			if(result == S_OK || result == RPC_S_OK ||
				result == RPC_S_UUID_LOCAL_ONLY || result == RPC_S_UUID_NO_ADDRESS)
			{
				return ToString();
			}
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not generate UUID. Error Code %i\n"), result);
			delete m_uuid;
			m_uuid = NULL;
			return ML_STR("");
		}

		ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Warning,
			ML_STR("UUID has already been created.\n"));

		return ML_STR("");
	}

	ML_STRING MLUUID::GenerateSequential()
	{
		if(m_uuid)
		{
			m_uuid = new UUID;
			HRESULT result = UuidCreateSequential(m_uuid);
			if(result == S_OK || result == RPC_S_OK ||
				result == RPC_S_UUID_LOCAL_ONLY || result == RPC_S_UUID_NO_ADDRESS)
			{
				return ToString();
			}
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not generate UUID. Error Code %i\n"), result);
			delete m_uuid;
			m_uuid = NULL;
			return ML_STR("");
		}

		ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Warning,
			ML_STR("UUID has already been created.\n"));

		return ML_STR("");
	}

	MLUUIDComp MLUUID::Compare(const MLUUID& id) const
	{
		if(!m_uuid || !id.GetUUID())
		{
			RPC_STATUS status;
			MLUUIDComp result = (MLUUIDComp)UuidCompare(m_uuid, id.GetUUID(), &status);
			if(status == RPC_S_OK || status == S_OK)
			{
				return result;
			}
			result = MLUUIDComp::Invalid;
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not compare UUID's. Error Code %i\n"), result);
			return result;
			
		}

		ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Warning,
			ML_STR("One of the UUID's are invalid.\n"));

		return MLUUIDComp::Invalid;
	}

	ML_BOOL MLUUID::Equal(const MLUUID& id) const
	{
		ML_BOOL result = ML_FALSE;

		if(!m_uuid || !id.GetUUID())
		{
			
			RPC_STATUS status;
			if(UuidEqual(m_uuid, id.GetUUID(), &status) > FALSE)
				result = ML_TRUE;

			if(status == RPC_S_OK || status == S_OK)
			{
				return result;
			}
			result = ML_FALSE;
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not compare UUID's. Error Code %i\n"), result);
			return result;
		}

		ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Warning,
			ML_STR("One of the UUID's are invalid.\n"));

		return result;
	}

	ML_BOOL MLUUID::IsValid() const
	{
		if(!m_uuid)
			return ML_FALSE;
		else
			return ML_TRUE;		
	}

	ML_STRING MLUUID::ToString() const
	{
		if(m_uuid)
		{
			RPC_WSTR* sTemp = new RPC_WSTR[30];
			HRESULT result = UuidToString(m_uuid,sTemp);
			if(result == RPC_S_OK || result == S_OK)
			{
				ML_STRING outstr = (ML_SCHAR*)*sTemp;
				RpcStringFree(sTemp);
				return outstr;
			}
			result = MLUUIDComp::Invalid;
			ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not compare UUID's. Error Code %i\n"), result);
			
			return ML_STR("");
		}

		ML_TRACE( ML_STR("UUID"), MLKernel::MLConsoleStatus::Warning,
			ML_STR("UUID has already been created.\n"));

		return ML_STR("");
	}
}
