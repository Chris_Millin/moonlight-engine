﻿






//////////////////////////////////////////////////////////////////////////
//Engine Check List
/////////////////////////////////////////////////////////////////////////
//
// # = Required
// * = Not fully required
// - = Not Important
// ~ = Partly Implemented
// + = Implemented
//
//
// 23 July 2014
//
// __________MATH__________
// + Matrix 3 maths
// + Matrix 2 maths
// + Matrix inverse Function
// + Matrix 2 & 3 translation matrix Functions
// ~ Intersection Tests (need to do polygon intersection)
// * 2D splines
// * 2D quaternions
// * 2D raycasting
// - 3D raycasting
//
// __________Graphics__________
// + Bitmap Font
// + Vector Font
// + Anti Alising into swapping buffer
// + Post Processing
// + Screenshot
// + Fix issue with Batch rendering (this is to do with std::sort using < as the default check method when > is used and a > b and b > a returns true it has
//   a hissy fit because it cant tell there equal. using the < symbol does allow it to know when things are equal so it wont break. Also to if rhs.layer == lhs.layer
//   then the next value is checked otherwise return rhs.layer < lhs.layer and this keeps going down the line to allow for ordering by multiple values)
// + font rendering complexity (highlighting)
// # set camera in graphics batcher
// * decals on mesh objects
// * Better texture support
// * holes in mesh array
// * screen scaling 
// * cutting mesh objects
//
//__________Audio__________
//
// + One audio file can be played multiple times
// + pre-load small audio files
// # Sort out 2D (3D) sound
// * FFT processing with ffmpeg
// * audio recording
//
//__________Input__________
//
// + Text editing position support? may not be needed here
// # External program for creating keyboard config
// # Keyboard Config Loading
// 
//
//__________Networking__________
//
// * getting json data from online source
// * sending data to a server
// * receiving data from a server
// 
//
//__________Engine__________ 
//
// # Entity System
// # State management System
// # Threaded box2D possibly
// # AngleScript Integration
// # Generic Object types
// # Particle system
// # Engine Config
// # External Launcher
// # Internal Config
// # Angle Script UI
// # occlusion
// # Control Management (eg buttonX = E and Controller button A)
//
//__________Editor__________
//
// # Atomineer UI
// # Script Editing
// # build Prefabs
// # Entity placement
// # Undo Redo
// # Run and Pause Game
// # Build Mesh object by clicking
// # Entity placement
// # Physics building
// # Exe Builder
//
//
//
//
// 30 July 2014
//
// __________Core__________
//
// + Random number generation
//
// __________Graphics__________
//
// # implemented signed distance field creation
//
// 
// 1 August 2014
//
// ToDo (IMPORTANT)
// + Virtual Drive, Path, Folder
// Convert Library to wstring and unicode
//
//
// 16 August
// Convert Console to use a thread method.
// Create a thread function called monitor (infinate while(ture) {}) which
// will actually perform the log writing, printf and
// log callback. Log will just put the message on a stack
// which will then be processed in monitor function thread
// (This didnt really need to be done just added a mutex for the console
// this does mean that console writing is going to hold up the game but
// they should be all disabled once the game is stable and in release mode
//
// 17 August 
//
// There is a issue with the way FFMPEG was built for windows with
// OPT/:NOREF enabled inthe linker. This doesnt matter too much 
// but will cause dll issues if not set to the same. It may be 
// best to rebuild the FFMPEG library in future to remove this issue.
// as it will likely help reduce the DLL file size.
//
//
//
// 
// 21st August 
// 
// __________Graphics__________
//
// Text Rendering requirements
// 
// ✓ Characters have middle origin
// ✓ Characters can rotate on their own 
// ✓ Characters can scale on their own
// ✓ Characters can be repositioned on their own
// ✓ All Characters have a origin
// ✓ All Characters can rotate around a core origin
// ✓ All Characters can scale to a core scale and adjust 
//	  advance and other elements to scale entire font selection 
// ✓ All Characters can be repositioned
// ✓ All Characters can be justified
// ✓ Text can be wrapped
// ✓ Text can be positioned in a text box and be cut off before the bottom or at the top 
//	  (any text between the cut off line will not be shown)
// ✓ Text can be positioned in a text box and continue flowing (includes box x and y)
// ✓ Text can be re-coloured
// ✓ Text can be highlighted
// ✓ Text can be underlined, overlined, striked