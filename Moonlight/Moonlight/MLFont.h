#ifndef MLFONT_H
#define MLFONT_H

#include "MLIncludes.h"
#include "MLFontCore.h"
#include "MLTexture.h"
#include "MLTextureManager.h"
#include "MLTextureAtlas.h"
#include "MLTextureManager.h"

#include <MLMath/MLMath.h>

using namespace MLMath::MLVec3;

namespace MLRender
{

	struct MLSprite;


	///-------------------------------------------------------------------------------------------------
	/// <summary>	Enumeration of the different types of
	///				font the can be generated </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	enum MLFontType
	{
		NotLoaded	= -1,
		TrueType	= 0,
		Bitmap		= 1,
		BitmapSignedDistance	= 2,
		TureTypeSignedDistance	= 3
	};

	///-------------------------------------------------------------------------------------------------
	/// <summary>	The font class loads and manages a truetype
	///				font. It also creates a texture atlas from cached
	///				characters. Rendering will be done in the sprite batch
	///				but will be assisted by this class. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLFont
	{
	public:

		//Constructor
		MLCoreDLL MLFont(void);
		//Deconstructor
		MLCoreDLL virtual ~MLFont(void);
			
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Releases any objects created in the class. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_BOOL Release();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns one of the atlas map font textures. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="id">	The index of the font texture. </param>
		///
		/// <returns>	Returns the texture object for the specific id if invalid NULL is returned. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL MLTexture* GetFontTexture(ML_UINT id) const { if (id < m_fontTextures.size()) return m_fontTextures[id]; else return NULL; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the amount of glyphs that have been cached </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the amount of glyphs </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT GetGlyphCount(void)	{ return m_glyphSet.size();}

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns a specific glyph character if it is present </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="character">	The character to get the glyph from. </param>
		///
		/// <returns>	Returns the glyph for the character if valid. If not the invalid glyph is returned. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL MLGlyph GetGlyph(const ML_SCHAR& character) const;

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the type of font that is stored. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the type of font that is being stored </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL MLFontType GetType() const	{ return m_type; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the size value for the font to generate glyphs with. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the size of the font. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT GetSize()	const		{ return m_fontSize; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the value used for the texture atlas height. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the height of the texture. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT GetTextureHeight()		{ return m_textureHeight; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the value used for the texture atlas width. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns the width of the texture. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_UINT GetTextureWidth()	const	{ return m_textureWidth; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns True if a texture has been loaded.
		///				should be used to check before trying to load glyph
		///				data (unless you intend to load the texture later)
		///				or trying to render the font. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if there is a texture for the font. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL IsTextureAvailable() const	{ return m_fontTextures.size() > 0; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns True if glyph data has been loaded.
		///				This should be checked before rendering to ensure there
		///				is a glyph to render. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if there is glyph data available. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL IsGlyphDataAvailable() const	{ return m_glyphSet.size() > 0; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the type of font that is stored. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if kerning is available. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL KerningSupport()	const		{ return m_kerningEnabled; }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns a wrapped version of the string
		///				based on the size of the size of the lineWidth </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="lineWidth">	The width of the text. </param>
		/// <param name="input">		The text to wrap. </param>
		///
		/// <returns>	Returns the wrapped text if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_STRING WrapText(ML_UINT lineWidth, ML_STRING& input);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the maximum width of the text.
		///				works by counting the width to the next \n.
		///				This is unaffected by the font scale and will require
		///				modifying the result to get the correct scaled result. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="input">	The input string to get the width of. </param>
		///
		/// <returns>	The width value of the string. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_FLOAT GetTextWidth(const ML_STRING& input);
			
		///-------------------------------------------------------------------------------------------------
		/// <summary>	works out the width of the line from a
		///				specific start point. A end of line is defined by \n. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="start">	The start of the line to check the width of. </param>
		/// <param name="input">	The input string to read from. </param>
		///
		/// <returns>	The line width. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_FLOAT GetLineWidth(ML_UINT start, const ML_STRING& input);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	works out the width of a string defined by
		///				its start and end position. If the sub string
		///				contains a '\n' then nextLine will return the start
		///				of the next line (if its less than end). If the sub
		///				string does not contain a '\n' then nextLine will
		///				return 0 </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="start">   	The start of the string. </param>
		/// <param name="end">	   	The end if the string. </param>
		/// <param name="nextLine">	when the next new line is in the string. </param>
		/// <param name="input">   	The input string to get the sub string length of. </param>
		///
		/// <returns>	The sub string width. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_FLOAT GetSubStringWidth(ML_UINT start, ML_UINT end, ML_UINT& nextLine, const ML_STRING& input);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns the maximum height of the text.
		///				works by adding the font size after each new line \n.
		///				This is unaffected by the font scale and will require
		///				modifying the result to get the correct scaled result. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="input">	The input string to get the height of. </param>
		///
		/// <returns>	The text height. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual ML_FLOAT GetTextHeight(const ML_STRING& input);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	If the font supports kerning check it to find the value. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="lastChar">	The previous character to check against. </param>
		/// <param name="thisChar"> The current character to check with. </param>
		///
		/// <returns>	The kerning value. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual MLVector2f GetKerning(const ML_SCHAR&  lastChar, const ML_SCHAR&  thisChar) = 0;

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns a sprite object based on the
		///				specified glyph character. Must be Overridden! </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="character">	The character code. </param>
		///
		/// <returns>	The glyph sprite object. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual MLSprite GetGlyphSprite(const ML_SCHAR& character) = 0;

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Returns a sprite object based on the
		///				specified glyph character. Must be Overridden! </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="character">	The glyph character information. </param>
		///
		/// <returns>	The glyph sprite object. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual MLSprite GetGlyphSprite(const MLGlyph& character) = 0;

	protected:

		/// <summary>	Dictionary of the stored glyphs. </summary>
		std::map<ML_SCHAR, MLGlyph> m_glyphSet;
		
		/// <summary>	If the font is split between multiple atlas
		///				textures this will store each one. </summary>
		std::vector<MLTexture*> m_fontTextures;
		
		/// <summary>	Stores the width of the font
		///				textures which should always be the same for each stored
		///				texture in the m_fontTextures. </summary>
		ML_UINT m_textureWidth;
		
		/// <summary>	Stores the height of the font
		///				textures which should always be the same for each stored
		///				texture in the m_fontTextures. </summary>
		ML_UINT m_textureHeight;
		
		/// <summary>	If a specific glyph could not be found then
		///				this glyph is returned. </summary>
		MLGlyph m_unknowGlyph;

		/// <summary>	stores the type of font being stored. </summary>
		MLFontType m_type;

		/// <summary>	Stores if the font loaded successfully or not. </summary>
		ML_BOOL m_fontLoaded;

		/// <summary>	Size of the font. </summary>
		ML_UINT m_fontSize;

		/// <summary>	If the font has kerning support. </summary>
		ML_BOOL m_kerningEnabled;
	};
}



#endif