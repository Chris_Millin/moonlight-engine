#include "MLVertexBuffer.h"

namespace MLRender
{

	MLCoreDLL MLVertexBuffer::MLVertexBuffer(void)
	{
		//Set the default map access which is good for buffer orphaning
		m_access = (GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
		//set the default type which is an array buffer
		m_type = GL_ARRAY_BUFFER;
		//set the size to 0
		m_size = 0;
		//set the buffer ID to 0
		m_bufferID = 0;
	}


	MLCoreDLL MLVertexBuffer::~MLVertexBuffer(void)
	{
		Release();
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::Release()
	{
		//delete the buffer if its valid
		if (m_bufferID != 0)
		{
			glDeleteBuffers(1, &m_bufferID);
			m_bufferID = 0;
		}

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::CreateBuffer(const GLenum Type, const GLsizei size)
	{
		//create a new buffer ID if there is no current buffer
		if (m_bufferID == 0)
			glGenBuffers(1, &m_bufferID);
		m_size = size;
		m_type = Type;
		//bind and set the data and then unbind
		Bind();
		glBufferData(m_type, m_size, NULL, GL_STREAM_DRAW);
		Unbind();

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::CreateBuffer(const GLenum Type, const GLsizei size, const GLenum usage)
	{
		//create a new buffer ID if there is no current buffer
		if (m_bufferID == 0)
			glGenBuffers(1, &m_bufferID);
		m_size = size;
		m_type = Type;
		//bind and set the data and then unbind
		Bind();
		glBufferData(m_type, m_size, NULL,  usage);
		Unbind();

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::CreateBuffer(const GLenum Type, const GLsizei size, 
		const GLvoid* data, const GLenum usage)
	{
		if (m_bufferID == 0)
			glGenBuffers(1, &m_bufferID);
		m_size = size;
		m_type = Type;
		//bind and set the data and then unbind
		Bind();
		glBufferData(m_type, m_size, data,  usage);
		Unbind();

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::SetBufferData(const GLvoid* Data, const GLenum usage)
	{
		//set the buffer data
		glBufferData(m_type, m_size, Data, usage);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLVertexBuffer::SetBufferData(const GLsizei size, const GLvoid* Data, const GLenum usage)
	{
		//set the buffer data
		glBufferData(m_type, size, Data, usage);

		return ML_TRUE;
	}

	MLCoreDLL void* MLVertexBuffer::MapRange(const GLuint offset, const GLuint size)
	{
		//map a range of the buffer
		if (offset >= (GLuint)m_size)
			return NULL;
	
		return glMapBufferRange(m_type, offset, size, m_access);
	}

}