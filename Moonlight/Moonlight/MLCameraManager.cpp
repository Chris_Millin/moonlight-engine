#include "MLCameraManager.h"

namespace MLRender
{
	MLCoreDLL ML_STRING MLCameraManager::m_activeCameraName;
	MLCoreDLL MLCamera* MLCameraManager::m_activeCamera;
	MLCoreDLL std::map<ML_STRING, MLCamera*> MLCameraManager::m_cameraDictionary;

	MLCoreDLL ML_BOOL MLCameraManager::Initalise()
	{
		ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Information,
			ML_STR("Initalising Camera Manager"));

		m_cameraDictionary[ML_DEFAULT_CAMERA] = new MLCamera();
		m_activeCamera = m_cameraDictionary[ML_DEFAULT_CAMERA];
		m_activeCameraName = ML_DEFAULT_CAMERA;

 		m_activeCamera->SetPosition(0,0);
 		m_activeCamera->SetRotation(ML_Deg2Rad(0));
		m_activeCamera->SetScale(1);
	
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLCameraManager::RemoveCamera(const ML_STRING& Name)
	{
		if (HasCamera(Name))
		{
			delete m_cameraDictionary[Name];
			m_cameraDictionary.erase(Name.c_str());
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager has not camera %s"), Name.c_str());
			return ML_FALSE;
		}

		
	}

	MLCoreDLL ML_BOOL MLCameraManager::RemoveCameras(void)
	{
		std::map<ML_STRING, MLCamera*>::iterator it;
		
		for (it = m_cameraDictionary.begin(); it != m_cameraDictionary.end(); it++)
		{
			delete it->second;
			it->second = NULL;
		}

		m_cameraDictionary.clear();

		return ML_TRUE;
	}

	MLCoreDLL ML_STRING MLCameraManager::CreateCamera()
	{
		MLKernel::MLUUID uuid;
		ML_STRING Name = uuid.Generate();

		if (!HasCamera(Name))
		{
			m_cameraDictionary[Name] = new MLCamera();
			m_cameraDictionary[Name]->SetPosition(0, 0, 0);
			m_cameraDictionary[Name]->SetRotation(0);
			m_cameraDictionary[Name]->SetScale(1);
			return Name;
		}
		else
		{
			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager already has a camera named %s"), Name.c_str());
			return ML_EMPTY_STRING;
		}
	}

	MLCoreDLL ML_BOOL MLCameraManager::CreateCamera(const ML_STRING& Name)
	{
		if (!HasCamera(Name))
		{
			m_cameraDictionary[Name] = new MLCamera();
			m_cameraDictionary[Name]->SetPosition(0, 0, 0);
			m_cameraDictionary[Name]->SetRotation(0);
			m_cameraDictionary[Name]->SetScale(1);

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager already has a camera named %s"), Name.c_str());
			return ML_FALSE;
		}
	}

	MLCoreDLL ML_STRING MLCameraManager::AddCamera(MLCamera* camera)
	{
		MLKernel::MLUUID uuid;
		ML_STRING Name = uuid.Generate();
		if (!HasCamera(Name))
		{
			m_cameraDictionary[Name] = camera;
			return Name;
		}
		else
		{
			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager already has a camera named %s"), Name.c_str());
			return ML_EMPTY_STRING;
		}
	}

	MLCoreDLL ML_BOOL MLCameraManager::AddCamera(const ML_STRING& Name, MLCamera* camera)
	{
		if (!HasCamera(Name))
		{
			m_cameraDictionary[Name] = camera;
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager already has a camera named %s"), Name.c_str());
			return ML_FALSE;
		}
		
	}

	MLCoreDLL ML_BOOL MLCameraManager::SetActiveCamera(const ML_STRING& Name)
	{
		if (HasCamera(Name))
		{
			m_activeCamera = m_cameraDictionary[Name];
			m_activeCameraName = Name;
			return ML_TRUE;
		}
		else
		{
			if (!HasCamera(ML_DEFAULT_CAMERA) && Name == ML_DEFAULT_CAMERA)
			{
				m_cameraDictionary[ML_DEFAULT_CAMERA] = new MLCamera();
				m_activeCamera = m_cameraDictionary[ML_DEFAULT_CAMERA];
				m_activeCameraName = ML_DEFAULT_CAMERA;

				m_activeCamera->SetPosition(0, 0);
				m_activeCamera->SetRotation(ML_Deg2Rad(0));
				m_activeCamera->SetScale(1);
				return ML_TRUE;
			}

			ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Camera Manager already has a camera named %s"), Name.c_str());
			return ML_FALSE;
		}
	}

	MLCoreDLL ML_BOOL MLCameraManager::HasCamera(const ML_STRING& Name)
	{
		return (m_cameraDictionary.find(Name) != m_cameraDictionary.end());
	}

	MLCoreDLL ML_BOOL MLCameraManager::PrintCameraMatrix(void)
	{
		ML_TRACE(ML_LOG_CAMERA, MLKernel::MLConsoleStatus::Information,
			ML_STR("m_activeCamera %s\n00 = %f, 01 = %f, 02 = %f, 03 = %f\n10 = %f, 11 = %f, 12 = %f, 13 = %f\n20 = %f, 21 = %f, 22 = %f, 23 = %f\n30 = %f, 31 = %f, 32 = %f, 33 = %f\n"),
			  m_activeCameraName.c_str(),
			  m_activeCamera->GetMatrix().m[0][0],m_activeCamera->GetMatrix().m[0][1],m_activeCamera->GetMatrix().m[0][2],m_activeCamera->GetMatrix().m[0][3],
			  m_activeCamera->GetMatrix().m[1][0],m_activeCamera->GetMatrix().m[1][1],m_activeCamera->GetMatrix().m[1][2],m_activeCamera->GetMatrix().m[1][3],
			  m_activeCamera->GetMatrix().m[2][0],m_activeCamera->GetMatrix().m[2][1],m_activeCamera->GetMatrix().m[2][2],m_activeCamera->GetMatrix().m[2][3],
			  m_activeCamera->GetMatrix().m[3][0],m_activeCamera->GetMatrix().m[3][1],m_activeCamera->GetMatrix().m[3][2],m_activeCamera->GetMatrix().m[3][3]);
	
	  return ML_TRUE;
	}

	

}