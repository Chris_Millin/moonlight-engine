#ifndef MLCONTROLLER_H
#define MLCONTROLLER_H

#include "MLIncludes.h"
#include "MLInput.h"
#include <Xinput.h>

using namespace MLMath::MLVec2;
//The Controller class be specialize in a game controller and
//allow for multiple controllers to be created so there can be more player
//for a game
namespace MLInput
{

	//=========================================================
	// Name: Controller Buttons
	// Description: Enumeration of all the controller buttons
	// that can be obtained.
	// Creator: Chris Millin
	//=========================================================
	enum class MLControllerButtons
	{
		Up = 0x00,
		Down = 0x01,
		Left = 0x02,
		Right = 0x03,
		Start = 0x04,
		Back = 0x05,
		LStick = 0x06,
		RStick = 0x07,
		LShoulder = 0x08,
		RShoulder = 0x09,
		A = 0x0C,
		B = 0x0D,
		X = 0x0E,
		Y = 0x0F
	};

	//=========================================================
	// Name: Controller Analogs
	// Description: Enumeration of the different controller
	// Analogs.
	// Creator: Chris Millin
	//=========================================================
	enum class MLControllerAnalogs
	{
		LeftAnalog		= 0x00,
		RightAnalog		= 0x01,
		TriggerAnalog	= 0x02
	};

	//=========================================================
	//Name: MLController
	//Description: The class deals with reading information
	// about connected xbox controllers. It will only work with
	// xbox controllers but can be of any brand.
	//Creator: Chris Millin
	//=========================================================
	class MLController
	{
	public:
		//Constructor
		MLCoreDLL MLController(void);
		MLCoreDLL MLController(const ML_BYTE ID);
		//Deconstructor
		MLCoreDLL virtual ~MLController(void);

		//=========================================================
		// Name: Set Controller ID
		// Description: Will set the ID for the controller 1 - 4
		// If the controller is obtained through the Input Manager
		// This will be auto set so no need for this to be interfaced
		// unless you want to change a controller.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetControllerID(const ML_BYTE ID);
		
		//=========================================================
		// Name: Is Controller Active
		// Description: Checks to see if the controller is available
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsControllerActive(void) const;
		
		//=========================================================
		// Name: Is Button Held (enum)
		// Description: Used to see if a button on the controller
		// is held down or not.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsButtonHeld(const MLControllerButtons& button)
		{
			return IsButtonHeld((ML_BYTE)button);
		}

		//=========================================================
		// Name: Is Button Pressed (enum)
		// Description: Used to see if a button on the controller
		// is pressed or not.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsButtonPressed(const MLControllerButtons& button)
		{
			return IsButtonPressed((ML_BYTE)button);
		}

		//=========================================================
		// Name: Is Button Held
		// Description: Used to see if a button on the controller
		// is held down or not.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsButtonHeld(const ML_BYTE button);

		//=========================================================
		// Name: Is Button Pressed
		// Description: Used to see if a button on the controller
		// is pressed or not.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsButtonPressed(const ML_BYTE button);

		//=========================================================
		// Name: Get Analog
		// Description: This will return a vector of the controller
		// analog. the left and right analog stick are normalised
		// between -1.0 and 1.0. The trigger is between 0.0 and 1.0
		// Creator: Chris Millin 
		//=========================================================
		MLCoreDLL MLVector2f GetAnalog(const MLControllerAnalogs& analog);
		
		//=========================================================
		// Name: Set Force Feedback
		// Description: This will set the vibration of the controller
		// but will set the left and right vibration the same.
		// Creator:
		//=========================================================
		MLCoreDLL ML_BOOL SetForceFeedback(const ML_FLOAT amount); 

		//=========================================================
		// Name: Set Force Feedback
		// Description: This will set the vibration of the controller
		// but will allow for left and right motors to be set
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetForceFeedback(const ML_FLOAT amountL, const ML_FLOAT amountR); 

	private:

		//=========================================================
		// Name: Normalise Analog
		// Description: This will just normalise the 2 analog sticks
		// and also factor in deadzone. This is taken from the
		// Microsoft example.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVector2f NormaliseAnalog(MLVector2f inVec);
		
		//=========================================================
		// Name: Read Buttons
		// Description: This will read all the buttons on the controller
		// and set which ones are active and which ones are not.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ReadButtons(void);

	private:
		//The controller ID
		DWORD m_gamePadID;
		//3 axis on the controller
		//left stick, right stick and triggers
		MLVector2f m_axis[3];

		//This should be 14 but since bits 12 and 11 are not used it means
		//16 needs to be used to gain a quick way of getting the keys
		ML_BOOL m_buttonCurrentStates[16];
		ML_BOOL m_buttonPreviousStates[16];
	};
}


#endif