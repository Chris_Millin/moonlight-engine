#ifndef MLFORMATS_H
#define MLFORMATS_H

#include "MLIncludes.h"

using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;
using namespace MLMath::MLVec4;

namespace MLRender
{
	
	//=========================================================
	// Name: Screen Scaling
	// Description: Defines the type of screen 
	// scaling that can be applied.
	// Creator: Chris Millin
	//=========================================================
	enum class MLScreenScaling
	{
		//All screen scale will be ignored in favor of the base
		//resolution
		None = 0,
		//The screen will just scale the pixels and will not care about 
		//attempting to maintain a good aspect ration
		Strech = 1,
		//Will attempt to maintain the base aspect ratio and will apply
		//black bars to areas which arent being rendered to
		BestFit = 2,
		//same action as none but will offset the screen to center the same area as 
		//would be seen in a wider aspect ratio or more if the base ratio was smaller
		//this is only effected by ratio and will scale if the ratio is the same
		//but resolution is smaller than base
		Zoom = 3,

		Center = 4
	};

	//=========================================================
	// Name: Screen Scaler
	// Description: Designed to assist with screen scaling
	// Creator: Chris Millin
	//=========================================================
	struct MLScreenScaler
	{
		//This should cotain a original rendering resolution
		//this resolution should never change during the course of the game
		//as this is what defines the sizes's for the MLBatch to understand so
		//if set to 1920x1080 then position 60, 60 will be 1:1 however if the
		//actual resolution of the windows window is 1280x720 then the mapping is
		//actually 2:1 so for every 2 pixels in the 1920 resolution its 1 in the
		//window so 60, 60 will now become 30, 30 but will still maintin the same
		//position as before.
		ML_UINT BaseWidth;
		ML_UINT BaseHeight;

		ML_UINT OffsetWidth;
		ML_UINT OffsetHeight;

		//The method that is going to be applied for scaling
		MLScreenScaling Method;
	};

	const MLScreenScaler DefaultScreen = MLScreenScaler{ 0, 0, 0, 0, MLScreenScaling::None };

	//=========================================================
	// Name: Vertex Position Texture
	// Description: A structure to help describe a vertex that
	// has only a position and UV
	// Creator: Chris Millin
	//=========================================================
	struct MLVertexPosTex
	{
		MLVector3f VPos;
		MLVector2f UV;			
	};

	//=========================================================
	// Name: Vertex Position Texture Colour
	// Description: A structure to help describe a vertex that
	// has a position, UV and Colour
	// Creator: Chris Millin
	//=========================================================
	struct MLVertexPosTexCol
	{
		MLVector3f VPos;
		MLVector2f UV;
		MLVector4f Colour;
	};

}


#endif //MLFORMATS_H