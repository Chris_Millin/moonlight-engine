#ifndef MLFRAMEBUFFER_H
#define MLFRAMEBUFFER_H

#include "MLIncludes.h"
#include "MLGLContext.h"
#include "MLTexture.h"
#include "MLRenderBuffer.h"

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLFrameBuffer
	{
	public:
		MLCoreDLL MLFrameBuffer(void);
		MLCoreDLL virtual ~MLFrameBuffer(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void CreateBuffer(const GLenum& type);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AttachTexture(const MLTexture* texture, const GLenum& attachment);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLTexture* AttachTexture(const ML_UINT width, const ML_UINT height, const GLenum& attachment);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AttachMSTexture(const MLTexture* texture, const GLenum& attachment);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AttachRenderBuffer(const MLRenderBuffer* buffer, const GLenum& attachment);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Bind(const GLenum type = -1);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL UnBind(const GLenum type = -1);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Blit(const ML_INT width, const ML_INT height);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLenum GetStatus() const { return glCheckFramebufferStatus(m_type); }

	private:

		GLuint m_bufferID;
		GLenum m_type;
		GLenum m_textureAttachment;
		GLenum m_renderAttachment;

		ML_BOOL m_bound;
	};
}
#endif