#include "MLTextureManager.h"

namespace MLRender
{

	MLCoreDLL MLTextureManager::MLTextureGroup MLTextureManager::m_textures;

	MLCoreDLL MLTexture* MLTextureManager::LoadTexture(ML_STRING& name, const ML_STRING& path)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(!TextureExist(name))
		{
			MLTexture* texture = new MLTexture();
			texture->LoadTexture(MLtoAscii(path).c_str());
			texture->SetNameID(name);
			m_textures[name] = texture;
			return texture;
		}
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant create a new texture"), name.c_str());

			return NULL;
		}
	}

	MLCoreDLL MLTexture* MLTextureManager::LoadTexture(ML_STRING& name, const MLKernel::MLVirtualFile* path)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(!TextureExist(name))
		{
			MLTexture* texture = new MLTexture();
			texture->LoadTexture(path);
			texture->SetNameID(name);
			m_textures[name] = texture;
			return texture;
		}
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant create a new texture"), name.c_str());

			return NULL;
		}
	}

	MLCoreDLL MLTexture* MLTextureManager::CreateTexture()
	{
		return CreateTexture(ML_EMPTY_STRING);
	}

	MLCoreDLL MLTexture* MLTextureManager::CreateTexture(ML_STRING& name)
	{
		if(name.empty())
		{
			MLKernel::MLUUID uuid;
			name = uuid.Generate();
		}

		if(!TextureExist(name))
		{
			MLTexture* texture = new MLTexture();
			texture->SetNameID(name);
			m_textures[name] = texture;
			return texture;
		}
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s found. Cant create a new texture"), name.c_str());

			return NULL;
		}
	}

	MLCoreDLL ML_BOOL MLTextureManager::RemoveTexture(const ML_STRING& name)
	{
		if(TextureExist(name))
		{
			MLRELEASE(m_textures[name]);
			m_textures.erase(name);
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant remove a key that doesnt exist"), name.c_str());

			return ML_FALSE;
		}
	}

	MLCoreDLL ML_BOOL MLTextureManager::RemoveTexture(const MLTexture* texture)
	{
		MLTextureGroup::iterator it;

		for(it = m_textures.begin(); it != m_textures.end(); it++)
		{
			if(it->second->GetTextureID() == texture->GetTextureID())
			{
				MLRELEASE(it->second);
				break;
			}
		}

		if(it != m_textures.end())
		{
			m_textures.erase(it);
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLTextureManager::RemoveAll()
	{
		MLTextureGroup::iterator it;

		for(it = m_textures.begin(); it != m_textures.end(); it++)
			MLRELEASE(it->second);

		m_textures.clear();

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLTextureManager::TextureExist(const ML_STRING& name)
	{
		return m_textures.find(name) != m_textures.end();
	}

	MLCoreDLL MLTexture* MLTextureManager::GetTexture(const ML_STRING& name)
	{
		if(TextureExist(name))
		{
			return m_textures[name];
		}
		else
		{
			ML_TRACE(ML_LOG_TEXTURE_MANAGER, MLKernel::MLConsoleStatus::Error,
				ML_STR("Key %s not found. Cant get a key that doesnt exist"), name.c_str());

			return NULL;
		}
	}

	ML_STRING MLTextureManager::GetTextureName(const MLTexture* texture)
	{
		MLTextureGroup::iterator it;

		for(it = m_textures.begin(); it != m_textures.end(); it++)
		{
			if(it->second == texture)
			{
				return it->first;
			}
		}

		return ML_EMPTY_STRING;
	}
}