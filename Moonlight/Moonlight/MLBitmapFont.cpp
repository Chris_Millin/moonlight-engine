#include "MLBitmapFont.h"


namespace MLRender
{
	MLBitmapFont::MLBitmapFont(void)
	{
		m_type		= MLFontType::Bitmap;
		m_paddingV	= 0;
	}

	MLBitmapFont::~MLBitmapFont(void)
	{
		Release();
	}

	ML_BOOL MLBitmapFont::Release()
	{
		MLFont::Release();

		std::map<ML_SCHAR, std::map<ML_SCHAR, ML_INT>>::iterator it1;
		for(it1 = m_kerningData.begin(); it1 != m_kerningData.end(); it1++)
		{
			it1->second.clear();
		}

		m_kerningData.clear();

		return ML_TRUE;
	}

	ML_BOOL MLBitmapFont::LoadFontTexture(const MLKernel::MLVirtualFile* File)
	{
		if(!File)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Texture file path is invalid exiting loading font\n"));
			return ML_FALSE;
		}

		if(File->IsOpen())
			File->CloseFile();

		if(File->FullFilePath().empty())
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Texture file path is invalid exiting loading font\n"));
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Progress, ML_STR("Loading font texture from: %s \n"), File->FullFilePath().c_str());

		MLTexture* fontTexture = MLTextureManager::CreateTexture();
		if(!fontTexture->LoadTexture(File))
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Loading font texture failed\n"));
			return ML_FALSE;
		}

		m_fontTextures.push_back(fontTexture);

		return ML_FALSE;
	}

	ML_BOOL MLBitmapFont::LoadFontXML(const MLKernel::MLVirtualFile* glyphXML)
	{
		if(!glyphXML)
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Glyph XML file path is invalid exiting loading font XML\n"));
			return ML_FALSE;
		}

		if(glyphXML->IsOpen())
			glyphXML->CloseFile();

		if(glyphXML->FullFilePath().empty())
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Glyph XML file path is invalid exiting loading font XML\n"));
			return ML_FALSE;
		}

		try
		{
			m_fontTextures.clear();
			ticpp::Document glyphData(MLtoAscii(glyphXML->FullFilePath()).c_str());
			glyphData.LoadFile();

			m_fontLoaded = ML_TRUE;	

			ticpp::Iterator<ticpp::Element> child;	
			for(child = child.begin(glyphData.FirstChildElement()); child != child.end(); child++)
			{
				ML_U8STRING name;
				child->GetValue(&name);
				if(name == "info")
					if(!LoadInfoXML(child.Get()))		m_fontLoaded = ML_FALSE;
				if(name == "common")
					if(!LoadCommonXML(child.Get()))		m_fontLoaded = ML_FALSE;
				if(name == "pages")
					if(!LoadPagesXML(child.Get()))		m_fontLoaded = ML_FALSE;
				if(name == "chars")
					if(!LoadCharsXML(child.Get()))		m_fontLoaded = ML_FALSE;
				if(name == "kernings")
					if(!LoadKerningsXML(child.Get()))	m_fontLoaded = ML_FALSE;
			}	
		}
		catch( ticpp::Exception& ex )
		{
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to load XML file. %S\n"), ex.what());
			return ML_FALSE;
		}

		return m_fontLoaded;
	}

	ML_BOOL MLBitmapFont::SetGlyphInfo(const ML_SCHAR character, MLGlyph glyphData)
	{
		m_glyphSet[(ML_SCHAR)character] = glyphData;
		return ML_TRUE;
	}

	MLVector2f MLBitmapFont::GetKerning(const ML_SCHAR& lastChar, const ML_SCHAR& thisChar) const
	{
		MLKeringIterator it1;
		it1 = m_kerningData.find(lastChar);
		if(it1 != m_kerningData.end())
		{
			MLKerningSubIterator it2 = it1->second.find(thisChar);
			if(it2 != it1->second.end())
				return MLVector2f((ML_FLOAT)it2->second, 0.0f);
			else
				return 0.0f;
		}

		return 0.0f;
	}

	MLSprite MLBitmapFont::GetGlyphSprite(const ML_SCHAR& character) const
	{
		MLSprite GlyphSprite;

		if(m_fontLoaded)
		{
			MLGlyph Glyph = GetGlyph(character);

			ML_FLOAT glythX = Glyph.X + Glyph.paddingX;
			ML_FLOAT glythY = Glyph.Y + Glyph.paddingY;
			GlyphSprite.Position = MLVector2f(0.0f,0.0f);
			GlyphSprite.Texture = GetFontTexture(Glyph.textureID);
			GlyphSprite.Source = MLVector4f(glythX, glythY, (ML_FLOAT)(Glyph.Width + glythX), (ML_FLOAT)(Glyph.Height + glythY));
			GlyphSprite.Size = MLVector2f(Glyph.Width, Glyph.Height);
			GlyphSprite.Colour =  1.0f;
			GlyphSprite.SetOriginMiddleOfSize();
		}
		else
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));

		return GlyphSprite;
	}

	MLSprite MLBitmapFont::GetGlyphSprite(const MLGlyph& character) const
	{
		MLSprite GlyphSprite;

		if(m_fontLoaded)
		{
			ML_FLOAT glythX = character.X;
			ML_FLOAT glythY = character.Y;

			GlyphSprite.Position = MLVector2f(0.0f,0.0f);
			GlyphSprite.Texture = GetFontTexture(character.textureID);
			GlyphSprite.Source = MLVector4f(glythX, glythY, (ML_FLOAT)(character.Width + character.X), (ML_FLOAT)(character.Height + character.Y));
			GlyphSprite.Size = MLVector2f(character.Width, character.Height);
			GlyphSprite.Colour =  1.0f;
			GlyphSprite.SetOriginMiddleOfSize();
		}
		else
			ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, ML_STR("Font has not been loaded. Load a valid font first before manipulating it.\n"));

		return GlyphSprite;
	}

	ML_BOOL MLBitmapFont::LoadInfoXML(const ticpp::Element* xmlElement)
	{
		string fontName;
		string paddingString;
	
		m_paddingV = 0;

		xmlElement->GetAttribute("face", &fontName);
		xmlElement->GetAttribute("size", &m_fontSize);
		xmlElement->GetAttribute("padding", &paddingString);
		std::vector<std::string> padding = MLsplitString(paddingString, ',');
		m_paddingV.x = atoi(padding[0].c_str());
		m_paddingV.y = atoi(padding[1].c_str());
		m_paddingV.z = atoi(padding[2].c_str());
		m_paddingV.w = atoi(padding[3].c_str());

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Font Info:\n"));
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Type Bitmap\n"));
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Name %S\n"), fontName.c_str());
		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Size %i\n"), m_fontSize);

		return ML_TRUE;
	}

	ML_BOOL MLBitmapFont::LoadCommonXML(const ticpp::Element* xmlElement)
	{
		xmlElement->GetAttribute("scaleW", &m_textureWidth);
		xmlElement->GetAttribute("scaleH", &m_textureHeight);
		if(m_textureHeight == 0 || m_textureWidth == 0)
			return ML_FALSE;
		else
			return ML_TRUE;
	}

	ML_BOOL MLBitmapFont::LoadPagesXML(const ticpp::Element* xmlElement)
	{
		string textureFilePath;

		ticpp::Element* elm = xmlElement->FirstChildElement();
		ticpp::Iterator<ticpp::Element> pagesChild;
		for(pagesChild = xmlElement->FirstChildElement(); pagesChild != pagesChild.end(); pagesChild++)
		{
			pagesChild->GetAttribute("file", &textureFilePath);
			MLTexture* pageTexture = MLTextureManager::CreateTexture();
			if(pageTexture->LoadTexture(textureFilePath.c_str()))
				m_fontTextures.push_back(pageTexture);
			else
			{
				ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Error, 
					ML_STR("Texture %S count not be loaded. Make sure it is in the correct location\n"), textureFilePath.c_str());
			}
		}

		if(m_fontTextures.size() == 0)
			return ML_FALSE;

		return ML_TRUE;
	}

	ML_BOOL MLBitmapFont::LoadCharsXML(const ticpp::Element* xmlElement)
	{
		ML_UINT glyphCount		= 0;

		xmlElement->GetAttribute("count", &glyphCount);
		ticpp::Iterator<ticpp::Element> glyphChild;
		for(glyphChild = xmlElement->FirstChildElement(); glyphChild != glyphChild.end(); glyphChild++)
		{
			if(glyphChild->Value() == "char")
			{						
				MLGlyph glyph;
				glyph.paddingX = (ML_FLOAT)m_paddingV.x;
				glyph.paddingY = (ML_FLOAT)m_paddingV.y;
				glyph.paddingZ = (ML_FLOAT)m_paddingV.z;
				glyph.paddingW = (ML_FLOAT)m_paddingV.w;

				glyphChild->GetAttribute("id",		 &glyph.character);
				glyphChild->GetAttribute("x",		 &glyph.X);
				glyphChild->GetAttribute("y",		 &glyph.Y);
				glyphChild->GetAttribute("width",	 &glyph.Width);
				glyphChild->GetAttribute("height",	 &glyph.Height);
				glyphChild->GetAttribute("xoffset",  &glyph.bearingX);
				glyphChild->GetAttribute("yoffset",  &glyph.bearingY);
				glyphChild->GetAttribute("xadvance", &glyph.advance);
				glyphChild->GetAttribute("page",	 &glyph.textureID);

				glyph.bearingY = -glyph.bearingY;

				glyph.X			+= m_paddingV.x;
				glyph.Width		-= m_paddingV.x2;
				glyph.Y			+= m_paddingV.y;
				glyph.Height	-= m_paddingV.y2;
				m_glyphSet[(wchar_t)glyph.character] = glyph;

				//wstring val;
				//val.insert(val.begin(), (wchar_t)glyph.character);
				//ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Character: %s bearing x: %f bearing y: %f advance: %f W: %f H: %f\n"),
				//	val.c_str(), glyph.bearingX, glyph.bearingY, glyph.advance, glyph.Width, glyph.Height);
			}
		}

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Glyphs %i\n"), glyphCount);

		return ML_TRUE;
	}

	ML_BOOL MLBitmapFont::LoadKerningsXML(const ticpp::Element* xmlElement)
	{
		ticpp::Iterator<ticpp::Element> kerningChild;
		for(kerningChild = xmlElement->FirstChildElement(); kerningChild != kerningChild.end(); kerningChild++)
		{
			if(kerningChild->Value() == "kerning")
			{
				ML_UINT first	= 0;
				ML_UINT second	= 0;
				ML_INT amount	= 0;

				kerningChild->GetAttribute("first", &first);
				kerningChild->GetAttribute("second", &second);
				kerningChild->GetAttribute("amount", &amount);

				m_kerningData[first][second] = amount;
			}
		}

		ML_TRACE(ML_LOG_FONT, MLKernel::MLConsoleStatus::Information, ML_STR("Kerning Support %S\n"), m_kerningData.size() > 0 ? "True" : "False");
	
		return ML_TRUE;
	}
}