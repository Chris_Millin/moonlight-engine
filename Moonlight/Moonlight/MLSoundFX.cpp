#include "MLSoundFX.h"

namespace MLAudio
{
	MLSoundFX::MLSoundFX(const ML_STRING& groupName)
	{
		m_groupName = groupName;
		m_SFXPool.clear();
	}


	MLSoundFX::~MLSoundFX(void)
	{
		Release();
	}

	ML_BOOL MLSoundFX::Release()
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		StopAll();

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			MLSFX* sfxFile = it->second;
			bool unloaded = UnloadSFX(it->first);
			if (!unloaded)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Warning,
					ML_STR("Could not remove SFX %s the file was invalid will attempt to remove it any way.\n"));
			}

			if (sfxFile->PCMData)
			{
				delete[] sfxFile->PCMData;
				sfxFile->PCMData = NULL;
			}
			if (sfxFile)
			{
				delete sfxFile;
				sfxFile = NULL;
			}
		}

		m_SFXPool.clear();

		return ML_TRUE;
	}

	ML_STRING MLSoundFX::LoadFile(const ML_STRING& file)
	{
		MLKernel::MLUUID id;
		ML_STRING key = id.Generate();
		MLKernel::MLVirtualFile* vfile = new MLKernel::MLVirtualFile(file);

		ML_STRING result = LoadFile(key, vfile);
		MLRELEASE(vfile);
		if (result != key)
			return ML_EMPTY_STR;
		else
			return result;
	}

	ML_STRING MLSoundFX::LoadFile(const MLKernel::MLVirtualFile* file)
	{
		MLKernel::MLUUID id;
		ML_STRING key = id.Generate();

		ML_STRING result = LoadFile(key, file);
		if (result != key)
			return ML_EMPTY_STR;
		else
			return result;
	}

	ML_STRING MLSoundFX::LoadFile(const ML_STRING& ID, const ML_STRING& file)
	{
		MLKernel::MLVirtualFile* vfile = new MLKernel::MLVirtualFile(file);
		ML_STRING result = LoadFile(ID, vfile);
		MLRELEASE(vfile);
		if (result != ML_EMPTY_STR)
			return ML_EMPTY_STR;
		else
			return result;
	}

	ML_STRING MLSoundFX::LoadFile(const ML_STRING& ID, const MLKernel::MLVirtualFile* file)
	{
		if (file == NULL)
			return ML_STR("");

		MLSFX* sfxFile = new MLSFX();
		sfxFile->PCMData = NULL;
		sfxFile->PCMDataSize = 0;

		MLCodec* sfxCodec = MLAudioManager::LoadAudioFile(file);
		if (!sfxCodec)
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to load audio file with FFMPEG.\n"));
			return ML_EMPTY_STR;
		}

		ML_DOUBLE divDuration = (1.0 / (ML_DOUBLE)av_q2d(sfxCodec->avStream->time_base));

		sfxFile->FileName = file->FullFilePath();
		sfxFile->Frequency = sfxCodec->avCodecCtx->sample_rate;
		sfxFile->Channels = sfxCodec->avCodecCtx->channels;
		sfxFile->BitRate = sfxCodec->avCodecCtx->bit_rate;
		sfxFile->ChannelFormat = 0;
		sfxFile->DecodeFormat = sfxCodec->avCodecCtx->sample_fmt;
		sfxFile->Duration = (ML_DOUBLE)sfxCodec->avStream->duration/* / divDuration*/;

		switch (sfxFile->Channels)
		{
		case 1:
			sfxFile->ChannelFormat = AL_FORMAT_MONO16;
			break;
		case 2:
			sfxFile->ChannelFormat = AL_FORMAT_STEREO16;
			break;
		case 4:
			if (alIsExtensionPresent("AL_EXT_MCFORMATS"))
			{
				sfxFile->ChannelFormat = alGetEnumValue("AL_FORMAT_QUAD16");
			}
			break;
		case 6:
			if (alIsExtensionPresent("AL_EXT_MCFORMATS"))
			{
				sfxFile->ChannelFormat = alGetEnumValue("AL_FORMAT_51CHN16");
			}
			break;
		default:
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error, ML_STR("Channel format not supported by openAL.\n"));
			return ML_EMPTY_STR;
		}

		ML_STRING CheckID = ID;

		if (SFXExists(CheckID))
		{
			MLKernel::MLUUID id;
			CheckID = id.Generate();
		}

		if (!PreLoad(CheckID, sfxCodec, sfxFile))
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to pre-load audio file.\n"));
			return ML_EMPTY_STR;
		}

		MLAudioManager::ReleaseCodec(sfxCodec);

		return CheckID;
	}

	ML_BOOL MLSoundFX::PreLoad(const ML_STRING& key, const MLCodec* sfxCodec, MLSFX* sfxFile)
	{
		ML_UINT bufferSize = 0;

		std::vector<MLAudioFrame*> decodedFrames = DecodeStream(sfxFile, sfxCodec, bufferSize);

		ML_UINT BufferLoc = 0;
		if (bufferSize > 0 && decodedFrames.size() > 0)
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Information, ML_STR("Alloc Memory: %.3fkb %.2fmb\n"),
				(ML_DOUBLE)bufferSize / 1024.0, ((ML_DOUBLE)bufferSize / 1024.0) / 1024.0);
			sfxFile->PCMData = new ML_SHORT[bufferSize];
			sfxFile->PCMDataSize = bufferSize;

			for (ML_UINT i = 0; i < decodedFrames.size(); i++)
			{
				memcpy(&sfxFile->PCMData[BufferLoc], decodedFrames[i]->Data, decodedFrames[i]->Size * 2);
				BufferLoc += decodedFrames[i]->Size;
				delete[] decodedFrames[i]->Data;
				delete decodedFrames[i];
			}

			decodedFrames.clear();
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Buffer size (%d) or returned frame (%d) are invalid.\n"), bufferSize, decodedFrames.size());
			return ML_FALSE;
		}

		ALuint bufferID = 0;
		alGenBuffers(1, &bufferID);
		alBufferData(bufferID, sfxFile->ChannelFormat, sfxFile->PCMData, sfxFile->PCMDataSize * 2, sfxFile->Frequency);

		sfxFile->ALBuffer = bufferID;

		m_SFXPool[key] = sfxFile;

		return ML_TRUE;
	}

	ML_BOOL MLSoundFX::UnloadSFX(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			for (ML_UINT i = 0; i < m_SFXPool[ID]->ALSources.size(); i++)
			{
				ALint state;
				alGetSourcei(m_SFXPool[ID]->ALSources[i], AL_SOURCE_STATE, &state);
				if (state != AL_STOPPED)
				{
					alSourceStop(m_SFXPool[ID]->ALSources[i]);
				}
				alDeleteSources(1, &m_SFXPool[ID]->ALSources[i]);
			}

			alDeleteBuffers(1, &m_SFXPool[ID]->ALBuffer);
			m_SFXPool[ID]->ALSources.clear();

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::ReloadSFX(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->PCMDataSize != 0 || m_SFXPool[ID]->PCMData == NULL)
			{
				alGenBuffers(1, &m_SFXPool[ID]->ALBuffer);
				alBufferData(m_SFXPool[ID]->ALBuffer, m_SFXPool[ID]->ChannelFormat, m_SFXPool[ID]->PCMData, m_SFXPool[ID]->PCMDataSize * 2, m_SFXPool[ID]->Frequency);
				
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Cant reload a SFX file that does not have PCM data.\n"));
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::RemoveSFX(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			MLSFX* sfxFile = m_SFXPool[ID];
			ML_BOOL forceUnload = UnloadSFX(ID);
			if (!sfxFile->InUse && sfxFile->ALBuffer == 0 && !forceUnload)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Warning,
					ML_STR("Could not remove file as its in use still.\n"));
				return ML_FALSE;
			}
			
			if (sfxFile->PCMData)
			{
				delete[] sfxFile->PCMData;
				sfxFile->PCMData = NULL;
				sfxFile->PCMDataSize = 0;
			}
			if (sfxFile)
			{
				delete sfxFile;
				sfxFile = NULL;
			}

			m_SFXPool.erase(ID);
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::RemoveAll()
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			MLSFX* sfxFile = it->second;
			ML_BOOL forceUnload = UnloadSFX(it->first);
			if (!sfxFile->InUse && sfxFile->ALBuffer == 0 && !forceUnload)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Warning,
					ML_STR("SFX %s still in use but will be remove any way.\n"), it->first);
			}

			if (sfxFile->PCMData)
			{
				delete[] sfxFile->PCMData;
				sfxFile->PCMData = NULL;
				sfxFile->PCMDataSize = 0;
			}
			if (sfxFile)
			{
				delete sfxFile;
				sfxFile = NULL;
			}
		}

		m_SFXPool.clear();

		return ML_TRUE;
	}

	ML_BOOL MLSoundFX::SFXAvaliable(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer != 0)
			{
				return ML_TRUE;
			}
		}
		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::SFXExists(const ML_STRING& ID)
	{
		return m_SFXPool.find(ID) != m_SFXPool.end();
	}

	ML_BOOL MLSoundFX::SFXLoaded(const ML_STRING& path)
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			if (it->second->FileName == path && it->second->PCMData != NULL
				&& it->second->ALBuffer != 0)
			{
				return ML_TRUE;
			}
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::SFXLoaded(const MLKernel::MLVirtualFile* file)
	{
		return SFXLoaded(file->FullFilePath());
	}

	MLSFX* MLSoundFX::GetSFX(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			return m_SFXPool[ID];
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return NULL;
		}
	}

	ML_BOOL MLSoundFX::SetAllEffetVolume(const ML_FLOAT gain)
	{
		ML_BOOL complete = ML_TRUE;
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			if (!SetEffectVolume(it->first, gain))
			{
				complete = ML_FALSE;
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("SFX All Volume failed on SFX %s.\n"), it->first.c_str());
			}
		}

		return complete;
	}

	ML_BOOL MLSoundFX::SetEffectVolume(const ML_STRING& ID, const ML_FLOAT gain)
	{
		ML_BOOL complete = ML_TRUE;
		if (SFXExists(ID))
		{
			for (ML_UINT i = 0; i < m_SFXPool[ID]->ALSources.size(); i++)
			{
				alSourcef(m_SFXPool[ID]->ALSources[i], AL_MAX_GAIN, gain);
				if (MLAudioManager::ALFailed(__LINE__ - 1))
				{
					complete = ML_FALSE;
					ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
						ML_STR("SFX Volume failed on SFX %s source ID %d.\n"), ID.c_str(), i);
				}
			}

			return complete;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID);
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::SetSource(const ML_STRING& ID, const ML_INT& sourceID, const MLAudioSource& source)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALSources.size() < (ML_UINT)sourceID && sourceID > 0)
			{
				return MLAudioManager::SetSourceSettings(m_SFXPool[ID]->ALSources[sourceID], source);
			}
		}

		return ML_FALSE;
	}

	ML_INT MLSoundFX::Play(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return -1;
			}

			RemoveUnusedSources(m_SFXPool[ID]);
			ALuint buff = 0;
			alGenSources(1, &buff);
			alSourcei(buff, AL_BUFFER, m_SFXPool[ID]->ALBuffer);
			if (buff != 0)
			{
				m_SFXPool[ID]->ALSources.push_back(buff);
				alSourcePlay(buff);
				m_SFXPool[ID]->InUse = ML_TRUE;
				return m_SFXPool[ID]->ALSources.size() - 1;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Could not generate open al buffer for audio.\n"));
				return -1;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return -1;
		}
	}

	ML_INT MLSoundFX::Play(MLSFX* sfx)
	{
		if (sfx->ALBuffer != 0)
		{
			ALuint buff = 0;
			alGenSources(1, &buff);
			alSourcei(buff, AL_BUFFER, sfx->ALBuffer);
			if (buff != 0)
			{
				sfx->ALSources.push_back(buff);
				alSourcePlay(buff);
				sfx->InUse = ML_TRUE;
				return sfx->ALSources.size() - 1;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Could not generate open al buffer for audio.\n"));
				return -1;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return -1;
		}
	}

	ML_INT MLSoundFX::Play(const ML_STRING& ID, const MLAudioSource& source)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return -1;
			}

			RemoveUnusedSources(m_SFXPool[ID]);
			ALuint buff = 0;
			alGenSources(1, &buff);
			MLAudioManager::SetSourceSettings(buff, source);
			alSourcei(buff, AL_BUFFER, m_SFXPool[ID]->ALBuffer);
			if (buff != 0)
			{
				m_SFXPool[ID]->ALSources.push_back(buff);
				alSourcePlay(buff);
				m_SFXPool[ID]->InUse = ML_TRUE;
				return m_SFXPool[ID]->ALSources.size() - 1;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Could not generate open al buffer for audio.\n"));
				return -1;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return -1;
		}
	}

	ML_INT MLSoundFX::Play(MLSFX* sfx, const MLAudioSource& source)
	{
		if (sfx->ALBuffer != 0)
		{
			ALuint buff = 0;
			alGenSources(1, &buff);
			MLAudioManager::SetSourceSettings(buff, source);
			alSourcei(buff, AL_BUFFER, sfx->ALBuffer);
			if (buff != 0)
			{
				sfx->ALSources.push_back(buff);
				alSourcePlay(buff);
				sfx->InUse = ML_TRUE;
				return sfx->ALSources.size() - 1;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Could not generate open al buffer for audio.\n"));
				return -1;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return -1;
		}
	}

	ML_BOOL MLSoundFX::Pause(const ML_STRING& ID, const ML_INT index)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			if (m_SFXPool[ID]->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourcePause(m_SFXPool[ID]->ALSources[index]);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, m_SFXPool[ID]->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::Pause(const MLSFX* sfx, const ML_INT index)
	{
		if (sfx->ALBuffer != 0)
		{
			if (sfx->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourcePause(sfx->ALSources[index]);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, sfx->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::PauseAll(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			for (ML_UINT i = 0; i < m_SFXPool[ID]->ALSources.size(); i++)
			{
				alSourcePause(m_SFXPool[ID]->ALSources[i]);
				return ML_TRUE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::PauseAll(const MLSFX* sfx)
	{
		if (sfx->ALBuffer != 0)
		{
			for (ML_UINT i = 0; i < sfx->ALSources.size(); i++)
			{
				alSourcePause(sfx->ALSources[i]);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::PauseAll()
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			if (it->second->ALSources.size() > 0 && it->second->PCMData != NULL
				&& it->second->ALBuffer != 0)
			{
				for (ML_UINT i = 0; i < it->second->ALSources.size(); i++)
				{
					alSourcePause(it->second->ALSources[i]);
				}
			}
		}

		return ML_TRUE;
	}

	ML_BOOL MLSoundFX::Resume(const ML_STRING& ID, const ML_INT index)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			if (m_SFXPool[ID]->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourcePlay(m_SFXPool[ID]->ALSources[index]);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, m_SFXPool[ID]->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::Resume(const MLSFX* sfx, const ML_INT index)
	{
		if (sfx->ALBuffer != 0)
		{
			if (sfx->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourcePlay(sfx->ALSources[index]);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, sfx->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::ResumeAll(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			for (ML_UINT i = 0; i < m_SFXPool[ID]->ALSources.size(); i++)
			{
				alSourcePlay(m_SFXPool[ID]->ALSources[i]);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::ResumeAll(const MLSFX* sfx)
	{
		if (sfx->ALBuffer != 0)
		{
			for (ML_UINT i = 0; i < sfx->ALSources.size(); i++)
			{
				alSourcePlay(sfx->ALSources[i]);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::ResumeAll()
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			if (it->second->ALSources.size() > 0 && it->second->PCMData != NULL
				&& it->second->ALBuffer != 0)
			{
				for (ML_UINT i = 0; i < it->second->ALSources.size(); i++)
				{
					alSourcePlay(it->second->ALSources[i]);
				}
			}
		}

		return ML_TRUE;
	}

	ML_BOOL MLSoundFX::Stop(const ML_STRING& ID, const ML_INT index)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			if (m_SFXPool[ID]->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourceStop(m_SFXPool[ID]->ALSources[index]);
				RemoveUnusedSources(m_SFXPool[ID]);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, m_SFXPool[ID]->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::Stop(MLSFX* sfx, const ML_INT index)
	{
		if (sfx->ALBuffer != 0)
		{
			if (sfx->ALSources.size() > (ML_UINT)index && index >= 0)
			{
				alSourceStop(sfx->ALSources[index]);
				RemoveUnusedSources(sfx);
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Source index %d was invalid (Current source count %d).\n"), index, sfx->ALSources.size());
				return ML_FALSE;
			}
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}
	}

	ML_BOOL MLSoundFX::StopAll(const ML_STRING& ID)
	{
		if (SFXExists(ID))
		{
			if (m_SFXPool[ID]->ALBuffer == 0)
			{
				ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
					ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
				return ML_FALSE;
			}

			for (ML_UINT i = 0; i < m_SFXPool[ID]->ALSources.size(); i++)
			{
				alSourceStop(m_SFXPool[ID]->ALSources[i]);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Could not find SFX file %s.\n"), ID.c_str());
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::StopAll(MLSFX* sfx)
	{
		if (sfx->ALBuffer != 0)
		{
			for (ML_UINT i = 0; i < sfx->ALSources.size(); i++)
			{
				alSourceStop(sfx->ALSources[i]);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error,
				ML_STR("Audio not loaded into an AL Buffer so cant be bound to a source.\n"));
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLSoundFX::StopAll()
	{
		std::map<ML_STRING, MLSFX*>::iterator it;

		for (it = m_SFXPool.begin(); it != m_SFXPool.end(); it++)
		{
			if (it->second->ALSources.size() > 0 && it->second->PCMData != NULL
				&& it->second->ALBuffer != 0)
			{
				for (ML_UINT i = 0; i < it->second->ALSources.size(); i++)
				{
					alSourceStop(it->second->ALSources[i]);
				}

				RemoveUnusedSources(it->second);
			}
		}

		return ML_TRUE;
	}

	void MLSoundFX::RemoveUnusedSources(MLSFX* sfxFile)
	{
		std::vector<ALuint> sourceList;
		for (ML_UINT i = 0; i < sfxFile->ALSources.size(); i++)
		{
			ALint state;
			alGetSourcei(sfxFile->ALSources[i], AL_SOURCE_STATE, &state);
			if (state == AL_STOPPED)
			{
				alDeleteSources(1, &sfxFile->ALSources[i]);
			}
			else
			{
				sourceList.push_back(sfxFile->ALSources[i]);
			}
		}

		if (sourceList.size() == 0)
		{
			sfxFile->InUse = ML_FALSE;
			sfxFile->ALSources.clear();
		}
		else
		{
			sfxFile->ALSources = sourceList;
		}
	}

	std::vector<MLAudioFrame*> MLSoundFX::DecodeStream(const MLSFX* sfxFile, const MLCodec* sfxCodec, ML_UINT& dataSize)
	{
		AVFrame* currentFrame = av_frame_alloc();
		av_frame_unref(currentFrame);
		AVPacket currentPacket;
		std::vector<MLAudioFrame*> decodedFrames;
		av_init_packet(&currentPacket);

		ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Debug, ML_STR("Decoding Audio stream.\n"));
		while (av_read_frame(sfxCodec->avFmtCtx, &currentPacket) == 0)
		{
			if (currentPacket.stream_index == sfxCodec->avStream->index)
			{
				ML_INT frameComplete = 0;
				ML_INT bytesDecoded = avcodec_decode_audio4(sfxCodec->avCodecCtx, currentFrame, &frameComplete, &currentPacket);
				if (frameComplete)
				{
					MLAudioFrame* decodedAudio = MLAudioManager::ProcessAudioFrame(sfxFile->DecodeFormat, sfxCodec, currentFrame);
					if (decodedAudio == NULL)
					{
						ML_TRACE(ML_LOG_SFX, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to format decoded audio frame.\n"));
						return decodedFrames;
					}
					dataSize += decodedAudio->Size;
					decodedFrames.push_back(decodedAudio);
				}
			}
		}

		av_free(currentFrame);

		return decodedFrames;
	}
}