#ifndef MLGRAPHICTYPES_H
#define MLGRAPHICTYPES_H

#include "MLTexture.h"
#include "MLShader.h"
#include "MLFont.h"

#include <MLMath/MLMath.h>

#define BASE_SHADER ML_STRING(ML_STR("shaders\\SBatch"))
#define BASE_SHADER_VERT_POS ML_STRA("VertexPosition")
#define BASE_SHADER_VERT_COL ML_STRA("VertexColour")
#define BASE_SHADER_VERT_UV	 ML_STRA("TextureCoord")

#define VERTEX_SIZE sizeof(MLRender::MLVertexPosTexCol)
#define DEFAULT_BATCH_SIZE 5000
//5mb of memory for the vertex buffer to work with
//for each orphan block
#define DEFAULT_BUFFER_SIZE 0x500000

//The amount of vertexs for the preset types
#define POINT_SIZE 1
#define LINE_SIZE 2
#define TRI_SIZE 3
#define QUAD_SIZE 6



namespace MLRender
{
	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	enum MLSpriteEffect
	{
		None			= 0,
		FlippedVert		= 1,
		FlippedHoriz	= 2,
		FlippedBoth		= 3
	};

	//=========================================================
	// Name: BatchItem
	// Description: This is used to store batch information about
	//				the graphics system
	// Creator: Chris Millin
	//=========================================================
	struct MLBatchItem
	{
		ML_UINT TextureID;
		ML_INT LayerID;
		ML_UINT VirtualBufferOffset;
		ML_UINT VirtualBufferSize;
		ML_INT DrawMode;
		MLShader* DrawShader;
		UniformCallBack UniformatFunction;

		//Operator members
		ML_BOOL operator<(const MLBatchItem &inBatch);
		ML_BOOL operator==(const MLBatchItem &inBatch);
		ML_BOOL operator!=(const MLBatchItem &inBatch);
	};

	#pragma region MLBatch Members

	inline ML_BOOL MLBatchItem::operator<(const MLBatchItem &inBatch)
	{

		//This massive mess of an if statement is to check
		//if one batch is less than another and is used for
		//sorting batch items since it checks in order of
		//layer, texture ID, drawmode, shader, uniform function
		//so all layer will be grouped together and then in those
		//groups the texture ID's and then in those the draw modes
		//and so on.
		if(this->LayerID == inBatch.LayerID)
		{
			if (this->TextureID == inBatch.TextureID)
			{
				if(this->DrawMode == inBatch.DrawMode)
				{
					if (this->DrawShader == inBatch.DrawShader)
					{
						if (this->UniformatFunction < inBatch.UniformatFunction)
							return ML_TRUE;
						else
							return ML_FALSE;
					}
					else if(this->DrawShader < inBatch.DrawShader)
					{
						return ML_TRUE;
					}
					else
						return ML_FALSE;
				}
				else if(this->DrawMode < inBatch.DrawMode)
				{
					return ML_TRUE;
				}
				else 
					return ML_FALSE;
			}
			else if(this->TextureID < inBatch.TextureID)
			{
				return ML_TRUE;
			}
			else
				return ML_FALSE;
		}
		else if(this->LayerID < inBatch.LayerID)
		{
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	inline ML_BOOL MLBatchItem::operator==(const MLBatchItem &inBatch)
	{
		if (this->TextureID == inBatch.TextureID && this->LayerID == inBatch.LayerID
			&& this->DrawMode == inBatch.DrawMode && this->DrawShader == inBatch.DrawShader
			&& this->UniformatFunction == inBatch.UniformatFunction)
			return ML_TRUE;

		return ML_FALSE;
	}

	inline ML_BOOL MLBatchItem::operator!=(const MLBatchItem &inBatch)
	{
		if (this->TextureID != inBatch.TextureID || this->LayerID != inBatch.LayerID
			|| this->DrawMode != inBatch.DrawMode || this->DrawShader != inBatch.DrawShader
			|| this->UniformatFunction != inBatch.UniformatFunction)
			return ML_TRUE;

		return ML_FALSE;
	}

	#pragma endregion

	//=========================================================
	// Name: Sprite
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLBaseGraphicType
	{
		MLBaseGraphicType();

		MLTexture*	Texture;
	
		ML_INT		Layer;

		MLShader*	Shader;
		UniformCallBack UniformFunction;
	};

	inline MLBaseGraphicType::MLBaseGraphicType()
	{
		Texture		= NULL;
	
		Layer		= 0;

		Shader		= NULL;
		UniformFunction = NULL;
	}

	//=========================================================
	// Name: Sprite
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLSprite : public MLBaseGraphicType
	{
		MLSprite();

		MLVector2f	Position;
		MLVector2f	Size;
		MLVector2f	Scale;
		MLVector2f	Origin;

		MLVector4f	Source;
		MLVector4f	Colour;

		ML_FLOAT	Rotation;
		ML_FLOAT	TextureRotation;

		MLSpriteEffect Effect;

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetSizeFromTexture(const MLTexture* texture = NULL);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetDestinationPosition(const MLVector4f& destination);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetOriginMiddle(const MLTexture* texture = NULL);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetOriginMiddleOfSize();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetSourceFromTexture(const MLTexture* texture = NULL);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		void SetEffect(const MLSpriteEffect& effect);
	};

	#pragma region MLSprite Members

	inline MLSprite::MLSprite()
	{
		Texture		= NULL;

		Position	= MLVector2f::Zero();
		Size		= MLVector2f::Zero();
		Scale		= MLVector2f(1.0f);
		Origin		= MLVector2f::Zero();

		Source		= MLVector4f::Zero();
		Colour		= MLVector4f::Zero();

		Rotation	= 0.0f;
		TextureRotation = 0.0f;
		Layer		= 0;

		Effect = MLSpriteEffect::None;

		Shader		= NULL;
		UniformFunction = NULL;
	}

	inline void MLSprite::SetSizeFromTexture(const MLTexture* texture /* = NULL*/)
	{
		if (texture)
		{
			Size = MLVector2f((ML_FLOAT)texture->GetTextureWidth(), (ML_FLOAT)texture->GetTextureHeight());
		}
		else if(Texture)
		{
			Size = MLVector2f((ML_FLOAT)Texture->GetTextureWidth(), (ML_FLOAT)Texture->GetTextureHeight());
		}
	
	}

	inline void MLSprite::SetDestinationPosition(const MLVector4f& destination)
	{
		Position	= MLVector2f(destination.x, destination.y);
		Size		= MLVector2f(destination.Width, destination.Height);
	}

	inline void MLSprite::SetOriginMiddle(const MLTexture* texture /* = NULL*/)
	{
		if (texture)
		{
			Origin = MLVector2f(-((ML_FLOAT)texture->GetTextureWidth()/2.0f), -((ML_FLOAT)texture->GetTextureHeight()/2.0f));
		}
		else if(Texture)
		{
			Origin = MLVector2f(-((ML_FLOAT)Texture->GetTextureWidth()/2), -((ML_FLOAT)Texture->GetTextureHeight()/2));
		}
	}

	inline void MLSprite::SetOriginMiddleOfSize()
	{
		Origin = MLVector2f(-(Size.Width/2.0f), -(Size.Height/2.0f));
	}

	inline void MLSprite::SetSourceFromTexture(const MLTexture* texture /* = NULL */)
	{
		if (texture)
		{
			Source = MLVector4f(0.0f,0.0f, (ML_FLOAT)texture->GetTextureWidth(), (ML_FLOAT)texture->GetTextureHeight());
		}
		else if (Texture)
		{
			Source = MLVector4f(0.0f,0.0f, (ML_FLOAT)Texture->GetTextureWidth(), (ML_FLOAT)Texture->GetTextureHeight());
		}
	}

	inline void MLSprite::SetEffect(const MLSpriteEffect& effect)
	{
		Effect = effect;
		if (Effect == MLSpriteEffect::FlippedVert)
			Source = Source.zyxw_swizzle;
		else if (Effect == MLSpriteEffect::FlippedHoriz)
			Source = Source.xwzy_swizzle;
		else if (Effect == MLSpriteEffect::FlippedBoth)
			Source = Source.wzyx_swizzle;
	}

	#pragma endregion

	//=========================================================
	// Name: Point primitive
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPoint : public MLBaseGraphicType
	{
		MLPoint();

		MLVector2f	Position;

		MLVector4f	Colour;

		ML_FLOAT		Size;
	};

	inline MLPoint::MLPoint()
	{
		Position	= MLVector2f::Zero();

		Colour		= MLVector4f::Zero();

		Size		= 1;
		Layer		= 0;

		Shader		= NULL;
		UniformFunction = NULL;
	}

	//=========================================================
	// Name: Line primitive
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLLine : public MLBaseGraphicType
	{
		MLLine();

		MLVector2f	Point1;
		MLVector2f	Point2;

		MLVector4f	Colour;

		ML_FLOAT		Width;
	};

	inline MLLine::MLLine()
	{
		Point1		= MLVector2f::Zero();
		Point2		= MLVector2f::Zero();

		Colour		= MLVector4f::Zero();

		Width		= 1;
		Layer		= 0;

		Shader		= NULL;
		UniformFunction = NULL;
	}

	//=========================================================
	// Name: Quad primitive
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLQuad : public MLBaseGraphicType
	{
		MLQuad();

		MLVector2f	Position;
		MLVector2f	Size;
		MLVector2f	Scale;
		MLVector2f	Origin;

		MLVector4f	Colour;

		ML_FLOAT	Rotation;
		ML_INT		LineWidth;
		ML_BOOL		Filled;
	};

	inline MLQuad::MLQuad()
	{
		Position	= MLVector2f::Zero();
		Size		= MLVector2f::Zero();
		Scale		= 1.0f;
		Origin		= MLVector2f::Zero();
		Colour		= MLVector4f::Zero();

		Rotation	= 0.0f;
		LineWidth	= 0;
		Layer		= 0;
		Filled		= ML_TRUE;

		Shader		= NULL;
		UniformFunction = NULL;
	}

	//=========================================================
	// Name: Quad primitive
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	#define DEFAULT_CIRCLE_SEGMENTS 15

	struct MLCircle : public MLBaseGraphicType
	{
		MLCircle();

		MLVector2f	Position;
		MLVector2f	Size;
		MLVector2f	Scale;
		MLVector2f	Origin;

		MLVector4f	Colour;

		ML_FLOAT		Roation;
		ML_INT			Segments;
		ML_INT			LineWidth;
		ML_BOOL			Filled;
	};


	inline MLCircle::MLCircle()
	{
		Position	= MLVector2f::Zero();
		Size		= MLVector2f::Zero();
		Scale		= MLVector2f(1.0f);
		Origin		= MLVector2f::Zero();

		Colour		= MLVector4f::Zero();

		Roation		= 0.0f;
		Segments	= DEFAULT_CIRCLE_SEGMENTS;
		LineWidth	= 0;
		Layer		= 0;
		Filled		= ML_TRUE;

		Shader		= NULL;
		UniformFunction	= NULL;
	};

	//=========================================================
	// Name: Vertex Array
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLVertexArray : public MLBaseGraphicType
	{
		MLVertexArray();

		MLVector2f	Position;
		MLVector2f	Scale;
		MLVector2f	Origin;
		MLVector2f*	VertexData;

		MLVector4f	Colour;

		ML_FLOAT		Rotation;
		ML_FLOAT		TextureRotation;
		ML_INT			VertexCount;
	};

	inline MLVertexArray::MLVertexArray()
	{
		Texture		= NULL;

		Position	= MLVector2f::Zero();
		Scale		= MLVector2f(1.0f);
		Origin		= MLVector2f::Zero();
		VertexData	= NULL;

		Colour		= MLVector4f::Zero();

		Rotation	= 0.0f;
		TextureRotation = 0.0f;

		Layer		= 0;
		VertexCount	= 0;

		Shader		= NULL;
		UniformFunction = NULL;

	};


	enum MLJustifyText
	{
		LeftJustify		= 0,
		CentreJustify	= 1,
		RightJustify	= 2,
	};

	enum MLFontLine
	{
		NoLine		= 0,
		Strike		= 1,
		Underline	= 2,
	};

	//=========================================================
	// Name: Write
	// Description: Font Information for rendering
	// Creator: Chris Millin
	//=========================================================
	struct MLWrite
	{
		MLWrite();

		MLFont*	Font;
		MLJustifyText Alignment;
		MLFontLine LineType;
		ML_FLOAT LineThickness;

		//Text box position offset to allow the font to move in the text box
		MLVector2f  TBoxPositionOffset;
		//Text box size. Position is set by the writer position
		MLVector2f	TBoxSize;
		//Text box text flow. If the text is to stop rendering if it cant fit in
		//of to cut itself to fit being shown in the text box
		ML_BOOL TBoxFlow;

		ML_BOOL TBoxEnabled;

		ML_UINT HighlightStart;
		ML_UINT HighlightEnd;

		MLVector2f	Position;
		MLVector2f	Scale;
		MLVector2f	Origin;
	
		MLVector4f	Colour;

		ML_FLOAT		Rotation;
		ML_INT			Layer;

		MLShader*		Shader;
		UniformCallBack UniformFunction;	
	};

	inline MLWrite::MLWrite()
	{
		Font		= NULL;
		Alignment	= LeftJustify;
		LineType	= NoLine;
		LineThickness = 0.0f;

		TBoxPositionOffset = MLVector2f::Zero();
		//set to max so if a size is never specified it will be so massive it
		//would be near imposable to fill it.
		TBoxSize		   = MLVector2f(FLT_MAX, FLT_MAX); 
		TBoxFlow		   = ML_TRUE;
		TBoxEnabled		   = ML_FALSE;

		HighlightStart	= 0;
		HighlightEnd	= 0;

		Position	= MLVector2f::Zero();
		Scale		= MLVector2f(1.0f);
		Origin		= MLVector2f::Zero();

		Colour		= MLVector4f::Zero();

		Rotation	= 0.0f;
		Layer		= 0;

		Shader		= NULL;
		UniformFunction	= NULL;
	}

}

#endif