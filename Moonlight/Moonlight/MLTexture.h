#ifndef MLTEXTURE_H
#define MLTEXTURE_H

#include "MLIncludes.h"

#include <stdio.h>

#include <gl/glew.h>
#include <FreeImage.h>

#define ML_INVALID_TEXTURE_ID 0

#define COLOUR8_TO_FLOAT(a) (a / 255.0f)
#define FLOAT_TO_COLOUR8(a) (a * 255.0f)
#define INT_TO_COLOUR8(a)	(a * 255)

using namespace MLMath::MLVec4;
using namespace MLMath::MLVec2;
using namespace MLMath::MLStandard;

namespace MLRender
{
	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	enum MLPixelFormat
	{
		RGBA32	= 0, //For 8 bit Red 8 bit Green 8 bit Blue 8 bit Alpha
		RGB24	= 1, //For 8 bit Red 8 bit Green 8 bit Blue 
		BGRA32	= 2, //For Reverse RGBA
		BGR24	= 3, //For Revers RGB
		RedChannel8			= 4, //For Red Channel colour only 8bit
		GreenChannel8		= 5, //For Green Channel colour only 8bit
		BlueChannel8		= 6, //For Blue Channel colour only 8bit
		AlphaChannel8		= 7, //For Alpha Channel only 8bit
		SingleChannel8		= 8, //For Gray scale pixel format 8bit
		LuminanceAlpha16	= 9	 //For 8bit Gray scale and 8bit Alpha 
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPixelRGBA32
	{
		unsigned ML_CHAR R;
		unsigned ML_CHAR G;
		unsigned ML_CHAR B;
		unsigned ML_CHAR A;

		inline MLPixelRGBA32()
		{
			R = 255;
			G = 255;
			B = 255;
			A = 0;
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPixelRGB24
	{
		unsigned ML_CHAR R;
		unsigned ML_CHAR G;
		unsigned ML_CHAR B;

		inline MLPixelRGB24()
		{
			R = 255;
			G = 255;
			B = 255;
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPixelBGRA32
	{
		unsigned ML_CHAR B;
		unsigned ML_CHAR G;
		unsigned ML_CHAR R;
		unsigned ML_CHAR A;

		inline MLPixelBGRA32()
		{
			R = 255;
			G = 255;
			B = 255;
			A = 0;
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPixelBGR24
	{
		unsigned ML_CHAR B;
		unsigned ML_CHAR G;
		unsigned ML_CHAR R;

		inline MLPixelBGR24()
		{
			R = 255;
			G = 255;
			B = 255;
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	struct MLPixelLumiAlpha16
	{
		unsigned ML_CHAR L;
		unsigned ML_CHAR A; 

		inline MLPixelLumiAlpha16()
		{
			L = 255;
			A = 0; 
		}
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	typedef unsigned ML_CHAR MLPixelChannel8;

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* rotateTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format);

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* rotateRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* rotateRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* rotateBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* rotateBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* rotateLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height);
	MLCoreDLL void* rotateChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height);

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* flipVertTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format);

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* flipVertRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipVertRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipVertBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipVertBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipVertLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipVertChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height);

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* flipHorizTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format);

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL void* flipHorizRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipHorizRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipHorizBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipHorizBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipHorizLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height);
	MLCoreDLL void* flipHorizChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height);

	#define DEFAULT_INVALID_SAMPLES -1

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLTexture
	{

		friend class MLTextureManager;

	public:

		MLCoreDLL MLTexture(void);
		MLCoreDLL ~MLTexture(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadTexture(const ML_U8STRING& Path);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadTexture(const MLKernel::MLVirtualFile* Path) { return LoadTexture(MLtoAscii(Path->FullFilePath()).c_str()); }

		//This is all done off the GPU and on CPU
		//To improve performance this could all be done on the FPU
		//The SignDistanceField could be calculated on the GPU as well (might be slower)
		//This should be added at some point

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL StartEditing();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Convertto32Bit();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Convertto24Bit();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ConverttoGrayScale();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL FlipImageVert();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL FlipImageHoriz();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RotateImage();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ConverttoSignDistanceField();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ExportImage(const ML_U8STRING& location);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ExportImage(const ML_U8STRING& location, ML_BYTE* Data, const MLPixelFormat& format);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL EndEditing();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL StoreTexture(const ML_BYTE* data, const ML_INT width, const ML_INT height, const ML_INT imageFormat);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL CreateEmptyTexture(const ML_INT width, const ML_INT height, const ML_INT samples = DEFAULT_INVALID_SAMPLES);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetTextureID()	const { return m_textureID;     }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetTextureWidth() const { return m_textureWidth;  }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetTextureHeight() const { return m_textureHeight; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetWidth(ML_UINT width)	 { m_textureWidth  = width;  }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetHeight(ML_UINT height) { m_textureHeight = height; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_STRING& GetNameID() { return m_nameID; }

	private:

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetNameID(const ML_STRING& name) { m_nameID = name; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		//MLCoreDLL ML_FLOAT CalculateSignDistancePixel(ML_FLOAT* pixelData, ML_INT inX, ML_INT inY, ML_INT width, ML_INT height, ML_FLOAT clamp);

	private:

		//This is used if the image is loaded externally
		//by using this the image can be manipulated using freeimage functionality
		void* m_data; 

		ML_STRING m_nameID;

		//The openGL ID of the texture
		GLuint m_textureID;
	
		//The width of the texture
		ML_UINT m_textureWidth;
		//The height of the texture
		ML_UINT m_textureHeight;

		ML_BOOL m_editing;
		MLPixelFormat m_editFormat;

		GLuint m_textureType;
	};

}
#endif