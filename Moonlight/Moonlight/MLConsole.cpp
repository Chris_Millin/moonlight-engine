#include "MLConsole.h"
#include "MLVirtualPath.h"
#include "MLVirtualFile.h"
#include "MLThread.h"

namespace MLKernel
{
	ML_UINT					MLConsole::m_ignoreFlags;
	Logfn					MLConsole::m_logCallback;
	MLVirtualFile*			MLConsole::m_outLog;
	std::vector<ML_STRING>	MLConsole::m_fullLog;
	HANDLE					MLConsole::m_consoleHandle;
	ML_BOOL					MLConsole::m_initalised;
	ML_BOOL					MLConsole::m_stdConsole;
	ML_STRING				MLConsole::m_threadMutex;

	void MLConsole::InitaliseConsole()
	{
		CoInitialize(nullptr);

		m_initalised = ML_FALSE;
		m_stdConsole = ML_TRUE;
		m_ignoreFlags = 0;
		m_logCallback = NULL;

		MLVirtualPath* path = new MLVirtualPath();
		path->SetPath(ML_STR("Logs"));
		if(!path->IsValid())
		{
			path->Release();
			MLVirtualPath* newPath = path->CreatePath(ML_STR("Logs"));
			path->Release();
			path = newPath;
		}

		ML_STRING Name = ML_STR("Moonlight_Log ");
		Name += GetDateTimeString();
		Name += ML_STR(".txt");
	
		m_consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);


		if(!m_outLog)
		{
			m_outLog = new MLVirtualFile();
			m_outLog->OpenFile(Name, path, MLFileAccess::AppendOnly, ML_FALSE);
		}

		m_threadMutex = ML_STR("Console");

		MLMutexDictionary::AddMutex(m_threadMutex);

		m_initalised = ML_TRUE;
	}

	void MLConsole::Shutdown()
	{
		if(m_initalised)
		{
			MLRELEASE(m_outLog);
			m_logCallback = NULL;
			m_ignoreFlags = 0;
			m_fullLog.clear();
			m_consoleHandle = NULL;
			m_initalised = ML_FALSE;
		}
	}

	void MLConsole::SetIgnoreFlag(const MLConsoleStatus& Status, const ML_BOOL Ignore)
	{
		if(!(m_ignoreFlags & Status) && Ignore)
		{
			m_ignoreFlags |= Status;
		}
		else if((m_ignoreFlags & Status) && !Ignore)
		{
			m_ignoreFlags ^= Status;
		}
	}

	void MLConsole::Log(const ML_U8STRING& file, const ML_U8STRING& function, const ML_UINT& line,
		const ML_STRING& Area, const MLConsoleStatus& Status, const ML_STRING format, ...)
	{
		if(m_initalised && !(m_ignoreFlags & Status))
		{
			MLThread::ThreadWaitMutex(m_threadMutex, ML_MUTEX_TIMEOUT);

			va_list args;
			va_start(args, format);
			ML_UINT size = _vscwprintf(format.c_str(), args) + 1;
			ML_SCHAR* buffer = new ML_SCHAR[size];
			vswprintf(buffer, size, format.c_str(), args);

			ML_UINT statusID = MLMath::MLStandard::MLlog2(Status);

			if(m_logCallback)
				m_logCallback(Area, MLConsoleStatusString[statusID], ML_STRING(buffer));

			ML_STRING loggedString;
			loggedString = ML_STR("[") + Area + ML_STR("] ");
			loggedString += ML_STR("(") + MLConsoleStatusString[statusID] + ML_STR("): ");
		
			if(m_stdConsole)
			{
				ChangeConsoleFontColour((MLConsoleColour)MLConsoleStatusColour[statusID]);
				wprintf_s(ML_STR("%s"), loggedString.c_str());
				if(Status == MLConsoleStatus::Error || Status == MLConsoleStatus::FatalError)
				{
					ML_SCHAR* buffer2 = new ML_SCHAR[ML_MAX_CONSOLE_CHAR];
					swprintf_s(buffer2, ML_MAX_CONSOLE_CHAR, ML_STR("\nFile: %s\nFunction: %s(%d)\n"), MLtoUnicode(file).c_str(), MLtoUnicode(function).c_str(), line);
					loggedString += buffer2;
					wprintf_s(ML_STR("%s"), buffer2);
					delete buffer2;
				}
				ChangeConsoleFontColour(MLConsoleColour::ccWhite);
				wprintf_s(ML_STR("%s"), buffer);
			}

			loggedString += buffer;

			delete buffer;

			m_fullLog.push_back(loggedString);

			if(m_outLog)
				m_outLog->WriteString(loggedString.c_str());

			MLThread::ThreadReleaseMutex(m_threadMutex);
		}
	}

	void MLConsole::Log(const ML_STRING& Area, const MLConsoleStatus& Status, const ML_STRING format, ...)
	{
		if(m_initalised && !(m_ignoreFlags & Status))
		{
			va_list args;
			va_start(args, format);
			ML_UINT size = _vscwprintf(format.c_str(), args) + 1;
			ML_SCHAR* buffer = new ML_SCHAR[size];
			vswprintf(buffer, size, format.c_str(), args);

			ML_UINT statusID = MLMath::MLStandard::MLlog2(Status);

			if(m_logCallback)
				m_logCallback(Area, MLConsoleStatusString[statusID], ML_STRING(buffer));

			ML_STRING loggedString;
			loggedString = ML_STR("[") + Area + ML_STR("] ");
			loggedString += ML_STR("(") + MLConsoleStatusString[statusID] + ML_STR("): ");

			if(m_stdConsole)
			{
				ChangeConsoleFontColour((MLConsoleColour)MLConsoleStatusColour[statusID]);
				wprintf_s(ML_STR("%s"), loggedString.c_str());
				ChangeConsoleFontColour(MLConsoleColour::ccWhite);
				wprintf_s(ML_STR("%s"), buffer);
			}

			loggedString += buffer;

			delete buffer;

			m_fullLog.push_back(loggedString);

			if(m_outLog)
				m_outLog->WriteString(loggedString.c_str());
		}
	}

	void MLConsole::LogStack(const ML_U8STRING& file, const ML_U8STRING& function,
		const ML_UINT& line, const ML_STRING& Area, ML_USHORT frames)
	{
		void*			stack[100];
		SYMBOL_INFO*	symbol;
		HANDLE			process;

		process = GetCurrentProcess();

		SymSetOptions(SYMOPT_LOAD_LINES);

		SymInitialize(process, NULL, TRUE );

		frames               = CaptureStackBackTrace(0, 100, stack, NULL);
		symbol               = (SYMBOL_INFO*)calloc(sizeof(SYMBOL_INFO) + 256 * sizeof(char), 1);
		symbol->MaxNameLen   = 255;
		symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

		IMAGEHLP_LINEW64* linestack = (IMAGEHLP_LINEW64*)malloc(sizeof(IMAGEHLP_LINEW64));
		linestack->SizeOfStruct = sizeof(IMAGEHLP_LINEW64);
		

		Log(file, function, line, Area, MLConsoleStatus::Trace, ML_STR("Stack Trace -\n"));

		ML_UINT counter = 0;

		for(ML_UINT i = 0; i < frames; i++ )
		{
			DWORD ptr;
			if(SymGetLineFromAddrW64(process, (DWORD64)stack[i], &ptr, linestack))
			{
				if (SymFromAddr(process, (DWORD64)stack[i], 0, symbol))
				{
					Log(Area, MLConsoleStatus::Trace, ML_STR("%i: \nFile: %s\nFunction: %s(%d) - 0x%0X\n"),
						i, linestack->FileName, MLtoUnicode(symbol->Name).c_str(), linestack->LineNumber, symbol->Address);
				}
				else
				{
					Log(__FILE__, __FUNCTION__, __LINE__, ML_STR("Console"), MLConsoleStatus::Error, ML_STR("SymFromAddr() generate error: %s"),
						_com_error::HRESULTToWCode(GetLastError())); 
				}
			}
			else
			{
				Log(__FILE__, __FUNCTION__, __LINE__, ML_STR("Console"), MLConsoleStatus::Error, ML_STR("SymGetLineFromAddrW64() generate error: %s"),
					_com_error::HRESULTToWCode(GetLastError())); 
			}

			//if the trace reaches main stop showing the call stack
			if(!strcmp(symbol->Name, "main"))
			{
				break;
			}

			counter++;
		}

		Log(file, function, line, Area, MLConsoleStatus::Trace, ML_STR("Stack Trace Complete (%d/%d frames)\n\n"), counter, frames);

		free(symbol);
	}

	void MLConsole::ChangeConsoleFontColour(const MLConsoleColour& Colour)
	{
		ML_UINT col = Colour;
		col%=16;
		unsigned short wAttributes= ((unsigned)CurrentConsoleBackColour()<<4)|(unsigned)col;
		SetConsoleTextAttribute(m_consoleHandle, wAttributes);
	}

	void MLConsole::ChangeConsoleBackColour(const MLConsoleColour& Colour)
	{
		ML_UINT col = Colour;
		col%=16;
		unsigned short wAttributes= ((unsigned)col<<4)|(unsigned)CurrentConsoleFontColour();
		SetConsoleTextAttribute(m_consoleHandle, wAttributes);
	}

	ML_UINT MLConsole::CurrentConsoleFontColour()
	{
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(m_consoleHandle,&csbi);
		ML_UINT a=csbi.wAttributes;
		return a%16;
	}

	ML_UINT MLConsole::CurrentConsoleBackColour()
	{
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(m_consoleHandle,&csbi);
		ML_UINT a=csbi.wAttributes;
		return (a/16)%16;
	}


	ML_STRING MLConsole::GetDateTimeString()
	{
		MLDateTime dt = MLDateTime::Now();
		ML_SCHAR* buffer = new ML_SCHAR[ML_DATE_TIME_STRING_BUFFER];
		swprintf_s(buffer, ML_DATE_TIME_STRING_BUFFER, ML_STR("%d-%d-%d (%d-%d-%d)"), dt.Day, dt.Month, dt.Year,
			dt.Hour, dt.Minutes, dt.Seconds);

		ML_STRING out = buffer;

		delete buffer;
		return out;
	}
}