#ifndef MLCORE_H
#define MLCORE_H

//These headers are included
//here as they can cause
//compiler issues if not loaded
//in the correct order
//this ensures that they should be
//first loaded
//=======================
#include <windows.h>
#include <string.h>
#include <cstring>
#include <Shlwapi.h>
#include <locale>
#include <codecvt>
#include <stdarg.h>
#include <vector>

#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/wglext.h>
//=======================

#include <wchar.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <MLMath/MLMathDefine.h>
#include <MLMath/MLMath.h>

//=========================================================
// Used in order to build the DLL file in the correct
// way. DLL_BUILD has to be specified in the pre processor
// when building the DLL file and then when including the
// headers to use the DLL file in another project it should
// not be specified in the pre processor
//=========================================================
#ifdef DLL_BUILD
#	define MLCoreDLL  __declspec(dllexport)
#else
#	define MLCoreDLL  __declspec(dllimport)
#endif

//=========================================================
//Since all objects have a release function this is
//useful for some situations but should not be used
//for any double pointer items
//=========================================================
#define MLRELEASE(x) {							\
						if(x)					\
						{						\
							x->Release();		\
							delete x;			\
						}						\
					}							


#define ML_DEFAULT_SCREEN_WIDTH 1280
#define ML_DEFAULT_SCREEN_HEIGHT 720
#define ML_DEFAULT_SCREEN_OFFSET_WIDTH 0
#define ML_DEFAULT_SCREEN_OFFSET_HEIGHT 0
#define ML_DEFAULT_RATIO 16/9

MLCoreDLL extern ML_UINT  MLScreenWidth;
MLCoreDLL extern ML_UINT  MLScreenHeight;
MLCoreDLL extern ML_FLOAT MLScreenRatio;
MLCoreDLL extern ML_INT  MLScreenWidthOffset;
MLCoreDLL extern ML_INT  MLScreenHeightOffset;

//=========================================================
//Helper macro to make it easier to set unicode text
//=========================================================
typedef std::wstring ML_U16STRING;
typedef std::string	 ML_U8STRING;

typedef wchar_t ML_WCHAR;

#define ML_STRW(x) L##x
#define ML_STRA(x) x 



#define ML_EMPTY_STRING ML_STRING(ML_STR(""))
#define ML_EMPTY_STR ML_STR("")

#ifdef UNICODE
	typedef ML_U16STRING ML_STRING;
	typedef wchar_t	ML_SCHAR;
	#define ML_STR(x) ML_STRW(x) 
#else
	typedef ML_U8STRING ML_STRING;
	typedef char ML_SCHAR;
	#define ML_STR(x) ML_STRA(x) 
#endif

//=========================================================
// Name: Next Power of 2
// Description: This function will take a integer value and
// will work out the closest pow of 2 larger than the 
// original value.
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_INT nextPow2(ML_INT inVal)
{
	ML_INT rval=2;
	while(rval<inVal) rval<<=1;
	return rval;
}

//=========================================================
// Name: Random Integer 
// Description: This will obtain a random number between
// the specified min and max values. Make sure the seed is
// set before this function is used.
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_INT randomInt(ML_INT min, ML_INT max)
{	
	if(min > max)
	{
		ML_INT i = min;
		min = max;
		max = i;
	}
	return (ML_INT)((max - min) * rand()/(ML_FLOAT)RAND_MAX + min);
}

//=========================================================
// Name: round
// Description: A basic function to round floating points
// or double fix point.
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_DOUBLE round(ML_DOUBLE x) { return floor(x + 0.5); }

//=========================================================
// Name: File Exists
// Description: checks to see if a file location exists or not
// Windows Only
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_BOOL fileExists(ML_STRING szPath)
{
	DWORD dwAttrib = GetFileAttributesW(szPath.c_str());

	return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
		!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

//=========================================================
// Name: Folder Exists
// Description: checks to see if a path location exists or not
// Windows Only
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_BOOL folderExists(ML_STRING Path)
{
	if(PathFileExistsW(Path.c_str()) > FALSE)
		return ML_TRUE;
	else
		return ML_FALSE;
}


//=========================================================
// Name: String Unicode to Unicode
// Description: Pass through to ensure that if all non unicode
// strings are changed to unicode this function will be valid
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_U16STRING& MLtoUnicode(ML_U16STRING& inString)
{
	return inString;
}

//=========================================================
// Name: String Ascii to Unicode
// Description: Convert a string to a unicode string
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_U16STRING MLtoUnicode(const ML_U8STRING& inString)
{
	std::wstring_convert<std::codecvt_utf8_utf16<ML_WCHAR>> converter;
	ML_U16STRING wideString = converter.from_bytes(inString);
	return wideString;
}

//=========================================================
// Name: String Unicode to Ascii 
// Description: Convert a unicode string to a ascii string
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_U8STRING MLtoAscii(const ML_U16STRING& inString)
{
	std::wstring_convert<std::codecvt_utf8_utf16<ML_WCHAR>> converter;
	ML_U8STRING u8Str = converter.to_bytes(inString);
	return u8Str;
}

//=========================================================
// Name: String Ascii to Ascii 
// Description: Pass through to ensure that if all unicode
// strings are changed to ascii this function will be valid
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline ML_U8STRING& MLtoAscii(ML_U8STRING& inString)
{
	return inString;
}

//=========================================================
// Name: Split String Ascii 
// Description: will split a Ascii code string to separate
// components based on the key to split with.
// Creator: Chris Millin
//=========================================================
MLCoreDLL inline std::vector<ML_U8STRING> MLsplitString(const ML_U8STRING& input, const ML_CHAR& key)
{
	std::vector<ML_U8STRING> result;
	ML_U8STRING buf = "";
	ML_UINT i = 0;
	while (i < input.length())
	{
		if (input[i] != key)
			buf += input[i];
		else if (buf.length() > 0) 
		{
			result.push_back(buf);
			buf = "";
		}
		i++;
	}
	if (!buf.empty())
		result.push_back(buf);

	return result;
}

#endif //MLRENDER_H