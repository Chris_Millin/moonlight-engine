#include "MLRandom.h"

namespace MLKernel
{
	//////////////////////////////////////////////////////////////////////////
	#pragma region MLCRand

	ML_INT MLCRand::RandInt()
	{
		return RandInt(-RAND_MAX/2, RAND_MAX/2);
	}

	ML_INT MLCRand::RandInt(const ML_INT max)
	{
		return RandInt(0, RAND_MAX);
	}

	ML_INT MLCRand::RandInt(ML_INT min, ML_INT max)
	{
		if(max == ML_RAND_MAX) max -= 1;
		if(min > max)
		{
			ML_INT i = min;
			min = max;
			max = i;
		}
		ML_INT range = (max + 1) - min;
		return (rand() % range) + min;
	}

	ML_UINT MLCRand::RandUInt()
	{
		return RandUInt(0, ML_RAND_MAX);
	}

	ML_UINT MLCRand::RandUInt(ML_UINT max)
	{
		return RandUInt(0, max);
	}

	ML_UINT MLCRand::RandUInt(ML_UINT min, ML_UINT max)
	{
		if(max == ML_RAND_MAX) max -= 1;
		if(min > max)
		{
			ML_UINT i = min;
			min = max;
			max = i;
		}
		ML_UINT range = (max + 1) - min;
		return (rand() % range) + min;
	}

	ML_FLOAT MLCRand::RandFloat()
	{
		return RandFloat(-FLT_MAX, FLT_MAX);
	}

	ML_FLOAT MLCRand::RandFloat(ML_FLOAT max)
	{
		return RandFloat(0, max);
	}

	ML_FLOAT MLCRand::RandFloat(ML_FLOAT min, ML_FLOAT max)
	{
		if(min > max)
		{
			ML_FLOAT i = min;
			min = max;
			max = i;
		}
		return (ML_FLOAT)((max - min) * (ML_FLOAT)rand()/(ML_FLOAT)RAND_MAX + min);
	}

	MLCoreDLL ML_INT MLCRand::PeekNextInt()
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_INT MLCRand::PeekNextInt(ML_INT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_INT MLCRand::PeekNextInt(ML_INT min, ML_INT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_UINT MLCRand::PeekNextUInt() 
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_UINT MLCRand::PeekNextUInt(ML_UINT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_UINT MLCRand::PeekNextUInt(ML_UINT min, ML_UINT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_FLOAT MLCRand::PeekNextFloat()
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_FLOAT MLCRand::PeekNextFloat(ML_FLOAT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	MLCoreDLL ML_FLOAT MLCRand::PeekNextFloat(ML_FLOAT min, ML_FLOAT max)
	{
		ML_TRACE( ML_STR("Rand"), MLKernel::MLConsoleStatus::Warning, 
			ML_STR("MLCRand uses standard c rand() function and is not capable of peeking. Use a different random class if you wish to peek\n"));
		return RAND_MAX;
	}

	#pragma endregion		MLCRand
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	#pragma region MLMWCRand

	MLMWCRand::MLMWCRand(void)
	{
		seedX = 0x12345678;
		seedY = 0x12345678;
	}

	void MLMWCRand::Seed(ML_UINT seed)
	{
		seedX = seed * 2;
		seedY = seed;

		if(seedX == 0)
			seedX = 0x12345678;
		else if(seedX == 0x464fffff)
			seedX = seedX & seedX;

		if(seedY == 0)
			seedY = 0x12345678;
		else if(seedY == 0x9068ffff)
			seedY = seedY & seedY;
	}

	ML_UINT MLMWCRand::Rand()
	{
		seedX = 36969 * (seedX & 65535) + (seedX >> 16);
		seedY = 18000 * (seedY & 65535) + (seedY >> 16);
		return ((seedX << 16) + seedY);
	}

	ML_INT MLMWCRand::RandInt()
	{
		return RandInt(INT_MIN, INT_MAX);
	}

	ML_INT MLMWCRand::RandInt(ML_INT max)
	{
		return RandInt(0, max);
	}

	ML_INT MLMWCRand::RandInt(ML_INT min, ML_INT max)
	{
		if(max == INT_MAX) max -= 1;
		if(min > max)
		{
			ML_INT i = min;
			min = max;
			max = i;
		}
		ML_INT range = (max + 1) - min;
		return (Rand() % range) + min;  
	}

	ML_UINT MLMWCRand::RandUInt()
	{
		return RandUInt(0, UINT_MAX);
	}

	ML_UINT MLMWCRand::RandUInt(ML_UINT max)
	{
		return RandUInt(0, max);
	}

	ML_UINT MLMWCRand::RandUInt(ML_UINT min, ML_UINT max)
	{
		if(max == UINT_MAX) max -= 1;
		if(min > max)
		{
			ML_UINT i = min;
			min = max;
			max = i;
		}
		ML_UINT range = (max + 1) - min;
		return (Rand() % range) + min;  
	}

	ML_FLOAT MLMWCRand::RandFloat()
	{
		return RandFloat(-FLT_MAX, FLT_MAX);
	}

	ML_FLOAT MLMWCRand::RandFloat(ML_FLOAT max)
	{
		return RandFloat(0, max);
	}

	ML_FLOAT MLMWCRand::RandFloat(ML_FLOAT min, ML_FLOAT max)
	{
		if(min > max)
		{
			ML_FLOAT i = min;
			min = max;
			max = i;
		}
		return (ML_FLOAT)((max - min) * (ML_FLOAT)Rand()/(ML_FLOAT)ML_RAND_MAX + min);  
	}

	MLCoreDLL ML_INT MLMWCRand::PeekNextInt()
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_INT random = RandInt();
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_INT MLMWCRand::PeekNextInt(ML_INT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_INT random = RandInt(max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_INT MLMWCRand::PeekNextInt(ML_INT min, ML_INT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_INT random = RandInt(min, max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_UINT MLMWCRand::PeekNextUInt() 
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_UINT random = RandUInt();
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_UINT MLMWCRand::PeekNextUInt(ML_UINT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_UINT random = RandUInt(max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_UINT MLMWCRand::PeekNextUInt(ML_UINT min, ML_UINT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_UINT random = RandUInt(min, max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_FLOAT MLMWCRand::PeekNextFloat()
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_FLOAT random = RandFloat();
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_FLOAT MLMWCRand::PeekNextFloat(ML_FLOAT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_FLOAT random = RandFloat(max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	MLCoreDLL ML_FLOAT MLMWCRand::PeekNextFloat(ML_FLOAT min, ML_FLOAT max)
	{
		ML_UINT seedLastX = seedX;
		ML_UINT seedLastY = seedY;
		ML_FLOAT random = RandFloat(min, max);
		seedX = seedLastX;
		seedY = seedLastY;
		return random;
	}

	#pragma endregion	MLMWCRand
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	#pragma region MLTwisterRand

	MLTwisterRand::MLTwisterRand(void)
	{
		Seed(0x12345678);
	}

	void MLTwisterRand::Seed(ML_UINT seed)
	{
		index = 0;
		r[0] = seed & 0xffffffffUL;
		for(ML_UINT i = 1; i < 623; i++)
		{
			ML_QWORD high = (1812433253 * (r[i-1] ^ ((r[i-1]) >> 30)) + i);
			r[i] = high & 0xffffffff;
		}
		index++;
	}

	ML_UINT MLTwisterRand::Rand()
	{
		if(index == 0)
			BuildNumbers();

		ML_UINT y = r[index];
		y = y ^ (y >> 11);
		y = y ^ (y << 7) & (2636928640); // 0x9d2c5680
		y = y ^ (y << 15) & (4022730752); // 0xefc60000
		y = y ^ (y >> 18);

		index = (index + 1) % 624;
		return y;
	}

	void MLTwisterRand::BuildNumbers()
	{
		for(ML_UINT i = 0; i < 623; i++) 
		{
			int y = (r[i] & 0x80000000)	+ (r[(i+1) % 624] & 0x7fffffff);
			r[i] = r[(i + 397) % 624] ^ (y >> 1);
			if ((y % 2) != 0) 
			{
				r[i] = r[i] ^ (2567483615); // 0x9908b0df
			}
		}
	}

	ML_INT MLTwisterRand::RandInt()
	{
		return RandInt(INT_MIN, INT_MAX);
	}

	ML_INT MLTwisterRand::RandInt(ML_INT max)
	{
		return RandInt(0, max);
	}

	ML_INT MLTwisterRand::RandInt(ML_INT min, ML_INT max)
	{
		if(max == INT_MAX) max -= 1;
		if(min > max)
		{
			ML_INT i = min;
			min = max;
			max = i;
		}
		ML_INT range = (max + 1) - min;
		return (Rand() % range) + min;
	}

	ML_UINT MLTwisterRand::RandUInt()
	{
		return RandUInt(0, UINT_MAX);
	}

	ML_UINT MLTwisterRand::RandUInt(ML_UINT max)
	{
		return RandUInt(0, max);
	}

	ML_UINT MLTwisterRand::RandUInt(ML_UINT min, ML_UINT max)
	{
		if(max == UINT_MAX) max -= 1;
		if(min > max)
		{
			ML_UINT i = min;
			min = max;
			max = i;
		}
		ML_UINT range = (max + 1) - min;
		return (Rand() % range) + min;
	}

	ML_FLOAT MLTwisterRand::RandFloat()
	{
		return RandFloat(-FLT_MAX, FLT_MAX);
	}

	ML_FLOAT MLTwisterRand::RandFloat(ML_FLOAT max)
	{
		return RandFloat(0, max);
	}

	ML_FLOAT MLTwisterRand::RandFloat(ML_FLOAT min, ML_FLOAT max)
	{
		if(min > max)
		{
			ML_FLOAT i = min;
			min = max;
			max = i;
		}
		return (ML_FLOAT)((max - min) * (ML_FLOAT)Rand()/(ML_FLOAT)ML_RAND_MAX + min); 
	}

	MLCoreDLL ML_INT MLTwisterRand::PeekNextInt()
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;
		
		ML_UINT preRandNum = r[oldindex];
		ML_INT randNumber = RandInt();
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_INT MLTwisterRand::PeekNextInt(ML_INT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_INT randNumber = RandInt(max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_INT MLTwisterRand::PeekNextInt(ML_INT min, ML_INT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_INT randNumber = RandInt(min, max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_UINT MLTwisterRand::PeekNextUInt() 
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_UINT randNumber = RandUInt();
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_UINT MLTwisterRand::PeekNextUInt(ML_UINT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_UINT randNumber = RandUInt(max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_UINT MLTwisterRand::PeekNextUInt(ML_UINT min, ML_UINT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_UINT randNumber = RandUInt(min, max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_FLOAT MLTwisterRand::PeekNextFloat()
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_FLOAT randNumber = RandFloat();
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_FLOAT MLTwisterRand::PeekNextFloat(ML_FLOAT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_FLOAT randNumber = RandFloat(max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	MLCoreDLL ML_FLOAT MLTwisterRand::PeekNextFloat(ML_FLOAT min, ML_FLOAT max)
	{
		ML_UINT oldindex = 1;

		if(index == 0)
			BuildNumbers();
		else
			oldindex = index;

		ML_UINT preRandNum = r[oldindex];
		ML_FLOAT randNumber = RandFloat(min, max);
		r[oldindex] = preRandNum;
		index = oldindex;
		return randNumber;
	}

	#pragma endregion MLTwisterRand
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	#pragma region MLPseudoChance

	MLPseudoChance::MLPseudoChance(void)
	{
		m_count = 1;
		m_lastCount = 1;
		MLCRand* cRandObj = new MLCRand();
		m_randMethod = (MLRandom*)cRandObj;
	}

	MLPseudoChance::MLPseudoChance(MLRandomType rand)
	{
		m_count = 1;
		m_lastCount = 1;
		SetMethod(rand);
	}

	MLPseudoChance::~MLPseudoChance(void)
	{
		Release();
	}

	void MLPseudoChance::Release()
	{
		if(m_randMethod)
		{
			delete m_randMethod;
			m_randMethod = NULL;
		}
	}

	void MLPseudoChance::SetRandSeed(ML_UINT seed)
	{
		if (m_randMethod)
		{
			m_randMethod->Seed(seed);
		}
	}

	void MLPseudoChance::SetMethod(MLRandomType rand)
	{
		if(m_randMethod)
		{
			Release();
		}

		switch(rand)
		{
		case MLRandomType::CRandom :
			{
				MLCRand* cRandObj = new MLCRand();
				m_randMethod = (MLRandom*)cRandObj;
				break;
			}
		case MLRandomType::MultiplyWithCarry :
			{
				MLMWCRand* mwcRandObj = new MLMWCRand();
				m_randMethod = (MLRandom*)mwcRandObj;
				break;
			}
		case MLRandomType::TwisterRandom :
			{
				MLTwisterRand* twisterRandObj = new MLTwisterRand();
				m_randMethod = (MLRandom*)twisterRandObj;
				break;
			}
		}
	}

	void MLPseudoChance::SetBaseChance(ML_FLOAT chance, ML_BOOL usePreCalc)
	{
		if(chance > 100.0f)	chance = 100.0f;
		if(chance < 0.0f)	chance = 0.0f;

		m_baseChance = chance;

		if(chance > 0.0f && chance < 100.0f)
		{		
			if(usePreCalc)
			{
				m_constant = MLPseudoRandDistLut[(ML_UINT)(chance/5.0f)];
			}
			else
			{
				m_constant = (ML_FLOAT)CfromP((chance/100.0f));
			}
		}
	}
	
	ML_BOOL MLPseudoChance::IsProc()
	{
		if(IsProc(m_count))
		{
			m_lastCount = m_count;
			m_count = 1;
			return ML_TRUE;
		}
		else
		{
			m_lastCount = m_count;
			m_count++;
			return ML_FALSE;
		}
	}

	ML_BOOL MLPseudoChance::IsProc(ML_UINT count)
	{
		ML_UINT rand = m_randMethod->RandInt(0, 100);
		m_lastRand = rand;
		ML_FLOAT result = (m_constant * count) * 100.0f;
		if(result >= (ML_FLOAT)rand)
			return ML_TRUE;
		else
			return ML_FALSE;
	}

	MLCoreDLL ML_DOUBLE MLPseudoChance::CfromP(ML_DOUBLE p)
	{
		ML_DOUBLE Cupper = p;
		ML_DOUBLE Clower = 0;
		ML_DOUBLE Cmid;
		ML_DOUBLE p1;
		ML_DOUBLE p2 = 1;
		while(true)
		{
			Cmid = ( Cupper + Clower ) / 2;
			p1 = PfromC( Cmid );

			if (abs(p1 - p2) <= 0) 
				break;

			if ( p1 > p )
				Cupper = Cmid;
			else
				Clower = Cmid;

			p2 = p1;
		}

		return Cmid;
	}

	MLCoreDLL ML_DOUBLE MLPseudoChance::PfromC(ML_DOUBLE c)
	{
		ML_DOUBLE pProcOnN = 0;
		ML_DOUBLE pProcByN = 0;
		ML_DOUBLE sumNpProcOnN = 0;

		int maxFails = (int)ceil( 1 / c );

		for (int N = 1; N <= maxFails; ++N)
		{
			pProcOnN = min(1, N * c) * (1 - pProcByN);
			pProcByN += pProcOnN;
			sumNpProcOnN += N * pProcOnN;
		}

		return (1 / sumNpProcOnN);
	}


	#pragma endregion MLPesudoChance
	//////////////////////////////////////////////////////////////////////////
}