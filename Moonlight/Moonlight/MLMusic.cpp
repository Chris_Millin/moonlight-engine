#include "MLMusic.h"
#include "MLAudioThread.h"

namespace MLAudio
{
	MLAudioThread*  MLMusic::m_thread;

	MLMusic::MLMusic(void) 
	{
		m_playing = ML_FALSE;
		m_resetPlaying = ML_FALSE;
	}

	MLMusic::~MLMusic(void)
	{
		Release();
	}

	ML_BOOL MLMusic::Release()
	{
		m_thread->RemoveMusic(this);

		ML_BOOL success = ML_TRUE;
		MLAudioManager::ReleaseCodec(m_codec);
		if (m_codec)
		{
			ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to release codec object.\n"));
			success = ML_FALSE;
		}
		return success;
	}

	ML_BOOL MLMusic::LoadFile(const ML_STRING& file)
	{
		MLKernel::MLVirtualFile* vfile = new MLKernel::MLVirtualFile(file);
		bool result = LoadFile(vfile);
		MLRELEASE(vfile);
		return result;
	}

	ML_BOOL MLMusic::LoadFile(const MLKernel::MLVirtualFile* file)
	{
		if (m_thread == NULL)
		{
			m_thread = new MLAudioThread();
			m_thread->Intialise();
		}

		m_codec = MLAudioManager::LoadAudioFile(file);
		if (!m_codec)
		{
			ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to load audio file with FFMPEG.\n"));
			return ML_FALSE;
		}

		ML_DOUBLE divDuration = (1.0 / (ML_DOUBLE)av_q2d(m_codec->avStream->time_base));

		m_loadedFilePath = file->FullFilePath();

		m_frequency		= m_codec->avCodecCtx->sample_rate;
		m_channels		= m_codec->avCodecCtx->channels;
		m_bitRate		= m_codec->avCodecCtx->bit_rate;
		m_channelFormat = 0;
		m_decodeFormat	= m_codec->avCodecCtx->sample_fmt;
		m_duration		= m_codec->avStream->duration / divDuration;

		switch (m_channels)
		{
		case 1:
			m_channelFormat = AL_FORMAT_MONO16;
			break;
		case 2:
			m_channelFormat = AL_FORMAT_STEREO16;
			break;
		case 4:
			if (alIsExtensionPresent("AL_EXT_MCFORMATS"))
			{
				m_channelFormat = alGetEnumValue("AL_FORMAT_QUAD16");
			}
			break;
		case 6:
			if (alIsExtensionPresent("AL_EXT_MCFORMATS"))
			{
				m_channelFormat = alGetEnumValue("AL_FORMAT_51CHN16");
			}
			break;
		default:
			ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Channel format not supported by openAL.\n"));
			return ML_FALSE;
		}

		alGenBuffers(ML_MAX_ALBUFFER_STREAM, m_ALBuffer);
		alGenSources(1, &m_ALSource);

		m_thread->AddMusic(this);

		return ML_TRUE;
	}

	ML_STRING MLMusic::DurationString() const
	{
		ML_UINT minutes = (ML_UINT)(m_duration / 60);
		ML_UINT minutes_scale = minutes % 60;
		ML_UINT seconds_scale = (ML_UINT)m_duration % 60;

		ML_SCHAR* buffer = new ML_SCHAR[20];

		wsprintf(buffer, ML_STR("%dm%ds"), minutes_scale, seconds_scale);

		ML_STRING result = ML_STRING(buffer);
		delete[] buffer;

		return result;
	}

	ML_BOOL MLMusic::SetSource(MLAudioSource& source)
	{
		if (source.looping) source.looping = ML_FALSE;
		return MLAudioManager::SetSourceSettings(m_ALSource, source);
	}

	ML_BOOL MLMusic::SetGlobalMusicVolume(const ML_FLOAT gain)
	{
		return m_thread->SetMusicVolume(gain);
	}

	ML_BOOL MLMusic::SetMusicVolume(const ML_FLOAT gain)
	{
		ALError(alSourcef(m_ALSource, AL_MIN_GAIN, gain));
		ALError(alSourcef(m_ALSource, AL_MAX_GAIN, gain));

		return ML_TRUE;
	}

	ML_BOOL MLMusic::Play()
	{
		if (m_playing)
			return ML_TRUE;

		for (ML_INT i = 0; i < ML_MAX_ALBUFFER_STREAM; i++)
		{
			m_ALCurrentBuffer = m_ALBuffer[i];
			StreamAudio();
		}

		alSourceQueueBuffers(m_ALSource, ML_MAX_ALBUFFER_STREAM, m_ALBuffer);

		alSourcePlay(m_ALSource);

		m_playing = ML_TRUE;
		m_resetPlaying = ML_FALSE;
		return ML_TRUE;
	}

	ML_BOOL MLMusic::Play(MLAudioSource& source)
	{
		if (source.looping) source.looping = ML_FALSE;
		MLAudioManager::SetSourceSettings(m_ALSource, source);

		if (m_playing)
			return ML_TRUE;

		alSourceUnqueueBuffers(m_ALSource, ML_MAX_ALBUFFER_STREAM, m_ALBuffer);

		for (ML_INT i = 0; i < ML_MAX_ALBUFFER_STREAM; i++)
		{
			m_ALCurrentBuffer = m_ALBuffer[i];
			StreamAudio();
		}

		alSourceQueueBuffers(m_ALSource, ML_MAX_ALBUFFER_STREAM, m_ALBuffer);

		alSourcePlay(m_ALSource);
		m_resetPlaying = ML_FALSE;
		m_playing = ML_TRUE;

		return ML_TRUE;
	}

	ML_BOOL MLMusic::Pause()
	{
		m_playing = ML_FALSE;
		alSourcePause(m_ALSource);

		return ML_TRUE;
	}

	ML_BOOL MLMusic::Resume()
	{
		if (m_playing)
			return ML_TRUE;

		alSourcePlay(m_ALSource);
		m_playing = ML_TRUE;

		return ML_TRUE;
	}

	ML_BOOL MLMusic::Stop()
	{
		m_playing = ML_FALSE;

		alSourceStop(m_ALSource);

		return ML_TRUE;
	}

	ML_BOOL MLMusic::ResetStream()
	{
		int64_t timeBase = (int64_t(m_codec->avCodecCtx->time_base.num) * AV_TIME_BASE) / int64_t(m_codec->avCodecCtx->time_base.den);
		int64_t seekTarget = int64_t(0) * timeBase;

		if (av_seek_frame(m_codec->avFmtCtx, m_codec->avStream->index, 0, AVSEEK_FLAG_BACKWARD) < 0)
		{
			ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Failed set position back to 0\n"));
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Debug, ML_STR("Track Resetting\n"));

		return ML_TRUE;
	}

	ML_BOOL MLMusic::Update()
	{
		ML_INT bufferProc = 0;
		ALint state = 0;

		//if the audio is playing then need to check to see if it needs updating
		//and that the file is of buffered audio type
		if (m_playing)
		{
			ALint state;
			alGetSourcei(m_ALSource, AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
				alSourcePlay(m_ALSource);

			//obtain the processed buffers
			alGetSourcei(m_ALSource, AL_BUFFERS_PROCESSED, &bufferProc);

			//loop though all the completed buffers and start to process 
			while (bufferProc)
			{
				m_ALCurrentBuffer = 0;
				alSourceUnqueueBuffers(m_ALSource, 1, &m_ALCurrentBuffer);
				if (!StreamAudio())
				{
					ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Failed reading AVFrame. STOPPING AUDIO FILE\n"));
					Stop();
				}
				else
				{
					alSourceQueueBuffers(m_ALSource, 1, &m_ALCurrentBuffer);
				}

				bufferProc--;
			}

			bufferProc = 0;
		}

		return ML_TRUE;
	}
	
	ML_BOOL MLMusic::StreamAudio(ML_BOOL returnFaltyFrame)
	{
		ML_INT err = 0;
		//ALenum alError;
		ML_INT frameComplete = 0;
		ML_INT encBytesProcessed = 0;
		ML_INT dataSize = 0;
		AVPacket packet;
		AVFrame* decodedFrame = NULL;

		av_init_packet(&packet);

		err = av_read_frame(m_codec->avFmtCtx, &packet);
		if (err)
		{
			//if an error occurs it likely means one of two things
			//something went wrong or there are no more frames to process
			//hopefully the second option is true so reset the stream to 0
			//and try again. If that fails something went wrong
			if(!returnFaltyFrame) ResetStream();
			if (!StreamAudio(ML_TRUE))
				return ML_FALSE;
			else
				return ML_TRUE;
		}

		while (!frameComplete)
		{
			if (!decodedFrame)
			{
				if (!(decodedFrame = av_frame_alloc()))
				{
					ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Run out of memory, stop the streaming\n"));
					return ML_FALSE;
				}
			}
			else
			{
				av_frame_unref(decodedFrame);
			}

			encBytesProcessed += avcodec_decode_audio4(m_codec->avCodecCtx, decodedFrame, &frameComplete, &packet);

			if (frameComplete)
			{
				MLAudioFrame* decodedAudio = MLAudioManager::ProcessAudioFrame(m_decodeFormat, m_codec, decodedFrame);

				if (decodedAudio)
				{
					alBufferData(m_ALCurrentBuffer, m_channelFormat, decodedAudio->Data, decodedAudio->Size * 2, m_frequency);
					if (decodedAudio->Data)
					{
						delete[] decodedAudio->Data;
						decodedAudio->Data = NULL;
					}

					delete decodedAudio;
				}
				else
				{
					av_free(decodedFrame);
					ML_TRACE(ML_LOG_MUSIC, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to process audio frame\n"));
					return ML_FALSE;
				}
				av_free(decodedFrame);
			}
		}

		return ML_TRUE;
	}
}