#ifndef MLRANDOM_H
#define MLRANDOM_H

#include "MLCore.h"
#include "MLConsole.h"

#include <random>

#define ML_RAND_MAX 0xffffffff

namespace MLKernel
{
	//=========================================================
	// Name: Random
	// Description: Abstract class to provide a generic rand
	// method
	// Creator: Chris Millin
	//=========================================================
	class MLRandom
	{
	public:

		MLCoreDLL virtual ~MLRandom(void) {};

		MLCoreDLL virtual void Seed(ML_UINT seed) = 0;

		MLCoreDLL virtual ML_INT RandInt() = 0;
		MLCoreDLL virtual ML_INT RandInt(ML_INT max) = 0;
		MLCoreDLL virtual ML_INT RandInt(ML_INT min, ML_INT max) = 0;
		MLCoreDLL virtual ML_UINT RandUInt() = 0;
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT max) = 0;
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT min, ML_UINT max) = 0;
		MLCoreDLL virtual ML_FLOAT RandFloat() = 0;
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT max) = 0;
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT min, ML_FLOAT max) = 0;

		MLCoreDLL virtual ML_INT PeekNextInt() = 0;
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT max) = 0;
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT min, ML_INT max) = 0;
		MLCoreDLL virtual ML_UINT PeekNextUInt() = 0;
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT max) = 0;
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT min, ML_UINT max) = 0;
		MLCoreDLL virtual ML_FLOAT PeekNextFloat() = 0;
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT max) = 0;
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT min, ML_FLOAT max) = 0;
	};


	//=========================================================
	// Name: C Random
	// Description: C style Random using the C random library
	// Peek is not available on this method (need to get the
	// source to change that)
	// Creator: Chris Millin
	//=========================================================
	class MLCRand : virtual public MLRandom
	{
	public:
		MLCoreDLL MLCRand(void) {};
		MLCoreDLL virtual ~MLCRand(void) {};

		//=========================================================
		// Name: Seed
		// Description: Will seed the random function. Recommended
		// to use a detailed time value for this like file time
		// low value.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Seed(ML_UINT seed) { srand(seed); }

		//=========================================================
		// Name: Random Int
		// Description: Will random a integer value. Can choose
		// to set the min and max. single max function will gen
		// from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT RandInt();
		MLCoreDLL virtual ML_INT RandInt(ML_INT max);
		MLCoreDLL virtual ML_INT RandInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Random Unsigned Int
		// Description: Will random a unsigned integer value. Can 
		// choose to set the min and max. single max function 
		// will gen from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT RandUInt();
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Random Float
		// Description: Will random a floating point value. Can
		// choose to set the min and max. Single max function
		// will gen from 0. not value function will gen from
		// FTL_MAX to -FTL_MAX so will be unlikely to gen small
		// numbers (not recommended)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT RandFloat();
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT	RandFloat(ML_FLOAT min, ML_FLOAT max);

		//=========================================================
		// Name: Peek At Next Int
		// Description: Not Implemented
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT PeekNextInt();
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT max);
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Peek At Next UInt
		// Description: Not Implemented
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT PeekNextUInt();
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Peek At Next Float
		// Description: Not Implemented
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT PeekNextFloat();
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT min, ML_FLOAT max);
	};


	//=========================================================
	// Name: Multiply With Carry Random
	// Description: Will use the multiply with carry method
	// to generate the random value.
	// Creator: Chris Millin
	//=========================================================
	class MLMWCRand : virtual public MLRandom
	{
	public:
		MLCoreDLL MLMWCRand(void);
		MLCoreDLL virtual ~MLMWCRand(void) {}

		//=========================================================
		// Name: Seed
		// Description: Will seed the random function. Recommended
		// to use a detailed time value for this like file time
		// low value.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Seed(ML_UINT seed);

		//=========================================================
		// Name: Random Int
		// Description: Will random a integer value. Can choose
		// to set the min and max. single max function will gen
		// from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT RandInt();
		MLCoreDLL virtual ML_INT RandInt(ML_INT max);
		MLCoreDLL virtual ML_INT RandInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Random Unsigned Int
		// Description: Will random a unsigned integer value. Can 
		// choose to set the min and max. single max function 
		// will gen from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT RandUInt();
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Random Float
		// Description: Will random a floating point value. Can
		// choose to set the min and max. Single max function
		// will gen from 0. not value function will gen from
		// FTL_MAX to -FTL_MAX so will be unlikely to gen small
		// numbers (not recommended)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT RandFloat();
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT	RandFloat(ML_FLOAT min, ML_FLOAT max);

		//=========================================================
		// Name: Peek At Next Int
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT PeekNextInt();
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT max);
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Peek At Next UInt
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT PeekNextUInt();
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Peek At Next Float
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT PeekNextFloat();
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT min, ML_FLOAT max);

	private:

		//=========================================================
		// Name: Random
		// Description: will generate the random number 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT Rand();

	private:

		//Seed 1 for mwc random
		ML_UINT seedX;
		//Seed 2 for mwc random
		ML_UINT seedY;
	};

	//=========================================================
	// Name: Mersenne Twister Random
	// Description: Will use the Mersenne Twister method
	// to generate the random value.
	// Creator: Chris Millin
	//=========================================================
	class MLTwisterRand : virtual public MLRandom
	{
	public:
		MLCoreDLL MLTwisterRand(void);
		MLCoreDLL virtual ~MLTwisterRand(void) {}

		//=========================================================
		// Name: Seed
		// Description: Will seed the random function. Recommended
		// to use a detailed time value for this like file time
		// low value.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Seed(ML_UINT seed);

		//=========================================================
		// Name: Random Int
		// Description: Will random a integer value. Can choose
		// to set the min and max. single max function will gen
		// from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT RandInt();
		MLCoreDLL virtual ML_INT RandInt(ML_INT max);
		MLCoreDLL virtual ML_INT RandInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Random Unsigned Int
		// Description: Will random a unsigned integer value. Can 
		// choose to set the min and max. single max function 
		// will gen from 0
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT RandUInt();
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT RandUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Random Float
		// Description: Will random a floating point value. Can
		// choose to set the min and max. Single max function
		// will gen from 0. not value function will gen from
		// FTL_MAX to -FTL_MAX so will be unlikely to gen small
		// numbers (not recommended)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT RandFloat();
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT RandFloat(ML_FLOAT min, ML_FLOAT max);

		//=========================================================
		// Name: Peek At Next Int
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_INT PeekNextInt();
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT max);
		MLCoreDLL virtual ML_INT PeekNextInt(ML_INT min, ML_INT max);

		//=========================================================
		// Name: Peek At Next UInt
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_UINT PeekNextUInt();
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT max);
		MLCoreDLL virtual ML_UINT PeekNextUInt(ML_UINT min, ML_UINT max);

		//=========================================================
		// Name: Peek At Next Float
		// Description: Will return the next random and restore 
		// the random back to its previous position to calling
		// a rand function will gen that number.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_FLOAT PeekNextFloat();
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT max);
		MLCoreDLL virtual ML_FLOAT PeekNextFloat(ML_FLOAT min, ML_FLOAT max);

	private:

		//=========================================================
		// Name: Random
		// Description: will generate the random number 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT Rand();

		//=========================================================
		// Name: Random
		// Description: will generate the array of 624 random
		// numbers
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BuildNumbers();

	private:

		//Random number array
		ML_UINT r[624];
		//Current index for the random array 
		ML_UINT index;
	};

	//=========================================================
	// Name: Random Type
	// Description: The Pseudo Random Distribution can use
	// any one of these methods to generate its random number
	// so they are enumerate to make it easy to define which one
	// to use.
	// Creator: Chris Millin
	//=========================================================
	enum MLRandomType
	{
		CRandom = 0,
		MultiplyWithCarry = 1,
		TwisterRandom = 2
	};

	//=========================================================
	// Name: Pseudo Random Distribution Look Up Table
	// Description: To prevent the need to recalculate the
	// the constant C each time this table contains most of the
	// values in relation to a 5% increase in P
	// Creator: Chris Millin
	//=========================================================
	const static ML_FLOAT MLPseudoRandDistLut[21] =
	{
		/*0%*/ 0.0f, /*5%*/ 0.00380f, /*10%*/	0.01475f, /*15%*/ 0.03222f, /*20%*/ 0.05570f, 
		/*25%*/ 0.08474f, /*30%*/ 0.11895f, /*35%*/ 0.15798f, /*40%*/ 0.20155f, /*45%*/ 0.24931f,
		/*50%*/ 0.30210f, /*55%*/ 0.36040f, /*60%*/ 0.42265f, /*65%*/ 0.48113f, /*70%*/ 0.57143f, 
		/*75%*/ 0.66667f, /*80%*/ 0.75000f, /*85%*/ 0.82353f, /*90%*/ 0.88889f, /*95%*/ 0.94737f,
		/*100%*/ 1.0f
	};

	//=========================================================
	// Name: Pseudo Random Distribution 
	// Description: rather than generate just a random number
	// this class will generate a random number and compare
	// it to an increasing percentage value based on the number
	// of times the random has not hit a desired value.
	// This is based off the method used in Warcraft III slight
	// by using the true value or C in the formula P(E) = C * N
	// when P(E) is the desired expected value (say 10%) C is 
	// a constant based chance calculated from P(E) and N is the
	// number of times the random has not hit the % value
	// is all detailed here http://www.playdota.com/forums/showthread.php?t=7993
	// in a better explination than mine.
	// It is a good method to provide a more fair random for games
	// rather than a "true" random value. It also allows for slight
	// predictability for the a chance to hit the desired value
	// allowing skilled players to have a higher chance for value
	// to proc at a desired time (is they can generate a proc in
	// another way)
	// Creator: Chris Millin
	//=========================================================
	class MLPseudoChance
	{
	public:

		MLCoreDLL MLPseudoChance(void);
		MLCoreDLL MLPseudoChance(MLRandomType rand);
		MLCoreDLL ~MLPseudoChance(void);

		//=========================================================
		// Name: Release
		// Description: Releases the random object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Release();

		//=========================================================
		// Name: Set Random Seed
		// Description: Will seed the random function being used
		// for the proc method. Recommended to use a detailed time 
		// value for this like file time. It is also recommended here
		// to call this after the random method has changed
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetRandSeed(ML_UINT seed);

		//=========================================================
		// Name: Set Method
		// Description: Releases the random object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetMethod(MLRandomType rand);

		//=========================================================
		// Name: Set Base Chance
		// Description: Will set the base chance for the proc to
		// occur. usePreCalc will allow the use of the pre calculated
		// C values if set to true and values not in multiples of 5
		// will be converted too the next multiple of 5 (e.g 1% = 5%).
		// Setting usePreCalc to false will use the a specific formula
		// to calculate C which is best not to do in real time if can
		// be avoided.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetBaseChance(ML_FLOAT chance, ML_BOOL usePreCalc);

		//=========================================================
		// Name: Reset Counter
		// Description: Will reset the not proc counter
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void ResetCounter() { m_count = 0; }
		
		//=========================================================
		// Name: Is Proc
		// Description: Checks to see if a proc will occur with
		// the current non proc counter.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsProc();

		//=========================================================
		// Name: Set Method
		// Description: Checks to see if a proc will occur with
		// a specific non proc count. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL IsProc(ML_UINT count);

		//=========================================================
		// Name: Current Chance
		// Description: Will get the current chance to proc.
		// Calling this after IsProc which returns true will return 
		// the base C value as the count will have been reset
		// (unless IsProc(ML_UINT count) was called)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_FLOAT CurrentChance()	{ return (m_constant * m_count) * 100.0f; }

		//=========================================================
		// Name: Current Count 
		// Description: Returns the current non proc counter.
		// Calling this after IsProc which returns true will return 
		// the base C value as the count will have been reset
		// (unless IsProc(ML_UINT count) was called)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT CurrentCount()	{ return m_count; }

		//=========================================================
		// Name: Last Chance
		// Description: Will return what the last chance value was,
		// can be called after IsProc true.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_FLOAT LastChance()	{ return (m_constant * m_lastCount) * 100.0f; }

		//=========================================================
		// Name: Last Count
		// Description: Will return what the last non proc Count value 
		// was, can be called after IsProc true.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT LastCount()	{ return m_lastCount; }

		//=========================================================
		// Name: Last Random
		// Description: Will return what the last Random value used
		// to obtain a proc, can be called after IsProc true.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT LastRand()	{ return m_lastRand; }
	
	private:

		//Second Method shown
		//http://gaming.stackexchange.com/questions/161430/calculating-the-constant-c-in-dota-2-pseudo-random-distribution
		//=========================================================
		// Name: C From P
		// Description: Will convert a P value to its constant C 
		// value (return 0 - 1). 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_DOUBLE CfromP(ML_DOUBLE p);

		//=========================================================
		// Name: P from C
		// Description: Will convert a C value to its P precentage
		// value (return 0 - 1).
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_DOUBLE PfromC(ML_DOUBLE c);

	private:

		//The base percentage chance
		ML_FLOAT	m_baseChance;

		//The C value
		ML_FLOAT	m_constant;
		
		//The N value which will count up
		ML_UINT		m_count;

		//Stores the last N value
		ML_UINT		m_lastCount;
		
		//stores the last random value
		ML_UINT		m_lastRand;

		//Random number generator
		MLRandom*	m_randMethod;
	};
}

#endif