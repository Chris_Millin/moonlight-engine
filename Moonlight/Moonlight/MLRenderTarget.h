#ifndef MLRENDERTARGET_H
#define MLRENDERTARGET_H

#include "MLIncludes.h"
#include "MLFrameBuffer.h"
#include "MLTexture.h"
#include "MLGraphics.h"

#define ML_LOG_RENDER_TARGET ML_STR("Render Target")

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	enum MLFBOTypes
	{
		ColourBuffer	= 0,
		StencilBuffer	= 1,
		DepthBuffer		= 2
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLRenderTarget
	{
	public:
		MLCoreDLL MLRenderTarget(void);
		MLCoreDLL virtual ~MLRenderTarget(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Initalise
		// Description: Will initalise all the frame buffer objects
		// ready for rendering.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Initalise(const MLFBOTypes enableFBO = MLFBOTypes::ColourBuffer);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Bind();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Draw(MLGraphics* batch);

		//=========================================================
		// Name: End Process
		// Description: Will do any end processing.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Unbind();

		//=========================================================
		// Name: Get Output
		// Description: Returns the result texture output.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLTexture* GetOutput() const { return m_outputTexture; }

	protected:

		MLTexture* m_outputTexture;

		MLFrameBuffer* m_FBO;
	};

}

#endif
