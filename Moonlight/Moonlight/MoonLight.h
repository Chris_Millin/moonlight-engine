#include "MLCore.h"

//=========================================================
//RENDER HEADERS
//=========================================================
#include "MLGLContext.h"
#include "MLCamera.h"
#include "MLCameraManager.h"
#include "MLFontCore.h"
//#include "MLFont.h"
#include "MLTrueTypeFont.h"
#include "MLBitmapFont.h"
#include "MLFormats.h"
#include "MLGraphics.h"
#include "MLShader.h"
#include "MLTexture.h"
#include "MLTextureAtlas.h"
#include "MLVertexBuffer.h"
#include "MLFrameBuffer.h"
#include "MLRenderBuffer.h"
#include "MLGraphicTypes.h"
#include "MLRenderTarget.h"
#include "MLPostProcess.h"
#include "MLMSAA.h"
#include "MLFXAA.h"
#include "MLScreenshot.h"
#include "MLPolyList.h"
#include "MLTriangulation.h"

//=========================================================
//UTILITY HEADERS
//=========================================================
#include "MLThread.h"
#include "MLVirtualDrive.h"
#include "MLVirtualPath.h"
#include "MLVirtualFile.h"
#include "MLConsole.h"
#include "MLUUID.h"
#include "MLRandom.h"
#include "MLDateTime.h"


//=========================================================
//INPUT HEADERS
//=========================================================
#include "MLController.h"
#include "MLInput.h"
#include "MLInputManager.h"

//=========================================================
//AUDIO HEADERS
//=========================================================
#include "MLAudioManager.h"
#include "MLSoundFX.h"
#include "MLMusic.h"

//=========================================================
//WINDOWS HANDLER
//=========================================================
#include "MLWindowsContext.h"

#pragma warning(disable : 4482)