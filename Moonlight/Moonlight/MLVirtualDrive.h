#ifndef MLVIRTUALDRIVE_H
#define MLVIRTUALDRIVE_H

#include "MLCore.h"
#include "MLConsole.h"

#include <vector>

namespace MLKernel
{
	//=========================================================
	// Name: Virtual Drive
	// Description: A virtual drive can be any valid file drive
	// object such as a hard drive, USB drive or even a network
	// drive. This doest do anything without a file path but
	// to start a path this is required. It also provides 
	// information about the specified drive.
	// Creator: Chris Millin
	//=========================================================
	class MLVirtualDrive
	{
	public:
		MLCoreDLL MLVirtualDrive();
		MLCoreDLL virtual ~MLVirtualDrive();

		//=========================================================
		// Name: Release
		// Description: Release the object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Default Root
		// Description: Will set the Drive to the current application
		// path which is "\\"
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void DefaultRoot();

		//=========================================================
		// Name: Set a Drive
		// Description: Will set the current virtual drive to a valid
		// drive ID (normally "C:\" and so on)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetDrive(const ML_STRING& drive);

		//=========================================================
		// Name: Free Space
		// Description: Returns the free space on the drive in 
		// bytes
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_QWORD FreeSpace() const { return m_freeSpace; }

		//=========================================================
		// Name: Used Space
		// Description: Returns the used space on the drive in
		// bytes
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_QWORD UsedSpace() const { return m_space - m_freeSpace; }

		//=========================================================
		// Name: Space
		// Description: Returns the total volume of the drive in
		// bytes
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_QWORD Space() const { return m_space; }

		//=========================================================
		// Name: Is Valid
		// Description: Will check to see if the drive is valid
		// though this is done when the drive is set any way
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsValid() const ;

		//=========================================================
		// Name: Is Default
		// Description: Will check to see if the drive is the default
		// application path address
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsDefault() const { return m_drive == ML_STR("\\") ? ML_TRUE : ML_FALSE; }

		//=========================================================
		// Name: Enumerate Drives
		// Description: Will return all the drive ids to be used
		// with set drive (not default drive will not be returned
		// as its id is not a true drive id. use DefaultRoot to
		// set the drive to the root application drive)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<ML_STRING> EnumerateDrives() const;

		//=========================================================
		// Name: Get Drive
		// Description: Returns the ID of the current drive
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetDrive() const { return m_drive; }

		//=========================================================
		// Name: To String
		// Description: returns the drive information
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING ToString() const { return m_drive; }

		//=========================================================
		// Name: Equals Operator Virtual Drive
		// Description: Will set this virtual drive to the same
		// drive as the other if it is valid
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualDrive& operator=(const MLVirtualDrive& lhs)	{ SetDrive(lhs.m_drive); return *this; }
	
		//=========================================================
		// Name: Equals Operator Virtual Drive
		// Description: Will set this virtual drive to the drive
		// id in the string if it is valid
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualDrive& operator=(const ML_STRING& lhs)		{ SetDrive(lhs); return *this; }

	protected:

		//=========================================================
		// Name: Read Drive Space
		// Description: Reads the space information about the current
		// drive if valid
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL readDriveSpace();

	protected:

		//The drive ID
		ML_STRING m_drive;

		//Drive Free Space
		ML_QWORD m_freeSpace;
		//Drive Full Space
		ML_QWORD m_space;
	};
}

#endif