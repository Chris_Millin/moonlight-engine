#ifndef MLAUDIOTHREAD_H
#define MLAUDIOTHREAD_H

#include "MLIncludes.h"
#include "MLAudioHeaders.h"
#include "MLMusic.h"


namespace MLAudio
{
/// <summary>	Audio Thread Log Name. </summary>
#define ML_LOG_AUDIO_THREAD ML_STR("Audio Thread")

///-------------------------------------------------------------------------------------------------
/// <summary>	Timeout for the Audio thread mutex (1min). </summary>
///
/// <remarks>	Chris, 02/11/2014. </remarks>
///-------------------------------------------------------------------------------------------------
#define ML_AUDIO_MUTEX_TIMEOUT 60000

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Audio thread is a object that assists
	///	with updating the audio stream. The Audio class
	///	has a static instance of this so all audio classes
	///	can access it to run in the update thread. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLAudioThread : MLKernel::MLThread
	{
	public:

		MLCoreDLL MLAudioThread();
		MLCoreDLL virtual ~MLAudioThread();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Release the Thread object. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual void Release();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will set up the update thread. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL Intialise();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Set the volume for the all stored audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="gain">	The volume for all music added with the thread to proccess. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL SetMusicVolume(const ML_FLOAT gain);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will add a music object to be run in the
		///	update thread. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="music">	Music file to add to be proccessed by the thread. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL AddMusic(MLMusic* music);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will remove the music object from the
		///	update thread. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="music">	Music file to remove from the thread audio processor. </param>
		///
		/// <returns>	Return true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL ML_BOOL RemoveMusic(MLMusic* music);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will pause the thread pausing all audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void PauseUpdate()  { ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT); m_pauseThread = ML_TRUE; ThreadReleaseMutex(m_mutex); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will resume the thread resuming audio playback. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL void ResumeUpdate() { ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT); m_pauseThread = ML_FALSE; ThreadReleaseMutex(m_mutex); }

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Where the thread updates the audio. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL virtual void threadEntry();

	protected:

		/// <summary>	Check if the thread is to be paused or not. </summary>
		ML_BOOL m_pauseThread;
		/// <summary>	The mutex name key. </summary>
		ML_STRING m_mutex;

	private:

		/// <summary>	Vector of all the audio files being proccessed. </summary>
		std::vector <MLMusic*> m_activeAudio;
	};
}

#endif // MLAUDIOTHREAD_H