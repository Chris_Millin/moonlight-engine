#include "MLInputManager.h"

namespace MLInput
{

	RAWINPUTDEVICE			MLInputManager::m_rawDevices[2];

	ML_STRING				MLInputManager::m_recordingLowerString;
	ML_STRING				MLInputManager::m_recordingUpperString;
	ML_UINT					MLInputManager::m_recordingLimit;
	ML_UINT					MLInputManager::m_recordingIndex;
	ML_BOOL					MLInputManager::m_recording;

	MLInput::MLKeyEvent		MLInputManager::m_lastKeyPressed;

	ML_BOOL					MLInputManager::m_KeyCurrentState[ML_KEY_COUNT];
	ML_BOOL					MLInputManager::m_KeyPreviousState[ML_KEY_COUNT];
	MLInput::MLMouseState	MLInputManager::m_MouseState;

	MLInput::MLController	MLInputManager::m_controllers[4];
	ML_BOOL					MLInputManager::m_controllersInitalised = ML_FALSE;


	ML_SCHAR keyboardLut[ML_KEY_COUNT]		= { 0 };
	ML_SCHAR keyboardCAPLut[ML_KEY_COUNT]	= { 0 };
	ML_SCHAR keyboardShiftLut[ML_KEY_COUNT] = { 0 };

	MLCoreDLL MLInput::MLController* MLInputManager::GetController(const ML_INT ID)
	{
		if (ID > 4) return NULL;
		if (ID < 1) return NULL;

		if (m_controllersInitalised == ML_FALSE)
		{
			m_controllers[0].SetControllerID(1);
			m_controllers[1].SetControllerID(2);
			m_controllers[2].SetControllerID(3);
			m_controllers[3].SetControllerID(4);
			m_controllersInitalised = ML_TRUE;
		}

		return &m_controllers[ID];
	}

	MLCoreDLL ML_BOOL MLInputManager::CreateRawKeyboard(void)
	{
		//creates a lookup table for the keyboard
		//with normal, shift and caps lock
		CreateKeyboardLUT();

		m_lastKeyPressed.keyDown = ML_FALSE;
		m_lastKeyPressed.keyID = 0;
		m_lastKeyPressed.keyModifiyer = 0;
		m_lastKeyPressed.keyCharacter = 0;

		//Creates the raw input device for the keyboard
		m_rawDevices[0].usUsagePage = 0x01;
		m_rawDevices[0].usUsage = (ML_USHORT)MLInputType::Keyboard;
		m_rawDevices[0].dwFlags = RIDEV_NOLEGACY;
		m_rawDevices[0].hwndTarget = 0;
		if (RegisterRawInputDevices(&m_rawDevices[0], 1, sizeof(m_rawDevices[0])) == FALSE)
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to register raw keyboard"));
			return ML_FALSE;
		}
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLInputManager::ReleaseRawKeyboard(void)
	{
		//Creates the raw input device for the keyboard
		m_rawDevices[0].usUsagePage = 0x01;
		m_rawDevices[0].usUsage = (ML_USHORT)MLInputType::Keyboard;
		m_rawDevices[0].dwFlags = RIDEV_REMOVE;
		m_rawDevices[0].hwndTarget = NULL;
		if (RegisterRawInputDevices(&m_rawDevices[0], 1, sizeof(m_rawDevices[0])) == FALSE)
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to release raw keyboard"));
			return ML_FALSE;
		}
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLInputManager::IsKeyHeld(const ML_BYTE Key)
	{
		//returns the key state from the key array
		return m_KeyCurrentState[Key];
	}

	MLCoreDLL ML_BOOL MLInputManager::IsKeyPressed(const ML_BYTE Key)
	{
		//returns the key state from the key array
		if (m_KeyCurrentState[Key] && !m_KeyPreviousState[Key])
		{
			//force to true since the windows message handler may not proc in the time this function is called
			//again
			m_KeyPreviousState[Key] = ML_TRUE;
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLInputManager::StartRecordKeyString(ML_UINT limit /*= 1024*/)
	{
		m_recordingLimit = limit;
		m_recording = ML_TRUE;
		return m_recording;
	}

	MLCoreDLL ML_BOOL MLInputManager::SetRecordingIndex(const ML_UINT index)
	{
		if (index >= 0 && index <= (m_recordingLowerString.size() + m_recordingUpperString.size()))
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Debug,
				ML_STR("Setting at index %d\n"), index);
			if (m_recordingLowerString.size() > index)
			{
				ML_STRING newLower = m_recordingLowerString.substr(0, index);
				m_recordingUpperString.insert(0, m_recordingLowerString.substr(index));
				m_recordingLowerString = newLower;

				return ML_TRUE;
			}
			else if (m_recordingLowerString.size() < index)
			{
				int tempIndex = index - m_recordingLowerString.size();
				ML_STRING newUpper = m_recordingUpperString.substr(tempIndex);
				m_recordingLowerString += m_recordingUpperString.substr(0, tempIndex);
				m_recordingUpperString = newUpper;

				return ML_TRUE;
			}

			return ML_FALSE;
		}
		else
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Warning,
				ML_STR("Recording index %d was invalid and is ignored (%d)\n"), index, (m_recordingLowerString.size() + m_recordingUpperString.size()));
			return ML_FALSE;
		}
	}

	MLCoreDLL ML_STRING MLInputManager::GetRecordKeyString()
	{
		return m_recordingLowerString + m_recordingUpperString;
	}

	MLCoreDLL ML_BOOL MLInputManager::UpdateRecordingKeyString()
	{
		if (m_lastKeyPressed.keyDown && m_recording
			&& (m_recordingLowerString.size() + m_recordingUpperString.size()) <= m_recordingLimit)
		{
			ML_SCHAR c = 0;
			if (m_lastKeyPressed.keyID == MLKeys::Back && m_recordingLowerString.size() != 0)
			{
				m_recordingLowerString.erase(m_recordingLowerString.size() - 1, 1);
				m_lastKeyPressed.keyID = 0;
			}
			else if (m_lastKeyPressed.keyID == MLKeys::Delete && m_recordingUpperString.size() != 0)
			{
				m_recordingUpperString.erase(0, 1);
				m_lastKeyPressed.keyID = 0;
			}
			else
			{
				if (m_lastKeyPressed.keyModifiyer == 0)
					c = keyboardLut[m_lastKeyPressed.keyID];
				if (m_lastKeyPressed.keyModifiyer == MLKeys::Caps)
					c = keyboardCAPLut[m_lastKeyPressed.keyID];
				if (m_lastKeyPressed.keyModifiyer == MLKeys::Shift)
					c = keyboardShiftLut[m_lastKeyPressed.keyID];

				if (c != 0)
					m_recordingLowerString.insert(m_recordingLowerString.end(), c);
			}
			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLInputManager::StopRecordingKeyString()
	{
		m_recording = ML_FALSE;
		return m_recording;
	}

	MLCoreDLL ML_BOOL MLInputManager::CreateRawMouse(void)
	{
		//creates the raw input device for the mouse
		m_rawDevices[1].usUsagePage = 0x01;
		m_rawDevices[1].usUsage = (ML_USHORT)MLInputType::Mouse;
		m_rawDevices[1].dwFlags = RIDEV_NOLEGACY;
		m_rawDevices[1].hwndTarget = 0;
		if (RegisterRawInputDevices(&m_rawDevices[1], 1, sizeof(m_rawDevices[1])) == FALSE)
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to register raw mouse"));
			return ML_FALSE;
		}
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLInputManager::ReleaseRawMouse(void)
	{
		//creates the raw input device for the mouse
		m_rawDevices[1].usUsagePage = 0x01;
		m_rawDevices[1].usUsage = (ML_USHORT)MLInputType::Mouse;
		m_rawDevices[1].dwFlags = RIDEV_REMOVE;
		m_rawDevices[1].hwndTarget = NULL;
		if (RegisterRawInputDevices(&m_rawDevices[1], 1, sizeof(m_rawDevices[1])) == FALSE)
		{
			ML_TRACE(ML_LOG_INPUT, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to release raw mouse"));
			return ML_FALSE;
		}
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLInputManager::IsMouseButtonHeld(const ML_BYTE button)
	{
		//returns the buttons state from the mouse button array
		return m_MouseState.CurrentButttons[button];
	}

	MLCoreDLL ML_BOOL MLInputManager::IsMouseButtonPressed(const ML_BYTE button)
	{
		//returns the buttons state from the mouse button array
		if (m_MouseState.CurrentButttons[button] && !m_MouseState.PreviousButttons[button])
		{
			m_MouseState.PreviousButttons[button] = ML_TRUE;
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	MLCoreDLL const MLVector2f MLInputManager::GetMousePositon(void)
	{
		//returns the position of the mouse in window as a vector2
		return	MLVector2f((ML_FLOAT)m_MouseState.CurrentAxisX, (ML_FLOAT)m_MouseState.CurrentAxisY);
	}

	MLCoreDLL const MLVector2f MLInputManager::GetMousePositon(const MLRender::MLScreenScaler& screen)
	{
		//returns the position of the mouse in window as a vector2
		ML_FLOAT widthScale		= (ML_FLOAT)m_MouseState.CurrentAxisX / MLScreenWidth;
		ML_FLOAT heightScale	= (ML_FLOAT)m_MouseState.CurrentAxisY / MLScreenHeight;
		ML_FLOAT Ratio			= (ML_FLOAT)screen.BaseWidth / (ML_FLOAT)screen.BaseHeight;
		ML_FLOAT CurrentRatio	= (ML_FLOAT)MLScreenWidth / (ML_FLOAT)MLScreenHeight;
		ML_FLOAT invRatio		= (ML_FLOAT)screen.BaseHeight / (ML_FLOAT)screen.BaseWidth;
		ML_FLOAT newWidth		= (ML_FLOAT)(Ratio * MLScreenHeight);
		ML_FLOAT newHeight		= (ML_FLOAT)(MLScreenWidth / Ratio);
		ML_FLOAT offsetRatioW	= (MLScreenWidth - newWidth);
		ML_FLOAT offsetRatioH	= (MLScreenHeight - newHeight);

		switch (screen.Method)
		{
		case MLRender::MLScreenScaling::None:
			return	MLVector2f((ML_FLOAT)m_MouseState.CurrentAxisX, (ML_FLOAT)m_MouseState.CurrentAxisY);
		case MLRender::MLScreenScaling::Strech:
			return	MLVector2f(widthScale * screen.BaseWidth, heightScale * screen.BaseHeight);
		case MLRender::MLScreenScaling::BestFit:
			if (Ratio <= CurrentRatio) //pillar
				return	MLVector2f(((m_MouseState.CurrentAxisX / newWidth)  * screen.BaseWidth) - (((offsetRatioW / 2) / newWidth) * screen.BaseWidth), heightScale * screen.BaseHeight);
			else //bars
				return	MLVector2f(widthScale * screen.BaseWidth, ((m_MouseState.CurrentAxisY / newHeight)  * screen.BaseHeight) - (((offsetRatioH / 2) / newHeight) * screen.BaseHeight));
		case MLRender::MLScreenScaling::Zoom:
			if (Ratio < CurrentRatio) //width > base
			{
				return	MLVector2f(widthScale * screen.BaseWidth,
					((m_MouseState.CurrentAxisY / (MLScreenWidth / Ratio)) * screen.BaseHeight) + ((screen.BaseHeight - ((MLScreenHeight / (MLScreenWidth / Ratio)) * screen.BaseHeight)) / 2));
			}
			else if (Ratio > CurrentRatio) //height > base
			{
				return	MLVector2f(((m_MouseState.CurrentAxisX / (MLScreenHeight * Ratio)) * screen.BaseWidth) + ((screen.BaseWidth - ((MLScreenWidth / (MLScreenHeight * Ratio)) * screen.BaseWidth)) / 2),
					heightScale * screen.BaseHeight);
			}
			else
			{
				return	MLVector2f(widthScale * screen.BaseWidth, heightScale * screen.BaseHeight);
			}
		case MLRender::MLScreenScaling::Center:
			return MLVector2f((ML_FLOAT)m_MouseState.CurrentAxisX - (((ML_FLOAT)MLScreenWidth - screen.BaseWidth) / 2), (ML_FLOAT)m_MouseState.CurrentAxisY - (((ML_FLOAT)MLScreenHeight - screen.BaseHeight) / 2));
		}
		return	MLVector2f((ML_FLOAT)m_MouseState.CurrentAxisX, (ML_FLOAT)m_MouseState.CurrentAxisY);
	}

	MLCoreDLL const MLVector2f MLInputManager::GetMouseLastPositon(void)
	{
		//returns the mouse last x and last y as a vector2
		return	MLVector2f((ML_FLOAT)m_MouseState.PreviousAxisX, (ML_FLOAT)m_MouseState.PreviousAxisY);
	}

	MLCoreDLL const MLVector2f MLInputManager::GetMouseLastPositon(const MLRender::MLScreenScaler& screen)
	{
		//returns the mouse last x and last y as a vector2
		return	MLVector2f((ML_FLOAT)m_MouseState.PreviousAxisX, (ML_FLOAT)m_MouseState.PreviousAxisY);
	}

	MLCoreDLL ML_INT MLInputManager::GetScrollWheel(void)
	{
		return m_MouseState.WheelAxis;
	}

	MLCoreDLL void MLInputManager::WindowsMessageHook(LPARAM lParamHandle, UINT message, WPARAM wParam)
	{

		for (ML_UINT i = 0; i < ML_KEY_COUNT; i++)
			m_KeyPreviousState[i] = m_KeyCurrentState[i];
		for (ML_UINT i = 0; i < ML_MOUSE_BUTTON_COUNT; i++)
			m_MouseState.PreviousButttons[i] = m_MouseState.CurrentButttons[i];

		m_MouseState.WheelAxis = 0;
		//check the message to see what has been triggered
		switch (message)
		{
			case WM_LBUTTONDOWN:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Left] = ML_TRUE;
				break;
			}
			case WM_LBUTTONUP:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Left] = ML_FALSE;
				break;
			}
			case WM_MBUTTONDOWN:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Middle] = ML_TRUE;
				break;
			}
			case WM_MBUTTONUP:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Middle] = ML_FALSE;
				break;
			}
			case WM_RBUTTONDOWN:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Right] = ML_TRUE;
				break;
			}
			case WM_RBUTTONUP:
			{
				m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Right] = ML_FALSE;
				break;
			}
			case WM_XBUTTONDOWN:
			{
				//the x button is the 2 side buttons on the mouse which can
				//be obtained in this way
				ML_CHAR type = (ML_CHAR)HIWORD(wParam);
				if (type == XBUTTON1)
					m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::One] = ML_TRUE;
				else if (type == XBUTTON2)
					m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Two] = ML_TRUE;
				break;
			}
			case WM_XBUTTONUP:
			{
				ML_CHAR type = (ML_CHAR)HIWORD(wParam);
				if (type == XBUTTON1)
					m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::One] = ML_FALSE;
				else if (type == XBUTTON2)
					m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Two] = ML_FALSE;
				break;
			}
			case WM_MOUSEMOVE:
			{
				//get the mouse values when the mouse moves
				m_MouseState.CurrentAxisX = LOWORD(lParamHandle);
				m_MouseState.CurrentAxisY = HIWORD(lParamHandle);
				break;
			}
			case WM_MOUSEWHEEL:
			{
				m_MouseState.WheelAxis = 0;
				m_MouseState.WheelAxis = GET_WHEEL_DELTA_WPARAM(wParam);
				break;
			}
				//WM_INPUT is called for raw input
			case WM_INPUT:
			{

				UINT dwSize;
				//gets the raw input data to see if its active
				GetRawInputData((HRAWINPUT)lParamHandle, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
				LPBYTE lpb = new BYTE[dwSize];
				if (lpb == NULL)
				{
					return;
				}
				//if it is active obtain the raw input information
				if (GetRawInputData((HRAWINPUT)lParamHandle, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
					return;

				//cast as a raw input structure;
				RAWINPUT* raw = (RAWINPUT*)lpb;

				m_MouseState.PreviousAxisX = 0;
				m_MouseState.PreviousAxisY = 0;

				// if the type is a keyboard obtain the keyboard data
				if (raw->header.dwType == RIM_TYPEKEYBOARD)
				{
					//get the virtual key
					unsigned ML_SHORT key = raw->data.keyboard.VKey;

					//if the key is equal to 255 it is discarded
					if (key != 255)
					{
						//if the arrow keys are pressed this flag is triggered
						if (raw->data.keyboard.Flags == RI_KEY_E0)
						{
							m_KeyCurrentState[key] = ML_TRUE;
							m_lastKeyPressed.keyDown = ML_TRUE;
							m_lastKeyPressed.keyID = (ML_CHAR)key;
							m_lastKeyPressed.keyCharacter = keyboardLut[key];
						}
						//if the arrow keys are up then these flags are both triggered
						if (raw->data.keyboard.Flags == RI_KEY_BREAK + RI_KEY_E0)
						{
							m_KeyCurrentState[key] = ML_FALSE;
							m_lastKeyPressed.keyDown = ML_TRUE;

						}
						//if any other key is pressed down this flag is triggered
						if (raw->data.keyboard.Flags == RI_KEY_MAKE)
						{
							m_KeyCurrentState[key] = ML_TRUE;

							if (key == MLKeys::RShift || key == MLKeys::LShift || key == MLKeys::Shift)
							{
								m_lastKeyPressed.keyModifiyer = MLKeys::Shift;
							}
							if (key == MLKeys::LCtrl || key == MLKeys::RCtrl || key == MLKeys::Ctrl)
							{
								m_lastKeyPressed.keyModifiyer = MLKeys::Ctrl;
							}
							if (key == MLKeys::LAlt || key == MLKeys::RAlt || key == MLKeys::Alt)
							{
								m_lastKeyPressed.keyModifiyer = MLKeys::Alt;
							}
							if (key == MLKeys::Caps)
							{
								if (m_lastKeyPressed.keyModifiyer != MLKeys::Caps)
									m_lastKeyPressed.keyModifiyer = MLKeys::Caps;
								else
									m_lastKeyPressed.keyModifiyer = 0;
							}
							else
							{
								m_lastKeyPressed.keyDown = ML_TRUE;
								m_lastKeyPressed.keyID = (ML_CHAR)key;
								if (m_lastKeyPressed.keyModifiyer == MLKeys::Shift)
								{
									m_lastKeyPressed.keyCharacter = keyboardShiftLut[key];
								}
								if (m_lastKeyPressed.keyModifiyer == MLKeys::Caps)
								{
									m_lastKeyPressed.keyCharacter = keyboardCAPLut[key];
								}
								else
								{
									m_lastKeyPressed.keyCharacter = keyboardLut[key];
								}
							}
						}
						//if any other key is up this flag is triggered
						if (raw->data.keyboard.Flags == RI_KEY_BREAK)
						{
							m_KeyCurrentState[key] = ML_FALSE;

							if (key == MLKeys::RShift || key == MLKeys::LShift || key == MLKeys::Shift)
							{
								m_lastKeyPressed.keyModifiyer = 0;
							}
							if (key == MLKeys::LCtrl || key == MLKeys::RCtrl || key == MLKeys::Ctrl)
							{
								m_lastKeyPressed.keyModifiyer = 0;
							}
							if (key == MLKeys::LAlt || key == MLKeys::RAlt || key == MLKeys::Alt)
							{
								m_lastKeyPressed.keyModifiyer = 0;
							}
							if (key == MLKeys::Caps)
							{
								if (m_lastKeyPressed.keyModifiyer != MLKeys::Caps)
									m_lastKeyPressed.keyModifiyer = 0;
								else
									m_lastKeyPressed.keyModifiyer = MLKeys::Caps;
							}
							else
							{
								m_lastKeyPressed.keyDown = ML_FALSE;
								m_lastKeyPressed.keyID = (ML_CHAR)key;
								m_lastKeyPressed.keyCharacter = keyboardLut[key];
							}
						}
					}
				}
				//if the raw input device is a mouse this function is triggered
				else if (raw->header.dwType == RIM_TYPEMOUSE)
				{
					switch (raw->data.mouse.usButtonFlags)
					{
					case RI_MOUSE_LEFT_BUTTON_DOWN:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Left] = ML_TRUE;
						break;
					case RI_MOUSE_LEFT_BUTTON_UP:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Left] = ML_FALSE;
						break;
					case RI_MOUSE_RIGHT_BUTTON_DOWN:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Right] = ML_TRUE;
						break;
					case RI_MOUSE_RIGHT_BUTTON_UP:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Right] = ML_FALSE;
						break;
					case RI_MOUSE_MIDDLE_BUTTON_DOWN:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Middle] = ML_TRUE;
						break;
					case RI_MOUSE_MIDDLE_BUTTON_UP:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Middle] = ML_FALSE;
						break;
					case RI_MOUSE_BUTTON_4_DOWN:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::One] = ML_TRUE;
						break;
					case RI_MOUSE_BUTTON_4_UP:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::One] = ML_FALSE;
						break;
					case RI_MOUSE_BUTTON_5_DOWN:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Two] = ML_TRUE;
						break;
					case RI_MOUSE_BUTTON_5_UP:
						m_MouseState.CurrentButttons[(ML_BYTE)MLMouseButtons::Two] = ML_FALSE;
						break;
					case RI_MOUSE_WHEEL:
						m_MouseState.WheelAxis = 0;
						m_MouseState.WheelAxis = (SHORT)raw->data.mouse.usButtonData;
					}

					if (raw->data.mouse.lLastX < 5)
						m_MouseState.PreviousAxisX = 0;
					else
						m_MouseState.PreviousAxisX = raw->data.mouse.lLastX;
					if (raw->data.mouse.lLastY < 5)
						m_MouseState.PreviousAxisY = 0;
					else
						m_MouseState.PreviousAxisY = raw->data.mouse.lLastY;
				}
				//delete the raw input information
				delete[] lpb;

				UpdateRecordingKeyString();
			}
		}
	}
}