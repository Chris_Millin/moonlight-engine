#version 400

layout( points ) in;
layout( triangle_strip, max_vertices = 4 ) out;

uniform float SizeX; // Half the width of the quad
uniform float SizeY; // Half the width of the quad

uniform mat4 ProjectionMat;
uniform bool TimeEnabled;
uniform float MaxTime;
uniform vec3 tintColour;

out vec2 GTexCoord;
out vec4 GColour;

void main()
{
    vec4 pointin = vec4(gl_in[0].gl_Position.x,gl_in[0].gl_Position.y, 0, 1.0); 
    float Time = 1.0f;
    float normalisedvalue = 0.0f;
    if(TimeEnabled == true)
    {
        Time = gl_in[0].gl_Position.z;
        normalisedvalue = Time / MaxTime;
    }

    gl_Position = ProjectionMat * (vec4(-SizeX * Time,-SizeY * Time,0.0,0.0) + pointin);
    GTexCoord = vec2(0.0,0.0);
    GColour = vec4(tintColour.x,tintColour.y,tintColour.z,1 - normalisedvalue);
    EmitVertex();

    gl_Position = ProjectionMat * (vec4(SizeX * Time,-SizeY * Time,0.0,0.0) + pointin);
    GTexCoord = vec2(1.0,0.0);
    GColour = vec4(tintColour.x,tintColour.y,tintColour.z,1 - normalisedvalue);
    EmitVertex();

    gl_Position = ProjectionMat * (vec4(-SizeX * Time,SizeY * Time,0.0,0.0) + pointin);
    GTexCoord = vec2(0.0,1.0);
    GColour = vec4(tintColour.x,tintColour.y,tintColour.z,1 - normalisedvalue);
    EmitVertex();

    gl_Position = ProjectionMat *  (vec4(SizeX * Time,SizeY * Time,0.0,0.0) + pointin);
    GTexCoord = vec2(1.0,1.0);
    GColour = vec4(tintColour.x,tintColour.y,tintColour.z,1 - normalisedvalue);
    EmitVertex();
    EndPrimitive();
}