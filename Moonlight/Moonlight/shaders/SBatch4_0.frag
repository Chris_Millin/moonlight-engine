#version 400
noperspective in vec3 GEdgeDistance;
in vec2 GTexCoord;
in vec4 GColour;

uniform struct LineInfo {
  float Width;
  vec4 Colour;
} Line;

uniform sampler2D Tex1;
uniform bool EnableWire;

out vec4 FragColour;

void main() {

	FragColour = texture( Tex1, GTexCoord );
	FragColour = FragColour * GColour;
    if(EnableWire)
    {
    	float d = min( GEdgeDistance.x, GEdgeDistance.y );
        d = min( d, GEdgeDistance.z );

        float mixVal;
        if( d < Line.Width - 1 ) {
            mixVal = 1.0;
        } else if( d > Line.Width + 1 ) {
            mixVal = 0.0;
        } else {
            float x = d - (Line.Width - 1);
            mixVal = exp2(-2.0 * (x*x));
        }
        FragColour = mix(FragColour, Line.Colour, mixVal);
    }
	
}