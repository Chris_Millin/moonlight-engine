in vec2 GTexCoord;
in vec4 GColour;

uniform sampler2D Tex1;
uniform bool TimeEnabled;

out vec4 FragColour;

void main() {

	 FragColour  = texture( Tex1, GTexCoord );
	 FragColour = FragColour * GColour;
}