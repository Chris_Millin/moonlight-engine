#include "MLIncludes.h"
#include "MLShader.h"
#pragma once

#define ML_LOG_SHADER_MANAGER ML_STR("Shader Manager")

namespace MLRender
{

	class MLShaderManager
	{
	public:

		//=========================================================
		// Name: Create Shader
		// Description: Creates a shader with a unique UUID name
		// will then return the shader created.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLShader* CreateShader();

		//=========================================================
		// Name: Create Shader
		// Description: Creates a shader with a defined unique name.
		// if the name already exists then a UUID is used.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLShader* CreateShader(ML_STRING& name);

		//=========================================================
		// Name: Create Shader
		// Description: Creates a shader with a name and a string
		// file location with the type of shader loading. 
		// This does return the shader and creates it if it doesn't
		// already exist.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLShader* CreateShader(ML_STRING& name, ML_STRING& file, const GLenum type);

		//=========================================================
		// Name: Create Shader
		// Description: Creates a shader with a name and a string
		// file location with the type of shader loading. 
		// This does return the shader and creates it if it doesn't
		// already exist.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLShader* CreateShader(ML_STRING& name, MLKernel::MLVirtualFile* file, const GLenum type);

		//=========================================================
		// Name: Remove Shader
		// Description: Will remove the shader from the collection
		// using the name.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveShader(const ML_STRING& name);

		//=========================================================
		// Name: Remove Shader
		// Description: Will remove the shader from the collection
		// using the shader object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveShader(const MLShader* shader);

		//=========================================================
		// Name: Remove All
		// Description: Remove all the shader objects
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveAll();

		//=========================================================
		// Name: Shader Exists
		// Description: Checks to see if a shader name exists
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL ShaderExist(const ML_STRING& name);

		//=========================================================
		// Name: Get Shader
		// Description: Gets a shader by the name
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLShader* GetShader(const ML_STRING& name);

		
		//=========================================================
		// Name: Get Shader Name
		// Description: Returns the unique name assigned to the 
		// shader the is linked to a name
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_STRING GetShaderName(const MLShader* texture);

	private:

		MLCoreDLL MLShaderManager(void) {};
		MLCoreDLL virtual ~MLShaderManager(void) {};

	private:

		typedef std::map<ML_STRING, MLShader*> MLShaderGroup;

		MLCoreDLL static MLShaderGroup m_shaders;
	};

}

