#include "MLScreenshot.h"

//NOTE THIS IS FUCKING BROKEN AND THERE IS NO REASON FOR IT TO BE
//THERE IS AN ISSUE WITH GLREADPIXELS WHERE AT SPECIFIC WIDTH AND HEIGHTS
//THE PIXEL INFORMATION IS SKEWED AND PRODUCES SOME FUCKED BOLLOCKS SHIT
//LITRALLY FOR NO REASON SINCE MEMORY ALLOCATION SIZE IS FINE
//READING LESS THE 1 PIXEL ON THE WIDTH AND HEIGHT HAS SOLVED MEMORY CORRUPTION
//FOR THE MOMENT

namespace MLRender
{

	void MLScreenshot::Screenshot()
	{
		MLKernel::MLVirtualPath* scrotPath = new MLKernel::MLVirtualPath(ML_DEFAULT_SCREENSHOT_PATH);

		if(!scrotPath->IsValid())
			scrotPath->CreatePath();
		if(!scrotPath->IsValid())
			ML_TRACE(ML_LOG_SCROT, MLKernel::MLConsoleStatus::Error, ML_STR("There was a problem creating the default screenshot path screenshot failed."));
		else
		{
			ML_STRING fileName = ML_DEFAULT_SCREENSHOT_NAME + MLDateTime::Now().ToFileString() + ML_DEFAULT_SCREENSHOT_TYPE;
			MLKernel::MLVirtualFile* out = new MLKernel::MLVirtualFile(fileName, scrotPath);
			if(out->Exists())
				out->DeleteFile();
			if(out->Exists())
				ML_TRACE(ML_LOG_SCROT, MLKernel::MLConsoleStatus::Error, ML_STR("A file with the name %s already exists."), out->GetFileName());
			else
			{
				Screenshot(out);
			}

			delete out;
		}

		delete scrotPath;
	}

	void MLScreenshot::Screenshot(const ML_STRING& file)
	{
		MLKernel::MLVirtualPath* scrotPath = new MLKernel::MLVirtualPath(ML_DEFAULT_SCREENSHOT_PATH);

		if(!scrotPath->IsValid())
			scrotPath->CreatePath();
		if(!scrotPath->IsValid())
			ML_TRACE(ML_LOG_SCROT, MLKernel::MLConsoleStatus::Error, ML_STR("There was a problem creating the default screenshot path screenshot failed."));
		else
		{
			MLKernel::MLVirtualFile* out = new MLKernel::MLVirtualFile(file, scrotPath);
			if(out->Exists())
				out->DeleteFile();
			if(out->Exists())
				ML_TRACE(ML_LOG_SCROT, MLKernel::MLConsoleStatus::Error, ML_STR("A file with the name %s already exists."), out->GetFileName());
			else
			{
				Screenshot(out);
			}

			delete out;
		}

		delete scrotPath;
	}

	void MLScreenshot::Screenshot(const MLKernel::MLVirtualFile* file)
	{
		

		GLint viewport[4]; //current viewport  

		//get current viewport  
		glGetIntegerv(GL_VIEWPORT, viewport);

		ML_UINT Width = viewport[2];
		ML_UINT Height = viewport[3];

		ML_BYTE* pixels = new ML_BYTE[3 * Height * Width];
		glFinish();
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glReadBuffer(GL_FRONT);
		MLTexture* outScrot = new MLTexture();
		// Make the BYTE array, factor of 3 because it's RBG.
	
		glReadPixels(0, 0, Width-1, Height-1, GL_RGB, GL_UNSIGNED_BYTE, pixels);
		
		outScrot->SetHeight(Height);
		outScrot->SetWidth(Width);
		if(!outScrot->ExportImage(MLtoAscii(file->FullFilePath()).c_str(), pixels, MLPixelFormat::RGB24))
			ML_TRACE(ML_LOG_SCROT, MLKernel::MLConsoleStatus::Error, ML_STR("There was a problem exporting the screenshot."));

		delete[] pixels;
		delete outScrot;
	}

}