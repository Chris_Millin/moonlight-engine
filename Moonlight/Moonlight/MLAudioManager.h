#ifndef MLAUDIOMANAGER_H
#define MLAUDIOMANAGER_H


#include "MLIncludes.h"
#include "MLAudioHelper.h"

namespace MLAudio
{

/// <summary>	Audio Manager Log Name. </summary>
#define ML_LOG_AUDIO_MANAGER ML_STR("Audio Manager")

///-------------------------------------------------------------------------------------------------
/// <summary>	Macro to assist with testing openAL errors
///	Works by calling ALError(alfunction(params));
///	the macros will auto return a false from the function
///	the first Al error. This will only work in boolean
///	return functions. </summary>
///
/// <remarks>	Chris, 02/11/2014. </remarks>
///
/// <param name="x">	The OpenAL function to check for error. </param>
///-------------------------------------------------------------------------------------------------
#define ALError(x) {													\
						x;												\
						if (MLAudioManager::ALFailed(__LINE__))			\
							return ML_FALSE;							\
					}													\

	///-------------------------------------------------------------------------------------------------
	/// <summary>	Provides so helper functions in a static
	///	object for both openAL and FFMPEG. </summary>
	///
	/// <remarks>	Chris, 02/11/2014. </remarks>
	///-------------------------------------------------------------------------------------------------
	class MLAudioManager
	{
	public:
	
		///-------------------------------------------------------------------------------------------------
		/// <summary>	This will Initalise the openAL audio Library
		/// and will also Initalise all the ffmpeg codecs. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL InitaliseAudio();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Release OpenAL object. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL ReleaseAudio();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Releases an ffmpeg codec. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="codec">	The codec object to release. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL ReleaseCodec(MLCodec* codec);
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Sets the default listener object for openAL. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetListener();
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	This will set the Audio Listener which is
		///	core to creating 2D(3D) sound. The position can be updated
		///	on its own in SetListenerPosition but if all the values of
		///	the Listener need to be set they can be done through this function. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="listener">	The new value for the listener. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetListener(const MLAudio::MLAudioListener& listener);
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will set the listener Position only. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="position">	The new position of the listener. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetListenerPosition(const MLMath::MLVec3::MLVector3f& position);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will set the source parameters for an OpenAL
		///	source. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="val">	The new volume for the listener. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetListenerVolume(const ML_FLOAT val);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will set the source parameters for an OpenAL
		///	source. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="source">	  	The openAL Source ID for the Audio. </param>
		/// <param name="audioSource">	The audio source playback information. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL SetSourceSettings(const ALuint& source, const MLAudioSource& audioSource);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will check to see if the openAL error
		///	handler captured an error from a previous openAL call.
		///	Line is an optional line number parameter. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="line">	The line where the error may have occured. </param>
		///
		/// <returns>	Returns true if successful. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static ML_BOOL ALFailed(const ML_INT line);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Will load an audio file with FFMPEG and
		///	return a codec object which cant be used to get the audio data. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="input">	The audio file to open as a virtual File Structure. </param>
		///
		/// <returns>	null if it fails, else the audio codec for the audio file. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static MLCodec* LoadAudioFile(const MLKernel::MLVirtualFile* input);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Processes a audio frame to an audio frame
		///	object which contains the decoded audio data in PCM s16
		///	with the audio channels. </summary>
		///
		/// <remarks>	Chris, 02/11/2014. </remarks>
		///
		/// <param name="InputFormat">	The channel format of the audio data. </param>
		/// <param name="InputCodec"> 	The codec to use to decode the audio. </param>
		/// <param name="InputFrame"> 	The audio frame data to decode. </param>
		///
		/// <returns>	null if it fails, else returns a decoded audio frame structure. </returns>
		///-------------------------------------------------------------------------------------------------
		MLCoreDLL static MLAudioFrame* ProcessAudioFrame(const ML_UINT InputFormat, const MLCodec* InputCodec, const AVFrame* InputFrame);

	private:

		//Contructor
		MLCoreDLL MLAudioManager(void) {}
		//Deconstructor
		MLCoreDLL virtual ~MLAudioManager(void) {}

	private:

		/// <summary>	OpenAL context. </summary>
		MLCoreDLL static ALCcontext* m_context;
		/// <summary>	OpenAL device to playback on. </summary>
		MLCoreDLL static ALCdevice*  m_device;
	};
}


#endif