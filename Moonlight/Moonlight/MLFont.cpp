#include "MLFont.h"


namespace MLRender
{
	MLFont::MLFont(void)
	{
		m_type = MLFontType::NotLoaded;
	}
		
	MLFont::~MLFont(void)
	{
		//release if it has not been called already
		Release();
	}

	ML_BOOL MLFont::Release()
	{
		if(m_glyphSet.size() > 0)
			m_glyphSet.clear();
		if(m_fontTextures.size() > 0)
		{
			for (ML_UINT i = 0; i < m_fontTextures.size(); i++)
			{
				MLTextureManager::RemoveTexture(m_fontTextures[i]);
			}

			m_fontTextures.clear();
		}

		return ML_TRUE;
	}

	MLGlyph MLFont::GetGlyph(const ML_SCHAR& character) const
	{
		std::map<ML_SCHAR, MLGlyph>::const_iterator it;
		it = m_glyphSet.find(character);
		if(it == m_glyphSet.end())
			return m_unknowGlyph;
		else
			return it->second;
	}

	ML_STRING MLFont::WrapText(ML_UINT lineWidth, ML_STRING& input)
	{
		if(m_glyphSet.size() == 0)
			return input;
		//check to see if the string is not empty first
		if(!input.empty() && input.length() != 0)
		{
			ML_SCHAR* string = new ML_SCHAR[input.length()];

			ML_STRING result;

			//check to see if the font size is less than the width
			if(m_fontSize <= lineWidth)
			{
				ML_UINT c = 0;
				ML_FLOAT advance = 0;
				//loop through the entire string
				while(c < input.length())
				{
					//reset the advance after every new line
					//and check to see a single glyph is greater than the
					//line width
					advance = 0;

					if(string[c] != ML_STR('\n'))
						advance += GetGlyph(input[c]).advance;

					if (advance > lineWidth)
						return input;

					//loop through until the advance becomes greater than
					//the line width
					while(advance <= (ML_FLOAT)lineWidth)
					{
						//check the counter has not reached the end of the string
						if(c == input.length())
						{
							result = string;
							return result;
						}

						//copy over the character to the result
						string[c] = input[c];

						//if the character is a new line then will need to
						//reset the advance back to the start
						if(string[c] == ML_STR('\n'))
						{
							//increase the counter
							c++;
							if(c == input.length())
							{
								result = string;
								return result;
							}	

							advance = 0;
							//if the character is not a new line find the next
							//advance
							if(input[c] != ML_STR('\n'))
								advance += GetGlyph(input[c]).advance;
						}
						else
						{
							//increase the counter
							c++;
							if(c == input.length())
							{
								string[input.length()-1] = ML_STR('\0');
								result = string;
								return result;
							}

							//check to see the next character is not a new line
							if(input[c] != ML_STR('\n'))
								advance += GetGlyph(input[c]).advance;
						}
					}

					//if the line width was reached but the next character was white space
					//then a new line can be created.
					if(isspace(input[c]))
					{
						string[c] =  ML_STR('\n');
						c++;
					}
					else
					{
						ML_BOOL foundSpace = ML_FALSE;
						// check for nearest whitespace back in string
						ML_FLOAT inverseAdvance = 400;
						for (ML_UINT c2 = c; c2 > 0; c2--)
						{
							if (isspace(input[c2]))
							{
								string[c2] =  ML_STR('\n');

								c = c2 + 1;
								// set string index back to character after this one
								foundSpace = ML_TRUE;
								break;
							}
							else
							{
								inverseAdvance -= GetGlyph(input[c2]).advance;
							}

							if(inverseAdvance <= 0)
								break;
						}
						//if no white space could be found looping back then just add a new
						//line any way to allow the wrapping to still work
						if(foundSpace == ML_FALSE)
						{
							string[c] =  ML_STR('\n');
							input.insert(input.begin() + c, string[c]);
							c++;
						}
					}
				}
				result = string;
				return result;
			}
			else
			{
				return input;
			}

			delete[] string;
		}
		else
		{
			return input;
		}	
	}

	ML_FLOAT MLFont::GetTextWidth(const ML_STRING& input)
	{
		if(m_glyphSet.size() == 0)
			return 0.0f;
		//check to see if the string is not empty first
		if(!input.empty())
		{
			ML_FLOAT maxWidth		= 0.0f;
			ML_FLOAT widthCounter	= 0.0f;
			for (ML_UINT i = 0; i < input.length(); i++)
			{
				if(input[i] == ML_STR('\n'))
				{
					if(widthCounter > maxWidth)
						maxWidth = widthCounter;
					widthCounter = 0.0f;
				}
				else
				{
					MLGlyph glyph = GetGlyph(input[i]);
					widthCounter+= glyph.advance;
				}
			}

			if(widthCounter > maxWidth)
				maxWidth = widthCounter;
			return maxWidth;
		}

		return 0.0f;
	}

	ML_FLOAT MLFont::GetLineWidth(ML_UINT start, const ML_STRING& input)
	{
		if(m_glyphSet.size() == 0)
			return 0.0f;
		if(start > input.length())
			return 0.0f;
		//check to see if the string is not empty first
		if(!input.empty())
		{
			ML_FLOAT widthCounter	= 0.0f;
			for (ML_UINT i = start; i < input.length(); i++)
			{
				if(input[i] == ML_STR('\n'))
					return widthCounter;
				else
					widthCounter+= GetGlyph(input[i]).advance;
			}

			return widthCounter;
		}

		return 0.0f;
	}

	ML_FLOAT MLFont::GetSubStringWidth(ML_UINT start, ML_UINT end, ML_UINT& nextLine, const ML_STRING& input)
	{
		if(m_glyphSet.size() == 0)
			return 0.0f;
		if(start > input.length() && end < input.length())
			return 0.0f;
		//check to see if the string is not empty first
		if(!input.empty())
		{
			ML_FLOAT widthCounter	= 0.0f;
			for (ML_UINT i = start; i < end+1; i++)
			{
				if(input[i] == ML_STR('\n'))
				{
					nextLine = ++i;
					return widthCounter;
				}
				else
					widthCounter+= GetGlyph(input[i]).advance;
			}

			nextLine = 0;
			return widthCounter;
		}

		return 0.0f;
	}

	ML_FLOAT MLFont::GetTextHeight(const ML_STRING& input)
	{
		if(m_glyphSet.size() == 0)
			return 0.0f;
		//check to see if the string is not empty first
		if(!input.empty())
		{
			ML_FLOAT HeightCounter	= (ML_FLOAT)m_fontSize;
			for (ML_UINT i = 0; i < input.length(); i++)
			{
				MLGlyph glyph = GetGlyph(input[i]);
				if(input[i] == ML_STR('\n'))
				{
					HeightCounter += m_fontSize;
				}
			}

			return HeightCounter;
		}

		return 0.0f;
	}
}