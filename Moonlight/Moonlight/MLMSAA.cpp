#include "MLMSAA.h"

namespace MLRender
{

	MLMSAA::MLMSAA(ML_UINT Amount) : MLRenderTarget()
	{
		m_msaaAmount = Amount;
		m_resolveFBO = NULL;
	}


	MLMSAA::~MLMSAA(void)
	{
		Release();
	}

	MLCoreDLL void MLMSAA::Release()
	{
		MLTextureManager::RemoveTexture(m_outputTexture);
		MLTextureManager::RemoveTexture(m_msaaTexture);
		MLRELEASE(m_FBO);
		MLRELEASE(m_resolveFBO);
	}

	MLCoreDLL void MLMSAA::Initalise(const MLFBOTypes enableFBO)
	{
		m_outputTexture = MLTextureManager::CreateTexture();
		m_outputTexture->CreateEmptyTexture(1280, 720); //THIS SHOULD BE SCREEN X AND Y!!!

		m_msaaTexture = MLTextureManager::CreateTexture();
		m_msaaTexture->CreateEmptyTexture(1280, 720, 8);

		m_FBO = new MLFrameBuffer();
		m_FBO->CreateBuffer(GL_FRAMEBUFFER);
		m_FBO->Bind();

		if(enableFBO == MLFBOTypes::ColourBuffer)
			m_FBO->AttachMSTexture(m_msaaTexture, GL_COLOR_ATTACHMENT0);
		else if(enableFBO == MLFBOTypes::StencilBuffer)
			m_FBO->AttachMSTexture(m_msaaTexture, GL_STENCIL_ATTACHMENT);
		else if(enableFBO == MLFBOTypes::DepthBuffer)
			m_FBO->AttachMSTexture(m_msaaTexture, GL_DEPTH_ATTACHMENT);

		if(m_FBO)
		{
			if(m_FBO->GetStatus() == GL_FRAMEBUFFER_COMPLETE)
				ML_TRACE(ML_LOG_MSAA, MLKernel::MLConsoleStatus::Information, ML_STR("Frame Buffer Status OK"));
			m_FBO->UnBind();
		}

		m_resolveFBO = new MLFrameBuffer();
		m_resolveFBO->CreateBuffer(GL_FRAMEBUFFER);
		m_resolveFBO->Bind();

		if(enableFBO == MLFBOTypes::ColourBuffer)
			m_resolveFBO->AttachMSTexture(m_outputTexture, GL_COLOR_ATTACHMENT0);
		else if(enableFBO == MLFBOTypes::StencilBuffer)
			m_resolveFBO->AttachMSTexture(m_outputTexture, GL_STENCIL_ATTACHMENT);
		else if(enableFBO == MLFBOTypes::DepthBuffer)
			m_resolveFBO->AttachMSTexture(m_outputTexture, GL_DEPTH_ATTACHMENT);

		if(m_resolveFBO)
		{
			if(m_resolveFBO->GetStatus() == GL_FRAMEBUFFER_COMPLETE)
				ML_TRACE(ML_LOG_MSAA, MLKernel::MLConsoleStatus::Information, ML_STR("Frame Buffer Status OK"));
			m_resolveFBO->UnBind();
		}
	}

	MLCoreDLL void MLMSAA::Draw(MLGraphics* batch)
	{
		//CLEAR FRAME

		m_FBO->UnBind();

		m_FBO->Bind(GL_READ_FRAMEBUFFER);
		m_resolveFBO->Bind(GL_DRAW_FRAMEBUFFER);
		m_resolveFBO->Blit(1280, 720);
		m_resolveFBO->UnBind();

		//START SPRITEBATCH

		MLSprite sprite;
		sprite.Texture = m_outputTexture;
		sprite.SetSizeFromTexture();
		sprite.SetSourceFromTexture();

		batch->Draw(&sprite);

		//ENDSPRITEBATCH
	}

}
