#include "MLTexture.h"

namespace MLRender
{

MLTexture::MLTexture(void)
	{
		m_textureID = ML_INVALID_TEXTURE_ID;
		m_data = NULL;
	}

	MLTexture::~MLTexture(void)
	{
	}


	ML_BOOL MLTexture::Release()
	{
		return ML_TRUE;
	}


	ML_BOOL MLTexture::LoadTexture(const ML_U8STRING& Path)
	{
		//The image format variable for free image
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
		//The bitmap of the image	
		FIBITMAP* fib = NULL;
		//the raw bytes of the image
		BYTE* bits = NULL;

		//the width and height of the image
		unsigned ML_INT width = 0, height = 0;

		//get the filetype from the path
		fif = FreeImage_GetFileType(Path.c_str());

		//if the file type is unknown for some reason see if it can
		//be opatined from the file path itself
		if (fif == FIF_UNKNOWN)
			fif = FreeImage_GetFIFFromFilename(Path.c_str());
		//if not then the file must be unsupported
		if (fif == FIF_UNKNOWN)
			return 0;

		//if the file is readable by free image the load it
		if (FreeImage_FIFSupportsReading(fif))
			fib = FreeImage_Load(fif, Path.c_str());
		if (!fib)
			return ML_FALSE;

		if(FreeImage_GetBPP(fib) != 32)
		{
			fib = FreeImage_ConvertTo32Bits(fib);
		}

		//flip the image as bitmaps are upsidedown
		FreeImage_FlipVertical(fib);
		//get the bits
		bits = FreeImage_GetBits(fib);
		//get the width
		width = FreeImage_GetWidth(fib);
		//get the height
		height = FreeImage_GetHeight(fib);

		//if there are any errors with loading the file then the data is not loaded
		if((bits == 0) || (width == 0) || (height == 0))
			return ML_FALSE;

		StoreTexture(bits, width, height, GL_BGRA);

		//unload the file
		FreeImage_Unload(fib);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLTexture::StartEditing()
	{
	
		if(m_textureType = GL_TEXTURE_2D)
		{
			m_data = new MLPixelRGBA32[m_textureWidth * m_textureHeight];
			glBindTexture(GL_TEXTURE_2D, m_textureID);
			glGetTexImage(GL_TEXTURE_2D, 0,  GL_RGBA, GL_UNSIGNED_BYTE, m_data);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		else
		{
			return ML_FALSE;
		}

		m_editing = ML_TRUE;
		m_editFormat = MLPixelFormat::RGBA32;

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLTexture::Convertto32Bit()
	{
		if(m_editing && m_data != NULL)
		{
			MLPixelRGBA32* newData = new MLPixelRGBA32[m_textureWidth*m_textureHeight];
			if (m_editFormat == MLPixelFormat::RGB24)
			{
				MLPixelRGB24* RGBdata = (MLPixelRGB24*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						newData[(y * m_textureWidth) + x].R = RGBdata[(y * m_textureWidth) + x].R;
						newData[(y * m_textureWidth) + x].G = RGBdata[(y * m_textureWidth) + x].G;
						newData[(y * m_textureWidth) + x].B = RGBdata[(y * m_textureWidth) + x].B;
						newData[(y * m_textureWidth) + x].A = 0;
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::RGBA32;
				return ML_TRUE;
			}
			else if (m_editFormat == MLPixelFormat::SingleChannel8)
			{
				MLPixelChannel8* GrayScaledata = (MLPixelChannel8*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						newData[(y * m_textureWidth) + x].R = GrayScaledata[(y * m_textureWidth) + x];
						newData[(y * m_textureWidth) + x].G = GrayScaledata[(y * m_textureWidth) + x];
						newData[(y * m_textureWidth) + x].B = GrayScaledata[(y * m_textureWidth) + x];
						newData[(y * m_textureWidth) + x].A = 0;
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::RGBA32;
				return ML_TRUE;
			}
			else if (m_editFormat != MLPixelFormat::RGBA32)
			{
				delete[] newData;
				return ML_TRUE;
			}

			delete[] newData;
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLTexture::Convertto24Bit()
	{
		if(m_editing && m_data != NULL)
		{
			MLPixelRGB24* newData = new MLPixelRGB24[m_textureWidth*m_textureHeight];
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				MLPixelRGBA32* RGBAdata = (MLPixelRGBA32*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						newData[(y * m_textureWidth) + x].R = RGBAdata[(y * m_textureWidth) + x].R;
						newData[(y * m_textureWidth) + x].G = RGBAdata[(y * m_textureWidth) + x].G;
						newData[(y * m_textureWidth) + x].B = RGBAdata[(y * m_textureWidth) + x].B;
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::RGB24;
				return ML_TRUE;
			}
			else if (m_editFormat == MLPixelFormat::SingleChannel8)
			{
				MLPixelChannel8* GrayScaledata = (MLPixelChannel8*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						newData[(y * m_textureWidth) + x].R = GrayScaledata[(y * m_textureWidth) + x];
						newData[(y * m_textureWidth) + x].G = GrayScaledata[(y * m_textureWidth) + x];
						newData[(y * m_textureWidth) + x].B = GrayScaledata[(y * m_textureWidth) + x];
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::RGB24;
				return ML_TRUE;
			}
			else if (m_editFormat != MLPixelFormat::RGB24)
			{
				delete[] newData;
				return ML_TRUE;
			}
	
			delete[] newData;
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLTexture::ConverttoGrayScale()
	{
		if(m_editing && m_data != NULL)
		{
			MLPixelChannel8* newData = new MLPixelChannel8[m_textureWidth*m_textureHeight];
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				MLPixelRGBA32* RGBAdata = (MLPixelRGBA32*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						ML_CHAR weightedR = (ML_CHAR)(0.2126f * RGBAdata[(y * m_textureWidth) + x].R);
						ML_CHAR weightedG = (ML_CHAR)(0.7152f * RGBAdata[(y * m_textureWidth) + x].G);
						ML_CHAR weightedB = (ML_CHAR)(0.0722f * RGBAdata[(y * m_textureWidth) + x].B);
						newData[(y * m_textureWidth) + x] = (weightedR + weightedG + weightedB);
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::SingleChannel8;
				return ML_TRUE;
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				MLPixelRGB24* RGBdata = (MLPixelRGB24*)m_data;
				for(ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for (ML_UINT x = 0; x < m_textureWidth; x++)
					{
						ML_CHAR weightedR = (ML_CHAR)(0.2126f * RGBdata[(y * m_textureWidth) + x].R);
						ML_CHAR weightedG = (ML_CHAR)(0.7152f * RGBdata[(y * m_textureWidth) + x].G);
						ML_CHAR weightedB = (ML_CHAR)(0.0722f * RGBdata[(y * m_textureWidth) + x].B);
						newData[(y * m_textureWidth) + x] = weightedR + weightedG + weightedB;
					}
				}

				delete[] m_data;
				m_data = newData;
				m_editFormat = MLPixelFormat::SingleChannel8;
				return ML_TRUE;
			}
			else if (m_editFormat != MLPixelFormat::SingleChannel8)
			{
				delete[] newData;
				return ML_TRUE;
			}

			delete[] newData;
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLTexture::FlipImageVert()
	{
		if(m_editing && m_data != NULL)
		{
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				void* RGBAdata;
				RGBAdata = flipVertTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGBA32);

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				void* RGBAdata;
				RGBAdata = flipVertTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGB24);

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat != MLPixelFormat::SingleChannel8)
			{
				void* RGBAdata;
				RGBAdata = flipVertTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::SingleChannel8);

				delete[] m_data;
				m_data = RGBAdata;
			}
	
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLTexture::FlipImageHoriz()
	{
		if(m_editing && m_data != NULL)
		{
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				void* RGBAdata;
				RGBAdata = flipHorizTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGBA32);

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				void* RGBAdata;
				RGBAdata = flipHorizTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGB24);

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat != MLPixelFormat::SingleChannel8)
			{
				void* RGBAdata;
				RGBAdata = flipHorizTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::SingleChannel8);

				delete[] m_data;
				m_data = RGBAdata;
			}

			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLTexture::RotateImage()
	{
		if(m_editing && m_data != NULL)
		{
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				void* RGBAdata;
				RGBAdata = rotateTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGBA32);
				ML_INT oldHeight = m_textureHeight;
				m_textureHeight = m_textureWidth;
				m_textureWidth = oldHeight;

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				void* RGBAdata;
				RGBAdata = rotateTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::RGB24);
				ML_INT oldHeight = m_textureHeight;
				m_textureHeight = m_textureWidth;
				m_textureWidth = oldHeight;

				delete[] m_data;
				m_data = RGBAdata;
			}
			else if (m_editFormat != MLPixelFormat::SingleChannel8)
			{
				void* RGBAdata;
				RGBAdata = rotateTexture(m_data, m_textureWidth, m_textureHeight, MLPixelFormat::SingleChannel8);
				ML_INT oldHeight = m_textureHeight;
				m_textureHeight = m_textureWidth;
				m_textureWidth = oldHeight;

				delete[] m_data;
				m_data = RGBAdata;
			}

			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLTexture::ExportImage(const ML_U8STRING& location, ML_BYTE* Data, const MLPixelFormat& format)
	{
		m_editFormat = format;
		m_editing = ML_TRUE;
		m_data = Data;
		ML_BOOL result = ExportImage(location);
		m_editing = ML_FALSE;

		return result;
	}

	ML_BOOL MLTexture::ExportImage(const ML_U8STRING& location)
	{
		if(m_editing && m_data != NULL)
		{
			FIBITMAP* Image = new FIBITMAP;

			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				Image = FreeImage_Allocate(m_textureWidth, m_textureHeight, 32);

				RGBQUAD colour; 

				MLPixelRGBA32* data = (MLPixelRGBA32*)m_data;

				for (ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for(ML_UINT x = 0; x < m_textureWidth; x++)
					{
						colour.rgbRed = data[y * m_textureWidth + x].R;
						colour.rgbGreen = data[y * m_textureWidth + x].G;
						colour.rgbBlue = data[y * m_textureWidth + x].B;
						colour.rgbReserved = data[y * m_textureWidth + x].A;
						FreeImage_SetPixelColor(Image, x, y, &colour);
					}
				}

				FreeImage_Save(FIF_PNG, Image, location.c_str());

				FreeImage_Unload(Image);

				return ML_TRUE;
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				Image = FreeImage_Allocate(m_textureWidth, m_textureHeight, 24);

				RGBQUAD colour; 

				MLPixelRGB24* data = (MLPixelRGB24*)m_data;

				for (ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for(ML_UINT x = 0; x < m_textureWidth; x++)
					{
						colour.rgbRed = data[y * m_textureWidth + x].R;
						colour.rgbGreen = data[y * m_textureWidth + x].G;
						colour.rgbBlue = data[y * m_textureWidth + x].B;
						FreeImage_SetPixelColor(Image, x, y, &colour);
					}
				}

				FreeImage_Save(FIF_PNG, Image, location.c_str());

				FreeImage_Unload(Image);

				return ML_TRUE;
			}
			else if (m_editFormat == MLPixelFormat::SingleChannel8)
			{
				Image = FreeImage_Allocate(m_textureWidth, m_textureHeight, 24);

				RGBQUAD colour; 

				MLPixelChannel8* data = (MLPixelChannel8*)m_data;

				for (ML_UINT y = 0; y < m_textureHeight; y++)
				{
					for(ML_UINT x = 0; x < m_textureWidth; x++)
					{
						colour.rgbRed = data[y * m_textureWidth + x];
						colour.rgbGreen = data[y * m_textureWidth + x];
						colour.rgbBlue = data[y * m_textureWidth + x];
						FreeImage_SetPixelColor(Image, x, y, &colour);
					}
				}

				FreeImage_Save(FIF_PNG, Image, location.c_str());

				FreeImage_Unload(Image);

				return ML_TRUE;
			}
		}

		return ML_FALSE;
	}

	ML_BOOL MLTexture::EndEditing()
	{
		if(m_editing && m_data != NULL)
		{
			if (m_editFormat == MLPixelFormat::RGBA32)
			{
				if(m_textureType = GL_TEXTURE_2D)
				{
					glBindTexture(GL_TEXTURE_2D, m_textureID);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_textureWidth, m_textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glBindTexture(GL_TEXTURE_2D, 0);
				}
			}
			else if (m_editFormat == MLPixelFormat::RGB24)
			{
				if(m_textureType = GL_TEXTURE_2D)
				{
					glBindTexture(GL_TEXTURE_2D, m_textureID);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_textureWidth, m_textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, m_data);
					glBindTexture(GL_TEXTURE_2D, 0);
				}
			}
			else if (m_editFormat == MLPixelFormat::SingleChannel8)
			{
				if(m_textureType = GL_TEXTURE_2D)
				{
					glBindTexture(GL_TEXTURE_2D, m_textureID);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_textureWidth, m_textureHeight, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_data);
					glBindTexture(GL_TEXTURE_2D, 0);
				}
			}

			delete[] m_data;
			m_data = NULL;

			m_editing = ML_FALSE;

			return ML_TRUE;
		}

		return ML_FALSE;
	}

	//=========================================================
	// Name: Load Raw Bytes
	// Description: Will create a texture from a raw 8bit data source
	// with each 8bits is a single pixel
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL ML_BOOL MLTexture::StoreTexture(const ML_BYTE* data, const ML_INT width, const ML_INT height, const ML_INT imageFormat)
	{

		//generate a textureID
		glGenTextures(1, &m_textureID);
		//Bind the texture
		glBindTexture(GL_TEXTURE_2D, m_textureID);

		//set the texture data
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, imageFormat, GL_UNSIGNED_BYTE, data);

		//set filters
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//unbind textre
		glBindTexture(GL_TEXTURE_2D, 0);

		//add the texture to the texture map
		m_textureWidth		= width;
		m_textureHeight		= height;
		m_textureType		= GL_TEXTURE_2D;
		//return the textureID
		return ML_TRUE;
	}


	MLCoreDLL ML_BOOL MLTexture::CreateEmptyTexture(const ML_INT width, const ML_INT height, const ML_INT samples)
	{
		//generate a textureID
		glGenTextures(1, &m_textureID);
		if (samples == DEFAULT_INVALID_SAMPLES)
		{
			//Bind the texture
			glBindTexture(GL_TEXTURE_2D, m_textureID);

			//set the texture data
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
			glBindTexture(GL_TEXTURE_2D, 0);

			m_textureType = GL_TEXTURE_2D;
		}
		else if(samples >= 0)
		{
			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_textureID);

			//set the texture data
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_RGBA, width, height, ML_TRUE);

			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
		
			m_textureType = GL_TEXTURE_2D_MULTISAMPLE;
		}
		else
		{
			return ML_FALSE;
		}
	
		//add the texture to the texture map
		m_textureWidth		= width;
		m_textureHeight		= height;

		return ML_TRUE;
	}




	MLCoreDLL void* rotateTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format)
	{
		switch(format)
		{
		case MLPixelFormat::RGBA32: return rotateRGBA32((MLPixelRGBA32*)data, width, height); break;
		case MLPixelFormat::BGRA32: return rotateBGRA32((MLPixelBGRA32*)data, width, height); break;
		case MLPixelFormat::RGB24:  return rotateRGB24((MLPixelRGB24*)data, width, height); break;
		case MLPixelFormat::BGR24:  return rotateBGR24((MLPixelBGR24*)data, width, height); break;
		case MLPixelFormat::LuminanceAlpha16:	return rotateLumiAlpha16((MLPixelLumiAlpha16*)data, width, height); break;
		case MLPixelFormat::SingleChannel8:		return rotateChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::RedChannel8:		return rotateChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::GreenChannel8:		return rotateChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::BlueChannel8:		return rotateChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::AlphaChannel8:		return rotateChannel8((MLPixelChannel8*)data, width, height); break;
		}

		return NULL;
	}

	MLCoreDLL void* rotateRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height)
	{
		MLPixelRGBA32* newData = new MLPixelRGBA32[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}

	MLCoreDLL void* rotateBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height)
	{
		MLPixelBGRA32* newData = new MLPixelBGRA32[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}

	MLCoreDLL void* rotateRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height)
	{
		MLPixelRGB24* newData = new MLPixelRGB24[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}

	MLCoreDLL void* rotateBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height)
	{
		MLPixelBGR24* newData = new MLPixelBGR24[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}

	MLCoreDLL void* rotateLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height)
	{
		MLPixelLumiAlpha16* newData = new MLPixelLumiAlpha16[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}

	MLCoreDLL void* rotateChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height)
	{
		MLPixelChannel8* newData = new MLPixelChannel8[height * width];

		ML_INT x2 = 0, y2 = 0; 
		for (ML_INT y1 = 0; y1 < height; y1++)
		{
			for (ML_INT x1 = 0; x1 < width; x1++)
			{
				newData[(x2 * height) + y2] = data[(y1 * width) + x1];
				x2++;
			}
			x2 = 0;
			y2++;
		}

		return newData;
	}


	MLCoreDLL void* flipVertTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format)
	{
		switch(format)
		{
		case MLPixelFormat::RGBA32: return flipVertRGBA32((MLPixelRGBA32*)data, width, height); break;
		case MLPixelFormat::BGRA32: return flipVertBGRA32((MLPixelBGRA32*)data, width, height); break;
		case MLPixelFormat::RGB24:  return flipVertRGB24((MLPixelRGB24*)data, width, height); break;
		case MLPixelFormat::BGR24:  return flipVertBGR24((MLPixelBGR24*)data, width, height); break;
		case MLPixelFormat::LuminanceAlpha16:	return flipVertLumiAlpha16((MLPixelLumiAlpha16*)data, width, height); break;
		case MLPixelFormat::SingleChannel8:		return flipVertChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::RedChannel8:		return flipVertChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::GreenChannel8:		return flipVertChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::BlueChannel8:		return flipVertChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::AlphaChannel8:		return flipVertChannel8((MLPixelChannel8*)data, width, height); break;
		}

		return NULL;
	}

	MLCoreDLL void* flipVertRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height)
	{
		MLPixelRGBA32* newData = new MLPixelRGBA32[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipVertRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height)
	{
		MLPixelRGB24* newData = new MLPixelRGB24[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipVertBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height)
	{
		MLPixelBGRA32* newData = new MLPixelBGRA32[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipVertBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height)
	{
		MLPixelBGR24* newData = new MLPixelBGR24[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipVertLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height)
	{
		MLPixelLumiAlpha16* newData = new MLPixelLumiAlpha16[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipVertChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height)
	{
		MLPixelChannel8* newData = new MLPixelChannel8[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[(y * width) + (width - x)];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizTexture(void* data, ML_INT width, ML_INT height, MLPixelFormat format)
	{
		switch(format)
		{
		case MLPixelFormat::RGBA32: return flipHorizRGBA32((MLPixelRGBA32*)data, width, height); break;
		case MLPixelFormat::BGRA32: return flipHorizBGRA32((MLPixelBGRA32*)data, width, height); break;
		case MLPixelFormat::RGB24:  return flipHorizRGB24((MLPixelRGB24*)data, width, height); break;
		case MLPixelFormat::BGR24:  return flipHorizBGR24((MLPixelBGR24*)data, width, height); break;
		case MLPixelFormat::LuminanceAlpha16:	return flipHorizLumiAlpha16((MLPixelLumiAlpha16*)data, width, height); break;
		case MLPixelFormat::SingleChannel8:		return flipHorizChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::RedChannel8:		return flipHorizChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::GreenChannel8:		return flipHorizChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::BlueChannel8:		return flipHorizChannel8((MLPixelChannel8*)data, width, height); break;
		case MLPixelFormat::AlphaChannel8:		return flipHorizChannel8((MLPixelChannel8*)data, width, height); break;
		}

		return NULL;
	}

	MLCoreDLL void* flipHorizRGBA32(MLPixelRGBA32* data, ML_INT width, ML_INT height)
	{
		MLPixelRGBA32* newData = new MLPixelRGBA32[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizRGB24(MLPixelRGB24* data, ML_INT width, ML_INT height)
	{
		MLPixelRGB24* newData = new MLPixelRGB24[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizBGRA32(MLPixelBGRA32* data, ML_INT width, ML_INT height)
	{
		MLPixelBGRA32* newData = new MLPixelBGRA32[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizBGR24(MLPixelBGR24* data, ML_INT width, ML_INT height)
	{
		MLPixelBGR24* newData = new MLPixelBGR24[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizLumiAlpha16(MLPixelLumiAlpha16* data, ML_INT width, ML_INT height)
	{
		MLPixelLumiAlpha16* newData = new MLPixelLumiAlpha16[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

	MLCoreDLL void* flipHorizChannel8(MLPixelChannel8* data, ML_INT width, ML_INT height)
	{
		MLPixelChannel8* newData = new MLPixelChannel8[width * height];

		for (ML_INT y = 0; y < height; y++)
		{
			for (ML_INT x = 0; x < width; x++)
			{
				newData[(y * width) + x] = data[((height - y) * width) + x];

			}
		}

		return newData;
	}

}