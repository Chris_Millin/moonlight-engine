#ifndef MLSCREENSHOT_H
#define MLSCREENSHOT_H

#include "MLIncludes.h"
#include "MLTexture.h"

#define ML_DEFAULT_SCREENSHOT_PATH ML_STR("Screenshots")
#define ML_DEFAULT_SCREENSHOT_NAME ML_STR("Moonlight_Scrot_")
#define ML_DEFAULT_SCREENSHOT_TYPE ML_STR(".png")
#define ML_LOG_SCROT ML_STR("Screenshot")

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLScreenshot
	{
	public:

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Screenshot();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Screenshot(const ML_STRING& filePath);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static void Screenshot(const MLKernel::MLVirtualFile* filePath);

	private:
		MLScreenshot(void);
		virtual ~MLScreenshot(void);


	};

}

#endif
