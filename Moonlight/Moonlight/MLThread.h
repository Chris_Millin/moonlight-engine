#ifndef MLTHREAD_H 
#define MLTHREAD_H

#include "MLConsole.h"
#include "MLCore.h"
#include <Psapi.h>
#include <map>

#define ML_MUTEX_TIMEOUT 60000

namespace MLKernel
{
	#define ML_LOG_THREAD ML_STR("Thread")

	//=========================================================
	// Name: Thread Entry 
	// Description: Function pointer to a valid thread entry
	// function.
	// Creator: Chris Millin
	//=========================================================
	typedef ML_DWORD (*ThreadEntry)(void* arg);

	//=========================================================
	// Name: Thread
	// Description: There are two ways this thread class can
	// be used and both are valid methods. The first is to use
	// the class as a standalone object and call the Thread
	// Create function with the thread entry parameter all other
	// thread functionality to manage the thread will work as
	// intended. The second method is to inherit from
	// the thread class and override the threadEntry function
	// which will allow for the class its own internal thread
	// function and manage the thread from within itself as
	// well as from outside of the class. 
	// Creator: Chris Millin
	//=========================================================
	class MLThread
	{
		//=========================================================
		// Name: Friend Thread Entry
		// Description: Used for the inherit based thread management
		// which is the thread entry point from create thread and is
		// past the this object as its parameter. The object pointer
		// is then used to run the internal thread entry function
		// which should be overriden by the inherited class. The
		// only problem with inheriting is that only one instance
		// of the thread entry will be able to run at a time.
		// multiple instances of the same parent object type will
		// allow for multiple thread entries but each will be 
		// independent of each other. though it may be possible
		// to run ThreadCreate with the params to run multiple
		// threads in the single object which inherits from thread.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL friend DWORD _ThreadEntry(void* param);

	public:
		MLCoreDLL MLThread(void);
		MLCoreDLL MLThread(ThreadEntry theadEntry, void* params);
		MLCoreDLL ~MLThread(void);

		//=========================================================
		// Name: Release
		// Description: will attempt to release the thead handle
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Thread Create (no param)
		// Description: Used for inherit based thread management.
		// Call this at any point to run the overridden entry point
		// method if a thread inst already running.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadCreate();

		//=========================================================
		// Name: Thread Create (params)
		// Description: Sets a thread entry point and the params 
		// to pass to it and then creates a thread running the
		// thread entry method.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadCreate(ThreadEntry theadEntry, void* params);
		
		//=========================================================
		// Name: Thread Sleep
		// Description: Sets the amount of time for the current
		// thread to sleep. 
		// Note: Calling this in the main thread will cause the main
		// thread to sleep
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadSleep(const ML_DWORD ms) const;

		//=========================================================
		// Name: Thread Suspend 
		// Description: Will Suspend the thread task until resume
		// is called or the thread is terminated
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadSuspend();

		//=========================================================
		// Name: Thread Resume
		// Description: Will resume the thread task if it has
		// been suspended.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadResume();

		//=========================================================
		// Name: Thread Wait
		// Description: Will wait for the thread based on
		// the amount of time specified. Use INFINATE to wait for
		// the thread to finish
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadWait(const ML_DWORD ms);

		//=========================================================
		// Name: Thread Stop 
		// Description: Will wait for the thread to finish. Uses
		// Thread wait with INFINATE time.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadStop();

		//=========================================================
		// Name: Thread Terminate
		// Description: Will Terminate the thread even if the 
		// thread has not finished its task.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadTerminate();

		//=========================================================
		// Name: Thread Finished
		// Description: Will check to see if the thread has finished
		// its task.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadFinished();

		//=========================================================
		// Name: Thread Set Priority
		// Description: Chance the priority of the thread running.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ThreadSetPriority(const ML_INT p);

		//=========================================================
		// Name: Thread Get Priority
		// Description: Returns the current priority of the thread
		// running.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_INT  ThreadGetPriority();

		//=========================================================
		// Name: Thread Wait Mutex
		// Description: Will open the mutex object defined by
		// the name mutexName and will wait for it to be free or
		// can specify a time out value if the mutex could not
		// be obtained. 
		// Used the MutexDictionary class to generate a valid
		// mutex to obtain for the function otherwise it will fail
		// be careful with alphanumeric case values as they are
		// checked.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static ThreadWaitMutex(const ML_STRING& mutexName, ML_DWORD ms = INFINITE);

		//=========================================================
		// Name: Thread Release Mutex
		// Description: If a mutex has been opened with ThreadWait Mutex
		// then when the thread has finished the critical section
		// the mutex should be release for other threads to use.
		// Same as WaitMutex this is case sensitive with the name.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static ThreadReleaseMutex(const ML_STRING& mutexName);

		//=========================================================
		// Name: Thread Get Thread ID
		// Description: returns the current threadID value.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_DWORD ThreadGetThreadID() { return m_threadID; }

	protected:

		//=========================================================
		// Name: Thread Entry
		// Description: The virtual entry point for a class that
		// inherits from thread. This will be called once the
		// thread is created.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void threadEntry() {}

	private:

		//=========================================================
		// Name: Valid Thread Handle
		// Description: Checks to see if the m_ThreadHandle object
		// is currently active or not and will release it if not
		// done so already if inactive.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ValidThreadHandle();

	private:

		//The thread handler used to obtain the thread
		DWORD* m_threadHandle;
		//ID for the thread object
		DWORD m_threadID;
		//Stores if the thread has finished
		ML_BOOL m_threadFinished;
	};

	//=========================================================
	// Name: Mutex Dictionary
	// Description: Stores a Dictionary of all the mutex
	// object along with their handles. This is a static
	// class so mutex object can be created anywhere and is
	// also in the same header as thread to ensure its always
	// available with the thread. Mutexs are also defined by
	// a name to make it easy to read which mutex is which.
	// duplicate name mutex's are not allowed.
	// Creator: Chris Millin
	//=========================================================
	class MLMutexDictionary
	{
	public:

		//=========================================================
		// Name: Add Mutex
		// Description: Will add a mutex with the mutexName
		// and store it in the dictionary. If any other mutex
		// with the same name exists it will be closed and removed
		// so the new one can be added.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL AddMutex(const ML_STRING& mutexName);

		//=========================================================
		// Name: Get Handler
		// Description: Returns the mutex handle object for the
		// specified mutex.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static DWORD*	GetHandler(const ML_STRING& mutexName) { return m_mutexStack[mutexName]; }

		//=========================================================
		// Name: Remove Mutex
		// Description: Will remove the mutex from the dicitionary
		// also closing it at the same time.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveMutex(const ML_STRING& mutexName);

	private:
		
		//Mutex dictionary store.
		MLCoreDLL static std::map<ML_STRING, DWORD*> m_mutexStack;
	};
	
}

extern "C"
{
	//=========================================================
	// Name: Thread Entry 
	// Description: The actual thread entry point for a
	// inherited thread object. Will take the param of MLThread
	// and then call the threadEntry function. Is a friend
	// function so has access to all methods and members.
	// Do any thread pre-initalising here if required later.
	// Creator: Chris Millin
	//=========================================================
	MLCoreDLL DWORD _ThreadEntry(void* param);
};

#endif