#ifndef MLSPRITEBATCH_H
#define MLSPRITEBATCH_H

#include "MLFormats.h"
#include "MLShader.h"
#include "MLFont.h"
#include "MLVertexBuffer.h"
#include "MLTextureManager.h"
#include "MLParticalEmitter.h"
#include <algorithm>
#include <vector>
#include <stdarg.h>
#include "MLCore.h"
#include "MLCameraManager.h"
#include "windowsHandler.h"


//using MLRenderer::MLSprite;
using MLRenderer::MLShader;
using MLRenderer::MLTextureManager;
using MLRenderer::MLText::MLFont;
using MLMath::MLVector2::Vector2f;
using MLMath::MLVector3::Vector3f;
using MLMath::MLVector4::Vector4f;
using MLMath::MLMat4::Mat4f;
using namespace MLFormats;

#define ML_DEFAULT_BATCH_SIZE 4096
#define ML_BATCH_BUFFER_SIZE (ML_DEFAULT_BATCH_SIZE * 216)

//=========================================================
//Name: MLSpriteBatch
//Namespace: MLRenderer
//Description: This class can be used to render multiple
// different 2D objects such as primitve shapes, sprites
// and text. The sprite batch has been designed to optimise
// rendering if used correctly
//Creator: Chris Millin
//Librarys: OpenGL
//=========================================================

namespace MLRenderer
{

	class MLSpriteBatch
	{
	public:
		
		//Constructor
		MLCoreDLL MLSpriteBatch();
		//Consturctor with a set buffersize
		MLCoreDLL MLSpriteBatch(int bufferSize);
		//Consturctor with custom shader and buffer size
		MLCoreDLL MLSpriteBatch(MLShader* inShader, int buffersize);
		//Deconsturctor
		MLCoreDLL virtual ~MLSpriteBatch(void);
		
		//releases the objects before deletion
		MLCoreDLL bool Release(void);
		//will delete and then create a new batch array with the original size
		MLCoreDLL bool ReShrink(void);

		//This is the fist call to the sprite batcher and preps it ready for rendering
		MLCoreDLL bool Begin(bool Alpha = true, MLFormats::MLSortMode sortType = MLFormats::MLSortMode::Texture, bool wireFrame = false);

		//Draw calls which can be used for rendering a 2D textured sprite
		MLCoreDLL void Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, float rotation, Vector2f& origin, MLFormats::MLSpriteEffects effect = MLFormats::MLSpriteEffects::None, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector4f& Destination, bool camera, Vector4f& Source, float rotation, Vector2f& origin, int layer, MLFormats::MLSpriteEffects effect = MLFormats::MLSpriteEffects::None, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector2f& Position, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, float rotation, Vector2f& origin, Vector2f& scale, MLFormats::MLSpriteEffects effect = MLFormats::MLSpriteEffects::None, Vector4f& Colour = Vector4f(1.0f));
		MLCoreDLL void Draw(GLuint Texture, Vector2f& Position, bool camera, Vector4f& Source, float rotation, Vector2f& origin, int layer, Vector2f& scale, MLFormats::MLSpriteEffects effect = MLFormats::MLSpriteEffects::None, Vector4f& Colour = Vector4f(1.0f));

		//Write call for rendering font onto the screen
		//uses the font class to define the font that it uses to render
		MLCoreDLL void Write(MLFont* font, Vector2f& position, bool camera, std::string& text);
		MLCoreDLL void Write(MLFont* font, Vector2f& position, bool camera, int layer, std::string& text);
		MLCoreDLL void Write(MLFont* font, Vector2f& position, bool camera, const wchar_t* format, ...);
		MLCoreDLL void Write(MLFont* font, Vector2f& position, bool camera, int layer, const wchar_t* format, ...);
		MLCoreDLL void Write(MLFont* font, Vector4f& TextBlock, bool camera, const wchar_t* format, ...);


		//Drawing a point
 		MLCoreDLL void DrawPoint(Vector2f& Position);
		MLCoreDLL void DrawPoint(Vector2f& Position, float size);
		MLCoreDLL void DrawPoint(Vector2f& Position, float size, Vector4f& colour);
		MLCoreDLL void DrawPoint(Vector2f& Position, float size, MLFormats::MLPointType style, Vector4f& colour);
		MLCoreDLL void DrawPoint(Vector2f& Position, float size, MLFormats::MLPointType style, float layer, Vector4f& colour);

		//Drawing a line
 		MLCoreDLL void DrawLine(Vector2f& PosA, Vector2f& PosB);
		MLCoreDLL void DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness);
		MLCoreDLL void DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness, Vector4f& colour);
		MLCoreDLL void DrawLine(Vector2f& PosA, Vector2f& PosB, float lineThinkness, float layer, Vector4f& colour);

		//Drawing a triangle
 		MLCoreDLL void DrawTriangle(Vector2f& Position, float size);
		MLCoreDLL void DrawTriangle(Vector2f& Position, float size, Vector4f& colour);
		MLCoreDLL void DrawTriangle(Vector2f& Position, float size, Vector2f& origin, Vector4f& colour);
		MLCoreDLL void DrawTriangle(Vector2f& Position, Vector2f& size, float rot, Vector4f& colour);
		MLCoreDLL void DrawTriangle(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, Vector4f& colour);
		MLCoreDLL void DrawTriangle(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, float layer, Vector4f& colour);

		//Drawing a quad
 		MLCoreDLL void DrawQuad(Vector2f& Position, float size);
		MLCoreDLL void DrawQuad(Vector2f& Position, float size, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector2f& Position, Vector2f& size, Vector2f& origin, float rot, float layer, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector4f& Destination);
		MLCoreDLL void DrawQuad(Vector4f& Destination, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector4f& Destination, float lineThinkness, Vector4f& colour);
		MLCoreDLL void DrawQuad(Vector4f& Destination, float lineThinkness, float rot, Vector4f& colour) {}
		MLCoreDLL void DrawQuad(Vector4f& Destination, float lineThinkness, float rot, float layer, Vector4f& colour) {}
		//Drawing a circle
		MLCoreDLL void DrawCircle(Vector2f& Position, float radius);
		MLCoreDLL void DrawCircle(Vector2f& Position, float radius, Vector4f& colour);
 		MLCoreDLL void DrawCircle(Vector2f& Position, float radius, float thinkness, int segments);
		MLCoreDLL void DrawCircle(Vector2f& Position, float radius, float thinkness, int segments, Vector4f& colour);
		MLCoreDLL void DrawCircle(Vector2f& Position, Vector2f& size, float thinkness, int segments, float rot, Vector4f& colour);
		MLCoreDLL void DrawCircle(Vector2f& Position, Vector2f& size, float thinkness, int segments, float rot, float Layer, Vector4f& colour) {}

		//Drawing a Filled circle
		MLCoreDLL void DrawCircleFill(Vector2f& Position, float radius);
		MLCoreDLL void DrawCircleFill(Vector2f& Position, float radius, Vector4f& colour);
		MLCoreDLL void DrawCircleFill(Vector2f& Position, float radius, int segments);
		MLCoreDLL void DrawCircleFill(Vector2f& Position, float radius, int segments, Vector4f& colour);
		MLCoreDLL void DrawCircleFill(Vector2f& Position, Vector2f& size, int segments, float rot, Vector4f& colour);

		//Drawing a Vertex array
		MLCoreDLL void DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, Vector4f& colour);
		MLCoreDLL void DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector4f& source, Vector4f& colour);
		MLCoreDLL void DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector4f& source, float rotation, Vector2f& origin, Vector4f& colour);
		MLCoreDLL void DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector2f& source, float rotation, Vector2f& origin, MLFormats::MLSpriteEffects effect, Vector4f& colour);
		MLCoreDLL void DrawVertexArray(GLuint textureID, Vector2f& position, Vector2f* vertices, int vertexCount, bool camera, Vector2f& source, float rotation, Vector2f& origin, int layer, MLFormats::MLSpriteEffects effect, Vector4f& colour);

		//Drawing a Paticle Effect 
		MLCoreDLL void DrawParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect);
		MLCoreDLL void DrawParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect, int layer);

		//This should be the final call which will flush all the sprites and render them all
		MLCoreDLL bool End(void);

	private:

		//initalises the sprite batch object
		MLCoreDLL bool Initalise(void);

		//due to the difference in the way objects are rendered they have been split into their own render functions
		MLCoreDLL bool RenderPoints(GLuint texture, int startIndex, int count);
		MLCoreDLL bool RenderLines(GLuint texture, int startIndex, int count);
		MLCoreDLL bool RenderTriangles(GLuint texture, int startIndex, int count, bool fan);
		MLCoreDLL bool RenderQuads(GLuint texture, int startIndex, int count);
		MLCoreDLL bool RenderVertexArray(GLuint texture, int startIndex, int count);
		MLCoreDLL bool RenderParticleEmitter(GLuint texture, int startIndex, int count);

		//This is the master batch all which will convert all the information into a primitive object which will then be put in the batch array
		MLCoreDLL bool Batch(GLuint Texture, Vector2f Position,  bool camera, Vector4f Source, float rotation, Vector2f origin, int layer, Vector2f size, Vector2f scale, MLFormats::MLSpriteEffects effect, Vector4f Colour, MLFormats::MLPrimitiveTypes Type);
		//due to some of the differences in the way font is handled it is batched in a different function but its still fairly similar
		MLCoreDLL bool BatchChar(MLFont* font, MLRenderer::MLText::MLFontCache* character, bool camera, int xLoc, int yLoc, int layer);
		//batch vertex array
		MLCoreDLL bool BatchVertexArray(GLuint Texture, Vector2f Position, Vector2f* vertices, int vertexCount, bool camera, Vector4f Source, float rotation, Vector2f origin, int layer, Vector2f size, Vector2f scale, MLFormats::MLSpriteEffects effect, Vector4f Colour, MLFormats::MLPrimitiveTypes Type);
		//batch patricle effect
		MLCoreDLL bool BatchParticleEffect(GLuint textureID, Vector2f& position, MLParticleEffect* pEffect, bool camera, int layer);

		//The print which prints the formatted text
		MLCoreDLL bool Print(MLFont* font, Vector4f TextBlock, bool camera, const wchar_t* Text, int layer);

		//Flushes all the batch items so they are ready for rendering
		MLCoreDLL bool Flush(void);

		//sorts the sprite batch 
		MLCoreDLL bool Sort(void);

		//expands the batch if it needs to store more sprites
		MLCoreDLL bool ExpandBatch(void);

		//sorting functions for std::sort
		MLCoreDLL bool static SortTexture(MLBatchItem* x, MLBatchItem* y)		{ return x->GetTexture() < y->GetTexture(); }
		MLCoreDLL bool static SortBacktoFront(MLBatchItem* x, MLBatchItem* y)	{ return x->GetLayer() > y->GetLayer();}
 		MLCoreDLL bool static SortFronttoBack(MLBatchItem* x, MLBatchItem* y)	{ return x->GetLayer() < y->GetLayer();}
	private:
		
		//used to check if begin is called before end
		bool m_beginCall;

		//the vertex buffer that will be used for the batch
		MLVertexBuffer* m_vertexBuffer;

		//sprite size is used for the size of data per sprite so for PosTexCol it will be ((3 * 4byte) + (2 * 4bytes) + (4 * 4bytes)) * 4points
		//This is then used to move the vertex buffer object to the correct position 
		const int m_spriteSize;
		//the buffersize will store the Max size of the vertex buffer buffer 
		const int m_BufferSize;
		//the buffer offset is used to move the buffer so that data can be mapped if a free area of memory
		int m_BufferOffset;

		//amount of sprites added so far
		int m_currentSpriteCount;
		//the current size of the batch (mainly used for expanding the batch);
		int m_currentBatchSize;

		//the shader being used for rendering
		MLShader* m_shader;
		//shader used for rendering primitives
		MLShader* m_primitiveShader;
		//false if the shader is set by the user so that its not deleted when release is called
		bool m_defaultShader;

		GLuint m_blankTexture; //a bland 2x2 white texture to be used for primitive rendering

		//in theory this is all that is needed for the sprite batch to work and 
		//all that is needed is for a sprite to be created each time draw data is batched
		//Though this is a pointer it will be used as a dynamic array for which sprites
		//will be loaded into and stored. This array will disregard order and ordering
		//will be managed by the vector array as it may have better search algorithms
		MLBatchItem* m_sprites;

		//this may be needed just to speed up the search process but i could be wrong
		std::vector<MLBatchItem*> m_spriteQueue;

		MLFormats::MLSortMode m_sortMode;

		bool m_wireFrame;		
	};
}


#endif //MLSPRITEBATCH_H
