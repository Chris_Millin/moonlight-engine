#ifndef MLSHADER_H
#define MLSHADER_H

#include "MLIncludes.h"
#include "MLGLContext.h"

#include <unordered_map>
#include <MLMath/MLMath.h>
#include <tchar.h>

using namespace MLMath::MLVec2;
using namespace MLMath::MLVec3;
using namespace MLMath::MLVec4;
using namespace MLMath::MLMat4;

#define ML_LOG_SHADER ML_STR("Shader")

namespace MLRender
{

	//=========================================================
	// Name: Attribute ID
	// Description: Has ID's for each vertex attribute
	// Creator: Chris Millin
	//=========================================================
	typedef enum MLAttID
	{
		ML_ATT_VERTEXPOS = 0,
		ML_ATT_VERTEXCOL = 1,
		ML_ATT_TEXTURE0 = 2,
	}MLAttID;

	//=========================================================
	//Name: MLShader
	//Namespace: MLRenderer
	//Description: This class manages openGL shaders and can
	// deal with multiple different shader types
	// attributes and uniforms can also be set through the
	// shader class
	//Creator: Chris Millin
	//Librarys: OpenGL
	//=========================================================
	class MLShader
	{
	public:
		//Constructor
		MLCoreDLL MLShader(void);
		MLCoreDLL virtual ~MLShader(void);

		//=========================================================
		// Name: Release
		// Description: Releases the shader
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release(void);

		//=========================================================
		// Name: Load Shader File
		// Description: Loads a specified shader file. The type
		// of shader needs to be given to know how to load it into
		// opengl
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadShaderFile(ML_STRING& File, const GLenum type);

		//=========================================================
		// Name: Load Shader File
		// Description: Loads a specified shader file. The type
		// of shader needs to be given to know how to load it into
		// opengl
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadShaderFile(MLKernel::MLVirtualFile* File, GLenum type);

		//=========================================================
		// Name: Load Shader String
		// Description: Will load a shader into opengl from a
		// source string. This allows shaders to be written
		// in the code rather than in a seperate file
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadShaderString(const ML_CHAR* Source, const GLenum type);

		//=========================================================
		// Name: Link Shaders
		// Description: This will link all the shaders together.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LinkShaders(void);										

		//=========================================================
		// Name: Lock
		// Description: Will use the program (if this is called for
		// the first time it will create the program instead)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Lock(const ML_BOOL bind = ML_FALSE);	
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Unlock(void)	{ glUseProgram(0); return ML_TRUE; }					

		//=========================================================
		// Name: Bind Att
		// Description: Binds an attribute to a fixed location this
		// should be used before the shader is compiled.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL BindAtt(const MLAttID& type, ML_CHAR* Name);

		//=========================================================
		// Name: Add Att
		// Description: Will add an attribute to the shader. This
		// can be used after the shader is compiled but will require
		// the order to be correct within the shader or the layout
		// numbers to be specified.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL AddAtt(ML_CHAR* Name);								
		

		//=========================================================
		// Name: Add Uniform
		// Description: This will add a uniform to the shader
		// Creator: Chris Millin
		//=========================================================							
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, ML_BOOL	inVal);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, ML_INT	inVal);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, ML_UINT	inVal);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, ML_FLOAT	inVal);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, const MLVector2f &inVec);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, const MLVector3f &inVec);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, const MLVector4f &inVec);
		MLCoreDLL ML_BOOL AddUniform(const ML_CHAR* Name, const MLMatrix4f &inMat);
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLint GetAtt(ML_CHAR* Name)		{return m_attributes[Name];	}
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLint GetUniform(ML_CHAR* Name)	{return m_uniforms[Name]; }	

		//=========================================================
		// Name: Print Active Atts
		// Description: Will print all the active attributes in the
		// shader
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL PrintActiveAtts(void);	

		//=========================================================
		// Name: Print Active Uniforms
		// Description: Will print the active uniforms in the
		// shader
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL PrintActiveUniform(void);								

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetProgram(void) { return m_program; }

	private:

		//=========================================================
		// Name: Compile Shader
		// Description: Will compile the source string into
		// shader assembly within openGL
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL CompileShader(const ML_CHAR* sSource, const GLenum type);	
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_STRING GetHighestVersion(ML_STRING& location, const GLenum type);

		////=========================================================
		//// Name: 
		//// Description: THIS SHOULD BE IMPLEMENTED ONE DAY
		//// Idea would be to take the name remove the extension
		//// then check for versioning. Should modify the system
		//// so that if it all fails then there should always be
		//// a file without versioning which is the lowest version
		//// fail safe e.g
		//// shader4_2.frag is the 4.2 shader
		//// shader2_1.frag is the 2.1 shader (lowest version)
		//// shader.frag is the 2.1 shader again but is fail safe
		//// since there is version no key and should work on all
		//// systems with the lowest version.
		//// Creator: Chris Millin
		////=========================================================
		//MLCoreDLL ML_CHAR* GetHighestVersion(MLKernel::MLVirtualFile* location, GLenum type);

	private:
		// stores the shader types so that they can be reference easily in m_Shaders
		enum sType { MLS_GEOM = 0, MLS_VERT = 1, MLS_FRAG = 2};
		//will store the 3 main shaders frag = 2, vert = 1 and geom = 0
		GLuint m_shaders[3];
		//sortes the ID for the opengl program
		GLuint m_program;
		//stores the attribute locations. If BindAtt is used then the int will be associated with one of the predefined attributes in attID.
		std::unordered_map<ML_CHAR*, ML_INT> m_attributes;
		//stores the uniform locations
		std::unordered_map<const ML_CHAR*, ML_INT> m_uniforms;				
	};

	typedef void (*UniformCallBack)(MLShader* activeShader);
}



#endif


//USAGE
//=========================================================
//usage of this class is very similar to Movania's (http://mmmovania.blogspot.co.uk/2011/02/c-class-for-glslshader.html)
//the shader is loaded first
//
// sClass->LoadShaderFile("shaders/vertex.vert, GL_VERTEX_SHADER);
//
//then the program is locked (created in the first case)
//
// sClass->lock(true); //true means it will create a program
//
//now any attributes can be added or at this point it may be best to bind them if you know them all
//
// sClass->bindAtt(ML_VERTEX_POS, "VertexPosition");
// sClass->addAtt("VertexPosition");
//
//uniforms can also be added
//
// sClass->addUniform("RotMat");
//
//then the shader can be linked one it has all attributes binded
//
// sClass->LinkShaders();
//
//the shader can now be unlocked (if needed)
//
// sClass->unlock();
//
//at any point the shader can be locked and unlocked by calling lock() (no need for true as it has already created the program) and unlock()
//
//so to render
//
// sClass->lock();
// /*	render	*/
// sClass->unlock();
// sClass2->lock()
// /*	render	*/
// sClass2->unlock();
//=========================================================
