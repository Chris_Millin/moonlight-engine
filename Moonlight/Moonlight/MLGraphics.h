#ifndef MLGRAPHICS_H
#define MLGRAPHICS_H

#include "MLIncludes.h"
#include "MLFormats.h"
#include "MLShader.h"
#include "MLVertexBuffer.h"
#include "MLTexture.h"
#include "MLCameraManager.h"
#include "MLGraphicTypes.h"
#include "MLShaderManager.h"

#include <MLMath/Matrix/MLMat4.h>
#include <algorithm>
#include <stdarg.h>

#define ML_LOG_GRAPHICS ML_STR("Graphics")

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLGraphics
	{
	public:
		MLCoreDLL MLGraphics(void)  {}
		MLCoreDLL ~MLGraphics(void) { Release(); }

		//=========================================================
		// Name: Release
		// Description: Releases any dynamic objects created
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Release();

		//=========================================================
		// Name: Initalise
		// Description: Will initalise each section of the Graphics
		//				system
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Initalise(const ML_UINT initalBatchSize, const MLScreenScaler& scaling = DefaultScreen);

		//=========================================================
		// Name: Begin 
		// Description: Start the sprite batch off. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Begin(const ML_BOOL alpha = ML_TRUE, const ML_BOOL wireFrame = ML_FALSE);

		//=========================================================
		// Name: Draw
		// Description: Draw calls which can be used for rendering
		//				a 2D textured sprite
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLSprite* sprite);

		//=========================================================
		// Name: Draw
		// Description: Drawing a point
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLPoint* point)	{ DrawPoint(point); }

		//=========================================================
		// Name: Draw 
		// Description: Drawing a line
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLLine* line)	{ DrawLine(line); }
	
		//=========================================================
		// Name: Draw
		// Description: Drawing a quad
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLQuad* quad)	{ DrawQuad(quad); }
	
		//=========================================================
		// Name: Draw
		// Description: Drawing a circle
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLCircle* circle) { DrawCircle(circle); }
	
		//=========================================================
		// Name: Draw
		// Description: Drawing a Vertex array
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Draw(const MLVertexArray* vertexArray) { DrawVertexArray(vertexArray); }

		//=========================================================
		// Name: Draw Point
		// Description: Drawing a point
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawPoint(const MLPoint* point);

		//=========================================================
		// Name: Draw Line 
		// Description: Drawing a line
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawLine(const MLLine* line);

		//=========================================================
		// Name: Draw Quad
		// Description: Drawing a quad
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawQuad(const MLQuad* quad);

		//=========================================================
		// Name: Draw Duel Quad
		// Description: Draws a filled and unfilled quad in the 
		//				same position
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawDuelQuad(MLQuad* quad, const MLVector4f& colour);

		//=========================================================
		// Name: Draw Circle
		// Description: Drawing a circle
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawCircle(const MLCircle* circle);

		//=========================================================
		// Name: Draw Duel Circle
		// Description: Drawing a Filled circle
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawDuelCircle(MLCircle* circle, const MLVector4f& colour);

		//=========================================================
		// Name: Draw Vertex Array
		// Description: Drawing a Vertex array
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void DrawVertexArray(const MLVertexArray* vertexArray);

		//=========================================================
		// Name: Write
		// Description: Write call for rendering font onto the screen
		//				uses the font class to define the font that 
		//				it uses to render.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Write(const MLWrite* write, ML_STRING& text, ...);

		//=========================================================
		// Name: End
		// Description: This should be the final call which will 
		//				flush all the sprites and render them all
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL End(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetScreenScaler(const MLScreenScaler& scaler);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SetCamera(const ML_STRING& name);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLScreenScaler GetScreenScaler()	{ return m_screenScaler; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_STRING GetCamera()				{ return m_cameraName;  }

	private:

		//=========================================================
		// Name: Initialise Shader
		// Description: Will load in the base shader file along
		//				with binding the attributes and linking
		//				the shaders
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL InitialiseShader();

		//=========================================================
		// Name: Initalise Vertex Buffer
		// Description: This will create both the virtual and graphics
		//				buffers.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL InitaliseVertexBuffer(const ML_UINT initalBatchSize);

		//=========================================================
		// Name: Initalise Final
		// Description: The last step in the inialisation which will
		//				create the item batch array and also create
		//				the default 2x2 texture used for the primitve
		//				shapes.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL InitaliseFinal(const ML_UINT initalBatchSize);

		//=========================================================
		// Name: Load Blank Texture
		// Description: Will load a 2x2 white texture into the
		// texture manager to be used for basic objects
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL LoadBlankTexture();

		//=========================================================
		// Name: Batch Sprite
		// Description: Add a sprite to the virtual vertex buffer
		//				and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchSprite(const MLSprite* sprite, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Point
		// Description: Add a point to the virtual vertex buffer
		//				and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchPoint(const MLPoint* point, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Line
		// Description: Add a line to the virtual vertex buffer
		//				and store a Batch item related to it. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchLine(const MLLine* line, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Quad
		// Description: Add a quad to the virtual vertex buffer
		//				and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchQuad(const MLQuad* quad, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Circle
		// Description: Add a circle to the virtual vertex buffer
		//				and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchCircle(const MLCircle* circle, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Vertex Array
		// Description: Add a vertex array to the virtual vertex 
		//				buffer and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchVertexArray(const MLVertexArray* vertexArray, const ML_INT drawMode);

		//=========================================================
		// Name: Batch Text
		// Description: Add a text glyphs to the virtual vertex 
		//				buffer and store a Batch item related to it.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void BatchText(const MLWrite* write, const ML_STRING& text);
	
		//=========================================================
		// Name: Matrix Set Up
		// Description: Sets up the matrix's for use with rendering
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLMatrix4f MatrixSetUp(const MLVector2f& scale, const MLVector2f& origin, const ML_FLOAT& rotation, const MLVector2f& position);

		//=========================================================
		// Name: Add To Virtual Buffer
		// Description: Adds a single vertex to the virtual vertex
		//				buffer. Uses the 3 pre-defined shader
		//				attributes.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AddToVirtualBuffer(const MLVector3f& position, const MLVector2f& UV, const MLVector4f& colour);

		//=========================================================
		// Name: Add To Batch Queue
		// Description: Add an item to the batch queue for rendering
		//				containing all the information to render the
		//				object.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void AddToBatchQueue(const MLBaseGraphicType* item, const ML_INT& drawMode, const ML_INT& bufferSize);

		//=========================================================
		// Name: Ready Vertex Buffer
		// Description: Will ready the opengl vertex buffer for
		//				taking the 3 pre-defined attributes.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void ReadyVertexBuffer(void);
	
		//=========================================================
		// Name: Render In Text Box
		// Description: Will check if the sprite should be rendered
		//				in the text box. Is only used for text
		//				rendering
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RenderInTextBox(MLSprite* sprite, const MLWrite* write);
		MLCoreDLL ML_BOOL RenderInTextBox(MLQuad* quad, const MLWrite* write);
		MLCoreDLL ML_BOOL RenderInTextBox(MLLine* line, const MLWrite* write);

		//=========================================================
		// Name: Flush Batch 
		// Description: Flushes all the batch items so they are
		//				ready for rendering
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL FlushBatch(void);

		//=========================================================
		// Name: Sort Batch
		// Description: sorts the sprite batch based on layer,
		//				texture, draw mode, shader, and uniform.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL SortBatch(void);

		//=========================================================
		// Name: Expand Batch
		// Description: expands the batch if it needs to store 
		//				more batch items
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL ExpandBatch(void);

		//=========================================================
		// Name: Render 
		// Description: Performs the rendering of all the batch
		//				items in the queue.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Render(const ML_INT& startIndex, const ML_INT& count);

		//=========================================================
		// Name: Sort Front To Back
		// Description: sorting functions for std::sort. Sorts 
		//				lowest values to highest.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static SortFronttoBack(MLBatchItem* x, MLBatchItem* y)	{ return (*x) < (*y); }
	
		//=========================================================
		// Name: Sort Back To Front
		// Description: sorting functions for std::sort. Sorts 
		//				highest values to lowest.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static SortBacktoFront(MLBatchItem* x, MLBatchItem* y)	{ return (*y) < (*x); }

	private:

		MLScreenScaler m_screenScaler;

		ML_STRING m_cameraName;

		ML_BOOL m_wireFrame;

		ML_BOOL m_beginCall;
	
		//Virtual buffer members
		MLVertexPosTexCol* m_virtualBuffer;
		ML_INT m_virtualBufferOffset;
		ML_INT m_virutalBufferSize;
		
		//The graphics buffer
		MLVertexBuffer* m_vertexBuffer;
		ML_INT m_vertexBufferSize;
		ML_INT m_vertexBufferOffset;

		//The shader that will be used if one isnt specified
		MLShader* m_baseShader;

		ML_INT m_drawableObjectMaxSize;
		MLBatchItem* m_drawableObjects;

		std::vector<MLBatchItem*> m_drawableObjectQueue;

		//This is a blank texture containing a 2x2 white pixel to be used
		//with primitives
		MLTexture* m_blankTexture;

		MLVector2f m_scaleModifier;
	};
}
#endif