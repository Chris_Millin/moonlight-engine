#include "MLPostProcess.h"

namespace MLRender
{

	MLPostProcess::MLPostProcess(void)
	{
		m_PostProcessTextureInput.clear();

		m_outputTexture = NULL;

		m_FBO			= NULL;

		m_shader		= NULL;

		m_UniformCallback = NULL;

		m_postRender	= ML_TRUE;
		m_started		= ML_FALSE;
		m_finished		= ML_FALSE;
	}

	MLPostProcess::~MLPostProcess(void)
	{
		Release();
	}


	MLCoreDLL void MLPostProcess::Release()
	{
		if(m_PostProcessTextureInput.size() != 0)
		{
			for (ML_UINT i = 0; i < m_PostProcessTextureInput.size(); i++)
			{
				delete m_PostProcessTextureInput[i];
				m_PostProcessTextureInput[i] = NULL;
			}

			m_PostProcessTextureInput.clear();
		}

		MLTextureManager::RemoveTexture(m_outputTexture);
		MLRELEASE(m_FBO);
	}

	MLCoreDLL void MLPostProcess::Initalise(const MLFBOTypes enableFBO)
	{
		m_outputTexture = MLTextureManager::CreateTexture();
		m_outputTexture->CreateEmptyTexture(1280, 720); //THIS SHOULD BE SCREEN X AND Y!!!

		m_FBO = new MLFrameBuffer();
		m_FBO->CreateBuffer(GL_FRAMEBUFFER);
		m_FBO->Bind();

		if(enableFBO == MLFBOTypes::ColourBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_COLOR_ATTACHMENT0);
		else if(enableFBO == MLFBOTypes::StencilBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_STENCIL_ATTACHMENT);
		else if(enableFBO == MLFBOTypes::DepthBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_DEPTH_ATTACHMENT);

		if(m_FBO)
		{
			if(m_FBO->GetStatus() == GL_FRAMEBUFFER_COMPLETE)
				ML_TRACE(ML_LOG_POST_PROCESS, MLKernel::MLConsoleStatus::Information, ML_STR("Frame Buffer Status OK"));
			m_FBO->UnBind();
		}
	}

	MLCoreDLL void MLPostProcess::Bind()
	{
		m_finished	= ML_FALSE;
		m_started	= ML_FALSE;

		m_FBO->Bind();
	}

	MLCoreDLL void MLPostProcess::Draw(MLGraphics* batch)
	{
		m_started = ML_TRUE;
		//CLEAR FRAME

		m_shader->Lock();
	
		for (ML_UINT i = 0; i < m_PostProcessTextureInput.size(); i++)
		{
			MLPostProcessInput* input = m_PostProcessTextureInput[i];
			m_shader->AddUniform(input->InputName.c_str(), input->ShaderTextureIndex);

			if(input->TextureID != ML_INVALID_TEXTURE_ID)
			{
				glActiveTexture(input->GLTextureIndex);
				glBindTexture(input->GLTextureType, input->TextureID);
			}
			else
			{
				ML_TRACE(ML_LOG_POST_PROCESS, MLKernel::MLConsoleStatus::Error,
					ML_STR("Could not find post process %S in manager. Ensure the post process is run before this one! Drawing is aborted"), input->InputName);
				m_shader->Unlock();
				return;
			}	
		}

		m_UniformCallback(m_shader);

		m_shader->Unlock();
	
		//Set shader to spritebatch
		//START SPRITEBATCH

		MLSprite sprite;
		sprite.Texture = m_outputTexture;
		sprite.SetSizeFromTexture();
		sprite.SetSourceFromTexture();
		sprite.Shader = m_shader;

		batch->Draw(&sprite);

		//ENDSPRITEBATCH
	
		m_finished = ML_TRUE;
		m_started = ML_FALSE;
	}

	MLCoreDLL void MLPostProcess::Unbind()
	{
		m_FBO->UnBind();
	}

	MLCoreDLL ML_BOOL MLPostProcess::AddInput(const ML_U8STRING& name, const ML_INT shaderTextureIndex, 
		const ML_INT textureID, const ML_UINT glTextureIndex, const ML_UINT glTextureType)
	{
		MLPostProcessInput* input = new MLPostProcessInput();
		input->InputName			= name;
		input->ShaderTextureIndex	= shaderTextureIndex;
		input->GLTextureIndex		= glTextureIndex;
		input->GLTextureType		= glTextureType;
		input->TextureID			= textureID;

		for (ML_UINT i = 0; i < m_PostProcessTextureInput.size(); i++)
		{
			MLPostProcessInput* inputTest = m_PostProcessTextureInput[i];
			if( inputTest->InputName == input->InputName || inputTest->GLTextureIndex == input->GLTextureIndex
				|| inputTest->TextureID == input->TextureID)
			{
				ML_TRACE(ML_LOG_POST_PROCESS, MLKernel::MLConsoleStatus::Warning, 
					ML_STR("Input %S contains the same Name/Index/ID as input %S which is invalid. Input %S qued for adding will not be added."),
					input->InputName.c_str(), inputTest->InputName.c_str(), input->InputName.c_str());
				return ML_FALSE;
			}
		}

		m_PostProcessTextureInput.push_back(input);

		return ML_TRUE;
	}


	MLCoreDLL MLPostProcessInput* MLPostProcess::GetInput(const ML_U8STRING& name)
	{
		for (ML_UINT i = 0; i < m_PostProcessTextureInput.size(); i++)
		{
			MLPostProcessInput* inputTest = m_PostProcessTextureInput[i];
			if( inputTest->InputName == name)
				return inputTest;
		}

		return NULL;
	}

}