#include "MLPolyList.h"


namespace MLRender
{

	MLCoreDLL MLPolyList::MLPolyList(void)
	{
		parent = NULL;
		first = NULL;
		pointCount = 0;
	}

	MLCoreDLL MLPolyList::MLPolyList(MLPolyList* inParent)
	{
		parent = inParent;
		first = NULL;
		pointCount = 0;

		if (parent != NULL)
		{
			parent->AddChild(this);
		}
	}

	MLCoreDLL MLPolyList::MLPolyList(MLPolyList* inParent, const MLVector2f& point)
	{
		parent = inParent;

		MLPolyNode* node1 = new MLPolyNode(point);


		first = node1;
		first->next = node1;
		first->previous = node1;

		pointCount = 1; 

		if (parent != NULL)
		{
			parent->AddChild(this);
		}
	}

	MLCoreDLL MLPolyList::MLPolyList(MLPolyList* inParent, const MLVector2f& pointA, const MLVector2f& pointB)
	{
		parent = inParent;

		MLPolyNode* node1 = new MLPolyNode(pointA);
		MLPolyNode* node2 = new MLPolyNode(pointB);

		first = node1;
		first->next = node2;
		first->previous = node2;

		node2->next = node1;
		node2->previous = node1;

		pointCount = 2; 

		if (parent != NULL)
		{
			parent->AddChild(this);
		}
	}

	MLCoreDLL MLPolyList::MLPolyList(MLPolyList* inParent, const MLVector2f& pointA, const MLVector2f& pointB, const MLVector2f& pointC)
	{
		parent = inParent;

		MLPolyNode* node1 = new MLPolyNode(pointA);
		MLPolyNode* node2 = new MLPolyNode(pointB);
		MLPolyNode* node3 = new MLPolyNode(pointC);

		first = node1;
		first->next = node2;
		first->previous = node3;

		node2->next = node3;
		node2->previous = node1;

		node3->next = node1;
		node3->previous = node2;

		pointCount = 3;

		if (parent != NULL)
		{
			parent->AddChild(this);
		}
	}

	MLCoreDLL MLPolyList::~MLPolyList(void)
	{
		MLPolyNode* node;
		while(pointCount != 0 && pointCount >= 0)
		{
			node = first->previous;
			first->previous = first->previous->previous;

			delete node;

			pointCount--;
		}
	}

	MLCoreDLL ML_BOOL MLPolyList::Clear()
	{
		MLPolyNode* node;
		while(pointCount != 0 && pointCount >= 0)
		{
			node = first->previous;
			first->previous = first->previous->previous;

			delete node;

			pointCount--;
		}

		first = NULL;
		pointCount = 0;

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLPolyList::AddChild(MLPolyList* child)
	{
		if (child == NULL)
			return ML_FALSE;

		children.push_back(child);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLPolyList::AddPoint(const ML_FLOAT x, const ML_FLOAT y)
	{
		if (first == NULL)
		{
			first = new MLPolyNode(x, y);
			first->next = first;
			first->previous = first;

			pointCount++;

			return ML_TRUE;
		}


		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point.x == x && current->point.y == y)
			{
				return ML_FALSE;
			}

			current = current->next;
			count--;
		}

		current = new MLPolyNode(x, y);

		current->next = first;
		current->previous = first->previous;
		current->previous->next = current;
		first->previous = current;

		pointCount++;

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLPolyList::AddPoint(const MLVector2f& point)
	{
		if (first == NULL)
		{
			first = new MLPolyNode(point);
			first->next = first;
			first->previous = first;

			pointCount++;

			return ML_TRUE;
		}


		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == point)
			{
				return ML_FALSE;
			}

			current = current->next;
			count--;
		}

		current = new MLPolyNode(point);

		current->next = first;
		current->previous = first->previous;
		current->previous->next = current;
		first->previous = current;

		pointCount++;

		return ML_TRUE;
	}


	MLCoreDLL ML_BOOL MLPolyList::RemovePoint(const ML_FLOAT x, const ML_FLOAT y)
	{
		if (pointCount == 0)
			return ML_FALSE;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point.x == x && current->point.y == y)
			{
				break;
			}

			current = current->next;
			count--;
		}

		if (current->point.x == x && current->point.y == y)
		{
			current->previous->next = current->next;
			current->next->previous = current->previous;

			if (current == first)
				first = current->next;

			delete current;

			pointCount--;

			if (pointCount == 0)
				first = NULL;
			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLPolyList::RemovePoint(const MLVector2f& point)
	{
		if (pointCount == 0)
			return ML_FALSE;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == point)
			{
				break;
			}

			current = current->next;
			count--;
		}

		if (current->point == point)
		{
			current->previous->next = current->next;
			current->next->previous = current->previous;

			if (current == first)
				first = current->next;

			delete current;

			pointCount--;

			if (pointCount == 0)
				first = NULL;
			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLPolyList::RemovePoint(const MLPolyNode &point)
	{
		if (pointCount == 0)
			return ML_FALSE;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == point.point &&
				current->next->point == point.next->point &&
				current->previous->point == point.previous->point)
			{
				break;
			}

			current = current->next;
			count--;
		}

		if (current->point == point.point &&
			current->next->point == point.next->point &&
			current->previous->point == point.previous->point)
		{
			current->previous->next = current->next;
			current->next->previous = current->previous;

			if (current == first)
				first = current->next;

			delete current;

			pointCount--;

			if (pointCount == 0)
				first = NULL;
			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLPolyList::RemoveLast()
	{
		if (pointCount == 0)
			return ML_FALSE;

		MLPolyNode* current = first->previous;

		current->previous->next = current->next;
		current->next->previous = current->previous;

		delete current;

		pointCount--;

		if (pointCount == 0)
			first = NULL;

		return ML_TRUE;
	}


	MLCoreDLL MLPolyNode* MLPolyList::GetPoint(const ML_FLOAT x, const ML_FLOAT y) const
	{
		if (pointCount == 0)
			return NULL;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point.x == x && current->point.y == y)
				return current;

			current = current->next;
			count--;
		}

		return NULL;
	}

	MLCoreDLL MLPolyNode* MLPolyList::GetPoint(const MLVector2f& point) const
	{
		if (pointCount == 0)
			return NULL;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == point)
				return current;

			current = current->next;
			count--;
		}

		return NULL;
	}

	MLCoreDLL MLPolyList* MLPolyList::GetChild(ML_UINT pos) const
	{
		if (children.size() > pos && pos != -1)
			return children[pos];
		else
			return NULL;
	}

	MLCoreDLL ML_BOOL MLPolyList::RemoveChild(ML_UINT pos)
	{
		if (children.size() > pos && pos != -1)
		{
			delete children[pos];
			children.erase(children.begin() + pos);
			return ML_TRUE;
		}
		else
			return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLPolyList::InsertPoint(const ML_FLOAT x, const ML_FLOAT y, const MLPolyNode* pointNode)
	{
		if (pointNode == NULL)
			return ML_FALSE;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == pointNode->point &&
				current->next->point == pointNode->next->point &&
				current->previous->point == pointNode->previous->point)
				break;

			current = current->next;
			count--;
		}

		if (current->point == pointNode->point &&
			current->next->point == pointNode->next->point &&
			current->previous->point == pointNode->previous->point)
		{
			MLPolyNode* newNode = new MLPolyNode(x, y);

			current->previous->next = newNode;
			newNode->previous = current->previous;

			current->previous = newNode;
			newNode->next = current;

			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL ML_BOOL MLPolyList::InsertPoint(const MLVector2f& point, const MLPolyNode* pointNode)
	{
		if (pointNode == NULL)
			return ML_FALSE;

		MLPolyNode* current = first;
		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			if (current->point == pointNode->point &&
				current->next->point == pointNode->next->point &&
				current->previous->point == pointNode->previous->point)
				break;

			current = current->next;
			count--;
		}

		if (current->point == pointNode->point &&
			current->next->point == pointNode->next->point &&
			current->previous->point == pointNode->previous->point)
		{
			MLPolyNode* newNode = new MLPolyNode(point);

			current->previous->next = newNode;
			newNode->previous = current->previous;

			current->previous = newNode;
			newNode->next = current;

			return ML_TRUE;
		}

		return ML_FALSE;
	}

	MLCoreDLL void MLPolyList::Reverse()
	{

		MLPolyNode* current = first;
		MLPolyNode* temp;

		ML_UINT count = pointCount;
		while(count != 0 && count >= 0)
		{
			temp = current->next;

			current->next = current->previous;
			current->previous = temp;

			current = temp;

			count--;
		}
	}

}
