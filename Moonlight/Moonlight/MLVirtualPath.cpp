#include "MLVirtualPath.h"
#include "MLVirtualFile.h"

namespace MLKernel
{
	MLVirtualPath::MLVirtualPath(void)
	{
		m_drivePath = NULL;
		m_pathAddress.clear();
	}

	MLVirtualPath::MLVirtualPath(const ML_STRING& Path)
	{
		m_drivePath = NULL;
		m_pathAddress = Path;
	}

	MLVirtualPath::MLVirtualPath(MLVirtualPath* Path)
	{
		if(Path->GetDrive())
			m_drivePath = Path->GetDrive(); 
		m_pathAddress = Path->GetPath();
	}

	MLVirtualPath::MLVirtualPath(MLVirtualDrive* Drive, const ML_STRING& Path)
	{
		m_drivePath = Drive;
		m_pathAddress = Path;
		if(!IsValid())
			m_pathAddress.clear();
	}

	MLVirtualPath::MLVirtualPath(MLVirtualDrive* Drive, MLVirtualPath* Path)
	{
		m_drivePath = Drive;
		m_pathAddress = Path->GetPath();
		if(!IsValid())
			m_pathAddress.clear();
	}

	MLVirtualPath::~MLVirtualPath(void)
	{
		Release();
	}

	void MLVirtualPath::Release()
	{
		MLRELEASE(m_drivePath)
		m_pathAddress.clear();
	}

	void MLVirtualPath::CreatePath()
	{ 
		ML_STRING SearchPath;
		if(m_drivePath)
		{	
			if(m_drivePath->IsDefault())
			{
				SearchPath = GetDefaultDrivePath();
			}
			else
			{
				SearchPath = m_drivePath->GetDrive();
			}
		}
		else
		{
			SearchPath = GetDefaultDrivePath();
		}

		IShellItem* file = CreateShellItem(SearchPath);
		IFileOperation* fileOp = CreateOperation();
		if(file && fileOp)
		{
			fileOp->NewItem(file, FILE_ATTRIBUTE_DIRECTORY, m_pathAddress.c_str(), NULL, NULL);
			fileOp->PerformOperations();
		}
		else
		{
			if(fileOp)	fileOp->Release();
			if(file)	file->Release();

			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create windows ShellItem or FileOperation from path %s for folder creation\n"), m_pathAddress.c_str());
		}


		ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::FileAccess, ML_STR("Created Path %s\n"), m_pathAddress.c_str());

		if(fileOp)	fileOp->Release();
		if(file)	file->Release();
	}

	MLVirtualPath* MLVirtualPath::CreatePath(const ML_STRING& Name)
	{ 
		MLVirtualPath* result = NULL;
		ML_STRING fullPath = BuildFullPathAddress();

		IShellItem* file = CreateShellItem(fullPath);
		IFileOperation* fileOp = CreateOperation();
		if(file && fileOp)
		{
			fileOp->NewItem(file, FILE_ATTRIBUTE_DIRECTORY, Name.c_str(), NULL, NULL);
			fileOp->PerformOperations();

			result = new MLVirtualPath();
			result->SetPath(BuildNewFullPathAddress(Name));
			if(m_drivePath)
			{
				result->SetDrive(m_drivePath);
			}
		}
		else
		{
			if(fileOp)	fileOp->Release();
			if(file)	file->Release();

			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
							ML_STR("Could not create windows ShellItem or FileOperation from path %s for folder creation\n"), fullPath.c_str());
		
			return result;
		}

		if(result)
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::FileAccess, ML_STR("Created Path %s\n"), fullPath.c_str());

		if(fileOp)	fileOp->Release();
		if(file)	file->Release();

		return result;
	}

	MLVirtualPath* MLVirtualPath::CreatePath(MLVirtualPath* Name)
	{
		return CreatePath(Name->GetPath());
	}

	ML_BOOL MLVirtualPath::DeletePath()
	{
		ML_STRING fullPath = BuildFullPathAddress();

		IShellItem* file = CreateShellItem(fullPath);
		IFileOperation* fileOp = CreateOperation();
		if(file && fileOp)
		{
			fileOp->DeleteItem(file, NULL);
			fileOp->PerformOperations();
		}
		else
		{
			if(fileOp)	fileOp->Release();
			if(file)	file->Release();

			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create windows ShellItem or FileOperation from path %s for folder creation\n"), fullPath.c_str());

			return ML_FALSE;
		}

		if(file && fileOp)
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::FileAccess, ML_STR("Deleted Path %s\n"), fullPath.c_str());

		if(fileOp)	fileOp->Release();
		if(file)	file->Release();

		return ML_TRUE;
	}

	ML_BOOL MLVirtualPath::DeletePath(const ML_STRING& Path)
	{
		ML_STRING fullPath = BuildFullPathAddress() + ML_STR("\\") + Path;

		IShellItem* file = CreateShellItem(fullPath);
		IFileOperation* fileOp = CreateOperation();
		if(file && fileOp)
		{
			fileOp->DeleteItem(file, NULL);
			fileOp->PerformOperations();
		}
		else
		{
			if(fileOp)	fileOp->Release();
			if(file)	file->Release();

			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create windows ShellItem or FileOperation from path %s for folder Deletion\n"), fullPath.c_str());

			return ML_FALSE;
		}
	
		if(fileOp)	fileOp->Release();
		if(file)	file->Release();

		return ML_TRUE;
	}

	ML_BOOL MLVirtualPath::DeletePath(MLVirtualPath* Path)
	{
		return DeletePath(Path->GetPath());
	}

	std::vector<ML_STRING> MLVirtualPath::EnumerateFolders() const
	{
		std::vector<ML_STRING> folders;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		WIN32_FIND_DATA ffd;

		ML_STRING find = BuildFullPathAddress() + ML_STR("\\*");

		hFind = FindFirstFile(find.c_str(), &ffd);
		if (INVALID_HANDLE_VALUE == hFind) 
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
				ML_STR("Invalid Handle from FindFirstFile %s\n"), find.c_str());

			return folders;
		}
	
		do
		{
			if (INVALID_HANDLE_VALUE == hFind) 
			{
				ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
					ML_STR("Invalid Handle from FindNextFile %s (folder)\n"), find.c_str());
			}
			else if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if(wcscmp(ffd.cFileName, ML_STR(".\0")) && wcscmp(ffd.cFileName, ML_STR("..\0")))
					folders.push_back(ffd.cFileName);
			}
		}while(FindNextFile(hFind, &ffd) != 0);

		FindClose(hFind);

		return folders;
	}

	std::vector<ML_STRING> MLVirtualPath::EnumerateFiles() const
	{
		std::vector<ML_STRING> files;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		WIN32_FIND_DATA ffd;

		ML_STRING find = BuildFullPathAddress() + ML_STR("\\*");

		hFind = FindFirstFile(find.c_str(), &ffd);
		if (INVALID_HANDLE_VALUE == hFind) 
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
				ML_STR("Invalid Handle from FindFirstFile %s (file)\n"), find.c_str());

			return files;
		}

		do
		{
			if (INVALID_HANDLE_VALUE == hFind) 
			{
				ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
					ML_STR("Invalid Handle from FindNextFile %s (file)\n"), find.c_str());
			}
			else if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				files.push_back(ffd.cFileName);
			}
		}while(FindNextFile(hFind, &ffd) != 0);

		FindClose(hFind);

		return files;
	}

	std::vector<ML_STRING> MLVirtualPath::EnumerateFiles(const ML_STRING& Extention) const
	{
		std::vector<ML_STRING> files;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		WIN32_FIND_DATA ffd;
	
		ML_STRING ext = Extention;
		ML_STRING find = BuildFullPathAddress() + ML_STR("\\*");

		if(ext.compare(0, 1, ML_STR("*")))
		{
			ext.insert(0, ML_STR("*"), 1);
		}

		if(ext.compare(1, 1, ML_STR(".")))
		{
			//err not a file extension
			return files;
		}

		hFind = FindFirstFile(find.c_str(), &ffd);
		if (INVALID_HANDLE_VALUE == hFind) 
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
				ML_STR("Invalid Handle from FindFirstFile %s (file)\n"), find.c_str());
			return files;
		}

		do
		{
			if (INVALID_HANDLE_VALUE == hFind) 
			{
				ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
					ML_STR("Invalid Handle from FindNextFile %s (file)\n"), find.c_str());
			}
			else if((PathMatchSpec(ffd.cFileName, ext.c_str())) && !(ffd.dwFileAttributes & !FILE_ATTRIBUTE_DIRECTORY))
			{
				files.push_back(ffd.cFileName);
			}
		}while(FindNextFile(hFind, &ffd) != 0);

		FindClose(hFind);

		return files;
	}

	std::vector<MLVirtualPath*> MLVirtualPath::GetFolders() const
	{
		std::vector<MLVirtualPath*> folders;
		std::vector<ML_STRING> subFolders;
		subFolders = EnumerateFolders();
		for (ML_UINT i = 0; i < subFolders.size(); i++)
		{
			MLVirtualPath* tempPath = new MLVirtualPath();
		
			if(m_drivePath)
				tempPath->SetDrive(m_drivePath);

			tempPath->SetPath(BuildNewFullPathAddress(subFolders[i]));

			folders.push_back(tempPath);
		}

		return folders;
	}

	std::vector<MLVirtualFile*> MLVirtualPath::GetFiles() const
	{
		std::vector<MLVirtualFile*> files;
		std::vector<ML_STRING> subFiles;
		subFiles = EnumerateFiles();
		for (ML_UINT i = 0; i < subFiles.size(); i++)
		{
			MLVirtualFile* tempPath = new MLVirtualFile();
			tempPath->SetFile(subFiles[i], this);
			files.push_back(tempPath);
		}

		return files;
	}

	MLVirtualPath* MLVirtualPath::GetFolder(const ML_STRING& Name) const
	{
		std::vector<ML_STRING> subFolders;
		subFolders = EnumerateFolders();
		if(std::find(subFolders.begin(), subFolders.end(), Name) != subFolders.end())
		{
			MLVirtualPath* tempPath = new MLVirtualPath();

			if(m_drivePath)
				tempPath->SetDrive(m_drivePath);

			tempPath->SetPath(BuildNewFullPathAddress(Name));

			return tempPath;
		}

		ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
						ML_STR("Could not find folder %s in path %s\n"), Name.c_str(), GetFullPath().c_str());

		return NULL;
	}

	MLVirtualFile* MLVirtualPath::GetFile(const ML_STRING& Name) const
	{
		std::vector<ML_STRING> subFolders;
		subFolders = EnumerateFiles();
		if(std::find(subFolders.begin(), subFolders.end(), Name) != subFolders.end())
		{
			MLVirtualFile* tempPath = new MLVirtualFile();
			tempPath->SetFile(Name, this);

			return tempPath;
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not find File %s in path %s\n"), Name.c_str(), GetFullPath().c_str());

			return NULL;
		}
	}

	ML_BOOL MLVirtualPath::IsValid() const
	{
		return folderExists(BuildFullPathAddress());
	}

	ML_STRING MLVirtualPath::GetDefaultDrivePath() const
	{
		ML_STRING result;
		WCHAR  currentPath[MAX_PATH] = ML_STR("");
		DWORD error = GetCurrentDirectoryW(ARRAYSIZE(currentPath), currentPath);
		if (error == 0)
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::FatalError, 
							ML_STR("Could not get default path. Try setting the drive manually through SetDrive\n"));
			result.clear();
			return result;
		}

		result = currentPath;
		return result;
	}

	IShellItem* MLVirtualPath::CreateShellItem(const ML_STRING& path)
	{
		IShellItem* psiTo = NULL;
		HRESULT hr = SHCreateItemFromParsingName(path.c_str() , NULL, IID_PPV_ARGS(&psiTo));
		if (SUCCEEDED(hr))
		{
			return psiTo;
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create shellitem from path %s\n"), path.c_str());
			return NULL;
		}
	}

	IFileOperation* MLVirtualPath::CreateOperation()
	{
		IFileOperation* fileOp;
		HRESULT hr = CoCreateInstance(CLSID_FileOperation, NULL, CLSCTX_ALL, IID_PPV_ARGS(&fileOp));
		if(SUCCEEDED(hr))
		{
			return fileOp;
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create FileOperation\n"));
			return NULL;
		}
	}

	ML_STRING MLVirtualPath::BuildFullPathAddress() const
	{
		ML_STRING outFile;
		ML_STRING tempPath = m_pathAddress;
		if(m_drivePath)
		{	
			if(m_drivePath->IsDefault())
			{
				outFile = GetDefaultDrivePath();
				if (outFile.empty() == ML_TRUE)
				{
					ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
						ML_STR("Could not build full path address\n"));

					return ML_STR("\0");
				}
				if(tempPath.compare(0, 1, ML_STR("\\")))
				{
					tempPath.insert(0, ML_STR("\\"), 1);
				}
			}
			else
			{
				outFile = m_drivePath->GetDrive();
				if(outFile.empty() == ML_TRUE)
				{
					ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
						ML_STR("Could not build full path address\n"));

					return ML_STR("\0");
				}
				if(!tempPath.compare(0, 1, ML_STR("\\")))
				{
					tempPath = tempPath.substr(1);
				}
			}
		}
		else
		{
			outFile = GetDefaultDrivePath();
			if (outFile.empty() == ML_TRUE)
			{
				ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
					ML_STR("Could not build full path address\n"));

				return ML_STR("\0");
			}
			if(tempPath.compare(0, 1, ML_STR("\\")))
			{
				tempPath.insert(0, ML_STR("\\"), 1);
			}
		}

		if(!tempPath.compare(tempPath.length() - 1, 1, ML_STR("\\")))
		{
			tempPath.resize(tempPath.length() - 1);
		}

		outFile += tempPath;

		return outFile;
	}

	ML_STRING MLVirtualPath::BuildNewFullPathAddress(const ML_STRING& path) const
	{
		ML_STRING tempPath = m_pathAddress;
		if(!tempPath.empty())
		{	
			if(!tempPath.compare(0, 1, ML_STR("\\")))
			{
				tempPath = tempPath.substr(1);
			}

			if(!tempPath.compare(tempPath.length() - 1, 1, ML_STR("\\")))
			{
				tempPath.insert(tempPath.length() - 1, ML_STR("\\"), 1);
			}
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Warning, 
				ML_STR("Stored Path Empty was this intended?\n"));
		}

		if (tempPath.empty())
			return path;
		else
			return tempPath + ML_STR("\\") + path;
	}
}