#include "MLThread.h"


namespace MLKernel
{
	DWORD _ThreadEntry(void* param)
	{
		MLThread* _thread = (MLThread*)param;
		_thread->threadEntry();
		return 0;
	}

	MLThread::MLThread(void)
	{
		m_threadHandle		= NULL;
		m_threadID			= NULL;
		m_threadFinished	= ML_TRUE;
	}

	MLThread::MLThread(ThreadEntry threadEntry, void* params)
	{
		m_threadHandle		= NULL;
		m_threadID			= NULL;
		m_threadFinished	= ML_TRUE;
		ThreadCreate(threadEntry, params);
	}

	MLThread::~MLThread(void)
	{
		Release();
	}

	void MLThread::Release()
	{
		if(m_threadHandle)
		{
			if(!CloseHandle(m_threadHandle))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Closing thread handle failed reason: %s\nWill Stop thread and try again.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				if(!ThreadTerminate())
				{
					ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::FatalError, 
						ML_STR("Failed to terminate thread.\n"));
					ML_TRACE_STACK(ML_LOG_THREAD);
				}
				else
				{
					if(!CloseHandle(m_threadHandle))
					{
						ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
							ML_STR("Closing thread handle failed again reason: %s\nWill Stop thread and try again.\n"),
							_com_error::HRESULTToWCode(GetLastError()));
					}
					else
					{
						m_threadFinished	= ML_TRUE;
						m_threadHandle		= NULL;
						m_threadID			= NULL;
					}
				}
			}
			else
			{
				m_threadFinished	= ML_TRUE;
				m_threadHandle		= NULL;
				m_threadID			= NULL;
			}		
		}
	}

	ML_BOOL MLThread::ThreadCreate()
	{
		if(!ValidThreadHandle() && ThreadFinished())
		{
			m_threadHandle = (DWORD*)CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)_ThreadEntry,(MLThread*)this,0,&m_threadID);
			if(m_threadHandle == NULL)
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Creating thread failed reason: %s\n"), _com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Thread, ML_STR("Create new thread Thread. ID = %d\n"), m_threadID);
			m_threadFinished	= ML_FALSE;
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant start a thread on an already active handle\n"));
		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadCreate(ThreadEntry threadEntry, void* params)
	{
		if(!ValidThreadHandle() && ThreadFinished())
		{
			m_threadHandle = (DWORD*)CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)threadEntry, params, 0, &m_threadID);
			if(m_threadHandle == NULL)
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Creating thread failed reason: %s\n"), _com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Thread, ML_STR("Create new thread Thread. ID = %d Function = 0x%0X\n"), m_threadID, threadEntry);
			m_threadFinished	= ML_FALSE;
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant start a thread on an already active handle\n"));
		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadSleep(const ML_DWORD ms) const
	{
		Sleep(ms);
		return ML_TRUE;
	}

	ML_BOOL MLThread::ThreadWait(const ML_DWORD ms)
	{
		if (ValidThreadHandle() && !ThreadFinished())
		{
			DWORD result = WaitForSingleObject(m_threadHandle, ms);
			if(result == WAIT_OBJECT_0)
			{
				Release();
				m_threadFinished = ML_TRUE;
				return ML_TRUE;
			}
			else if(result == WAIT_TIMEOUT)
			{
				return ML_TRUE;
			}
			else
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Error occured waiting for thread reason: %s\n"), _com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant wait on a thread thread that has finished or has a invalid handle\n"));
		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadStop()
	{
		return ThreadWait(INFINITE);
	}

	ML_BOOL MLThread::ThreadSuspend()
	{
		if (ValidThreadHandle() && !ThreadFinished())
		{
			if(!SuspendThread(m_threadHandle))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Suspending thread failed reason: %s\n"), 
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant suspend a thread thread that has finished or has a invalid handle\n"));
		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadResume()
	{
		if (ValidThreadHandle() && !ThreadFinished())
		{
			if(!ResumeThread(m_threadHandle))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Resuming thread failed reason: %s\n"), 
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
			return ML_TRUE;
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant resume a thread thread that has finished or has a invalid handle\n"));
		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadTerminate()
	{
		if (ValidThreadHandle() && !ThreadFinished())
		{
			ML_DWORD exitCode;
			if(!GetExitCodeThread(m_threadHandle, &exitCode))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed to get exit code reason: %s.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}

			if(!TerminateThread(m_threadHandle, exitCode))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed to terminate thread reason: %s.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
		}
		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant terminate a thread thread that has finished or has a invalid handle\n"));
		return ML_TRUE;
	}

	ML_BOOL MLThread::ThreadFinished()
	{
		if (ValidThreadHandle() && !m_threadFinished)
		{
			DWORD result = WaitForSingleObject(m_threadHandle, 0);
			if(result == WAIT_OBJECT_0)
			{
				Release();
				m_threadFinished = ML_TRUE;
				return ML_TRUE;
			}
			else if(result == WAIT_TIMEOUT)
			{
				m_threadFinished = ML_FALSE;
				return ML_FALSE;
			}
			else
			{
				m_threadFinished = ML_FALSE;
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Error occured waiting for thread reason: %s.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}
		}
		if(m_threadFinished)
			return ML_TRUE;

		return ML_FALSE;
	}

	ML_BOOL MLThread::ThreadSetPriority(const ML_INT p)
	{
		if(ValidThreadHandle() && !ThreadFinished())
		{
			if(!SetThreadPriority(m_threadHandle, p))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed set thread priority reason: %s.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				return ML_FALSE;
			}

			return ML_TRUE;
		}

		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant set priority of a thread thread that has finished or has a invalid handle\n"));
		return ML_FALSE;
	}

	ML_INT MLThread::ThreadGetPriority()
	{
		ML_DWORD priority = THREAD_PRIORITY_ERROR_RETURN;
		if(ValidThreadHandle() && !ThreadFinished())
		{
			priority = GetThreadPriority(m_threadHandle);
			if(priority == THREAD_PRIORITY_ERROR_RETURN)
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed get thread priority reason: %s.\n"),
					_com_error::HRESULTToWCode(GetLastError()));
				return priority;
			}

			return priority;
		}

		ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, ML_STR("Cant set priority of a thread thread that has finished or has a invalid handle\n"));
		return priority;
	}

	ML_BOOL MLThread::ThreadWaitMutex(const ML_STRING& mutexName, ML_DWORD ms /* = INFINITE */)
	{
		HANDLE mutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, mutexName.c_str());	
		if (mutex == NULL)
		{
			CloseHandle(mutex);
			DWORD error = GetLastError();
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
				ML_STR("Could not open Mutex %s code: %d reason: %s.\n"), mutexName.c_str(), error,
				_com_error::HRESULTToWCode(error));
			return ML_FALSE;
		}
		DWORD result = WaitForSingleObject(mutex, ms);
		if(result == WAIT_OBJECT_0)
		{
			CloseHandle(mutex);
			return ML_TRUE;
		}
		else
		{
			CloseHandle(mutex);
			DWORD error = GetLastError();
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
				ML_STR("Could not wait for Mutex %s code: %d reason: %s.\n"), mutexName.c_str(), error,
				_com_error::HRESULTToWCode(error));
			return ML_FALSE;
		}
	}

	ML_BOOL MLThread::ThreadReleaseMutex(const ML_STRING& mutexName)
	{
		HANDLE mutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, mutexName.c_str());
		if (mutex == NULL)
		{
			CloseHandle(mutex);
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
				ML_STR("Could not open Mutex %s reason: %s.\n"), mutexName.c_str(),
				_com_error::HRESULTToWCode(GetLastError()));

			return ML_FALSE;
		}
		if(!ReleaseMutex(mutex))
		{
			CloseHandle(mutex);
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
				ML_STR("Failed to release Mutex %s reason: %s.\n"), mutexName.c_str(),
				_com_error::HRESULTToWCode(GetLastError()));
			return ML_FALSE;
		}
		CloseHandle(mutex);
		return ML_TRUE;
	}

	ML_BOOL MLThread::ValidThreadHandle()
	{
		if(m_threadHandle)
		{
			if(!m_threadFinished)
			{
				Release();
				if(m_threadFinished)
					return ML_FALSE;
				else
					return ML_TRUE;
			}
			else
			{
				return ML_TRUE;
			}
		}

		return ML_FALSE;
	}

	//Mutex Class
	std::map<ML_STRING, DWORD*> MLMutexDictionary::m_mutexStack;

	ML_BOOL MLMutexDictionary::AddMutex(const ML_STRING& mutexName)
	{
		if (m_mutexStack[mutexName] != NULL)
		{
			if(!CloseHandle(m_mutexStack[mutexName]))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed to close Mutex handle %s reason: %s.\n"), mutexName.c_str(),
					_com_error::HRESULTToWCode(GetLastError()));
				
				return ML_FALSE;
			}

			m_mutexStack.erase(mutexName);
		}
		m_mutexStack[mutexName] = (DWORD*)CreateMutex(NULL, FALSE, mutexName.c_str());
		if(m_mutexStack[mutexName] == NULL)
		{
			ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
				ML_STR("Failed to create Mutex handle %s reason: %s.\n"), mutexName.c_str(),
				_com_error::HRESULTToWCode(GetLastError()));

			m_mutexStack.erase(mutexName);

			return ML_FALSE;
		}
		return true;
	}

	ML_BOOL MLMutexDictionary::RemoveMutex(const ML_STRING& mutexName)
	{
		if(m_mutexStack[mutexName] != NULL)
		{
			if(!CloseHandle(m_mutexStack[mutexName]))
			{
				ML_TRACE(ML_LOG_THREAD, MLConsoleStatus::Error, 
					ML_STR("Failed to close Mutex handle %s reason: %s.\n"), mutexName.c_str(),
					_com_error::HRESULTToWCode(GetLastError()));

				return ML_FALSE;
			}
		}
		m_mutexStack.erase(mutexName);

		return ML_TRUE;
	}
}

