#include "MLVirtualFile.h"
#include "MLVirtualPath.h"
#include "MLConsole.h"

namespace MLKernel
{
	MLVirtualFile::MLVirtualFile(void)
	{
		m_lastRead	= 0;
		m_lastWrite = 0;
		m_file = new MLFile();
	}

	MLVirtualFile::MLVirtualFile(const ML_STRING& Name)
	{
		ML_STRING fullPath = Name;

		m_lastRead	= 0;
		m_lastWrite = 0;
		m_file = new MLFile();
		m_file->FullPath = fullPath;

		ML_UINT index = Name.find_last_of('\\');

		if(index != -1)
		{
			m_file->FileName = Name.substr(index, Name.length());
		}
		else
		{
			m_file->FileName = Name;
		}

		Exists();
		BuildFileInfo();

	}

	MLVirtualFile::MLVirtualFile(const ML_STRING& Name, const ML_STRING& Path)
	{
		ML_STRING fullPath = Path + ML_STR("\\") + Name;

		m_lastRead	= 0;
		m_lastWrite = 0;
		m_file = new MLFile();
		m_file->FullPath = fullPath;
		m_file->FileName = Name;
		Exists();
		BuildFileInfo();

	}

	MLVirtualFile::MLVirtualFile(const ML_STRING& Name, const MLKernel::MLVirtualPath* Path)
	{
		ML_STRING fullPath = Path->GetFullPath() + ML_STR("\\") + Name;

		m_lastRead	= 0;
		m_lastWrite = 0;
		m_file = new MLFile();
		m_file->FullPath = fullPath;
		m_file->FileName = Name;
		Exists();
		BuildFileInfo();
	}

	MLVirtualFile::~MLVirtualFile(void)
	{
		Release();
	}

	void MLVirtualFile::Release()
	{
		if(m_file)
		{
			if(m_file->File)
				CloseFile();
			delete m_file;
			m_file = NULL;
		}

		m_lastRead	= 0;
		m_lastWrite = 0;
	}

	ML_BOOL MLVirtualFile::OpenFile()
	{
		if(m_file)
		{
			if(m_file->FullPath.empty() != ML_TRUE)
			{
				m_file->Exists = fileExists(m_file->FullPath.c_str());
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("Path to file is empty try setting the file path first! or use a different OpenFile Method\n"));

				return ML_FALSE;
			}
			if(!m_file->Exists)
			{
				CreateNewFile(m_file->FullPath);
				m_file->Exists = fileExists(m_file->FullPath.c_str());
			}
			if(m_file->Exists && !m_file->File)
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Opening File %s\n"), m_file->FullPath.c_str());

				FILE* fp;
				ML_BOOL result = fopen_s(&fp, MLtoAscii(m_file->FullPath).c_str(), (ML_CHAR*)MLFileAccess::ReadWrite);
				if (!result)
				{
					m_file->File = fp;
					m_file->IsOpen = ML_TRUE;
					BuildFileInfo();

					m_lastRead = 0;
					m_lastWrite = 0;

					return ML_TRUE;
				}
				return ML_FALSE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File does not exist or the file is already open\n"));

				return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::OpenFile(const MLFileAccess& mode, const ML_BOOL binary)
	{
		if(m_file)
		{
			if(m_file->FullPath.empty() != ML_TRUE)
			{
				m_file->Exists = fileExists(m_file->FullPath.c_str());
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("Path to file is empty try setting the file path first! or use a different OpenFile Method\n"));

				return ML_FALSE;
			}
			if(!m_file->Exists)
			{
				CreateNewFile(m_file->FullPath);
				m_file->Exists = fileExists(m_file->FullPath.c_str());
			}
			if(m_file->Exists && !m_file->File)
			{
				ML_UINT accessMode = 'r';
				if(binary)
				{
					accessMode = ('\0' << 24) + ('b' << 16) + mode;
				}
				else
					accessMode = ('\0' << 16) + mode;

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Opening File %s in mode %s 0x%06x\n"), m_file->FullPath.c_str(), 
					MLtoUnicode((ML_CHAR*)&accessMode).c_str(), accessMode);

				FILE* fp;
				ML_BOOL error = fopen_s(&fp, MLtoAscii(m_file->FullPath).c_str(), (ML_CHAR*)&accessMode);
				if (!error)
				{
					m_file->File = fp;
					m_file->IsOpen = ML_TRUE;
					BuildFileInfo();

					m_lastRead = 0;
					m_lastWrite = 0;

					return ML_TRUE;
				}
				else
				{
					ML_TRACE(ML_STR("Virtual File"), MLConsoleStatus::Error,
						ML_STR("Failed to open file\n"));
					return ML_FALSE;
				}
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File does not exist or the file is already open\n"));

				return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::OpenFile(const ML_STRING& Name, const ML_STRING& Path)
	{
		if(m_file)
		{
			ML_STRING fullPath = Path + ML_STR("\\") + Name;
			m_file->Exists = fileExists(fullPath.c_str());
			if(!m_file->Exists)
			{
				CreateNewFile(fullPath);
				m_file->Exists = fileExists(fullPath.c_str());
			}
			if(m_file->Exists && (!m_file->File || !m_file->IsOpen))
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Opening File %s in %s\n"), Name.c_str(), Path.c_str());

				FILE* fp;
				ML_BOOL error = fopen_s(&fp, MLtoAscii(fullPath).c_str(), (ML_CHAR*)MLFileAccess::ReadWrite);
				if (!error)
				{
					m_file->File = fp;
					m_file->FullPath = fullPath;
					m_file->FileName = Name;
					m_file->IsOpen = ML_TRUE;
					BuildFileInfo();

					m_lastRead = 0;
					m_lastWrite = 0;

					return ML_TRUE;
				}
				else
				{
					ML_TRACE(ML_STR("Virtual File"), MLConsoleStatus::Error,
						ML_STR("Failed to open file\n"));
					return ML_FALSE;
				}
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File does not exist or the file is already open\n"));

				return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}
	ML_BOOL MLVirtualFile::OpenFile(const ML_STRING& Name, const MLVirtualPath* Path)
	{
		return OpenFile(Name, Path->GetFullPath());
	}

	ML_BOOL MLVirtualFile::OpenFile(const ML_STRING& Name, const ML_STRING& Path, const MLFileAccess& mode, const ML_BOOL binary)
	{

		if(m_file)
		{
			ML_STRING fullPath = Path + ML_STR("\\") + Name;
			m_file->Exists = fileExists(fullPath.c_str());
			if(!m_file->Exists)
			{
				CreateNewFile(fullPath);
				m_file->Exists = fileExists(fullPath.c_str());
			}
			if(m_file->Exists && (!m_file->File || !m_file->IsOpen))
			{
				ML_UINT accessMode = 'r';
				if(binary)
				{
					accessMode = ('\0' << 24) + ('b' << 16) + mode;
				}
				else
					accessMode = ('\0' << 16) + mode;

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Opening File %s in %s in mode %s 0x%06x\n"), Name.c_str(),
					Path.c_str(), MLtoUnicode((ML_CHAR*)&accessMode).c_str(), accessMode);

				FILE* fp;
				ML_BOOL error = fopen_s(&fp, MLtoAscii(m_file->FullPath).c_str(), (ML_CHAR*)&accessMode);
				if (!error)
				{
					m_file->File = fp;
					m_file->FileName = Name;
					m_file->IsOpen = ML_TRUE;
					BuildFileInfo();

					m_lastRead = 0;
					m_lastWrite = 0;

					return ML_TRUE;
				}
				else
				{
					ML_TRACE(ML_STR("Virtual File"), MLConsoleStatus::Error,
						ML_STR("Failed to open file\n"));
					return ML_FALSE;
				}
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File does not exist or the file is already open\n"));

				return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::OpenFile(const ML_STRING& Name, const MLVirtualPath* Path, const MLFileAccess& mode, const ML_BOOL binary)
	{
		return OpenFile(Name, Path->GetFullPath(), mode, binary);
	}

	ML_BOOL MLVirtualFile::SetFile(const ML_STRING& Name, const ML_STRING& Path)
	{
		ML_STRING fullPath = Path + ML_STR("\\") + Name;

		if(fileExists(fullPath.c_str()))
		{
			if(m_file)
			{
				if(!m_file->File && !m_file->IsOpen)
				{
					m_file->FullPath = fullPath;
					m_file->FileName = Name;
					m_file->Exists = fileExists(fullPath.c_str());
					BuildFileInfo();

					return ML_TRUE;
				}
				else
				{
					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Cant set an already open file\n"));

					return ML_FALSE;
				}
			
			}
			else
			{
				m_file = new MLFile();
				m_file->FullPath = fullPath;
				m_file->FileName = Name;
				m_file->Exists = fileExists(fullPath.c_str());
				BuildFileInfo();

				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File %s does not exist\n"), fullPath.c_str());

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::SetFile(const ML_STRING& Name, const MLVirtualPath* Path)
	{
		return SetFile(Name, Path->GetFullPath());
	}

	ML_BOOL MLVirtualFile::CloseFile() const
	{
		if(m_file)
		{
			if(IsFileOpen())
			{
				fclose(m_file->File);
				m_file->IsOpen = ML_FALSE;
				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::WriteData(void* Data, const ML_UINT ItemSize, const ML_UINT Size)
	{
		if(m_file)
		{
			if(IsFileOpen())
			{
				fwrite(Data, ItemSize, Size, m_file->File);
				m_lastWrite = ItemSize * Size;
				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::WriteString(const ML_STRING& string)
	{
		if(m_file)
		{
			if(IsFileOpen())
			{
				fputws(string.c_str(), m_file->File);
				fflush(m_file->File);
				m_lastWrite = sizeof(ML_SCHAR) * string.length();
				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	void* MLVirtualFile::ReadData(const ML_UINT ItemSize, const ML_UINT Size)
	{
		if(m_file)
		{
			if(IsFileOpen())
			{
				ML_BYTE* data = new ML_BYTE[ItemSize * Size];
				fread(data, ItemSize, Size, m_file->File);
				m_lastRead = ItemSize * Size;
				return data;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return NULL;
	}

	ML_STRING MLVirtualFile::ReadString(const ML_UINT charCount)
	{
		ML_SCHAR* string = new ML_SCHAR[charCount];
		ML_STRING result;
		result.clear();
		if(m_file)
		{
			if(IsFileOpen())
			{
				fgetws(string, charCount, m_file->File);
				m_lastRead = sizeof(ML_SCHAR) * charCount;
				result = string;
				return result;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return result;
	}

	ML_BOOL MLVirtualFile::Seek(const MLSeekMode& mode, const ML_USHORT amount/* = 0*/)
	{
		ML_INT val = amount;

		if(m_file)
		{
			if(IsFileOpen())
			{
				if(mode == MLSeekMode::Backwards || mode == MLSeekMode::ToEnd)
					fseek(m_file->File, -val, mode);
				else
					fseek(m_file->File, val, mode);

				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::EndOfFile() const
	{
		if(m_file)
		{
			if(IsFileOpen())
			{
				if(feof(m_file->File) > FALSE)
					return ML_TRUE;
				else
					return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::MoveFile(const ML_STRING& ToPath)
	{
		if(m_file)
		{
			if(IsFileClosedandValid() && folderExists(ToPath))
			{
				IShellItem* fromFile	= CreateShellItem(m_file->FullPath);
				IShellItem* toFile		= CreateShellItem(ToPath);
				IFileOperation* fileOp	= CreateOperation();
				if(fromFile && toFile && fileOp)
				{
					fileOp->MoveItem(fromFile, toFile, m_file->FileName.c_str(), NULL);
					fileOp->PerformOperations();
				}
				else
				{
					if(fromFile) fromFile->Release();
					if(toFile)	 toFile->Release();
					if(fileOp)	 fileOp->Release();

					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Could not move file %s to %s\n"), m_file->FullPath.c_str(), ToPath.c_str());

					return ML_FALSE;
				}

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Moved file %s to %s\n"), m_file->FullPath.c_str(), ToPath.c_str());

				m_file->FullPath = ToPath;

				if(fromFile) fromFile->Release();
				if(toFile)	 toFile->Release();
				if(fileOp)	 fileOp->Release();
			
				return ML_TRUE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File open or file %s path is invalid\n"), m_file->FullPath.c_str());
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::MoveFile(const MLVirtualPath* ToPath)
	{
		if(ToPath->IsValid())
			return MoveFile(ToPath->GetFullPath());
		else
		{
			ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
				ML_STR("ToPath %s is invalid\n"), ToPath->GetFullPath());

			return ML_FALSE;
		}
	}

	ML_BOOL MLVirtualFile::CopyFile(const ML_STRING& ToPath)
	{
		if(m_file)
		{
			if(IsFileClosedandValid() && folderExists(ToPath))
			{
				IShellItem* fromFile	= CreateShellItem(m_file->FullPath);
				IShellItem* toFile		= CreateShellItem(ToPath);
				IFileOperation* fileOp	= CreateOperation();
				if(fromFile && toFile && fileOp)
				{
					fileOp->CopyItem(fromFile, toFile, m_file->FileName.c_str(), NULL);
					fileOp->PerformOperations();
				}
				else
				{
					if(fromFile) fromFile->Release();
					if(toFile)	 toFile->Release();
					if(fileOp)	 fileOp->Release();

					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Could not copy file %s to %s\n"), m_file->FullPath.c_str(), ToPath.c_str());

					return ML_FALSE;
				}

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Copying file %s to %s\n"), m_file->FullPath.c_str(), ToPath.c_str());

				if(fromFile) fromFile->Release();
				if(toFile)	 toFile->Release();
				if(fileOp)	 fileOp->Release();

				return ML_TRUE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File open or file %s path is invalid\n"), m_file->FullPath.c_str());
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::CopyFile(const MLVirtualPath* ToPath)
	{
		if(ToPath->IsValid())
			return CopyFile(ToPath->GetFullPath());
		else
		{
			ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
				ML_STR("ToPath %s is invalid\n"), ToPath->GetFullPath().c_str());

			return ML_FALSE;
		}
	}

	ML_BOOL MLVirtualFile::RenameFile(const ML_STRING& NewName)
	{
		if(m_file)
		{
			if(IsFileClosedandValid())
			{
				IShellItem* fromFile	= CreateShellItem(m_file->FullPath);
				IFileOperation* fileOp	= CreateOperation();
				if(fromFile && fileOp)
				{
					fileOp->RenameItem(fromFile, NewName.c_str(), NULL);
					fileOp->PerformOperations();
				}
				else
				{
					if(fromFile) fromFile->Release();
					if(fileOp)	 fileOp->Release();

					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("could not create shell item or file operation for %s\n"), m_file->FullPath.c_str());

					return ML_FALSE;
				}

				ML_UINT index = m_file->FullPath.find_last_of(ML_STR("\\"));
				ML_STRING name = m_file->FullPath.substr(index);

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Copying file %s to %s\n"), name.c_str(), NewName.c_str());

				m_file->FileName = NewName;
				m_file->FullPath.resize(index);
				m_file->FullPath = m_file->FullPath + ML_STR("\\") + NewName;

				if(fromFile) fromFile->Release();
				if(fileOp)	 fileOp->Release();

				return ML_TRUE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File open or file %s path is invalid\n"), m_file->FullPath.c_str());
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::DeleteFile()
	{
		if(m_file)
		{
			if(IsFileClosedandValid())
			{
				IShellItem* fromFile	= CreateShellItem(m_file->FullPath);
				IFileOperation* fileOp	= CreateOperation();
				if(fromFile && fileOp)
				{
					fileOp->DeleteItem(fromFile, NULL);
					fileOp->PerformOperations();
				}
				else
				{
					if(fromFile) fromFile->Release();
					if(fileOp)	 fileOp->Release();

					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Shell item or file operation could not be created\n"));

					return ML_FALSE;
				}

				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
					ML_STR("Deleting this file. Releasing this\n"));

				if(fromFile) fromFile->Release();
				if(fileOp)	 fileOp->Release();

				Release();

				return ML_TRUE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File open or file %s path is invalid\n"), m_file->FullPath.c_str());
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	//This will be used when MLMD5 is implemented. It will use the file size to create an
	//MD5 which can be checked against an existing MD5. A database of MD5's can be generated
	//for all game files and this will be used to check if the file is valid
	//ML_BOOL VarifyMD5(MLMD5Hash* Hash); 

	ML_BOOL MLVirtualFile::Exists() const
	{
		if(m_file)
		{
			if (PathFileExists(m_file->FullPath.c_str()))
				m_file->Exists = ML_TRUE;
			else
				m_file->Exists = ML_FALSE;

			return m_file->Exists;
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::IsOpen() const
	{
		if (m_file)
		{
			return IsFileOpen();
		}
		
		return ML_FALSE;
	}

	ML_STRING MLVirtualFile::FullFilePath() const
	{
		if (m_file)
		{
			if(Exists())
				return m_file->FullPath;
			else
				return m_file->FullPath;
		}

		return ML_STR("");
	}

	ML_BOOL MLVirtualFile::CreateNewFile(const ML_STRING& fullFilePath)
	{
		if(m_file)
		{
			FILE* fp;
			ML_BOOL error = fopen_s(&fp, MLtoAscii(fullFilePath).c_str(), "wb");
			if(error)
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("New file at %s could not be created\n"), fullFilePath.c_str());

				return ML_FALSE;
			}

			ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::FileAccess, 
				ML_STR("Created new File at %s\n"), fullFilePath.c_str());

			fclose(fp);
			m_file->FullPath = fullFilePath;
			return ML_TRUE;
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::SetFileName(const ML_STRING& name)
	{
		if(m_file)
		{
			if (m_file->IsOpen)
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File is already open. Close it before attempting to set a different file name\n"));
				
			}
			else
			{
				ML_INT nameIndex = m_file->FullPath.find_last_of(m_file->FileName);
				if(nameIndex == -1)
				{
					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Failed to find current file name %s in full file path %s. Wont be able to set name to %s\n"), m_file->FileName, m_file->FullPath, name);
					return ML_FALSE;
				}
				m_file->FullPath.resize(nameIndex);
				m_file->FullPath += (name + m_file->Extention);
				m_file->FileName = name;

				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::SetFileExtention(const ML_STRING& ext)
	{
		if(m_file)
		{
			if (m_file->IsOpen)
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
					ML_STR("File is already open. Close it before attempting to set a different file name\n"));
				return ML_FALSE;
			}
			else
			{
				ML_INT extIndex = m_file->FullPath.find_last_of(m_file->Extention);
				if(extIndex == -1)
				{
					ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
						ML_STR("Failed to find current file extention %s in full file path %s. Wont be able to set extention to %s\n"), m_file->Extention, m_file->FullPath, ext);
					return ML_FALSE;
				}
				m_file->FullPath.resize(extIndex);
				m_file->FullPath += ext;
				m_file->Extention = ext;

				return ML_TRUE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	ML_BOOL MLVirtualFile::BuildFileInfo()
	{
		if(m_file)
		{
			if(m_file->FullPath.empty() != ML_TRUE && m_file->Exists)
			{
				WIN32_FILE_ATTRIBUTE_DATA fileInfo;
				ML_BOOL err = ML_TRUE;
				if(GetFileAttributesEx(m_file->FullPath.c_str(), GetFileExInfoStandard, &fileInfo) > FALSE)
					err = ML_FALSE;
				m_file->Created		= fileInfo.ftCreationTime;
				m_file->Accessed	= fileInfo.ftLastAccessTime;
				m_file->LastWrite	= fileInfo.ftLastWriteTime;
				m_file->Size		= fileInfo.nFileSizeLow;
				m_file->Extention	= m_file->FullPath.substr(m_file->FullPath.find_last_of(ML_STR(".")));

				return ML_TRUE;
			}
			else
			{
				ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Warning, 
					ML_STR("Could not build file info for file at %s may be empty or does not exist\n"), m_file->FullPath.c_str());

				return ML_FALSE;
			}
		}

		ML_TRACE( ML_STR("Virtual File"), MLConsoleStatus::Error, 
			ML_STR("File structure has not been created correctly or has been deleted\n"));

		return ML_FALSE;
	}

	IShellItem* MLVirtualFile::CreateShellItem(const ML_STRING& path)
	{
		IShellItem* psiTo = NULL;
		HRESULT hr = SHCreateItemFromParsingName(path.c_str() , NULL, IID_PPV_ARGS(&psiTo));
		if (SUCCEEDED(hr))
		{
			return psiTo;
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create shellitem from path %s\n"), path.c_str());
			return NULL;
		}
	}

	IFileOperation* MLVirtualFile::CreateOperation()
	{
		IFileOperation* fileOp;
		HRESULT hr = CoCreateInstance(CLSID_FileOperation, NULL, CLSCTX_ALL, IID_PPV_ARGS(&fileOp));
		if(SUCCEEDED(hr))
		{
			return fileOp;
		}
		else
		{
			ML_TRACE( ML_STR("Virtual Path"), MLConsoleStatus::Error, 
				ML_STR("Could not create FileOperation\n"));
			return NULL;
		}
	}

	ML_STRING MLVirtualFile::ToString() const
	{
		return	m_file->FullPath + ML_STR(" File Open = ") + 
				(m_file->IsOpen ? ML_STR("true") : ML_STR("false")) +
				ML_STR(" Created = ") + m_file->Created.ToString() +
				ML_STR(" Size = ");
	}
}