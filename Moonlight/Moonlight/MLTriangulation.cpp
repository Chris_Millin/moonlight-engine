#include "MLTriangulation.h"

namespace MLRender
{

	MLCoreDLL std::vector<MLVector2f> MLTriangulation::Triangulate(MLPolyList* poly)
	{
		CheckDirection(poly);

		if (poly->ChildSize() > 0)
		{
			MergePoly(poly);
		}

		return CreateEars(poly);
	}

	MLCoreDLL void MLTriangulation::CheckDirection(MLPolyList* poly)
	{
		//Parent
		MLPolyNode* point = poly->Start();

		ML_UINT total = 0;

		for(ML_UINT i = 0; i < poly->Size(); i++)
		{
			total += (ML_UINT)((point->next->point.x - point->point.x) * (point->next->point.y + point->point.y));
			point = point->next;
		}

		if (total >= 0)
			poly->Reverse();

		//Children
		/*for( int i = 0; i < poly->Size(); i++ )
		{
		point = poly->GetChild(i)->Start();
		total = 0;

		if( point == NULL )
		break;

		for( int j = 0; j < poly->GetChild(i)->Size( ); j++ )
		{
		total+=(point->next->point.x - point->point.x) * (point->next->point.y + point->point.y);
		point = point->next;
		}

		if (total >= 0)
		poly->Reverse(-1);
		}*/
	}

	MLCoreDLL void MLTriangulation::MergePoly(MLPolyList* poly)
	{

	}

	MLCoreDLL std::vector<MLVector2f> MLTriangulation::CreateEars(MLPolyList* poly)
	{
		std::vector<MLVector2f> finalPoints;

		ML_BOOL earFound;
	
		MLPolyNode* point = poly->Start();
		while(poly->Size() >= 3)
		{
			earFound = ML_TRUE;
			if (IsConvex(point))
			{
				if (IsEar(point))
				{
					finalPoints.push_back(point->previous->point);
					finalPoints.push_back(point->point);
					finalPoints.push_back(point->next->point);

					point = point->next;
					poly->RemovePoint(*point->previous);
				}
				else
					point = point->next;
			}
			else
				point = point->next;
		}

		return finalPoints;
	}

	MLCoreDLL ML_BOOL MLTriangulation::IsConvex(MLPolyNode* point)
	{
		return ( ( point->previous->point.x * ( point->next->point.y - point->point.y ) ) + ( point->point.x * ( point->previous->point.y - point->next->point.y ) ) + ( point->next->point.x * ( point->point.y - point->previous->point.y ) ) ) < 0;
	}


	MLCoreDLL ML_BOOL MLTriangulation::IsEar(MLPolyNode* point)
	{
		MLPolyNode* checkNode = point->next->next;
		while (checkNode != point->previous)
		{
			if (InTriangle(checkNode, point))
			{
				return ML_FALSE;
			}

			checkNode = checkNode->next;
		}

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLTriangulation::InTriangle(MLPolyNode* checkPoint, MLPolyNode* ear)
	{
		if (checkPoint->point == ear->point ||
			checkPoint->point == ear->next->point ||
			checkPoint->point == ear->previous->point)
		{
			return ML_FALSE;
		}


		MLVector2f v0 = ear->previous->point - ear->point;
		MLVector2f v1 = ear->next->point - ear->point;
		MLVector2f v2 = checkPoint->point - ear->point;

		ML_FLOAT u = (MLMath::MLVec2::MLdot(v1, v1) * MLMath::MLVec2::MLdot(v2, v0) - MLMath::MLVec2::MLdot(v1, v0) * MLMath::MLVec2::MLdot(v2, v1)) / 
					(MLMath::MLVec2::MLdot(v0, v0) * MLMath::MLVec2::MLdot(v1, v1) - MLMath::MLVec2::MLdot(v0, v1) * MLMath::MLVec2::MLdot(v1, v0));
		ML_FLOAT v = (MLMath::MLVec2::MLdot(v0, v0) * MLMath::MLVec2::MLdot(v2, v1) - MLMath::MLVec2::MLdot(v0, v1) * MLMath::MLVec2::MLdot(v2, v0)) / 
					(MLMath::MLVec2::MLdot(v0, v0) * MLMath::MLVec2::MLdot(v1, v1) - MLMath::MLVec2::MLdot(v0, v1) * MLMath::MLVec2::MLdot(v1, v0));

		if (u < 0 || v < 0 || u > 1 || v > 1 || (u + v) > 1)
			return ML_FALSE;

		return ML_TRUE;
	}

}