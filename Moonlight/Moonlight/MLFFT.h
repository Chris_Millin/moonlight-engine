//#ifndef MLFFT_H
//#define MLFFT_H
//
//
//#include "MLCore.h"
//#include "MLAudioManager.h"
//#include <fftw3.h>
//
//
////TO DO
//// Sort the sampleDely out so that PerformFFT does it automatic
//// Allow for the FFT to be performed outside of the update
//// so that the file can be analyzed before it is run.
//namespace MLAudio
//{
//
//	//=========================================================
//	// Name: Fast Fourier Transform
//	// Namespace: MLAudio
//	// Description: This class allows for a wave file to be
//	// analyzed (only while it is playing at the moment)
//	// 
//	// To use this class:
//	// Load the wave file with Audio Manager and obtain the WAVEID
//	// Then load the wave file into the FFT class
//	// Next you Create the FFT with the amount of samples you wish
//	//  
//	// Then call PerformFFT per update.
//	// GetResultPointer can be called to get the FFT Result 
//	// GetResultCount Should also be used to get the amount of
//	// values stored the in the result array so they can be looped
//	// through and processed
//	//
//	// Creator: Chris Millin
//	// Libraries: fftwf (float version is used in this class)
//	//=========================================================
//	class MLFFT
//	{
//	public:
//		MLCoreDLL MLFFT(void);
//		MLCoreDLL virtual ~MLFFT(void);
//
//		MLCoreDLL bool release(void);
//
//		MLCoreDLL bool LoadWavFFTData(MLAudioFile* af);
//		MLCoreDLL bool CreateFFT(int nSamples, double sampleDelay);
//		MLCoreDLL bool PerformFFT(double deltaTime);
//
//		MLCoreDLL bool SetResultPointer(float* result,int count);
//		MLCoreDLL float* GetResultPointer(void) { return m_resultData; }
//
//		MLCoreDLL int GetResultCount(void) { return m_resultCount; }
//
//	private:
//
//		WAVEID m_currentWavID;
//
//		fftwf_plan m_fftPlan;
//		fftwf_complex* m_fftResult;
//
//		float* m_wavData;
//
//		float* m_inData;
//		float* m_resultData;
//
//		bool m_custonResult;
//
//		int m_sampleCount;
//		int m_resultCount;
//		double m_sampleDelay;
//	};
//}
//
//
//#endif