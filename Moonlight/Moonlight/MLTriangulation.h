#ifndef MLTRIANGULATION_H
#define MLTRIANGULATION_H

#include "MLPolyList.h"

#include <vector>
#include <MLMath/MLMath.h>

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLTriangulation
	{
	public:

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL std::vector<MLVector2f> static Triangulate(MLPolyList* poly);

	private:

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static CheckDirection(MLPolyList* poly);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void static MergePoly(MLPolyList* poly);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL std::vector<MLVector2f> static CreateEars(MLPolyList* poly);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static InTriangle(MLPolyNode* checkPoint, MLPolyNode* ear);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static IsConvex(MLPolyNode* point);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL static IsEar(MLPolyNode* point);

	};

}

#endif