#include "MLAudioThread.h"

namespace MLAudio
{
	MLAudioThread::MLAudioThread() : MLKernel::MLThread()
	{
		m_activeAudio.clear();
	}


	MLAudioThread::~MLAudioThread()
	{
		m_activeAudio.clear();
		Release();
	}

	void MLAudioThread::Release()
	{
		m_activeAudio.clear();
		MLKernel::MLThread::Release();
	}

	ML_BOOL MLAudioThread::Intialise()
	{
		m_pauseThread = ML_FALSE;

		MLKernel::MLUUID mutexKey;
		m_mutex = mutexKey.Generate();

		if (!MLKernel::MLMutexDictionary::AddMutex(m_mutex))
		{
			ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
				ML_STR("Failed to create Mutex attempting again with a new UUID.\n"));

			m_mutex = mutexKey.Generate();
			if (!MLKernel::MLMutexDictionary::AddMutex(m_mutex))
			{
				ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
					ML_STR("Failed to create Mutex again thread may get deadlocked.\n"));
				return ML_FALSE;
			}
		}

		ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Thread, ML_STR("Creating audio update thread.\n"));
		if (!ThreadCreate())
		{
			ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error, 
				ML_STR("Failed to create thread audio will require manual updating.\n"));
			return ML_FALSE;
		}

		return ML_TRUE;
	}
	
	ML_BOOL MLAudioThread::SetMusicVolume(const ML_FLOAT gain)
	{
		if (ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT))
		{
			for (ML_UINT i = 0; i < m_activeAudio.size(); i++)
			{
				m_activeAudio[i]->SetMusicVolume(gain);
				
				ThreadReleaseMutex(m_mutex);
				return ML_TRUE;
			}

			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
				ML_STR("Waiting for mutex timed out after 1 min.\n"));
			return ML_FALSE;
		}

		return ML_FALSE;
	}

	ML_BOOL MLAudioThread::AddMusic(MLMusic* music)
	{
		if (ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT))
		{
			std::vector<MLMusic*>::iterator it;
			for (it = m_activeAudio.begin(); it != m_activeAudio.end(); it++)
			{
				if (*it == music)
				{
					ThreadReleaseMutex(m_mutex);
					return ML_TRUE;
				}
			}

			m_activeAudio.push_back(music);
			ThreadReleaseMutex(m_mutex);
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
				ML_STR("Waiting for mutex timed out after 1 min.\n"));
			return ML_FALSE;
		}
	}

	ML_BOOL MLAudioThread::RemoveMusic(MLMusic* music)
	{
		if (ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT))
		{
			ML_INT index = -1;
			for (ML_UINT i = 0; i < m_activeAudio.size(); i++)
			{
				if (m_activeAudio[i]== music)
				{
					index = i;
					break;
				}
			}

			if (index != -1 && index < (ML_INT)m_activeAudio.size())
				m_activeAudio.erase(m_activeAudio.begin() + index);

			ThreadReleaseMutex(m_mutex);
			return ML_TRUE;
		}
		else
		{
			ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
				ML_STR("Waiting for mutex timed out after 1 min.\n"));
			return ML_FALSE;
		}
	}

	void MLAudioThread::threadEntry()
	{
		while (1)
		{
			if (!m_pauseThread)
			{
				if (ThreadWaitMutex(m_mutex, ML_AUDIO_MUTEX_TIMEOUT))
				{
					for (ML_UINT i = 0; i < m_activeAudio.size(); i++)
					{
						m_activeAudio[i]->Update();
					}

					ThreadReleaseMutex(m_mutex);
				}
				else
				{
					ML_TRACE(ML_LOG_AUDIO_THREAD, MLKernel::MLConsoleStatus::Error,
						ML_STR("Waiting for mutex timed out after 1 min.\n"));
				}
			}
		}
	}
}