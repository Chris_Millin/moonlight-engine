#include "MLWindowsContext.h"

using namespace MLWindows;

#ifdef __cplusplus
extern "C" {
#endif

#define IDC_MAIN_BUTTON 101 


//static prototyping
MLCoreDLL HWND			MLWindowsContext::m_hWnd;
MLCoreDLL ML_INT		MLWindowsContext::m_wHeight;
MLCoreDLL ML_INT		MLWindowsContext::m_wWidth;
MLCoreDLL ML_BOOL		MLWindowsContext::m_fullScreen;
MLCoreDLL ML_INT		MLWindowsContext::m_BitMode;
MLCoreDLL ResizeFnc		MLWindowsContext::m_resizeFunction;
MLCoreDLL InputFnc		MLWindowsContext::m_inputFunction;

MLCoreDLL FILETIME		MLWindowsContext::m_time;
MLCoreDLL ML_DOUBLE		MLWindowsContext::m_currentTime;
MLCoreDLL ML_DOUBLE		MLWindowsContext::m_deltaTime;
MLCoreDLL ML_DOUBLE		MLWindowsContext::m_oldTime;



MLCoreDLL MLWindowsContext::MLWindowsContext(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, ML_INT nCmdShow) : m_hInstance(hInstance), m_hPrevInstance(hPrevInstance),
	m_lpCmdLine(lpCmdLine), m_nCmdShow(nCmdShow), m_clsName(L"MoonLight")
{
	m_oldTime		= 0;
	m_deltaTime		= 0;
	m_currentTime	= 0;
	
	m_resizeFunction	= NULL;
	m_inputFunction		= NULL;
}

MLWindowsContext::~MLWindowsContext(void)
{
	Release();
}

MLCoreDLL ML_BOOL MLWindowsContext::Release()
{
	if (m_hWnd && !DestroyWindow(m_hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,L"Could Not Release hWnd.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		m_hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass(m_clsName ,m_hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,L"Could Not Unregister Class.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		m_hInstance=NULL;									// Set hInstance To NULL
	}

	return ML_TRUE;
}

MLCoreDLL ML_BOOL MLWindowsContext::Create(const ML_STRING& title, const ML_INT width, const ML_INT height, const ML_INT BitCount, const ML_BOOL Console)
{
	if (Console == ML_TRUE)
		LoadConsole();

	m_wWidth	= width;
	m_wHeight	= height;
	m_BitMode	= BitCount;

	WNDCLASSEX WndCls;

	// Create the application window
	m_hInstance			 = GetModuleHandle(NULL);
	WndCls.style         = CS_HREDRAW;
	WndCls.lpfnWndProc	 = WindowsMessage;
	WndCls.cbSize		 = sizeof(WNDCLASSEX);
	WndCls.cbClsExtra    = NULL;
	WndCls.cbWndExtra    = NULL;
	WndCls.hInstance	 = m_hInstance;
	WndCls.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	WndCls.hCursor       = LoadCursor(NULL, IDC_ARROW);
	WndCls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndCls.lpszMenuName  = NULL;
	WndCls.lpszClassName = m_clsName;
	WndCls.hInstance     = m_hInstance;
	WndCls.hIconSm		 = NULL;

	// Register the application
	if (!RegisterClassEx(&WndCls))									// Attempt To Register The Window Class
	{
		MessageBox(NULL, ML_STR("Failed To Register The Window Class."), ML_STR("WIN_ERROR"), MB_OK|MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}

	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT windowRect = {0, 0, m_wWidth, m_wHeight};


	if (m_fullScreen)											
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;								
		dwStyle=WS_POPUP;										
		SetDisplayMode(m_fullScreen);
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			
		dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;					
		AdjustWindowRectEx (&windowRect, dwStyle, 0, dwExStyle);
		SetDisplayMode(m_fullScreen);
	}



	// Create the window object
	if(!(m_hWnd = CreateWindowEx(dwExStyle,
		m_clsName,
		title.c_str(),
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN |					// Required Window Style
		dwStyle ,							// Defined Window Style,				
		CW_USEDEFAULT, CW_USEDEFAULT,		// Window Position
		windowRect.right - windowRect.left,	// Window Width
		windowRect.bottom - windowRect.top,	// Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		m_hInstance,						// Instance
		NULL)))
	{
		MessageBox(NULL, ML_STR("Window Creation Error."), ML_STR("WIN_ERROR"), MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	GetSystemTimeAsFileTime(&m_time);

	srand((LONGLONG)m_time.dwLowDateTime + ((LONGLONG)(m_time.dwHighDateTime) << 32LL));

	UpdateWindow(m_hWnd);

	return ML_TRUE;
}

MLCoreDLL ML_BOOL MLWindowsContext::BindToOpenGL()
{
	return MLRender::MLGLContext::Bind(m_hInstance, m_hWnd, m_wWidth, m_wHeight, m_BitMode);
}

MLCoreDLL ML_BOOL MLWindowsContext::Update()
{
	m_currentTime = GetRunningTime();
	m_deltaTime = m_currentTime - m_oldTime; 
	m_oldTime = m_currentTime;

	if(PeekMessage(&m_msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&m_msg);
		DispatchMessage(&m_msg);

		if(m_msg.message == WM_QUIT)
		{
			MLKernel::MLConsole::Shutdown();
			Release();
			return 0;
		}
		//keyboard input
		if (m_msg.wParam == VK_ESCAPE)
		{
			//do some stuff on keypress
			Release();
			return ML_FALSE;
		}
	}

	return ML_TRUE;
}

MLCoreDLL LRESULT CALLBACK MLWindowsContext::WindowsMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (m_inputFunction != NULL)
		m_inputFunction(lParam, uMsg, wParam);

	switch(uMsg)
	{
	case WM_CREATE:
		{
			break;
		}
		// If the user wants to close the application
	case WM_DESTROY:
		{
			// then close it
			MLKernel::MLConsole::Shutdown();
			PostQuitMessage(WM_QUIT);
			break;	
		}
	case WM_SIZE:
		{
			if (m_resizeFunction != NULL)
				m_resizeFunction(LOWORD(lParam), HIWORD(lParam));
			break;
		}
	default:
		// Process the left-over messages
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	// If something was not done, let it go
	return DefWindowProc(hWnd, uMsg, wParam, lParam);;	
}


MLCoreDLL ML_DOUBLE MLWindowsContext::GetRunningTime()
{
	FILETIME time;
	GetSystemTimeAsFileTime(&time);

	LONGLONG currentTime = (LONGLONG)time.dwLowDateTime + ((LONGLONG)(time.dwHighDateTime) << 32LL);
	currentTime /= 10000;
	LONGLONG startTime = (LONGLONG)m_time.dwLowDateTime + ((LONGLONG)(m_time.dwHighDateTime) << 32LL);
	startTime /= 10000;

	return (ML_DOUBLE)(currentTime - startTime)/1000;
}

MLCoreDLL void MLWindowsContext::LoadConsole()
{
	ML_INT hConHandle;

	ML_DWORD lStdHandle;

	CONSOLE_SCREEN_BUFFER_INFO coninfo;

	FILE *fp;

	// allocate a console for this app

	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&coninfo);

	coninfo.dwSize.Y = MAX_CONSOLE_LINES;

	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),11);

	// redirect unbuffered STDOUT to the console

	lStdHandle = (ML_DWORD)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stdout = *fp;

	setvbuf( stdout, NULL, _IONBF, 0 );

	// redirect unbuffered STDIN to the console

	lStdHandle = (ML_DWORD)GetStdHandle(STD_INPUT_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "r" );

	*stdin = *fp;

	setvbuf( stdin, NULL, _IONBF, 0 );

	// redirect unbuffered STDERR to the console

	lStdHandle = (ML_DWORD)GetStdHandle(STD_ERROR_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stderr = *fp;

	setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog

	// point to console as well

	std::ios_base::sync_with_stdio();
}

MLCoreDLL void MLWindowsContext::SetDisplayMode(ML_BOOL FullScreen)
{
	m_fullScreen = FullScreen;

	if (m_fullScreen)
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= m_wWidth;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= m_wHeight;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= m_BitMode;				// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,L"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?",L"WIN_ERROR",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				m_fullScreen=FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,L"Program Will Now Close. (FOR THE MOMENT THIS WILL JUST SET IT TO NON FULL SCREEN",L"WIN_ERROR",MB_OK|MB_ICONSTOP);
				m_fullScreen=FALSE;								// Return FALSE
			}
		}
		else
		{
			SetWindowLongPtr(m_hWnd, GWL_STYLE,
				WS_SYSMENU | WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE);
			MoveWindow(m_hWnd, 0, 0, m_wWidth, m_wHeight, TRUE);
		}
	}

	if (!m_fullScreen)
	{
		if (ChangeDisplaySettings(NULL, 0)!=DISP_CHANGE_SUCCESSFUL)
		{
			printf("Test Error couldnt change back");
		}
		else
		{
			RECT rect;
			rect.left = 0;
			rect.top = 0;
			rect.right = m_wWidth;
			rect.bottom = m_wHeight;

			SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE);
			AdjustWindowRect(&rect, WS_CAPTION | WS_POPUPWINDOW, FALSE);
			MoveWindow(m_hWnd, 0, 0, rect.right - rect.left, rect.bottom - rect.top, TRUE);
		}
	}
	
	
}

#ifdef __cplusplus
};
#endif