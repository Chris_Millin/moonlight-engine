#ifndef MLTEXTUREMANAGER_H
#define MLTEXTUREMANAGER_H

#include "MLGLContext.h"
#include "MLIncludes.h"
#include "MLTexture.h"

#include <map>

#define ML_LOG_TEXTURE_MANAGER ML_STR("Texture Manager")

namespace MLRender
{
	//=========================================================
	// Name: MLTextureManager
	// Namespace: MLRenderer
	// Description: This is a manager for the textures so that
	// texture ID's can be managed and contained. information
	// about the texture is also stored in the manager
	// Creator: Chris Millin
	// Librarys: OpenGL, FreeImage
	//=========================================================
	class MLTextureManager
	{
	public:

		//=========================================================
		// Name: Load Texture
		// Description: Will load a texture using the name and file
		// path. Returns the new Texture object.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLTexture* LoadTexture(ML_STRING& name, const ML_STRING& path);

		//=========================================================
		// Name: Load Texture
		// Description: Will load a texture using the name and virtual
		// file path object. Returns the new Texture object.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLTexture* LoadTexture(ML_STRING& name, const MLKernel::MLVirtualFile* path);
		
		//=========================================================
		// Name: Create Texture
		// Description: Will create a new empty texture with a UUID
		// name.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLTexture* CreateTexture();

		//=========================================================
		// Name: Create Texture
		// Description: Will create a new empty texture with the
		// defined name.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLTexture* CreateTexture(ML_STRING& name);

		//=========================================================
		// Name: Remove Texture
		// Description: Will remove the defined texture by its unique
		// name id.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveTexture(const ML_STRING& name);

		//=========================================================
		// Name: Remove Texture
		// Description: Remove a texture using the texture object
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveTexture(const MLTexture* texture);
		
		//=========================================================
		// Name: Remove All
		// Description: Removes all the texture objects in the manager
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL RemoveAll();

		//=========================================================
		// Name: Texture Exist
		// Description: Checks to see if a texture name exists
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL TextureExist(const ML_STRING& name);

		//=========================================================
		// Name: Get Texture
		// Description: Returns the Texture the is linked to a name
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static MLTexture* GetTexture(const ML_STRING& name);

		//=========================================================
		// Name: Get Texture Name
		// Description: Returns the unique name assigned to the 
		// texture the is linked to a name
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_STRING GetTextureName(const MLTexture* texture);
	
	private:

		//Constructor
		MLCoreDLL MLTextureManager(void) {};
		//Deconstructor
		MLCoreDLL virtual ~MLTextureManager(void) {};

	private:
		
		typedef std::map<ML_STRING, MLTexture*>	MLTextureGroup;
		//TextureLocMap stores the GLuint as the UID and stores the texture location in the map
		//when the user loads another already loaded texture it will iterate through the map
		//and find the location and the key to go with it which will then be passed back
		MLCoreDLL static MLTextureGroup m_textures;
	};
}

#endif

