#ifndef WINDOWSHANDLER_H
#define WINDOWSHANDLER_H

#include <windows.h>
#include <WindowsX.h>
#include <MMSystem.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>

#include "MLIncludes.h"
#include "MLGLContext.h"

#define MAX_CONSOLE_LINES 500;
#define WIN32_LEAN_AND_MEAN


#ifdef __cplusplus
extern "C" {
#endif


namespace MLWindows
{
	//windows resize handler
	typedef void(*ResizeFnc)(unsigned ML_INT, unsigned ML_INT);
	//windows input handler
	typedef void(*InputFnc)(LPARAM lParam, UINT message, WPARAM wParam);

	class  MLWindowsContext
	{
	public:
		//Constructor
		//////////////////////////////////////////////////////////////////////////

		//=========================================================
		// Name: Windows Context
		// Description: Creates the windows Context using the
		// windows objects
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLWindowsContext(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, ML_INT nCmdShow);
		MLCoreDLL virtual ~MLWindowsContext(void);
		//////////////////////////////////////////////////////////////////////////

		//=========================================================
		// Name: Release
		// Description: Release all the windows objects
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release(void);

		//=========================================================
		// Name: 
		// Description:
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Create(const ML_STRING& title, ML_INT Width = MLScreenWidth, ML_INT Height = MLScreenHeight,
								 ML_INT BitCount = ML_DEFAULT_BIT_COLOUR, ML_BOOL Console = ML_TRUE);
		
		//=========================================================
		// Name: 
		// Description:
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL BindToOpenGL();
		
		//=========================================================
		// Name: 
		// Description:
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Update(void);
		
		//Getters
		//////////////////////////////////////////////////////////////////////////
		#pragma region Getters
		//=========================================================
		// Name: Running Time
		// Description: Get the total running time of the application
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_DOUBLE GetRunningTime(void);

		//=========================================================
		// Name: Delta Time
		// Description: Get the Detla running time of the application
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_DOUBLE GetDeltaTime(void) 			{ return m_deltaTime; }

		//=========================================================
		// Name: Window Width
		// Description: Get the current window width
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_INT GetWindowWidth(void)	const			{ return m_wWidth; }

		//=========================================================
		// Name: Window Height
		// Description: Get the current window height
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_INT GetWindowHeight(void)	const			{ return m_wHeight; }

		//=========================================================
		// Name: Display Mode
		// Description: Gets if the application is full screen or
		// not
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL GetDisplayMode(void)	const			{ return m_fullScreen; }
		#pragma endregion
		//////////////////////////////////////////////////////////////////////////

		//Setters
		//////////////////////////////////////////////////////////////////////////
		#pragma region Setters
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetText(const ML_STRING& Text)		{ SetWindowText(m_hWnd, Text.c_str()); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetResizeCallBack(ResizeFnc Function) { m_resizeFunction = Function; m_resizeFunction(m_wWidth, m_wHeight); }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetInputCallBack(InputFnc Function)	{ m_inputFunction = Function; }

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetDisplayMode(ML_BOOL m_fullscreen);
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void SetWindowsize(ML_UINT width, ML_UINT height);
		#pragma endregion
		//////////////////////////////////////////////////////////////////////////

	private:

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static LRESULT CALLBACK WindowsMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
		
		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void LoadConsole();
		
	private:

		MLCoreDLL static FILETIME		m_time;
		MLCoreDLL static ML_DOUBLE		m_deltaTime;
		MLCoreDLL static ML_DOUBLE		m_currentTime;
		MLCoreDLL static ML_DOUBLE		m_oldTime;
		MLCoreDLL static ResizeFnc		m_resizeFunction;
		MLCoreDLL static InputFnc		m_inputFunction;
		//These should be the only public variables used within the whole application
		MLCoreDLL static HWND		m_hWnd;
		MLCoreDLL static ML_INT		m_wHeight;
		MLCoreDLL static ML_INT		m_wWidth;
		MLCoreDLL static ML_BOOL	m_fullScreen;
		MLCoreDLL static ML_INT		m_BitMode;

		MSG				m_msg;

		HINSTANCE		m_hInstance;
		HINSTANCE		m_hPrevInstance;
		LPSTR			m_lpCmdLine;
		ML_INT			m_nCmdShow;
		LPCWSTR			m_clsName;

		
	};
}

#ifdef __cplusplus
}
#endif

#endif //WINDOWSHANDLER_H