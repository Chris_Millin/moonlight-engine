#include "MLRenderTarget.h"

namespace MLRender
{

	MLRenderTarget::MLRenderTarget(void)
	{
		m_outputTexture = NULL;
		m_FBO = NULL;
	}


	MLRenderTarget::~MLRenderTarget(void)
	{
		Release();
	}

	void MLRenderTarget::Release()
	{
		MLTextureManager::RemoveTexture(m_outputTexture);
		MLRELEASE(m_FBO);
	}

	void MLRenderTarget::Initalise(MLFBOTypes enableFBO /* = MLFBOTypes::ColourBuffer */)
	{
		m_outputTexture = MLTextureManager::CreateTexture();
		m_outputTexture->CreateEmptyTexture(1280, 720); //THIS SHOULD BE SCREEN X AND Y!!!

		m_FBO = new MLFrameBuffer();
		m_FBO->CreateBuffer(GL_FRAMEBUFFER);
		m_FBO->Bind();

		if(enableFBO == MLFBOTypes::ColourBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_COLOR_ATTACHMENT0);
		else if(enableFBO == MLFBOTypes::StencilBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_STENCIL_ATTACHMENT);
		else if(enableFBO == MLFBOTypes::DepthBuffer)
			m_FBO->AttachTexture(m_outputTexture, GL_DEPTH_ATTACHMENT);

		if(m_FBO)
		{
			if(m_FBO->GetStatus() == GL_FRAMEBUFFER_COMPLETE)
				ML_TRACE(ML_LOG_RENDER_TARGET, MLKernel::MLConsoleStatus::Information, ML_STR("Frame Buffer Status OK"));
			m_FBO->UnBind();
		}
	}

	void MLRenderTarget::Bind()
	{
		m_FBO->Bind();
	}

	void MLRenderTarget::Draw(MLGraphics* batch)
	{
		MLSprite sprite;
		sprite.Texture = m_outputTexture;
		sprite.SetSizeFromTexture();
		sprite.SetSourceFromTexture();

		batch->Draw(&sprite);
	}

	void MLRenderTarget::Unbind()
	{
		m_FBO->UnBind();
	}

}