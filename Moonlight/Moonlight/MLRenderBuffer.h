#ifndef MLRENDERBUFFER_H
#define MLRENDERBUFFER_H

#include "MLGLContext.h"
#include "MLIncludes.h"

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLRenderBuffer
	{
	public:
		MLCoreDLL MLRenderBuffer(void);
		MLCoreDLL ~MLRenderBuffer(void);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Release();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void CreateBuffer(const GLenum format, const GLsizei width, const GLsizei height);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void CreateMultiSampleBuffer(const GLenum format, const GLsizei width, 
			const GLsizei height, const ML_INT smaples);

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Bind();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Unbind();

		//=========================================================
		// Name: 
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL GLuint GetBufferID() const { return m_bufferID; }

	private:

		GLuint m_bufferID;
		GLenum m_format;

		ML_BOOL m_bound;
	};

}
#endif