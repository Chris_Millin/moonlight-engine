#include "MLShader.h"

using namespace MLRender;namespace MLRender
{

	MLCoreDLL MLShader::MLShader(void)
	{
		//set the program and shaders to 0 to initalise
		m_program = 0;
		m_shaders[MLS_VERT]=0;
		m_shaders[MLS_FRAG]=0;
		m_shaders[MLS_GEOM]=0;
	}


	MLShader::~MLShader(void)
	{
		Release();
	}

	
	MLCoreDLL ML_BOOL MLShader::Release()
	{
		//if a shader exists then delete it and detach it from the program
		//(Detach before delete)
		if(m_shaders[1])
		{
			glDetachShader(m_program, m_shaders[1]);
			glDeleteShader(m_shaders[1]);
		}
		if(m_shaders[2])
		{
			glDetachShader(m_program, m_shaders[2]);
			glDeleteShader(m_shaders[2]);
		}
		if(m_shaders[3])
		{
			glDetachShader(m_program, m_shaders[3]);
			glDeleteShader(m_shaders[3]);
		}

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::LoadShaderFile(MLKernel::MLVirtualFile* File, GLenum type)
	{	
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Loading Shader File %s\n"), File->FullFilePath());
		
		File->OpenFile(MLKernel::MLFileAccess::ReadOnly, ML_TRUE);

		//if the file was opened ok then data can be read
		if(File->IsOpen()) 
		{
			ML_INT fileSize = File->GetFileSize();

			//if the file size is 0 then there is something wrong
			if (fileSize <= 0)
			{
				ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Error getting shader file size\n"));
				return ML_FALSE;
			}

			//create a character array to store the shader with an extra character for the escape character
			ML_CHAR* sSource = (ML_CHAR*)malloc((fileSize/sizeof(ML_CHAR)) + 1);

			sSource = (ML_CHAR*)File->ReadData(sizeof(ML_CHAR), fileSize);

			//set the last character to terminator
			sSource[fileSize] = '\0';

			//send the shader code to be compiled
			return CompileShader(sSource, type);
		}
		else
		{
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Could not load file\n"));
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Loaded Shader File\n"));
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::LoadShaderFile(ML_STRING& File, const GLenum type)
	{	
		ML_STRING fileLoc = GetHighestVersion(File, type);
		if(fileLoc.empty())
			return ML_FALSE;
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Loading Shader File %s\n"), MLtoUnicode(fileLoc).c_str());
		//create a file to read in the data from
		FILE* inStream;
		//open a read binary stream
		ML_BOOL error = fopen_s(&inStream, MLtoAscii(fileLoc).c_str(), "rb");
		//if the file was opened ok then data can be read
		if (!error)
		{
			ML_INT fileSize = 0;
			//seek to the end of the file
			fseek(inStream, 0, SEEK_END);
			//get the amount of bytes the seek has progressed
			fileSize=ftell(inStream);
			//seek back to the start
			fseek(inStream, 0, SEEK_SET);
		
			//if thje file size is 0 then there is something wrong
			if (fileSize <= 0)
			{
				ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Error getting shader file size\n"));
				return ML_FALSE;
			}

			//create a character array to store the shader with an extra character for the escape character
			ML_CHAR* sSource = (ML_CHAR*)malloc((fileSize/sizeof(ML_CHAR)) + 1);

			//read the data in the file
			fread(sSource, sizeof(ML_CHAR), fileSize, inStream);
		
			//set the last character to terminator
			sSource[fileSize] = '\0';

			//send the shader code to be compiled
			return CompileShader(sSource, type);
		}
		else
		{
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Could not load file\n"));
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Loaded Shader File\n"));
		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::LoadShaderString(const ML_CHAR* Source, const GLenum type)
	{
		ML_BOOL result;
		//if the shader has already been converted to a string it is just passed onto the
		//compile stage
		result = CompileShader(Source, type);
		return result;
	}

	MLCoreDLL ML_BOOL MLShader::CompileShader(const ML_CHAR* sSource, const GLenum type)
	{
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Compiling Shader Type: %i\n"), (ML_UINT)type);
	
		//creates a shader of the specified type
		GLuint shader = glCreateShader(type);

		//check to see if there was an error
		if (!shader)
		{
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Error Could not create Shader\n"));
			return ML_FALSE;
		}

		//send the shader source to openGL
		glShaderSource(shader, 1, &sSource, NULL);
		//compile the shader
		glCompileShader(shader);
		GLint result;
		//get the shader compile result
		glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
		//check to see if there were any errors and print them out if there are
		if (result == GL_FALSE)
		{
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Error Could not compile Shader\n"));
			GLint LogLen;
			glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &LogLen );
			if( LogLen > 0 )
			{
				ML_CHAR* log = (ML_CHAR* )malloc(LogLen);
				GLsizei written;
				glGetShaderInfoLog(shader, LogLen, &written, log);
				ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Shader Log: \n %s\n"), MLtoUnicode(log).c_str());
				free(log);
				return ML_FALSE;
			}
			return ML_FALSE;
		}
	
		//set the shader to the correct shader location based on type
		switch(type)
		{
			case GL_FRAGMENT_SHADER:
				m_shaders[MLS_FRAG] = shader;
				break;
			case GL_VERTEX_SHADER:
				m_shaders[MLS_VERT] = shader;
				break;
			case GL_GEOMETRY_SHADER:
				m_shaders[MLS_GEOM] = shader;
				break;
		}
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Shader Compiled\n"));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::LinkShaders()
	{
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Linking Shader\n"));

		//depending on which shaders have been compiled they are attached to
		//the program
		if (m_shaders[MLS_FRAG] != 0)
		{
			glAttachShader (m_program, m_shaders[MLS_FRAG]);
		}
		if (m_shaders[MLS_VERT] != 0)
		{
			glAttachShader (m_program, m_shaders[MLS_VERT]);
		}
		if (m_shaders[MLS_GEOM] != 0)
		{
			glAttachShader (m_program, m_shaders[MLS_GEOM]);
		}

		//the program is linked up
		glLinkProgram(m_program);
	
		//enables gl_pointsize in the shader i believe
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	
		//Print any errors that occured
		GLint status;
		glGetProgramiv( m_program, GL_LINK_STATUS, &status );
		if( GL_FALSE == status ) {
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Failed to link shader\n"));
			GLint logLen;
			glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logLen);
			if( logLen > 0 )
			{
				ML_CHAR * log = (ML_CHAR *)malloc(logLen);
				GLsizei written;
				glGetProgramInfoLog(m_program, logLen, &written, log);
				ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Error, ML_STR("Program Log: \n %s\n"), MLtoUnicode(log).c_str());
				free(log);
				return ML_FALSE;
			}
			return ML_FALSE;
		}

		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Progress, ML_STR("Shaders Linked\n"));
		//PrintActiveAtts();
		//PrintActiveUniform();

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::BindAtt(const MLAttID& type, ML_CHAR* Name)
	{
		//attributes can be bound by using the prefixed locations
		glBindAttribLocation(m_program, (GLuint)type, Name);
		m_attributes[Name] = type;

		return ML_TRUE;
	};

	MLCoreDLL ML_BOOL MLShader::AddAtt(ML_CHAR* Name)
	{
		//if the attribute cant be added before linking it can be specified after
		//this requires the use of layout = x in the shader
		m_attributes[Name] = glGetAttribLocation(m_program, Name);

		return ML_TRUE;
	}


	// MLCoreDLL bool MLShader::addUniform(char* Name)
	// {
	// 	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	// }

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, ML_BOOL inVal)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform1i(m_uniforms[Name], inVal);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, ML_INT inVal)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform1i(m_uniforms[Name], inVal);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, ML_UINT inVal)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform1ui(m_uniforms[Name], inVal);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, ML_FLOAT inVal)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform1f(m_uniforms[Name], inVal);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, const MLVector2f& inVec)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform2fv(m_uniforms[Name],1, (GLfloat*)MLVector2f::toptr(inVec));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, const MLVector3f& inVec)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform3fv(m_uniforms[Name],1, (GLfloat*)MLVector3f::toptr(inVec));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, const MLVector4f& inVec)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniform4fv(m_uniforms[Name],1, (GLfloat*)MLVector4f::toptr(inVec));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::AddUniform(const ML_CHAR* Name, const MLMatrix4f& inMat)
	{
		m_uniforms[Name] = glGetUniformLocation(m_program, Name);
		glUniformMatrix4fv(m_uniforms[Name],1, GL_FALSE, (GLfloat*)toptr(inMat));

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::Lock(const ML_BOOL bind)
	{
		//if there is no program it is created otherwise
		//the program is used
		if (m_program == 0)
		{
			m_program = glCreateProgram();
			return ML_TRUE;
		}

		glUseProgram(m_program);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::PrintActiveAtts()
	{
		GLint maxLength, nAttribs;
		glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTES, &nAttribs);				//get the count of attributes
		glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLength);	//get the string length of the largest active attribute

		GLchar * name = (GLchar *) malloc( maxLength ); //create a string to hold the largest attribute

		GLint written, size, location;
		GLenum type;
	
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("ACTIVE ATTRIBUTES\n"));
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("------------------------------------------------\n"));
		for( ML_INT i = 0; i < nAttribs; i++ ) 
		{
			glGetActiveAttrib( m_program, i, maxLength, &written, &size, &type, name );
			location = glGetAttribLocation(m_program, name);
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("Name %s\n"), MLtoUnicode(name).c_str());
			sprintf_s(name, strlen(name), "%d", location);
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("Location %s\n"), MLtoUnicode(name).c_str());
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("------------------------------------------------\n"));
		}
		free(name);

		return ML_TRUE;
	}

	MLCoreDLL ML_BOOL MLShader::PrintActiveUniform()
	{
		GLint maxLength, nAttribs;
		glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &nAttribs);				//get the count of attributes
		glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLength);	//get the string length of the largest active attribute

		GLchar * name = (GLchar *) malloc( maxLength ); //create a string to hold the largest attribute

		GLint written, size, location;
		GLenum type;
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("ACTIVE UNIFORMS\n"));
		ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("------------------------------------------------\n"));
		for( ML_INT i = 0; i < nAttribs; i++ ) 
		{
			glGetActiveUniform( m_program, i, maxLength, &written, &size, &type, name );
			location = glGetUniformLocation(m_program, name);
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("Name %s\n"), MLtoUnicode(name).c_str());
			sprintf_s(name, strlen(name), "%d", location);
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR("Location %s\n"), MLtoUnicode(name).c_str());
			ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Information, ML_STR( "------------------------------------------------\n"));
		}
		free(name);

		return ML_TRUE;
	}

	ML_STRING MLShader::GetHighestVersion(ML_STRING& location, const GLenum type)
	{
		ML_INT fileLength = location.size();
		
		for(ML_UINT i = ML_GL_VERSIONS_AVAILABLE - 1; i >= 0 && i < ML_GL_VERSIONS_AVAILABLE; i--)
		{
			location.insert(fileLength, MLRender::versionList[i], _tcslen(MLRender::versionList[i]));

			ML_INT versionFileLength = location.size();

			switch(type)
			{
			case GL_FRAGMENT_SHADER:
				{
				location.insert(versionFileLength, ML_STR(".frag\0"), 5);
					break;
				}
			case GL_GEOMETRY_SHADER:
				{
					GLint versMaj;
					glGetIntegerv(GL_MAJOR_VERSION, &versMaj);
					if (versMaj > 3)
					{
						location.insert(versionFileLength, ML_STR(".geom\0"), 5);
					}
					else
					{
						ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Warning, 
							ML_STR("ERROR loading a geometry shader on a version < 3\n"));
						ML_TRACE(ML_LOG_SHADER, MLKernel::MLConsoleStatus::Warning, 
							ML_STR("Shader %s.geom will not be loaded\n"), location.c_str());

						return ML_EMPTY_STRING;
					}
					break;
				}
			case GL_VERTEX_SHADER:
				{
				location.insert(versionFileLength, ML_STR(".vert\0"), 5);
					break;
				}
			}

			if (fileExists(location))
			{
				return location;
			}
			else
			{
				location.resize(fileLength);
			}
		
		}

		return ML_EMPTY_STRING;
	}
}