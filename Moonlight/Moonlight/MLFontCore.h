#ifndef MLFONTCORE_H
#define MLFONTCORE_H

#include "MLIncludes.h"

#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>

#define ML_LOG_FONT ML_STR("Font")

namespace MLRender
{
	//=========================================================
	// Name: Font Cache
	// Description: This structure is used to describe information
	// about a single character in the font cache. 
	// Creator: Chris Millin
	//=========================================================
	struct MLGlyph
	{
		ML_UINT character;		//The character
		ML_FLOAT X;				//The X position in the texture atlas
		ML_FLOAT Y;				//The Y position in the texture atlas 
		ML_FLOAT Width;			//The width of the character
		ML_FLOAT Height;		//The height of the character
		ML_FLOAT paddingX;		//Padding between characters X
		ML_FLOAT paddingY;		//Padding between characters Y
		ML_FLOAT paddingZ;		//Padding between characters X
		ML_FLOAT paddingW;		//Padding between characters Y
		ML_FLOAT bearingX;		//The bearing in the X
		ML_FLOAT bearingY;		//The bearing in the Y
		ML_FLOAT advance;		//The advancement of the character
		ML_INT textureID;		//Used for when multi textures are required
		ML_BOOL Rotated;		//Used for if the character data is flipped in the texture

		MLGlyph()
		{
			character = 0;
			X		 = 0.0f;
			Y		 = 0.0f;
			Width	 = 0.0f;
			Height	 = 0.0f;
			paddingX = 0.0f;
			paddingY = 0.0f;
			paddingZ = 0.0f;
			paddingW = 0.0f;
			bearingX = 0.0f;
			bearingY = 0.0f;
			advance  = 0.0f;
			textureID = -1;
			Rotated = ML_FALSE;
		}
	};

	//=========================================================
	// Name: MLFontCore
	// Namespace: MLRenderer, MLText
	// Description: This is just a manager for the freetype
	// library to ensure it is initalised once and the library
	// can be obtained by any font
	// Creator: Chris Millin
	// Librarys: FreeType
	//=========================================================
	class MLFontCore
	{
	public:
			
		//=========================================================
		// Name: Initalise
		// Description: Initalises the Freetype library
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static ML_BOOL Initalise(void);

		//=========================================================
		// Name: Get Font Library
		// Description: returns the font library
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL static FT_Library GetFontLibrary(void) { return m_library; }
		
	private:

		MLCoreDLL MLFontCore(void);
		MLCoreDLL virtual ~MLFontCore(void);

	private:

		MLCoreDLL static FT_Library m_library;
	};
}

#endif

