#ifndef MLPOLYLIST_H
#define MLPOLYLIST_H

#include "MLIncludes.h"

using namespace MLMath::MLVec2;

namespace MLRender
{
	//=========================================================
	// Name: Poly Node
	// Description: Poly node object which is a single
	// object in the linked list.
	// Creator: Chris Millin
	//=========================================================
	struct MLPolyNode
	{
		MLCoreDLL MLPolyNode(const ML_FLOAT x, const ML_FLOAT y) : next(NULL), previous(NULL)
		{
			point.x = x;
			point.y = y;
		}
		MLCoreDLL MLPolyNode(const MLVector2f XY) : point(XY), next(NULL), previous(NULL) {}
	
		MLVector2f point;
		MLPolyNode* next;
		MLPolyNode* previous;

		MLCoreDLL ML_BOOL operator==(const MLPolyNode& rhs){ return ( this->point.x == rhs.point.x ) && ( this->point.y == rhs.point.y ); }
		MLCoreDLL ML_BOOL operator!=(const MLPolyNode& rhs){ return !(( this->point.x == rhs.point.x ) && ( this->point.y == rhs.point.y )); }
	};

	//=========================================================
	//Name: MLPolyList
	//Description: This is a class to assist with triangulation
	// This acts as a double linked list for the points in the
	// polygon and also acts as a parent to child relationship for
	// a polygon with holes.
	// Creator: Chris Millin
	//=========================================================
	class MLPolyList
	{
	public:
		MLCoreDLL MLPolyList(void);
		MLCoreDLL MLPolyList(MLPolyList* inParent);
		MLCoreDLL MLPolyList(MLPolyList* inParent, const MLVector2f& point);
		MLCoreDLL MLPolyList(MLPolyList* inParent, const MLVector2f& pointA, const MLVector2f& pointB);
		MLCoreDLL MLPolyList(MLPolyList* inParent, const MLVector2f& pointA, const MLVector2f& pointB, const MLVector2f& pointC);
		MLCoreDLL ~MLPolyList(void);

		//=========================================================
		// Name: Clear 
		// Description: Will clear the linked list 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL Clear();

		//=========================================================
		// Name: Add Child
		// Description: Will add a child to the Poly list for
		// holes (holes not implemented
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL AddChild(MLPolyList* child);

		//=========================================================
		// Name: Add Point
		// Description: Will add a new point to the poly linked list
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL AddPoint(const MLVector2f& point);

		//=========================================================
		// Name: Add Point
		// Description: Will add a new point to the poly linked list
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL AddPoint(const ML_FLOAT x, const ML_FLOAT y);

		//=========================================================
		// Name: Remove Point
		// Description: Will remove a point from the poly linked
		// list.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RemovePoint(const ML_FLOAT x, const ML_FLOAT y);

		//=========================================================
		// Name: Remove Point
		// Description: Will remove a point from the poly linked
		// list.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RemovePoint(const MLVector2f& point);

		//=========================================================
		// Name: Remove Point
		// Description: Will remove a point from the poly linked
		// list.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RemovePoint(const MLPolyNode& Node);

		//=========================================================
		// Name: Remove Last
		// Description: Will remove the element at the end of the
		// list
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RemoveLast();

		//=========================================================
		// Name: Start
		// Description: Will return the start of the linked list 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLPolyNode* Start() const { return first; }

		//=========================================================
		// Name: Get Point
		// Description: Will return a point if it exists based
		// on the x, y
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLPolyNode* GetPoint(const ML_FLOAT x, const ML_FLOAT y) const ;

		//=========================================================
		// Name: Get Point
		// Description: Will return a point if it exists based
		// on the x, y
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLPolyNode* GetPoint(const MLVector2f& point) const;

		//=========================================================
		// Name: Get Point
		// Description: Will return a point if it exists based
		// on the x, y
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLPolyList* GetChild(ML_UINT pos) const;

		//=========================================================
		// Name: Get Children
		// Description: Returns a reference to the children vector
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL std::vector<MLPolyList*>& GetChildren() { return children; }

		//=========================================================
		// Name: Remove Child
		// Description: Will remove a child from the children
		// vector.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL RemoveChild(ML_UINT pos);
		
		//=========================================================
		// Name: Size
		// Description: Return the amount of elements in the list
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT Size() const { return pointCount; }

		//=========================================================
		// Name: Child Size
		// Description: Returns the size of the child vector
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_UINT ChildSize() const { return children.size(); }

		//=========================================================
		// Name: Insert Point
		// Description: Insert a point after the specified node
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL InsertPoint(const ML_FLOAT x, const ML_FLOAT y, const MLPolyNode* pointNode);

		//=========================================================
		// Name: Insert Point
		// Description: 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL ML_BOOL InsertPoint(const MLVector2f& point, const MLPolyNode* pointNode);

		//=========================================================
		// Name: Reverse
		// Description: Reverse the linked list
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL void Reverse();

	private:

		MLPolyNode* first;
		MLPolyList* parent;
		ML_UINT pointCount;

		std::vector<MLPolyList*> children;
	
	};
}

#endif