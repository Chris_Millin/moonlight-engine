#ifndef MLFXAA_H
#define MLFXAA_H

#include "MLIncludes.h"
#include "MLPostProcess.h"
#include "MLGraphics.h"
#include "MLShaderManager.h"

#define ML_FXAA_SHADER ML_STRING(ML_STR("shaders/fxaa"))
#define ML_LOG_FXAA ML_STR("FXAA")
#define ML_FXAA_SHADER_VERT_POS ML_STRA("VertexPosition")
#define ML_FXAA_SHADER_VERT_COL ML_STRA("VertexColour")
#define ML_FXAA_SHADER_VERT_UV	ML_STRA("TextureCoord")

namespace MLRender
{

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	enum MLFXAAType
	{
		FXAAHight = 2,
		FXAAMedium = 1,
		FXAALow = 0
	};

	//=========================================================
	// Name: 
	// Description: 
	// Creator: Chris Millin
	//=========================================================
	class MLFXAA : MLRenderTarget
	{
	public:
		MLCoreDLL MLFXAA(MLFXAAType type);
		MLCoreDLL virtual ~MLFXAA(void);

		//=========================================================
		// Name: Release
		// Description: Releases any dynamic objects created
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Initalise
		// Description: Will initalise all the frame buffer objects
		// ready for rendering the scene to a multi sampled texture
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Initalise(const MLFBOTypes enableFBO = MLFBOTypes::ColourBuffer);

		//=========================================================
		// Name: Draw
		// Description: This does not have to be called but can be
		// used to draw the MSAA result. If post processing is
		// desired call bind on the post process before calling
		// this and then unbind after this call and then draw the
		// post process. 
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Draw(MLGraphics* batch);

	private:

		MLShader* m_fxaaShader;

		MLFXAAType m_type;
	};
}
#endif

