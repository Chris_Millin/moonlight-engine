#ifndef MLVIRTUALPATH_H
#define MLVIRTUALPATH_H

#include "MLVirtualDrive.h"
#include "MLCore.h"
#include "MLConsole.h"

#include <shobjidl.h>
#include <shlwapi.h>

namespace MLKernel
{
	class MLVirtualFile;

	//=========================================================
	// Name: Virtual Path
	// Description: A location to a folder on the system.
	// It can also be a single folder for use with another 
	// virtual path for example
	// MLVirtual* newPath = new MLVirtualPath();
	// newPath->SetPath(ML_UNICODE("newFolder");
	// builtPath->CreatePath(newPath);
	//
	// The above will add a folder called newFolder
	// to the full path of whatever is contained in builtPath.
	//
	// If no drive is set for the path then the default
	// application path is used.
	// Creator: Chris Millin
	//=========================================================
	class MLVirtualPath
	{
	public:

		//=========================================================
		// Name: Default Constructor
		// Description: sets a empty path and empty drive
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualPath(void);

		//=========================================================
		// Name: String Path Constructor
		// Description: Creates a virtual path with the defined
		// path and an empty drive
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualPath(const ML_STRING& Path);

		//=========================================================
		// Name: Virtual Path Constructor
		// Description: Creates a virtual path with the defined
		// path in the virtual path and an empty drive
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualPath(MLVirtualPath* Path);

		//=========================================================
		// Name: Drive and String Path Constructor
		// Description: Creates a virtual path with the defined 
		// drive and the defined string path
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualPath(MLVirtualDrive* Drive, const ML_STRING& Path);

		//=========================================================
		// Name: Drive and String Path Constructor
		// Description: Creates a virtual path with the defined 
		// drive and the defined virtual path
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL MLVirtualPath(MLVirtualDrive* Drive, MLVirtualPath* Path);

		MLCoreDLL virtual ~MLVirtualPath(void);

		//=========================================================
		// Name: Release
		// Description: will release and NULL the drive and will
		// clear the path string
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void Release();

		//=========================================================
		// Name: Create Path String
		// Description: This will create the sub folder if it is
		// only one level deep at the moment. Will work on getting it
		// to make the full path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual void CreatePath();

		//=========================================================
		// Name: Create Path String
		// Description: This will create a sub folder in the current
		// path location and return the new path object which contains
		// access to the full path location. 
		// Note: This wont be able to make multiple sub folders
		// yet so if Name is "subfolder/subSubfolder" it wont create
		// two folders one inside the other and will fail as / is
		// an invalid folder character.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLVirtualPath* CreatePath(const ML_STRING& Name);

		//=========================================================
		// Name: Create Path Virtual Path
		// Description: This will create a sub folder in the current
		// path location and return the new path object which contains
		// access to the full path location.
		// Note: This wont be able to make multiple sub folders
		// yet so if Name is "subfolder/subSubfolder" it wont create
		// two folders one inside the other and will fail as / is
		// an invalid folder character.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLVirtualPath* CreatePath(MLVirtualPath* Name);

		//=========================================================
		// Name: Delete Path (this)
		// Description: will delete the current path.
		// Note: Any action made after this call will be invalid
		// as the path wont exist any more. The path should be
		// released and destroyed once this function is called
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL DeletePath();

		//=========================================================
		// Name: Delete Sub Folder String
		// Description: Will delete one of the sub folders in the
		// current path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL DeletePath(const ML_STRING& Path);

		//=========================================================
		// Name: Delete Sub Folder Virtual Path
		// Description: Will delete one of the sub folders in the
		// current path denoted by a virtual Path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL DeletePath(MLVirtualPath* Path);

		//=========================================================
		// Name: Set Path
		// Description: will set the address of the current path
		// without the drive letter (no C:\ needed) but the path
		// will require a drive to the C:\ drive.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetPath(const ML_STRING& path) { m_pathAddress = path; return ML_TRUE; }

		//=========================================================
		// Name: Set Drive
		// Description: Will set the current drive the path should
		// exist on.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL SetDrive(MLVirtualDrive* drive) { m_drivePath = drive; return ML_TRUE; }

		//=========================================================
		// Name: Enumerate Folders
		// Description: Returns a string list of all the sub folders
		// located in the current path. (0 if the path is invalid
		// or has no sub folders)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<ML_STRING> EnumerateFolders() const;

		//=========================================================
		// Name: Enumerate Files
		// Description: Returns a string list of all the files
		// located ion the current path. (0 if the path is invalid
		// or has no files)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<ML_STRING> EnumerateFiles() const;

		//=========================================================
		// Name: Enumerate Files With Extention
		// Description: Returns a string list of all the files
		// located ion the current path with a defined file
		// extension (e.g. ".txt"). (0 if the path is invalid
		// or has no files)
		// Note: Valid Styles - *.txt, .txt and, txt
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<ML_STRING> EnumerateFiles(const ML_STRING& Extention) const;

		//=========================================================
		// Name: Get Folders
		// Description: Will get all the sub folders as virtual 
		// paths.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<MLVirtualPath*> GetFolders() const;

		//=========================================================
		// Name: Get Files
		// Description: Will get all the files as virtual files.
		// The files will not be open through this call.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual std::vector<MLVirtualFile*> GetFiles() const;

		//=========================================================
		// Name: Get Folder
		// Description: Will obtain a single sub folder as a 
		// virtual path based on its folder name if valid and 
		// and exists.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLVirtualPath* GetFolder(const ML_STRING& Name) const;

		//=========================================================
		// Name: Get File
		// Description: Will obtain a single file as a virtual
		// file based on its file name if valid and exists 
		// file extension will be required!
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLVirtualFile* GetFile(const ML_STRING& Name) const;

		//=========================================================
		// Name: Get Drive
		// Description: Returns the current drive (NULL if default
		// drive is used but not set)
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual MLVirtualDrive* GetDrive() const { return m_drivePath; }

		//=========================================================
		// Name: Get Path
		// Description: Returns the current path without the
		// drive letter.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetPath() const		{ return m_pathAddress; }

		//=========================================================
		// Name: Get Full Path 
		// Description: Returns the full path address with the drive
		// letter.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetFullPath() const { return BuildFullPathAddress(); } 

		//=========================================================
		// Name: Is Valid
		// Description: Returns true is the current path exists
		// on the file system.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_BOOL IsValid() const; 

		//=========================================================
		// Name: To String
		// Description: Returns the Virtual Path as its full
		// string
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING ToString() const { return BuildFullPathAddress(); }

	protected:

		//=========================================================
		// Name: Get Default Drive Path
		// Description: Returns the path for the default drive
		// location
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING GetDefaultDrivePath() const;

		//=========================================================
		// Name: Build Full Path Address
		// Description: Builds the full path as a string based
		// on the drive and the defined path.
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING BuildFullPathAddress() const;

		//=========================================================
		// Name: Build New Full Path Address
		// Description: Builds the full path (without drive letter)
		// of sub folders in the current folder
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual ML_STRING BuildNewFullPathAddress(const ML_STRING& path) const;

		//for windows
		//=========================================================
		// Name: Create ShellItem
		// Description: Creates a windows folder shellitem used
		// for defining a folder
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual IShellItem* CreateShellItem(const ML_STRING& path);

		//for windows
		//=========================================================
		// Name: Create File Operation
		// Description: Will create a windows file operation for use
		// with the shellitems to delete and create folders
		// Creator: Chris Millin
		//=========================================================
		MLCoreDLL virtual IFileOperation* CreateOperation();

	protected:

		//=========================================================
		// Name: Path Address 
		// Description: Path of the virtual folder
		//=========================================================
		ML_STRING m_pathAddress;

		//=========================================================
		// Name: Drive Path
		// Description: The Drive path that will be used for the
		// path address (NULL value will result in the uses of 
		// the default path)
		//=========================================================
		MLVirtualDrive* m_drivePath;
	};
}

#endif