#include <Box2D/Box2D.h>


class b2BodyAngel : public b2Body
{
	public:
		b2BodyAngel();
		~b2BodyAngel();

		int AddScriptRef();
		int RemoveScriptRef();

		int refCount;
};

inline int b2BodyAngel::AddScriptRef()
{
	return ++refCount;
}

inline int b2BodyAngel::RemoveScriptRef()
{
	if( --refCount == 0 )
	{
		delete this;
		return 0;
	}
	return refCount;
}