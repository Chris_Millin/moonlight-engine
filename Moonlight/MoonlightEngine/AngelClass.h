
//Angel Script header
//========================
#include <angelscript.h>
//========================

#include <assert.h>
#pragma once


class AngelClass
{
public:
	AngelClass();
	~AngelClass(void);

	static void setEngine(asIScriptEngine* engine) { m_engine = engine; }

	template <class T>
	static bool registerClass(const char* className);
	static bool registerClass(const char* className, int byteSize, int objFlag);
	static bool registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer);
	static bool registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer, int flag);
	static bool registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer);
	static bool registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer, int flag);
	static bool registerClassProperty(const char* className, const char* propDef, int offset);

	static bool registerInterface(const char* interfaceName);
	static bool registerInterfaceMethod(const char* interfaceName, const char* functionDef);

private:

	static asIScriptEngine* m_engine;
};

//property 