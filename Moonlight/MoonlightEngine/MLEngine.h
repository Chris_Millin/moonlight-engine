
//MoonLight DLL Headers
//==============================
#include <MLRenderHeaders.h>
#include <MLInputManager.h>
#include <MLAudioHeaders.h>
//==============================

//box2D
//==============================
#include "MLb2Draw.h"
#include "MLb2Manager.h"
//==============================

//Angel Script
//==============================
#include "AngelManager.h"
#include "MLModule.h"
#include "MLAngelRegister.h"
//==============================

//Engine Headers
//==============================
#include "MLStateManager.h"
//==============================

#include "DemoLoader.h"
#include "DemoState.h"
#include "MLEngineHeader.h"

#pragma once

namespace MLEngine
{
	class MLEngineCore
	{
	public:
		MLEngineCore(void);
		~MLEngineCore(void);

		//release the engine
		void release();

		//basic initalise function which also takes a config file which will allow for the engine to be customized on launch
		void InitaliseEngine(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd, wchar_t configFile);
		//update function
		void updateEngine();
		//reload the engine if needed
		void reloadEngine();

	private:

		bool loadGame(const char* file);

	private:
		int textureID1;

		MLWindows::windowsHandler*		m_windowsHandler;
		MLEngine::MLStateManager*		m_stateManager;

		MLEngine::MLBox2D::MLb2Draw* m_b2DebugDraw;
		MLRenderer::MLSpriteBatch* m_b2DebugSpriteBatch;
	};
}

