#include "MLEngine.h"

INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
	MLEngine::MLEngineCore* MoonLight = new MLEngine::MLEngineCore();

	MoonLight->InitaliseEngine(hInstance, hPrevInstance, lpCmdLine, nShowCmd, NULL);
	
	MoonLight->updateEngine();

	MoonLight->release();
}