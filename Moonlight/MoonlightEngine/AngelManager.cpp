#include "AngelManager.h"

using namespace MLEngine;

asIScriptEngine* AngelManager::m_engine;

void MessageCallback(const asSMessageInfo* msg, void* param)
{
	const char *type = "ERR ";
	if( msg->type == asMSGTYPE_WARNING ) 
		type = "WARN";
	else if( msg->type == asMSGTYPE_INFORMATION ) 
		type = "INFO";

	printf("%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);
}

void print(std::string& str)
{
	printf("%s", str.c_str());
}

AngelManager::AngelManager(void)
{
}


AngelManager::~AngelManager(void)
{
}

bool AngelManager::Initalise()
{
	//create the script engine
	m_engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
	if (m_engine == NULL)
		return false;
	//set the error call back function
	m_engine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);

	//call the helper addon functions to register string and the handler
	RegisterStdString(m_engine);
	RegisterScriptHandle(m_engine);

	//register the print function
	int r;
	r = m_engine->RegisterGlobalFunction("void Print(string &in)", asFUNCTION(print), asCALL_CDECL);
	assert( r >= 0 );
	if (r < 0)
		return false;

	//set the engine for the angelclass class
	AngelClass::setEngine(m_engine);
}

AngelModule* AngelManager::createNewModule(const char* moduleName)
{
	AngelModule* mod = new AngelModule();
	mod->createModule(m_engine, moduleName);
	return mod;
}

template<class class_type>
bool AngelManager::registerClass(const char* className)
{
	return AngelClass::registerClass<class_type>(className);
}

bool AngelManager::registerClass(const char* className, int byteSize, int objFlag)
{
	return AngelClass::registerClass(className, byteSize, objFlag);
}

bool AngelManager::registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer)
{
	return AngelClass::registerClassBehaviour(className, functionDef, behaviour, funcPointer);
}

bool AngelManager::registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer, int flag)
{
	return AngelClass::registerClassBehaviour(className, functionDef, behaviour, funcPointer, flag);
}

bool AngelManager::registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer)
{
	return AngelClass::registerClassMethod(className, functionDef, funcPointer);
}

bool AngelManager::registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer, int flag)
{
	return AngelClass::registerClassMethod(className, functionDef, funcPointer, flag);
}

bool AngelManager::registerClassProperty(const char* className, const char* propDef, int offset)
{
	return AngelClass::registerClassProperty(className, propDef, offset);
}

bool AngelManager::registerInterface(const char* interfaceName)
{
	return AngelClass::registerInterface(interfaceName);
}

bool AngelManager::registerInterfaceMethod(const char* interfaceName, const char* functionDef)
{
	return AngelClass::registerInterfaceMethod(interfaceName, functionDef);
}

bool AngelManager::registerGlobalFunction(const char* functionDef, asSFuncPtr function, asDWORD flag)
{
	int r;
	r = m_engine->RegisterGlobalFunction(functionDef, function, flag);
	assert( r >= 0 );
	if(r < 0)
		return false;
}

bool AngelManager::registerGlobalProperty(const char* propDef, void* prop)
{
	int r;
	r = m_engine->RegisterGlobalProperty(propDef, prop);
	assert( r >= 0 );
	if (r < 0)
		return false;
}

bool AngelManager::registerEnum(const char* enumName)
{
	int r;
	r = m_engine->RegisterEnum(enumName);
	assert( r >= 0 );
	if(r < 0)
		return false;
}

bool AngelManager::registerEnumValue(const char* enumName, const char* valueName, int value)
{
	int r;
	r = m_engine->RegisterEnumValue(enumName, valueName, value);
	assert( r >= 0 );
	if(r < 0)
		return false;
}

