
//Angel Script header
//===========================
#include <angelscript.h>
//===========================

//Angel Script Add-On header
//===========================
#include <scriptstdstring/scriptstdstring.h>
#include <scripthandle/scripthandle.h>
//===========================

//MoonLight Angel Script Helper header
//===========================
#include "AngelModule.h"
#include "AngelClass.h"
//===========================

//Assert header
//===========================
#include <assert.h>
//===========================
#pragma once


void MessageCallback(const asSMessageInfo* msg, void* param);
void print(std::string& str);

namespace MLEngine
{
	class AngelManager
	{
	public:
		AngelManager(void);
		~AngelManager(void);

		static bool Initalise();

		static AngelModule* createNewModule(const char* moduleName);

		static bool registerGlobalFunction(const char* functionDef, asSFuncPtr function, asDWORD flag);
		static bool registerGlobalProperty(const char* propDef, void* prop);

		template <class class_type>
		static bool registerClass(const char* className);

		static bool registerClass(const char* className, int byteSize, int objFlag);
		static bool registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer);
		static bool registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer, int flag);
		static bool registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer);
		static bool registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer, int flag);
		static bool registerClassProperty(const char* className, const char* propDef, int offset);
		static bool registerEnum(const char* enumName);
		static bool registerEnumValue(const char* enumName, const char* valueName, int value);

		static bool registerInterface(const char* interfaceName);
		static bool registerInterfaceMethod(const char* interfaceName, const char* functionDef);

	private:

		static asIScriptEngine* m_engine;

	};
}
