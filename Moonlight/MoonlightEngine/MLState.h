//Angel Script header
//===========================
#include <angelscript.h>
//===========================

//MoonLight DLL header
//==============================
#include <MLRenderHeaders.h>
//==============================

//Engine header
//==============================
#include "MLThread.h"
//==============================

//Angel Script Helper header
//==============================
#include "AngelModule.h"
//==============================

#include "MLEngineHeader.h"

#pragma once


namespace MLEngine
{
	struct MLStateFunctions
	{
		asIScriptFunction* intialise;
		asIScriptFunction* update;
		asIScriptFunction* draw;
		asIScriptFunction* release;
	};

	class MLState : public MLThread
	{
	public:	
		MLState(void);
		virtual ~MLState(void);

		void startThreadedLoad() { MLThread::createThread(); }
		void startThreadedUnloading() { MLThread::createThread(); }
		void obtainScriptFunctions(AngelModule* module);
		virtual void Initalise();
		virtual void Update();
		virtual void Draw();
		virtual void Update(double delta) {};
		virtual void Draw(MLRenderer::MLSpriteBatch* sb) {};
		virtual void Unload();

		MLStateFunctions getFunctions() { return m_functions; }
		asIScriptObject* getObject()	{ return m_scriptObj; }
		void setObject(asIScriptObject* obj) { m_scriptObj = obj; }

		bool loadedAssets() {return m_InitaliseComplete;}
	protected:
		void threadEntry() 
		{
			if(m_InitaliseComplete == false)
				Initalise();
			else
				Unload();
		} 

	protected:
		
		int m_loadedAmount;
		bool m_InitaliseComplete;
		wchar_t* m_scriptLoaction; 
	
	private:

		AngelModule* m_currentModule;
		MLStateFunctions m_functions;
		asIScriptObject* m_scriptObj;
	};
}



