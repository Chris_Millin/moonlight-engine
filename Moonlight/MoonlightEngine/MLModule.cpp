#include "MLModule.h"

using namespace MLEngine;

AngelModule* MLModule::m_currenMod;
char*		 MLModule::m_gameModFile;

MLModule::MLModule(void)
{
}


MLModule::~MLModule(void)
{
}


bool MLModule::changeModule(int modId)
{
	if(m_currenMod != NULL)
	{
	
	}
	else
	{
		
	}

	return intialiseMod(modId);
}

bool MLModule::update()
{
	MLEngine::MLStateManager::updateState();
	m_currenMod->LoadScript("demo.as");
	m_currenMod->buildModule();
	return true;
}

bool MLModule::intialiseMod(int modID)
{
	//TEMP!!!!
	AngelManager::registerGlobalProperty("MLSpriteBatch batch", MLStateManager::getSBatch());
	m_currenMod = MLEngine::AngelManager::createNewModule("TestMod");
	m_currenMod->LoadScript("demo.as");
	m_currenMod->buildModule();
	std::vector<asIObjectType*> states = m_currenMod->getObjectsFromInterface("MLState");
	for(int i = 0; i < states.size(); i++)
	{
		std::string s = std::string(states[i]->GetName()) + "@ " + std::string(states[i]->GetName()) + "()";
		m_currenMod->startFunction(m_currenMod->findObjectFactoryFunction(s.c_str(), states[i]));
		MLEngine::MLState* state = new MLEngine::MLState();
		state->setObject(m_currenMod->runSOFunction());
		state->obtainScriptFunctions(m_currenMod);
		MLStateManager::addState(std::string(states[i]->GetName()), state);
	}

	std::vector<asIObjectType*> loadstates = m_currenMod->getObjectsFromInterface("MLStateLoad");
	if(loadstates.size() != 0)
	{
		std::string s = std::string(loadstates[0]->GetName()) + "@ " + std::string(loadstates[0]->GetName()) + "()";
		m_currenMod->startFunction(m_currenMod->findObjectFactoryFunction(s.c_str(), loadstates[0]));
		MLEngine::MLState* lstate = new MLEngine::MLState();
		lstate->setObject(m_currenMod->runSOFunction());
		lstate->obtainScriptFunctions(m_currenMod);
		MLStateManager::setLoadingState(lstate);
	}
	

	asIScriptFunction* startFunc = m_currenMod->findFunction("void startModule()");
	m_currenMod->startFunction(startFunc);
	m_currenMod->runFunction();
	//TEMP!!!!

	return true;
}

bool MLModule::unloadMod()
{
	return true;
}