#include "MLAngelRegister.h"

using namespace MLEngine;
using namespace MLEngine::MLBox2D;
using namespace MLMath::MLVector2;
using namespace MLMath::MLVector3;
using namespace MLMath::MLVector4;
using namespace MLRenderer;
using namespace MLRenderer::MLText;
using namespace MLInput;
using namespace MLAudio;

void Vec2Construct(void* memory)
{
	new(memory) Vector2f();
}

void Vec2Construct(void* memory, float x)
{
	new(memory) Vector2f(x);
}

void Vec2Construct(void* memory, float x, float y)
{
	new(memory) Vector2f(x, y);
}

void Vec3Construct(void* memory)
{
	new(memory) Vector3f();
}

void Vec3Construct(void* memory, float x)
{
	new(memory) Vector3f(x);
}

void Vec3Construct(void* memory, float x, float y, float z)
{
	new(memory) Vector3f(x, y, z);
}

void Vec4Construct(void* memory)
{
	new(memory) Vector4f();
}

void Vec4Construct(void* memory, float x)
{
	new(memory) Vector4f(x);
}

void Vec4Construct(void* memory, float x, float y, float z, float w)
{
	new(memory) Vector4f(x, y, z, w);
}

void b2Vec2Construnct(void* memory)
{
	new(memory) b2Vec2();
}

void b2Vec2Construnct(void* memory, float x, float y)
{
	new(memory) b2Vec2(x, y);
}

void b2Vec3Construnct(void* memory)
{
	new(memory) b2Vec3();
}

void b2Vec3Construnct(void* memory, float x, float y, float z)
{
	new(memory) b2Vec3(x, y, z);
}

MLFont* MLFontFactory()
{
	return new MLFont();
}

MLParticleEmitter* MLPEmitterFactory()
{
	return new MLParticleEmitter();
}

MLCamera* MLCameraFactory()
{
	return new MLCamera();
}

void b2BodyAdd_Ref(b2Body* obj)
{

}
void b2BodyRemove(b2Body* obj)
{

}

void b2BodyDAdd_Ref(b2BodyDef* obj)
{
	
}
void b2BodyDRemove(b2BodyDef* obj)
{

}

void b2JointAdd_Ref(b2Joint* obj)
{

}
void b2JointRemove(b2Joint* obj)
{

}

void b2JointDAdd_Ref(b2JointDef* obj)
{

}
void b2JointDRemove(b2JointDef* obj)
{

}

void b2RJointDAdd_Ref(b2RevoluteJointDef* obj)
{

}
void b2RJointDRemove(b2RevoluteJointDef* obj)
{

}

void b2FixtureAdd_Ref(b2Fixture* obj)
{

}
void b2FixtureRemove(b2Fixture* obj)
{

}

void b2ShapeAdd_Ref(b2Shape* obj)
{

}
void b2ShapeRemove(b2Shape* obj)
{

}

void b2PShapeAdd_Ref(b2PolygonShape* obj)
{

}
void b2PShapeRemove(b2PolygonShape* obj)
{

}



MLAngelRegister::MLAngelRegister(void)
{
}


MLAngelRegister::~MLAngelRegister(void)
{
}

void MLAngelRegister::setGlobalSpriteBatch(MLRenderer::MLSpriteBatch* sb)
{
	AngelManager::registerGlobalProperty("MLSpriteBatch Batch", sb);
}

void MLAngelRegister::registerEngine()
{
	AngelManager::registerGlobalFunction("double GetDeltaTime()", asFUNCTION(MLWindows::windowsHandler::getDeltaTime), asCALL_CDECL);

	AngelManager::registerInterface("MLState");
	AngelManager::registerInterfaceMethod("MLState", "void Initalise()");
	AngelManager::registerInterfaceMethod("MLState", "void Update()");
	AngelManager::registerInterfaceMethod("MLState", "void Draw()");
	AngelManager::registerInterfaceMethod("MLState", "void Release()");

	AngelManager::registerInterface("MLStateLoad");
	AngelManager::registerInterfaceMethod("MLStateLoad", "void Initalise()");
	AngelManager::registerInterfaceMethod("MLStateLoad", "void Update()");
	AngelManager::registerInterfaceMethod("MLStateLoad", "void Draw()");
	AngelManager::registerInterfaceMethod("MLStateLoad", "void Release()");

	AngelManager::registerGlobalFunction("void MLState_ChangeState(string& in)", asFUNCTIONPR(MLStateManager::changeState, (std::string&), void),  asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLState_ChangeState(string& in, bool showLoading)", asFUNCTIONPR(MLStateManager::changeState, (std::string&, bool), void),  asCALL_CDECL);
}

void MLAngelRegister::registerMaths()
{

	//vector 2
	//=============================================
	AngelManager::registerClass("Vec2", sizeof(MLMath::MLVector2::Vector2f), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassBehaviour("Vec2", "void f()", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec2Construct, (void*), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec2", "void f(float x)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec2Construct, (void*, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec2", "void f(float x, float y)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec2Construct, (void*, float, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassMethod("Vec2", "Vec2& opAssign(Vec2& in)", asMETHODPR(Vector2f, operator=, (const Vector2f&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opAssign(float& in)", asMETHODPR(Vector2f, operator=, (const float&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opAddAssign(Vec2& in)", asMETHODPR(Vector2f, operator+=, (const Vector2f&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opSubAssign(Vec2& in)", asMETHODPR(Vector2f, operator-=, (const Vector2f&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opMulAssign(Vec2& in)", asMETHODPR(Vector2f, operator*=, (const Vector2f&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opMulAssign(float& in)", asMETHODPR(Vector2f, operator*=, (const float&), Vector2f&));
	AngelManager::registerClassMethod("Vec2", "Vec2& opDivAssign(Vec2& in)", asMETHODPR(Vector2f, operator/=, (const Vector2f&), Vector2f&));
	//AngelManager::registerClassMethod("Vec2", "Vec2 opAdd(Vec2& in)", asMETHODPR(Vector2f, operator+, (const Vector2f&), Vector2f));
	AngelManager::registerClassProperty("Vec2", "float x", asOFFSET(Vector2f, x));
	AngelManager::registerClassProperty("Vec2", "float y", asOFFSET(Vector2f, y));
	
	//AngelManager::registerGlobalFunction("Vec2 Vec2_normalise(Vec2& in)", asFUNCTION(MLMath::MLVector2::normalize), asCALL_CDECL);
	//AngelManager::registerGlobalFunction("float Vec2_length(Vec2& in)", asFUNCTION(MLMath::MLVector2::length), asCALL_CDECL);
	//AngelManager::registerGlobalFunction("float Vec2_dot(Vec2& in, Vec2& in2)", asFUNCTION(MLMath::MLVector2::dot), asCALL_CDECL);
	//AngelManager::registerGlobalFunction("float Vec2_angle(Vec2& in, Vec2& in2)", asFUNCTION(MLMath::MLVector2::angle), asCALL_CDECL);
	//=============================================


	//vector 3
	//=============================================
	AngelManager::registerClass("Vec3", sizeof(MLMath::MLVector3::Vector3f), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassBehaviour("Vec3", "void f()", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec3Construct, (void*), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec3", "void f(float x)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec3Construct, (void*, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec3", "void f(float x, float y, float z)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec3Construct, (void*, float, float, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassMethod("Vec3", "Vec3& opAssign(Vec3& in)", asMETHODPR(Vector3f, operator=, (const Vector3f&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opAssign(float& in)", asMETHODPR(Vector3f, operator=, (const float&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opAddAssign(Vec3& in)", asMETHODPR(Vector3f, operator+=, (const Vector3f&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opSubAssign(Vec3& in)", asMETHODPR(Vector3f, operator-=, (const Vector3f&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opMulAssign(Vec3& in)", asMETHODPR(Vector3f, operator*=, (const Vector3f&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opMulAssign(float& in)", asMETHODPR(Vector3f, operator*=, (const float&), Vector3f&));
	AngelManager::registerClassMethod("Vec3", "Vec3& opDivAssign(Vec3& in)", asMETHODPR(Vector3f, operator/=, (const Vector3f&), Vector3f&));
	//AngelManager::registerClassMethod("Vec2", "Vec2 opAdd(Vec2& in)", asMETHODPR(Vector2f, operator+, (const Vector2f&), Vector2f));
	AngelManager::registerClassProperty("Vec3", "float x", asOFFSET(Vector3f, x));
	AngelManager::registerClassProperty("Vec3", "float y", asOFFSET(Vector3f, y));
	AngelManager::registerClassProperty("Vec3", "float z", asOFFSET(Vector3f, z));

	//AngelManager::registerGlobalFunction("float Vec2_length(Vec2& in)", asFUNCTION(MLMath::MLVector2::length), asCALL_CDECL);
//	AngelManager::registerGlobalFunction("float Vec3_dot(Vec3& in, Vec3& in2)", asFUNCTION(MLMath::MLVector3::dot), asCALL_CDECL);
	//=============================================


	//vector 4
	//=============================================
	AngelManager::registerClass("Vec4", sizeof(MLMath::MLVector4::Vector4f), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassBehaviour("Vec4", "void f()", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec4Construct, (void*), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec4", "void f(float x)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec4Construct, (void*, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("Vec4", "void f(float x, float y, float z, float w)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(Vec4Construct, (void*, float, float, float, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassMethod("Vec4", "Vec4& opAssign(Vec4& in)", asMETHODPR(Vector4f, operator=, (const Vector4f&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opAssign(float& in)", asMETHODPR(Vector4f, operator=, (const float&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opAddAssign(Vec4& in)", asMETHODPR(Vector4f, operator+=, (const Vector4f&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opSubAssign(Vec4& in)", asMETHODPR(Vector4f, operator-=, (const Vector4f&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opMulAssign(Vec4& in)", asMETHODPR(Vector4f, operator*=, (const Vector4f&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opMulAssign(float& in)", asMETHODPR(Vector4f, operator*=, (const float&), Vector4f&));
	AngelManager::registerClassMethod("Vec4", "Vec4& opDivAssign(Vec4& in)", asMETHODPR(Vector4f, operator/=, (const Vector4f&), Vector4f&));
//	AngelManager::registerClassMethod("Vec2", "Vec2& opAdd(Vec2& in)", asMETHODPR(Vector2f, operator+, (const Vector2f&), Vector2f));
	AngelManager::registerClassProperty("Vec4", "float x", asOFFSET(Vector4f, x));
	AngelManager::registerClassProperty("Vec4", "float y", asOFFSET(Vector4f, y));
	AngelManager::registerClassProperty("Vec4", "float z", asOFFSET(Vector4f, z));
	AngelManager::registerClassProperty("Vec4", "float w", asOFFSET(Vector4f, w));

	//AngelManager::registerGlobalFunction("float Vec2_length(Vec2& in)", asFUNCTION(MLMath::MLVector2::length), asCALL_CDECL);
//	AngelManager::registerGlobalFunction("float Vec4_dot(Vec4& in, Vec4& in2)", asFUNCTION(MLMath::MLVector4::dot), asCALL_CDECL);
	//============================================

	//Matrix 4x4
	//=============================================

	//=============================================
}

void MLAngelRegister::registerGraphics()
{
	//sortMode enum
	//==============================================
	AngelManager::registerEnum("sortMode");
	AngelManager::registerEnumValue("sortMode", "Texture",		MLFormats::sortMode::Texture);
	AngelManager::registerEnumValue("sortMode", "NoSort",		MLFormats::sortMode::NoSort);
	AngelManager::registerEnumValue("sortMode", "Immediate",	MLFormats::sortMode::Immediate);
	AngelManager::registerEnumValue("sortMode", "FronttoBack",	MLFormats::sortMode::FronttoBack);
	AngelManager::registerEnumValue("sortMode", "BacktoFront",	MLFormats::sortMode::BacktoFront);
	//=============================================


	//spriteEffect enum
	//=============================================
	AngelManager::registerEnum("spriteEffect");
	AngelManager::registerEnumValue("spriteEffect", "None",				MLFormats::spriteEffects::None);
	AngelManager::registerEnumValue("spriteEffect", "FlipVertical",		MLFormats::spriteEffects::FlipVertical);
	AngelManager::registerEnumValue("spriteEffect", "FlipHorizontal",	MLFormats::spriteEffects::FlipHorizontal);
	AngelManager::registerEnumValue("spriteEffect", "FlipBoth",			MLFormats::spriteEffects::FlipBoth);
	//=============================================

	//point Type
	//=============================================
	AngelManager::registerEnum("pointType");
	AngelManager::registerEnumValue("pointType", "Flat", MLFormats::pointType::Flat);
	AngelManager::registerEnumValue("pointType", "Smooth", MLFormats::pointType::Smooth);
	//=============================================

	//font
	//=============================================
	AngelManager::registerClass("MLFont", 0, asOBJ_REF);

	AngelManager::registerClassBehaviour("MLFont", "MLFont@ f()", asBEHAVE_FACTORY, asFUNCTION(MLFontFactory));
	AngelManager::registerClassBehaviour("MLFont", "void f()", asBEHAVE_ADDREF, asMETHOD(MLFont, AddScriptRef));
	AngelManager::registerClassBehaviour("MLFont", "void f()", asBEHAVE_RELEASE, asMETHOD(MLFont, RemoveScriptRef));

	AngelManager::registerClassMethod("MLFont", "void LoadFontFace(string Loc)", asMETHODPR(MLFont, LoadFontFace, (std::string), void));
	AngelManager::registerClassMethod("MLFont", "void SetFontSize(int width, int height)", asMETHODPR(MLFont, SetFontSize, (unsigned int, unsigned int), void));
	AngelManager::registerClassMethod("MLFont", "void SetFontSize(int size)", asMETHODPR(MLFont, SetFontSize, (unsigned int), void));
	AngelManager::registerClassMethod("MLFont", "bool CacheChar(string chars)", asMETHODPR(MLFont, CacheChar, (std::string), bool));
	AngelManager::registerClassMethod("MLFont", "void SetPenColour(float r, float g, float b)", asMETHODPR(MLFont, setPenColour, (float, float, float), void));
	//=============================================

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//Need to update this!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	//SpriteBatch
	//=============================================
	/*AngelManager::registerClass("MLSpriteBatch", 0, asOBJ_REF | asOBJ_NOHANDLE);
	
	AngelManager::registerClassMethod("MLSpriteBatch", "void begin(bool Alpha, sortMode mode, bool wireFrame)", asMETHOD(MLSpriteBatch, Begin));
	
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec4& in, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector4f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec4& in, Vec4& in, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector4f&, Vector4f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec4& in, Vec4& in, float Rot, Vec2& in, spriteEffect Effect, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector4f&, Vector4f&, float, Vector2f&, MLFormats::spriteEffects, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec4& in, Vec4& in, float Rot, Vec2& in, int Layer, spriteEffect Effect, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector4f&, Vector4f&, float, Vector2f&, int, MLFormats::spriteEffects, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec2& in, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector2f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec2& in, Vec4& in, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector2f&, Vector4f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec2& in, Vec4& in, float Rot, Vec2& in, Vec2& in, spriteEffect Effect, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector2f&, Vector4f&, float, Vector2f&, Vector2f&, MLFormats::spriteEffects, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void Draw(int textureID, Vec2& in, Vec4& in, float Rot, Vec2& in, int Layer, Vec2& in, spriteEffect Effect, Vec4& in)", asMETHODPR(MLSpriteBatch, Draw, (GLuint, Vector2f&, Vector4f&, float, Vector2f&, int, Vector2f&, MLFormats::spriteEffects, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void Write(const MLFont@+ to, Vec2& in, string& in)", asMETHODPR(MLSpriteBatch, Write, (MLFont*, Vector2f&, std::string&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawPoint(Vec2& in)", asMETHODPR(MLSpriteBatch, DrawPoint, (Vector2f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawPoint(Vec2& in, float Size)", asMETHODPR(MLSpriteBatch, DrawPoint, (Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawPoint(Vec2& in, float Size, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawPoint, (Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawPoint(Vec2& in, float Size, pointType Type, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawPoint, (Vector2f&, float, MLFormats::pointType, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawPoint(Vec2& in, float Size, pointType Type, float Layer, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawPoint, (Vector2f&, float, MLFormats::pointType, float, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawLine(Vec2& in, Vec2& in)", asMETHODPR(MLSpriteBatch, DrawLine, (Vector2f&, Vector2f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawLine(Vec2& in, Vec2& in, float LineThickness)", asMETHODPR(MLSpriteBatch, DrawLine, (Vector2f&, Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawLine(Vec2& in, Vec2& in, float LineThickness, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawLine, (Vector2f&, Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawLine(Vec2& in, Vec2& in, float LineThickness, float Layer, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawLine, (Vector2f&, Vector2f&, float, float, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, float Size)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, float Size, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, float Size, Vec2& in, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, float, Vector2f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, Vec2& in, float Rot, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, Vec2& in, Vec2& in, float Rot, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, Vector2f&, Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawTriangle(Vec2& in, Vec2& in, Vec2& in, float Rot, float Layer,  Vec4& in)", asMETHODPR(MLSpriteBatch, DrawTriangle, (Vector2f&, Vector2f&, Vector2f&, float, float, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec2& in, float Size)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec2& in, float Size, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec2& in, Vec2& in, Vec2& in, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector2f&, Vector2f&, Vector2f&, Vector4f&), void)); 
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec2& in, Vec2& in, Vec2& in, float Rot, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector2f&, Vector2f&, Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec2& in, Vec2& in, Vec2& in, float Rot, float Layer, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector2f&, Vector2f&, Vector2f&, float, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec4& in, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector4f&, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawQuad(Vec4& in, float Thickness, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawQuad, (Vector4f&, float, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircle(Vec2& in, float Radius)", asMETHODPR(MLSpriteBatch, DrawCircle, (Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircle(Vec2& in, float Radius, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircle, (Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircle(Vec2& in, float Radius, float Thickness, int Segments)", asMETHODPR(MLSpriteBatch, DrawCircle, (Vector2f&, float, float, int), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircle(Vec2& in, float Radius, float Thickness, int Segments, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircle, (Vector2f&, float, float, int, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircle(Vec2& in, Vec2& in, float Thickness, int Segments, float Rot, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircle, (Vector2f&, Vector2f&, float, int, float, Vector4f&), void));

	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircleFill(Vec2& in, float Radius)", asMETHODPR(MLSpriteBatch, DrawCircleFill, (Vector2f&, float), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircleFill(Vec2& in, float Radius, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircleFill, (Vector2f&, float, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircleFill(Vec2& in, float Radius, int Segment)", asMETHODPR(MLSpriteBatch, DrawCircleFill, (Vector2f&, float, int), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircleFill(Vec2& in, float Radius, int Segment, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircleFill, (Vector2f&, float, int, Vector4f&), void));
	AngelManager::registerClassMethod("MLSpriteBatch", "void DrawCircleFill(Vec2& in, Vec2& in, int Segment, float Rot, Vec4& in)", asMETHODPR(MLSpriteBatch, DrawCircleFill, (Vector2f&, Vector2f&, int, float, Vector4f&), void));
	
	AngelManager::registerClassMethod("MLSpriteBatch", "void End()", asMETHOD(MLSpriteBatch, End));*/
	//=============================================
	
	//Effect Setter
	//=============================================
	AngelManager::registerClass("MLParticleEffect", sizeof(MLParticleEffect), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassProperty("MLParticleEffect", "int MaxAmount", asOFFSET(MLParticleEffect, MLParticleEffect::amount));
	AngelManager::registerClassProperty("MLParticleEffect", "double EmitLife", asOFFSET(MLParticleEffect, MLParticleEffect::emitLife));
	AngelManager::registerClassProperty("MLParticleEffect", "double EmitTime", asOFFSET(MLParticleEffect, MLParticleEffect::emitTime));
	AngelManager::registerClassProperty("MLParticleEffect", "int SpriteWidth", asOFFSET(MLParticleEffect, MLParticleEffect::spriteWidth));
	AngelManager::registerClassProperty("MLParticleEffect", "int SpriteHeight", asOFFSET(MLParticleEffect, MLParticleEffect::spriteHeight));
	AngelManager::registerClassProperty("MLParticleEffect", "float EmitWidth", asOFFSET(MLParticleEffect, MLParticleEffect::emitWidth));
	AngelManager::registerClassProperty("MLParticleEffect", "float EmitHeight", asOFFSET(MLParticleEffect, MLParticleEffect::emitHeight));
	AngelManager::registerClassProperty("MLParticleEffect", "float EmitSpeed", asOFFSET(MLParticleEffect, MLParticleEffect::emitSpeed));
	AngelManager::registerClassProperty("MLParticleEffect", "int MinAngle", asOFFSET(MLParticleEffect, MLParticleEffect::MinAngle));
	AngelManager::registerClassProperty("MLParticleEffect", "int MaxAngle", asOFFSET(MLParticleEffect, MLParticleEffect::MaxAngle));
	AngelManager::registerClassProperty("MLParticleEffect", "int MinSpredFromCentre", asOFFSET(MLParticleEffect, MLParticleEffect::minSpredFromCentre));
	AngelManager::registerClassProperty("MLParticleEffect", "int MaxSpredFromCentre", asOFFSET(MLParticleEffect, MLParticleEffect::maxSpredFromCentre));
	AngelManager::registerClassProperty("MLParticleEffect", "bool SetTimeAsZAxis", asOFFSET(MLParticleEffect, MLParticleEffect::setTimeAsZaxis));
	AngelManager::registerClassProperty("MLParticleEffect", "Vec3 TintColour", asOFFSET(MLParticleEffect, MLParticleEffect::tintColour));
	//=============================================

	//Particle Emitter
	//=============================================
	AngelManager::registerClass("MLParticleEmitter", 0, asOBJ_REF);

	AngelManager::registerClassBehaviour("MLParticleEmitter", "MLParticleEmitter@ f()", asBEHAVE_FACTORY, asFUNCTION(MLPEmitterFactory));
	AngelManager::registerClassBehaviour("MLParticleEmitter", "void f()", asBEHAVE_ADDREF, asMETHOD(MLParticleEmitter, AddScriptRef));
	AngelManager::registerClassBehaviour("MLParticleEmitter", "void f()", asBEHAVE_RELEASE, asMETHOD(MLParticleEmitter, RemoveScriptRef));

	AngelManager::registerClassMethod("MLParticleEmitter", "void SetEmitterEffect(MLParticleEffect& in)", asMETHOD(MLParticleEmitter, setEmitterEffect));
	AngelManager::registerClassMethod("MLParticleEmitter", "void SetPosition(Vec2& in)", asMETHODPR(MLParticleEmitter, setPosition, (Vector2f&), void));
	AngelManager::registerClassMethod("MLParticleEmitter", "void SetTexture(int TextureID)", asMETHOD(MLParticleEmitter, setTexture));
	//AngelManager::registerClassMethod("MLParticleEmitter", "void SetShader(MLShader& Shader)", asMETHOD(MLParticleEmitter, setEmitterShader));

	AngelManager::registerClassMethod("MLParticleEmitter", "void Start()", asMETHOD(MLParticleEmitter, start));
	AngelManager::registerClassMethod("MLParticleEmitter", "void Stop()", asMETHOD(MLParticleEmitter, stop));
	AngelManager::registerClassMethod("MLParticleEmitter", "void Clear()", asMETHOD(MLParticleEmitter, clearEmitter));
	AngelManager::registerClassMethod("MLParticleEmitter", "void Draw()", asMETHOD(MLParticleEmitter, Draw));
	AngelManager::registerClassMethod("MLParticleEmitter", "void Update(double Delta)", asMETHOD(MLParticleEmitter, update));
	//=============================================

	//Texture Manager
	//=============================================
	AngelManager::registerGlobalFunction("int MLTexture_LoadTexture(string Path)", asFUNCTIONPR(MLTextureManager::loadTexture, (std::string), GLuint), asCALL_CDECL);
	AngelManager::registerGlobalFunction("int MLTexture_GetWidth(int TextureID)", asFUNCTION(MLTextureManager::getWidth), asCALL_CDECL);
	AngelManager::registerGlobalFunction("int MLTexture_GetHeight(int TextureID)", asFUNCTION(MLTextureManager::getHeight), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLTexture_DeleteTexture(int TextureID)", asFUNCTION(MLTextureManager::deleteTexture), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLTexture_DeleteAllTextures()", asFUNCTION(MLTextureManager::deleteAllTextures), asCALL_CDECL);
	//=============================================

	//Camera
	//=============================================
	AngelManager::registerClass("MLCamera", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("MLCamera", "MLCamera@ f()", asBEHAVE_FACTORY, asFUNCTION(MLCameraFactory));
	AngelManager::registerClassBehaviour("MLCamera", "void f()", asBEHAVE_ADDREF, asMETHOD(MLCamera, AddScriptRef));
	AngelManager::registerClassBehaviour("MLCamera", "void f()", asBEHAVE_RELEASE, asMETHOD(MLCamera, RemoveScriptRef));

	AngelManager::registerClassMethod("MLCamera", "void SetPosition(float x, float y)", asMETHODPR(MLCamera, setPosition, (float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void SetPosition(Vec2& in)", asMETHODPR(MLCamera, setPosition, (Vector2f&), void));
	AngelManager::registerClassMethod("MLCamera", "void SetPosition(float x, float y, float z)", asMETHODPR(MLCamera, setPosition, (float, float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void SetPosition(Vec3& in)", asMETHODPR(MLCamera, setPosition, (Vector3f&), void));
	AngelManager::registerClassMethod("MLCamera", "void SetRotation(float rot)", asMETHOD(MLCamera, setRotation));
	AngelManager::registerClassMethod("MLCamera", "void SetScale(float xy)", asMETHODPR(MLCamera, setScale, (float), void));
	AngelManager::registerClassMethod("MLCamera", "void SetScale(Vec2& in)", asMETHODPR(MLCamera, setScale, (Vector2f&), void));
	AngelManager::registerClassMethod("MLCamera", "void SetScale(float x, float y)", asMETHODPR(MLCamera, setScale, (float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void MoveCamera(float x, float y)", asMETHODPR(MLCamera, moveCamera, (float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void MoveCamera(Vec2& in)", asMETHODPR(MLCamera, moveCamera, (Vector2f&), void));
	AngelManager::registerClassMethod("MLCamera", "void MoveCamera(float x, float y, float z)", asMETHODPR(MLCamera, moveCamera, (float, float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void MoveCamera(Vec3& in)", asMETHODPR(MLCamera, moveCamera, (Vector3f&), void));
	AngelManager::registerClassMethod("MLCamera", "void ZoomCamera(float xy)", asMETHODPR(MLCamera, zoomCamera, (float), void));
	AngelManager::registerClassMethod("MLCamera", "void ZoomCamera(float x, float y)", asMETHODPR(MLCamera, zoomCamera, (float, float), void));
	AngelManager::registerClassMethod("MLCamera", "void ZoomCamera(Vec2& in)", asMETHODPR(MLCamera, zoomCamera, (Vector2f&), void));
	AngelManager::registerClassMethod("MLCamera", "void RotateCamera(float rot)", asMETHOD(MLCamera, rotateCamera));
	//=============================================


	//Camera Manager
	//=============================================
	AngelManager::registerGlobalFunction("void MLCMang_CreateCamera(string Name)", asFUNCTION(MLCameraManager::createCamera), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_RemoveCamera(string Name)", asFUNCTION(MLCameraManager::removeCamera), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_AddCamera(MLCamera@+ to, string Name)", asFUNCTION(MLCameraManager::addCamera), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetActiveCamera(string Name)", asFUNCTION(MLCameraManager::setActiveCamera), asCALL_CDECL);
	AngelManager::registerGlobalFunction("MLCamera@+ MLCMang_GetActiveCamera()", asFUNCTION(MLCameraManager::getActiveCamera), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACPos(float x, float y)", asFUNCTIONPR(MLCameraManager::setActiveCameraPos, (float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACPos(float x, float y, float z)", asFUNCTIONPR(MLCameraManager::setActiveCameraPos, (float, float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACPos(Vec2& in)", asFUNCTIONPR(MLCameraManager::setActiveCameraPos, (Vector2f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACPos(Vec3& in)", asFUNCTIONPR(MLCameraManager::setActiveCameraPos, (Vector3f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACRotation(float rot)", asFUNCTION(MLCameraManager::setActiveCameraRotation), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACScale(float xy)", asFUNCTIONPR(MLCameraManager::setActiveCameraScale, (float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACScale(float x, float y)", asFUNCTIONPR(MLCameraManager::setActiveCameraScale, (float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_SetACScale(Vec2& in)", asFUNCTIONPR(MLCameraManager::setActiveCameraScale, (Vector2f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_MoveAC(float x, float y)", asFUNCTIONPR(MLCameraManager::moveActiveCamera, (float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_MoveAc(float x, float y, float z)", asFUNCTIONPR(MLCameraManager::moveActiveCamera, (float, float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_MoveAC(Vec2& in)", asFUNCTIONPR(MLCameraManager::moveActiveCamera, (Vector2f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_MoveAC(Vec3& in)", asFUNCTIONPR(MLCameraManager::moveActiveCamera, (Vector3f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_ZoomAC(float xy)", asFUNCTIONPR(MLCameraManager::zoomActiveCamera, (float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_ZoomAC(float x, float y)", asFUNCTIONPR(MLCameraManager::zoomActiveCamera, (float, float), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_ZoomAC(Vec2& in)", asFUNCTIONPR(MLCameraManager::zoomActiveCamera, (Vector2f&), void), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLCMang_RotateAC(float rot)", asFUNCTION(MLCameraManager::rotateActiveCamera), asCALL_CDECL);
	//=============================================
}

void MLAngelRegister::registerInput()
{
	//input Enums
	//============================================
	AngelManager::registerEnum("MLControllerAnalogs");
	AngelManager::registerEnumValue("MLControllerAnalogs", "CLeftAnalog", MLControllerAnalogs::CLeftAnalog);
	AngelManager::registerEnumValue("MLControllerAnalogs", "CRightAnalog", MLControllerAnalogs::CRightAnalog);
	AngelManager::registerEnumValue("MLControllerAnalogs", "CTriggerAnalog", MLControllerAnalogs::CTriggerAnalog);

	AngelManager::registerEnum("MLControllerButtons");
	AngelManager::registerEnumValue("MLControllerButtons", "CUp", MLControllerButtons::CUp);
	AngelManager::registerEnumValue("MLControllerButtons", "CDown", MLControllerButtons::CDown);
	AngelManager::registerEnumValue("MLControllerButtons", "CLeft", MLControllerButtons::CLeft);
	AngelManager::registerEnumValue("MLControllerButtons", "CRight", MLControllerButtons::CRight);
	AngelManager::registerEnumValue("MLControllerButtons", "CStart", MLControllerButtons::CStart);
	AngelManager::registerEnumValue("MLControllerButtons", "CBack", MLControllerButtons::CBack);
	AngelManager::registerEnumValue("MLControllerButtons", "CLStick", MLControllerButtons::CLStick);
	AngelManager::registerEnumValue("MLControllerButtons", "CRStick", MLControllerButtons::CRStick);
	AngelManager::registerEnumValue("MLControllerButtons", "CLShoulder", MLControllerButtons::CLShoulder);
	AngelManager::registerEnumValue("MLControllerButtons", "CRShoulder", MLControllerButtons::CRShoulder);
	AngelManager::registerEnumValue("MLControllerButtons", "Ca", MLControllerButtons::Ca);
	AngelManager::registerEnumValue("MLControllerButtons", "Cb", MLControllerButtons::Cb);
	AngelManager::registerEnumValue("MLControllerButtons", "Cx", MLControllerButtons::Cx);
	AngelManager::registerEnumValue("MLControllerButtons", "Cy", MLControllerButtons::Cy);

	AngelManager::registerEnum("MLMouseButtons");
	AngelManager::registerEnumValue("MLMouseButtons", "MLeft", MLMouseButtons::MLeft);
	AngelManager::registerEnumValue("MLMouseButtons", "MMiddle", MLMouseButtons::MMiddle);
	AngelManager::registerEnumValue("MLMouseButtons", "MRight", MLMouseButtons::MRight);
	AngelManager::registerEnumValue("MLMouseButtons", "MOne", MLMouseButtons::MOne);
	AngelManager::registerEnumValue("MLMouseButtons", "MTwo", MLMouseButtons::MTwo);

	AngelManager::registerEnum("MLKeys");
	AngelManager::registerEnumValue("MLKeys", "Back", MLKeys::Back);
	AngelManager::registerEnumValue("MLKeys", "Tab", MLKeys::Tab);
	AngelManager::registerEnumValue("MLKeys", "Clear", MLKeys::Clear);
	AngelManager::registerEnumValue("MLKeys", "Enter", MLKeys::Enter);
	AngelManager::registerEnumValue("MLKeys", "Shift", MLKeys::Shift);
	AngelManager::registerEnumValue("MLKeys", "Ctrl", MLKeys::Ctrl);
	AngelManager::registerEnumValue("MLKeys", "Alt", MLKeys::Alt);
	AngelManager::registerEnumValue("MLKeys", "Pause", MLKeys::Pause);
	AngelManager::registerEnumValue("MLKeys", "Caps", MLKeys::Caps);
	AngelManager::registerEnumValue("MLKeys", "Kana", MLKeys::Kana);
	AngelManager::registerEnumValue("MLKeys", "Hangeul", MLKeys::Hangeul);
	AngelManager::registerEnumValue("MLKeys", "Hangul", MLKeys::Hangul);
	AngelManager::registerEnumValue("MLKeys", "Final", MLKeys::Final);
	AngelManager::registerEnumValue("MLKeys", "Hanja", MLKeys::Hanja);
	AngelManager::registerEnumValue("MLKeys", "Kanji", MLKeys::Kanji);
	AngelManager::registerEnumValue("MLKeys", "Convert", MLKeys::Convert);
	AngelManager::registerEnumValue("MLKeys", "NonConvert", MLKeys::NonConvert);
	AngelManager::registerEnumValue("MLKeys", "Accept", MLKeys::Accept);
	AngelManager::registerEnumValue("MLKeys", "ModeChange", MLKeys::ModeChange);
	AngelManager::registerEnumValue("MLKeys", "Esc", MLKeys::Esc);
	AngelManager::registerEnumValue("MLKeys", "Space", MLKeys::Space);
	AngelManager::registerEnumValue("MLKeys", "PageUp", MLKeys::PageUp);
	AngelManager::registerEnumValue("MLKeys", "PageDown", MLKeys::PageDown);
	AngelManager::registerEnumValue("MLKeys", "End", MLKeys::End);
	AngelManager::registerEnumValue("MLKeys", "Home", MLKeys::Home);
	AngelManager::registerEnumValue("MLKeys", "Left", MLKeys::Left);
	AngelManager::registerEnumValue("MLKeys", "Up", MLKeys::Up);
	AngelManager::registerEnumValue("MLKeys", "Right", MLKeys::Right);
	AngelManager::registerEnumValue("MLKeys", "Down", MLKeys::Down);
	AngelManager::registerEnumValue("MLKeys", "Select", MLKeys::Select);
	AngelManager::registerEnumValue("MLKeys", "Print", MLKeys::Print);
	AngelManager::registerEnumValue("MLKeys", "Execute", MLKeys::Execute);
	AngelManager::registerEnumValue("MLKeys", "PrintScreen", MLKeys::PrintScreen);
	AngelManager::registerEnumValue("MLKeys", "Insert", MLKeys::Insert);
	AngelManager::registerEnumValue("MLKeys", "Delete", MLKeys::Delete);
	AngelManager::registerEnumValue("MLKeys", "Help", MLKeys::Help);
	AngelManager::registerEnumValue("MLKeys", "A", MLKeys::A);
	AngelManager::registerEnumValue("MLKeys", "B", MLKeys::B);
	AngelManager::registerEnumValue("MLKeys", "C", MLKeys::C);
	AngelManager::registerEnumValue("MLKeys", "D", MLKeys::D);
	AngelManager::registerEnumValue("MLKeys", "E", MLKeys::E);
	AngelManager::registerEnumValue("MLKeys", "F", MLKeys::F);
	AngelManager::registerEnumValue("MLKeys", "G", MLKeys::G);
	AngelManager::registerEnumValue("MLKeys", "H", MLKeys::H);
	AngelManager::registerEnumValue("MLKeys", "I", MLKeys::I);
	AngelManager::registerEnumValue("MLKeys", "J", MLKeys::J);
	AngelManager::registerEnumValue("MLKeys", "K", MLKeys::K);
	AngelManager::registerEnumValue("MLKeys", "L", MLKeys::L);
	AngelManager::registerEnumValue("MLKeys", "M", MLKeys::M);
	AngelManager::registerEnumValue("MLKeys", "N", MLKeys::N);
	AngelManager::registerEnumValue("MLKeys", "O", MLKeys::O);
	AngelManager::registerEnumValue("MLKeys", "P", MLKeys::P);
	AngelManager::registerEnumValue("MLKeys", "Q", MLKeys::Q);
	AngelManager::registerEnumValue("MLKeys", "R", MLKeys::R);
	AngelManager::registerEnumValue("MLKeys", "S", MLKeys::S);
	AngelManager::registerEnumValue("MLKeys", "T", MLKeys::T);
	AngelManager::registerEnumValue("MLKeys", "U", MLKeys::U);
	AngelManager::registerEnumValue("MLKeys", "V", MLKeys::V);
	AngelManager::registerEnumValue("MLKeys", "W", MLKeys::W);
	AngelManager::registerEnumValue("MLKeys", "X", MLKeys::X);
	AngelManager::registerEnumValue("MLKeys", "Y", MLKeys::Y);
	AngelManager::registerEnumValue("MLKeys", "Z", MLKeys::Z);
	AngelManager::registerEnumValue("MLKeys", "Zero", MLKeys::Zero);
	AngelManager::registerEnumValue("MLKeys", "One", MLKeys::One);
	AngelManager::registerEnumValue("MLKeys", "Two", MLKeys::Two);
	AngelManager::registerEnumValue("MLKeys", "Three", MLKeys::Three);
	AngelManager::registerEnumValue("MLKeys", "Four", MLKeys::Four);
	AngelManager::registerEnumValue("MLKeys", "Five", MLKeys::Five);
	AngelManager::registerEnumValue("MLKeys", "Six", MLKeys::Six);
	AngelManager::registerEnumValue("MLKeys", "Seven", MLKeys::Seven);
	AngelManager::registerEnumValue("MLKeys", "Eight", MLKeys::Eight);
	AngelManager::registerEnumValue("MLKeys", "Nine", MLKeys::Nine);
	AngelManager::registerEnumValue("MLKeys", "LMeta", MLKeys::LMeta);
	AngelManager::registerEnumValue("MLKeys", "RMeta", MLKeys::RMeta);
	AngelManager::registerEnumValue("MLKeys", "Apps", MLKeys::Apps);
	AngelManager::registerEnumValue("MLKeys", "SleepKey", MLKeys::SleepKey);
	AngelManager::registerEnumValue("MLKeys", "NPZero", MLKeys::NPZero);
	AngelManager::registerEnumValue("MLKeys", "NPOne", MLKeys::NPOne);
	AngelManager::registerEnumValue("MLKeys", "NPTwo", MLKeys::NPTwo);
	AngelManager::registerEnumValue("MLKeys", "NPThree", MLKeys::NPThree);
	AngelManager::registerEnumValue("MLKeys", "NPFour", MLKeys::NPFour);
	AngelManager::registerEnumValue("MLKeys", "NPFive", MLKeys::NPFive);
	AngelManager::registerEnumValue("MLKeys", "NPSix", MLKeys::NPSix);
	AngelManager::registerEnumValue("MLKeys", "NPSeven", MLKeys::NPSeven);
	AngelManager::registerEnumValue("MLKeys", "NPEight", MLKeys::NPEight);
	AngelManager::registerEnumValue("MLKeys", "NPNine", MLKeys::NPNine);
	AngelManager::registerEnumValue("MLKeys", "NPMul", MLKeys::NPMul);
	AngelManager::registerEnumValue("MLKeys", "NPAdd", MLKeys::NPAdd);
	AngelManager::registerEnumValue("MLKeys", "NPSeperator", MLKeys::NPSeperator);
	AngelManager::registerEnumValue("MLKeys", "NPSub", MLKeys::NPSub);
	AngelManager::registerEnumValue("MLKeys", "NPDecimal", MLKeys::NPDecimal);
	AngelManager::registerEnumValue("MLKeys", "NPDivide", MLKeys::NPDivide);
	AngelManager::registerEnumValue("MLKeys", "NPNumLock", MLKeys::NPNumLock);
	AngelManager::registerEnumValue("MLKeys", "NPEqual", MLKeys::NPEqual);
	AngelManager::registerEnumValue("MLKeys", "F1", MLKeys::F1);
	AngelManager::registerEnumValue("MLKeys", "F2", MLKeys::F2);
	AngelManager::registerEnumValue("MLKeys", "F3", MLKeys::F3);
	AngelManager::registerEnumValue("MLKeys", "F4", MLKeys::F4);
	AngelManager::registerEnumValue("MLKeys", "F5", MLKeys::F5);
	AngelManager::registerEnumValue("MLKeys", "F6", MLKeys::F6);
	AngelManager::registerEnumValue("MLKeys", "F7", MLKeys::F7);
	AngelManager::registerEnumValue("MLKeys", "F8", MLKeys::F8);
	AngelManager::registerEnumValue("MLKeys", "F9", MLKeys::F9);
	AngelManager::registerEnumValue("MLKeys", "F10", MLKeys::F10);
	AngelManager::registerEnumValue("MLKeys", "F11", MLKeys::F11);
	AngelManager::registerEnumValue("MLKeys", "F12", MLKeys::F12);
	AngelManager::registerEnumValue("MLKeys", "F13", MLKeys::F13);
	AngelManager::registerEnumValue("MLKeys", "F14", MLKeys::F14);
	AngelManager::registerEnumValue("MLKeys", "F15", MLKeys::F15);
	AngelManager::registerEnumValue("MLKeys", "F16", MLKeys::F16);
	AngelManager::registerEnumValue("MLKeys", "F17", MLKeys::F17);
	AngelManager::registerEnumValue("MLKeys", "F18", MLKeys::F18);
	AngelManager::registerEnumValue("MLKeys", "F19", MLKeys::F19);
	AngelManager::registerEnumValue("MLKeys", "F20", MLKeys::F20);
	AngelManager::registerEnumValue("MLKeys", "F21", MLKeys::F21);
	AngelManager::registerEnumValue("MLKeys", "F22", MLKeys::F22);
	AngelManager::registerEnumValue("MLKeys", "F23", MLKeys::F23);
	AngelManager::registerEnumValue("MLKeys", "F24", MLKeys::F24);
	AngelManager::registerEnumValue("MLKeys", "ScrollLock", MLKeys::ScrollLock);
	AngelManager::registerEnumValue("MLKeys", "LShift", MLKeys::LShift);
	AngelManager::registerEnumValue("MLKeys", "RShift", MLKeys::RShift);
	AngelManager::registerEnumValue("MLKeys", "LCtrl", MLKeys::LCtrl);
	AngelManager::registerEnumValue("MLKeys", "RCtrl", MLKeys::RCtrl);
	AngelManager::registerEnumValue("MLKeys", "LAlt", MLKeys::LAlt);
	AngelManager::registerEnumValue("MLKeys", "RAlt", MLKeys::RAlt);
	AngelManager::registerEnumValue("MLKeys", "BrowserBack", MLKeys::BrowserBack);
	AngelManager::registerEnumValue("MLKeys", "BrowserFoward", MLKeys::BrowserFoward);
	AngelManager::registerEnumValue("MLKeys", "BrowserRefresh", MLKeys::BrowserRefresh);
	AngelManager::registerEnumValue("MLKeys", "BrowserStop", MLKeys::BrowserStop);
	AngelManager::registerEnumValue("MLKeys", "BrowserSearch", MLKeys::BrowserSearch);
	AngelManager::registerEnumValue("MLKeys", "BrowserFav", MLKeys::BrowserFav);
	AngelManager::registerEnumValue("MLKeys", "BrowserHome", MLKeys::BrowserHome);
	AngelManager::registerEnumValue("MLKeys", "VolumeUp", MLKeys::VolumeUp);
	AngelManager::registerEnumValue("MLKeys", "VolumeDown", MLKeys::VolumeDown);
	AngelManager::registerEnumValue("MLKeys", "VolumeMute", MLKeys::VolumeMute);
	AngelManager::registerEnumValue("MLKeys", "NextTrack", MLKeys::NextTrack);
	AngelManager::registerEnumValue("MLKeys", "PrevTrack", MLKeys::PrevTrack);
	AngelManager::registerEnumValue("MLKeys", "StopTrack", MLKeys::StopTrack);
	AngelManager::registerEnumValue("MLKeys", "PlayTrack", MLKeys::PlayTrack);
	AngelManager::registerEnumValue("MLKeys", "Mail", MLKeys::Mail);
	AngelManager::registerEnumValue("MLKeys", "Media", MLKeys::Media);
	AngelManager::registerEnumValue("MLKeys", "App1", MLKeys::App1);
	AngelManager::registerEnumValue("MLKeys", "App2", MLKeys::App2);
	AngelManager::registerEnumValue("MLKeys", "OEM_1", MLKeys::OEM_1);
	AngelManager::registerEnumValue("MLKeys", "Plus", MLKeys::Plus);
	AngelManager::registerEnumValue("MLKeys", "Comma", MLKeys::Comma);
	AngelManager::registerEnumValue("MLKeys", "Minus", MLKeys::Minus);
	AngelManager::registerEnumValue("MLKeys", "Period", MLKeys::Period);
	AngelManager::registerEnumValue("MLKeys", "OEM_2", MLKeys::OEM_2);
	AngelManager::registerEnumValue("MLKeys", "OEM_3", MLKeys::OEM_3);
	AngelManager::registerEnumValue("MLKeys", "OEM_4", MLKeys::OEM_4);
	AngelManager::registerEnumValue("MLKeys", "OEM_5", MLKeys::OEM_5);
	AngelManager::registerEnumValue("MLKeys", "OEM_6", MLKeys::OEM_6);
	AngelManager::registerEnumValue("MLKeys", "OEM_7", MLKeys::OEM_7);
	AngelManager::registerEnumValue("MLKeys", "OEM_8", MLKeys::OEM_8);
	AngelManager::registerEnumValue("MLKeys", "AX", MLKeys::AX);
	AngelManager::registerEnumValue("MLKeys", "OEM_102", MLKeys::OEM_102);
	//============================================


	//MLController
	//============================================
	//There is no factory function as the controller can only be obtained from the
	//input manager
	AngelManager::registerClass("MLController", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("MLController", "void f()", asBEHAVE_ADDREF, asMETHOD(MLController, AddScriptRef));
	AngelManager::registerClassBehaviour("MLController", "void f()", asBEHAVE_RELEASE, asMETHOD(MLController, RemoveScriptRef));

	AngelManager::registerClassMethod("MLController", "bool IsControllerActive()", asMETHOD(MLController, isControllerActive));
	AngelManager::registerClassMethod("MLController", "bool IsButtonPressed(uint8 button)", asMETHOD(MLController, isButtonPressed));
	AngelManager::registerClassMethod("MLController", "Vec2& GetAnalog(MLControllerAnalogs analog)", asMETHOD(MLController, getAnalog));
	AngelManager::registerClassMethod("MLController", "bool SetForceFeedback(float amount)", asMETHODPR(MLController, setForceFeedback, (float), bool));
	AngelManager::registerClassMethod("MLController", "bool SetForceFeedback(float LeftAmount, float RightAmount)", asMETHODPR(MLController, setForceFeedback, (float, float), bool));
	//============================================


	//Input Manager
	//============================================
	AngelManager::registerGlobalFunction("bool MLIManager_IsKeyPressed(int key)", asFUNCTION(MLInputManager::isKeyPressed), asCALL_CDECL);
	AngelManager::registerGlobalFunction("Vec2& MLIManager_GetMousePosition()", asFUNCTION(MLInputManager::getMousePositon), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLIManager_IsMouseButtonPressed(int button)", asFUNCTION(MLInputManager::isMouseButtonPressed), asCALL_CDECL);
	AngelManager::registerGlobalFunction("int MLIManager_GetScrollWheel()", asFUNCTION(MLInputManager::getScrollWheel), asCALL_CDECL);
	AngelManager::registerGlobalFunction("MLController@+ MLIManager_GetController(int ID)", asFUNCTION(MLInputManager::getController), asCALL_CDECL);
	//AngelManager::registerGlobalFunction("keyEvent MLIManager_GetEvent()", asFUNCTION(MLInputManager::getEvent), asCALL_CDECL);
	//============================================
}

void MLAngelRegister::registerAudio()
{
	AngelManager::registerClass("MLAudioListener", sizeof(MLAudioListener), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassProperty("MLAudioListener", "float Gain", asOFFSET(MLAudioListener, Gain));
	AngelManager::registerClassProperty("MLAudioListener", "Vec3 Position", asOFFSET(MLAudioListener, Position));
	AngelManager::registerClassProperty("MLAudioListener", "Vec3 Velocity", asOFFSET(MLAudioListener, Velocity));

	AngelManager::registerClass("MLAudioSource", sizeof(MLAudioSource), asOBJ_VALUE | asOBJ_POD );
	AngelManager::registerClassProperty("MLAudioSource", "float MaxDistance", asOFFSET(MLAudioSource, MaxDistance));
	AngelManager::registerClassProperty("MLAudioSource", "float RollOff", asOFFSET(MLAudioSource, RollOff));
	AngelManager::registerClassProperty("MLAudioSource", "float ReferenceDistance", asOFFSET(MLAudioSource, ReferenceDistance));
	AngelManager::registerClassProperty("MLAudioSource", "float MinGain", asOFFSET(MLAudioSource, MinGain));
	AngelManager::registerClassProperty("MLAudioSource", "float MaxGain", asOFFSET(MLAudioSource, MaxGain));
	AngelManager::registerClassProperty("MLAudioSource", "float ConeOuterGain", asOFFSET(MLAudioSource, ConeOuterGain));
	AngelManager::registerClassProperty("MLAudioSource", "float ConeInnerAngle", asOFFSET(MLAudioSource, ConeInnerAngle));
	AngelManager::registerClassProperty("MLAudioSource", "float ConeOuterAngle", asOFFSET(MLAudioSource, ConeOuterAngle));
	AngelManager::registerClassProperty("MLAudioSource", "Vec3 Position", asOFFSET(MLAudioSource, Position));
	AngelManager::registerClassProperty("MLAudioSource", "Vec3 Velocity", asOFFSET(MLAudioSource, Velocity));
	AngelManager::registerClassProperty("MLAudioSource", "Vec3 Direction", asOFFSET(MLAudioSource, Direction));
	AngelManager::registerClassProperty("MLAudioSource", "bool Looping", asOFFSET(MLAudioSource, Looping));

	AngelManager::registerGlobalFunction("bool MLAudio_SetListener(MLAudioListener& list)", asFUNCTION(MLAudioManager::setListener), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_SetListenerPosition(Vec3& pos)", asFUNCTION(MLAudioManager::setListenerPosition), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_LoadWavFile(string loc, int& out)", asFUNCTIONPR(MLAudioManager::loadWavFile, (std::string, int&), bool), asCALL_CDECL);
	//AngelManager::registerGlobalFunction("bool MLAudio_PreLoadBuffer(int ID)", asFUNCTION(MLAudioManager::preLoadBuffer), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_PlayAudio(int ID)", asFUNCTIONPR(MLAudioManager::playAudio, (int), bool), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_PlayAudio(int ID, MLAudioSource& source)", asFUNCTIONPR(MLAudioManager::playAudio, (int, MLAudioSource), bool), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_PauseAudio(int ID)", asFUNCTION(MLAudioManager::pauseAudio), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_StopAudio(int ID)", asFUNCTION(MLAudioManager::stopAudio), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_PauseAllAudio()", asFUNCTION(MLAudioManager::pauseAllAudio), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_ResumeAllAudio()", asFUNCTION(MLAudioManager::resumeAllAudio), asCALL_CDECL);
	AngelManager::registerGlobalFunction("bool MLAudio_StopAllAudio()", asFUNCTION(MLAudioManager::stopAllAudio), asCALL_CDECL);
	AngelManager::registerGlobalFunction("int MLAudio_GetAudioPosition(int ID)", asFUNCTION(MLAudioManager::getAudioPosition), asCALL_CDECL);
}

void MLAngelRegister::registerBox2D()
{
	//b2vec2
	//=============================================
	AngelManager::registerClass("b2Vec2", sizeof(b2Vec2), asOBJ_VALUE | asOBJ_POD);

	AngelManager::registerClassBehaviour("b2Vec2", "void f()", asBEHAVE_CONSTRUCT, asFUNCTIONPR(b2Vec2Construnct, (void*), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("b2Vec2", "void f(float x, float y)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(b2Vec2Construnct, (void*, float, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassMethod("b2Vec2", "void opAddAssign(Vec2& in)", asMETHODPR(b2Vec2, operator+=, (const b2Vec2&), void));
	AngelManager::registerClassMethod("b2Vec2", "void opSubAssign(Vec2& in)", asMETHODPR(b2Vec2, operator-=, (const b2Vec2&), void));
	AngelManager::registerClassMethod("b2Vec2", "void opMulAssign(float in)", asMETHODPR(b2Vec2, operator*=, (const float), void));
	AngelManager::registerClassMethod("b2Vec2", "void Set(float x, float y)", asMETHOD(b2Vec2, Set), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Vec2", "float Length()", asMETHOD(b2Vec2, Length), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Vec2", "float Normalize()", asMETHOD(b2Vec2, Normalize), asCALL_THISCALL);

	AngelManager::registerClassProperty("b2Vec2", "float x", asOFFSET(b2Vec2, x));
	AngelManager::registerClassProperty("b2Vec2", "float y", asOFFSET(b2Vec2, y));
	//=============================================


	//b2vec3
	//=============================================
	AngelManager::registerClass("b2Vec3", sizeof(b2Vec3), asOBJ_VALUE | asOBJ_POD);

	AngelManager::registerClassBehaviour("b2Vec3", "void f()", asBEHAVE_CONSTRUCT, asFUNCTIONPR(b2Vec3Construnct, (void*), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassBehaviour("b2Vec3", "void f(float x, float y, float z)", asBEHAVE_CONSTRUCT, asFUNCTIONPR(b2Vec3Construnct, (void*, float, float, float), void), asCALL_CDECL_OBJFIRST);
	AngelManager::registerClassMethod("b2Vec3", "void opAddAssign(Vec2& in)", asMETHODPR(b2Vec3, operator+=, (const b2Vec3&), void));
	AngelManager::registerClassMethod("b2Vec3", "void opSubAssign(Vec2& in)", asMETHODPR(b2Vec3, operator-=, (const b2Vec3&), void));
	AngelManager::registerClassMethod("b2Vec3", "void opMulAssign(float in)", asMETHODPR(b2Vec3, operator*=, (const float), void));
	AngelManager::registerClassMethod("b2Vec3", "void Set(float x, float y, float z)", asMETHOD(b2Vec3, Set), asCALL_THISCALL);

	AngelManager::registerClassProperty("b2Vec3", "float x", asOFFSET(b2Vec3, x));
	AngelManager::registerClassProperty("b2Vec3", "float y", asOFFSET(b2Vec3, y));
	AngelManager::registerClassProperty("b2Vec3", "float z", asOFFSET(b2Vec3, z));
	//=============================================

	//b2AABB
	//=============================================
	AngelManager::registerClass("b2AABB", sizeof(b2AABB), asOBJ_VALUE | asOBJ_POD);
	
	AngelManager::registerClassMethod("b2AABB", "bool IsValid()", asMETHOD(b2AABB, IsValid), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "b2Vec2& GetCenter()", asMETHOD(b2AABB, GetCenter), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "b2Vec2& GetExtents()", asMETHOD(b2AABB, GetExtents), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "float GetPerimeter()", asMETHOD(b2AABB, GetPerimeter), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "void Combine(const b2AABB& in)", asMETHODPR(b2AABB, Combine,(const b2AABB&), void), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "void Combine(const b2AABB& in, const b2AABB& in)", asMETHODPR(b2AABB, Combine,(const b2AABB&, const b2AABB&), void), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2AABB", "bool Contains(const b2AABB& in)", asMETHOD(b2AABB, Contains), asCALL_THISCALL);

	AngelManager::registerClassProperty("b2AABB", "b2Vec2 lowerBound", asOFFSET(b2AABB, lowerBound));
	AngelManager::registerClassProperty("b2AABB", "b2Vec2 upperBound", asOFFSET(b2AABB, upperBound));
	//=============================================

	//b2Body
	//=============================================
	//asOBJ_NOCOUNT should be correct for all box2D objects since their memory management is all handled by the b2World object
	//That Object however will most likely not be NOCOUNT and I assume handled by angelscript/c++
	AngelManager::registerClass("b2Body", 0, asOBJ_REF | asOBJ_NOCOUNT);
	
	//b2Fixture * 	CreateFixture (const b2FixtureDef *def)
	//b2Fixture * 	CreateFixture (const b2Shape *shape, float32 density)
	//void 	DestroyFixture (b2Fixture *fixture)
	AngelManager::registerClassMethod("b2Body", "void SetTransform(const b2Vec2& in, float Angle)", asMETHOD(b2Body, SetTransform), asCALL_THISCALL);
	//const b2Transform & 	GetTransform () const 
	AngelManager::registerClassMethod("b2Body", "const b2Vec2& GetPosition()", asMETHOD(b2Body, GetPosition), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetAngle()", asMETHOD(b2Body, GetAngle), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "const b2Vec2& GetWorldCenter()", asMETHOD(b2Body, GetWorldCenter), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "const b2Vec2& GetLocalCenter()", asMETHOD(b2Body, GetLocalCenter), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetLinearVelocity(const b2Vec2& in)", asMETHOD(b2Body, SetLinearVelocity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetLinearVelocity()", asMETHOD(b2Body, GetLinearVelocity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetAngularVelocity(float val)", asMETHOD(b2Body, SetAngularVelocity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetAngularVelocity()", asMETHOD(b2Body, GetAngularVelocity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void ApplyForce(const b2Vec2& in, const b2Vec2& in)", asMETHOD(b2Body, ApplyForce), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void ApplyForceToCenter(const b2Vec2& in)", asMETHOD(b2Body, ApplyForceToCenter), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void ApplyTorque(float torque)", asMETHOD(b2Body, ApplyTorque), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void ApplyLinearImpulse(const b2Vec2& in, const b2Vec2& in)", asMETHOD(b2Body, ApplyLinearImpulse), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void ApplyAngularImpulse(float impulse)", asMETHOD(b2Body, ApplyAngularImpulse), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetMass()", asMETHOD(b2Body, GetMass), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetInertia()", asMETHOD(b2Body, GetInertia), asCALL_THISCALL);
	//void 	GetMassData (b2MassData *data) const
	//void 	SetMassData (const b2MassData *data)
	AngelManager::registerClassMethod("b2Body", "void ResetMassData()", asMETHOD(b2Body, ResetMassData), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetWorldPoint(const b2Vec2& in)", asMETHOD(b2Body, GetWorldPoint), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetWorldVector(const b2Vec2& in)", asMETHOD(b2Body, GetWorldVector), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetLocalPoint(const b2Vec2& in)", asMETHOD(b2Body, GetLocalPoint), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetLocalVector(const b2Vec2& in)", asMETHOD(b2Body, GetLocalVector), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetLinearVelocityFromWorldPoint(const b2Vec2& in)", asMETHOD(b2Body, GetLinearVelocityFromWorldPoint), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "b2Vec2 GetLinearVelocityFromLocalPoint(const b2Vec2& in)", asMETHOD(b2Body, GetLinearVelocityFromLocalPoint), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetLinearDamping()", asMETHOD(b2Body, GetLinearDamping), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetLinearDamping(float linearDamping)", asMETHOD(b2Body, SetLinearDamping), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetAngularDamping()", asMETHOD(b2Body, GetAngularDamping), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetAngularDamping()", asMETHOD(b2Body, SetAngularDamping), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "float GetGravityScale()", asMETHOD(b2Body, GetGravityScale), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetGravityScale(float scale)", asMETHOD(b2Body, SetGravityScale), asCALL_THISCALL);	
	//void 	SetType (b2BodyType type)
	//Set the type of this body. This may alter the mass and velocity.
	//b2BodyType 	GetType () const
	//Get the type of this body.
	AngelManager::registerClassMethod("b2Body", "void SetBullet(bool flag)", asMETHOD(b2Body, SetBullet), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "bool IsBullet()", asMETHOD(b2Body, IsBullet), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetSleepingAllowed(bool flag)", asMETHOD(b2Body, SetSleepingAllowed), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "bool IsSleepingAllowed()", asMETHOD(b2Body, IsSleepingAllowed), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetAwake(bool flag)", asMETHOD(b2Body, SetAwake), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "bool IsAwake()", asMETHOD(b2Body, IsAwake), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetActive(bool flag)", asMETHOD(b2Body, SetActive), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "bool IsActive()", asMETHOD(b2Body, IsActive), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "void SetFixedRotation(bool flag)", asMETHOD(b2Body, SetFixedRotation), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Body", "bool IsFixedRotation()", asMETHOD(b2Body, IsFixedRotation), asCALL_THISCALL);
	//b2Fixture * 	GetFixtureList ()
	//Get the list of all fixtures attached to this body.
	//const b2Fixture * 	GetFixtureList () const
	//b2JointEdge * 	GetJointList ()
	//Get the list of all joints attached to this body.
	//const b2JointEdge * 	GetJointList () const
	//b2ContactEdge * 	GetContactList ()
	//const b2ContactEdge * 	GetContactList () const
	//b2Body * 	GetNext ()
	//Get the next body in the world's body list.
	//const b2Body * 	GetNext () const
	//void * 	GetUserData () const
	//Get the user data pointer that was provided in the body definition.
	//void 	SetUserData (void *data)
	//Set the user data. Use this to store your application specific data.
	//b2World * 	GetWorld ()
	//Get the parent world of this body.
	//const b2World * 	GetWorld () const
	//void 	Dump ()
	//Dump this body to a log file. 
	//=============================================

	//b2bodydef
	//=============================================
	AngelManager::registerClass("b2BodyDef", sizeof(b2BodyDef), asOBJ_VALUE | asOBJ_POD);

	AngelManager::registerClassProperty("b2BodyDef", "int type", asOFFSET(b2BodyDef, type));
	AngelManager::registerClassProperty("b2BodyDef", "b2Vec2 position", asOFFSET(b2BodyDef, position));
	AngelManager::registerClassProperty("b2BodyDef", "float angle", asOFFSET(b2BodyDef, angle));
	AngelManager::registerClassProperty("b2BodyDef", "b2Vec2 linearVelocity", asOFFSET(b2BodyDef, linearVelocity));
	AngelManager::registerClassProperty("b2BodyDef", "float angularVelocity", asOFFSET(b2BodyDef, angularVelocity));
	AngelManager::registerClassProperty("b2BodyDef", "float linearDamping", asOFFSET(b2BodyDef, linearDamping));
	AngelManager::registerClassProperty("b2BodyDef", "float angularDamping", asOFFSET(b2BodyDef, angularDamping));
	AngelManager::registerClassProperty("b2BodyDef", "bool allowSleep", asOFFSET(b2BodyDef, allowSleep));
	AngelManager::registerClassProperty("b2BodyDef", "bool awake", asOFFSET(b2BodyDef, awake));
	AngelManager::registerClassProperty("b2BodyDef", "bool fixedRotation", asOFFSET(b2BodyDef, fixedRotation));
	AngelManager::registerClassProperty("b2BodyDef", "bool bullet", asOFFSET(b2BodyDef, bullet));
	AngelManager::registerClassProperty("b2BodyDef", "bool active", asOFFSET(b2BodyDef, active));
	//	void * 	userData
	//	Use this to store application specific body data.
	AngelManager::registerClassProperty("b2BodyDef", "float gravityScale", asOFFSET(b2BodyDef, gravityScale));
	//=============================================

	//b2Shape
	//=============================================
	AngelManager::registerClass("b2Shape", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("b2Shape", "void f()", asBEHAVE_ADDREF, asFUNCTION(b2ShapeAdd_Ref), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassBehaviour("b2Shape", "void f()", asBEHAVE_RELEASE, asFUNCTION(b2ShapeRemove), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassMethod("b2Shape", "int GetType()", asMETHOD(b2Shape, GetType), asCALL_THISCALL);
	//=============================================

	//b2PolygonShape
	//=============================================
	AngelManager::registerClass("b2PolygonShape", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("b2PolygonShape", "void f()", asBEHAVE_ADDREF, asFUNCTION(b2PShapeAdd_Ref), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassBehaviour("b2PolygonShape", "void f()", asBEHAVE_RELEASE, asFUNCTION(b2PShapeRemove), asCALL_CDECL_OBJLAST);
	//b2Shape * 	Clone (b2BlockAllocator *allocator) const
	//Implement b2Shape.
	AngelManager::registerClassMethod("b2PolygonShape", "const int GetChildCount()", asMETHOD(b2PolygonShape, GetChildCount), asCALL_THISCALL);
	//void 	Set (const b2Vec2 *vertices, int32 vertexCount)
	AngelManager::registerClassMethod("b2PolygonShape", "void SetAsBox(float height, float width)", asMETHODPR(b2PolygonShape, SetAsBox, (float, float), void), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2PolygonShape", "void SetAsBox(float height, float width, const b2Vec2& in, float angle)", asMETHODPR(b2PolygonShape, SetAsBox, (float, float, const b2Vec2&, float), void), asCALL_THISCALL);
	//bool 	TestPoint (const b2Transform &transform, const b2Vec2 &p) const
	//bool 	RayCast (b2RayCastOutput *output, const b2RayCastInput &input, const b2Transform &transform, int32 childIndex) const
	//Implement b2Shape.
	//void 	ComputeAABB (b2AABB *aabb, const b2Transform &transform, int32 childIndex) const
	//void 	ComputeMass (b2MassData *massData, float32 density) const
	AngelManager::registerClassMethod("b2PolygonShape", "const int GetVertexCount()", asMETHOD(b2PolygonShape, GetVertexCount), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2PolygonShape", "const b2Vec2& GetVertex(int index)", asMETHOD(b2PolygonShape, GetVertex), asCALL_THISCALL); 
	//=============================================

	//b2Fixture
	//=============================================
	AngelManager::registerClass("b2Fixture", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("b2Fixture", "void f()", asBEHAVE_ADDREF, asFUNCTION(b2FixtureAdd_Ref), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassBehaviour("b2Fixture", "void f()", asBEHAVE_RELEASE, asFUNCTION(b2FixtureRemove), asCALL_CDECL_OBJLAST);
	//b2Shape::Type 	GetType () const
	AngelManager::registerClassMethod("b2Fixture", "b2Shape@ GetShape()", asMETHODPR(b2Fixture, GetShape, (void), b2Shape*), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "void SetSensor(bool sensor)", asMETHOD(b2Fixture, SetSensor), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "bool IsSensor()", asMETHOD(b2Fixture, IsSensor), asCALL_THISCALL);
	//void 	SetFilterData (const b2Filter &filter)
	//const b2Filter & 	GetFilterData () const
	//Get the contact filtering data.
	//void 	Refilter ()
	//Call this if you want to establish collision that was previously disabled by b2ContactFilter::ShouldCollide.
	AngelManager::registerClassMethod("b2Fixture", "b2Body@ GetBody()", asMETHODPR(b2Fixture, GetBody, (void), b2Body* ), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "b2Fixture@ GetNext()", asMETHODPR(b2Fixture, GetNext, (void), b2Fixture*), asCALL_THISCALL);
	//void * 	GetUserData () const
	//void 	SetUserData (void *data)
	//Set the user data. Use this to store your application specific data.
	AngelManager::registerClassMethod("b2Fixture", "bool TestPoint(const b2Vec2& in)", asMETHOD(b2Fixture, TestPoint), asCALL_THISCALL);
	//bool 	RayCast (b2RayCastOutput *output, const b2RayCastInput &input, int32 childIndex) const
	//void 	GetMassData (b2MassData *massData) const
	AngelManager::registerClassMethod("b2Fixture", "void SetDensity(float density)", asMETHOD(b2Fixture, SetDensity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "const float GetDensity()", asMETHOD(b2Fixture, GetDensity), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "const float GetFriction()", asMETHOD(b2Fixture, GetFriction), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "void SetFriction(float friction)", asMETHOD(b2Fixture, SetFriction), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "const float GetRestitution()", asMETHOD(b2Fixture, GetRestitution), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "void SetRestitution(float restitution)", asMETHOD(b2Fixture, SetRestitution), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Fixture", "const b2AABB& GetAABB(int childIndex)", asMETHOD(b2Fixture, GetAABB), asCALL_THISCALL);
	//=============================================


	//b2Joint
	//=============================================
	AngelManager::registerClass("b2Joint", 0, asOBJ_REF);
	//AngelManager::registerClassBehaviour("b2Joint", "b2Joint@ f()", asBEHAVE_FACTORY, );
	AngelManager::registerClassBehaviour("b2Joint", "void f()", asBEHAVE_ADDREF, asFUNCTION(b2JointAdd_Ref), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassBehaviour("b2Joint", "void f()", asBEHAVE_RELEASE, asFUNCTION(b2JointRemove), asCALL_CDECL_OBJLAST);
	//b2JointType 	GetType () const
	//	Get the type of the concrete joint.
	AngelManager::registerClassMethod("b2Joint", "b2Body@ GetBodyA()", asMETHOD(b2Joint, GetBodyA), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Joint", "b2Body@ GetBodyB()", asMETHOD(b2Joint, GetBodyB), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Joint", "b2Joint@ GetNext()", asMETHODPR(b2Joint, GetNext, (void), b2Joint*), asCALL_THISCALL);
	//	void * 	GetUserData () const
	//	Get the user data pointer.
	//	void 	SetUserData (void *data)
	//	Set the user data pointer.
	AngelManager::registerClassMethod("b2Joint", "bool IsActive()", asMETHOD(b2Joint, IsActive), asCALL_THISCALL);
	AngelManager::registerClassMethod("b2Joint", "bool GetCollideConnected()", asMETHOD(b2Joint, GetCollideConnected), asCALL_THISCALL);
	//	virtual void 	Dump ()
	//	Dump this joint to the log file. 
	//=============================================

	//b2JointDef
	//=============================================
	AngelManager::registerClass("b2JointDef", sizeof(b2JointDef), asOBJ_VALUE | asOBJ_POD);
	AngelManager::registerClassProperty("b2JointDef", "int type", asOFFSET(b2JointDef, type));
	AngelManager::registerClassProperty("b2JointDef", "b2Body@ bodyA", asOFFSET(b2JointDef, bodyA));
	AngelManager::registerClassProperty("b2JointDef", "b2Body@ bodyB", asOFFSET(b2JointDef, bodyB));
	AngelManager::registerClassProperty("b2JointDef", "bool collideConnected", asOFFSET(b2JointDef, collideConnected));
	//=============================================

	//b2MouseJointDef
	//=============================================
	AngelManager::registerClass("b2MouseJointDef", sizeof(b2MouseJointDef), asOBJ_VALUE | asOBJ_POD);
	//AngelManager::registerClassProperty("b2M")
	//=============================================

	//b2RevoluteJointDef
	//============================================
	AngelManager::registerClass("b2RevoluteJointDef", 0, asOBJ_REF);
	AngelManager::registerClassBehaviour("b2RevoluteJointDef", "void f()", asBEHAVE_ADDREF, asFUNCTION(b2RJointDAdd_Ref), asCALL_CDECL_OBJLAST);
	AngelManager::registerClassBehaviour("b2RevoluteJointDef", "void f()", asBEHAVE_RELEASE, asFUNCTION(b2RJointDRemove), asCALL_CDECL_OBJLAST);

	AngelManager::registerClassProperty("b2RevoluteJointDef", "int type", asOFFSET(b2RevoluteJointDef, type));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "b2Body@ bodyA", asOFFSET(b2RevoluteJointDef, bodyA));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "b2Body@ bodyB", asOFFSET(b2RevoluteJointDef, bodyB));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "bool collideConnected", asOFFSET(b2RevoluteJointDef, collideConnected));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "b2Vec2 localAnchorA", asOFFSET(b2RevoluteJointDef, localAnchorA));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "b2Vec2 localAnchorB", asOFFSET(b2RevoluteJointDef, localAnchorB));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "float referenceAngle", asOFFSET(b2RevoluteJointDef, referenceAngle));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "bool enableLimit", asOFFSET(b2RevoluteJointDef, enableLimit));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "float lowerAngle", asOFFSET(b2RevoluteJointDef, lowerAngle));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "float upperAngle", asOFFSET(b2RevoluteJointDef, upperAngle));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "bool enableMotor", asOFFSET(b2RevoluteJointDef, enableMotor));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "float motorSpeed", asOFFSET(b2RevoluteJointDef, motorSpeed));
	AngelManager::registerClassProperty("b2RevoluteJointDef", "float maxMotorTorque", asOFFSET(b2RevoluteJointDef, maxMotorTorque));
	//============================================

	//b2Manager
	//=============================================
	AngelManager::registerGlobalFunction("void MLb2Mang_CreateWorld(Vec2& in, float scale)", asFUNCTION(MLb2Manager::createWorld), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLb2Mang_SetSleep(bool sleep)", asFUNCTION(MLb2Manager::setSleep), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLb2Mang_SetStepSetting(float time, in velocityIt, int positionIt)", asFUNCTION(MLb2Manager::setStepSettings), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLb2Mang_SetScale(float scale)", asFUNCTION(MLb2Manager::setScale), asCALL_CDECL);

	AngelManager::registerGlobalFunction("b2Body@+ MLb2Mang_CreateBody(b2BodyDef@+ in)", asFUNCTION(MLb2Manager::createBody), asCALL_CDECL);
	AngelManager::registerGlobalFunction("void MLb2Mang_DestroyBody(b2BodyDef@+ in)", asFUNCTION(MLb2Manager::DestroyBody), asCALL_CDECL);
	AngelManager::registerGlobalFunction("float MLb2Mang_GetScale()", asFUNCTION(MLb2Manager::getScale), asCALL_CDECL);
	//=============================================

}

