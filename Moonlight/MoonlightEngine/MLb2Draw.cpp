#include "MLb2Draw.h"

using namespace MLEngine::MLBox2D;

MLb2Draw::MLb2Draw(void)
{
}


MLb2Draw::~MLb2Draw(void)
{
}


void MLb2Draw::Initalise(MLRenderer::MLSpriteBatch* batch)
{
	m_batcher = batch;
	m_debugFont = new MLRenderer::MLText::MLFont();
	m_debugFont->LoadFontFace(L"DebugFont/segoeui.ttf");
	m_debugFont->SetFontSize(18);
	m_debugFont->CacheChar(L"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#%^&*()_+-=/.|\';[]:,\\\"�");
}

void MLb2Draw::Initalise(MLRenderer::MLSpriteBatch* batch, MLRenderer::MLText::MLFont* debugFont)
{
	m_batcher = batch;
	m_debugFont = debugFont;

}

void MLb2Draw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{

}

void MLb2Draw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{

}

void MLb2Draw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	m_batcher->DrawCircle(Vector2f(center.x, center.y), radius, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
}

void MLb2Draw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	m_batcher->DrawCircleFill(Vector2f(center.x, center.y), radius, MLFormats::MLColour(color.r * 0.5f, color.g * 0.5f, color.b * 0.5f, 0.5f));
	m_batcher->DrawCircle(Vector2f(center.x, center.y), radius, 0.5f, 50, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
	b2Vec2 p = center + radius * axis;
	m_batcher->DrawLine(Vector2f(center.x, center.y), Vector2f(p.x, p.y), 0.5f, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
}

void MLb2Draw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	m_batcher->DrawLine(Vector2f(p1.x, p1.y), Vector2f(p2.x, p2.y), 1, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
}

void MLb2Draw::DrawTransform(const b2Transform& xf)
{
	b2Vec2 p1 = xf.p, p2;
	const float32 k_axisScale = 0.4f;

	p2 = p1 + k_axisScale * xf.q.GetXAxis();
	m_batcher->DrawLine(Vector2f(p1.x, p1.y), Vector2f(p2.x, p2.y), 1, MLFormats::MLColour(1.0f, 0.0f, 0.0f, 1.0f));

	p2 = p1 + k_axisScale * xf.q.GetYAxis();
	m_batcher->DrawLine(Vector2f(p1.x, p1.y), Vector2f(p2.x, p2.y), 1, MLFormats::MLColour(0.0f, 1.0f, 0.0f, 1.0f));
}

void MLb2Draw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{
	m_batcher->DrawPoint(Vector2f(p.x, p.y), size, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
}

void MLb2Draw::DrawString(int x, int y, const char* string, ...)
{
	char buffer[1024];
	va_list arg;
	va_start(arg, string);
	vsprintf(buffer, string, arg);
	va_end(arg);

	wchar_t  ws[1024];
	swprintf(ws, 1024, L"%hs", buffer);

	m_batcher->Write(m_debugFont, Vector2f(x,y), false, ws);
}

void MLb2Draw::DrawAABB(b2AABB* aabb, const b2Color& color)
{
	m_batcher->DrawQuad(Vector4f(aabb->lowerBound.x, aabb->lowerBound.y, aabb->upperBound.x - aabb->lowerBound.x,aabb->upperBound.y - aabb->lowerBound.y), 2, MLFormats::MLColour(color.r, color.g, color.b, 1.0f));
}
