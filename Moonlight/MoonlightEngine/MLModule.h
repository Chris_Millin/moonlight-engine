
//Angel Script Helper header
//============================
#include "AngelModule.h"
#include "AngelManager.h"
//============================

//String header
//============================
#include <string>
//============================

//Engine header
//============================
#include "MLState.h"
#include "MLStateManager.h"
//============================

#pragma once

namespace MLEngine
{
	class MLModule
	{
	public:
		MLModule(void);
		virtual ~MLModule(void);

		static bool loadGameFile(const char* fileLoc) { m_gameModFile = const_cast<char*>(fileLoc); return intialiseMod(1); }
		static bool changeModule(int modId);
		static bool update();

	private:

		static bool intialiseMod(int modID);
		static bool unloadMod();

	private:
	
		static AngelModule* m_currenMod;
		//stores the location to the xml file with the module information
		static char* m_gameModFile;

	};
}



//this class is used to help with the way that angelscript works with modules
//the class will have a create Module and a unload Module which will load and unload
//a group of scripts.
//when a module is created the old one will be destroyed which will also destroy any objects associated with
//that module such as states, any objects created within the script all textures and particle effect as well as sound
//will be unloaded.

//an xml file will contain all the script files to load and the modules used for them in the format of
//<Module id="1">
//	<Name>ModuleName</Name>
//	<LoadingScript>file/load.as<LoadingScript>
//  <Scripts>
//		<Script>file/myscript.as<Script>
//		<Script>file/myscript2.as<Script>
//	<Script>
//</Module>

//the loading script is the file the contains the script for the loading screen if it is used.
//the module ID will be used to change modules and to reference them from the script
//so once a level is finished and the state needs to change to the next level the script will have something line
//MLMod_Change(2);
//this will then unload the current module and then look to the next module in the xml file and load all the information related to it
