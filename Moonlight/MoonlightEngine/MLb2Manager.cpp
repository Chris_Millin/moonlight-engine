#include "MLb2Manager.h"

using namespace MLEngine::MLBox2D;

b2Vec2 MLb2Manager::m_gravity;
b2World* MLb2Manager::m_world;

float MLb2Manager::m_scale;
float MLb2Manager::m_timeStep;
int MLb2Manager::m_velocityIterations;
int MLb2Manager::m_positionIterations;

b2DestructionListener* MLb2Manager::m_destructionListener;
b2ContactListener* MLb2Manager::m_contactListener;
b2Draw* MLb2Manager::m_debugDraw;


MLb2Manager::MLb2Manager(void)
{
}


MLb2Manager::~MLb2Manager(void)
{
}

void MLb2Manager::createWorld(Vector2f inGravity, float scale)
{
	b2Vec2 gravity;
	gravity.Set(0.0f, 9.8f);
	m_world = new b2World(gravity);

	m_scale = scale;

	m_timeStep = 1.0f/60.0f;
	m_velocityIterations = 8;
	m_positionIterations = 8;
}

void MLb2Manager::setStepSettings(float time, int velocityIt, int positionIt)
{
	m_timeStep = time;
	m_velocityIterations = velocityIt;
	m_positionIterations = positionIt;
}

void MLb2Manager::step()
{
	m_world->Step(m_timeStep, m_velocityIterations, m_positionIterations);
	m_world->DrawDebugData();
}

b2Body* MLb2Manager::createBody(b2BodyDef* def)
{
	return m_world->CreateBody(def);
}

b2Joint* MLb2Manager::createJoint(b2JointDef* def)
{
	return m_world->CreateJoint(def);
}


void MLb2Manager::DestroyBody(b2Body* body)
{
	m_world->DestroyBody(body);
}

void MLb2Manager::DestroyJoint(b2Joint* joint)
{
	m_world->DestroyJoint(joint);
}

void MLb2Manager::setDestructionListener(b2DestructionListener* listener)
{
	m_destructionListener = listener;
	m_world->SetDestructionListener(m_destructionListener);
}

void MLb2Manager::setContactListener(b2ContactListener* listener)
{
	m_contactListener = listener;
	m_world->SetContactListener(m_contactListener);
}

void MLb2Manager::setDebugDraw(b2Draw* debugDraw)
{
	m_debugDraw = debugDraw;
	m_world->SetDebugDraw(m_debugDraw);
}