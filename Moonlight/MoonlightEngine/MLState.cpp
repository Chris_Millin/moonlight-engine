#include "MLState.h"

using namespace MLEngine;

MLState::MLState(void)
{
	m_InitaliseComplete = false;
}


MLState::~MLState(void)
{
}

void MLState::obtainScriptFunctions(AngelModule* module)
{
	m_currentModule = module;
	m_functions.intialise	= m_currentModule->findObjectFunction("void Initalise()",	m_scriptObj->GetObjectType());
	m_functions.update		= m_currentModule->findObjectFunction("void Update()",		m_scriptObj->GetObjectType());
	m_functions.draw		= m_currentModule->findObjectFunction("void Draw()",		m_scriptObj->GetObjectType());
	m_functions.release		= m_currentModule->findObjectFunction("void Release()",		m_scriptObj->GetObjectType());
}

void MLState::Initalise()
{
	_GLCore->setThreadContext();
	m_currentModule->startFunction(m_functions.intialise);
	m_currentModule->setObject(m_scriptObj);
	m_currentModule->runFunction();
	m_InitaliseComplete = true;
}

void MLState::Update()
{
	m_currentModule->startFunction(m_functions.update);
	m_currentModule->setObject(m_scriptObj);
	m_currentModule->runFunction();
}

void MLState::Draw()
{
	m_currentModule->startFunction(m_functions.draw);
	m_currentModule->setObject(m_scriptObj);
	m_currentModule->runFunction();
}

void MLState::Unload()
{
	m_currentModule->startFunction(m_functions.release);
	m_currentModule->setObject(m_scriptObj);
	m_currentModule->runFunction();
}