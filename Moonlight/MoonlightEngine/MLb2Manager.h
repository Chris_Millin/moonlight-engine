
//Box2D header
//===========================
#include <Box2D/Box2D.h>
//===========================

//MoonLight Math header
//===========================
#include <MLMath/MLMath.h>
//===========================
#pragma once

using namespace MLMath::MLVector2;

namespace MLEngine
{
	namespace MLBox2D
	{
		class MLb2Manager
		{
		public:
			MLb2Manager(void);
			~MLb2Manager(void);

			static void createWorld(Vector2f inGravity, float scale);
			static void setSleep(bool sleep) { m_world->SetAllowSleeping(sleep); }
			static void setStepSettings(float time, int velocityIt, int positionIt);
			static void setScale(float scale) { m_scale = scale; }

			static b2Body* createBody(b2BodyDef* def);
			static b2Joint* createJoint(b2JointDef* def);
			static void DestroyBody(b2Body* body);
			static void DestroyJoint(b2Joint* joint);

			static void setDestructionListener(b2DestructionListener* listener);
			static void setContactListener(b2ContactListener* listener);
			static void setDebugDraw(b2Draw* debugDraw);

			static b2World* getWorld() { return m_world; }
			static float getScale() { return m_scale; }
			static void step();
		private:

			static b2Vec2 m_gravity;
			static b2World* m_world;

			static float m_scale;

			static float m_timeStep;
			static int m_velocityIterations;
			static int m_positionIterations; 

			static b2DestructionListener* m_destructionListener;
			static b2ContactListener* m_contactListener;
			static b2Draw* m_debugDraw;
		};
	}
}


