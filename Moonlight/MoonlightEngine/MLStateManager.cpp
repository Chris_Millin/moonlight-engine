#include "MLStateManager.h"


using namespace MLEngine;

bool							MLStateManager::m_loading;
bool							MLStateManager::m_UnloadedLoading;
MLState*						MLStateManager::m_loadingState;
MLState*						MLStateManager::m_activeState;
std::map<std::string, MLState*>	MLStateManager::m_stateStack;
MLRenderer::MLSpriteBatch*		MLStateManager::m_spriteBatch;

MLStateManager::MLStateManager(void)
{
	
}


MLStateManager::~MLStateManager(void)
{
}

void MLStateManager::Initalise()
{
	m_spriteBatch = new MLRenderer::MLSpriteBatch(0x500000);
	m_UnloadedLoading = true;
}

void MLStateManager::addState(std::string& stateName, MLState* state)
{
	m_stateStack[stateName] = state;	
}

void MLStateManager::setLoadingState(MLState* state)
{
	m_loadingState = state;
}

void MLStateManager::changeState(std::string& stateName)
{
	std::map<std::string, MLState*>::iterator it = m_stateStack.find(stateName);
	if(it == m_stateStack.end())
	{
		wprintf(L"State % Could not be found", stateName);
	}
	else
	{
		m_activeState = m_stateStack[stateName];
		m_activeState->startThreadedLoad();
		m_loading = false;
	}
}

void MLStateManager::changeState(std::string& stateName, bool showLoading)
{
	std::map<std::string, MLState*>::iterator it = m_stateStack.find(stateName);
	if(it == m_stateStack.end())
	{
		wprintf(L"State %s Could not be found", stateName);
	}
	else
	{
		MLError::ErrorHandler::ML_Printf(ML_App, "%g error before thread\n");
		wprintf(L"starting loading active state\n");
		m_activeState = m_stateStack[stateName];
		m_activeState->startThreadedLoad();
		if(m_UnloadedLoading == true)
			m_loadingState->Initalise();
		m_loading = showLoading;
	}
}

void MLStateManager::updateState()
{
	m_spriteBatch->Begin();
	if (m_activeState->loadedAssets() == true && m_loading == false)
	{
		m_activeState->Update();
		m_activeState->Draw();
	}
	else if(m_loading == true && m_activeState->loadedAssets() == false)
	{
		m_loadingState->Update();
		m_loadingState->Draw();
	}
	
	if (m_activeState->loadedAssets() == true && m_UnloadedLoading == false)
	{
		m_loading = false;
		m_loadingState->startThreadedUnloading();
		m_UnloadedLoading = true;
	}
	m_spriteBatch->End();
}