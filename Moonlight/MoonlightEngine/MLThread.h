#include <Windows.h>
#include <stdio.h>
#include <map>
#pragma once

namespace MLEngine
{
	class MLThread
	{
	public:
		MLThread(void);
		~MLThread(void);

		void createThread();
		virtual void threadEntry() {};
		void sleepThread(long ms);
		void suspendThread();
		void resumeThread();
		void stopThread();

		void setThreadPriority(int p);

		bool waitMutex(const wchar_t* mutexName, long ms = 5000);
		void releaseMutex(const wchar_t* mutexName);


	private:
		//The thread handler used to obtain the thread
		DWORD* m_ThreadHandle;
		DWORD m_ThreadID;
	};

	class MLMutexDictionary
	{
	public:

		static bool addMutex(wchar_t* mutexName);
		static DWORD* getHandler(wchar_t* mutexName) {return m_mutexStack[mutexName];}
		static void RemoveMutex(wchar_t* mutexName);

	private:
		static std::map<wchar_t*, DWORD*> m_mutexStack;
	};
}

extern "C"
{
	DWORD _ThreadEntry(void* param);
};

