#include "AngelManager.h"

//Engine header
//===========================
#include "MLStateManager.h"
#include "MLb2Manager.h"
#include <MLMath/MLMath.h>
#include <MLRenderHeaders.h>
#include <MLInputManager.h>
#include <MLAudioHeaders.h>
#include <Box2D/Box2D.h>
//===========================
#pragma once
class MLAngelRegister
{
public:
	MLAngelRegister(void);
	virtual ~MLAngelRegister(void);

	static void setGlobalSpriteBatch(MLRenderer::MLSpriteBatch* sb);
	static void registerEngine();
	static void registerMaths();
	static void registerInput();
	static void registerBox2D();
	static void registerGraphics();
	static void registerAudio();

	static std::map<b2Body*, int>			m_b2BodyRef;
	static std::map<b2BodyDef*, int>		m_b2BodyDefRef;

	static std::map<b2Fixture*, int>		m_b2FixtureRef;

	static std::map<b2Shape*, int>			m_b2ShapeRef;
	static std::map<b2PolygonShape*, int>	m_b2PShapeRef;

	static std::map<b2Joint*, int>			m_b2JointRef;
	static std::map<b2JointDef*, int>		m_b2JointDefRef;
	static std::map<b2RevoluteJointDef*, int> m_b2RJointDefRef;
	static std::map<b2MouseJointDef*, int>	m_b2MouseJointDefRef;

};

void Vec2Construct(void* memory);
void Vec2Construct(void* memory, float x);
void Vec2Construct(void* memory, float x, float y);
void Vec3Construct(void* memory);
void Vec3Construct(void* memory, float x);
void Vec3Construct(void* memory, float x, float y, float z);
void Vec4Construct(void* memory);
void Vec4Construct(void* memory, float x);
void Vec4Construct(void* memory, float x, float y, float z, float w);

MLRenderer::MLText::MLFont* MLFontFactory();
MLRenderer::MLParticleEmitter* MLPEmitterFactory();
MLRenderer::MLCamera* MLCameraFactory();

void b2BodyAdd_Ref(b2Body* obj);
void b2BodyRemove(b2Body* obj);

void b2BodyDAdd_Ref(b2BodyDef* obj);
void b2BodyDRemove(b2BodyDef* obj);

void b2JointAdd_Ref(b2Joint* obj);
void b2JointRemove(b2Joint* obj);

void b2JointDAdd_Ref(b2JointDef* obj);
void b2JointDRemove(b2JointDef* obj);

void b2RJointDAdd_Ref(b2RevoluteJointDef* obj);
void b2RJointDRemove(b2RevoluteJointDef* obj);

void b2FixtureAdd_Ref(b2Fixture* obj);
void b2FixtureRemove(b2Fixture* obj);

void b2ShapeAdd_Ref(b2Shape* obj);
void b2ShapeRemove(b2Shape* obj);

void b2PShapeAdd_Ref(b2PolygonShape* obj);
void b2PShapeRemove(b2PolygonShape* obj);

