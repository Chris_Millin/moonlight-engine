#include "MLThread.h"

using namespace MLEngine;

DWORD _ThreadEntry(void* param)
{
	MLThread* _thread = (MLThread*)param;
	_thread->threadEntry();
	return 0;
}


MLThread::MLThread(void)
{
	m_ThreadHandle = NULL;
	m_ThreadID = NULL;
}


MLThread::~MLThread(void)
{
}

void MLThread::createThread()
{
	m_ThreadHandle = (DWORD*)CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)_ThreadEntry,(MLThread*)this,0,&m_ThreadID);
	wprintf(L"[Thread] Thread ID = %d, Thread Handler = %d\n", m_ThreadID, m_ThreadHandle);
	if(m_ThreadHandle == NULL)
	{
	  wprintf(L"[Thread] Could not create Thread");
	}
}

void MLThread::sleepThread(long ms)
{
	Sleep(ms);
}

void MLThread::stopThread()
{
	if(m_ThreadHandle == NULL)
		return;
	WaitForSingleObject(m_ThreadHandle, INFINITE);
	CloseHandle(m_ThreadHandle);
	m_ThreadHandle = NULL;
}

void MLThread::suspendThread()
{
	if (m_ThreadHandle == NULL)
		return;
	else
		if(SuspendThread(m_ThreadHandle) < 0)
			return;
}

void MLThread::resumeThread()
{
	if (m_ThreadHandle == NULL)
		return;
	else
		if(ResumeThread(m_ThreadHandle) < 0)
			return;
}


void MLThread::setThreadPriority(int p)
{

}

bool MLThread::waitMutex(const wchar_t* mutexName, long ms /* = 5000 */)
{
	HANDLE h = OpenMutexW(MUTEX_ALL_ACCESS, FALSE, mutexName);
	if (h == NULL)
	{
		wprintf(L"[Thread] Mutex not found\n");
	}
	DWORD d = WaitForSingleObject(m_ThreadHandle, ms);
	return true;
}

void MLThread::releaseMutex(const wchar_t* mutexName)
{
	HANDLE h = OpenMutexW(MUTEX_ALL_ACCESS, FALSE, mutexName);
	if (h == NULL)
	{
		wprintf(L"[Thread] Mutex not found\n");
	}
	if(ReleaseMutex(h) == 0)
	{
		wprintf(L"[Thread] Fail to release Mutex\n");
	}
}




//Mutex Class
std::map<wchar_t*, DWORD*> MLMutexDictionary::m_mutexStack;

bool MLMutexDictionary::addMutex(wchar_t* mutexName)
{
	if (m_mutexStack[mutexName] != NULL)
	{
		CloseHandle(mutexName);
		m_mutexStack.erase(mutexName);
	}
	m_mutexStack[mutexName] = (DWORD*)CreateMutexW(NULL,FALSE,mutexName);
	if(m_mutexStack[mutexName] == NULL)
	{
		m_mutexStack.erase(mutexName);
	}
	return true;
}

void MLMutexDictionary::RemoveMutex(wchar_t* mutexName)
{
	if(m_mutexStack[mutexName] != NULL)
	{
		CloseHandle(m_mutexStack[mutexName]);
	}
	m_mutexStack.erase(mutexName);
}

