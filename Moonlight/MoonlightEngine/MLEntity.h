#pragma once
class MLEntity
{
	enum functionDef
	{
		E_INIT = 0,
		E_UPDATE = 1,
		E_DRAW = 2,
		E_RELEASE = 3
	};

public:
	MLEntity(void);
	~MLEntity(void);

	void initalise();
	void update();
	void draw();
	void release();

private:

	void RegisterFunction(functionDef functionType, int functionID);

private:

	//stores the lua function ID's
	int functionIDs[4]; 


};


//The way this will work is that with the entity only setFunction will be exposed to lua
//this means that when local c = mycClass.new() is called
//c:setFunction(EntityFunc.Initalise, initaliseActor1)
//
//function initaliseActor1()
//	io.write("Actor1");
//end
//
//can be written and means that that will invoke the c Entity's initalise function
//its not the most eligent way of doing it but will allow for an actor manager to exist and
//for a way to override the c++ functions from within lua for the actor 
