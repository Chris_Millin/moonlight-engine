#include "AngelClass.h"

asIScriptEngine* AngelClass::m_engine;

AngelClass::AngelClass()
{

}


AngelClass::~AngelClass(void)
{
}

template <class T>
bool AngelClass::registerClass(const char* className)
{
	int r;
	r =	m_engine->RegisterObjectType(className, sizeof(T), asOBJ_VALUE | asOBJ_POD);
	assert( r >= 0 );
	if (r < 0)
		return false; 
}

bool AngelClass::registerClass(const char* className, int byteSize, int objFlag)
{
	int r;
	r =	m_engine->RegisterObjectType(className, byteSize, objFlag);
	assert( r >= 0 );
	if (r < 0)
		return false; 
}

bool AngelClass::registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer)
{
	int r;
	if(behaviour == asBEHAVE_FACTORY)
	{
		r = m_engine->RegisterObjectBehaviour(className, behaviour, functionDef, funcPointer, asCALL_CDECL);
	}
	else
	{
		r = m_engine->RegisterObjectBehaviour(className, behaviour, functionDef, funcPointer, asCALL_THISCALL);
	}
	assert( r >= 0 );
	if (r < 0)
		return false; 
}

bool AngelClass::registerClassBehaviour(const char* className, const char* functionDef, asEBehaviours behaviour, const asSFuncPtr &funcPointer, int flag)
{
	int r;
	r = m_engine->RegisterObjectBehaviour(className, behaviour, functionDef, funcPointer, flag);
	assert( r >= 0 );
	if (r < 0)
		return false; 
}

bool AngelClass::registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer)
{
	int r;
	r = m_engine->RegisterObjectMethod(className, functionDef, funcPointer, asCALL_THISCALL);
	assert( r >= 0 );
	if (r < 0)
		return false;
}

bool AngelClass::registerClassMethod(const char* className, const char* functionDef, const asSFuncPtr &funcPointer, int flag)
{
	int r;
	r = m_engine->RegisterObjectMethod(className, functionDef, funcPointer, flag);
	assert( r >= 0 );
	if (r < 0)
		return false;
}

bool AngelClass::registerClassProperty(const char* className, const char* propDef, int offset)
{
	int r;
	r = m_engine->RegisterObjectProperty(className, propDef, offset);
	assert( r >= 0 );
	if (r < 0)
		return false;
}

bool AngelClass::registerInterface(const char* interfaceName)
{
	int r;
	r = m_engine->RegisterInterface(interfaceName);
	assert( r >= 0 );
	if (r < 0)
		return false;
}

bool AngelClass::registerInterfaceMethod(const char* interfaceName, const char* functionDef)
{
	int r;
	r = m_engine->RegisterInterfaceMethod(interfaceName, functionDef);
	assert( r >= 0 );
	if (r < 0)
		return false;
}