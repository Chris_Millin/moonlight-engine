//Angel Script header
//===========================
#include <angelscript.h>
//===========================

//Angel Script Add-On header
//===========================
#include <scriptbuilder/scriptbuilder.h>
//===========================

//Vector header
//==========================
#include <vector>
//==========================

//assert header
//==========================
#include <assert.h>
//==========================

//Angel Script Helper header
//==========================
#include "AngelClass.h"
//==========================
#pragma once


class AngelModule
{
public:
	AngelModule(void);
	~AngelModule(void);

	bool createModule(asIScriptEngine* engine,const char* moduleName);
	
	bool LoadScript(const char* scriptLoc);
	bool buildModule();
	bool rebuildModule();
	bool unloadModule();
	
	asIScriptFunction* findFunction(const char* functionDef);
	asIScriptFunction* findObjectFunction(const char* functionDef, const char* objectName);
	asIScriptFunction* findObjectFunction(const char* functionDef, asIObjectType* obj);
	asIScriptFunction* findObjectFactoryFunction(const char* functionDef, const char* objectName);
	asIScriptFunction* findObjectFactoryFunction(const char* functionDef, asIObjectType* obj);

	asIObjectType* getObject(const char* objectName);
	std::vector<asIObjectType*> getObjectsFromInterface(const char* interfaceName);



	bool startFunction(const char* functionDef);
	bool startFunction(asIScriptFunction* func);
	bool setObject(void* obj);
	bool addArg(int val);
	bool addArg(bool val);
	bool addArg(float val);
	bool addArg(double val);
	bool addArg(short val);
	bool addArg(char val);
	bool addArg(void* val);
	bool runFunction();
	asIScriptObject* runSOFunction();


	char* getModuleName() { return m_name; }
	asIScriptModule* getModule() { return m_module; }

private:

	int m_currentArgIndex;
	asIScriptFunction* m_currentFunction;
	asIScriptContext* m_currentContext;
	CScriptBuilder m_scriptBuilder;
	asIScriptModule* m_module;
	asIScriptEngine* m_engine;
	std::vector<asIScriptContext*> m_contexts;
	std::vector<const char*> m_loadedScripts;
	char* m_name;
};

