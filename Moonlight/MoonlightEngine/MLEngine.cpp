#include "MLEngine.h"

using namespace MLEngine;
using namespace MLMath::MLVector4;

MLRenderer::MLCoreGL* _GLCore;

MLEngineCore::MLEngineCore(void)
{
}


MLEngineCore::~MLEngineCore(void)
{
}


void MLEngineCore::InitaliseEngine(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd, wchar_t configFile)
{
	//create the window
	m_windowsHandler = new MLWindows::windowsHandler(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
	m_windowsHandler->createWindow(L"MoonLightRenderDLL", 1280, 720, 32, true);

	//set up the openGL stuff
	_GLCore = new MLRenderer::MLCoreGL();
	_GLCore->Bind(hInstance, MLWindows::windowsHandler::_hWnd, MLWindows::windowsHandler::_wWidth, MLWindows::windowsHandler::_wHeight, MLWindows::windowsHandler::_BitMode);
	_GLCore->initaliseCore();

	

	//set up the keyboard initalise the font core and also set the resize and input functions
	MLInput::MLInputManager::Initalise();
	MLInput::MLInputManager::createRawKeyboard();
	MLRenderer::MLText::MLFontCore::Initalise();
	MLRenderer::MLCameraManager::Initalise();
	MLAudio::MLAudioManager::InitaliseAudio();
	MLWindows::setResizeFunction(&MLRenderer::ResizeGL);
	MLWindows::setInputFunction(&MLInput::MLInputManager::inputMSGHandler);

	m_b2DebugDraw = new MLEngine::MLBox2D::MLb2Draw();

	m_b2DebugSpriteBatch = new MLRenderer::MLSpriteBatch(0x500000);
	m_b2DebugDraw->Initalise(m_b2DebugSpriteBatch);
	m_b2DebugDraw->SetFlags(b2Draw::e_shapeBit);

	MLEngine::MLBox2D::MLb2Manager::createWorld(Vector2f(0,-9.8f), 30);
	MLEngine::MLBox2D::MLb2Manager::setDebugDraw(m_b2DebugDraw);
	MLEngine::MLBox2D::MLb2Manager::setScale(30);

	//This is where launch lua is run and the statemanager can be loaded with scripts and states 
 	//DemoState* ds = new DemoState();
 	//DemoLoader* dl = new DemoLoader();

	MLStateManager::Initalise();
	MLEngine::AngelManager::Initalise();
	MLAngelRegister::registerEngine();
	MLAngelRegister::registerMaths();
	MLAngelRegister::registerGraphics();
	MLAngelRegister::registerInput();
	//MLAngelRegister::setGlobalSpriteBatch(MLStateManager::getSBatch());
 	//MLStateManager::setLoadingState(dl);
	//MLStateManager::addState(std::string("DemoState"), ds);
 	//MLStateManager::changeState(std::string("DemoState"), true);

	//MLEngine::MLLuaManager::openLua();
	
	textureID1 = MLRenderer::MLTextureManager::loadTexture(L"textures/Emit2.png");

//	loadGame("");
}



void MLEngineCore::updateEngine()
{
	

	while(m_windowsHandler->update())
	{

		if(MLInput::MLInputManager::isKeyPressed(MLKeys::Up))
		{
			MLRenderer::MLCameraManager::moveActiveCamera(0, -5);
		}
		if(MLInput::MLInputManager::isKeyPressed(MLKeys::Down))
		{
			MLRenderer::MLCameraManager::moveActiveCamera(0, 5);
		}
		if(MLInput::MLInputManager::isKeyPressed(MLKeys::Left))
		{
			MLRenderer::MLCameraManager::moveActiveCamera(-5, 0);
		}
		if(MLInput::MLInputManager::isKeyPressed(MLKeys::Right))
		{
			MLRenderer::MLCameraManager::moveActiveCamera(5, 0);
		}

		_GLCore->startFrame();
		m_b2DebugSpriteBatch->Begin();
		//MLModule::update();
		//MLStateManager::updateState();
		m_b2DebugSpriteBatch->Draw(textureID1, Vector2f(100,100), true, Vector4f(0,0,24,24), Vector4f(1.0f));
		m_b2DebugSpriteBatch->Draw(textureID1, Vector2f(100,100), false, Vector4f(0,0,24,24), Vector4f(1.0f));
		MLEngine::MLBox2D::MLb2Manager::step();
		m_b2DebugSpriteBatch->End();
		_GLCore->endFrame();
	}
}

void MLEngineCore::reloadEngine()
{

}

void MLEngineCore::release()
{

}


bool MLEngineCore::loadGame(const char* file)
{
	return MLEngine::MLModule::loadGameFile(file);
}