#include <map>
#include <MLRenderHeaders.h>
#include "MLState.h"
#include "MLEngineHeader.h"
#pragma once
namespace MLEngine
{
	class MLStateManager
	{
	public:
		MLStateManager(void);
		~MLStateManager(void);

		static void Initalise();

		static void addState(std::string& stateName, MLState* state);
		//this will be run in a separate thread whenever the state changes
		static void setLoadingState(MLState* state);

		static void changeState(std::string& stateName);
		static void changeState(std::string& stateName, bool showLoading);
		static void removeState(std::string& stateName);

		static void updateState();

		static MLRenderer::MLSpriteBatch* getSBatch() { return m_spriteBatch; }

	private:

		static MLRenderer::MLSpriteBatch* m_spriteBatch;
		static bool m_loading;
		static bool m_UnloadedLoading;
		static MLState* m_loadingState;
		static MLState* m_activeState;
		static std::map<std::string, MLState*> m_stateStack;
	};
}

