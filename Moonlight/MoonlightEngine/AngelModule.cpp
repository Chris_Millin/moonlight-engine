#include "AngelModule.h"


AngelModule::AngelModule(void)
{
}


AngelModule::~AngelModule(void)
{
}

bool AngelModule::createModule(asIScriptEngine* engine,const char* moduleName)
{
	int r;
	m_engine = engine;
	m_name = const_cast<char*>(moduleName);
	r = m_scriptBuilder.StartNewModule(m_engine, m_name);
	if (r < 0)
		return false;
	
	m_module = m_engine->GetModule(m_name);

	for (int i = 0; i < 10; i++)
		m_contexts.push_back(m_engine->CreateContext());

}


bool AngelModule::LoadScript(const char* scriptLoc)
{
	int r;
	r = m_scriptBuilder.AddSectionFromFile(scriptLoc);
	if (r < 0)
		return false;
	m_loadedScripts.push_back(scriptLoc);
}

bool AngelModule::buildModule()
{
	int r;
	r = m_scriptBuilder.BuildModule();
	assert(r >= 0 );
	if (r < 0)
		return false;
}

bool AngelModule::unloadModule()
{
	int r;
	r = m_engine->DiscardModule(m_name);
	if ( r < 0)
		return false;
	m_loadedScripts.clear();
}

bool AngelModule::rebuildModule()
{
	return true;
}

asIScriptFunction* AngelModule::findFunction(const char* functionDef)
{
	return m_module->GetFunctionByDecl(functionDef);
}

asIScriptFunction* AngelModule::findObjectFunction(const char* functionDef, const char* objectName)
{
	asIObjectType* obj = getObject(objectName);
	return obj->GetMethodByDecl(functionDef);
}

asIScriptFunction* AngelModule::findObjectFunction(const char* functionDef, asIObjectType* obj)
{
	return obj->GetMethodByDecl(functionDef);
}

asIScriptFunction* AngelModule::findObjectFactoryFunction(const char* functionDef, const char* objectName)
{
	asIObjectType* obj = getObject(objectName);
	return obj->GetFactoryByDecl(functionDef);
}

asIScriptFunction* AngelModule::findObjectFactoryFunction(const char* functionDef, asIObjectType* obj)
{
	return obj->GetFactoryByDecl(functionDef);
}

asIObjectType* AngelModule::getObject(const char* objectName)
{
	return m_module->GetObjectTypeByName(objectName);
}

std::vector<asIObjectType*> AngelModule::getObjectsFromInterface(const char* interfaceName)
{
	std::vector<asIObjectType*> objects;

	int tc = m_module->GetObjectTypeCount();
	for( int n = 0; n < tc; n++ )
	{
		bool found = false;
		asIObjectType* obj = m_module->GetObjectTypeByIndex(n);
		int ic = obj->GetInterfaceCount();
		for( int i = 0; i < ic; i++ )
		{
			if(strcmp(obj->GetInterface(i)->GetName(), interfaceName) == 0 )
			{
				found = true;
				objects.push_back(obj);
				break;
			}
		}
	}

	return objects;
}

bool AngelModule::startFunction(const char* functionDef)
{
	m_currentArgIndex = 0;
	m_currentFunction = NULL;
	m_currentFunction = m_module->GetFunctionByDecl(functionDef);
	if (m_currentFunction == NULL)
		return false;

	if (m_contexts.size())
	{
		m_currentContext = *m_contexts.rbegin();
		m_contexts.pop_back();
	}
	else
	{
		m_currentContext = m_engine->CreateContext();
	}

	if (m_currentContext == NULL)
		return false;

	int r;
	r = m_currentContext->Prepare(m_currentFunction);
	assert( r >= 0 );
	if (r < 0)
		return false;

	return true;
}

bool AngelModule::startFunction(asIScriptFunction* func)
{
	m_currentFunction = func;

	if(m_currentFunction == NULL)
		return false;

	if (m_contexts.size())
	{
		m_currentContext = *m_contexts.rbegin();
		m_contexts.pop_back();
	}
	else
	{
		m_currentContext = m_engine->CreateContext();
	}

	if (m_currentContext == NULL)
		return false;

	int r;
	r = m_currentContext->Prepare(m_currentFunction);
	assert( r >= 0 );
	if (r < 0)
		return false;

	return true;
}

bool AngelModule::setObject(void* obj)
{
	int r;
	r = m_currentContext->SetObject(obj);
	assert( r >= 0 );
	if ( r < 0)
		return false;
	return true;
}

bool AngelModule::addArg(int val)
{
	int r;
	r = m_currentContext->SetArgDWord(m_currentArgIndex, val);
	assert( r >= 0 );
	if ( r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(bool val)
{
	int r;
	r = m_currentContext->SetArgByte(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(float val)
{
	int r;
	r = m_currentContext->SetArgFloat(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(short val)
{
	int r;
	r = m_currentContext->SetArgWord(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(double val)
{
	int r;
	r = m_currentContext->SetArgDouble(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(char val)
{
	int r;
	r = m_currentContext->SetArgByte(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::addArg(void* val)
{
	int r;
	r = m_currentContext->SetArgObject(m_currentArgIndex, val);
	assert( r >= 0 );
	if (r < 0)
		return false;
	m_currentArgIndex++;
	return true;
}

bool AngelModule::runFunction()
{
	int r;
	r = m_currentContext->Execute();
	if( r != asEXECUTION_FINISHED )
	{
		if( r == asEXECUTION_EXCEPTION )
		{
			printf("\nException: %s\n", m_currentContext->GetExceptionString());
			printf("Function: %s\n", m_currentContext->GetExceptionFunction()->GetDeclaration());
			printf("Line: %s\n", m_currentContext->GetExceptionLineNumber());
		}
		
	}

	m_contexts.push_back(m_currentContext);
	m_currentContext->Unprepare();

	return true;
}

asIScriptObject* AngelModule::runSOFunction()
{
	asIScriptObject* returnAd = NULL;
	int r;
	r = m_currentContext->Prepare(m_currentFunction);
	assert( r >= 0 );
	if (r < 0)
		return false;
	r = m_currentContext->Execute();
	if( r != asEXECUTION_FINISHED )
	{
		if( r == asEXECUTION_EXCEPTION )
		{
			printf("Exception: %s", m_currentContext->GetExceptionString());
			printf("Function: %s", m_currentContext->GetExceptionFunction()->GetDeclaration());
			printf("Line: %s", m_currentContext->GetExceptionLineNumber());
		}

	}
	else if( r == asEXECUTION_FINISHED )
	{
		returnAd = *((asIScriptObject**)m_currentContext->GetAddressOfReturnValue());
		returnAd->AddRef();
	}

	m_contexts.push_back(m_currentContext);
	m_currentContext->Unprepare();

	return returnAd;
}